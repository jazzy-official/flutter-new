import 'package:flutter/material.dart';
import 'package:room_buddy/Models/room_model.dart';
import 'package:room_buddy/Screens/Home/Widgets/room_card.dart';

import 'city_card.dart';

class RoomList extends StatelessWidget {

  final List<Room> rooms;
  const RoomList({Key? key, required this.rooms}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      // physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
      controller: ScrollController(),
      itemCount: rooms.length,
      itemBuilder: (context, index) {
        return  RoomCard(room: rooms[index],);
      },
    );
  }
}
