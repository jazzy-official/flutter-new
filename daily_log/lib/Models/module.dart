
import 'package:daily_log/Models/sub_module.dart';

class Module {
  final String? id;
  final String name;
   bool status;
  final List<SubModule>? submodules;


  Module( {required this.status,required this.name,this.id,this.submodules});



  factory Module.fromJson(dynamic json, key) {
    return Module(
      id: key.toString(),
      name: json['name'],
      status : json['status'],
      submodules: json['submodules'] == null ? [] :SubModule.fromSubModuleMap(json['submodules']),

    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['status'] = status;
    data['submodules'] = submodules != null ?  SubModule.toSubModuleMap(submodules!):null;
    return data;
  }

  static Map<dynamic, dynamic> toModuleMap(List<Module> cartItems) {
    Map<dynamic, dynamic> orderMap =  <dynamic, dynamic>{};
    for (Module cart in cartItems) {
      orderMap[cart.id] = {
        'name': cart.name,
        'status': cart.status,
        'submodules' : SubModule.toSubModuleMap(cart.submodules!),
      };
    }
    return orderMap;
  }

  static fromModuleMap(var map) {
    Map values = map as Map;
    List<Module> cartItem = [];
    values.forEach((key, data) {
      final Module connect = Module.fromJson(data,key);
      cartItem.add(connect);
    });
    return cartItem;
  }
}