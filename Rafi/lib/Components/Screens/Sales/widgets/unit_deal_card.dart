import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:rafi_admin/Components/Screens/Sales/Models/deal_unit_model.dart';

class DealUnitCard extends StatelessWidget {
  final DealUnit dealUnit;
  DealUnitCard({Key? key, required this.dealUnit}) : super(key: key);
  var formatter = NumberFormat('#,##,000');
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        height: 80,
        padding: const EdgeInsets.all(12.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Text("Unit Name : ",style: TextStyle(fontWeight: FontWeight.w500,fontSize: 16),),
                    Text(dealUnit.unitNo+"-"+dealUnit.phase,style: TextStyle(fontSize: 17),),
                  ],
                ),
                Row(
                  children: [
                    Text("Status : ",style: TextStyle(fontWeight: FontWeight.w500),),
                    Text(dealUnit.status),
                  ],
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
              Column(children: [
                Text("Rate/Marla",style: TextStyle(fontWeight: FontWeight.w500),),
                Text( formatter
                    .format(dealUnit.saleRate),),
              ],),
              Column(children: [
                Text("x",style: TextStyle(color: Colors.black38),),
              ],),
              Column(children: [
                Text("Net Marla",style: TextStyle(fontWeight: FontWeight.w500),),
                Text(dealUnit.netMarla.toString()),
              ],),
                Column(children: [
                  Text("=",style: TextStyle(color: Colors.black38),),
                ],),
              Column(children: [
                Text("Net Amount",style: TextStyle(fontWeight: FontWeight.w500),),
                Text( formatter
                    .format(dealUnit.totalSaleValue),),
              ],),
            ],)
          ],
        ),
      ),
    );
  }
}
