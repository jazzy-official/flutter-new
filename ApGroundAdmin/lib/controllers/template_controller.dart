import 'package:ap_ground_admin/Screens/Grounds/Models/template_model.dart';
import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:firebase_database/firebase_database.dart';


class TemplateController {
  final DatabaseReference databaseReference =
  FirebaseDatabase.instance.reference().child("Template");

  Future<void> saveTemplate(Template template) async {
    try{ await databaseReference.push().set(template.toJson());
    }catch(e){
      displayToast(e.toString());
    }
  }

  Future<void> updateTemplate(Template template) async {

    databaseReference.update({
      template.id! : template.toJson()
    });

  }
  Future<void> deleteTemplate(String id) async {
    databaseReference.child(id).remove();

  }

  Future<List<Template>> readTemplate() async {
    List<Template> template = [];
    await databaseReference.once().then((DataSnapshot snapshot) {
      if(snapshot.value != null){
        snapshot.value.forEach((key, value) {
          Template gr = Template.fromJson(value,key);
          template.add(gr);
        });
      }

    });
    return template;
  }

}
