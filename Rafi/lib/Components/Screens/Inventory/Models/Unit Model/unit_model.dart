class UnitModel {
  int UnitId;
  String entryDate;
  String? projectName;
  String phase;
  String unitNo;
  double sqft;
  double unitMarla;
  double rate;
  double netAmount;
  String status;
  String investmentName;
  double netMarla;
  double netsqft;
  String unitType;
  String? affiliateName;
  String? buyerName;
  double? totalamount;
  String? contractDate;

  UnitModel({
    required this.unitMarla,
    this.contractDate,
    required this.status,
    this.affiliateName,
    required this.netMarla,
    required this.entryDate,
    required this.investmentName,
    required this.netAmount,
    required this.netsqft,
    required this.phase,
    this.projectName,
    required this.rate,
    required this.sqft,
    this.totalamount,
    required this.UnitId,
    required this.unitNo,
    required this.unitType,
    this.buyerName,
  });

  factory UnitModel.fromJson(Map<String, dynamic> json) {
    return UnitModel(
      UnitId: json['unitId'],
      entryDate: json['entryDate'].toString(),
      projectName: json['projectName'].toString(),
      phase: json['phaseName'].toString(),
      unitNo: json['unitNumber'].toString(),
      sqft: json['sqft'],
      unitMarla: json['unitMarla'],
      status: json['status'].toString(),
      rate: json['rate'],
      netAmount: json['netAmount'],
      investmentName: json['investmentName'].toString(),
      netMarla: json['netMarla'],
      netsqft: json['netSqft'],
      unitType: json['unitType'].toString(),
      affiliateName: json['soldDetail'] != null
          ? (json['soldDetail']['affiliateAccountNavigation'] != null
              ? json['soldDetail']['affiliateAccountNavigation']['accounttitle']
              : null).toString()
          : null,
      contractDate: json['soldDetail'] != null
          ? json['soldDetail']['contractDate']
          : null,
      totalamount:
          json['soldDetail'] != null ? json['soldDetail']['totalPrice'] : null,
      buyerName: json['soldDetail'] != null
          ? (json['soldDetail']['buyerAccountNavigation'] != null
          ? json['soldDetail']['buyerAccountNavigation']['accounttitle']
          : null)
          : null,
    );
  }
}
