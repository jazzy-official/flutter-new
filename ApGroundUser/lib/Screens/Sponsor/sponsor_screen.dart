
import 'package:ap_booking_user/controllers/sponsor_controller.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';

import 'models/sponsor_models.dart';

class SponsorScreen extends StatefulWidget {
   SponsorScreen({Key? key}) : super(key: key);

  @override
  State<SponsorScreen> createState() => _SponsorScreenState();
}

class _SponsorScreenState extends State<SponsorScreen> {
  List<Sponsor> sponsorlist=[];

  void refresh()async{
    List<Sponsor> temp=[];
    temp = await SponsorController().readSponsor();
    setState(() {
      sponsorlist = temp;
    });
  }
  @override
  void initState() {
    refresh();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
                Expanded(
          child: ListView.builder(
              physics: const BouncingScrollPhysics(
                  parent: AlwaysScrollableScrollPhysics()),
              padding: const EdgeInsets.all(8),
              itemCount: sponsorlist.length,
              itemBuilder: (BuildContext context, int index) {
                Sponsor sponsor = sponsorlist[index];
                return Center(
                  child: Container(
                      height: 500,
                      width: MediaQuery.of(context).size.width*0.96,
                      margin: EdgeInsets.only(bottom: 20),
                      decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.circular(10),
                        image: DecorationImage(
                          colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.5), BlendMode.dstATop),
                          image: NetworkImage(sponsor.imgUrl),
                          fit: BoxFit.cover,
                        ),
                        boxShadow: const [
                          BoxShadow(
                            color: Colors.black26,
                            spreadRadius: 3,
                            blurRadius: 5,
                            offset: Offset(0, 5), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(sponsor.name,style: TextStyle(color: Colors.white,fontWeight: FontWeight.w600,fontSize: 18),),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(sponsor.description,style: TextStyle(color: Colors.white,fontSize: 13),),
                                )

                              ],
                            ),
                          ],
                        ),
                      )),
                );
              }),
        ),
      ],
    );
  }
}
