class RecoveryAmmount {
  double amount;
  RecoveryAmmount({
    required this.amount,
  });

  factory RecoveryAmmount.fromJson(double json) {
    return RecoveryAmmount(
      amount: json,
    );
  }
}
