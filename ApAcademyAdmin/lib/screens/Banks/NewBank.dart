import 'dart:io';

import 'package:ap_academy_admin/ModelView/Controller/BankController.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class AddBanks extends StatefulWidget {
  @override
  _AddBanksState createState() => _AddBanksState();
}

class _AddBanksState extends State<AddBanks> {
  TextEditingController bankNameController = TextEditingController();
  TextEditingController accNameController = TextEditingController();
  TextEditingController bankAccController = TextEditingController();
  TextEditingController fpsAccController = TextEditingController();
  final bankController = Get.put(BanksController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: MyResources.buttonColor,
        brightness: Brightness.light,
        title: Text('ADD BANK'),
        centerTitle: true,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 80.0),
            child: Container(
              child: Icon(
                Icons.account_balance_outlined,
                size: 100,
                color: MyResources.buttonColor,
              ),
            ),
          ),
          Padding(
            //Add padding around textfield
            padding: EdgeInsets.only(top: 7.0, left: 20, right: 20),
            child: Container(
              height: 50,
              child: TextField(
                controller: bankNameController,
                decoration: InputDecoration(
                  hintText: 'Bank Name',
                  fillColor: Colors.white,
                  //Add th Hint text here.
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            //Add padding around textfield
            padding: EdgeInsets.only(top: 7.0, left: 20, right: 20),
            child: Container(
              height: 50,
              child: TextField(
                controller: accNameController,
                decoration: InputDecoration(
                  hintText: 'Account Name',
                  fillColor: Colors.white,
                  //Add th Hint text here.
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            //Add padding around textfield
            padding: EdgeInsets.only(top: 7.0, left: 20, right: 20),
            child: Container(
              height: 50,
              child: TextField(
                controller: bankAccController,
                decoration: InputDecoration(
                  hintText: 'Account Number',
                  fillColor: Colors.white,
                  //Add th Hint text here.
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            //Add padding around textfield
            padding: EdgeInsets.only(top: 7.0, left: 20, right: 20),
            child: Container(
              height: 50,
              child: TextField(
                controller: fpsAccController,
                decoration: InputDecoration(
                  hintText: 'FPS ID',
                  fillColor: Colors.white,
                  //Add th Hint text here.
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: RaisedButton(
              child: Text('SAVE'),
              textColor: Colors.white,
              color: MyResources.buttonColor,
              onPressed: () async {
                Get.dialog(LoadingWidget());
                if (bankNameController.text != '' &&
                    bankAccController.text != '' &&
                    accNameController.text!="" &&
                fpsAccController.text != '') {
                  Map<String, dynamic> data = {
                    'bankName':bankNameController.text,
                    'accName': accNameController.text,
                    'BankAcc': bankAccController.text,
                    'fpsid': fpsAccController.text,
                  };
                  await bankController.addBanks(data);
                  Get.back();
                  Get.defaultDialog(
                    title: 'Saved',
                    content: Text('You Bank Acc is saved'),
                    textConfirm: 'ok',
                    confirmTextColor: Colors.white,
                    buttonColor: MyResources.buttonColor,
                    onConfirm: () {
                      bankNameController.clear();
                      accNameController.clear();
                      bankAccController.clear();
                      fpsAccController.clear();
                      Get.back();
                      Get.back();
                    },
                  );
                } else {
                  Get.back();
                  Get.rawSnackbar(
                      title: 'Error', message: 'Please fill all details');
                }
              },
            ),
          ),
        ],
      ),
    );
  }
}
