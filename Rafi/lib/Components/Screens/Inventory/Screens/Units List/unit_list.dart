import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:intl/intl.dart';
import 'package:rafi_admin/Components/Screens/Inventory/Models/Unit%20List/unit_list_model.dart';
import 'package:rafi_admin/Components/Screens/Inventory/Models/Unit%20Model/unit_model.dart';
import 'package:rafi_admin/Components/Screens/Inventory/Screens/Units%20List/unit_card.dart';
import 'package:rafi_admin/Controllers/units_controller.dart';

import '../../../../loader_widget.dart';


enum UnitStatus { sold, available, all,reserved,repurchased,dealreserved }
class UnitList extends StatefulWidget {
  UnitList({Key? key,}) : super(key: key);

  @override
  State<UnitList> createState() => _UnitListState();
}

class _UnitListState extends State<UnitList> {
  var formatter = NumberFormat('#,##,000');
  UnitStatus? _status = UnitStatus.all;
  String status = '';
  String unitNo = '';
  String marla = '';
  String rate = '';
  String netRate = '';


  @override
  initState() {
    // at the beginning, all users are shown
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        buildFilter(context),
        const SizedBox(
          height: 10,
        ),
        Expanded(
          child:GetBuilder<UnitsController>(
    init: UnitsController(),
    builder: (cont) => cont.unitListModel==null?const LoadingWidget() : cont.results.isNotEmpty
        ? RefreshIndicator(
      onRefresh: (){
        return cont.refreshAll();
      },
          child: ListView.builder(
      itemCount: cont.results.length,
      physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
      itemBuilder: (context, index) {
          final unit = cont.results[index];
          return UnitCard(unit: unit);
      },
    ),
        )
        : const Center(
      child: Text(
        'No results found',
        style: TextStyle(fontSize: 24),
      ),
    ),
    ),
        ),
      ],
    );
  }



  Widget buildFilter(BuildContext context){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: GetBuilder<UnitsController>(
        init: UnitsController(),
        builder: (cont) => Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("Status",
                style: cont.isStatus
                    ? const TextStyle(
                    decoration: TextDecoration.underline,
                    color: Colors.blue,
                    fontWeight: FontWeight.bold,
                    fontSize: 12)
                    : const TextStyle(fontSize: 10)),
            Text("Unit no",
                style: cont.isUnit
                    ? const TextStyle(
                    decoration: TextDecoration.underline,
                    color: Colors.blue,
                    fontWeight: FontWeight.bold,
                    fontSize: 12)
                    : const TextStyle(fontSize: 10)),
            Text("Marla",
                style: cont.isMarla
                    ? const TextStyle(
                    decoration: TextDecoration.underline,
                    color: Colors.blue,
                    fontWeight: FontWeight.bold,
                    fontSize: 12)
                    : const TextStyle(fontSize: 10)),
            Text("Rate/Marla",
                style: cont.isRate
                    ? const TextStyle(
                    decoration: TextDecoration.underline,
                    color: Colors.blue,
                    fontWeight: FontWeight.bold,
                    fontSize: 12)
                    : const TextStyle(fontSize: 10)),
            Text("Net Rate",
                style: cont.isNetRate
                    ? const TextStyle(
                    decoration: TextDecoration.underline,
                    color: Colors.blue,
                    fontWeight: FontWeight.bold,
                    fontSize: 12)
                    : const TextStyle(fontSize: 10)),
            ElevatedButton.icon(
              style: ElevatedButton.styleFrom(
                primary: const Color(0xfff1f6f8),
              ),
              onPressed: () {
                showModalBottomSheet<void>(
                  isScrollControlled: true,
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(20),
                        topLeft: Radius.circular(20)),
                  ),
                  context: context,
                  builder: (BuildContext context) {
                    return StatefulBuilder(
                        builder: (BuildContext context, StateSetter setState /*You can rename this!*/) {
                          return
                            Padding(
                              padding: MediaQuery.of(context).viewInsets,
                              child: Container(
                                height: 315,
                                padding: const EdgeInsets.all(15.0),
                                child: Center(
                                  child: Column(
                                    children: [
                                      const Padding(
                                        padding: EdgeInsets.only(top: 5.0),
                                        child: Text(
                                          'Filter search',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18),
                                        ),
                                      ),
                                      const SizedBox(height: 10,),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Expanded(
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text("Status : ",style: TextStyle(fontWeight: FontWeight.bold),),
                                                Container(
                                                  height: 25,
                                                  child: Row(
                                                    children: [
                                                      Radio<UnitStatus>(
                                                        value: UnitStatus.all,
                                                        groupValue: _status,
                                                        onChanged: (UnitStatus? value) {
                                                            cont.isStatus = false;

                                                          setState(() {
                                                            _status = value;
                                                          });
                                                          setState((){
                                                            status = '';
                                                          });
                                                        },
                                                      ),
                                                      Text("All")
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  height: 25,
                                                  child: Row(
                                                    children: [
                                                      Radio<UnitStatus>(
                                                        value: UnitStatus.sold,
                                                        groupValue: _status,
                                                        onChanged: (UnitStatus? value) {
                                                            cont.isStatus = true;

                                                          setState(() {
                                                            _status = value;
                                                          });
                                                          setState((){
                                                            status = 'sold';
                                                          });
                                                        },
                                                      ),
                                                      const Text("Sold")
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  height: 25,
                                                  child: Row(
                                                    children: [
                                                      Radio<UnitStatus>(
                                                        value: UnitStatus.available,
                                                        groupValue: _status,
                                                        onChanged: (UnitStatus? value) {
                                                            cont.isStatus = true;

                                                          setState(() {
                                                            _status = value;
                                                          });
                                                          setState((){
                                                            status = 'available';
                                                          });
                                                        },
                                                      ),
                                                      Text("Available")
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  height: 25,
                                                  child: Row(
                                                    children: [
                                                      Radio<UnitStatus>(
                                                        value: UnitStatus.reserved,
                                                        groupValue: _status,
                                                        onChanged: (UnitStatus? value) {
                                                            cont.isStatus = false;

                                                          setState(() {
                                                            _status = value;
                                                          });
                                                          setState((){
                                                            status = 'reserved';
                                                          });
                                                        },
                                                      ),
                                                      Text("Reserved")
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  height: 25,
                                                  child: Row(
                                                    children: [
                                                      Radio<UnitStatus>(
                                                        value: UnitStatus.repurchased,
                                                        groupValue: _status,
                                                        onChanged: (UnitStatus? value) {
                                                            cont.isStatus = true;
                                                            setState(() {
                                                            _status = value;
                                                          });
                                                          setState((){
                                                            status = 're-purchased';
                                                          });
                                                        },
                                                      ),
                                                      Text("Re-Purchased")
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  height: 25,
                                                  child: Row(
                                                    children: [
                                                      Radio<UnitStatus>(
                                                        value: UnitStatus.dealreserved,
                                                        groupValue: _status,
                                                        onChanged: (UnitStatus? value) {
                                                          cont.isStatus = true;

                                                          setState(() {
                                                            _status = value;
                                                          });
                                                          setState((){
                                                            status = 'deal';
                                                          });

                                                        },
                                                      ),
                                                      Text("Deal Reserved")
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Column(
                                            mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                            children: [
                                              Container(
                                                width: (MediaQuery.of(context).size.width *
                                                    0.45),
                                                decoration: BoxDecoration(
                                                    color: Colors.white,
                                                    borderRadius: BorderRadius.circular(4),
                                                    boxShadow: const [
                                                      BoxShadow(
                                                          color: Colors.black26,
                                                          blurRadius: 5,
                                                          offset: Offset(0, 0.5))
                                                    ]),
                                                height: 40,
                                                child: TextFormField(
                                                  initialValue: marla,
                                                  onChanged: (val) {
                                                    setState(() {
                                                      marla = val;
                                                    });
                                                  },
                                                  style: const TextStyle(
                                                    color: Colors.black87,
                                                  ),
                                                  decoration: const InputDecoration(
                                                      border: InputBorder.none,
                                                      contentPadding: EdgeInsets.only(
                                                        top: 7,
                                                      ),
                                                      prefixIcon: Icon(
                                                        Icons.architecture,
                                                        color: Color(0xff7F7C82),
                                                      ),
                                                      hintText: "Marla",
                                                      hintStyle: TextStyle(
                                                        color: Colors.black38,
                                                      )),
                                                ),
                                              ),
                                              SizedBox(height: 10,),
                                              Container(
                                                width: (MediaQuery.of(context).size.width *
                                                    0.45),
                                                decoration: BoxDecoration(
                                                    color: Colors.white,
                                                    borderRadius: BorderRadius.circular(4),
                                                    boxShadow: const [
                                                      BoxShadow(
                                                          color: Colors.black26,
                                                          blurRadius: 5,
                                                          offset: Offset(0, 0.5))
                                                    ]),
                                                height: 40,
                                                child: TextFormField(
                                                  initialValue: unitNo,
                                                  keyboardType: TextInputType.number,
                                                  onChanged: (val) {
                                                    setState(() {
                                                      unitNo = val;
                                                    });
                                                  },
                                                  style: const TextStyle(
                                                    color: Colors.black87,
                                                  ),
                                                  decoration: const InputDecoration(
                                                      border: InputBorder.none,
                                                      contentPadding: EdgeInsets.only(
                                                        top: 7,
                                                      ),
                                                      prefixIcon: Icon(
                                                        Icons.house,
                                                        color: Color(0xff7F7C82),
                                                      ),
                                                      hintText: "Unit No",
                                                      hintStyle: TextStyle(
                                                        color: Colors.black38,
                                                      )),
                                                ),
                                              ),
                                              SizedBox(height: 10,),
                                              Container(
                                                width: (MediaQuery.of(context).size.width *
                                                    0.45),
                                                decoration: BoxDecoration(
                                                    color: Colors.white,
                                                    borderRadius: BorderRadius.circular(4),
                                                    boxShadow: const [
                                                      BoxShadow(
                                                          color: Colors.black26,
                                                          blurRadius: 5,
                                                          offset: Offset(0, 0.5))
                                                    ]),
                                                height: 40,
                                                child: TextFormField(
                                                  initialValue: rate,
                                                  onChanged: (val) {
                                                    setState(() {
                                                      rate = val;
                                                    });
                                                  },
                                                  style: const TextStyle(
                                                    color: Colors.black87,
                                                  ),
                                                  decoration: const InputDecoration(
                                                      border: InputBorder.none,
                                                      contentPadding: EdgeInsets.only(
                                                        top: 7,
                                                      ),
                                                      prefixIcon: Icon(
                                                        Icons.attach_money,
                                                        color: Color(0xff7F7C82),
                                                      ),
                                                      hintText: "Rate/Marla",
                                                      hintStyle: TextStyle(
                                                        color: Colors.black38,
                                                      )),
                                                ),
                                              ),
                                              SizedBox(height: 10,),
                                              Container(
                                                width: (MediaQuery.of(context).size.width *
                                                    0.45),
                                                decoration: BoxDecoration(
                                                    color: Colors.white,
                                                    borderRadius: BorderRadius.circular(4),
                                                    boxShadow: const [
                                                      BoxShadow(
                                                          color: Colors.black26,
                                                          blurRadius: 5,
                                                          offset: Offset(0, 0.5))
                                                    ]),
                                                height: 40,
                                                child: TextFormField(
                                                  initialValue: netRate,
                                                  keyboardType: TextInputType.number,
                                                  onChanged: (val) {
                                                    setState(() {
                                                      netRate = val;
                                                    });
                                                  },
                                                  style: const TextStyle(
                                                    color: Colors.black87,
                                                  ),
                                                  decoration: const InputDecoration(
                                                      border: InputBorder.none,
                                                      contentPadding: EdgeInsets.only(
                                                        top: 7,
                                                      ),
                                                      prefixIcon: Icon(
                                                        Icons.attach_money,
                                                        color: Color(0xff7F7C82),
                                                      ),
                                                      hintText: "Net Rate",
                                                      hintStyle: TextStyle(
                                                        color: Colors.black38,
                                                      )),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                      SizedBox( height:10),
                                      ElevatedButton(
                                        child: const Text('Save'),
                                        onPressed: () async {
                                          cont.results = cont.unitListModel!.units;
                                          await cont.runStatusFilter(status);
                                          await cont.runUnitFilter(unitNo);
                                          await cont.runMarlaFilter(marla);
                                          await cont.runRateFilter(rate);
                                          await cont.runNetRateFilter(netRate);
                                          Navigator.pop(context);
                                        },
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            );
                        });
                  },
                );
              },
              icon: const Icon(
                Icons.filter_list_alt,
                color: Colors.black,
                size: 24.0,
              ),
              label: const Text(
                'Filters',
                style: TextStyle(color: Colors.black),
              ),
            ),
          ],
        ),
      ),
    );
}
}
