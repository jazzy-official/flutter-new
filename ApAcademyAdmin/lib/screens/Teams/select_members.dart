//import 'package:apsportacademy/screens/Events/add_team.dart';
import 'package:ap_academy_admin/screens/Teams/groupScreen.dart';
import 'package:ionicons/ionicons.dart';
import 'package:ap_academy_admin/ModelView/Controller/GroupController.dart';
import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:ap_academy_admin/ModelView/Controller/teamsController.dart';
import 'package:ap_academy_admin/screens/Teams/add_team.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class SelectMembers extends StatefulWidget {
  final List userModel;

  const SelectMembers({Key? key, required this.userModel,}) : super(key: key);
  @override
  _SelectMembersState createState() => _SelectMembersState();
}

class _SelectMembersState extends State<SelectMembers> {
  TextEditingController searchTextController = TextEditingController();
  final teamC = Get.put(TeamController());

  List userList = [];
  List selectedUserList = [];
  final databaseController = Get.put(DatabaseController());
  RxString selectedValue = ''.obs;
  @override
  void initState() {
    userList = widget.userModel; // teamC.userList;
    selectedMember = List<bool>.generate(userList.length, (index) => false);

    super.initState();
  }

  void _runFilter(String enteredKeyword) {
    List results = [];
    if (enteredKeyword.isEmpty) {
      // if the search field is empty or only contains white-space, we'll display all users
      results = widget.userModel;
    } else {
      if(_value==0){results = widget.userModel
          .where((e) => e['name']
          .toUpperCase()
          .contains(enteredKeyword.toUpperCase()))
          .toList();
      }else if(_value==1){
        results = widget.userModel
            .where((e) => calculateAge(e['dob']).toString()
            .toUpperCase()
            .contains(enteredKeyword.toUpperCase()))
            .toList();
      }
      // we use the toLowerCase() method to make it case-insensitive
      }
    // Refresh the UI
    setState(() {
      userList = results;
    });
    print(userList);
  }
  List<bool> selectedMember = [];
//////////////////////////////---ye
  getSelMember() {
    selectedUserList.isNotEmpty ? selectedUserList.clear() : null;
    int count = 0;
    selectedMember.forEach((element) {
      if (element == true) {
        selectedUserList.add(userList[count]);
        print(selectedUserList.length);
      }

      ++count;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }
  int? _value = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          " Select Members",
          style: MyResources.appHeadingStyle,
        ),
        centerTitle: true,
        actions: [
          Container(
            margin:EdgeInsets.all(12),
            decoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.circular(5)
            ),
            
            child: TextButton(
              // textColor: Colors.white,
              // color: MyResources.buttonColor,
              child: Text(
                "ADD",
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                getSelMember();
                Get.back(result: selectedUserList);

                //  selectedUserList.clear();
              },
              // shape: new RoundedRectangleBorder(
              //   borderRadius: new BorderRadius.circular(10.0),
              // ),
            ),
          ),
        ],
        backgroundColor: MyResources.buttonColor,
      ),
      body: Column(
        children: [
      Row(
        children: [
          Container(
            width: Get.width -125,
            height: 60,
            padding: EdgeInsets.all(10),
            child: TextField(
              onChanged: (val) {
                _runFilter(val);
              },
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(horizontal: 10,vertical: 0),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
              hintText: 'Search',
            ),
    ),
          ),
          Wrap(
              children: [
                ChoiceChip(
                  elevation: 4,
                  pressElevation: 2,
                  shadowColor: Colors.black,
                  label: Text('Name'),
                  selected: _value == 0,
                  onSelected: (bool selected) {
                    setState(() {
                      _value = selected ? 0 : 0;
                      print(_value.toString());
                    });
                  },
                ),
                SizedBox(width: 3,),
                ChoiceChip(
                  elevation: 4,
                  pressElevation: 2,
                  shadowColor: Colors.black,
                  label: Text('Age'),
                  selected: _value == 1,
                  onSelected: (bool selected) {
                    setState(() {
                      _value = selected ? 1 : 1;
                      print(_value.toString());
                    });
                  },
                )
              ]
          ),
        ],
      ),
          Expanded(
            child: SizedBox(
              width: Get.width,
              child: SingleChildScrollView(
                child: DataTable(
                  columnSpacing: 120,
                  columns: const <DataColumn>[
                    DataColumn(
                      label: Text(
                        'Name',
                        style: TextStyle(fontStyle: FontStyle.normal),
                      ),
                    ),
                    DataColumn(
                      label: Text(
                        'Age',
                        style: TextStyle(fontStyle: FontStyle.normal),
                      ),
                    ),
                  ],
                  rows: List<DataRow>.generate(userList.length,
                      (index) {
                    return DataRow(
                      color: MaterialStateProperty.resolveWith<Color>(
                          (Set<MaterialState> states) {
                        // All rows will have the same selected color.
                        if (states.contains(MaterialState.selected))
                          return Theme.of(context)
                              .colorScheme
                              .primary
                              .withOpacity(0.08);

                        if (index % 2 == 0)
                          return Colors.grey.withOpacity(0.3);
                        return Colors.transparent; // Use default value for other states and odd rows.
                      }),
                      cells: [
                        DataCell(Text(userList[index]['name'])),
                        DataCell(userList[index]['dob'] != null
                            ? Text(calculateAge(userList[index]['dob']).toString())
                            : Text('N/A')),
                      ],
                      selected: selectedMember[index],
                      onSelectChanged: (bool? value) {
                        selectedMember[index] = value!;
                        setState(() {
                          print(value.toString());
                        });
                      },
                    );
                  }),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
