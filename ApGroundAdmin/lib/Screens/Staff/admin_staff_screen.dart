import 'package:ap_ground_admin/Screens/Authentication/Models/user_model.dart';
import 'package:ap_ground_admin/Screens/Authentication/signup_screen.dart';
import 'package:ap_ground_admin/Screens/Staff/admin_card.dart';
import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:ap_ground_admin/controllers/user_controller.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';

import '../delete_popup.dart';

class AdminStaffScreen extends StatefulWidget {
  AdminStaffScreen({Key? key}) : super(key: key);

  @override
  State<AdminStaffScreen> createState() => _AdminStaffScreenState();
}

class _AdminStaffScreenState extends State<AdminStaffScreen> {
  List<UserModel> admins=[];

  void refresh()async{
    List<UserModel> temp=[];
    temp = await UserController().readAdmins();
    setState(() {
      admins = temp;
    });
  }
  @override
  void initState() {
    refresh();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 12.0),
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SignupScreen()),
                  ).then((value) => refresh());
                },
                child: const Text("Add"),
                style: ElevatedButton.styleFrom(primary: Color(0xff1758B4)),
              ),
            ),
          ],),

        Expanded(
          child: GridView.builder(
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: 4 / 5,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10),
              physics: const BouncingScrollPhysics(
                  parent: AlwaysScrollableScrollPhysics()),
              padding: const EdgeInsets.all(8),
              itemCount: admins.length,
              itemBuilder: (BuildContext context, int index) {
                UserModel admin = admins[index];
                return AdminCard(admin: admin, refresh: refresh);
              }),
        ),
      ],
    );
  }
}