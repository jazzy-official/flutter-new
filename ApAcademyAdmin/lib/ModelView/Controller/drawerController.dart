import 'package:ap_academy_admin/ModelView/Controller/auth_controller.dart';
import 'package:get/get.dart';

class DrawerController extends GetxController {
  final authC = Get.put(AuthController());
  @override
  Future<void> onInit() async {
    super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
