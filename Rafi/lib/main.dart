import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:rafi_admin/Wrapper/wrapper.dart';

import 'Components/loader_widget.dart';
import 'Controllers/authcontroller.dart';

void main() {
  runApp(MyApp());
}
int projectid = 1;
class MyApp extends StatelessWidget {
   MyApp({Key? key}) : super(key: key);
  final authController = Get.put(AuthController());
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      home:  FutureBuilder(
        future: authController.checkUserLoggedIn(),
        builder: (context, dynamic snapshot) {
          if (snapshot.hasData) {
            return snapshot.data;
          }
          return const Center(child: LoadingWidget());
        },
      ),
    );
  }
}

