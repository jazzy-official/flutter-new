
import 'package:flutter/material.dart';

import 'Models/booking_model.dart';
import 'booking_card.dart';

class UpcomingBookingScreen extends StatefulWidget {
  final List<Booking> upcomingBookings;
  const UpcomingBookingScreen({Key? key, required this.upcomingBookings}) : super(key: key);

  @override
  _UpcomingBookingScreenState createState() => _UpcomingBookingScreenState();
}

class _UpcomingBookingScreenState extends State<UpcomingBookingScreen> {

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xfffefefe),
      child: Column(
        children: [
          widget.upcomingBookings.isEmpty ? Expanded(child: Center(child: Text("No Result found"),)):
          Expanded(
            child: ListView.builder(
                padding: const EdgeInsets.all(8),
                itemCount: widget.upcomingBookings.length,
                itemBuilder: (BuildContext context, int index) {
                  Booking booking = widget.upcomingBookings[index];
                  return BookingCard(booking: booking,);


                }),
          ),
        ],
      ),
    );
  }
}
