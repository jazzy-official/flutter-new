import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Screens/Home/Widgets/room_list.dart';
import 'package:room_buddy/Screens/Home/create_listing_page.dart';
import 'package:room_buddy/Screens/Home/room_feed.dart';
import 'package:room_buddy/Screens/Messaging/message_list.dart';

class MessageScreen extends StatelessWidget {
  const MessageScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isdata = true;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Messages",style: TextStyle(color: Colors.black),),
        backgroundColor: Colors.white,
      ),
      body: isdata? Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: 16,left: 16,right: 16,bottom: 16),
            child: TextField(
              decoration: InputDecoration(
                hintText: "Search...",
                hintStyle: TextStyle(color: Colors.grey.shade600),
                prefixIcon: Icon(Icons.search,color: Colors.grey.shade600, size: 20,),
                filled: true,
                fillColor: Colors.grey.shade100,
                contentPadding: EdgeInsets.only(left: 10,top: 13),
                focusedBorder: UnderlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                  borderSide: BorderSide(color: Colors.transparent),
                ),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                    borderSide: BorderSide(
                        color: Colors.grey.shade100
                    )
                ),
              ),
            ),
          ),
          Expanded(child: MessageList()),
        ],
      ) : Center(
        child: SizedBox(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children:  [
              Image.asset("assets/nomessage.png"),
              const Text("No conversations",style: TextStyle(fontSize: 18,fontWeight: FontWeight.w600),),
              const SizedBox(height: 10,),
              SizedBox(
                width: 200,
                child: const Text("""     All conversations with potential
      customers will be shown here""",style: TextStyle(fontSize: 12,color: Colors.black45),),
              ),
              const SizedBox(height: 20,),
              TextButton(
                  onPressed: () {
                    // Get.to(ListingPage());
                  },
                  child: Container(
                    padding: EdgeInsets.all(12),
                    decoration: BoxDecoration(
                        color: myResources.themeColor,
                        borderRadius: BorderRadius.circular(3)

                    ),
                    child: Text("EXPLORE ROOM LISTINGS",style: TextStyle(color: Colors.white),),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
