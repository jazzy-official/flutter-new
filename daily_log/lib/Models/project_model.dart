import 'package:daily_log/Models/module.dart';

class ProjectModel {
  final String? id;
  final String name;
  final String des;
  final bool status;
  final String? deadline;
  final String? assignto;
  final List<Module> modules;
  final DateTime? completedDate;

  ProjectModel(
      {this.id,
      this.assignto,
      this.completedDate, this.deadline,
      required this.status,
      required this.name,
      required this.des,
      required this.modules});

  factory ProjectModel.fromJson(dynamic json, key) {
    return ProjectModel(
      id: key.toString(),
      name: json['name'],
      des: json['des'],
      status: json['status'],
      deadline:json["deadline"] != "null"? json['deadline']:null,
      assignto: json['assignto'],
      completedDate: json['completedDate'] == "null"
          ? null
          : DateTime.parse(json["completedDate"]),
      modules: json['modules'] == null ? [] : Module.fromModuleMap(json['modules']),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['des'] = des;
    data['status'] = status;
    data['deadline'] = deadline.toString();
    data['assignto'] = assignto;
    data['completedDate'] = completedDate.toString();
    data['modules'] = Module.toModuleMap(modules);
    return data;
  }
}
