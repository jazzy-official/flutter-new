import 'package:ap_academy_admin/ModelView/Controller/AlbumController.dart';
import 'package:ap_academy_admin/screens/Gallery/addphotos_videos_inalbum.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:get/get.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:photo_view/photo_view.dart';

class PhotosTab extends StatefulWidget {
  PhotosTab({Key? key, this.title, this.i}) : super(key: key);
  final String? title;
  final int? i;
  @override
  _PhotosTabState createState() => _PhotosTabState();
}

class _PhotosTabState extends State<PhotosTab> {
  final galeyController = Get.put(PhotoAlbumController());

  int page = 1;
  bool isLoading = false;
  //List<String> items = ['item 1', 'item 2', ];
  Future _loadData() async {
    // perform fetching data delay
    await new Future.delayed(new Duration(seconds: 2));
    print("load more");
    // update data and loading status
    setState(() {
      /*items.addAll( ['item 1']);
      print('items: '+ items.toString());*/
      isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: Get.height,
          child: Column(
            children: [
              // Padding(
              //   padding: const EdgeInsets.only(
              //       top: 5.0, bottom: 20, right: 10, left: 10),
              //   child: Row(
              //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //     crossAxisAlignment: CrossAxisAlignment.start,
              //     children: [
              //       Text(
              //         "",
              //         style: GoogleFonts.roboto(
              //           fontSize: 25,
              //           fontWeight: FontWeight.bold,
              //         ),
              //       ),
              //       // Padding(
              //       //   padding: const EdgeInsets.all(8.0),
              //       //   child: Container(
              //       //     width: Get.width * 0.4,
              //       //     height: 35,
              //       //     child: RaisedButton(
              //       //       textColor: Colors.white,
              //       //       color: Colors.blue,
              //       //       child: Text(
              //       //         "Add New Photo",
              //       //       ),
              //       //       onPressed: () {
              //       //         Get.dialog(
              //       //           AddPhoto(
              //       //             isPhoto: true,
              //       //           ),
              //       //         );
              //       //       },
              //       //       shape: new RoundedRectangleBorder(
              //       //         borderRadius: new BorderRadius.circular(10.0),
              //       //       ),
              //       //     ),
              //       //   ),
              //       // ),
              //     ],
              //   ),
              // ),
              Container(
                constraints: new BoxConstraints(
                  minHeight: 80,
                ),
                child: Card(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Obx(
                              () => Container(
                                child: Text(
                                  galeyController.album[widget.i!]['albumName']
                                      .toString().toUpperCase(),
                                  style: TextStyle(
                                      color: MyResources.buttonColor, fontSize: 16,fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.fade,
                                ),
                                constraints: new BoxConstraints(
                                  maxHeight: 500,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.only(left: 10),
                            child: Text(
                              "Discription:",
                            ),
                          ),
                          Obx(
                            () => Container(
                              padding: EdgeInsets.only(left: 20),
                              child: Text(
                                galeyController.album[widget.i!]['desciption']
                                    .toString(),
                                textAlign: TextAlign.left,
                                style: TextStyle(color: Colors.blue),
                                overflow: TextOverflow.visible,
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: NotificationListener<ScrollNotification>(
                  onNotification: (ScrollNotification scrollInfo) {
                    if (!isLoading &&
                        scrollInfo.metrics.pixels ==
                            scrollInfo.metrics.maxScrollExtent) {
                      _loadData();
                      // start loading data
                      setState(() {
                        isLoading = true;
                      });
                    }
                    return isLoading;
                  },
                  child: Container(
                    height: Get.height * 0.7,
                    child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Obx(
                          () => GridView.count(
                            shrinkWrap: true,
                            // Create a grid with 2 columns. If you change the scrollDirection to
                            // horizontal, this produces 2 rows.
                            crossAxisCount: 3,
                            // Generate 100 widgets that display their index in the List.
                            children: List.generate(
                                galeyController.album[widget.i!]['albumPhotos']
                                    .length, (index) {
                              return InkWell(
                                onTap: () =>
                                {
                                  Get.to(PhotoViewGallery.builder(
                                    scrollPhysics: const BouncingScrollPhysics(),
                                    builder: (BuildContext context,  int i) {

                                      return PhotoViewGalleryPageOptions(
                                        imageProvider: NetworkImage(galeyController.album[widget.i!]
                                        ['albumPhotos'][i].toString()),
                                        initialScale: PhotoViewComputedScale.contained,

                                        // heroAttributes: PhotoViewHeroAttributes(tag: approvel['documents'][index]),
                                      );
                                    },
                                    itemCount: galeyController.album[widget.i!]
                                    ['albumPhotos'].length,
                                    loadingBuilder: (context, event) => Center(
                                      child: Container(
                                        width: 20.0,
                                        height: 20.0,
                                        child: CircularProgressIndicator(
                                          value: event == null
                                              ? 0
                                              : event.cumulativeBytesLoaded / event.expectedTotalBytes!.toInt(),
                                        ),
                                      ),
                                    ),
                                    // backgroundDecoration: widget.backgroundDecoration,
                                    // pageController: widget.pageController,
                                    // onPageChanged: onPageChanged,
                                  ))
                                },
                                child: Card(
                                  margin: EdgeInsets.all(10),
                                  color: Colors.white60,
                                  child: Container(
                                      width: Get.width,
                                      height: Get.height,
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                            colorFilter: new ColorFilter.mode(
                                                Colors.black.withOpacity(0.5),
                                                BlendMode.dstATop),
                                            fit: BoxFit.cover,
                                            image: NetworkImage(
                                              galeyController.album[widget.i!]
                                                      ['albumPhotos'][index]
                                                  .toString(),
                                            )),
                                      ),
                                      child: Container(
                                          //child:
                                          //Text('Pic  ${index + 1}',
                                          //  style: MyResources.appTextStyle),
                                          )),
                                ),
                              );
                            }),
                          ),
                        )),
                  ),
                ),
              ),
              Container(
                height: isLoading ? 50.0 : 0,
                color: Colors.transparent,
                child: Center(
                  child: new LoadingWidget(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class PhotViewer extends StatefulWidget {
  final imageString;

  const PhotViewer({Key? key, this.imageString}) : super(key: key);
  @override
  _PhotViewerState createState() => _PhotViewerState();
}

class _PhotViewerState extends State<PhotViewer> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          child: PhotoView(
        imageProvider: NetworkImage(widget.imageString),
      )),
    );
  }
}
