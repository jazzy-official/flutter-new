import 'package:ap_ground_admin/Screens/Staff/admin_staff_screen.dart';
import 'package:ap_ground_admin/Screens/Staff/ground_staff.dart';
import 'package:ap_ground_admin/constants/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StaffScreen extends StatelessWidget {
  const StaffScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          toolbarHeight: 0,
          bottom:  PreferredSize(
            preferredSize: Size(double.infinity,40),
            child: ColoredBox(
              color: Colors.white,
              child: SizedBox(
                height: 40,
                child: TabBar(
                  indicator: BoxDecoration(
                      borderRadius:
                      BorderRadius.circular(0),
                      color: MyResources.buttonColor),
                  labelColor: Colors.white,
                  unselectedLabelColor: Colors.black,
                  tabs: const [
                    Tab(child: Text("Staff"),),
                    Tab(child: Text("Admins"),),
                  ],
                ),
              ),
            ),
          ),
        ),
        body: TabBarView(
          children: [
            GroundStaffScreen(),
            AdminStaffScreen(),
          ],
        ),
      ),
    );
  }
}

