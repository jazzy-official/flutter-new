


import 'package:get/get_state_manager/src/simple/get_controllers.dart';

import 'model/account_model.dart';

class OrderController extends GetxController {
    Order _orders = Order();

  Order get orders {
    return _orders;
  }

  void addOrder(Order? order, String id,Order mainorder) {
    if(mainorder.order!=null){
      addOrder(order, id,mainorder.order!);
    }else{
      mainorder.order = order;
    }
    update();
  }
}