import 'dart:ffi';

import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Screens/Listings/Create%20Listing%20Forms/room_amentities_form.dart';
import 'package:room_buddy/controllers/my_listing_controller.dart';

import '../../../Constants/Widgets/info_widget.dart';

class RoomOverviewForm extends StatelessWidget {
   RoomOverviewForm({Key? key}) : super(key: key);
   TextEditingController roomsizecontroller = TextEditingController();
   TextEditingController propertysizecontroller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          iconTheme: const IconThemeData(color: Colors.black),
          title: Text("Step 2: Room overview",style: TextStyle(color: Colors.black),),
          bottom: PreferredSize(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 22),
              child: LinearProgressIndicator(
                color: myResources.themeColor,
                backgroundColor: Colors.grey.shade300,
                minHeight: 6,
                value: 2 / 6,
              ),
            ),
            preferredSize: const Size.fromHeight(50),
          )),
      body:GetBuilder<MyListingController>(
        init: MyListingController(),
        builder: (cont) => Padding(
          padding: const EdgeInsets.all(15.0),
          child: ListView(
            // crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text("Number of rooms",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      BouncingWidget(
                        scaleFactor: 4.0,
                        onPressed: ()  {
                          if(cont.roomNo<=1){

                          }else{
                            cont.roomNo--;
                            cont.update();
                          }

                        },
                        child: Container(
                          decoration: BoxDecoration(
                            // color: myResources.themeColor,
                              shape: BoxShape.circle,
                              border: Border.all(color: Colors.grey,width: 1)
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(3.0),
                            child: Icon(Icons.remove,color: myResources.themeColor,),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        child: Text(cont.roomNo.toString(),style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16,color: Colors.green),),
                      ),
                      BouncingWidget(
                        scaleFactor: 4.0,
                        onPressed: () async {
                          cont.roomNo++;
                          cont.update();
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            // color: myResources.themeColor,
                              shape: BoxShape.circle,
                              border: Border.all(color: Colors.grey,width: 1)
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(3.0),
                            child: Icon(Ionicons.add,color: myResources.themeColor,),
                          ),
                        ),
                      ),
                      SizedBox(width: 15,),
                    ],
                  ),
                ],
              ),
              const SizedBox(height: 10,),
              const Text(
                "Room type",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            const BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.roomType == "Private"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.roomType ="Private";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 12),
                        child: Center(
                            child: Text(
                              'Private',
                              style: TextStyle(
                                  color: cont.roomType == "Private"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.roomType == "Sharing"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.roomType = "Sharing";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 12),
                        child: Center(
                            child: Text(
                              'Sharing',
                              style: TextStyle(
                                  color: cont.roomType == "Sharing"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.roomType == "Any"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.roomType = "Any";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 12),
                        child: Center(
                            child: Text(
                              'Any',
                              style: TextStyle(
                                  color: cont.roomType == "Any"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                ],
              ),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text("Number of bathrooms",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      BouncingWidget(
                        scaleFactor: 4.0,
                        onPressed: ()  {
                          if(cont.bathroomNo<=1){}
                          else{
                            cont.bathroomNo--;
                            cont.update();
                          }
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            // color: myResources.themeColor,
                              shape: BoxShape.circle,
                              border: Border.all(color: Colors.grey,width: 1)
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(3.0),
                            child: Icon(Icons.remove,color: myResources.themeColor,),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        child: Text(cont.bathroomNo.toString(),style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16,color: Colors.green),),
                      ),
                      BouncingWidget(
                        scaleFactor: 4.0,
                        onPressed: ()  {

                            cont.bathroomNo++;
                            cont.update();

                        },
                        child: Container(
                          decoration: BoxDecoration(
                            // color: myResources.themeColor,
                              shape: BoxShape.circle,
                              border: Border.all(color: Colors.grey,width: 1)
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(3.0),
                            child: Icon(Ionicons.add,color: myResources.themeColor,),
                          ),
                        ),
                      ),
                      SizedBox(width: 15,),
                    ],
                  ),
                ],
              ),
              const SizedBox(height: 10,),
              const Text(
                "Bathroom type",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            const BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.bathroomType == "Attached"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.bathroomType = "Attached";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 12),
                        child: Center(
                            child: Text(
                              'Attached',
                              style: TextStyle(
                                  color: cont.bathroomType == "Attached"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.bathroomType == "Non-attached"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.bathroomType = "Non-attached";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 12),
                        child: Center(
                            child: Text(
                              'Non-attached',
                              style: TextStyle(
                                  color: cont.bathroomType == "Non-attached"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.bathroomType == "Any"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.bathroomType = "Any";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 12),
                        child: Center(
                            child: Text(
                              'Any',
                              style: TextStyle(
                                  color: cont.bathroomType == "Any"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                ],
              ),
              Divider(),
              const Text(
                "Furnishing type",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            const BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.furnishedType == "Full"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.furnishedType = "Full";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 12),
                        child: Center(
                            child: Text(
                              'Full',
                              style: TextStyle(
                                  color: cont.furnishedType == "Full"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.furnishedType == "Semi"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {

                        cont.furnishedType = "Semi";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 12),
                        child: Center(
                            child: Text(
                              'Semi',
                              style: TextStyle(
                                  color: cont.furnishedType == "Semi"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.furnishedType == "Any"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.furnishedType = "Any";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 12),
                        child: Center(
                            child: Text(
                              'Any',
                              style: TextStyle(
                                  color: cont.furnishedType == "Any"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                ],
              ),

              Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          "Room Size",
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                        ),
                        Container(
                          width: Get.width*0.33,
                          height: 80,
                          //Add padding around textfield
                          padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                          child: TextField(
                            controller: roomsizecontroller,
                            keyboardType: TextInputType.number,
                            onChanged: (val) {},
                            decoration: InputDecoration(
                              //Add th Hint text here.
                              hintText: "Sq ft",
                              hintStyle: TextStyle(fontSize: 13),
                              contentPadding: const EdgeInsets.only(top: 15, left: 15,),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(3.0),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          "Property Size",
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                        ),
                        Container(
                          width: Get.width*0.33,
                          height: 80,
                          //Add padding around textfield
                          padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                          child: TextField(
                            controller: propertysizecontroller,
                            keyboardType: TextInputType.number,
                            onChanged: (val) {},
                            decoration: InputDecoration(
                              //Add th Hint text here.
                              hintText: "Sq ft",
                              hintStyle: TextStyle(fontSize: 13),
                              contentPadding: const EdgeInsets.only(top: 15, left: 15,),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(3.0),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: 30,),

              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: myResources.themeColor,
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(14))),
                ),
                onPressed: () {
                  if(roomsizecontroller.text==""){
                    Get.snackbar("Warning", "Enter room size",snackPosition: SnackPosition.BOTTOM);
                  } else if(propertysizecontroller.text==""){
                    Get.snackbar("Warning", "Enter property size",snackPosition: SnackPosition.BOTTOM);
                  } else{
                    cont.roomSize = double.tryParse(roomsizecontroller.text);
                    cont.propertySize = double.tryParse(propertysizecontroller.text);
                    Get.to(RoomAmenities());
                  }

                },
                child: Container(
                  padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SizedBox(width: 20,),
                      Text(
                        'Proceed',
                        style: TextStyle(color: Colors.white),
                      ),
                      Container(
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                          borderRadius:
                          const BorderRadius.all(Radius.circular(20)),
                          color: myResources.themeColor,
                        ),
                        child: Icon(
                          Icons.arrow_forward_ios,
                          color: Colors.white,
                          size: 16,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
