
class Attendance {
  final String? id;
  final String? comment;
  final String status;
  final DateTime date;
  final Duration? latearival;
  final Duration? earlyleave;
  final Duration? longbreak;
  final Duration? overtime;

  Attendance(
      {this.id,
        required this.status,
        required this.date,
        this.comment,
        this.earlyleave,
        this.latearival,
        this.longbreak,
        this.overtime,
      });

  factory Attendance.fromJson(dynamic json, key) {
    return Attendance(
      id: key.toString(),
      comment: json['comment'],
      date: json['date'],
      status: json['status'],
      earlyleave: json['earlyleave'],
      latearival: json['latearival'],
      longbreak: json['longbreak'],
      overtime: json['overtime'],

    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['comment'] = comment;
    data['status'] = status;
    data['date'] = date.toString();
    data['earlyleave'] = earlyleave;
    data['latearival'] = latearival;
    data['longbreak'] = longbreak;
    data['overtime'] = overtime;
    return data;
  }
}
