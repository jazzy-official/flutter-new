import 'package:ap_ground_admin/Screens/Ball%20Management/Models/ball_model.dart';
import 'package:ap_ground_admin/constants/util.dart';
import 'package:ap_ground_admin/controllers/ball_management_controller.dart';
import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class BallScreen extends StatefulWidget {
  const BallScreen({Key? key}) : super(key: key);

  @override
  _BallScreenState createState() => _BallScreenState();
}

class _BallScreenState extends State<BallScreen> {
  Ball ball = Ball(newball: 0, oldball: 0, lostball: 0);
  

  int tempnew = 0;
  int tempold = 0;
  int templost = 0;
  int i =0;
  List<Color> colors = [
    Color.fromRGBO(255,189,57,1),
  Color.fromRGBO(228,0,124,1),
  Color.fromRGBO(9,0,136,1),
  ];
  List<ChartData> chartData = [

  ];
  late TooltipBehavior _tooltip;
  void refresh () async {
    setState(() {
      chartData=[];
    });
    List<Ball> temp = await BallController().readBall();
    chartData.add(ChartData("New balls", temp.first.newball.toDouble(), Color.fromRGBO(9,0,136,1)));
    chartData.add(ChartData("Old balls", (temp.first.oldball).toDouble(), Color.fromRGBO(255,189,57,1)));
    chartData.add(ChartData("Lost balls", temp.first.lostball.toDouble(), Color.fromRGBO(228,0,124,1)));

    setState(() {
      ball = temp.first;
    });
    
  }
  
  @override
  void initState() {
    _tooltip = TooltipBehavior(enable: true);
    refresh();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding:
          const EdgeInsets.symmetric(vertical: 2.0, horizontal: 8),
          child: Card(
              child: Container(
                  height: 150,
                  child: SfCircularChart(
                      tooltipBehavior: _tooltip,
                      legend: Legend(isVisible: true,position: LegendPosition.right),
                      series: <CircularSeries>[
                        // Renders doughnut chart
                        DoughnutSeries<ChartData, String>(
                            dataSource: chartData,
                            pointColorMapper:(ChartData data,  _) => data.color,
                            xValueMapper: (ChartData data, _) => data.x,
                            yValueMapper: (ChartData data, _) => data.y
                        )
                      ]
                  )
              )),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Expanded(
            child: Column(
              children: [
                Card(
                  color: Colors.blue.shade50,
                  child: Container(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Row(
                          children: [
                            Text("New balls",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Quantity : "+ball.newball.toString(),style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                BouncingWidget(
                                  scaleFactor: 4.0,
                                  onPressed: () async {
                                    setState(() {
                                      tempnew++;
                                    });


                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: MyResources.buttonColor,
                                      shape: BoxShape.circle,
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(3.0),
                                      child: Icon(Icons.add,color: Colors.white,),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                                  child: Text(tempnew.toString(),style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16,color: Colors.green),),
                                ),
                                BouncingWidget(
                                  scaleFactor: 4.0,
                                  onPressed: () async {
                                    setState(() {
                                      tempnew--;
                                    });
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: MyResources.buttonColor,
                                      shape: BoxShape.circle,
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(3.0),
                                      child: Icon(Ionicons.remove,color: Colors.white,),
                                    ),
                                  ),
                                ),
                                SizedBox(width: 15,),
                                BouncingWidget(
                                  scaleFactor: 4.0,
                                  onPressed: () async {
                                    await BallController().updateBall(Ball(newball: ball.newball+tempnew, oldball: ball.oldball,id: ball.id, lostball: ball.lostball));
                                    refresh();
                                    displayToast(tempnew.toString()+" balls added");
                                    setState(() {
                                      tempnew=0;
                                    });

                                    },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: MyResources.buttonColor,
                                      shape: BoxShape.circle,
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(7.0),
                                      child: Text("Ok",style: TextStyle(color: Colors.white),),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Card(
                  color: Colors.yellow.shade50,
                  child: Container(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Row(
                          children: [
                            Text("Old balls",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Quantity : "+ball.oldball.toString(),style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                BouncingWidget(
                                  scaleFactor: 4.0,
                                  onPressed: () async {
                                    setState(() {
                                      tempold++;
                                    });


                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: MyResources.buttonColor,
                                      shape: BoxShape.circle,
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(3.0),
                                      child: Icon(Icons.add,color: Colors.white,),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                                  child: Text(tempold.toString(),style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16,color: Colors.green),),
                                ),
                                BouncingWidget(
                                  scaleFactor: 4.0,
                                  onPressed: () async {
                                    setState(() {
                                      tempold--;
                                    });
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: MyResources.buttonColor,
                                      shape: BoxShape.circle,
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(3.0),
                                      child: Icon(Ionicons.remove,color: Colors.white,),
                                    ),
                                  ),
                                ),
                                SizedBox(width: 15,),
                                BouncingWidget(
                                  scaleFactor: 4.0,
                                  onPressed: () async {
                                    await BallController().updateBall(Ball(id:ball.id,newball: ball.newball, oldball: ball.oldball+tempold, lostball: ball.lostball));
                                    refresh();
                                    displayToast(tempold.toString()+" balls added");
                                    setState(() {
                                      tempold=0;
                                    });
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: MyResources.buttonColor,
                                      shape: BoxShape.circle,
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(7.0),
                                      child: Text("Ok",style: TextStyle(color: Colors.white),),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Card(
                  color: Colors.red.shade50,
                  child: Container(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Row(
                          children: [
                            Text("Lost balls",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Quantity : "+ball.lostball.toString(),style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                BouncingWidget(
                                  scaleFactor: 4.0,
                                  onPressed: () async {
                                    setState(() {
                                      templost++;
                                    });


                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: MyResources.buttonColor,
                                      shape: BoxShape.circle,
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(3.0),
                                      child: Icon(Icons.add,color: Colors.white,),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                                  child: Text(templost.toString(),style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16,color: Colors.green),),
                                ),
                                BouncingWidget(
                                  scaleFactor: 4.0,
                                  onPressed: () async {
                                    setState(() {
                                      templost--;
                                    });
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: MyResources.buttonColor,
                                      shape: BoxShape.circle,
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(3.0),
                                      child: Icon(Ionicons.remove,color: Colors.white,),
                                    ),
                                  ),
                                ),
                                SizedBox(width: 15,),
                                BouncingWidget(
                                  scaleFactor: 4.0,
                                  onPressed: () async {
                                    await BallController().updateBall(Ball(id:ball.id,newball: ball.newball, oldball: ball.oldball, lostball: ball.lostball+templost));
                                    refresh();
                                    displayToast(templost.toString()+" balls added");
                                    setState(() {
                                      templost=0;
                                    });
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: MyResources.buttonColor,
                                      shape: BoxShape.circle,
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(7.0),
                                      child: Text("Ok",style: TextStyle(color: Colors.white),),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}


class ChartData {
  ChartData(this.x, this.y, this.color);
  final String x;
  final double y;
  final Color color;
}