import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:firebase_database/firebase_database.dart';


class DesignationController {
  final DatabaseReference databaseReference =
  FirebaseDatabase.instance.reference().child("Designation");

  Future<void> saveDesignation(String name) async {
    try{ await databaseReference.push().set(name);
    }catch(e){
      displayToast(e.toString());
    }
  }

  Future<void> updateDesignation(String name,String id) async {

    databaseReference.update({
      id : name
    });

  }
  Future<void> deleteDesignation(String id) async {
    databaseReference.child(id).remove();

  }

  Future<List<Map<String,String>>> readDesignation() async {
    List<Map<String,String>> designation = [];
    await databaseReference.once().then((DataSnapshot snapshot) {
      if(snapshot.value != null){
        snapshot.value.forEach((key, value) {
          Map<String,String> gr = {key : value};
          designation.add(gr);
        });
      }

    });
    return designation;
  }

}
