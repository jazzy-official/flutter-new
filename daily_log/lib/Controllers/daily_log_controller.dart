import 'package:daily_log/Controllers/employeedetailcontroller.dart';
import 'package:daily_log/Controllers/employees_controller.dart';
import 'package:daily_log/Controllers/profilecontroller.dart';
import 'package:daily_log/Models/commit.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import '../Models/daili_log_model.dart';
import '../Models/task_model.dart';

class DailyLogController extends GetxController {
  List<DailyLog> commits = [];
  List<DailyLog> commitsbydate = [];
  int number = 1;
  List<Commit> comments = [];
  List<Commit> foundcomments = [];
  final DatabaseReference databaseReference =
      FirebaseDatabase.instance.ref("DailyLog");
  // var profileController = Get.put(ProfileController());
  // var employeeController = Get.put(EmployeeDetailController());
  DateTime date = DateTime.now();
  String? description;
  @override
  Future<void> onInit() async {
    // TODO: implement onInit
    await getData();
    datefilter();
    super.onInit();
  }

  // Future<void> addCommit(String taskid,String taskname,String des) async {
  //   DailyLog commit = DailyLog(time: DateTime.now(), name: profileController.userDetail.name, taskid: taskid, taskname: taskname,description: des);
  //   try{ await databaseReference.push().set(commit.toJson());
  //   }catch(e){
  //     Get.snackbar("Error", e.toString());
  //   }
  // }

  datefilter() {
    commitsbydate = commits
        .where((element) =>
            element.time.day == date.day &&
            element.time.month == date.month &&
            element.time.year == date.year)
        .toList();
    update();
  }


  //
  // // Future<void> updateBall(Ball ball) async {
  // //   databaseReference.update({
  // //     ball.id! : ball.toJson()
  // //   });
  // // }
  //
  //
  // Future<void> deleteTask(String id) async {
  //   databaseReference.child(id).remove();
  //
  // }
  //
  Future<void> getData() async {
    List<DailyLog> ball = [];
    await databaseReference.once().then((data) {
      if (data.snapshot.value != null) {
        Map<dynamic, dynamic> values =
            data.snapshot.value as Map<dynamic, dynamic>;
        values.forEach((key, value) {
          DailyLog gr = DailyLog.fromJson(value, key);
          ball.add(gr);
        });
      }
    });
    commits = ball;
    datefilter();
    update();
  }
}
