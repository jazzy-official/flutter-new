import 'package:ap_academy_admin/ModelView/Controller/NotificationController.dart';
import 'package:ap_academy_admin/ModelView/Controller/auth_controller.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import 'ModelView/Controller/db_cred.dart';

import 'screens/shared/SplashWidget.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await getMongoPort();
  // if (GetPlatform.isAndroid) {

  // }
  runApp(
    MyApp(),
  );
}

Future getMongoPort() async {
  try {
    if (!db.isConnected) {
      try {
        // if (!db.isConnected && db.state.toString() != "State.OPENING") {
        await db.open(secure:true).then((value) {});
        //  }
      } catch (error) {
        Get.snackbar("error", "Connection error try to restart Application");
        print('mongo db is not opening');
        print(error.toString());
      }
    }
  } catch (e) {
    print(e.toString());
    Get.snackbar("MongoDb Error", e.toString());
  }

  print("[AP Sports] Database is connected : " + db.isConnected.toString());
}

class MyApp extends StatelessWidget {
  final authController = Get.put(AuthController());

  @override
  Widget build(BuildContext context) {
    // FlutterStatusbarcolor.setStatusBarWhiteForeground(true);
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      themeMode: ThemeMode.dark,
      theme: ThemeData(
        primaryColor: MyResources.buttonColor,
        textTheme: GoogleFonts.aBeeZeeTextTheme(
          Theme.of(context).textTheme,
        ),
      ),
      home: FutureBuilder(
        future: authController.checkUserLoggedIn(),
        builder: (context, dynamic snapshot) {
          // if (snapshot.hasError) {
          //   return Text('Error Processing');
          // }

          if (snapshot.hasData) {
            return snapshot.data;
          }
          return LoadingWidget();
        },
      ),
    );
  }
}
