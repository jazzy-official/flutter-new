import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:rafi_admin/Authentication/login.dart';
import 'package:rafi_admin/Components/Screens/Purchase/Model/investment_model.dart';
import 'package:rafi_admin/Components/Screens/Purchase/Model/investments_list_model.dart';
import 'package:rafi_admin/Components/Screens/Purchase/SubScreens/Investments/investment_list.dart';
import 'package:rafi_admin/Components/loader_widget.dart';
import 'package:rafi_admin/Controllers/purchase_controller.dart';
import 'package:rafi_admin/Controllers/sales_controller.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;


class InvestmentsPage extends StatefulWidget {
  const InvestmentsPage({Key? key}) : super(key: key);

  @override
  _InvestmentsPageState createState() => _InvestmentsPageState();
}

class _InvestmentsPageState extends State<InvestmentsPage> {

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PurchaseController>(
      autoRemove: false,
      init: PurchaseController(),
      builder: (cont) => Scaffold(
        floatingActionButton: Container(
          height: 35,
          width: 170,
          decoration: BoxDecoration(
              color: Color(0xff205370),
              borderRadius: BorderRadius.circular(20)
          ),
          child: cont.investmentsData==null? Center(child: Text("Loading...",style: TextStyle(color: Colors.white),),) :Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              IconButton(
                padding: EdgeInsets.zero,
                constraints: BoxConstraints(),
                icon: const Icon(CupertinoIcons.backward_end_alt_fill,size: 13,color: Colors.white,),
                onPressed: () {
                  cont.investmentsData!.previousPage == null ? displayToast("You're on first page", context) :
                  {

                      cont.url = cont.investmentsData!.firstPage,

                    cont.getInvestmentData(),
                  };
                },
              ),
              IconButton(
                icon: const Icon(Icons.arrow_back_ios,size: 13,color: Colors.white,),
                padding: EdgeInsets.zero,
                constraints: BoxConstraints(),
                onPressed: () {
                  cont.investmentsData!.previousPage == null ? displayToast("You're on first page", context) :
                  {
                      cont.url = cont.investmentsData!.previousPage!,

                  cont.getInvestmentData(),
                  };
                },
              ),
              SizedBox(
                child: Center(
                  child: Text(
                    cont.investmentsData!.pageNo.toString(),
                    style: const TextStyle(
                        color: Colors.white, fontSize: 15, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              IconButton(
                padding: EdgeInsets.zero,
                constraints: BoxConstraints(),
                icon: const Icon(Icons.arrow_forward_ios,size: 13,color: Colors.white,),
                onPressed: () {
                  cont.investmentsData!.nextPage == null ? displayToast("You're on last page", context):
                  {

                      cont.url = cont.investmentsData!.nextPage!,

                  cont..getInvestmentData(),
                  };
                },
              ),
              IconButton(
                padding: EdgeInsets.zero,
                constraints: BoxConstraints(),
                icon: const Icon(CupertinoIcons.forward_end_alt_fill,size: 13,color: Colors.white,),
                onPressed: () {
                  cont.investmentsData!.nextPage == null ? displayToast("You're on last page", context) :
                  {

                    cont.url = cont.investmentsData!.lastPage,

                  cont.getInvestmentData(),
                  };
                },
              ),
            ],
          ),
        ),
        body: Column(
          children: [
            const SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width *0.95,
                    // (MediaQuery.of(context).size.width * 1) - 90,
                    alignment: Alignment.centerLeft,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8),
                        boxShadow: const [
                          BoxShadow(
                              color: Colors.black26,
                              blurRadius: 5,
                              offset: Offset(0, 0.5))
                        ]),
                    height: 35,
                    child: TextFormField(
                      onChanged: (val) {
                        cont.runFilter(val);
                      },
                      style: const TextStyle(
                        color: Colors.black87,
                      ),
                      decoration: const InputDecoration(
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.only(
                            top: 2,
                          ),
                          prefixIcon: Icon(
                            Icons.search,
                            color: Color(0xff7F7C82),
                          ),
                          hintText: 'Search',
                          hintStyle: TextStyle(
                            color: Colors.black38,
                          )),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Flexible(
              child: cont.investmentsData==null?LoadingWidget() :
              RefreshIndicator(
                onRefresh: () async {
                  return await cont.refreshAll();
                },
                  child: InvestmentList(investments: cont.foundResult,)
              ),
            ),
          ],
        ),
      ),
    );
  }
}
