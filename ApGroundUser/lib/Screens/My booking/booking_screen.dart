import 'package:ap_booking_user/Screens/My%20booking/Models/booking_model.dart';
import 'package:ap_booking_user/Screens/My%20booking/add_booking.dart';
import 'package:ap_booking_user/Screens/My%20booking/booking_card.dart';
import 'package:ap_booking_user/Screens/My%20booking/past_bookings.dart';
import 'package:ap_booking_user/Screens/My%20booking/today_bookings.dart';
import 'package:ap_booking_user/Screens/My%20booking/upcomming_booking.dart';
import 'package:ap_booking_user/Screens/Shared/loading_widget.dart';
import 'package:ap_booking_user/constants/util.dart';
import 'package:ap_booking_user/controllers/booking_controller.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
class MyBookingScreen extends StatefulWidget {
  const MyBookingScreen({Key? key}) : super(key: key);

  @override
  State<MyBookingScreen> createState() => _MyBookingScreenState();
}

class _MyBookingScreenState extends State<MyBookingScreen> {
  void refresh() async {
    setState(() {
      isLoading=true;
    });
    String uid = FirebaseAuth.instance.currentUser!.uid;
    myBookings = await BookingController().readBookingbyUid(uid);
    setState(() {
      upcomingBookings = myBookings.where((element) => element.date.isAfter(DateTime.parse(DateFormat('yyyy-MM-dd').format(DateTime.now().add(Duration(days: 1)))).subtract(Duration(hours: 1)))).toList();
      todayBookings = myBookings.where((element) => DateFormat('yyyy-MM-dd').format(element.date)==DateFormat('yyyy-MM-dd').format(DateTime.now())).toList();
      pastBookings = myBookings.where((element) => element.date.isBefore(DateTime.parse(DateFormat('yyyy-MM-dd').format(DateTime.now())))).toList();
      isLoading=false;
    });
  }
  bool isLoading = true;
  late List<Booking> myBookings;
  List<Booking> upcomingBookings = [];
  List<Booking> todayBookings = [];
  List<Booking> pastBookings = [];
  @override
  void initState() {
    refresh();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          toolbarHeight: 0,
          bottom:  PreferredSize(
            preferredSize: Size(double.infinity,40),
            child: ColoredBox(
              color: Colors.white,
              child: SizedBox(
                height: 40,
                child: TabBar(
                  indicator: BoxDecoration(
                      borderRadius:
                      BorderRadius.circular(0),
                      color: MyResources.buttonColor),
                  labelColor: Colors.white,
                  unselectedLabelColor: Colors.black,
                  tabs: const [

                    Tab(child: Text("Today"),),
                    Tab(child: Text("Upcoming"),),
                    Tab(child: Text("Past"),),

                  ],
                ),
              ),
            ),
          ),
        ),
        body: isLoading? const Center(child: LoadingWidget()) :  TabBarView(
          children: [
            TodayBookings(myBookings: todayBookings),
            UpComingBookings(myBookings: upcomingBookings,),
            PastBookings(myBookings:pastBookings)
          ],
        ),
      ),
    );
  }
}
