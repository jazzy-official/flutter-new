import 'package:ap_academy_admin/ModelView/Controller/GroupController.dart';
import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:ap_academy_admin/ModelView/Controller/teamsController.dart';
import 'package:get/get.dart';

import 'db_cred.dart';

class DashBoardController extends GetxController {
  Rx<List<Map<String, dynamic>>> pendingApprovalList =
      Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get pendingApproval => pendingApprovalList.value;
  Rx<List<Map<String, dynamic>>> groupList = Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get grouplist => groupList.value;
  final gControl = Get.put(GroupsControler());
  final tControl = Get.put(TeamController());
  @override
  Future<void> onInit() async {
    // pendingApprovalList.bindStream(
    //     DatabaseController().approvalStream()); //id)); //Stream Comming from
    tControl.refreshTeams();
    gControl.refreshGroups();

    groupWiseRefresh();
    super.onInit();
  }
//////////////////////////////////////////////////////////////////

  final _dbHelper = Get.put(DatabaseController());
  Future<bool> approveUser(dynamic _id,) async {
    String aproved = await _dbHelper.approveUser(_id,);
    await _dbHelper.updategroupusers(aproved);
    return true;
  }

  pendingRefresh() {
    pendingApprovalList.bindStream(DatabaseController().approvalStream());
  }

  //////////////////for team creation/////////////////
  RxList<String> teamList = ["Please Wait"].obs;

  Rx<List<Map<String, dynamic>>> group18WiseList =
      Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get group18Wise => group18WiseList.value;

  Rx<List<Map<String, dynamic>>> group11WiseList =
      Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get group11Wise => group11WiseList.value;
  Rx<List<Map<String, dynamic>>> group20WiseList =
      Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get group20Wise => group20WiseList.value;
  Rx<List<Map<String, dynamic>>> group25WiseList =
      Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get group25Wise => group25WiseList.value;
  groupWiseRefresh() async {
    groupList.bindStream(DatabaseController().groupnamesStream());
    group11WiseList
        .bindStream(DatabaseController().groupWiseStream(numb: 'Under 11'));
    group18WiseList
        .bindStream(DatabaseController().groupWiseStream(numb: 'Under 18'));
    group20WiseList
        .bindStream(DatabaseController().groupWiseStream(numb: 'Under 20'));
    group25WiseList
        .bindStream(DatabaseController().groupWiseStream(numb: 'Under 25'));
    await Future.delayed(Duration(seconds: 5));
  }

///////////////////////////////////////////////////on close//////////////////////////
  @override
  void onClose() {
    //db.close();
    print("DashBoard Controller is closed now");
    super.onClose();
  }
}
