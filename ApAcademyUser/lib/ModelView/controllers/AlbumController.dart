import 'dart:io';

import 'package:get/get.dart';

import 'db_controller.dart';

class PhotoAlbumController extends GetxController {
  Rx<List<Map<String, dynamic>>> albums = Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get album => albums.value;
  final dbHelper = Get.put(DatabaseController());
  RxList<File> photos = <File>[].obs;
///////////////////////////////////////////////
  ///refresh
///////////////////////////////////
  refreshData() {
    albums.bindStream(dbHelper.getAlbumData());
  }

//////////////////////////////////////////////////////////////////
  @override
  void onInit() async {
    albums.bindStream(dbHelper.getAlbumData());

    super.onInit();
  }
  ///////////////////////////////////////Upload Video

}
