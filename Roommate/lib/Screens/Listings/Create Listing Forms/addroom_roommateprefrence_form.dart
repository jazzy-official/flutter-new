import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/controllers/explore_controller.dart';

import '../../../controllers/my_listing_controller.dart';

class RoommatePreferenceForm extends StatefulWidget {
   const RoommatePreferenceForm({Key? key}) : super(key: key);

  @override
  State<RoommatePreferenceForm> createState() => _RoommatePreferenceFormState();
}

class _RoommatePreferenceFormState extends State<RoommatePreferenceForm> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          iconTheme: const IconThemeData(color: Colors.black),
          title: const Text("Step 6: Roommate preferences",style: TextStyle(color: Colors.black),),
          bottom: PreferredSize(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 22),
              child: LinearProgressIndicator(
                color: myResources.themeColor,
                backgroundColor: Colors.grey.shade300,
                minHeight: 6,
                value: 6 / 6,
              ),
            ),
            preferredSize: const Size.fromHeight(50),
          )),
      body: GetBuilder<MyListingController>(
        init: MyListingController(),
        builder: (cont) => Padding(
          padding: const EdgeInsets.all(15.0),
          child: ListView(
            children: [
              const Padding(
                padding: EdgeInsets.symmetric(vertical: 8.0),
                child: Text(
                  "Gender",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [

                  Expanded(
                    // margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            const BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.gender == "Male"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.gender="Male";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 0),
                        child: Center(
                            child: Text(
                              'Male',
                              style: TextStyle(
                                  color: cont.gender == "Male"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  Expanded(
                    // margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.gender == "Female"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.gender = "Female";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 0),
                        child: Center(
                            child: Text(
                              'Female',
                              style: TextStyle(
                                  color: cont.gender == "Female"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  Expanded(
                    // margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.gender == "Others"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.gender = "Others";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 0),
                        child: Center(
                            child: Text(
                              'Others',
                              style: TextStyle(
                                  color: cont.gender == "Others"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: const Text(
                  "Martial Status",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    // margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            const BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.martialStatus == "Married"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.martialStatus = "Married";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 0),
                        child: Center(
                            child: Text(
                              'Married',
                              style: TextStyle(
                                  color: cont.martialStatus == "Married"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  Expanded(
                    // margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.martialStatus == "Unmarried"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.martialStatus = "Unmarried";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 0),
                        child: Center(
                            child: Text(
                              'Unmarried',
                              style: TextStyle(
                                  color: cont.martialStatus == "Unmarried"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  Expanded(
                    // margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.martialStatus == "Any"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.martialStatus = "Any";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 0),
                        child: Center(
                            child: Text(
                              'Any',
                              style: TextStyle(
                                  color: cont.martialStatus == "Any"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: const Text(
                  "Occupation",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    // margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            const BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.occupation == "Student"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.occupation = "Student";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 0),
                        child: Center(
                            child: Text(
                              'Student',
                              style: TextStyle(
                                  color: cont.occupation == "Student"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  Expanded(
                    // margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.occupation == "Professional"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.occupation = "Professional";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 0),
                        child: Center(
                            child: Text(
                              'Professional',
                              style: TextStyle(
                                  color: cont.occupation == "Professional"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  Expanded(
                    // margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.occupation == "Any"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.occupation = "Any";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 0),
                        child: Center(
                            child: Text(
                              'Any',
                              style: TextStyle(
                                  color: cont.occupation == "Any"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: const Text(
                  "Age",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
              ),
              Text(
                "${cont.selectedrange.start.round()} years   -   ${cont.selectedrange.end.round()} years",
                style: TextStyle(
                    color: myResources.themeColor,
                    fontWeight: FontWeight.w500,
                    fontSize: 16),
              ),
              RangeSlider(
                values: cont.selectedrange,
                max: 50,
                min: 10,
                divisions: 40,
                labels: RangeLabels("${cont.selectedrange.start.round()} years","${cont.selectedrange.end.round()} years"),
                activeColor: myResources.themeColor,
                onChanged: (range) {
                  cont.selectedrange = range;
                  cont.update();
                  },
              ),
              Divider(height: 2,color: Colors.grey.shade600,),
              const Padding(
                padding: EdgeInsets.symmetric(vertical: 8.0),
                child: Text(
                  "Smoking",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    // margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            const BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.smoking == "Daily"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.smoking = "Daily";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 0),
                        child: Center(
                            child: Text(
                              'Daily',
                              style: TextStyle(
                                  color: cont.smoking == "Daily"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  Expanded(
                    // margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.smoking == "Occasionally"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.smoking = "Occasionally";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 0),
                        child: Center(
                            child: Text(
                              'Occasionally',
                              style: TextStyle(
                                  color: cont.smoking == "Occasionally"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  Expanded(
                    // margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.smoking == "Never"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.smoking = "Never";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 0),
                        child: Center(
                            child: Text(
                              'Never',
                              style: TextStyle(
                                  color: cont.smoking == "Never"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: const Text(
                  "Partying",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [

                  Expanded(
                    // margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            const BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.partying == "Weekend"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.partying = "Weekend";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 0),
                        child: Center(
                            child: Text(
                              'Weekend',
                              style: TextStyle(
                                  color: cont.partying == "Weekend"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  Expanded(
                    // margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.partying == "Occasionally"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.partying = "Occasionally";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 0),
                        child: Center(
                            child: Text(
                              'Occasionally',
                              style: TextStyle(
                                  color: cont.partying == "Occasionally"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  Expanded(
                    // margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.partying == "Never"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.partying = "Never";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 0),
                        child: Center(
                            child: Text(
                              'Never',
                              style: TextStyle(
                                  color: cont.partying == "Never"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: const Text(
                  "Guests",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [

                  Expanded(
                    // margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            const BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.guests == "Daily"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.guests = "Daily";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 0),
                        child: Center(
                            child: Text(
                              'Daily',
                              style: TextStyle(
                                  color: cont.guests == "Daily"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  Expanded(
                    // margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.guests == "Occasionally"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.guests = "Occasionally";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 0),
                        child: Center(
                            child: Text(
                              'Occasionally',
                              style: TextStyle(
                                  color: cont.guests == "Occasionally"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  Expanded(
                    // margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.guests == "Never"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.guests = "Never";
                        cont.update();

                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 0),
                        child: Center(
                            child: Text(
                              'Never',
                              style: TextStyle(
                                  color: cont.guests == "Never"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: const Text(
                  "Pets",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    // margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            const BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.pets == "Have"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.pets = "Have";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 0),
                        child: Center(
                            child: Text(
                              'Have',
                              style: TextStyle(
                                  color: cont.pets == "Have"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  Expanded(
                    // margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.pets == "Don't Have"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.pets = "Don't Have";
                        cont.update();
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 0),
                        child: Center(
                            child: Text(
                              'Don\'t Have',
                              style: TextStyle(
                                  color: cont.pets == "Don't Have"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  Expanded(
                    // margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(3)),
                            side: BorderSide(
                                color: cont.pets == "May have"
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        cont.pets = "May have";
                        cont.update();

                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 0),
                        child: Center(
                            child: Text(
                              'May have',
                              style: TextStyle(
                                  color: cont.pets == "May have"
                                      ? myResources.themeColor
                                      : Colors.black54),
                            )),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 30,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: myResources.themeColor,
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(14))),
                ),
                onPressed: () {
                  cont.addroom();
                },
                child: Container(
                  padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SizedBox(width: 20,),
                      Text(
                        'Proceed',
                        style: TextStyle(color: Colors.white),
                      ),
                      Container(
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                          borderRadius:
                          const BorderRadius.all(Radius.circular(20)),
                          color: myResources.themeColor,
                        ),
                        child: Icon(
                          Icons.arrow_forward_ios,
                          color: Colors.white,
                          size: 16,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
