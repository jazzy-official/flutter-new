import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:rafi_admin/Components/AppBar/appbar.dart';
import 'package:rafi_admin/Components/Drawer/drawer.dart';
import 'package:rafi_admin/Components/Screens/Sales/Models/deals_model.dart';
import 'package:rafi_admin/Components/Screens/Sales/widgets/deal_card.dart';
import 'package:http/http.dart' as http;
import 'package:rafi_admin/Components/loader_widget.dart';
import 'package:rafi_admin/Controllers/sales_controller.dart';
import 'package:rafi_admin/Controllers/units_controller.dart';
class SalesPage extends StatefulWidget {
  SalesPage({Key? key,}) : super(key: key);

  @override
  State<SalesPage> createState() => _SalesPageState();
}

class _SalesPageState extends State<SalesPage> {

  int? _value = 1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Sales", style: const TextStyle(
            fontSize: 20.0,
            fontWeight: FontWeight.w500,
            color: Colors.white),),
        backgroundColor: Color(0xff004251),
      ),
      drawer: const Drawer(child: SideDrawer()),
      body: GetBuilder<SalesController>(
      autoRemove: false,
      init: SalesController(),
      builder: (cont) => Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width *0.6,
                  // (MediaQuery.of(context).size.width * 1) - 90,
                  alignment: Alignment.centerLeft,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: const [
                        BoxShadow(
                            color: Colors.black26,
                            blurRadius: 5,
                            offset: Offset(0, 0.5))
                      ]),
                  height: 35,
                  child: TextFormField(
                    onChanged: (val) {
                      cont.runFilter(val);
                    },
                    style: const TextStyle(
                      color: Colors.black87,
                    ),
                    decoration: const InputDecoration(
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.only(
                          top: 2,
                        ),
                        prefixIcon: Icon(
                          Icons.search,
                          color: Color(0xff7F7C82),
                        ),
                        hintText: 'Search',
                        hintStyle: TextStyle(
                          color: Colors.black38,
                        )),
                  ),
                ),
                Wrap(
                    children: [
                      ChoiceChip(
                        elevation: 4,
                        pressElevation: 2,
                        shadowColor: Colors.black,
                        label: Text('Affiliate'),
                        selected: _value == 0,
                        onSelected: (bool selected) {
                          setState(() {
                            _value = selected ? 0 : 0;
                          });
                          cont.value = selected ? 0 : 0;
                        },
                      ),
                      SizedBox(width: 3,),
                      ChoiceChip(
                        elevation: 4,
                        pressElevation: 2,
                        shadowColor: Colors.black,
                        label: Text('Deal'),
                        selected: _value == 1,
                        onSelected: (bool selected) {
                          setState(() {
                            _value = selected ? 1 : 1;
                          });
                          cont.value = 1;
                        },
                      )
                    ]
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Expanded(
            child: cont.deals==null?LoadingWidget(): RefreshIndicator(
              onRefresh: () async {
                return await cont.refreshAll();
              },
              child: ListView.builder(
                physics: const BouncingScrollPhysics(
                    parent: AlwaysScrollableScrollPhysics()),
                itemCount: cont.foundResult.length,
                itemBuilder: (context, i) {
                  final deal = cont.foundResult[i];
                  return DealCard(
                    deal: deal,
                  );
                },
              ),
            ),
          ),
        ],
      ),
    ),
    );
  }
}
