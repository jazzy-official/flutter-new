import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class MyResources {
  //static Color primaryColor = Color(0xfff5f5f5);
  static Color backgroundColor = Color(0xfff5f5f5);
  static Color appBarActionsColor = Colors.white;
  static Color themecolor = Color(0xff222972);
  static Color themecolor2 = Color(0xff63f6f9);
  static Color blackcolor = Color(0xff282828);
  static const Color headingColor = Colors.orange;
  static Color buttonColor = Color(0xff1758B4);
  static Color sideBarColor = Color(0xfff5f5f5);
  static Color buttontextColor = Color(0xffebebeb);
  static Color hintColor = Color(0xffd2d2d2);
  static TextStyle titleTextStyle = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.bold,
    color: Colors.black,
  );
  static TextStyle nameTextStyle = TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.w300,
    color: Color(0xff1758B4),
  );
  static TextStyle appTextStyle = TextStyle(
    fontSize: 14,
    color: Colors.black,
  );
  static TextStyle appTextStyle2 = TextStyle(
    fontSize: 14,
    color: Colors.black,
    fontWeight: FontWeight.bold,
  );
  static TextStyle appHeadingStyle = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w600,
  );
  static TextStyle timestampStyle = TextStyle(
    fontSize: 12,
    color: Colors.black54,
  );
  static TextStyle modelHeadingStyle = TextStyle(
    fontSize: 20,
    color: Colors.white,
    fontWeight: FontWeight.w700,
  );
  static TextStyle textStyleprofile = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w400,
  );
  ///////////////////////Input Decoration////////////////
  static InputDecoration textFieldBorder = const InputDecoration(
      border: InputBorder.none,
      contentPadding: EdgeInsets.only(
        top: 5,
      ),
      prefixIcon: Icon(
        Icons.search,
        color: Color(0xff7F7C82),
      ),
      hintText: 'Search',
      hintStyle: TextStyle(
        color: Colors.black38,
      ));
  static BoxDecoration roundedBoxDecore = BoxDecoration(
      color: backgroundColor,
      border: Border.all(color: Colors.transparent),
      borderRadius: BorderRadius.circular(10));
  static BoxDecoration roundeddropdown = BoxDecoration(
      color: backgroundColor,
      border: Border.all(color: Colors.grey),
      borderRadius: BorderRadius.circular(10));


}
String formatnmbr(double n){
  var formatter = NumberFormat('#,##,000');
  String nmbr = formatter.format(n.toInt());
  return nmbr;
}

InputDecoration inputdecoration(String text,Icon? icon){
  return  InputDecoration(
    border: const OutlineInputBorder(),
    focusedBorder: const OutlineInputBorder(
  borderSide: const BorderSide(color: Colors.black, width: 0.0),
  ),
    labelText: text,
    floatingLabelStyle: TextStyle(color: Colors.black),
    prefixIcon: icon,
    isDense: true,
    contentPadding: const EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 10.0),
  );
}