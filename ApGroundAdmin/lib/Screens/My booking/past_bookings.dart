
import 'package:flutter/material.dart';

import 'Models/booking_model.dart';
import 'booking_card.dart';

class PastBookingScreen extends StatefulWidget {
  final List<Booking> pastBookings;
  const PastBookingScreen({Key? key, required this.pastBookings}) : super(key: key);

  @override
  _PastBookingScreenState createState() => _PastBookingScreenState();
}

class _PastBookingScreenState extends State<PastBookingScreen> {

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xfffefefe),
      child: Column(
        children: [
          widget.pastBookings.isEmpty ? Expanded(child: Center(child: Text("No Result found"),)):
          Expanded(
            child: ListView.builder(
                padding: const EdgeInsets.all(8),
                itemCount: widget.pastBookings.length,
                itemBuilder: (BuildContext context, int index) {
                  Booking booking = widget.pastBookings[index];
                  return BookingCard(booking: booking,);


                }),
          ),
        ],
      ),
    );
  }
}
