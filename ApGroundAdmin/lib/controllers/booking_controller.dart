import 'dart:convert';
import 'package:ap_ground_admin/Screens/My%20booking/Models/booking_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:intl/intl.dart';
import 'fire_base_controller.dart';
import 'package:firebase_database/firebase_database.dart';

class BookingController {
  final _auth = FirebaseAuth.instance;
  final DatabaseReference databaseReference =
      FirebaseDatabase.instance.reference().child("Bookings");

  Future<void> saveBooking(Booking booking) async {
    try {
      await databaseReference.push().set(booking.toJson());
    } catch (e) {
      displayToast(e.toString());
      print(e.toString());
    }
  }

  Future<void> updateBooking(Booking Booking) async {
    databaseReference.update({Booking.id!: Booking.toJson()});
  }

  Future<void> deleteBooking(String id) async {
    databaseReference.child(id).remove();
  }

  Future<List<Booking>> readAllBookings() async {
    List<Booking> bookings = [];
    await databaseReference.once().then((DataSnapshot snapshot) {
      if (snapshot.exists) {
        snapshot.value.forEach((key, value) {
          Booking gr = Booking.fromJson(value, key);
          bookings.add(gr);
        });
      }
    });
    return bookings;
  }

  Future<List<Booking>> readBookingbyUid(String uid) async {
    List<Booking> bookings = await readAllBookings();
    List<Booking> booking = bookings
        .where((element) => element.slot.Pslot1!.teamId == uid||element.slot.Pslot2!.teamId == uid)
        .toList();
    print(booking.length);
    return booking;
  }

  Future<List<Booking>> readBookingDate(DateTime date) async {
    List<Booking> bookings = await readAllBookings();
    List<Booking> booking = bookings
        .where((element) => DateFormat('yyyy:MM:dd').format(element.date) == DateFormat('yyyy:MM:dd').format(date))
        .toList();
    print(booking.length);
    return booking;
  }
  Future<List<Booking>> readUpcomingooking() async {
    List<Booking> bookings = await readAllBookings();
    List<Booking> booking = bookings
        .where((element) => element.date.isAfter(DateTime.parse(DateFormat('yyyy-MM-dd').format(DateTime.now().add(Duration(days: 1)))).subtract(Duration(hours: 1))))
        .toList();
    print(booking.length);
    return booking;
  }
}
