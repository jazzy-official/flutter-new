import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:ap_academy_admin/screens/drawer_sreen.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:mongo_dart/mongo_dart.dart';
import 'package:http/http.dart' as http;

class NotificationController extends GetxController {
  final dbHelper = Get.put(DatabaseController());
  Rx<List<Map<String, dynamic>>> notifList = Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get notify => notifList.value;
  @override
  Future<void> onInit() async {
    // String id = Get.find<AuthController>().firebaseUser.uid;
    notifList.bindStream(DatabaseController()
        .notificationStream()); //id)); //Stream Comming from

    super.onInit();
  }

  Future<bool> delnotif(ObjectId id) async {
    if (await DatabaseController().deletNotif(id)) {
      refreshDate();
      return true;
    } else {
      return false;
    }
  }

  Future refreshDate() async {
    notifList.bindStream(DatabaseController().notificationStream());
  }

  Future<List<String>?> getTokensbyGroup(String groupName) async {
    return await dbHelper.getTokensbygroup(groupName);
  }

  Future<List<String>?> getTokensbyTeam(String teamName) async {
    return await dbHelper.getTokensbyTeamgroup(teamName);
  }

  Future<bool> addnewNotification(
      Map<String, dynamic> notificationModel) async {
    bool done = await dbHelper.addNotifications(notificationModel);
    refreshDate();
    return done;
  }

  /////////////////////////////////////////////////////notification section
  ///
  final String serverToken =
      'AAAAjwzCMbM:APA91bF3fX844AxrlF7-HhYj2twvl9yQR3i_csFs9pDOwUIiBA0YDCRWsxLEtrlnx7ZW3f_tPdA1bTwnW-bU6JfqACPl_7gG8iD9tAHDSA3rG-4aHiqxKqwV4UT8ploK2MLT2TfsPJr7';

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  Future<void> openNotification(Map<String, dynamic> msgData) async {
    const AndroidNotificationDetails androidPlatformChannelSpecifics =
    AndroidNotificationDetails(
        'click_action', 'FLUTTER_NOTIFICATION_CLICK',
        channelDescription: 'Ap Sports academy ',
        importance: Importance.high,
        priority: Priority.high,
        showWhen: true,
        playSound: true);
    const NotificationDetails platformChannelSpecifics =
    NotificationDetails(android: androidPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
        1,
        msgData['notification']['title'],
        msgData['notification']['body'],
        platformChannelSpecifics,
        payload: 'item x');
  }



  Future<void> sendAndRetrieveMessage(
      Map<String, dynamic> dataMsg, List<String> tokens) async {
    if (Platform.isIOS) {
      await _firebaseMessaging.requestPermission(
        alert: true,
        announcement: false,
        badge: true,
        carPlay: false,
        criticalAlert: false,
        provisional: false,
        sound: true,
      );
    }
    try {
      Map data = {
        "registration_ids": tokens,
        "collapse_key": "type_a",
        "notification": {
          "title": dataMsg['title'],
          "body": dataMsg['desc'],
        },
        "data": {
          "title": dataMsg['title'],
          "body": dataMsg['desc'],
        }
      };
      await http.post(
        Uri.parse('https://fcm.googleapis.com/fcm/send'),
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Authorization': 'key=$serverToken',
        },
        body: json.encode(data),
      );
    } catch (e) {
      print(e.toString());
    }

    final Completer<Map<String, dynamic>> completer =
    Completer<Map<String, dynamic>>();

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
          (Map<String, dynamic> message) async {
        completer.complete(message);
      };
    });


    //  return completer.future;
  }

  Future onDidReceiveLocalNotification(
      int id, String? title, String? body, String? payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    Get.defaultDialog(
      title: title!,
      content: Text(body!),
      actions: [
        CupertinoDialogAction(
          isDefaultAction: true,
          child: Text('Ok'),
          onPressed: () async {
            Get.to(() => HomeDrawer());
          },
        )
      ],
    );
  }
}

abstract class NotificationAction {
  NotificationAction(this.notification);

  Notification notification;
  bool? isInForeground;
}

final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
Future<dynamic> myBackgroundMessageHandler(RemoteMessage message) async {
  final localNotif = Get.put(NotificationController());
  if (message != null) {
    await localNotif.openNotification(message.data);
  } else {
    print('sorry');
  }

  // Or do other work.
}


fcmListner() {
  final localNotif = Get.put(NotificationController());
  try {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
          (Map<String, dynamic> message) async {
        await localNotif.openNotification(message);
        print("onMessage: $message");
      };
    });
    FirebaseMessaging.onBackgroundMessage((RemoteMessage message) {
      return myBackgroundMessageHandler(message);
    });
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
          (Map<String, dynamic> message) async {
        Get.to(HomeDrawer());
        print("onLaunch: $message");
      };
    });


    const AndroidInitializationSettings initializationSettingsAndroid =
    AndroidInitializationSettings('@mipmap/ic_launcher');
    final IOSInitializationSettings initializationSettingsIOS =
    IOSInitializationSettings(
        onDidReceiveLocalNotification:
        localNotif.onDidReceiveLocalNotification);
    final InitializationSettings initializationSettings =
    InitializationSettings(
        android: initializationSettingsAndroid,
        iOS: initializationSettingsIOS);
    localNotif.flutterLocalNotificationsPlugin
        .initialize(initializationSettings)
        .then((value) {
      print('initialized notification');
    });

    if (Platform.isIOS) {
      _firebaseMessaging.requestPermission(
        alert: true,
        announcement: false,
        badge: true,
        carPlay: false,
        criticalAlert: false,
        provisional: false,
        sound: true,
      );
    }
    _firebaseMessaging.getToken().then((String? token) {
      assert(token != null);

      print(token.toString());
    });
  } catch (ex) {
    print(ex.toString());
  }
}
