import 'package:ap_academy_admin/screens/Teams/add_group.dart';
import 'package:ap_academy_admin/screens/Teams/group_detail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:ap_academy_admin/ModelView/Controller/GroupController.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:ap_academy_admin/screens/shared/DeletPop.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GroupsScreen extends StatefulWidget {
  @override
  _GroupsScreenState createState() => _GroupsScreenState();
}

class _GroupsScreenState extends State<GroupsScreen> {
  final groupController = Get.put(GroupsControler());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyResources.backgroundColor,
      body: Material(
        //Create an event add button
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "",
                    style: MyResources.appHeadingStyle,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(0),
                    child: Container(
                      width: Get.width * 0.2,
                      height: 25,
                      child: RaisedButton(
                        textColor: MyResources.buttontextColor,
                        color: MyResources.buttonColor,
                        child: Text(
                          "Add",
                        ),
                        onPressed: () {
                          Get.to(AddNewGroup());
                        },
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            //Card
            GetX<GroupsControler>(
              init: Get.put<GroupsControler>(GroupsControler()),
              builder: (GroupsControler teamController) {
                if (teamController != null && teamController.group != null) {
                  print(teamController.group.toString());
                  return Expanded(
                    flex: 1,
                    child: ListView.builder(
                      itemCount: teamController.group.length,
                      itemBuilder: (BuildContext context, int index) =>
                          getTeamCard(teamController.group[index]),
                    ),
                  );
                } else {
                  return Container(
                    child: LoadingWidget(),
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget getTeamCard(Map<String, dynamic> team) {
    return Padding(
      padding: const EdgeInsets.only( left: 4, right:4),
      child: Center(
        child: InkWell(
          onTap: () {
            Get.to(GroupDetails(
              teamModel: team,
            ));
          },
          child: Container(
            height: 70,
            //create the cards for all details of event happening
            child: Card(
              color: MyResources.backgroundColor,
              child: Padding(
                padding: const EdgeInsets.all(4),
                child: Row(
                  //crossAxisAlignment: CrossAxisAlignment.start,
                  //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text("Group : ", style: MyResources.titleTextStyle),
                          // SizedBox(
                          //   width: 20,
                          // ),
                          // Text("Type:", style: MyResources.titleTextStyle),
                          // SizedBox(
                          //   height: 20,
                          // ),
                          // Text(" Coach:", style: MyResources.titleTextStyle),
                        ],
                      ),
                    ),
                    Container(
                      width:Get.width*0.5,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(team['teamName'].toString().toUpperCase(),
                              style: MyResources.nameTextStyle),
                          // SizedBox(
                          //   height: 20,
                          // ),
                          // // Text(
                          // //   team['teamType'],
                          // // ),
                          // SizedBox(
                          //   height: 20,
                          // ),
                          // Text(
                          //   team['coach'],
                          // ),
                        ],
                      ),
                    ),
                    Container(
                      alignment:Alignment.centerRight,
                      //padding: const EdgeInsets.only(left:80),
                      child:
                      // Column(
                      //   mainAxisAlignment: MainAxisAlignment.end,
                      //   children: [
                      MaterialButton(
                        // onPressed: () {
                        //   Get.dialog(
                        //     TeamsDetails(
                        //       team1Model: team,
                        //     ),
                        //   );},
                        onPressed: () {  },
                        child: Icon(
                          CupertinoIcons.person_3_fill,
                          color: Colors.blueAccent,
                          size: 20,
                        ),
                      ),

                    ),
                    Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(

                            team['teamMembers'] != null
                                ? team['teamMembers'].length.toString()
                                : '0',
                          ),
                          // SizedBox(
                          //   height: 20,
                          // ),
                          // // Text(
                          // //   team['teamType'],
                          // // ),
                          // SizedBox(
                          //   height: 20,
                          // ),
                          // Text(
                          //   team['coach'],
                          // ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
