import 'package:flutter_clean_calendar/clean_calendar_event.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'CalendarEventList.dart';
import 'dashboard_screen.dart';

class EventList extends StatefulWidget {
  final List<Meeting> event;

  const EventList({Key? key, required this.event}) : super(key: key);

  @override
  _EventListState createState() => _EventListState();
}

class _EventListState extends State<EventList> {
  bool isExpand = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      width: Get.width,
      child: Column(
        children: [
          Container(
            height: 400,
            child: ListView.builder(
              itemCount: widget.event.length,
              itemBuilder: (context, index) {
                Meeting e = widget.event[index];
                return Container(
                  padding: EdgeInsets.only(top: 10,bottom: 5),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 1.0, color: Colors.lightBlue.shade900),
                    ),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(Icons.event_available, color: Colors.orange),
                      ),
                      SizedBox(width: 20,),
                      Container(
                        width:Get.width*0.75,
                        child: Column(
                          children: [
                            Text(e.eventName,style: TextStyle(color: Colors.lightBlue.shade900,fontSize: 17,fontWeight: FontWeight.w600),),
                            Row(
                              crossAxisAlignment:CrossAxisAlignment.start,
                              children: [
                                Text("Name : ",),
                                Flexible(child: Text(e.description,style: TextStyle(color: Colors.lightBlue.shade900),)),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  crossAxisAlignment:CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(height: 5,),
                                    Text("Date"),
                                    Text(DateFormat('dd-MMM-yyyy').format(e.from.toLocal())),
                                  ],
                                ),
                                Column(
                                  children: [
                                    SizedBox(height: 5,),
                                    Text("Start Time"),
                                    Text(DateFormat('jm').format(e.from.toLocal())),
                                  ],
                                ),
                                Column(
                                  children: [
                                    SizedBox(height: 5,),
                                    Text("End Time"),
                                    e.to == null?Text("N/A"):
                                    Text(DateFormat('jm').format(e.to!.add(Duration(hours: 2)).toLocal())),
                                  ],
                                ),
                              ],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                );
              },
            ),
          ),

          isExpand
              ? Card(
            elevation: 3,
            shadowColor: Colors.orange,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      "Date",
                      style: MyResources.linkColor,
                    ),
                    SizedBox(height: 20),
                    Container(
                      width: 100,
                      child: Text(
                        "Start",
                        style: MyResources.appTextStyle,
                      ),
                    ),SizedBox(height: 10),
                  ],
                ),

                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Container(
                      width: 130,
                      child: Text(
                        DateFormat.yMEd().format(day1.value),
                        style: MyResources.linkColor,
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      width: 130,
                      child: Text(
                        DateFormat.jm().format(day1.value),
                        style: MyResources.appTextStyle,
                      ),
                    )
                  ],
                ),
                SizedBox(height: 10),
              ],
            ),
          )
              : Container(),
        ],
      ),
    );
  }
}
