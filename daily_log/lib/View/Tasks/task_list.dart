import 'package:daily_log/Controllers/task_controller.dart';
import 'package:daily_log/Models/task_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:intl/intl.dart';
import '../delete_popup.dart';
import 'Add Tasks/add_task.dart';

class TaskList extends StatelessWidget {
  final List<TaskModel> tasks;
   TaskList({Key? key, required this.tasks}) : super(key: key);
  var taskcont = Get.put(TaskController());
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
      itemCount: tasks.length,
      itemBuilder: (context, index) {
        TaskModel task = tasks[index];
        return Slidable(
          // Specify a key if the Slidable is dismissible.
          key: const ValueKey(0),

          // The start action pane is the one at the left or the top side.

          // The end action pane is the one at the right or the bottom side.
          endActionPane: ActionPane(
            // dismissible: DismissiblePane(onDismissed: () {}),
            motion: ScrollMotion(),
            children: [
              SlidableAction(
                onPressed: (BuildContext context) {
                  Get.to(AddTask(task:task));
                },
                backgroundColor: const Color(0xFF0392CF),
                foregroundColor: Colors.white,
                icon: Icons.edit,
                label: 'Edit',
              ),
              SlidableAction(
                onPressed: (BuildContext context) async {
                  showDialog(
                      context: context,
                      builder: (_) => AlertDialog(
                        content: DeletepopUp(message: "Are you sure?",pressFunc: () async {
                          await taskcont.deleteTask(task.id!);
                          Get.back();
                          Get.snackbar("Successful", "Task Deleted");
                          await taskcont.readBall();
                          taskcont.update();;

                        },),
                      ));



                },
                backgroundColor: Color(0xFFFE4A49),
                foregroundColor: Colors.white,
                icon: Icons.delete,
                label: 'Delete',
              ),
            ],
          ),

          // The child of the Slidable is what the user sees when the
          // component is not dragged.
          child: Card(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        task.name,
                        style: const TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 16),
                      ),
                      Column(
                        children: [
                          SizedBox(
                            width: Get.width-50,
                            child: Text(
                              task.des,
                              style: TextStyle(fontSize: 12,color: Colors.grey),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Text(
                            "Assign to : ",
                            style: TextStyle(
                                fontWeight: FontWeight.w600),
                          ),
                          Text(task.assignto!),
                        ],
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          const Text(
                            "Deadline : ",
                            style: TextStyle(
                                fontWeight: FontWeight.w600),
                          ),
                          Text(DateFormat("dd MMM yyyy")
                              .format(task.deadline)),
                        ],
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          const Text(
                            "Status : ",
                            style: TextStyle(
                                fontWeight: FontWeight.w600),
                          ),
                          Text(
                            task.status ? "Completed" : "Pending",
                            style: TextStyle(
                                color: task.status
                                    ? Colors.green
                                    : Colors.red),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          Text(
                            "Project : ",
                            style: TextStyle(
                                fontWeight: FontWeight.w600),
                          ),
                          Text(
                              task.project.toString()),

                        ],
                      ),
                    ],
                  ),
                  Center(
                    child: Icon(
                      Icons.chevron_left,
                      size: 22,
                      color: Colors.grey,
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
