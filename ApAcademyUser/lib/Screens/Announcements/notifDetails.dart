import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ap_academy_new/Constants/util.dart';
import 'package:intl/intl.dart';

class NotifyDetails extends StatelessWidget {
  final Map<String, dynamic> notificationModel;

  const NotifyDetails({Key? key, required this.notificationModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 0.0, left: 10, right: 10),
      child: Center(
        child: Container(
          height: Get.height * 0.6,
          child: Card(
            color: myResources.backgroundColor,
            child: Stack(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 20.0, right: 10, left: 10),
                      child: Center(
                        child: Container(
                          height: 40,
                          width: Get.width,
                          color: myResources.buttonColor,
                          child: Center(
                            child: Text(
                              notificationModel['title'],
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                        ),
                      ),
                    ),
                    notificationModel['type'] != null
                        ? Padding(
                            padding: const EdgeInsets.only(
                              top: 40,
                              right: 10,
                              left: 10,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Type: ',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: myResources.buttonColor,
                                      fontSize: 15),
                                ),
                                Text(
                                  notificationModel['type'].toString(),
                                  textAlign: TextAlign.center,
                                  style:
                                      TextStyle(color: myResources.buttonColor),
                                ),
                              ],
                            ),
                          )
                        : Container(),
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 40,
                        right: 10,
                        left: 10,
                      ),
                      child: Text(
                        notificationModel['description'].toString(),
                        // textAlign: TextAlign.center,
                        style: TextStyle(color: myResources.buttonColor),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
                Align(
                  alignment: Alignment.bottomLeft,
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 10, right: 10, left: 10, bottom: 10),
                    child: Container(
                      width: 200,
                      child: Text(
                        DateFormat.yMEd()
                            .add_jm()
                            .format(notificationModel['timeCreated']),
                        style: TextStyle(color: Colors.grey),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
