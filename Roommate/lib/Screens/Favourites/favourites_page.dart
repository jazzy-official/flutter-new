import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Screens/Home/Widgets/room_list.dart';
import 'package:room_buddy/Screens/Home/Widgets/roommate_list.dart';
import 'package:room_buddy/Screens/Home/room_detail.dart';
import 'package:room_buddy/Screens/Home/roommate_detail.dart';
import 'package:room_buddy/controllers/favourites_controller.dart';

import '../../controllers/explore_controller.dart';

class FavouritePage extends StatelessWidget {
   FavouritePage({Key? key}) : super(key: key);
  var expcont = Get.put(ExplorerController());
  @override
  Widget build(BuildContext context) {
    return
      // DefaultTabController(
      // length: 2,
      // child:
    Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          // bottom:  TabBar(
          //   indicatorWeight: 4,
          //   indicatorColor: myResources.themeColor,
          //   tabs: const [
          //     Padding(
          //       padding: EdgeInsets.all(8.0),
          //       child: Text("Rooms",style: TextStyle(color: Colors.black,fontWeight: FontWeight.w600,fontSize: 16),),
          //     ),
          //     Padding(
          //       padding: EdgeInsets.all(8.0),
          //       child: Text("Roommates",style: TextStyle(color: Colors.black,fontWeight: FontWeight.w600,fontSize: 16),),
          //     ),
          //   ],
          // ),
          title: const Text('Favourites',style: TextStyle(color: Colors.black),),
        ),
        body: ListView(
          children: [
            GetBuilder<FavouritesController>(
              init: FavouritesController(),
              builder: (cont) => cont.favrooms==null? const CircularProgressIndicator(): RoomList(rooms: cont.favrooms!,),
            ),
            SizedBox(height: 50,)
          ],
        ),
        // TabBarView(
        //   children: [
        //     RoomList(route: () { Get.to(RoomDetailScreen());  },),
        //     RoommateList(route: () { Get.to(RoommateDetailScreen()); },),
        //   ],
        // ),
      );
    // );
  }
}
