import 'package:ap_booking_user/Screens/About/about_screen.dart';
import 'package:ap_booking_user/Screens/Home/home_screen.dart';
import 'package:ap_booking_user/Screens/My%20booking/booking_screen.dart';
import 'package:ap_booking_user/Screens/Payment%20Method/payment_screen.dart';
import 'package:ap_booking_user/Screens/Privacy%20Policy/policy_screen.dart';
import 'package:ap_booking_user/Screens/Sponsor/sponsor_screen.dart';
import 'package:ap_booking_user/constants/util.dart';
import 'package:ap_booking_user/controllers/user_controller.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';

import '../wrapper.dart';
import 'Authentication/Models/user_model.dart';
import 'Grounds/gounds_screen.dart';


Widget selectedWidget = HomeScreen();
String title = "Home";
class NavBar extends StatefulWidget {
  const NavBar({Key? key}) : super(key: key);

  @override
  State<NavBar> createState() => _NavBarState();
}

class _NavBarState extends State<NavBar> {

  late UserModel user;
  Future<void> _signOut() async {
    await FirebaseAuth.instance.signOut();
  }
  bool isLoading = true;
  void refresh()async{
    setState(() {
      isLoading = true;
    });
    user =await UserController().readData(FirebaseAuth.instance.currentUser!.uid);
    setState(() {
      isLoading = false;
    });
  }
  @override
  void initState() {
    refresh();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyResources.buttonColor,
        title: Text(title),
        centerTitle: true,
      ),
      drawer: Drawer(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ListView(
              // Remove padding
              padding: EdgeInsets.zero,
              shrinkWrap: true,
              children: [
                isLoading ? Container(
                    height:200,child: const Center(child: CircularProgressIndicator())) :UserAccountsDrawerHeader(
                  accountName: Text(user.name),
                  accountEmail: Text(user.email),
                  currentAccountPicture: CircleAvatar(
                    child: ClipOval(
                      child:user.imgUrl==""
                          ? Icon(Ionicons.person,size: 55,color: Colors.grey.shade300,):
                       Image.network(
                        user.imgUrl,
                        fit: BoxFit.cover,
                        width: 90,
                        height: 90,
                      ),
                    ),
                  ),
                  decoration:  BoxDecoration(
                    color: Color(0xff000000),
                    image: DecorationImage(
                        colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.3), BlendMode.dstATop),
                        fit: BoxFit.fill,
                        image: AssetImage(
                            'assets/ap.jpg')),
                  ),
                ),
                ListTile(
                  tileColor: title == "Home"
                      ? Color(0xff1758B4).withOpacity(0.2)
                      : Colors.transparent,
                  leading: Icon(Ionicons.home_outline,color: Color(0xff1758B4),),
                  title: Text('Home'),
                  onTap: () {
                    if (mounted)
                      setState(() {
                        title = "Home";
                        selectedWidget = HomeScreen();
                      });
                    Navigator.pop(context);

                    // Navigator.push(context,
                    //     MaterialPageRoute(builder: (context) => EventScreen()));
                  },
                ),
                ListTile(
                  tileColor: title == "Grounds"
                      ? Color(0xff1758B4).withOpacity(0.2)
                      : Colors.transparent,
                  leading: Image.asset('assets/field.png',height: 25,),
                  title: Text('Grounds'),
                  onTap: () {
                    if (mounted) {
                      setState(() {
                        // selectedWidget = EventScreen().obs;
                        title = "Grounds";
                        selectedWidget = GroundScreen();
                      });
                    }
                    Navigator.pop(context);

                  },
                ),
                ListTile(
                  tileColor: title == "My Booking"
                      ? Color(0xff1758B4).withOpacity(0.2)
                      : Colors.transparent,
                  leading: Icon(Ionicons.book_outline,color: Color(0xff1758B4),),
                  title: Text('My Booking'),
                  onTap: () {
                    if (mounted)
                      setState(() {
                        // selectedWidget = EventScreen().obs;
                        title = "My Booking";
                        selectedWidget = MyBookingScreen();
                      });
                    Navigator.pop(context);

                  },
                ),
                ListTile(
                  tileColor: title == "Payment Method"
                      ? Color(0xff1758B4).withOpacity(0.2)
                      : Colors.transparent,
                  leading: Icon(Ionicons.cash_outline,color: Color(0xff1758B4),),
                  title: Text('Payment Method'),
                  onTap:  () {
                    if (mounted)
                      setState(() {
                        // selectedWidget = EventScreen().obs;
                        title = "Payment Method";
                        selectedWidget = PaymentMethodScreen();
                      });
                    Navigator.pop(context);
                    // Navigator.push(context,
                    //     MaterialPageRoute(builder: (context) => EventScreen()));
                  },
                ),
                ListTile(
                  tileColor: title == "Sponsor"
                      ? Color(0xff1758B4).withOpacity(0.2)
                      : Colors.transparent,
                  leading: Icon(Ionicons.medal_outline,color: Color(0xff1758B4),),
                  title: Text('Sponsor'),
                  onTap:  () {
                    if (mounted)
                      setState(() {
                        // selectedWidget = EventScreen().obs;
                        title = "Sponsor";
                        selectedWidget = SponsorScreen();
                      });
                    Navigator.pop(context);
                    // Navigator.push(context,
                    //     MaterialPageRoute(builder: (context) => EventScreen()));
                  },
                ),
                ListTile(
                  tileColor: title == "About"
                      ? Color(0xff1758B4).withOpacity(0.2)
                      : Colors.transparent,
                  leading: Icon(Ionicons.information_circle_outline,color: Color(0xff1758B4),),
                  title: Text('About'),
                  onTap:  () {
                    if (mounted)
                      setState(() {
                        // selectedWidget = EventScreen().obs;
                        title = "About";
                        selectedWidget = AboutScreen();
                      });
                    Navigator.pop(context);
                    // Navigator.push(context,
                    //     MaterialPageRoute(builder: (context) => EventScreen()));
                  },
                ),
                ListTile(
                  tileColor: title == "Privacy Policies"
                      ? Color(0xff1758B4).withOpacity(0.2)
                      : Colors.transparent,
                  leading: Icon(Icons.description,color: Color(0xff1758B4),),
                  title: Text('Privacy Policies'),
                  onTap:  () {
                    if (mounted)
                      setState(() {
                        // selectedWidget = EventScreen().obs;
                        title = "Privacy Policies";
                        selectedWidget = PolicyScreen();
                      });
                    Navigator.pop(context);
                    // Navigator.push(context,
                    //     MaterialPageRoute(builder: (context) => EventScreen()));
                  },
                ),

              ],
            ),
            Expanded(child: Image.asset("assets/logo.png"),),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    height: 35,
                    decoration: BoxDecoration(
                      color: MyResources.buttonColor.withOpacity(0.2),
                      borderRadius: BorderRadius.circular(5)
              ),
                    
                    child: TextButton(onPressed: ()async {
                      await _signOut();
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => const Wrapper()),
                      );
                    }, child: Row(
                      children: [
                        Text('Log out',style: TextStyle(color: MyResources.buttonColor),),
                        SizedBox(width: 10,),
                        Icon(Icons.exit_to_app,color: MyResources.buttonColor,),
                      ],
                    )),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
      body: selectedWidget,
    );
  }
}