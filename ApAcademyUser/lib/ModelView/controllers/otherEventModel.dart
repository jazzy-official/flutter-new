import 'package:get/get.dart';

import 'db_controller.dart';

class OtherEventController extends GetxController {
  Rx<List<Map<String, dynamic>>> eventList = Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get event => eventList.value;
  @override
  Future<void> onInit() async {
    // await db.open();
    // String id = Get.find<AuthController>().firebaseUser.uid;
    // eventList.bindStream(
    //     DatabaseController().otherEventStream()); //id)); //Stream Comming from
    super.onInit();
  }
refreshData(){
eventList.bindStream(
        DatabaseController().otherEventStream()); //id));
}
  @override
  void onClose() {
    // db.close();
    super.onClose();
  }
}
