import 'package:daily_log/Controllers/daily_log_controller.dart';
import 'package:daily_log/Controllers/task_controller.dart';
import 'package:daily_log/Models/task_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

import '../Models/commit.dart';
import '../Models/daili_log_model.dart';
import '../Models/user_model.dart';

class EmployeeDetailController extends GetxController {
  String? employeeeid;
  String? employeeename;
  int number = 1;
  List<Commit> comments = [];
  List<Commit> foundcomments = [];
  List<TaskModel> tasks = [];
  final DatabaseReference databaseReference =
  FirebaseDatabase.instance.ref("Users");
  var logcont = Get.put(DailyLogController());
  var taskcont = Get.put(TaskController());
  @override
  Future<void> onInit() async {
    // TODO: implement onInit
    await getTasks();
    filtercomments();
    super.onInit();
  }
  Future<void> runFilter(String enteredKeyword,) async {
    if (enteredKeyword.isEmpty) {
      // if the search field is empty or only contains white-space, we'll display all users
      foundcomments = comments;
    } else {
      foundcomments = comments
          .where((user) => user.task
          .toUpperCase()
          .contains(enteredKeyword.toUpperCase()))
          .toList();
      // we use the toLowerCase() method to make it case-insensitive
    } // Refresh the UI
    update();
  }

  filtercomments() {
    comments = [];
    List<DailyLog> commitsofemployee = [];
    List<DailyLog> weeklycommitsofemployee = [];
    commitsofemployee =
        logcont.commits.where((element) => element.employeeid! == employeeeid).toList();
    weeklycommitsofemployee = commitsofemployee
        .where((element) =>
    element.time
        .isAfter(DateTime.now().subtract(Duration(days: 7 * number))) &&
        element.time.isBefore(
            DateTime.now().subtract(Duration(days: 7 * (number - 1)))))
        .toList();
    update();
    weeklycommitsofemployee.forEach((element) {
      element.firsthalf.forEach((element2) {
        comments.add(element2);
      });
      element.secondhalf.forEach((element3) {
        comments.add(element3);
      });
    });
    runFilter("");
    update();
  }

  Future<void> getTasks() async {
    await taskcont.readBall();
    tasks = taskcont.tasks.where((element) => element.assignto == employeeename).toList();
    update();
  }
  Future<void> refreshtask() async {
    await taskcont.readBall();
    getTasks();
    update();
  }
}