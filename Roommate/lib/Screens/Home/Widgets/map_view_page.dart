import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_sliding_up_panel/sliding_up_panel_widget.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_place_picker_mb/google_maps_place_picker.dart';
import 'package:ionicons/ionicons.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Models/location_model.dart';

class MapViewPage extends StatefulWidget {
  // final VoidCallback toggleView;

  const MapViewPage({Key? key,}) : super(key: key);

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(31.4504, 73.1350),
    zoom: 14.4746,
  );

  // static final CameraPosition _kLake = CameraPosition(
  //     bearing: 192.8334901395799,
  //     target: LatLng(37.43296265331129, -122.08832357078792),
  //     tilt: 59.440717697143555,
  //     zoom: 19.151926040649414);

  @override
  State<MapViewPage> createState() => _MapViewPageState();
}

class _MapViewPageState extends State<MapViewPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            child: PlacePicker(
              apiKey: "AIzaSyB4LHrclZAs5VwIs0iFNyFPcSvVX3_VjuA",   // Put YOUR OWN KEY here.
              onPlacePicked: (result) {
                print(result.adrAddress);
                Navigator.of(context).pop();
              },
              initialPosition: const LatLng(35.65565,76.45454),
              useCurrentLocation: true,
            ),
          ),
          Container(
            height: 70,
            decoration: BoxDecoration(
              border: Border(top: BorderSide(color: Colors.grey.shade300, width: 1)),
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(),
                  TextButton(
                      onPressed: () {},
                      child: Container(
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                            color: myResources.themeColor,
                            borderRadius: BorderRadius.circular(3)

                        ),
                        child: Text("Confirm",style: TextStyle(color: Colors.white),),
                      ))
                ],
              ),
            ),
          )
        ],
      ),
    );

  }

  // Future<void> _goToTheLake() async {
  //   final GoogleMapController controller = await _controller.future;
  //   controller.animateCamera(CameraUpdate.newCameraPosition(MapViewPage._kLake));
  // }
}
