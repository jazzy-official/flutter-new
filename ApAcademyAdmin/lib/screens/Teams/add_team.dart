import 'package:ap_academy_admin/ModelView/Controller/NotificationController.dart';
import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:ap_academy_admin/ModelView/Controller/matchController.dart';
import 'package:ap_academy_admin/ModelView/Controller/teamsController.dart';

import 'package:ap_academy_admin/screens/Teams/select_members.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mongo_dart/mongo_dart.dart' as mongo;


class AddTeam extends StatefulWidget {
  final Map<String, dynamic>? matchModel;

  const AddTeam({
    Key? key,
    this.matchModel,
  }) : super(key: key);
  @override
  _AddTeamState createState() => _AddTeamState();
}

class _AddTeamState extends State<AddTeam> {
  TextEditingController team1Controller = TextEditingController();
  TextEditingController team2Controller = TextEditingController();
  TextEditingController discriptionController = TextEditingController();
  final mController = Get.put(MatchController());
  final dbC = Get.put(DatabaseController());
  final teamController = Get.put(TeamController());
  //TextEditingController nameController = TextEditingController();

  String _selectedTypevalue = "";
  List<Map<String, dynamic>> _result1SelectMember = [];
  List<Map<String, dynamic>> _result2SelectMember = [];
  //List<dynamic> _memberIdList = [];
////////////////////////---------------------------------
  // setMembers() {
  //   _memberIdList = [];

  //   int count = 0;
  //   _resultSelectMember.forEach((mIds) {
  //     _memberIdList.add(mIds['_id']);

  //     count++;
  //   });
  // }
  bool validate() {
    if (team1Controller.text == '') {
      Get.back();
      Get.rawSnackbar(
        title: 'Error',
        message: 'Fill all fields and team 1 name.',
      );
      return false;
    } else if (team2Controller.text == '') {
      Get.back();
      Get.rawSnackbar(
        title: 'Error',
        message: 'Fill all fields and team 2 name.',
      );
      return false;
    } else if (_result1SelectMember.length == 0) {
      Get.back();
      Get.rawSnackbar(
        title: 'Error',
        message: 'Select members for Team 1 .',
      );
      return false;
    } else if (_result2SelectMember.length == 0) {
      Get.back();
      Get.rawSnackbar(
        title: 'Error',
        message: 'Select members for team 2.',
      );
      return false;

    // } else if (mController.selectedCoachValue.value == '') {
    //   Get.back();
    //   Get.rawSnackbar(
    //     title: 'Error',
    //     message: 'Select coach value.',
    //   );
    //   return false;
    // } else if (discriptionController.text == '') {
    //   Get.back();
    //   Get.rawSnackbar(
    //     title: 'Error',
    //     message: 'Fill all fields and team 1 name.',
    //   );
      //return false;
    } else
      return true;
  }

   checkDubliicate(List members) {
    bool isDouble = false;
    members.forEach((element1) {
      _result1SelectMember.forEach((element2) {
        if (element1['firebaseId'] == element2['firebaseId'])
          isDouble = true;
        else
          isDouble =  false;
      });
      _result2SelectMember.forEach((element2) {
        if (element1['firebaseId'] == element2['firebaseId'])
          isDouble = true;
        else
          isDouble = false;
      });
    });
    return isDouble;
  }

  populateData() {
    team1Controller.text = widget.matchModel!['team1name'];
    team2Controller.text = widget.matchModel!['team2name'];
  }

  @override
  void initState() {
    super.initState();
    populateData();
  }

  @override
  void dispose() {
    team1Controller.dispose();
    team2Controller.dispose();
    discriptionController.dispose();
    super.dispose();
  }

/////////////////////////////////---------------------------
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyResources.backgroundColor,
      appBar: AppBar(
        backgroundColor: MyResources.buttonColor,
        title: Container(
          child: Text(
            'CREATE TEAMS',
          ),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            //add team  and select members
widget.matchModel!["otherteam1"]==false?
            //Team Name Textfield
            Padding(
              padding: const EdgeInsets.only(top: 20.0, left: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  Text(
                    "Team "+widget.matchModel!['team1name'],
                    style: GoogleFonts.roboto(
                        fontSize: 22, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ):Container(),

            widget.matchModel!["otherteam1"]==false?
            Padding(
              padding: const EdgeInsets.only(top: 10.0, left: 20, right: 20),
              child: Container(
                width: Get.width * 0.38,
                height: 25,
                child: RaisedButton(
                  textColor: Colors.white,
                  color: MyResources.buttonColor,
                  child: Text(
                    "MEMBERS",
                  ),
                  onPressed: () async {
                    //   await teamController.getUsers().then((value) async {
                    var members = await Get.dialog(SelectMembers(
                        userModel: widget.matchModel!['attendance']));
                    if (members != null) {
                      if (!checkDubliicate(members)) {
                        members.forEach((ev) {
                          _result1SelectMember.add(ev);
                        });
                      } else {
                        Get.rawSnackbar(
                            title: 'Duplicate',
                            message: 'Duplicate members can not be selected');
                      }
                    }
                    //    });

                    //  setMembers();
                    // print(_memberIdList);
                    if (mounted) {
                      setState(() {});
                    }
                  },
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ):Container(),
            _result1SelectMember.length == 0
                ? Container()
                :widget.matchModel!["otherteam1"]==false?
            Padding(
                    padding: const EdgeInsets.only(right: 20.0, left: 20),
                    child: Container(
                      constraints: BoxConstraints(
                        maxHeight: 120,
                        minHeight: 50,
                      ),
                      child: ListView.builder(
                        itemCount: _result1SelectMember.length,
                        itemBuilder: (BuildContext context, int index) =>
                            getUserCard(
                                _result1SelectMember[index], index, true),
                      ),
                    ),
                  ):Container(),

            widget.matchModel!["otherteam2"]==false?
            Padding(
              padding: const EdgeInsets.only(top: 6.0, left: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Team "+widget.matchModel!['team2name'],
                    style: GoogleFonts.roboto(
                        fontSize: 22, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ):Container(),
            // widget.matchModel["otherteam2"]==false?
            // Padding(
            //   //Add padding around textfield
            //   padding: EdgeInsets.only(top: 7.0, left: 20, right: 20),
            //   child: Container(
            //     color: MyResources.backgroundColor,
            //     height: 50,
            //     child: TextField(
            //       controller: team2Controller,
            //       decoration: InputDecoration(
            //         //Add th Hint text here.
            //         border: OutlineInputBorder(
            //           borderRadius: BorderRadius.circular(10.0),
            //         ),
            //       ),
            //     ),
            //   ),
            // ):Container(),
            widget.matchModel!["otherteam2"]==false?
            Padding(
              padding: const EdgeInsets.only(top: 10.0, left: 20, right: 20),
              child: Container(
                width: Get.width * 0.38,
                height: 25,
                child: RaisedButton(
                  textColor: Colors.white,
                  color: MyResources.buttonColor,
                  child: Text(
                    "MEMBERS",
                  ),
                  onPressed: () async {
                    // List<String> selectedGroup = await showDialogGroupedCheckbox(
                    //     context: context,
                    //     cancelDialogText: "cancel",
                    //     isMultiSelection: true,
                    //     itemsTitle: List.generate(
                    //         widget.matchallModel['attendace'].length,
                    //         (index) =>
                    //             "${widget.matchallModel['attendace']['name']}"),
                    //     submitDialogText: "selected",
                    //     dialogTitle: Text("Select  Groups"),
                    //     values: List.generate(
                    //         widget.matchallModel['attendace'].length,
                    //         (index) => index));
                    // print(selectedGroup);

                    // await teamController.getUsers().then((value) async {
                    var members = await Get.dialog(SelectMembers(
                        userModel: widget.matchModel!['attendance']));
                    if (members != null) {
                      if (!checkDubliicate(members)) {
                        members.forEach((ev) {
                          _result2SelectMember.add(ev);
                        });
                      } else {
                        Get.rawSnackbar(
                            title: 'Duplicate',
                            message: 'Duplicate members can not be selected');
                      }
                    }
                    //  });

                    //  setMembers();
                    // print(_memberIdList);
                    if (mounted) {
                      setState(() {});
                    }
                  },
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ):Container(),
            _result2SelectMember.length == 0
                ? Container()
                :             widget.matchModel!["otherteam2"]==false?
            Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Container(
                      constraints:
                          BoxConstraints(maxHeight: 120, minHeight: 100),
                      child: ListView.builder(
                        itemCount: _result2SelectMember.length,
                        itemBuilder: (BuildContext context, int index) =>
                            getUserCard(
                                _result2SelectMember[index], index, false),
                      ),
                    ),
                  ):Container(),

            // Padding(
            //   padding: const EdgeInsets.only(top: 7.0, right: 20, left: 20),
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //     crossAxisAlignment: CrossAxisAlignment.start,
            //     children: [
            //       Text(
            //         "  Incharge Coach:",
            //         style: GoogleFonts.roboto(
            //             fontSize: 15, fontWeight: FontWeight.w400),
            //       ),
            //     ],
            //   ),
            // ),
            // Padding(
            //   //Add padding around textfield
            //   padding: EdgeInsets.only(top: 7.0, right: 20, left: 20),
            //   child: Padding(
            //     padding: const EdgeInsets.all(0.0),
            //     child: Container(
            //       decoration: BoxDecoration(
            //           color: MyResources.backgroundColor,
            //           border: Border.all(color: Colors.grey),
            //           borderRadius: BorderRadius.circular(10)),
            //       //  color: MyResources.backgroundColor,
            //
            //       height: 50,
            //       child: Padding(
            //           padding: const EdgeInsets.all(12.0),
            //           child: Obx(
            //             () => DropdownButton(
            //               isExpanded: true,
            //               underline: Text(""),
            //               value: mController.selectedCoachValue.value,
            //               // focusColor: Colors.blue,
            //               elevation: 12,
            //               items: mController.coachdropValues.map(
            //                 (map) {
            //                   return DropdownMenuItem(
            //                     child: Text(map['name']),
            //                     value: map['value'],
            //                   );
            //                 },
            //               ).toList(),
            //               onChanged: (value) {
            //                 FocusScope.of(context)
            //                     .requestFocus(new FocusNode());
            //                 print(value);
            //                 setState(() {
            //                   mController.selectedCoachValue.value = value;
            //                 });
            //               },
            //               hint: Text('Select Item'),
            //             ),
            //           )),
            //     ),
            //   ),
            // ),
            // //Date and time

            /////////////////////////////Date time/////////////////
            // Padding(
            //   padding: const EdgeInsets.only(top: 10.0, left: 18),
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //     crossAxisAlignment: CrossAxisAlignment.start,
            //     children: [
            //       Text(
            //         " Description:",
            //         style: GoogleFonts.roboto(
            //             fontSize: 15, fontWeight: FontWeight.w400),
            //       ),
            //     ],
            //   ),
            // ),
            // Padding(
            //   //Add padding around textfield
            //   padding: EdgeInsets.only(top: 7.0, left: 20, right: 20),
            //   child: Container(
            //     height: 120,
            //     color: MyResources.backgroundColor,
            //     child: TextField(
            //         controller: discriptionController,
            //         keyboardType: TextInputType.multiline,
            //         maxLines: 5,
            //         decoration: MyResources.textFieldBorder),
            //   ),
            // ),
            ////////////////////////submit///

            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10,vertical: 30),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.7,
                height: 35,
                child: RaisedButton(
                  textColor: Colors.white,
                  color: MyResources.buttonColor,
                  child: Text(
                    "SUBMIT",
                  ),
                  onPressed: () async {
                    Get.dialog(Container(
                      height: 200,
                      child: LoadingWidget(),
                    ));
                    if (validate()) {
                      mongo.ObjectId tid1=mongo.ObjectId();
                      mongo.ObjectId tid2=mongo.ObjectId();
                      Map<String, dynamic> nTeam1 = {
                        '_id':  tid1,

                        'teamName': team1Controller.text,

                        // 'teamType': _selectedTypevalue,
                        'teamMembers': _result1SelectMember,
                        // 'coach': mController.selectedCoachValue.value,
                        // 'discription': discriptionController.text,
                        'CreatedTime': DateTime.now()
                      };
                      Map<String, dynamic> nTeam2 = {
                        //  'team1Name': team1Controller.text,
                        '_id':  tid2,
                        'teamName': team2Controller.text,
                        // 'teamType': _selectedTypevalue,
                        'teamMembers': _result2SelectMember,
                        // 'coach': mController.selectedCoachValue.value,
                        // 'discription': discriptionController.text,
                        'CreatedTime': DateTime.now()
                      };
                      Map<String, dynamic> notifyModel = {
                        'title': 'Match: '+widget.matchModel!['match'],
                        'description': 'New members are selected for this match.Please check match details!',
                        'timeCreated': DateTime.now()
                      };
                      await Get.find<DatabaseController>()
                          .addNotifications(notifyModel);
                      await teamController.submitTeams(nTeam2);
                      await teamController.submitTeams(nTeam1);
                      List<String> membersTokenList = [];
                      _result2SelectMember.forEach((element) {
                        membersTokenList.add(element['token']);
                      });
                      _result1SelectMember.forEach((element) {
                        membersTokenList.add(element['token']);
                      });
                      await teamController.approveTeams(
                          widget.matchModel!['_id'],
                          _result1SelectMember,
                          _result2SelectMember,tid1.toHexString(),tid2.toHexString());
                      await Get.put(NotificationController())
                          .sendAndRetrieveMessage({
                        'title':'Match: '+ widget.matchModel!['match'],
                        'desc': 'New members are selected for match',
                      }, membersTokenList);
                      Get.back();
                      Get.defaultDialog(
                        title: 'Saved',
                        barrierDismissible: false,
                        content: Text(
                            '${team1Controller.text} and ${team2Controller.text} is saved'),
                        textConfirm: "Ok",
                        buttonColor: MyResources.buttonColor,
                        onConfirm: () {
                          team1Controller.clear();
                          team2Controller.clear();
                          teamController.refreshTeams();
                          mController.refreshDate();
                          Get.back();
                          Get.back();
                        },
                      );
                    }
                  },
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getUserCard(Map<String, dynamic> userModel, int ind, bool team) {
    return Card(
      child: Container(
        height: 50,
        width: Get.width,
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            SizedBox(
              width: 10,
            ),
            Text(
              userModel['name'],
              style: TextStyle(color: MyResources.coachHeading, fontSize: 16),
            ),
            IconButton(
                icon: Icon(
                  Icons.delete,
                  color: Colors.red,
                ),
                onPressed: () {
                  team ? delet1User(ind) : delet2User(ind);
                })
          ]),
        ),
      ),
    );
  }

  delet1User(int i) {
    _result1SelectMember.removeAt(i);
    if (mounted) setState(() {});
  }

  delet2User(int i) {
    _result2SelectMember.removeAt(i);
    if (mounted) setState(() {});
  }
}
