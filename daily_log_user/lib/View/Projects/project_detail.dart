import 'dart:io';

import 'package:daily_log_user/Controllers/projectscontroller.dart';
import 'package:daily_log_user/View/delete_popup.dart';
import 'package:daily_log_user/loading_widget.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:tree_view/tree_view.dart';
import 'package:path/path.dart' as path;
import 'package:url_launcher/url_launcher.dart';
import '../../Models/project_model.dart';
import '../../constants/util.dart';

class ProjectDetail extends StatefulWidget {
  final ProjectModel project;
  const ProjectDetail({Key? key, required this.project}) : super(key: key);

  @override
  State<ProjectDetail> createState() => _ProjectDetailState();
}

class _ProjectDetailState extends State<ProjectDetail> {
  var projectcont = Get.put(ProjectController());
  TextEditingController urlCont = TextEditingController();
  void _getClipboardText() async {
    final clipboardData = await Clipboard.getData(Clipboard.kTextPlain);
    setState(() {
      urlCont.text = clipboardData?.text ?? "";
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.project.name),
        centerTitle: true,
        backgroundColor: MyResources.themecolor,
      ),
      body: TreeView(
        startExpanded: false,
        children: [
          for (var i in widget.project.modules)
            TreeViewChild(
              parent: Card(
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8.0, top: 8),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  " " + i.name.toUpperCase(),
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16),
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    const Text(" Status : "),
                                    Text(
                                      i.status ? "Completed" : "Pending",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          color: i.status
                                              ? Colors.green
                                              : Colors.red),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    i.submodules!.isNotEmpty
                        ? const Icon(
                      CupertinoIcons.chevron_down,
                      size: 16,
                    )
                        : SizedBox(
                      height: 10,
                    )
                  ],
                ),
              ),
              children: [
                for (var j in i.submodules!)
                  Padding(
                    padding: const EdgeInsets.only(left: 18.0),
                    child: Row(
                      children: [
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  " " + j.name.toUpperCase(),
                                  style: const TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16),
                                ),
                                Row(
                                  children: [
                                    const Text(
                                      " Status : ",
                                      style: TextStyle(fontSize: 12),
                                    ),
                                    Transform.scale(
                                      scale: 0.7,
                                      child: CupertinoSwitch(
                                          value: j.status,
                                          onChanged: (val) async {
                                            setState(() {
                                              j.status = val;
                                            });
                                            await projectcont.updateSubModule(
                                                j, widget.project.id!, i.id!);
                                            if (i.submodules!.any((element) =>
                                            element.status == false)) {
                                              setState(() {
                                                i.status = false;
                                              });
                                            } else {
                                              setState(() {
                                                i.status = true;
                                              });
                                            }
                                            if (i.submodules!.isEmpty) {
                                              setState(() {
                                                i.status = true;
                                              });
                                            }
                                            // await projectcont.updateModule(i, widget.project.id!);
                                          }),
                                    )
                                  ],
                                ),
                                if (j.doc != null)
                                  SizedBox(
                                    width: 280,
                                    child: Align(
                                      alignment: Alignment.topLeft,
                                      child: InkWell(

                                          onTap: () => launch(j.doc!),

                                          child: Text(
                                            j.doc!,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(color: Colors.blue),
                                          )),
                                    ),
                                  )
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  primary: MyResources.themecolor,
                                  padding: const EdgeInsets.all(5)),
                              onPressed: () async {
                                showModalBottomSheet(
                                    isScrollControlled: true,
                                    context: context,
                                    builder: (context) {
                                      return Padding(
                                        padding:
                                        MediaQuery.of(context).viewInsets,
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment
                                                  .spaceBetween,
                                              children: [
                                                InkWell(
                                                  onTap: () {
                                                    urlCont.clear();
                                                    Get.back();
                                                  },
                                                  child: Padding(
                                                    padding:
                                                    EdgeInsets.all(8.0),
                                                    child: Text(
                                                      "Cancel",
                                                      style: TextStyle(
                                                          color: Colors.blue,
                                                          fontWeight:
                                                          FontWeight.bold),
                                                    ),
                                                  ),
                                                ),
                                                InkWell(
                                                  onTap: () {
                                                    _getClipboardText();
                                                  },
                                                  child: Padding(
                                                    padding:
                                                    EdgeInsets.all(8.0),
                                                    child: Text(
                                                      "Paste",
                                                      style: TextStyle(
                                                          color: Colors.blue,
                                                          fontWeight:
                                                          FontWeight.bold),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Padding(
                                              padding:
                                              const EdgeInsets.all(8.0),
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    child: SizedBox(
                                                      height: 40,
                                                      child: TextField(
                                                        maxLines: 1,
                                                        controller: urlCont,
                                                        decoration:
                                                        inputdecoration(
                                                            "Paste url",
                                                            null),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                              const EdgeInsets.symmetric(
                                                  horizontal: 8.0),
                                              child: ElevatedButton(
                                                  onPressed: () async {
                                                    setState(() {
                                                      j.doc = urlCont.text;
                                                    });
                                                    await projectcont
                                                        .updateSubModule(
                                                        j,
                                                        widget.project.id!,
                                                        i.id!);
                                                    urlCont.clear();
                                                    Get.back();
                                                  },
                                                  style:
                                                  ElevatedButton.styleFrom(
                                                      primary: MyResources
                                                          .themecolor),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .center,
                                                    children: const [
                                                      Text("Save"),
                                                    ],
                                                  )),
                                            ),
                                          ],
                                        ),
                                      );
                                    });
                              },
                              child: const Text("Add Doc")),
                        )
                      ],
                    ),
                  )
              ],
            ),
        ],
      ),
    );
  }
}
