import 'package:ap_academy_new/ModelView/controllers/auth_controller.dart';
import 'package:ap_academy_new/Screens/Authentication/sign_up_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/get_utils/src/extensions/string_extensions.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ap_academy_new/Constants/util.dart';

import 'forget_password.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  bool _autoValidate = false;

  /////////////////////////////////////GetX Controller///////////////////////////////////
  final authController = Get.put(AuthController());

  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  /////////////////////////////////Controllers////////////////////////////////////////////////////////
  TextEditingController _emailControllerlogin = TextEditingController();

  TextEditingController _passControllerlogin = TextEditingController();

///////////////////////////////////Pass Visibility//////////////////////////////
  bool _passwordVisible1 = true;

  /////////////////////////////Validator

  bool validator = true;

///////////////////////////////////Firebase Auth////////////////////////////////////////////////////////

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: myResources.backgroundColor,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            autovalidate: _autoValidate,
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 20, bottom: 0),
                  child: Container(
                    child: Image.asset(
                      "assets/logo.png",
                      width: 150,
                      height: 200,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 0.0),
                  child: Center(
                    child: Text(
                      "Welcome Back!",
                      style: myResources.appHeadingStyle,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: Center(
                    child: Text(
                      "Login to your existing account ",
                      style: myResources.appTextStyle,
                    ),
                  ),
                ),
                Padding(
                  //Add padding around textfield
                  padding: EdgeInsets.only(top: 15.0, left: 10, right: 10),
                  child: TextFormField(
                    controller: _emailControllerlogin,
                    keyboardType: TextInputType.emailAddress,
                    validator: (val) {
                      if (val!.length == 0)
                        return "Please enter email";
                      else if (!val.contains("@"))
                        return "Please enter valid email";
                      else
                        return null;
                    },
                    decoration: InputDecoration(
                      //Add th Hint text here.
                      hintText: "Email",
                      hintStyle: myResources.hintfontStyle,
                      prefixIcon: Icon(Icons.email),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                ),
                Padding(
                  //Add padding around textfield
                  padding: EdgeInsets.only(top: 15.0, left: 10, right: 10),
                  child: TextFormField(
                    controller: _passControllerlogin,
                    validator: (val) {
                      if (val!.length == 0) {
                        return ('Please enter password');
                      } else if (val.length <= 7) {
                        return ('your password must be atleast 8 chracters ');
                      }
                      return null;
                    },
                    obscureText: _passwordVisible1,
                    decoration: InputDecoration(
                      //Add th Hint text here.
                      hintText: "Password",
                      suffixIcon: IconButton(
                          icon: Icon(
                            // Based on passwordVisible state choose the icon
                            _passwordVisible1
                                ? Icons.visibility_off_outlined
                                : Icons.visibility_outlined,
                          ),
                          onPressed: () {
                            _passwordVisible1
                                ? _passwordVisible1 = false
                                : _passwordVisible1 = true;
                            setState(() {});
                            // Update the state i.e. toogle the state of passwordVisible variable
                          }),

                      hintStyle: myResources.hintfontStyle,
                      prefixIcon: Icon(Icons.lock),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    Get.to(Forgetpassword());
                  },
                  child: const Padding(
                    padding: EdgeInsets.only(top: 10.0, left: 200.0),
                    child: Center(
                      child: Text(
                        "Forgot Password?",
                        style: TextStyle(
                          decoration: TextDecoration.underline,
                          height: 1.5,
                          fontSize: 14,
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 15.0, left: 10, right: 10),
                  child: Container(
                    width: Get.width * 0.9,
                    height: 40,
                    child: RaisedButton(
                      textColor: Colors.white,
                      color: myResources.buttonColor,
                      child: Text(
                        "Login",
                      ),
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          await authController.signInwithEmail(
                              _emailControllerlogin.text,
                              _passControllerlogin.text);
                        }
                      },
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                ),
                // Padding(
                //   padding: EdgeInsets.only(top: 15.0, left: 10, right: 10),
                //   child: Container(
                //     width: Get.width * 0.9,
                //     height: 40,
                //     child: RaisedButton(
                //       textColor: Colors.white,
                //       color: myResources.buttonColor,
                //       child: Text(
                //         "Login as Parent",
                //       ),
                //       onPressed: () async {
                //         // if (_formKey.currentState!.validate()) {
                //         //   await authController.signInwithEmail(
                //         //       _emailControllerlogin.text,
                //         //       _passControllerlogin.text);
                //         // }
                //         // await authController.getsheetfun();
                //       },
                //       shape: new RoundedRectangleBorder(
                //         borderRadius: new BorderRadius.circular(10.0),
                //       ),
                //     ),
                //   ),
                // ),
                // Padding(
                //   padding: const EdgeInsets.only(top: 15.0),
                //   child: Center(
                //     child: Column(
                //       mainAxisSize: MainAxisSize.min,
                //       mainAxisAlignment: MainAxisAlignment.center,
                //       children: const [
                //         // Container(
                //         //   width: Get.width * 0.8,
                //         //   child: RaisedButton(
                //         //     textColor: Colors.white,
                //         //     color: Color(0xff3A5998),
                //         //     child: Row(
                //         //       mainAxisAlignment: MainAxisAlignment.center,
                //         //       children: [
                //         //         Padding(
                //         //           padding: const EdgeInsets.only(left: 12.0),
                //         //           child: Image.asset(
                //         //             "assets/facebook.png",
                //         //             width: 20,
                //         //             height: 20,
                //         //           ),
                //         //         ),
                //         //         Padding(
                //         //           padding: const EdgeInsets.only(left: 16.0),
                //         //           child: Text(
                //         //             "Continue with Facebook",
                //         //           ),
                //         //         ),
                //         //       ],
                //         //     ),
                //         //     onPressed: () {
                //         //       authController.signInWithFacebook();
                //         //     },
                //         //     shape: new RoundedRectangleBorder(
                //         //       borderRadius: new BorderRadius.circular(10.0),
                //         //     ),
                //         //   ),
                //         // ),
                //
                //         // SizedBox(
                //         //   width: 10,
                //         // ),
                //         // Container(
                //         //   width: MediaQuery.of(context).size.width * 0.8,
                //         //   child: RaisedButton(
                //         //     textColor: Colors.white,
                //         //     color: Colors.red,
                //         //     child: Row(
                //         //       mainAxisAlignment: MainAxisAlignment.center,
                //         //       children: [
                //         //         Padding(
                //         //           padding: const EdgeInsets.only(
                //         //             right: 12.0,
                //         //           ),
                //         //           child: Image.asset(
                //         //             "assets/google.png",
                //         //             width: 20,
                //         //             height: 16,
                //         //           ),
                //         //         ),
                //         //         Text(
                //         //           "Continue with Google",
                //         //         ),
                //         //       ],
                //         //     ),
                //         //     onPressed: () {
                //         //       authController.signInWithGoogle();
                //         //     },
                //         //     shape: new RoundedRectangleBorder(
                //         //       borderRadius: new BorderRadius.circular(10.0),
                //         //     ),
                //         //   ),
                //         // ),
                //
                //       ],
                //     ),
                //   ),
                // ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Don't have an Account?",
                          style: myResources.appTextStyle,
                        ),
                        MaterialButton(
                            onPressed: () {
                              Get.to(SignupPage());
                            },
                            child: Text(
                              " Sign Up",
                              style: myResources.linkStyle,
                            ))
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}