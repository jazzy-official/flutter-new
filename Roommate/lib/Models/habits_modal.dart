class Habits {
  final String foodChoice ;
  final String smoking ;
  final String party ;
  final String guests ;
  final String pets ;

  Habits( this.smoking,  this.foodChoice, this.party, this.guests, this.pets);

  Habits.fromJson(Map<String, dynamic> json)
      : smoking = json['name'],
        party = json['name'],
        guests = json['name'],
        pets = json['name'],
        foodChoice = json['email'];

  Map<String, dynamic> toJson() => {
    'name': smoking,
    'email': foodChoice,
  };
}