import 'package:ap_academy_admin/constants/util.dart';
import 'package:ap_academy_admin/screens/Coaches/AddAdminPage.dart';
import 'package:ap_academy_admin/screens/Coaches/coachList.dart';
import 'package:ap_academy_admin/screens/Setting/profile_edit.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'adminList.dart';

class AdminPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: MyResources.buttonColor,
            toolbarHeight: 10,
            bottom: TabBar(tabs: [
              Tab(
                text: 'COACHES',
              ),
              Tab(
                text: 'ADMINS',
              ),
            ]),
          ),
          body: TabBarView(
            children: [CoachList(), AdminList()],
          ),
        ));
  }
}
