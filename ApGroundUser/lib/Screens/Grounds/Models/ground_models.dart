
import 'package:ap_booking_user/Screens/Grounds/Models/template_model.dart';

class Ground {
  final String? id;
  final String name;
  final String address;
  final List<String> imgUrls;
  final double mealPrice;
  List<Template> schedule;

  Ground({this.id,required this.name, required this.address,required this.imgUrls,required this.schedule,required this.mealPrice});



  factory Ground.fromJson(dynamic json, key) {
    return Ground(
      id: key.toString(),
      name : json['name'],
      address : json['address'],
      imgUrls: json['imgUrls'] != null ? List<String>.from(json['imgUrls'].map((dynamic item) => item.toString(),).toList()):[''],
      schedule: List<Template>.from(json["schedule"].map((dynamic item) => Template.fromJson(item,""),).toList()),
      mealPrice: json['mealPrice'].toDouble(),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = name;
    data['address'] = address;
    data['imgUrls'] = imgUrls;
    data['mealPrice'] = mealPrice;
    data['schedule'] = List<dynamic>.from(schedule.map((x) => x.toJson()));
    return data;
  }
}
