class UserModel {
  final String? id;
  final String displayName;
  final String email;
  final String phone;
  final String? photo;
  final String? designation;
  final String role;
  final bool isActive;
  final bool? approved;

  const UserModel(
      {required this.role,
        required this.isActive,
      this.id,
      required this.displayName,
      required this.email,
      required this.phone,
      this.photo,
      this.designation,
      this.approved});
}
