class Expense {
  int accId;
  String accountno;
  String accounttitle;
  String? accountcategory;
  String accountdes;
  String? balancetype;
  bool tax;
  int heading;
  double? openingbalance;
  double currentbalance;
  int? projectId;

  Expense({
    required this.accId,
    required this.accountno,
    required this.accounttitle,
    this.accountcategory,
    required this.accountdes,
    this.balancetype,
    required this.tax,
    required this.heading,
    this.openingbalance,
    required this.currentbalance,
    this.projectId,
  });

  factory Expense.fromJson(Map<String, dynamic> json) {
    return Expense(
      accId: json['Accid'] as int,
      accountno: json['Accountno'] as String,
      accountdes: json['Accountdes'] as String,
      tax: json['Tax'] as bool,
      projectId:  json['ProjectId'],
      accountcategory:json['Accountcategory'],
      heading: json['Heading'] as int,
      balancetype: json['Balancetype'],
      currentbalance:  json['CurrentBalance'] as double,
      accounttitle:json['Accounttitle'] as String,
      openingbalance: json['OpeningBalance'],
    );
  }
}
