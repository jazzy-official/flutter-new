import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class myResources {
  static Color backgroundColor = Color(0xfff5f5f5);
  static Color appBarActionsColor = Colors.white;
  static const Color headingColor = Colors.orange;
  static Color coachHeading = Color(0xff3c4da9);
  static Color drawerIconColor = Color(0xff1758B4);
  static Color buttonColor = Color(0xff1758B4);
  static Color sideBarColor = Color(0xfff5f5f5);
  static Color buttontextColor = Color(0xffebebeb);
  static Color hintColor = Color(0xffd2d2d2);
  static TextStyle vsStyle = GoogleFonts.monoton(
    color: Colors.red,
    fontSize: 14,
    fontWeight: FontWeight.w500,
  );
  static TextStyle appTextStyle = TextStyle(
    fontSize: 14,
    color: Colors.black,
    // fontWeight: FontWeight.bold,
  );
  static TextStyle titleTextStyle = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.bold,
    color: Colors.black,
  );
  static TextStyle appHeadingStyle = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.bold,
  );
  static TextStyle timestampStyle = TextStyle(
    fontSize: 12,
    color: Colors.black54,
  );

  static TextStyle modelHeadingStyle = TextStyle(
    fontSize: 20,
    color: Colors.white,
    fontWeight: FontWeight.w700,
  );

  static TextStyle linkStyle = TextStyle(fontSize: 14, color: Colors.blue);

  static TextStyle hintfontStyle = TextStyle(fontSize: 14, color: hintColor);
  static TextStyle textStyleprofile = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w500,
  );
  static TextStyle myTextStyle = TextStyle(
    fontSize: 16,
    // fontWeight: FontWeight.w700,
  );
}
