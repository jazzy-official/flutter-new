import 'package:ap_ground_admin/Screens/Attendance/Models/attendance_model.dart';
import 'package:ap_ground_admin/Screens/Attendance/Models/entry_model.dart';
import 'package:ap_ground_admin/Screens/Staff/Models/staff_model.dart';
import 'package:ap_ground_admin/Screens/Staff/staff_card.dart';
import 'package:ap_ground_admin/controllers/attendance_controller.dart';
import 'package:ap_ground_admin/controllers/designation_controller.dart';
import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:ap_ground_admin/controllers/staff_controller.dart';
import 'package:ap_ground_admin/loading_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ionicons/ionicons.dart';

class AttendanceScreen extends StatefulWidget {
  AttendanceScreen({Key? key}) : super(key: key);
  @override
  State<AttendanceScreen> createState() => _AttendanceScreenState();
}

class _AttendanceScreenState extends State<AttendanceScreen> {
  late List<Staff> dbstaff;
  List<Entry> _foundResult = [];
  List<Entry> Result1 = [];
  List<String> designations = ["All"];
  List<Entry> attendance=[];
  DateTime _chosenDateTime = DateTime.now();
  bool isDisable = true;
  late List<Attendance> attendancelist;
  String search = "";
  bool isLoading = true;
  void refresh() async {
    setState(() {
      isLoading = true;
    });
    setState(() {
      designations = ["All"];
    });
    dbstaff = await StaffController().readStaff();
    attendancelist = await AttendanceController().readAttendance();

    List<Map<String, String>> desg =
        await DesignationController().readDesignation();
    desg.forEach((element) {
      designations.add(element.values.toString());
    });
    await _rundatecheck();
    _runDesgFilter("");
    setState(() {
      isLoading = false;
    });
  }

  void _runFilter(String enteredKeyword) {
    List<Entry> results = [];
    if (enteredKeyword.isEmpty) {
      // if the search field is empty or only contains white-space, we'll display all users
      results = Result1;
    } else {
      results = Result1.where((user) =>
              user.name.toUpperCase().contains(enteredKeyword.toUpperCase()))
          .toList();
      // we use the toLowerCase() method to make it case-insensitive
    }

    // Refresh the UI
    setState(() {
      _foundResult = results;
    });
  }

  void _runDesgFilter(String enteredKeyword) {
    List<Entry> results = [];
    if (enteredKeyword == "All" || enteredKeyword == "") {
      // if the search field is empty or only contains white-space, we'll display all users
      results = attendance;
    } else {
      results = attendance
          .where((user) =>
              user.designation ==
              enteredKeyword.substring(1, enteredKeyword.length - 1))
          .toList();
      // we use the toLowerCase() method to make it case-insensitive
    }

    // Refresh the UI
    setState(() {
      Result1 = results;
    });
    _runFilter(search);
  }

  Future<void> _rundatecheck() async {
    List<Entry> att= [];
    if(attendancelist.isEmpty) {
      dbstaff.forEach((element) {att.add(Entry(name: element.name, designation: element.designation, isPresent: false, staffId: element.id!));});
      setState(() {
        attendance = att;
      });
    }else if( DateFormat('dd-MMM-yyyy').format(DateTime.parse(attendancelist.last.date))==DateFormat('dd-MMM-yyyy').format(DateTime.now())){
      setState(() {
        att = attendancelist.last.entries;
        attendance=att;
      });
    }else {
      dbstaff.forEach((element) {att.add(Entry(name: element.name, designation: element.designation, isPresent: false, staffId: element.id!));});
      setState(() {
        attendance = att;
      });
    };


  }

  void rundatefilter(DateTime date){
    if(DateFormat('dd-MMM-yyyy').format(date)==DateFormat('dd-MMM-yyyy').format(DateTime.now())){
      _rundatecheck();
    }else{
      int index = attendancelist.indexWhere((element) => DateFormat('dd-MMM-yyyy').format(DateTime.parse(element.date))==DateFormat('dd-MMM-yyyy').format(date));
      setState(() {
        attendance = index == -1 ? [] : attendancelist[index].entries;
      });
      print(attendancelist[3].date);
    }
  }

  String dropdownValue = "All";
  DateTime tempDateTime = DateTime.now();
  void _showDatePicker(ctx) {
    showCupertinoModalPopup(
        context: ctx,
        builder: (_) => Container(
          height: 353,
          color: Color.fromARGB(255, 255, 255, 255),
          child: Column(
            children: [
              Container(
                height: 300,
                child: CupertinoDatePicker(
                    initialDateTime: _chosenDateTime,
                    mode: CupertinoDatePickerMode.date,
                    onDateTimeChanged: (val) {
                        setState(() {
                          tempDateTime = val;
                        });


                      print(_chosenDateTime);
                    }),
              ),

              // Close the modal
              CupertinoButton(
                child: Text('OK'),
                onPressed: () async{
                  if(tempDateTime.isAfter(DateTime.now())){
                  displayToast("Can't Select this date");
                }else{
                  setState(() {
                    _chosenDateTime = tempDateTime;
                    isDisable = DateFormat('dd-MMM-yyyy').format(_chosenDateTime) == DateFormat('dd-MMM-yyyy').format(DateTime.now());
                  });
                  rundatefilter(_chosenDateTime);
                  _runDesgFilter("");

                } Navigator.of(ctx).pop();}
              )
            ],
          ),
        ));
  }
  @override
  void initState() {
    refresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:  isLoading
          ? Center(child: LoadingWidget())
          :  Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.symmetric(horizontal: 12),
                    height: 45,
                    child: TextFormField(
                      keyboardType: TextInputType.name,
                      onChanged: (val) {
                        setState(() {
                          search = val;
                        });
                        _runFilter(search);
                      },
                      onSaved: (String? val) {}, // ame Controller
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.only(left: 10),
                        //Add th Hint text here.
                        hintText: "Search",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 10, top: 1),
                  child: DropdownButton<String>(
                    value: dropdownValue,
                    icon: const Icon(Ionicons.chevron_down),
                    iconSize: 18,
                    elevation: 10,
                    style: const TextStyle(color: Colors.deepPurple),
                    // underline: Container(
                    //   height: 2,
                    //   color: Colors.deepPurpleAccent,
                    // ),
                    onChanged: (String? newValue) {
                      _runDesgFilter(newValue!);
                      setState(() {
                        dropdownValue = newValue;
                      });
                    },
                    items: designations
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 12.0),
                  child: ElevatedButton(
                    onPressed: !isDisable? null :  () async {

                      if(DateFormat('dd-MMM-yyyy').format(DateTime.parse(attendancelist.last.date))==DateFormat('dd-MMM-yyyy').format(DateTime.now())){
                        await AttendanceController().updateAttendance(Attendance(date: DateTime.now().toString(), entries: attendance,id: attendancelist.last.id));
                        refresh();
                      }else{
                        await AttendanceController().saveAttendance(Attendance(date: _chosenDateTime.toString(), entries: attendance));
                        refresh();
                      }

                    },
                    child: Text("Submit"),
                    style: ElevatedButton.styleFrom(primary: Color(0xff1758B4) ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: ElevatedButton(
                    onPressed: ()  {
                      _showDatePicker(context);
                    },
                    child: Row(
                      children: [
                        Icon(Ionicons.calendar_clear_outline,color: Colors.black,size: 22,),
                        SizedBox(width: 8,),
                        Text(DateFormat('dd-MMM-yyyy').format(_chosenDateTime),style: TextStyle(color: Colors.black),),
                      ],
                    ),
                    style: ElevatedButton.styleFrom(primary: Color(0xfff1f6f8)),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child:attendance.length==0? Center(child: Text("No Result Found"),) :
            ListView.builder(
                physics: BouncingScrollPhysics(
                    parent: AlwaysScrollableScrollPhysics()),
                padding: const EdgeInsets.all(8),
                itemCount: _foundResult.length,
                itemBuilder: (BuildContext context, int index) {
                  Entry staf = _foundResult[index];
                  return Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          Expanded(
                            child: Column(
                              mainAxisAlignment:
                              MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  staf.name,
                                  style: const TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600),
                                ),
                                Text(
                                  "(" + staf.designation + ")",
                                  style: const TextStyle(fontSize: 12),
                                ),
                              ],
                            ),
                          ),
                          Checkbox(value: staf.isPresent, onChanged: !isDisable ? null : (val) {
                            setState(() {
                              staf.isPresent = val!;
                            });
                          },activeColor: Colors.green,)
                        ],
                      ),
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }
}
