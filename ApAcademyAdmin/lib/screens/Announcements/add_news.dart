import 'package:ap_academy_admin/ModelView/Controller/NotificationController.dart';
import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:ap_academy_admin/ModelView/Controller/newsController.dart';
import 'package:ap_academy_admin/Models/NewsModel.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../constants/util.dart';
import '../shared/loading_widget.dart';
import 'Notifications_screen.dart';

class AddNews extends StatefulWidget {
  @override
  _AddNewsState createState() => _AddNewsState();
}

class _AddNewsState extends State<AddNews> {
  final dbHelper = Get.put(DatabaseController());
  final nController = Get.put(NewsController());
  final notifyController = Get.put(NotificationController());
  // String _value = "";
/////////////////////////////////////////////////--------------controllers------------///////////
  TextEditingController titleController = TextEditingController();
  TextEditingController desController = TextEditingController();
////////////////////////////////////////////////---------------Funcs----------////////////
  Future<void> submitNews() async {
    Map<String, dynamic> newsModel = {
      'title': titleController.text,
      // 'type': _value.toString(),
      'desc': desController.text,
      'timeCreated': DateTime.now()
    };
    Get.defaultDialog(title: '', content: const LoadingWidget(),backgroundColor: Colors.transparent);
    List<String> allTokens = [];
    allTokens = (await dbHelper.getAllTokens())!;
    bool temp = await dbHelper.addNews(newsModel);

    if (temp) {
      await notifyController.sendAndRetrieveMessage(
          {"title": titleController.text, 'disc': desController.text}, allTokens);
      Get.back();
      Get.back();
      Get.back();
      Get.snackbar("Sent", "Data is saved",
          snackPosition: SnackPosition.BOTTOM);
      nController.refreshDate();

    } else {
      Get.dialog(AlertDialog(
          title: Text("Not saved"), content: Text("Save again Please")));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyResources.backgroundColor,
      appBar: AppBar(
        backgroundColor: MyResources.buttonColor,
        title: Container(
          child: Text(
            'ADD NEWS',
            style: GoogleFonts.roboto(),
          ),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            //title of news
            Padding(
              padding: const EdgeInsets.only(top: 15.0, left: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "  Title:",
                    style: MyResources.appTextStyle,
                  ),
                ],
              ),
            ),
            Padding(
              //Add padding around textfield
              padding: EdgeInsets.only(top: 7.0, left: 20, right: 20),
              child: Container(
                height: 50,
                child: TextField(
                  controller: titleController,
                  decoration: InputDecoration(
                    fillColor: Colors.white,
                    //Add th Hint text here.
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
            //event category
            // Padding(
            //   padding: const EdgeInsets.only(top: 10.0, left: 15),
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //     crossAxisAlignment: CrossAxisAlignment.start,
            //     children: [
            //       Text(
            //         " Type:",
            //         style: MyResources.appTextStyle,
            //       ),
            //     ],
            //   ),
            // ),
            // Padding(
            //   //Add padding around textfield
            //   padding: EdgeInsets.only(top: 7.0, left: 20, right: 20),
            //   child: Padding(
            //     padding: const EdgeInsets.all(0.0),
            //     child: Container(
            //       decoration: BoxDecoration(
            //           color: MyResources.backgroundColor,
            //           border: Border.all(color: Colors.grey),
            //           borderRadius: BorderRadius.circular(10)),
            //       width: Get.width,
            //       height: 50,
            //       child: Padding(
            //         padding: const EdgeInsets.all(12.0),
            //         child: DropdownButton<String>(
            //           isExpanded: true,
            //           underline: Text(""),
            //           value: _value,
            //           //focusColor: MyResources.buttonColor,
            //           elevation: 12,
            //           items: [
            //             DropdownMenuItem<String>(
            //               child: Text('Select Event Category'),
            //               value: '',
            //             ),
            //             DropdownMenuItem<String>(
            //               child: Text('Match'),
            //               value: 'Match',
            //             ),
            //             DropdownMenuItem<String>(
            //               child: Text('Training'),
            //               value: 'Training',
            //             ),
            //             DropdownMenuItem<String>(
            //               child: Text('Other Event'),
            //               value: 'Other Event',
            //             ),
            //           ],
            //           onChanged: (String value) {
            //             FocusScope.of(context).requestFocus(new FocusNode());
            //             print(value);
            //             setState(() {
            //               _value = value;
            //             });
            //           },
            //           hint: Text('Select Item'),
            //         ),
            //       ),
            //     ),
            //   ),
            // ),
            // //add description
            Padding(
              padding: const EdgeInsets.only(top: 10.0, left: 18),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    " Description:",
                    style: MyResources.appTextStyle,
                  ),
                ],
              ),
            ),
            Padding(
              //Add padding around textfield
              padding: EdgeInsets.only(top: 7.0, left: 20, right: 20),
              child: Container(
                height: 120,
                color: MyResources.backgroundColor,
                child: TextField(
                  controller: desController,
                  keyboardType: TextInputType.multiline,
                  maxLines: 5,
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
            //submit
            Padding(
              padding: EdgeInsets.only(top: 40.0, left: 10, right: 10),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.9,
                height: 45,
                child: RaisedButton(
                  textColor: Colors.white,
                  color: MyResources.buttonColor,
                  child: Text(
                    "SUBMIT",
                  ),
                  onPressed: () async {
                    if (titleController.text.isNotEmpty &&
                        desController.text.isNotEmpty) await submitNews();
                  },
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
