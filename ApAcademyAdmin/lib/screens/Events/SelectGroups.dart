// import 'package:apsportacademy/ModelView/Controller/GroupController.dart';
// import 'package:apsportacademy/ModelView/Controller/dashboardController.dart';
// import 'package:apsportacademy/ModelView/Controller/databaseController.dart';
// import 'package:apsportacademy/constants/util.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:intl/intl.dart';

// import '../../constants/util.dart';

// class SelectGroups extends StatefulWidget {
//   @override
//   _SelectGroupsState createState() => _SelectGroupsState();
// }

// class _SelectGroupsState extends State<SelectGroups> {
//   final dashC = Get.put(DashBoardController());
//   String selectedValue = "", searchText = '';
//   List<Map<String, dynamic>> userList = [];
//   List<Map<String, dynamic>> selectedUserList = [];
//   final databaseController = Get.put(DatabaseController());

//   @override
//   void initState() {
//     dashC.groupWiseRefresh();
//     userList = dashC.group18Wise;
//     dashC.groupWiseRefresh();
//     filteruserList();
//     selectedMember = List<bool>.generate(userList.length, (index) => false);

//     super.initState();
//   }

//   filteruserList() {
//     userList = [];
//     dashC.group18Wise.forEach((element) {
//       if (element['groupType'].contains(selectedValue)) {
//         if (element['name']
//             .toString()
//             .toLowerCase()
//             .contains(searchText.toLowerCase())) userList.add(element);
//       }
//     });
//     dashC.group11Wise.forEach((element) {
//       if (element['groupType'].contains(selectedValue)) {
//         if (element['name']
//             .toString()
//             .toLowerCase()
//             .contains(searchText.toLowerCase())) userList.add(element);
//       }
//     });
//     dashC.group20Wise.forEach((element) {
//       if (element['groupType'].contains(selectedValue)) {
//         if (element['name']
//             .toString()
//             .toLowerCase()
//             .contains(searchText.toLowerCase())) userList.add(element);
//       }
//     });
//     dashC.group25Wise.forEach((element) {
//       if (element['groupType'].contains(selectedValue)) {
//         if (element['name']
//             .toString()
//             .toLowerCase()
//             .contains(searchText.toLowerCase())) userList.add(element);
//       }
//     });
//   }

//   List<bool> selectedMember = [];
// //////////////////////////////---ye
//   getSelMember() {
//     if (selectedUserList.isNotEmpty) selectedUserList.clear();
//     int count = 0;

//     selectedMember.forEach((element) {
//       if (element == true) {
//         selectedUserList.add(userList[count]);
//         print(selectedUserList.length);
//       }

//       ++count;
//     });
//   }

//   @override
//   void dispose() {
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Center(
//       child: Card(
//         child: Container(
//           height: Get.height * 0.86,
//           width: Get.width * 0.96,
//           child: SingleChildScrollView(
//             child: Padding(
//               padding: const EdgeInsets.all(8.0),
//               child: Column(
//                 children: [
//                   Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       Container(
//                         width: 130,
//                         child: TextField(
//                           decoration: InputDecoration(
//                               prefixIcon: Icon(Icons.search),
//                               border: InputBorder.none,
//                               fillColor: Colors.grey,
//                               hintText: "Search Name"),
//                           onChanged: (value) {
//                             filteruserList();
//                             setState(() {
//                               searchText = value;
//                             });
//                           },
//                         ),
//                       ),
//                       Padding(
//                         //Add padding around textfield
//                         padding: EdgeInsets.only(top: 7.0, left: 10, right: 10),
//                         child: Padding(
//                           padding: const EdgeInsets.all(0.0),
//                           child: Container(
//                             decoration: BoxDecoration(
//                                 color: MyResources.backgroundColor,
//                                 border: Border.all(color: Colors.blue),
//                                 borderRadius: BorderRadius.circular(10)),
//                             //  color: MyResources.backgroundColor,
//                             width: 130,
//                             height: 35,
//                             child: Padding(
//                               padding: const EdgeInsets.all(8.0),
//                               child: GetX<GroupsControler>(
//                                 init: Get.put<GroupsControler>(GroupsControler()),
//                                 builder: (GroupsControler groupController) {
//                                   if (groupController != null) {
//                                     if (groupController.groupName.length != 0) {
//                                       if (selectedValue == '')
//                                         selectedValue = groupController
//                                             .groupName[0]['name']
//                                             .toString();
//                                       return DropdownButton<String>(
//                                         underline: Text(""),
//                                         isExpanded: true,
//                                         value: selectedValue,
//                                         focusColor: MyResources.buttonColor,
//                                         elevation: 12,
//                                         items: groupController.groupName
//                                             .map((map) {
//                                           return DropdownMenuItem(
//                                             child: Text(
//                                               map['name'].toString(),
//                                               style: TextStyle(fontSize: 12),
//                                             ),
//                                             value: map['name'].toString(),
//                                           );
//                                         }).toList(),
//                                         onChanged: (String value) {
//                                           print(value);
//                                           selectedValue = value;
//                                           filteruserList();
//                                           setState(() {});
//                                         },
//                                         hint: Text('Select Item'),
//                                       );
//                                     } else
//                                       return Container();
//                                   } else
//                                     return Container();
//                                 },
//                               ),
//                             ),
//                           ),
//                         ),
//                       ),
//                     ],
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.only(top: 12.0),
//                     child: Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: [
//                         Text(
//                           " Select Members",
//                           style: MyResources.appHeadingStyle,
//                         ),
//                         Container(
//                           width: Get.width * 0.2,
//                           height: 35,
//                           child: RaisedButton(
//                             textColor: Colors.white,
//                             color: MyResources.buttonColor,
//                             child: Text(
//                               "Add",
//                             ),
//                             onPressed: () {
//                               getSelMember();
//                               Get.back(result: selectedUserList);

//                               //  selectedUserList.clear();
//                             },
//                             shape: new RoundedRectangleBorder(
//                               borderRadius: new BorderRadius.circular(10.0),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                   userList.length > 0
//                       ? DataTable(
//                           columnSpacing: 120,
//                           columns: const <DataColumn>[
//                             DataColumn(
//                               label: Text(
//                                 'Name',
//                                 style: TextStyle(fontStyle: FontStyle.normal),
//                               ),
//                             ),
//                             DataColumn(
//                               label: Text(
//                                 'Group',
//                                 style: TextStyle(fontStyle: FontStyle.normal),
//                               ),
//                             ),
//                           ],
//                           rows:
//                               List<DataRow>.generate(userList.length, (index) {
//                             return DataRow(
//                               color: MaterialStateProperty.resolveWith<Color>(
//                                   (Set<MaterialState> states) {
//                                 // All rows will have the same selected color.
//                                 if (states.contains(MaterialState.selected))
//                                   return Theme.of(context)
//                                       .colorScheme
//                                       .primary
//                                       .withOpacity(0.08);

//                                 if (index % 2 == 0)
//                                   return Colors.grey.withOpacity(0.3);
//                                 return null; // Use default value for other states and odd rows.
//                               }),
//                               cells: [
//                                 DataCell(Text(userList[index]['name'])),
//                                 DataCell(userList[index]['groupType'] != null
//                                     ? Text(
//                                         userList[index]['groupType'].toString())
//                                     : Text('Not Added')),
//                               ],
//                               selected: selectedMember[index],
//                               onSelectChanged: (bool value) {
//                                 selectedMember[index] = value;
//                                 setState(() {
//                                   print(value.toString());
//                                 });
//                               },
//                             );
//                           }),
//                         )
//                       : Container(),
//                 ],
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }
