import 'package:flutter/foundation.dart';

class TeamsModel {
  int id;
  String teamName;
  String coach;
  String teamType;
  List teamMembers;

  TeamsModel(
      {required this.id, required this.teamName, required this.teamType, required this.teamMembers, required this.coach});

  factory TeamsModel.fromJson(Map<String, dynamic> json) {
   return TeamsModel(
       id : json['id'],
       teamName : json['teamName'],
       coach : json['coach'],
       teamType : json['teamType'],
       teamMembers : json['teamMembers'],);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['teamName'] = this.teamName;
    data['coach'] = this.coach;
    data['teamType'] = this.teamType;
    data['teamMembers'] = this.teamMembers;
    return data;
  }
}
