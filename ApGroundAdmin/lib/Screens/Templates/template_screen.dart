import 'package:ap_ground_admin/Screens/Grounds/Models/slot_model.dart';
import 'package:ap_ground_admin/Screens/Grounds/Models/template_model.dart';
import 'package:ap_ground_admin/Screens/Templates/add_template.dart';
import 'package:ap_ground_admin/constants/util.dart';
import 'package:ap_ground_admin/controllers/template_controller.dart';
import 'package:ap_ground_admin/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class TemplatesScreen extends StatefulWidget {
  const TemplatesScreen({Key? key}) : super(key: key);

  @override
  _TemplatesScreenState createState() => _TemplatesScreenState();
}

class _TemplatesScreenState extends State<TemplatesScreen> {

  late List<Template> templates;
  bool isLoading = true;
  void refresh() async {
    setState(() {
      isLoading = true;
    });
    templates = await TemplateController().readTemplate();
    setState(() {
      isLoading = false;
    });
  }

   onGoBack(dynamic value) {
    refresh();
    setState(() {});
  }
  @override
  void initState() {
    refresh();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Padding(
          padding: const EdgeInsets.only(right: 8.0),
          child: ElevatedButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AddTemplate()),
              ).then((value) => onGoBack(value));
            },
            child: Text("Add"),
            style: ElevatedButton.styleFrom(primary: Color(0xff1758B4)),
          ),
        ),
        Expanded(
          child: isLoading
              ? LoadingWidget()
              : templates.length == 0 ? Center(child: Text("No Result Found"),) :
          ListView.builder(
              padding: const EdgeInsets.all(8),
              itemCount: templates.length,
              itemBuilder: (BuildContext context, int index) {
                Template template = templates[index];
                return InkWell(
                  onTap: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => AddTemplate(template: template,)),
                    ).then((value) => onGoBack(value));
                  },
                  child: Card(
                    child: Column(
                      children: [
                        Container(
                          height:40,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(3),
                            color: MyResources.buttonColor,
                          ),
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Name : " + template.name.toUpperCase(),style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),),
                              Text("Slots : "+template.slots.length.toString(),style: TextStyle(color: Colors.white),),

                            ],
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black, width: 1),
                              borderRadius: BorderRadius.circular(3)),
                          child: Column(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(3),
                                  color: Colors.grey.shade300,
                                ),
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: const [
                                    Expanded(flex: 1, child: Center(child: Text("Slot no"))),
                                    Expanded(flex: 2, child: Center(child: Text("Start Time"))),
                                    Expanded(flex: 2, child: Center(child: Text("Price"))),
                                    Expanded(flex: 2, child: Center(child: Text("Hours"))),
                                  ],
                                ),
                              ),
                              Container(
                                constraints: BoxConstraints(minHeight: 50,),
                                child: ListView.builder(
                                    shrinkWrap: true,
                                    itemCount: template.slots.length,
                                    itemBuilder: (BuildContext context, int index) {
                                      Slot slot = template.slots[index];
                                      return Card(
                                        elevation: 4,
                                        child: Container(
                                          color: const Color(0xfff1f2f8),
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Expanded(
                                                  flex: 1,
                                                  child: Center(
                                                      child: Text((index + 1).toString()))),
                                              Expanded(
                                                  flex: 2,
                                                  child: Center(
                                                      child: Text(DateFormat('hh:mm a').format(slot.startTime)))),
                                              Expanded(
                                                  flex: 2,
                                                  child: Center(
                                                      child: Text(formatnmbr(slot.price)))),
                                              Expanded(
                                                  flex: 2,
                                                  child: Center(
                                                      child: Text((slot.hours).toString()))),
                                            ],
                                          ),
                                        ),
                                      );
                                    }),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              }),
        ),
      ],
    );
  }
}
