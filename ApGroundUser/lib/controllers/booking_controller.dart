import 'dart:convert';

import 'package:ap_booking_user/Screens/My%20booking/Models/booking_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'firebase_controllers.dart';
import 'package:firebase_database/firebase_database.dart';

class BookingController {
  final _auth = FirebaseAuth.instance;
  final DatabaseReference databaseReference =
      FirebaseDatabase.instance.reference().child("Bookings");

  Future<void> saveBooking(Booking booking) async {
    try {
      await databaseReference.push().set(booking.toJson());
    } catch (e) {
      displayToast(e.toString());
      print(e.toString());
    }
  }

  Future<void> updateBooking(Booking Booking) async {
    databaseReference.update({Booking.id!: Booking.toJson()});
  }

  Future<void> deleteBooking(String id) async {
    databaseReference.child(id).remove();
  }

  Future<List<Booking>> readAllBookings() async {
    List<Booking> grounds = [];
    await databaseReference.once().then((DataSnapshot snapshot) {
      if (snapshot.exists) {
        snapshot.value.forEach((key, value) {
          Booking gr = Booking.fromJson(value, key);
          grounds.add(gr);
        });
      }
    });
    return grounds;
  }

  Future<List<Booking>> readBookingbyUid(String uid) async {
    List<Booking> bookings = await readAllBookings();
    List<Booking> booking = bookings
        .where((element) => element.slot.Pslot1!.teamId == uid||(element.slot.Pslot2!= null&&element.slot.Pslot2!.teamId == uid))
        .toList();
    print(booking.length);
    return booking;
  }
}
