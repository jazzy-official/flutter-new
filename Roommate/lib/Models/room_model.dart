import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:room_buddy/Models/about_user_model.dart';
import 'package:room_buddy/Models/location_model.dart';

class Room {
  final int? id;
  final int ownerId;
  final String city;
  final String locality;
  final String address;
  final LatLng location;
  final List<String> images;
  final DateTime datePosted;
  final String description;
  final double rent;
  final double securityDeposit;
  final bool billIncluded;
  final String furnishingType;
  final String ownerDp;
  final int bedrooms;
  final String bedroomType;
  final int bathroom;
  final String bathroomType;
  final double roomsize;
  final double propertySize;
  final String availability;
  final String minStay;
  final List<String> amenities;
  final String ownerName;
  final AboutUser aboutOwner;
  final AboutUser roommatePreferences;
  final int? priority;

  Room({
    required this.address,
    required this.location,
    required this.images,
    required this.city,
    required this.locality,
    required this.datePosted,
    required this.description,
    required this.rent,
    required this.securityDeposit,
    required this.billIncluded,
    required this.furnishingType,
    required this.bedrooms,
    required this.bedroomType,
    required this.bathroom,
    required this.bathroomType,
    required this.roomsize,
    required this.propertySize,
    required this.availability,
    required this.minStay,
    required this.amenities,
    required this.ownerName,
    required this.aboutOwner,
    required this.roommatePreferences,
    this.priority,
    this.id,
    required this.ownerDp,
    required this.ownerId,
  });

  Room.fromJson(
    Map<String, dynamic> json,
  )   : address = json['address'],
        location = json['location'],
        id = json['id'],
        images = json['images'],
        city = json['images'],
        locality = json['images'],
        datePosted = json['images'],
        description = json['images'],
        rent = json['images'],
        securityDeposit = json['images'],
        billIncluded = json['images'],
        furnishingType = json['images'],
        bedrooms = json['images'],
        bedroomType = json['images'],
        bathroom = json['images'],
        bathroomType = json['images'],
        roomsize = json['images'],
        propertySize = json['images'],
        availability = json['images'],
        minStay = json['images'],
        amenities = json['images'],
        ownerName = json['images'],
        aboutOwner = json['images'],
        roommatePreferences = json['images'],
        ownerDp = json['images'],
        ownerId = json['images'],
        priority = json['images'];

  Map<String, dynamic> toJson() => {
        'address': address,
        'location': location,
        'images': images,
      };
}
