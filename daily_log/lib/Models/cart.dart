class Cart {
  String itemId;
  String variationId;
  String itemName;
  String subtitle;
  num itemPerPrice;
  int itemQuantity;

  Cart({
    required this.itemId,
    required this.variationId,
    required this.itemName,
    required this.subtitle,
    this.itemPerPrice = 0.0,
    this.itemQuantity = 1,
  });

  factory Cart.fromMap(Map<dynamic, dynamic> map) {
    return  Cart(
      itemId: map['itemId'] as String,
      variationId: map['variationId'] as String,
      itemName: map['itemName'] as String,
      subtitle: map['subtitle'] as String,
      itemPerPrice: map['itemPerPrice'] ,
      itemQuantity: map['itemQuantity'] as int,
    );
  }

  Map<dynamic, dynamic> toMap() {
    return {
      'itemId': this.itemId,
      'variationId': this.variationId,
      'itemName': this.itemName,
      'subtitle': this.subtitle,
      'itemPerPrice': this.itemPerPrice,
      'itemQuantity': this.itemQuantity,
    } as Map<dynamic, dynamic>;
  }

  static Map<dynamic, dynamic> toOrderMap(List<Cart> cartItems) {
    Map<dynamic, dynamic> orderMap =  Map<dynamic, dynamic>();
    for (Cart cart in cartItems) {
      orderMap[cart.variationId] = {
        'itemId': cart.itemId,
        'variationId': cart.variationId,
        'itemName': cart.itemName,
        'subtitle': cart.subtitle,
        'itemPerPrice': cart.itemPerPrice,
        'itemQuantity': cart.itemQuantity,
      };
    }
    return orderMap;
  }

  static toOrderItemList(var map) {
    Map values = map as Map;
    List<Cart> cartItem = [];
    values.forEach((key, data) {
      final Cart connect = Cart.fromMap(data);
      cartItem.add(connect);
    });
    return cartItem;
  }
}