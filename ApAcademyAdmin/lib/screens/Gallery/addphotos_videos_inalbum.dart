import 'dart:io';

import 'package:ap_academy_admin/ModelView/Controller/AlbumController.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ionicons/ionicons.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

class EditAlbum extends StatefulWidget {
  final Map<String, dynamic> album;

  const EditAlbum({Key? key, required this.album, }) : super(key: key);
  @override
  _EditAlbumState createState() => _EditAlbumState();
}

class _EditAlbumState extends State<EditAlbum> {
  final albumController = Get.put(PhotoAlbumController());
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  bool isImage = false;
  List<dynamic> photos = [];
  @override
  void initState() {
   _titleController.text = widget.album['albumName'];
   _descriptionController.text = widget.album['desciption'];
   photos = widget.album['albumPhotos'];
    super.initState();
  }

  @override
  void dispose() {
    albumController.photos.clear();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Edit Album"),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 6.0, left: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "  Title:",
                        style: GoogleFonts.roboto(
                            fontSize: 15, fontWeight: FontWeight.w400),
                      ),
                    ],
                  ),
                ),
                Padding(
                  /////////////////////////////////////////////////////////////Add padding around textfield
                  padding: EdgeInsets.only(top: 7.0, left: 20, right: 20),
                  child: Container(
                    color: MyResources.backgroundColor,
                    constraints: BoxConstraints(maxHeight: 200),
                    child: TextField(
                      controller: _titleController,
                      maxLines: 4,
                      minLines: 1,
                      decoration: InputDecoration(
                        //Add th Hint text here.

                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                    ),
                  ),
                ),
                ///////////////////////////////////////////////////////////////Add Description
                Padding(
                  padding: const EdgeInsets.only(top: 10.0, left: 18),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        " Description:",
                        style: GoogleFonts.roboto(
                            fontSize: 15, fontWeight: FontWeight.w400),
                      ),
                    ],
                  ),
                ),
                Padding(
                  //////////////////////////////////////////////////////////Add padding around textfield
                  padding: EdgeInsets.only(top: 7.0, left: 20, right: 20),
                  child: Container(
                    height: 120,
                    color: MyResources.backgroundColor,
                    child: TextField(
                      keyboardType: TextInputType.multiline,
                      maxLines: 5,
                      controller: _descriptionController,
                      decoration: InputDecoration(
                        //Add th Hint text here.
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: Get.height * 0.3,
                  child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: GridView.count(
                        shrinkWrap: true,
                        // Create a grid with 2 columns. If you change the scrollDirection to
                        // horizontal, this produces 2 rows.
                        crossAxisCount: 3,
                        // Generate 100 widgets that display their index in the List.
                        children: List.generate(
                            photos
                                .length, (index) {
                          return InkWell(
                            onTap: () =>
                            {
                              Get.to(PhotoViewGallery.builder(
                                scrollPhysics: const BouncingScrollPhysics(),
                                builder: (BuildContext context,  int i) {

                                  return PhotoViewGalleryPageOptions(
                                    imageProvider: NetworkImage(photos[i].toString()),
                                    initialScale: PhotoViewComputedScale.contained,

                                    // heroAttributes: PhotoViewHeroAttributes(tag: approvel['documents'][index]),
                                  );
                                },
                                itemCount: photos.length,
                                loadingBuilder: (context, event) => Center(
                                  child: Container(
                                    width: 20.0,
                                    height: 20.0,
                                    child: CircularProgressIndicator(
                                      value: event == null
                                          ? 0
                                          : event.cumulativeBytesLoaded / event.expectedTotalBytes!.toInt(),
                                    ),
                                  ),
                                ),
                                // backgroundDecoration: widget.backgroundDecoration,
                                // pageController: widget.pageController,
                                // onPageChanged: onPageChanged,
                              ))
                            },
                            child: Stack(
                              children: [
                                Card(
                                  margin: EdgeInsets.all(10),
                                  color: Colors.white60,
                                  child: Container(
                                      width: Get.width,
                                      height: Get.height,
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                            colorFilter: new ColorFilter.mode(
                                                Colors.black.withOpacity(0.5),
                                                BlendMode.dstATop),
                                            fit: BoxFit.cover,
                                            image: NetworkImage(
                                              photos[index]
                                                  .toString(),
                                            )),
                                      ),
                                      child: Container(
                                        //child:
                                        //Text('Pic  ${index + 1}',
                                        //  style: MyResources.appTextStyle),
                                      )),
                                ),
                                 Positioned(child: InkWell(
                                   onTap:(){
                                     photos.removeAt(index);
                                     setState(() {

                                     });
                          },
                                    child: Icon(Ionicons.remove_circle_outline,color: Colors.red,))),
                              ],
                            ),
                          );
                        }),
                      ),
                  ),
                ),
                Text(
                  "Upload new Photos  ",
                  style: GoogleFonts.roboto(
                    fontSize: 25,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10.0, left: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        " Upload Photos:",
                        style: GoogleFonts.roboto(
                            fontSize: 15, fontWeight: FontWeight.w400),
                      ),
                    ],
                  ),
                ),
                Padding(
                  /////////////////////////////////////////////////////////Add padding around textfield
                  padding: EdgeInsets.only(top: 7.0, left: 10, right: 10),
                  child: Container(
                    height: 140,
                    width: Get.width,
                    child: Card(
                      color: MyResources.backgroundColor,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(10.0),
                              child: isImage
                                  ? Obx(() => Container(
                                width: 200,
                                height: 100,
                                child: GridView.count(
                                  scrollDirection: Axis.horizontal,
                                  crossAxisCount: 1,
                                  shrinkWrap: true,
                                  children: List.generate(
                                      albumController.photos.length,
                                          (index) {
                                        return Image.file(
                                          albumController.photos[index],
                                          width: 100,
                                          height: 100,
                                        );
                                      }),
                                ),
                              ))
                                  : Container(
                                child: Icon(
                                  Icons.image_outlined,
                                  size: 50,
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 10.0),
                            child: IconButton(
                              onPressed: () async {
                                try {
                                  await albumController.geT()
                                      .then((value) => {
                                    setState(() {
                                      if (isImage == false) {
                                        isImage = true;
                                      }

                                      /////////////////////////////////////////////////////IsImage=isImage? false:true;
                                    }),
                                  });
                                } catch (er) {
                                  print(er);
                                  Get.snackbar('Warning',
                                      'You did not selected any picture!');
                                }
                              },
                              icon: Icon(
                                Icons.upload_sharp,
                                size: 30,
                              ),
                            ),
                          ),

                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding:
                  EdgeInsets.only(top: 10.0, left: 20, right: 10, bottom: 10),
                  child: Container(
                    width: Get.width * 0.9,
                    height: 45,
                    child: RaisedButton(
                      textColor: Colors.white,
                      color: MyResources.buttonColor,
                      child: Text(
                        "Submit",
                      ),
                      onPressed: () async {
                        if (_titleController.text.isNotEmpty) {
                          Get.defaultDialog(
                              barrierDismissible: false,
                              title: "Loading...${albumController.count.value} ",
                              content: Center(
                                child: LoadingWidget(),
                              ));
                          await albumController.editAlbum(
                              _titleController.text, _descriptionController.text,widget.album['_id'],photos);
                          albumController.photos.clear();

                          Get.back();
                          Get.back();
                          await albumController.refreshData();
                        } else {
                          albumController.photos.clear();
                          Get.snackbar(
                              "Name Error", "Please write name and description");
                        }
                      },
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
