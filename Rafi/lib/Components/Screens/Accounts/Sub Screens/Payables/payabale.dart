import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:intl/intl.dart';
import 'package:rafi_admin/Components/loader_widget.dart';
import 'package:rafi_admin/Controllers/payables_controller.dart';


class PayableScreen extends StatelessWidget {
   PayableScreen({Key? key}) : super(key: key);

   @override
  Widget build(BuildContext context) {
    var formatter = NumberFormat('#,##,000');
    return  GetBuilder<PayableController>(
      autoRemove: false,
      init: PayableController(),
      builder: (cont) => Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          Container(
            width: MediaQuery.of(context).size.width - 30,
            // (MediaQuery.of(context).size.width * 1) - 90,
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8),
                boxShadow: const [
                  BoxShadow(
                      color: Colors.black26,
                      blurRadius: 5,
                      offset: Offset(0, 0.5))
                ]),
            height: 45,
            child: TextFormField(
              onChanged: (val) {
                cont.runFilter(val);
              },
              style: const TextStyle(
                color: Colors.black87,
              ),
              decoration: const InputDecoration(
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.only(
                    top: 14,
                  ),
                  prefixIcon: Icon(
                    Icons.search,
                    color: Color(0xff7F7C82),
                  ),
                  hintText: 'Search',
                  hintStyle: TextStyle(
                    color: Colors.black38,
                  )),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Flexible(
            child: cont.payables == null? const LoadingWidget() : cont.foundResult.isNotEmpty
                ? RefreshIndicator(
              onRefresh: () async {
                return await cont.refreshAll();
              },
                  child: ListView.builder(
              physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
              itemCount: cont.foundResult.length,
              itemBuilder: (context, index) {
                  final payable = cont.foundResult[index];
                  return Padding(
                    padding: const EdgeInsets.only(right: 5.0, left: 5),
                    child: Card(
                      elevation: 5,
                      color: Color(0xffbf953f),
                      child: InkWell(
                          splashColor: Colors.blue.withAlpha(30),
                          onTap: () {

                          },
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(3),
                              color: Color(0xfff1f6f8),
                            ),
                            margin: EdgeInsets.all(1),
                            padding: const EdgeInsets.all(15),
                            // height: 200,
                            child: Column(
                              mainAxisAlignment:
                              MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Balance : ' + formatter.format(payable.currentbalance),
                                  style: const TextStyle(
                                      color:  Color(0xff004251),
                                      fontWeight: FontWeight.w500),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),

                                Column(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      payable.accounttitle,
                                      style: const TextStyle(
                                          color:  Color(0xff004251),
                                          fontWeight: FontWeight.w500,
                                          fontSize: 22),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                      CrossAxisAlignment.end,
                                      children: [
                                        Text(
                                          payable.accountno,
                                          style: const TextStyle(
                                            color:  Color(0xff004251),
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                        Container(
                                          width: 100,
                                          child: Flex(
                                              direction: Axis.vertical,
                                              children: [Text(
                                                payable.accountdes,
                                                style: const TextStyle(
                                                    color:  Color(0xff004251),
                                                    fontSize: 11),
                                              ),
                                              ]
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          )),
                    ),
                  );
              },
            ),
                )
                : const Center(
              child: Text(
                'No results found',
                style: TextStyle(fontSize: 24),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

