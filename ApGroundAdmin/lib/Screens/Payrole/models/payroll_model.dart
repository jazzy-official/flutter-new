class Payroll {
  DateTime date;
  double salary;
  bool isPaid;

  Payroll({required this.date, required this.salary, required this.isPaid,});

// NOTE: implementing functionality here in the next step!

  factory Payroll.fromJson(dynamic json) {
    return Payroll(
      date : DateTime.parse(json['date']),
      salary : double.parse(json['salary'].toString()),
      isPaid : json['isPaid'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this.date.toString();
    data['salary'] = this.salary;
    data['isPaid'] = this.isPaid;
    return data;
  }
}