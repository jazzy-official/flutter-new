
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:rafi_admin/Components/loader_widget.dart';
import 'package:rafi_admin/Controllers/chart_accounts_controller.dart';
import 'package:tree_view/tree_view.dart';

import 'Model/document_modal.dart';
import 'widgets/directory_widget.dart';

class ChartAccountScreen extends StatefulWidget {
  ChartAccountScreen({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  _ChartAccountScreenState createState() => _ChartAccountScreenState();
}

class _ChartAccountScreenState extends State<ChartAccountScreen> {
  // List<Document> documentList = [
  //   Document(
  //     title: 'Desktop',
  //     balance: 200000,
  //     code: "0001",
  //     childData: [
  //       Document(title: 'Projects', balance: 300000000,code: "0222", childData: [
  //         Document(
  //             title: 'flutter_app',
  //             balance: 576575656,
  //             code: "8787",
  //             childData: [
  //               Document(
  //                 title: 'README.md',
  //                 code: "089768",
  //                 balance: 12435454,
  //               ),
  //               Document(
  //                 title: 'README.md',
  //                 code: "089768",
  //                 balance: 12435454,
  //               ),
  //               Document(
  //                 title: 'README.md',
  //                 code: "089768",
  //                 balance: 12435454,
  //               ),
  //
  //
  //             ])
  //       ]),
  //
  //       Document(
  //         title: 'image3.png',
  //         code: "0999",
  //         balance: 2345435345,
  //       ),
  //     ],
  //   ),
  // ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<ChartAccountController>(
        autoRemove: false,
        init: ChartAccountController(),
        builder: (cont) => cont.accounts==null? LoadingWidget(): RefreshIndicator(
          onRefresh: (){
            return cont.refreshAll();
          },
          child: TreeView(
            startExpanded: false,
            children: _getChildList(cont.accounts??[]),
          ),
        ),
      ),
    );
  }

  List<Widget> _getChildList(List<Document?> childDocuments) {
    return childDocuments.map((document) {
        return Container(
          margin: EdgeInsets.only(left: 12),
          child: TreeViewChild(
            parent: _getDocumentWidget(document: document!),
            children:_getChildList(document.childData)+_getChildList(document.account),
          ),
        );
    }).toList();
  }

  Widget _getDocumentWidget({required Document document}) =>
       _getDirectoryWidget(document: document);

  DirectoryWidget _getDirectoryWidget({required Document document}) =>
      DirectoryWidget(
        balance: document.balance,
        code: document.code,
        tilte: document.title,

      );

}