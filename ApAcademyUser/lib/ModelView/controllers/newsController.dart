import 'package:get/get.dart';

import 'db_controller.dart';

class NewsController extends GetxController {
  Rx<List<Map<String, dynamic>>> newsList = Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>>? get news => newsList.value;

  @override
  Future<void> onInit() async {
    // await db.open();
    // String id = Get.find<AuthController>().firebaseUser.uid;
    newsList.bindStream(
        DatabaseController().newsStream()); //id)); //Stream Comming from
    super.onInit();
  }

  @override
  Future<void> onClose() async {
    // await db.close();
    super.onClose();
  }
}
