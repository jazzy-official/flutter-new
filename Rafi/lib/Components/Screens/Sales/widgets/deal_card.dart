import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:rafi_admin/Components/Screens/Sales/Models/deal_unit_model.dart';
import 'package:rafi_admin/Components/Screens/Sales/Models/deals_model.dart';
import 'package:rafi_admin/Components/Screens/Sales/widgets/deal_units_list_page.dart';

class DealCard extends StatelessWidget {
  final DealModel deal;
  DealCard({Key? key, required this.deal}) : super(key: key);
  var formatter = NumberFormat('#,##,000');
  @override
  Widget build(BuildContext context) {
    List<DealUnit> _dealunit =
        deal.dealUnits.where((i) => i.status == 'Sold').toList();
    int sold = _dealunit.length;
    return Card(
      elevation: 4,
      child: InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => DealUnitList(dealunits: deal.dealUnits)),
          );
        },
        child: Container(
          height: 150,
          padding: EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.6,
                    child: Flex(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      direction: Axis.vertical,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          deal.dealName,
                          style:
                          TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.6,
                    child: Row(
                      children: [
                        Text("Affilliate : ",style:TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
                        Flexible(
                            child: Text(
                          deal.affiliateName,
                          style:
                              TextStyle(fontSize: 15,),
                        )),
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      Text(
                        "Deal Ammount : ",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text(
                        formatter
                            .format(deal.dealAmount),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        "Deal Sold Ammount : ",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text(
                        formatter
                            .format(deal.dealSoldAmount),

                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        "Received Ammount : ",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text(
                        formatter
                            .format(deal.receivedAmount),

                      ),
                    ],
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                  DateFormat(' dd-MMM-yyyy').format(DateTime.parse(deal.entryDate))
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            "Deal units : ",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(
                            deal.dealUnits.length.toString(),
                          ),
                        ],
                      ),
                      SizedBox(height: 10,),
                      Row(
                        children: [
                          Text(
                            "Sold units : ",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(
                            sold.toString(),
                          ),
                        ],
                      ),
                      SizedBox(height: 10,),
                      Row(
                        children: [
                          Text(
                            "Available units : ",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(
                            (deal.dealUnits.length - sold).toString(),
                          ),
                        ],
                      ),
                    ],
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
