import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Screens/User%20Forms/iterest_form.dart';

class HabitsForm extends StatefulWidget {
   const HabitsForm({Key? key}) : super(key: key);

  @override
  State<HabitsForm> createState() => _HabitsFormState();
}

class _HabitsFormState extends State<HabitsForm> {
  String smoking = "Never";
  String alcohol = "Never";
  String partying = "Weekend";
  String guests = "Occasionally";
  String pets = "May Have";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: myResources.background,
        title: const Text("Add your personal & living habits",style: TextStyle(color: Colors.black),),
        centerTitle: true,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
      ),

      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              child: ListView(
                children: [
                  const Text(
                    "Smoking",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 10),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                const BorderRadius.all(Radius.circular(10)),
                                side: BorderSide(
                                    color: smoking == "Daily"
                                        ? myResources.themeColor
                                        : Colors.black54)),
                          ),
                          onPressed: () {
                            setState(() {
                              smoking = "Daily";
                            });
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 12),
                            child: Center(
                                child: Text(
                                  'Daily',
                                  style: TextStyle(
                                      color: smoking == "Daily"
                                          ? myResources.themeColor
                                          : Colors.black54),
                                )),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 10),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                                side: BorderSide(
                                    color: smoking == "Occasionally"
                                        ? myResources.themeColor
                                        : Colors.black54)),
                          ),
                          onPressed: () {
                            setState(() {
                              smoking = "Occasionally";
                            });
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 12),
                            child: Center(
                                child: Text(
                                  'Occasionally',
                                  style: TextStyle(
                                      color: smoking == "Occasionally"
                                          ? myResources.themeColor
                                          : Colors.black54),
                                )),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 10),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                                side: BorderSide(
                                    color: smoking == "Never"
                                        ? myResources.themeColor
                                        : Colors.black54)),
                          ),
                          onPressed: () {
                            setState(() {
                              smoking = "Never";
                            });
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 12),
                            child: Center(
                                child: Text(
                                  'Never',
                                  style: TextStyle(
                                      color: smoking == "Never"
                                          ? myResources.themeColor
                                          : Colors.black54),
                                )),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const Text(
                    "Alcohol",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 10),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                const BorderRadius.all(Radius.circular(10)),
                                side: BorderSide(
                                    color: alcohol == "Daily"
                                        ? myResources.themeColor
                                        : Colors.black54)),
                          ),
                          onPressed: () {
                            setState(() {
                              alcohol = "Daily";
                            });
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 12),
                            child: Center(
                                child: Text(
                                  'Daily',
                                  style: TextStyle(
                                      color: alcohol == "Daily"
                                          ? myResources.themeColor
                                          : Colors.black54),
                                )),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 10),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                                side: BorderSide(
                                    color: alcohol == "Occasionally"
                                        ? myResources.themeColor
                                        : Colors.black54)),
                          ),
                          onPressed: () {
                            setState(() {
                              alcohol = "Occasionally";
                            });
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 12),
                            child: Center(
                                child: Text(
                                  'Occasionally',
                                  style: TextStyle(
                                      color: alcohol == "Occasionally"
                                          ? myResources.themeColor
                                          : Colors.black54),
                                )),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 10),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                                side: BorderSide(
                                    color: alcohol == "Never"
                                        ? myResources.themeColor
                                        : Colors.black54)),
                          ),
                          onPressed: () {
                            setState(() {
                              alcohol = "Never";
                            });
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 12),
                            child: Center(
                                child: Text(
                                  'Never',
                                  style: TextStyle(
                                      color: alcohol == "Never"
                                          ? myResources.themeColor
                                          : Colors.black54),
                                )),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const Text(
                    "Partying",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 10),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                const BorderRadius.all(Radius.circular(10)),
                                side: BorderSide(
                                    color: partying == "Weekend"
                                        ? myResources.themeColor
                                        : Colors.black54)),
                          ),
                          onPressed: () {
                            setState(() {
                              partying = "Weekend";
                            });
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 12),
                            child: Center(
                                child: Text(
                                  'Weekend',
                                  style: TextStyle(
                                      color: partying == "Weekend"
                                          ? myResources.themeColor
                                          : Colors.black54),
                                )),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 10),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                                side: BorderSide(
                                    color: partying == "Occasionally"
                                        ? myResources.themeColor
                                        : Colors.black54)),
                          ),
                          onPressed: () {
                            setState(() {
                              partying = "Occasionally";
                            });
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 12),
                            child: Center(
                                child: Text(
                                  'Occasionally',
                                  style: TextStyle(
                                      color: partying == "Occasionally"
                                          ? myResources.themeColor
                                          : Colors.black54),
                                )),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 10),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                                side: BorderSide(
                                    color: partying == "Never"
                                        ? myResources.themeColor
                                        : Colors.black54)),
                          ),
                          onPressed: () {
                            setState(() {
                              partying = "Never";
                            });
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 12),
                            child: Center(
                                child: Text(
                                  'Never',
                                  style: TextStyle(
                                      color: partying == "Never"
                                          ? myResources.themeColor
                                          : Colors.black54),
                                )),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const Text(
                    "Guests",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 10),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                const BorderRadius.all(Radius.circular(10)),
                                side: BorderSide(
                                    color: guests == "Daily"
                                        ? myResources.themeColor
                                        : Colors.black54)),
                          ),
                          onPressed: () {
                            setState(() {
                              guests = "Daily";
                            });
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 12),
                            child: Center(
                                child: Text(
                                  'Daily',
                                  style: TextStyle(
                                      color: guests == "Daily"
                                          ? myResources.themeColor
                                          : Colors.black54),
                                )),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 10),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                                side: BorderSide(
                                    color: guests == "Occasionally"
                                        ? myResources.themeColor
                                        : Colors.black54)),
                          ),
                          onPressed: () {
                            setState(() {
                              guests = "Occasionally";
                            });
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 12),
                            child: Center(
                                child: Text(
                                  'Occasionally',
                                  style: TextStyle(
                                      color: guests == "Occasionally"
                                          ? myResources.themeColor
                                          : Colors.black54),
                                )),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 10),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                                side: BorderSide(
                                    color: guests == "Never"
                                        ? myResources.themeColor
                                        : Colors.black54)),
                          ),
                          onPressed: () {
                            setState(() {
                              guests = "Never";
                            });
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 12),
                            child: Center(
                                child: Text(
                                  'Never',
                                  style: TextStyle(
                                      color: guests == "Never"
                                          ? myResources.themeColor
                                          : Colors.black54),
                                )),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const Text(
                    "Pets",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 10),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                const BorderRadius.all(Radius.circular(10)),
                                side: BorderSide(
                                    color: pets == "Have"
                                        ? myResources.themeColor
                                        : Colors.black54)),
                          ),
                          onPressed: () {
                            setState(() {
                              pets = "Have";
                            });
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 12),
                            child: Center(
                                child: Text(
                                  'Have',
                                  style: TextStyle(
                                      color: pets == "Have"
                                          ? myResources.themeColor
                                          : Colors.black54),
                                )),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 10),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                                side: BorderSide(
                                    color: pets == "Don't Have"
                                        ? myResources.themeColor
                                        : Colors.black54)),
                          ),
                          onPressed: () {
                            setState(() {
                              pets = "Don't Have";
                            });
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 12),
                            child: Center(
                                child: Text(
                                  'Don\'t Have',
                                  style: TextStyle(
                                      color: pets == "Don't Have"
                                          ? myResources.themeColor
                                          : Colors.black54),
                                )),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(vertical: 10),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                                side: BorderSide(
                                    color: pets == "May have"
                                        ? myResources.themeColor
                                        : Colors.black54)),
                          ),
                          onPressed: () {
                            setState(() {
                              pets = "May have";
                            });
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 12),
                            child: Center(
                                child: Text(
                                  'May have',
                                  style: TextStyle(
                                      color: pets == "May have"
                                          ? myResources.themeColor
                                          : Colors.black54),
                                )),
                          ),
                        ),
                      ),
                    ],
                  ),



                ],
              ),
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: myResources.themeColor,
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(14))),
              ),
              onPressed: () {
                Get.to(()=>  InterestForm());
              },
              child: Container(
                padding: const EdgeInsets.symmetric(
                    vertical: 8, horizontal: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    const Text(
                      'Confirm',
                      style: TextStyle(color: Colors.white),
                    ),
                    Container(
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                        borderRadius:
                        const BorderRadius.all(Radius.circular(20)),
                        color: myResources.themeColor,
                      ),
                      child: Icon(
                        Icons.arrow_forward_ios,
                        color: Colors.white,
                        size: 16,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
