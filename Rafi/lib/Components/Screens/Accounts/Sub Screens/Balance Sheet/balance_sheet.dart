import 'dart:convert';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:intl/intl.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Balance%20Sheet/Modals/entity_model.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Balance%20Sheet/entity_list_view.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Balance%20Sheet/Modals/balance_sheet_modal.dart';
import 'package:rafi_admin/Components/loader_widget.dart';
import 'package:rafi_admin/Controllers/balancesheet_controller.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tab_indicator_styler/tab_indicator_styler.dart';
import 'package:http/http.dart' as http;

class BalanceSheetPage extends StatefulWidget {
  const BalanceSheetPage({Key? key}) : super(key: key);

  @override
  State<BalanceSheetPage> createState() => _BalanceSheetPageState();
}

class _BalanceSheetPageState extends State<BalanceSheetPage> {
  var formatter = NumberFormat('#,##,000');
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        child:GetBuilder<BalanceSheetController>(
            autoRemove: false,
            init: BalanceSheetController(),
            builder: (cont) => cont.balanceSheet==null ? LoadingWidget() : Column(crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    color: Color(0xfff1f6f8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Center(
                            child: Text(
                              "Select Date",
                              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                            )),
                        const SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            ElevatedButton.icon(
                              onPressed: () {
                                showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (BuildContext context) => AlertDialog(
                                      contentPadding: EdgeInsets.only(
                                          bottom: 20, top: 0, left: 10, right: 10),
                                      content: Container(
                                        height: 400,
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              height: 350,
                                              width: 250,
                                              child: Stack(
                                                children: <Widget>[
                                                  Positioned(
                                                    left: 0,
                                                    top: 0,
                                                    right: 0,
                                                    bottom: 0,
                                                    child: CupertinoDatePicker(
                                                      onDateTimeChanged: (val) {
                                                        setState(() {
                                                          cont.startDate =
                                                              DateFormat('yyyy-MM-dd')
                                                                  .format(val);
                                                        });
                                                      },
                                                      mode:
                                                      CupertinoDatePickerMode.date,
                                                      initialDateTime:
                                                      DateTime.parse(cont.startDate),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                              children: [
                                                Container(
                                                    height: 40,
                                                    decoration: BoxDecoration(
                                                        color: Colors.blue,
                                                        borderRadius:
                                                        BorderRadius.circular(5)),
                                                    child: TextButton(
                                                        onPressed: () {
                                                          Navigator.of(
                                                            context,
                                                          ).pop();
                                                        },
                                                        child: const Text(
                                                          "Confirm",
                                                          style: TextStyle(
                                                              color: Colors.white),
                                                        ))),
                                              ],
                                            )
                                          ],
                                        ),
                                      )),
                                );
                              },
                              icon: Icon(
                                Icons.calendar_today_outlined,
                                color: Colors.black,
                              ),
                              label: Text(
                                cont.startDate,
                                style: TextStyle(color: Colors.black),
                              ),
                              style: ElevatedButton.styleFrom(
                                primary: Colors.white,
                              ),
                            ),
                            ElevatedButton.icon(
                              onPressed: () {
                                showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (BuildContext context) => AlertDialog(
                                      contentPadding: EdgeInsets.only(
                                          bottom: 20, top: 0, left: 10, right: 10),
                                      content: Container(
                                        height: 400,
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              height: 350,
                                              width: 250,
                                              child: Stack(
                                                children: <Widget>[
                                                  Positioned(
                                                    left: 0,
                                                    top: 0,
                                                    right: 0,
                                                    bottom: 0,
                                                    child: CupertinoDatePicker(
                                                      onDateTimeChanged: (val) {
                                                        setState(() {
                                                          cont.endDate =
                                                              DateFormat('yyyy-MM-dd')
                                                                  .format(val);
                                                        });
                                                      },
                                                      mode:
                                                      CupertinoDatePickerMode.date,
                                                      initialDateTime:
                                                      DateTime.parse(cont.endDate),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                              children: [
                                                Container(
                                                    height: 40,
                                                    decoration: BoxDecoration(
                                                        color: Colors.blue,
                                                        borderRadius:
                                                        BorderRadius.circular(5)),
                                                    child: TextButton(
                                                        onPressed: () {
                                                          Navigator.of(
                                                            context,
                                                          ).pop();
                                                        },
                                                        child: const Text(
                                                          "Confirm",
                                                          style: TextStyle(
                                                              color: Colors.white),
                                                        ))),
                                              ],
                                            )
                                          ],
                                        ),
                                      )),
                                );
                              },
                              icon: Icon(
                                Icons.calendar_today_outlined,
                                color: Colors.black,
                              ),
                              label: Text(
                                cont.endDate,
                                style: TextStyle(color: Colors.black),
                              ),
                              style: ElevatedButton.styleFrom(
                                primary: Colors.white,
                              ),
                            ),
                            ElevatedButton(
                              onPressed: () async {
                                showDialog<void>(
                                  context: context,
                                  barrierDismissible: true,
                                  // false = user must tap button, true = tap outside dialog
                                  builder: (BuildContext dialogContext) {
                                    return Container(
                                        height: MediaQuery.of(context).size.height,
                                        width:  MediaQuery.of(context).size.width,
                                        color:Colors.black26,
                                        child: LoadingWidget());
                                  },
                                );
                                await cont.getList();
                                Get.back();
                              },
                              child: Icon(
                                Icons.search,
                                color: Colors.black,
                              ),
                              style: ElevatedButton.styleFrom(
                                primary: Colors.white,
                              ),
                            ),
                          ],
                        ),
                        Container(
                          height: 95,
                          margin: EdgeInsets.all(5),
                          padding: EdgeInsets.all(5),
                          child: Container(
                            decoration: BoxDecoration(
                                color: (cont.assets == (cont.liability + cont.equity))
                                    ? Colors.green.shade100
                                    : Colors.red.shade100,
                                borderRadius: BorderRadius.circular(5)),
                            padding: EdgeInsets.all(10),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "Total Assets",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Text(formatter.format(cont.assets)),
                                      ],
                                    ),
                                    Text(
                                      "=",
                                      style:
                                      TextStyle(fontWeight: FontWeight.bold),
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "Total Liabilty",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Text(formatter.format(cont.liability)),
                                      ],
                                    ),
                                    Text(
                                      "+",
                                      style:
                                      TextStyle(fontWeight: FontWeight.bold),
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "Total Equity",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Text(formatter.format(cont.equity)),
                                      ],
                                    ),
                                  ],
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(formatter.format(cont.assets)),
                                    Text(
                                      "=",
                                      style:
                                      TextStyle(fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      "",
                                    ),
                                    Text(
                                      "",
                                    ),
                                    Text(formatter.format(cont.equity + cont.liability)),
                                    Text(
                                      "",
                                    ),
                                    Text(
                                      "",
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 1.0,
                    color: Colors.black12,
                  ),
                  Flexible(
                    child: DefaultTabController(
                        length: 3, // length of tabs
                        initialIndex: 0,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Container(
                                decoration: const BoxDecoration(
                                  color: Color(0xffedf2f7),
                                  // boxShadow: [
                                  //   BoxShadow(
                                  //     color: Colors.grey.withOpacity(0.5),
                                  //     spreadRadius: 1,
                                  //     blurRadius: 3,
                                  //     offset: Offset(0, 2), // changes position of shadow
                                  //   ),
                                  // ],
                                ),
                                height: 50,
                                padding: EdgeInsets.all(5),
                                child: TabBar(
                                  labelColor: Colors.black,
                                  unselectedLabelColor: Colors.grey,
                                  indicator: RectangularIndicator(
                                    color: Colors.white,
                                    bottomLeftRadius: 5,
                                    bottomRightRadius: 5,
                                    topLeftRadius: 5,
                                    topRightRadius: 5,
                                  ),
                                  tabs: const [
                                    Tab(text: 'Assets'),
                                    Tab(text: 'Liabilities'),
                                    Tab(text: 'Equity'),
                                  ],
                                ),
                              ),
                              Container(
                                height: 1.0,
                                color: Colors.black12,
                              ),
                              Flexible(
                                child: TabBarView(children: <Widget>[
                                  EntityList(
                                      listData: cont.balanceSheet!.currentAsset),
                                  EntityList(
                                      listData:
                                      cont.balanceSheet!.currentLiabilities),
                                  EntityList(
                                      listData: cont.balanceSheet!.otherEquity),
                                ]),
                              )
                            ])),
                  ),
                ]),
        ),

      ),
    );
  }
}
