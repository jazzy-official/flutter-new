import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'login_screen.dart';
import 'login_screen.dart';

class PasswordReset extends StatefulWidget {
  @override
  _PasswordResetState createState() => _PasswordResetState();
}

class _PasswordResetState extends State<PasswordReset> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  bool _autoValidate = false;

  TextEditingController passController = TextEditingController();

  TextEditingController rePassController = TextEditingController();

  String? pass;

  String? retypepass;

  bool _passwordVisible1 = false;

  bool _passwordVisible2 = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyResources.backgroundColor,
      // appBar: AppBar(
      //   title: Container(
      //     child: Text(
      //       'Verification',
      //     ),
      //   ),
      //   centerTitle: true,
      // ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidate: true,
          child: Column(children: [
            Padding(
              padding: EdgeInsets.only(top: 20, bottom: 0),
              child: Container(
                child: Image.asset(
                  "assets/logo.png",
                  width: 300,
                  height: 200,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 15),
              child: Container(
                child: Text(
                  'Password Reset',
                  style: MyResources.appHeadingStyle,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20.0, left: 10, right: 10),
              child: Text(
                "Enter new password for your account.",
                style: MyResources.appTextStyle,
              ),
            ),
            Padding(
              //Add padding around textfield
              padding: EdgeInsets.only(top: 15.0, left: 10, right: 10),
              child: TextFormField(
                keyboardType: TextInputType.text,
                obscureText: !_passwordVisible1,
                controller: passController, //// pass Controller
                validator: (val) {
                  if (val!.length == 0) {
                    return ('Please enter password');
                  } else if (val.length <= 7) {
                    return ('Your password must be at least 8 characters long.');
                  } else {
                    return null;
                  }
                },
                onSaved: (val) => pass = val,
                decoration: InputDecoration(
                  //Add th Hint text here.
                  hintText: "Password",
                  hintStyle: MyResources.hintfontStyle,
                  suffixIcon: IconButton(
                      icon: Icon(
                        // Based on passwordVisible state choose the icon
                        _passwordVisible1
                            ? Icons.visibility
                            : Icons.visibility_off,
                        color: Colors.blue,
                      ),
                      onPressed: () {
                        // Update the state i.e. toogle the state of passwordVisible variable
                        setState(() {
                          _passwordVisible1 = !_passwordVisible1;
                        });
                      }),
                  prefixIcon: Icon(
                    Icons.lock,
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
            Padding(
              //Add padding around textfield
              padding: EdgeInsets.only(top: 15.0, left: 10, right: 10),
              child: TextFormField(
                keyboardType: TextInputType.text,
                obscureText: !_passwordVisible2,
                controller: rePassController,
                validator: (val) {
                  if (val!.length == 0) {
                    return ('Please enter password');
                  } else if (val != passController.text) {
                    return ('Password not match');
                  }
                  return null;
                },
                onSaved: (val) => retypepass = val,

                //pass Confirmation controller
                decoration: InputDecoration(
                  //Add th Hint text here.
                  hintText: "Retype Password",
                  hintStyle: MyResources.hintfontStyle,
                  prefixIcon: Icon(Icons.lock),
                  suffixIcon: IconButton(
                      icon: Icon(
                        // Based on passwordVisible state choose the icon
                        _passwordVisible2
                            ? Icons.visibility
                            : Icons.visibility_off,
                        color: Colors.blue,
                      ),
                      onPressed: () {
                        // Update the state i.e. toogle the state of passwordVisible variable
                        setState(() {
                          _passwordVisible2 = !_passwordVisible2;
                        });
                      }),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 50.0, left: 10, right: 10),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.9,
                height: 40,
                child: RaisedButton(
                  textColor: Colors.white,
                  color: MyResources.buttonColor,
                  child: Text(
                    "Reset",
                  ),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      Get.to(
                        LoginPage(),
                      );
                    }
                  },
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
