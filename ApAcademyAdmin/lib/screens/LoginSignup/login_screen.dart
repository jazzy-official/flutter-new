import 'package:ap_academy_admin/ModelView/Controller/auth_controller.dart';
import 'package:ap_academy_admin/screens/LoginSignup/forgetpassword.dart';
import 'package:ap_academy_admin/screens/drawer_sreen.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:get/get.dart';

import '../../constants/util.dart';
import 'signup_screen.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  bool _autoValidate = false;

  String? _email;

  String? _pass;

  /////////////////////////////////////GetX Controller///////////////////////////////////
  AuthController authController = new AuthController();

  /////////////////////////////////Controllers////////////////////////////////////////////////////////
  TextEditingController _emailControllerlogin = TextEditingController();

  TextEditingController _passControllerlogin = TextEditingController();

///////////////////////////////////Pass Visibility//////////////////////////////
  bool _passwordVisible1 = true;

  /////////////////////////////Validator

  bool validator = true;

///////////////////////////////////Firebase Auth////////////////////////////////////////////////////////

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyResources.backgroundColor,
      // appBar: AppBar(
      //   title: Container(
      //     child: Text(
      //       'Login',
      //     ),
      //   ),
      //   centerTitle: true,
      // ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 50, bottom: 0),
                child: Container(
                  child: Image.asset(
                    "assets/logo.png",
                    width: 250,
                    height: 250,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 0.0),
                child: Center(
                  child: Text(
                    "Welcome Back!",
                    style: MyResources.appHeadingStyle,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: Center(
                  child: Text(
                    "Login to your existing account ",
                    style: MyResources.appTextStyle,
                  ),
                ),
              ),
              Padding(
                //Add padding around textfield
                padding: EdgeInsets.only(top: 15.0, left: 10, right: 10),
                child: TextFormField(
                  controller: _emailControllerlogin,
                  keyboardType: TextInputType.emailAddress,
                  validator: (val) {
                    if (val!.length == 0)
                      return "Please enter email";
                    else if (!val.isEmail)
                      return "Please enter valid email";
                    else
                      return null;
                  },
                  onSaved: (val) => _email = val,
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    hintText: "Email",
                    hintStyle: MyResources.hintfontStyle,
                    prefixIcon: Icon(Icons.email),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
              Padding(
                //Add padding around textfield
                padding: EdgeInsets.only(top: 15.0, left: 10, right: 10),
                child: TextFormField(
                  controller: _passControllerlogin,
                  validator: (val) {
                    if (val!.length == 0) {
                      return ('Please enter password');
                    } else if (val.length <= 7) {
                      return ('your password must be atleast 8 chracters ');
                    }
                    return null;
                  },
                  onSaved: (val) => _pass = val,
                  obscureText: _passwordVisible1,
                  decoration: InputDecoration(
                    //Add th Hint text here.

                    hintText: "Password",
                    hintStyle: MyResources.hintfontStyle,
                    prefixIcon: Icon(Icons.lock),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    suffixIcon: IconButton(
                        icon: Icon(
                          // Based on passwordVisible state choose the icon
                          _passwordVisible1
                              ? Icons.visibility
                              : Icons.visibility_off,
                          color: _passwordVisible1 ? Colors.grey : Colors.blue,
                        ),
                        onPressed: () {
                          // Update the state i.e. toogle the state of passwordVisible variable
                          setState(() {
                            _passwordVisible1 = !_passwordVisible1;
                          });
                        }),
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  Get.to(() => forgetpassword());
                },
                child: Padding(
                  padding: const EdgeInsets.only(top: 10.0, left: 200.0),
                  child: Center(
                    child: Text(
                      "Forgot Password?",
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        height: 1.5,
                        fontSize: 14,
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15.0, left: 10, right: 10),
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.9,
                  height: 40,
                  child: RaisedButton(
                    textColor: Colors.white,
                    color: MyResources.buttonColor,
                    child: Text(
                      "Login",
                    ),
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        await authController.signInwithEmail(
                            _emailControllerlogin.text,
                            _passControllerlogin.text);
                      }
                    },
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
              // Padding(
              //   padding: EdgeInsets.only(top: 15.0, left: 10, right: 10),
              //   child: Container(
              //     width: MediaQuery.of(context).size.width * 0.9,
              //     height: 40,
              //     child: RaisedButton(
              //         textColor: Colors.white,
              //         color: MyResources.buttonColor,
              //         child: Text(
              //           "register",
              //         ),
              //         onPressed: () {
              //           Get.to(SignupPage());
              //         }),
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}
