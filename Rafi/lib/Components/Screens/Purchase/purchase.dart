import 'package:flutter/material.dart';
import 'package:rafi_admin/Components/AppBar/appbar.dart';
import 'package:rafi_admin/Components/Drawer/ProjectModel/project.dart';
import 'package:rafi_admin/Components/Drawer/drawer.dart';
import 'package:rafi_admin/Components/Screens/Purchase/SubScreens/Investments/investment_page.dart';

class PurchasePage extends StatefulWidget {

PurchasePage({Key? key,}) : super(key: key);

  @override
  _PurchasePageState createState() => _PurchasePageState();
}

class _PurchasePageState extends State<PurchasePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Purchase", style: const TextStyle(
            fontSize: 20.0,
            fontWeight: FontWeight.w500,
            color: Colors.white),),
        backgroundColor: Color(0xff004251),
      ),
      drawer: const Drawer(child: SideDrawer()),
      body: InvestmentsPage(),
    );
  }
}
