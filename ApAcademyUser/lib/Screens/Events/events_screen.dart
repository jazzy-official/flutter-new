
import 'package:ap_academy_new/Constants/util.dart';
import 'package:ap_academy_new/Screens/Events/other_event.dart';
import 'package:flutter/material.dart';

import 'match_screen.dart';

class EventScreen extends StatefulWidget {
  @override
  _EventScreenState createState() => _EventScreenState();
}

class _EventScreenState extends State<EventScreen> {

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: myResources.buttonColor,
            toolbarHeight: 5,
            bottom: const TabBar(tabs: [
              Tab(
                text: 'MATCHES',
              ),
              Tab(
                text: 'OTHERS',
              ),
            ]),
          ),
          body: TabBarView(
            children: [
              MatchScreen(),
              OtherScreen(),
            ],
          ),
        ));
  }
}
