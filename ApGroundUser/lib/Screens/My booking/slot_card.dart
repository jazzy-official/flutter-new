import 'package:ap_booking_user/Screens/Grounds/Models/slot_model.dart';
import 'package:ap_booking_user/constants/util.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'Models/booking_model.dart';

class SlotCard extends StatelessWidget {
  final Slot slot;
  final Slot? temSlot;
  final ValueChanged<Slot> onSlotSelect;
   SlotCard({Key? key, required this.slot, this.temSlot, required this.onSlotSelect}) : super(key: key);

  bool slotbool = false;
  late Booking tempbooking;
  @override
  Widget build(BuildContext context) {

    return Card(
      elevation: 4,
      child: Container(
        decoration: BoxDecoration(
            color: const Color(0xfff1f2f3),
            border: Border.all(width: 1,color: MyResources.buttonColor),
            borderRadius: BorderRadius.circular(3)
        ),
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Rs."+slot.price.toString()+"/-",style: TextStyle(fontWeight: FontWeight.bold),),
                Text(DateFormat('h: mm a')
                    .format(slot.startTime)+"-"+DateFormat('h: mm a')
                    .format(slot.startTime.add(Duration(hours:slot.hours))),style: TextStyle(fontWeight: FontWeight.bold),),
              ],
            ),
            SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: Colors.blue.shade50,
                      borderRadius: BorderRadius.circular(3),
                      border: Border.all(color: Colors.blue)
                  ),
                  padding: EdgeInsets.all(5),
                  child: Wrap(children:[Text("Select "),Container(
                    height: 18,
                    width: 20,
                    child:
                    Radio<Slot>(
                      value: slot,
                      groupValue: temSlot,
                      onChanged: (value) {
                        onSlotSelect(value!);
                      },
                    ),
                  ),] ),
                ),
                if(slot.Pslot1 != null)Column(
                  children: [
                    Text("First Slot is booked by",style: TextStyle(fontSize: 11,),),
                    Text(slot.Pslot1!.teamName.toUpperCase(),style: TextStyle(fontSize: 11,color: Colors.red,fontWeight: FontWeight.bold),),
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

