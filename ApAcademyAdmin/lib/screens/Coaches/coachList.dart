import 'package:ap_academy_admin/ModelView/Controller/CoachController.dart';
import 'package:ap_academy_admin/ModelView/Controller/profileController.dart';
import 'package:ap_academy_admin/screens/Coaches/AddAdminPage.dart';
import 'package:ap_academy_admin/screens/shared/DeletPop.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:get/get.dart';
import 'package:get/route_manager.dart';

import 'detailcard.dart';

class CoachList extends StatefulWidget {
  @override
  _CoachListState createState() => _CoachListState();
}

class _CoachListState extends State<CoachList> {
  final profileController = Get.put(ProfileController());
  final coachContr = Get.put(CoachController());
  @override
  void initState() {
    coachContr.refreshcoach();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      //Create an event add button
      child: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "",
                    style: MyResources.appHeadingStyle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    width: Get.width * 0.2,
                    height: 25,
                    child: RaisedButton(
                      textColor: Colors.white,
                      color: MyResources.buttonColor,
                      child: Text(
                        "ADD",
                      ),
                      onPressed: () {
                        final userC = Get.put(ProfileController());
                        if (userC.userDetail['role'] == 'Admin'|| userC.userDetail['role'] == 'SuperAdmin')
                          Get.to(
                            () => AddAdminPage(
                              role: 'Coach',
                            ),
                          );
                        else {
                          Get.snackbar('Admin Error',
                              'You are not admin, you can not add Coach',
                              snackPosition: SnackPosition.BOTTOM);
                        }
                      },
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Container(
              height: Get.height,
              child: GetX<CoachController>(
                init: Get.put<CoachController>(CoachController()),
                builder: (CoachController coachController) {
                  if (coachController != null &&
                      coachController.coaches != null) {
                    print(coachController.coaches.toString());
                    return Container(
                      height: Get.height,
                      child: Stack(
                        children: [
                          GridView.count(
                            keyboardDismissBehavior:
                                ScrollViewKeyboardDismissBehavior.onDrag,
                            crossAxisCount: 2,
                            children: List.generate(
                              coachController.coaches.length,
                              (index) {
                                return getcoachCard(
                                    coachController.coaches[index]);
                              },
                            ),
                          ),
                          // Center(
                          //     child: Image.asset(
                          //   'assets/coach.png',
                          //   width: 200,
                          //   height: 200,
                          // ))
                        ],
                      ),
                    );
                  } else {
                    return Container(
                      child: LoadingWidget(),
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  // getcoachCard(Map<String, dynamic> coachModel) {
  //   return Padding(
  //     padding: const EdgeInsets.all(4.0),
  //     child: InkWell(
  //       onTap: () {
  //         Get.dialog(DetailAdmincard(
  //           detailData: coachModel,
  //         ));
  //       },
  //       child: Container(
  //         color: Colors.black12,
  //         child: Column(
  //           children: [
  //             SizedBox(
  //               height: 10,
  //             ),
  //             Container(
  //               child: ClipRRect(
  //                 borderRadius: BorderRadius.circular(50),
  //                 child: coachModel['photoUrl'] != ''
  //                     ? Image.network(
  //                         coachModel['photoUrl'],
  //                         width: 50,
  //                         height: 50,
  //                         fit: BoxFit.fitHeight,
  //                       )
  //                     : Center(
  //                         child: Icon(
  //                         Icons.image_rounded,
  //                         size: 50,
  //                       )),
  //               ),
  //             ),
  //             SizedBox(
  //               height: 20,
  //             ),
  //             Text(coachModel['name'])
  //           ],
  //         ),
  //       ),
  //     ),
  //   );
  // }
  getcoachCard(Map<String, dynamic> sponsorModel) {
    return Stack(
      children:[
        Padding(
        padding: const EdgeInsets.all(8.0),
        child:InkWell(
          onTap: () {
            Get.dialog(DetailAdmincard(
              detailData: sponsorModel,
            ));
          },
          child: Container(
            //color: Colors.black12,
            child: Column(
              children: [

                Container(
                  height:MediaQuery.of(context).size.height*0.15,
                  width:MediaQuery.of(context).size.width*0.4,color: Colors.black12,
                  child: ClipRRect(
                    // borderRadius: BorderRadius.circular(15),
                    child: sponsorModel['photoUrl'] != ''
                        ? Image.network(
                      sponsorModel['photoUrl'],
                      // width: Get.width,
                      // height:100,
                      fit: BoxFit.fitHeight,
                    )
                        : Center(
                        child: Icon(
                          Icons.image_rounded,
                          size: 100,
                        )),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(width: 120,
                  child: Text(
                    sponsorModel['name'].toString().capitalize!,overflow: TextOverflow.ellipsis,textAlign: TextAlign.center,
                    style: MyResources.titleTextStyle,
                  ),
                ),

              ],
            ),
          ),
        ),
      ),
          Positioned(
              top: 15,
              right: 35,
              child: Container(
                height: 10,
                width: 10,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: sponsorModel["isActive"]?Colors.green:Colors.grey,
                ),
              )),
      ]
    );
  }

}
