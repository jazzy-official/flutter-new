import 'package:daily_log/Models/task_model.dart';
import 'package:daily_log/View/Tasks/Add%20Tasks/add_task.dart';
import 'package:daily_log/View/Tasks/task_list.dart';
import 'package:daily_log/constants/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../Controllers/task_controller.dart';

class TasksScreen extends StatelessWidget {
   TasksScreen({Key? key}) : super(key: key);
  var taskCont = Get.put(TaskController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Get.to(AddTask());
        },
        child: const Icon(Icons.add),
        backgroundColor: MyResources.themecolor,
      ),
      body: GetBuilder<TaskController>(
          autoRemove: false,
          init: TaskController(),
          builder: (cont) => RefreshIndicator(
            onRefresh: () { return cont.readBall(); },
            child: TaskList(tasks: cont.tasks,),
          )),
    );
  }
}
