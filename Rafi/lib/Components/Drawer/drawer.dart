import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:rafi_admin/Components/Navigation/navigation_bar.dart';
import 'package:rafi_admin/Components/loader_widget.dart';
import 'package:rafi_admin/Controllers/accounts_controller.dart';
import 'package:rafi_admin/Controllers/authcontroller.dart';
import 'package:rafi_admin/Controllers/drawer_controller.dart';
import 'package:rafi_admin/Controllers/expense_controller.dart';
import 'package:rafi_admin/Controllers/payables_controller.dart';
import 'package:rafi_admin/Controllers/receiveable_controller.dart';
import 'package:rafi_admin/Controllers/recovery_controller.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'ProjectModel/project.dart';

class SideDrawer extends StatefulWidget {
  const SideDrawer({Key? key,
  }) : super(key: key);

  @override
  State<SideDrawer> createState() => _SideDrawerState();
}

class _SideDrawerState extends State<SideDrawer> {
  final authcontroller = Get.put(AuthController());


  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return  Column(
      children: [
        DrawerHeader(
          decoration: const BoxDecoration(
            color: Color(0xff004251),
          ),
          child: Column(
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.11,
                // width: 100,
                child: Image.asset('assets/images/rafilogo.png'),
              ),
              SizedBox(
                height: 10,
                width: MediaQuery.of(context).size.width * 1,
              ),
              const Text(
                'Rafi Developers',
                style: TextStyle(color: Colors.white, fontSize: 18),
              ),
            ],
          ),
        ),
        Expanded(
          child:GetBuilder<DrawerrController>(
            autoRemove: false,
            init: DrawerrController(),
            builder: (cont) => cont.projects==null ? LoadingWidget() :  ListView.separated(
              // Important: Remove any padding from the ListView.
              padding: EdgeInsets.zero,
              itemCount: cont.projects!.length,
              itemBuilder: (BuildContext context, int index) {
                final tile = cont.projects![index];
                return TextButton(
                  onPressed: () async {
                    SharedPreferences prefs = await SharedPreferences.getInstance();
                    await prefs.setInt('projectId', tile.ProjectId);
                    await Get.delete<AccountController>();
                    await Get.delete<PayableController>();
                    await Get.delete<ReceivableController>();
                    await Get.delete<RecoveryController>();
                    await Get.delete<ExpenseController>();
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Navigation(Id: tile.ProjectId,)),
                    );
                  },
                  child: Text(tile.ProjectName),
                );
              },
              separatorBuilder: (BuildContext context, int index) =>
              const Divider(),
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            TextButton(
              onPressed: () async {
               authcontroller.logout();
              },
              child: Container(
                decoration: BoxDecoration(
                  color: const Color(0xbb004251),
                  borderRadius: BorderRadius.circular(10)
                ),
                
                height: 40,
                width: 120,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Text(
                      "Logout",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontSize: 14),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Icon(
                      Icons.logout,
                      color: Colors.white,
                    ),
                  ],
                ),
              ),
            )
          ],
        )
      ],
    );
  }
}
