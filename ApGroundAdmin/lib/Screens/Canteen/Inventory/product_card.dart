import 'package:ap_ground_admin/Screens/Canteen/Models/report_model.dart';
import 'package:ap_ground_admin/constants/util.dart';
import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:ap_ground_admin/controllers/inventory_controller.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';

import '../Models/product_model.dart';

class ProductCard extends StatefulWidget {
  final Product product;
  final Function() refresh;
  const ProductCard({Key? key, required this.product, required this.refresh}) : super(key: key);

  @override
  _ProductCardState createState() => _ProductCardState();
}


class _ProductCardState extends State<ProductCard> {
  int tempqty = 0;
  List<int> factorlist=[1,5,10,20,50];
  int factor = 1;

  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.blue.shade50,
      child: Container(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Row(
              children:  [
                Text(widget.product.name.toUpperCase(),style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),
                Text("/(Rs-"+formatnmbr(widget.product.price.toDouble())+")",style: TextStyle(fontSize: 12),),

              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Quantity : "+widget.product.qty.toString(),style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    BouncingWidget(
                      scaleFactor: 4.0,
                      onPressed: () async {
                        setState(() {
                          tempqty++;
                        });


                      },
                      child: Container(
                        decoration: BoxDecoration(
                          color: MyResources.buttonColor,
                          shape: BoxShape.circle,
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(3.0),
                          child: Icon(Icons.add,color: Colors.white,),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Text(tempqty.toString().toUpperCase(),style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16,color: Colors.green),),
                    ),
                    BouncingWidget(
                      scaleFactor: 4.0,
                      onPressed: () async {
                        setState(() {
                          tempqty--;
                        });
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          color: MyResources.buttonColor,
                          shape: BoxShape.circle,
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(3.0),
                          child: Icon(Ionicons.remove,color: Colors.white,),
                        ),
                      ),
                    ),
                    SizedBox(width: 15,),
                    BouncingWidget(
                      scaleFactor: 4.0,
                      onPressed: () async {
                        List<ReportModel> reports = widget.product.reports;
                        reports.add(ReportModel(date: DateTime.now(),inc: tempqty));
                        await InventoryController().updateProduct(Product(name: widget.product.name, reports: reports, price: widget.product.price, qty: widget.product.qty+tempqty,id: widget.product.id));

                        widget.refresh();
                        displayToast("Product Inventory Updated");
                        setState(() {
                          tempqty =0;
                        });
                        },
                      child: Container(
                        decoration: BoxDecoration(
                          color: MyResources.buttonColor,
                          shape: BoxShape.circle,
                        ),
                        child: Padding(
                            padding: const EdgeInsets.all(7.0),
                            child: Text("Ok",style: TextStyle(color: Colors.white),),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
