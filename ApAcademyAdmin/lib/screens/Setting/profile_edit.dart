import 'dart:io';

import 'package:image_picker/image_picker.dart';
import 'package:ap_academy_admin/ModelView/Controller/profileController.dart';
import 'package:ap_academy_admin/Models/UserModel.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import '../drawer_sreen.dart';

class ProfileEdit extends StatefulWidget {
  final Map<String, dynamic>? usrDetails;
  final bool? isDrawer;
  const ProfileEdit({Key? key, this.usrDetails, this.isDrawer})
      : super(key: key);

  @override
  _ProfileEditState createState() => _ProfileEditState();
}

class _ProfileEditState extends State<ProfileEdit> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final profileController = Get.put(ProfileController());
  bool _autoValidate = false;
  UserModel? user;
  String? _error;
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController surnameController = TextEditingController();
  TextEditingController dobController = TextEditingController();
  List<String> emails = [];
  bool isImage = true;
  XFile? _image;
  List<PlatformFile> images = <PlatformFile>[];
  DateTime _dateOfBirth = DateTime(1987) ;



  String imageString = '';
  @override
  void initState() {
    super.initState();

    if (widget.usrDetails != null) {
      emailController.text = widget.usrDetails!['email'];
      nameController.text = widget.usrDetails!['name'];
      surnameController.text = widget.usrDetails!['surname'];
      phoneController.text = widget.usrDetails!['phone'];
        _dateOfBirth = widget.usrDetails!['dob'];
        dobController.text = widget.usrDetails!['dob'];
      imageString = widget.usrDetails!['photoUrl']??'';
    } else {
      emailController.text =
          profileController.userDtails.value['email'].toString();
      nameController.text =
          profileController.userDtails.value['name'].toString();
      surnameController.text = profileController.userDtails.value['surname']??"";
      phoneController.text =
          profileController.userDtails.value['phone'].toString();
      imageString = profileController.userDtails.value['photoUrl']??"";
      dobController.text = profileController.userDtails.value['dob'] != null
          ? DateFormat.yMEd().format(profileController.userDtails.value['dob'])
          : '';
       _dateOfBirth = profileController.userDtails.value['dob'] != null
          ? profileController.userDtails.value['dob']
          : DateTime.now();


    }
  }

  Future<void> pickImages() async {
    setState(() {
      isImage = true;
    });
    //List<File> resultList = new List<File>();
    String? error = null;
    try {
      FilePickerResult? result = await FilePicker.platform.pickFiles(
        allowMultiple: true,
        type: FileType.image,
      );
      images = result!.files;
    } on PlatformException catch (e) {
      error = e.message!;
      setState(() {
        isImage = false;
      });
    }
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;
    setState(() {
      if (error == null) _error = 'No Error Dectected';
    });
  }

  @override
  void dispose() {
    nameController.dispose();
    emailController.dispose();
    dobController.dispose();
    phoneController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: widget.isDrawer != null
          ? AppBar(
        backgroundColor: MyResources.buttonColor,
              title: Container(
                child: Text(
                  'EDIT PROFILE',
                  style: GoogleFonts.roboto(),
                ),
              ),
              centerTitle: true,
            )
          : PreferredSize(
              preferredSize: Size.fromHeight(0.0), child: Container()),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              Center(
                child: GestureDetector(
                  onTap: () {
                    _showPicker(context);
                  },
                  child: CircleAvatar(
                    radius: 55,
                    backgroundColor: MyResources.buttonColor,
                    child: _image != null
                        ? ClipRRect(
                            borderRadius: BorderRadius.circular(50),
                            child: Image.file(
                              File(_image!.path),
                              width: 100,
                              height: 100,
                              fit: BoxFit.cover,
                            ),
                          )
                        : imageString != ""
                            ? ClipRRect(
                                borderRadius: BorderRadius.circular(50),
                                child: Image.network(
                                  imageString,
                                  width: 100,
                                  height: 100,
                                  fit: BoxFit.cover,
                                ),
                              )
                            : Container(
                                decoration: BoxDecoration(
                                    color: Colors.grey[200],
                                    borderRadius: BorderRadius.circular(50)),
                                width: 100,
                                height: 100,
                                child: Icon(
                                  Icons.camera_alt,
                                  color: Colors.grey[800],
                                ),
                              ),
                  ),
                ),
              ),
              //add the textfields to edit the profile information
              Padding(
                //Add padding around textfield
                padding: EdgeInsets.only(top: 25.0, left: 10, right: 10),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  validator: (String? arg) {
                    if (arg!.length < 3)
                      return 'Name must be more than 2 charater';
                    else
                      return null;
                  },
                  controller: nameController,
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    hintText: "Name",
                    hintStyle: MyResources.hintfontStyle,
                    prefixIcon: Icon(Icons.account_circle),

                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
              Padding(
                //Add padding around textfield
                padding: EdgeInsets.only(top: 25.0, left: 10, right: 10),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  validator: (String? arg) {
                    if (!arg!.contains("@"))
                      return 'Enter a valid email address';
                    else
                      return null;
                  },
                  controller: emailController,
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    hintText: "Email",
                    hintStyle: MyResources.hintfontStyle,
                    prefixIcon: Icon(Icons.account_circle),

                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
              Padding(
                //Add padding around textfield
                padding: EdgeInsets.only(top: 25.0, left: 10, right: 10),
                child: TextFormField(
                  // validator: (String? value) {
                  //   if (value!.length == 0) {
                  //     return 'Please enter mobile number';
                  //   } else if (!value.isPhoneNumber) {
                  //     return 'Please enter valid mobile number';
                  //   }
                  //   return null;
                  // },
                  controller: phoneController,
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    hintText: "Phone",
                    hintStyle: MyResources.hintfontStyle,
                    prefixIcon: Icon(Icons.phone),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
              MaterialButton(
                onPressed: () async {
                  _dateOfBirth = (await showDatePicker(
                    // initialEntryMode: DatePickerEntryMode.input,
                    context: context,
                    currentDate: DateTime.now(),
                    initialDate: DateTime(1980),
                    lastDate: DateTime(2015),
                    initialDatePickerMode: DatePickerMode.year,
                    firstDate: DateTime(1980),
                  ))!;
                  dobController.text =
                      DateFormat('yyyy-MM-dd').format(_dateOfBirth);
                },
                child: Padding(
                  //Add padding around textfield
                  padding: EdgeInsets.only(top: 25.0, left: 0, right: 0),
                  child: Container(
                    child: TextFormField(
                      enabled: false,
                      keyboardType: TextInputType.text,
                      validator: (String? arg) {
                        if (arg!.length < 3)
                          return 'Select date of Birth';
                        else
                          return null;
                      },
                      readOnly: true,
                      controller: dobController,
                      decoration: InputDecoration(
                        //Add th Hint text here.
                        hintText: "Date of Birth",
                        hintStyle: MyResources.hintfontStyle,
                        prefixIcon: Icon(Icons.calendar_today_outlined),

                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                    ),
                  ),
                ),
              ),

              Padding(
                padding:
                    EdgeInsets.only(top: 15.0, left: 10, right: 10, bottom: 20),
                child: Container(
                  width: Get.width * 0.7,
                  height: 45,
                  child: RaisedButton(
                    textColor: Colors.white,
                    color: MyResources.buttonColor,
                    child: Text(
                      "Save",
                    ),
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        Map<String, dynamic> _user = {
                          'email': emailController.text,
                          'phone': phoneController.text,
                          'name': nameController.text,
                          'surname': surnameController.text,
                          'dob': _dateOfBirth
                        };
                        Get.defaultDialog(
                            title: "Saving Information ",
                            content: LoadingWidget());

                        bool temp =
                            await profileController.saveProfile(_image!=null?File(_image!.path):null, _user);
                        print(temp);
                        if (temp) {
                          await profileController.refreshDate();
                          Get.back();
                          Get.offAll(
                            HomeDrawer(),
                          );
                          final m = Get.put(ProfileController());
                          await m.refreshDate();
                        } else {
                          Get.snackbar(
                              "Error", "User information is not updated");
                        }
                      }
                    },
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () async {
                        Get.back();
                        await profileController.imgFromGallery().then((value) {
                          _image = value;
                          if (mounted) setState(() {});
                        });
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () async {
                      Get.back();
                      await profileController.imgFromCamera().then((value) {
                        _image = value;
                        if (mounted) setState(() {});
                      });
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }
}
