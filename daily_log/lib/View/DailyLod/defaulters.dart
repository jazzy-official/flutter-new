import 'package:daily_log/Controllers/default_controller.dart';
import 'package:daily_log/Models/defaulters.dart';
import 'package:daily_log/constants/util.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:intl/intl.dart';

class DefaulterReportScreen extends StatelessWidget {
  const DefaulterReportScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyResources.themecolor,
      ),

      body: GetBuilder<DefaulterController>(
          autoRemove: false,
          init: DefaulterController(),
          builder: (cont) => Column(
            children: [
              Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(onPressed: (){
                  cont.number++;
                  cont.filtercomments();
                }, icon: const Icon(Icons.chevron_left)),
                Text(DateFormat("MMMM dd, yyyy").format(DateTime.now().subtract(Duration(days: 7 * cont.number)))),
                const Text("  -  "),
                Text(DateFormat("MMMM dd, yyyy").format(DateTime.now().subtract(Duration(days: 7 * (cont.number-1))))),
                IconButton(onPressed: cont.number==1?null: (){
                  cont.number--;
                  cont.filtercomments();
                }, icon: const Icon(Icons.chevron_right)),
              ],
            ),
              Expanded(
                child: ListView.builder(
                  physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                  itemCount: cont.defaultedUsers.length,
                  itemBuilder: (context, index) {
                    DefaultedUser user = cont.defaultedUsers[index];
                    return InkWell(
                      child: Card(
                        elevation: 3,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  const Text(
                                    "Name : ",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 16),
                                  ),
                                  Text(
                                    user.name.toUpperCase(),
                                    style: const TextStyle(
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const Text(
                                    "Defaulted Dates : ",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600),
                                  ),
                                  Expanded(
                                    child: ListView.builder(
                                      shrinkWrap: true,
                                      controller: ScrollController(),
                                      itemCount: user.date.length,
                                      itemBuilder: (context, index) {
                                        DateTime date = user.date[index];
                                        return Text(DateFormat("dd-MMM-yyyy")
                                            .format(date)
                                        );
                                      },
                                    ),
                                  ),

                                ],
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          )),
    );
  }
}
