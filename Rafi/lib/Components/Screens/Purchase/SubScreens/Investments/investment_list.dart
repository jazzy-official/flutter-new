import 'package:flutter/material.dart';
import 'package:rafi_admin/Components/Screens/Purchase/Model/investment_model.dart';

import 'investment_card.dart';

class InvestmentList extends StatelessWidget {
  final List<InvestmentModel>investments;
  const InvestmentList({Key? key, required this.investments}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: const BouncingScrollPhysics(
          parent: AlwaysScrollableScrollPhysics()),
      itemCount: investments.length,
      itemBuilder: (context, i) {
        final investment = investments[i];
        return InvestmentCard(investment: investment,);
      },
    );
  }
}
