import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Balance%20Sheet/Modals/entity_model.dart';

class EntityCard extends StatelessWidget {
  final Entity entity;
 EntityCard({Key? key, required this.entity}) : super(key: key);
  var formatter = NumberFormat('#,##,000');
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 4,
      child: Container(
        padding: const EdgeInsets.all(8.0),
        height: 50,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              entity.name,
              style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),
            ),
            Text(formatter.format(entity.amount)+"/-",),
          ],
        ),
      ),
    );
  }
}
