import 'dart:convert';

import 'package:ap_academy_admin/ModelView/Controller/GroupController.dart';
import 'package:ap_academy_admin/ModelView/Controller/NotificationController.dart';
import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:ap_academy_admin/ModelView/Controller/matchController.dart';
import 'package:ap_academy_admin/ModelView/Controller/teamsController.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:mongo_dart/mongo_dart.dart' as mongo;
class CreateMatch extends StatefulWidget {
  final Map<String, dynamic>? matchModel;

  const CreateMatch({Key? key,this.matchModel}) : super(key: key);
  @override
  _CreateMatchState createState() => _CreateMatchState();
}

class _CreateMatchState extends State<CreateMatch> {
  TextEditingController timeController = TextEditingController();
  TextEditingController venueController = TextEditingController();
  TextEditingController matchNameController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController team1NameController = TextEditingController();
  TextEditingController team2NameController = TextEditingController();
  TextEditingController noteController = TextEditingController();
  final mController = Get.put(MatchController());
  final tController = Get.put(TeamController());
  final notifyController = Get.put(NotificationController());
  int? ageSt;
  bool team1 = false, team2 = false;
  ///////////////////////////------------------/////////////////////////////////////
  List<String> allToken = [];
  String? _matchType = null;
  DateTime _selectedDate = DateTime.now();
///////////////////////////-------------------//////////////////////////////////
  String? selectedg1Value;
  String? selectedg2Value;
  String? selectedCoachValue;

  // Future getsheetfun() async {
  //   http.Response response = await http.get(
  //     Uri.parse('https://sheetdb.io/api/v1/5xeq0u2whmy1p'),
  //     headers: <String, String>{
  //       'Content-Type': 'application/json; charset=UTF-8',
  //     },
  //   );
  //   if (response.statusCode == 200) {
  //     // If the server did return a 201 CREATED response,
  //     // then parse the JSON.
  //     List<dynamic> sheetData = await jsonDecode(response.body);
  //     print(sheetData.length);
  //     for(var data in sheetData){
  //       print(data);
  //       // await uploaduser(data["NAME"], data["EMAIL ADDRESS"], "12345678", data["CRICHQ ID"], data["HKID/PASSPORT"], DateTime.parse(data["DOB"]), data["CONTACT NO"], data["PARENT NAME"]);
  //       Map<String, dynamic> matchModel = Map();
  //       final dbHelper = Get.put(DatabaseController());
  //       matchModel['coach'] = "";
  //       matchModel['match'] = data["League"];
  //       matchModel['team1name'] = data["Home"];
  //       matchModel['team2name'] = data["Away"];
  //       matchModel['otherteam1'] = true;
  //       matchModel['otherteam2'] = true;
  //       matchModel['matchTime'] = DateTime(DateTime.parse(data['Date']).year,DateTime.parse(data['Date']).month,DateTime.parse(data['Date']).day,DateTime.parse(data['Time']).hour,DateTime.parse(data['Time']).minute);
  //       matchModel['venue'] = data["Venue"];
  //       matchModel['timeCreated'] = DateTime.now();
  //       matchModel['type'] = "Cricket";
  //       matchModel['note'] = data["Notes"];
  //       matchModel['attendance'] = [];
  //       matchModel['isApproved'] = false;
  //
  //       await dbHelper.createMatch(matchModel);
  //     }
  //
  //   } else {
  //     // If the server did not return a 201 CREATED response,
  //     // then throw an exception.
  //     throw Exception('Failed to get payable data');
  //   }
  // }



  Future<void> createNewMatch() async {
    allToken = [];
    if (validate()) {
      Get.dialog(Center(child: LoadingWidget()), barrierDismissible: false);
      Map<String, dynamic> matchModel = Map();
      final dbHelper = Get.put(DatabaseController());
      matchModel['coach'] = selectedCoachValue;
      matchModel['match'] = matchNameController.text;
      // matchModel['group1'] = selectedg1Value;
      // matchModel['group2'] = selectedg2Value;
      if(team1)
      matchModel['team1name'] = team1NameController.text;
      else
        matchModel['team1name'] = selectedg1Value;
      if(team2)
      matchModel['team2name'] = team2NameController.text;
      else
        matchModel['team2name'] = selectedg2Value;
      matchModel['otherteam1'] = team1;
      matchModel['otherteam2'] = team2;
      matchModel['matchTime'] = _selectedDate;
      matchModel['venue'] = venueController.text;
      matchModel['timeCreated'] = DateTime.now();
      matchModel['type'] = _matchType;
      matchModel['note'] = noteController.text;
      matchModel['attendance'] = [];
      matchModel['isApproved'] = false;
      if (widget.matchModel == null) {
        bool check = await dbHelper.createMatch(matchModel);
        if (check) {
          List<String> team1List = [];
          //  if (!team1){
          if(!team1) {
            await mController.getTokensByTeamgroup(selectedg1Value!).then((
                value) {
              if (value != null) {
                value.forEach((element) {
                  bool issame = false;
                  for (int i = 0; i < team1List.length; i++)
                    if (team1List[i] == element) issame = true;
                  if (!issame) team1List.add(element);
                });
              }
            });
          }
          if(!team2) {
            await mController.getTokensByTeamgroup(selectedg2Value!).then((
                value) {
              if (value != null) {
                value.forEach((element) {
                  bool issame = false;
                  for (int i = 0; i < team1List.length; i++)
                    if (team1List[i] == element) issame = true;
                  if (!issame) team1List.add(element);
                });
              }
            });
          }
          if (team1List != []) {
            final nControl = Get.put(NotificationController());
            Map<String, dynamic> notifyModel = {
              'title': matchNameController.text,
              'description': 'A new match is set, please mark your attendance'
                  "\n"+'Time: '+ DateFormat.yMEd().format(_selectedDate).toString()+
                  "\n"+  '''Venue: '''+venueController.text+"\n"+
                  '''Note: '''+noteController.text,
              'timeCreated': DateTime.now()
            };
            bool temp = await nControl.addnewNotification(notifyModel);
            await notifyController.sendAndRetrieveMessage({
              'title': 'Match created',
              'desc': 'A new match has been set. Mark your attendance'
            }, team1List);
          }

          Get.back();
          Get.back();
          await mController.refreshDate();
          Get.snackbar("Saved", "Event is Saved",
              snackPosition: SnackPosition.BOTTOM,
              icon: Icon(
                Icons.check_circle,
                color: Colors.green,
              ));
        } else {
          Get.back();
          Get.snackbar("Not Saved", "Event is not Saved",
              snackPosition: SnackPosition.BOTTOM,
              icon: Icon(
                Icons.cancel_outlined,
                color: Colors.red,
              ));
          Get.back();
        }
      } else {
        matchModel['_id'] = widget.matchModel!['_id'];
        bool check = await dbHelper.updateMatch(matchModel);
        if (check) {
          await mController.refreshDate();
          Get.back();
          Get.back();
          Get.snackbar("Saved", "Event is Saved",
              snackPosition: SnackPosition.BOTTOM,
              icon: Icon(
                Icons.check_circle,
                color: Colors.green,
              ));
        } else {
          Get.snackbar("Not Saved", "Event is not Saved",
              snackPosition: SnackPosition.BOTTOM,
              icon: Icon(
                Icons.cancel_outlined,
                color: Colors.red,
              ));
          Get.back();
        }
      }
    } else {
      // Get.back();
      // Get.snackbar("Error", "Fill all required Fields",
      //     snackPosition: SnackPosition.BOTTOM,
      //     icon: Icon(
      //       Icons.cancel_outlined,
      //       color: Colors.red,
      //     ));
    }
  }

  getMatchPopulate() {
    if (widget.matchModel != null) {
      venueController.text = widget.matchModel!['venue'];
      selectedCoachValue = widget.matchModel!['coach'];
      matchNameController.text = widget.matchModel!['match'];
      team1 = widget.matchModel!['otherteam1'];
      team2 = widget.matchModel!['otherteam2'];
      team1NameController.text = widget.matchModel!['team1name'];
      team2NameController.text = widget.matchModel!['team2name'];
      if(team1)
      selectedg1Value = widget.matchModel!['group1'];
      else
        selectedg1Value = widget.matchModel!['team1name'];
      if(team2)
      selectedg2Value = widget.matchModel!['group2'];
      else
        selectedg2Value = widget.matchModel!['team2name'];
      _selectedDate = widget.matchModel!['matchTime'];
      _matchType = widget.matchModel!['type'];
      noteController.text = widget.matchModel!['note'];

      timeController.text =
          DateFormat.yMEd().format(widget.matchModel!['matchTime']);
    }
  }

  @override
  void dispose() {
    timeController.dispose();
    venueController.dispose();
    matchNameController.dispose();
    descriptionController.dispose();
    team1NameController.dispose();
    team2NameController.dispose();
    noteController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    // getsheetfun();
    getMatchPopulate();
    super.initState();
    //generatedropList();
  }

  ////////////////////////////////////////---------------------------------///////////////////////
  List listofteam=[];
  generatedropList() {
    if (tController.teamsList.value != null) {
      listofteam = List.generate(
        tController.teamsList.value.length,
            (index) => DropdownMenuItem(
          child: Text(tController.teams[index]['teamName']),
          value: tController.teams[index]['teamName'],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyResources.buttonColor,
        title: Container(
          child: Text(
            'CREATE MATCH',
            style: GoogleFonts.roboto(),
          ),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 30.0, left: 12, bottom: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    " Match Name",
                    style: GoogleFonts.roboto(
                        fontSize: 15, fontWeight: FontWeight.w400),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 0, left: 8, right: 8),
              child: Container(
                color: MyResources.backgroundColor,
                height: 40,
                child: TextField(
                  controller: matchNameController,
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
            //select team1 vs team 2
            Padding(
              padding: const EdgeInsets.only(top: 7.0,right: 8, left: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        //crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            " Team One ",
                            style: GoogleFonts.roboto(
                                fontSize: 15, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              Checkbox(
                                  value: team1,
                                  onChanged: (_) {
                                    if(selectedg1Value==selectedg2Value)
                                      selectedg1Value =null;

                                    print(_);
                                    team1 = _!;
                                    setState(() {});
                                  }),
                              Text(
                                'Other Team',
                                style: TextStyle(fontSize: 12),
                              )
                            ],
                          )
                        ],
                      ),
                      team1 ? Container(
                          //  color: MyResources.backgroundColor,
                          width: Get.width*0.44,
                          height: 35,
                          child:TextField(
                            controller: team1NameController,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                            ),
                          ),
                        //),
                      )
                          : Container(
                        decoration: BoxDecoration(
                            color: MyResources.backgroundColor,
                            border: Border.all(color: Colors.grey),
                            borderRadius: BorderRadius.circular(10)),
                        //  color: MyResources.backgroundColor,
                        width: Get.width*0.44,
                        height: 35,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: GetX<TeamController>(
                            init: Get.put<TeamController>(
                                TeamController()),
                            builder: (TeamController teamController) {
                              if (teamController != null) {
                                if (teamController.teams.length != 0) {
                                  // if (selectedValue.value == '')
                                  //   selectedValue.value = groupController
                                  //       .groupName[0]['name']
                                  //       .toString();
                                  return DropdownButton<String>(
                                    underline: Text(""),
                                    isExpanded: true,
                                    value: selectedg1Value,
                                    focusColor: MyResources.buttonColor,
                                    elevation: 12,
                                    items: teamController.teamsList.value
                                        .map((map) {return DropdownMenuItem(child: Text(
                                      map['teamName'].toString(),
                                      style: TextStyle(fontSize: 14),
                                    ),
                                      value: map['teamName'].toString(),
                                    );}).toList(),
                                    onChanged: (String? value) {
                                      print(value);
                                      // filteruserList();
                                      if (team2==true||selectedg2Value != value) {
                                        setState(() {
                                          selectedg1Value = value;
                                        });
                                      } else {
                                        Get.rawSnackbar(
                                            title: 'Error',
                                            message:
                                            'You cannot select the same team');
                                      }},
                                    hint: Text('Select Team'),
                                  );
                                } else
                                  return Container();
                              } else
                                return Container();},
                          ),
                        ),
                      )
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        " Team Two ",
                        style: GoogleFonts.roboto(
                            fontSize: 15, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Checkbox(
                              value: team2,
                              onChanged: (_) {
                                if(selectedg1Value==selectedg2Value)
                                  selectedg2Value = null;
                                print(_);
                                team2 = _!;
                                setState(() {});
                              }),
                          Text(
                            'Other Team',
                            style: TextStyle(fontSize: 12),
                          )
                        ],
                      ),
                      team2
                          ? Container(
                        //  color: MyResources.backgroundColor,
                          width: Get.width*0.44,
                          height: 35,
                        child: Container(
                          color: MyResources.backgroundColor,
                          height: 35,
                          width: Get.width*0.44,
                          child: TextField(
                            controller: team2NameController,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                            ),
                          ),
                        ),
                      )
                          : Padding(
                        //Add padding around textfield
                        padding: EdgeInsets.only(right: 0),
                        child: Padding(
                          padding: const EdgeInsets.all(0.0),
                          child: Container(
                            decoration: BoxDecoration(
                                color: MyResources.backgroundColor,
                                border: Border.all(color: Colors.grey),
                                borderRadius: BorderRadius.circular(10)),
                            //  color: MyResources.backgroundColor,
                            width: Get.width*0.44,
                            height: 35,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: GetX<TeamController>(
                                init: Get.put<TeamController>(
                                    TeamController()),
                                builder:
                                    (TeamController teamController) {
                                  if (teamController != null) {
                                    if (teamController.teams.length != 0) {
                                      // if (selectedValue.value == '')
                                            //   selectedValue.value = groupController
                                            //       .groupName[0]['name']
                                            //       .toString();
                                      return DropdownButton<String>(
                                        underline: Text(""),
                                        isExpanded: true,
                                        value: selectedg2Value,
                                        focusColor:
                                        MyResources.buttonColor,
                                        elevation: 12,
                                        items: teamController.teams.map((map) {
                                          return DropdownMenuItem(
                                            child: Text(
                                              map['teamName'].toString(),
                                              style: TextStyle(fontSize: 14),
                                            ),
                                            value: map['teamName'].toString(),
                                          );
                                        }).toList(),
                                        onChanged: (String? value) {
                                          if (team1==true||selectedg1Value != value) {
                                            print(value);
                                            setState(() {
                                              selectedg2Value = value;
                                            });
                                          } else {
                                            Get.rawSnackbar(
                                                title: 'Error',
                                                message:
                                                'You can not select same team');
                                          }},
                                        hint: Text('Select Team'),
                                      );
                                    } else
                                      return Container();
                                  } else
                                    return Container();},
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //   children: [
            //     Column(
            //       crossAxisAlignment: CrossAxisAlignment.start,
            //       children: [
            //         Padding(
            //           padding: const EdgeInsets.only(top: 10.0, left: 12),
            //           child: Text(
            //             " Team Name:",
            //             style: GoogleFonts.roboto(
            //                 fontSize: 15, fontWeight: FontWeight.w400),
            //           ),
            //         ),
            //         Padding(
            //           padding: EdgeInsets.only(top: 7.0, left: 8, right: 8,),
            //           child: Container(
            //             color: MyResources.backgroundColor,
            //             height: 35,
            //             width: Get.width*0.45,
            //             child: TextField(
            //               controller: team1NameController,
            //               decoration: InputDecoration(
            //                 border: OutlineInputBorder(
            //                   borderRadius: BorderRadius.circular(10.0),
            //                 ),
            //               ),
            //             ),
            //           ),
            //         ),
            //       ],
            //     ),
            //     Column(
            //       crossAxisAlignment: CrossAxisAlignment.start,
            //       children: [
            //         Padding(
            //           padding: const EdgeInsets.only(top: 10.0, left: 12),
            //           child: Text(
            //             " Team Name:",
            //             style: GoogleFonts.roboto(
            //                 fontSize: 15, fontWeight: FontWeight.w400),
            //           ),
            //         ),
            //         Padding(
            //           padding: EdgeInsets.only(top: 7.0, left: 8, right: 8,),
            //           child: Container(
            //             color: MyResources.backgroundColor,
            //             height: 35,
            //             width: Get.width*0.45,
            //             child: TextField(
            //               controller: team2NameController,
            //               decoration: InputDecoration(
            //                 border: OutlineInputBorder(
            //                   borderRadius: BorderRadius.circular(10.0),
            //                 ),
            //               ),
            //             ),
            //           ),
            //         ),
            //       ],
            //     ),
            //   ],
            // ),
            Padding(
              padding: const EdgeInsets.only(top: 20.0, left: 12),
              child: Text(
                " Match Type:",
                textAlign: TextAlign.left,
                style: GoogleFonts.roboto(
                    fontSize: 15, fontWeight: FontWeight.w400),
              ),
            ),
            Padding(
              //Add padding around textfield
              padding: EdgeInsets.only(top: 7.0, left: 8, right: 8),
              child: Padding(
                padding: const EdgeInsets.all(0.0),
                child: Container(
                  decoration: BoxDecoration(
                      color: MyResources.backgroundColor,
                      border: Border.all(color: Colors.grey),
                      borderRadius: BorderRadius.circular(10)),
                  //  color: MyResources.backgroundColor,
                  height: 35,
                  child: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: DropdownButton<String>(
                      isExpanded: true,
                      underline: Text(""),
                      value: _matchType,
                      focusColor: Colors.blue,
                      elevation: 12,
                      items: [
                        DropdownMenuItem<String>(
                          child: Text('Cricket'),
                          value: 'Cricket',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('Volleyball'),
                          value: 'VolleyBall',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('Basketball'),
                          value: 'BasketBall',
                        ),
                      ],
                      onChanged: (String? value) {
                        FocusScope.of(context).requestFocus(new FocusNode());
                        print(value);
                        setState(() {
                          _matchType = value;
                        });
                      },
                      hint: Text('Select match type'),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0, left: 12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    " Coach:",
                    style: GoogleFonts.roboto(
                        fontSize: 15, fontWeight: FontWeight.w400),
                  ),
                ],
              ),
            ),
            Padding(
              //Add padding around textfield
              padding: EdgeInsets.only(top: 7.0, left: 8, right: 8),
              child: Padding(
                padding: const EdgeInsets.all(0.0),
                child: Container(
                  decoration: BoxDecoration(
                    // color: ,
                      border: Border.all(color: Colors.grey),
                      borderRadius: BorderRadius.circular(10)),
                  //  color: MyResources.backgroundColor,
                  height: 35,
                  child: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: DropdownButton(
                      isExpanded: true,
                      underline: Text(""),
                      value: selectedCoachValue,
                      // focusColor: Colors.blue,
                      elevation: 12,
                      items: mController.coachdropValues.map((map)
                      {
                        return DropdownMenuItem(
                          child: Text(map['name']),
                          value: map['value'],
                        );},
                      ).toList(),
                      onChanged: (value) {
                        FocusScope.of(context).requestFocus(new FocusNode());
                        print(value);
                        setState(() {
                          selectedCoachValue = value.toString();
                        });
                      },
                      hint: Text('Select coach'),
                    ),
                  ),
                ),
              ),
            ),

            Padding(
              padding: const EdgeInsets.only(top: 10.0, left: 12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    " Venue:",
                    style: GoogleFonts.roboto(
                        fontSize: 15, fontWeight: FontWeight.w400),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 7.0, left: 8, right: 8,),
              child: Container(
                color: MyResources.backgroundColor,
                height: 35,
                child: TextField(
                  controller: venueController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 7.0, left: 20),
              child: Text(
                "Time:",
                style: GoogleFonts.roboto(
                    fontSize: 15, fontWeight: FontWeight.w400),
              ),
            ),
            Padding(
              //Add padding around textfield
              padding: EdgeInsets.only(top: 7.0, left: 8, right: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: InkWell(
                      onTap: () {
                        DatePicker.showDateTimePicker(context,
                            showTitleActions: true, onChanged: (time) {
                              print('change $time');},
                            onConfirm: (time) {
                              FocusScope.of(context).requestFocus(FocusNode());
                              timeController.text = DateFormat.yMEd().format(time) + ",  " + DateFormat.Hm().format(time);
                              _selectedDate = time;
                              print(timeController.text);
                              print('confirm $time');},
                            currentTime: DateTime.now(),);
                      },
                      child: Container(
                        decoration: MyResources.roundedBoxDecore,
                        child: TextFormField(
                          enabled: false,
                          readOnly: true,
                          controller: timeController,
                          decoration: InputDecoration(
                            prefixIcon: Icon(Icons.alarm_add_rounded),
                            //Add th Hint text here.
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0, left: 12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    " Note:",
                    style: GoogleFonts.roboto(
                        fontSize: 15, fontWeight: FontWeight.w400),
                  ),
                ],
              ),
            ),

            Padding(
              padding: EdgeInsets.only(top: 7.0, left: 12, right: 12,),
              child: Container(
                color: MyResources.backgroundColor,
                height: 100,
                child: TextField(
                  maxLines: 3,
                  controller: noteController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10.0, left: 20, right: 10, bottom: 10),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.9,
                height: 35,
                child: RaisedButton(
                  textColor: MyResources.buttontextColor,
                  color: MyResources.buttonColor,
                  child: Text(
                    "SAVE",
                  ),
                  onPressed: () async {
                    print(timeController);
                    await createNewMatch();
                  },
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  bool validate() {
    if (matchNameController.text.isEmpty) {
      Get.rawSnackbar(title: 'Alert', message: 'Match Name field is empty!');
      return false;
    } else if (!team1 && selectedg1Value == null) {
      Get.rawSnackbar(title: 'Alert', message: 'Choose team one');
      return false;
    } else if (!team2 && selectedg2Value == null) {
      Get.rawSnackbar(title: 'Alert', message: 'Choose team two');
      return false;
    }
    // else if (!team1 && selectedg1Value == selectedg2Value) {
    //   Get.rawSnackbar(
    //       title: 'Alert', message: 'Selected groups can not be same');
    //   return false;
    // }
    // else if (!team2 && selectedg1Value == selectedg2Value) {
    //   Get.rawSnackbar(
    //       title: 'Alert', message: 'Selected groups can not be same');
    //   return false;
    // }
    else if (team1NameController.text.isEmpty && selectedg1Value == null) {
      Get.rawSnackbar(title: 'Alert', message: 'Team 1 name field is empty!');
      return false;
    } else if (team2NameController.text.isEmpty && selectedg2Value == null) {
      Get.rawSnackbar(title: 'Alert', message: 'Team 2 name field is empty!');
      return false;
    } else if (venueController.text.isEmpty) {
      Get.rawSnackbar(title: 'Alert', message: 'Venue field is empty!');
      return false;
    } else if (noteController.text.isEmpty) {
      Get.rawSnackbar(title: 'Alert', message: 'Note field is empty!');
      return false;
    }  else if (timeController.text.isEmpty) {
      Get.rawSnackbar(title: 'Alert', message: 'Select time of match');
      return false;
    } else if (selectedCoachValue == null) {
      Get.rawSnackbar(title: 'Alert', message: 'Coach field is empty!');
      return false;
    } else
      return true;
  }
}
