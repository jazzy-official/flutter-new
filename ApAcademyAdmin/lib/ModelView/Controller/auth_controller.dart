import 'package:ap_academy_admin/ModelView/Controller/CoachController.dart';
import 'package:ap_academy_admin/ModelView/Controller/NotificationController.dart';
import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:ap_academy_admin/ModelView/Controller/profileController.dart';
import 'package:ap_academy_admin/Models/UserModel.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:ap_academy_admin/screens/LoginSignup/login_screen.dart';
import 'package:ap_academy_admin/screens/LoginSignup/signup_screen.dart';
import 'package:ap_academy_admin/screens/Setting/profile_edit.dart';
import 'package:ap_academy_admin/screens/drawer_sreen.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'db_cred.dart';
//import 'package:mongo_dart/mongo_dart.dart';

class AuthController extends GetxController {
  final database = Get.put(DatabaseController());

  // Intilize the flutter app
  FirebaseApp? firebaseApp;
  User? firebaseUser;
  FirebaseAuth? firebaseAuth;

  Future<void> initlizeFirebaseApp() async {
    firebaseApp = await Firebase.initializeApp();
    // await database.openDb();
  }

  Future<bool> checkisActive() async {
    bool val = await database
        .getisActive(FirebaseAuth.instance.currentUser!.uid);
    return val;
  }

  Future<Widget> checkUserLoggedIn() async {
    if (firebaseApp == null) {
      await initlizeFirebaseApp();
    }
    if (firebaseAuth == null) {
      firebaseAuth = FirebaseAuth.instance;
      update();
    }
    //  await Future.delayed(Duration(seconds: 5));
    if (firebaseAuth!.currentUser == null) {
      // return await database.getMongoPort(false).then((value) async {
      await fcmListner();
      // if (value)
      return LoginPage();
    } else {
      firebaseUser = firebaseAuth!.currentUser;
      update();
      // print(db.state);
      // return await database.getMongoPort(true).then((value) async {
      late bool isActive;
      await fcmListner();
      // if (value)
      await checkisActive().then((value) async {

        isActive = value;
      });
      return isActive? HomeDrawer(): LoginPage();
    }
  }




  Future<bool> checkUserOrAdmin() async {
    return await database
        .checkAdminorUser(FirebaseAuth.instance.currentUser!.uid);
  }

  ////////////////////////////////////////////////////////////Auth with email////////////////////////
  Future<void> signInwithEmail(String emails, String passs) async {
    final pController = Get.put(ProfileController());
    Get.dialog(Center(child: LoadingWidget()), barrierDismissible: false);
    try {
      print(emails);
      firebaseAuth = FirebaseAuth.instance;

      final userCredentialData = await firebaseAuth!.signInWithEmailAndPassword(
          email: emails, password: passs);
      if (userCredentialData.user!.uid != null) {
        if (!db.isConnected) await database.getMongoPort(true);
        Get.back();
        await checkisActive().then((value) async {
          if(value){
            await checkUserOrAdmin().then((value) async {
              if (value) {
                Get.offAll(() => HomeDrawer());
                firebaseUser = userCredentialData.user;
                U_id = firebaseUser!.uid;
                update();
              } else {
                Get.back();
                Get.defaultDialog(
                    barrierDismissible: false,
                    buttonColor: MyResources.buttonColor,
                    confirmTextColor: MyResources.appBarActionsColor,
                    content: Container(
                      child: Text(
                          'You can not sign in to this application. please contact admin to reset your email'),
                    ),
                    onConfirm: () async {
                      await signOut();
                    });
              }
            });
          }
          else {
            Get.back();
            Get.defaultDialog(
                barrierDismissible: false,
                buttonColor: MyResources.buttonColor,
                confirmTextColor: MyResources.appBarActionsColor,
                content: Container(
                  child: Text(
                      'You can not sign in to this application. Please contact your admin'),
                ),
                onConfirm: () async {
                  await signOut();
                });
          }
        });
      }else{
        Get.snackbar("Error", "The user does not exist",icon: Icon(
          Icons.error,
          color: Colors.red,
        ));
      }
      await pController.refreshDate();
    } catch (ex) {
      print("me"+ex.toString());

      Get.back();
      Get.snackbar('Error', "Email or Password is incorrect",
          duration: Duration(seconds: 5),
          backgroundColor: Colors.black54,
          colorText: Colors.white,
          snackPosition: SnackPosition.BOTTOM,
          icon: Icon(
            Icons.error,
            color: Colors.red,
          ));
    }
  }

  // function to createuser, login and sign out user

  Future<bool?> createnewCoach(
      String name, String email, String password, String role,String designation) async {
    firebaseAuth = FirebaseAuth.instance;
    UserModel _user;
    FirebaseApp app;
    Get.dialog(Center(child: LoadingWidget()), barrierDismissible: false);
    try {
      app = await Firebase.initializeApp();
      await FirebaseAuth.instanceFor(app: app)
          .createUserWithEmailAndPassword(email: email, password: password)
          // await firebaseAuth
          //     .createUserWithEmailAndPassword(email: email, password: password)
          .then((value) async {
        await value.user!.updateProfile(displayName: name);
        if (!db.isConnected) await database.getMongoPort(true);
        _user = UserModel(
          id: value.user!.uid,
          displayName: name,
          email: email,
          phone: "",
          photo: "",
          role: role,
          approved: true,
          isActive: true,
          designation: designation,
        );
        if (await database.addUser(_user)) {
          // await app.delete();
          // return Future.sync(() => userCredential);
          Get.find<CoachController>().refreshcoach();
          Get.back();
          Get.back();

          update();
        }
        return true;
      }).catchError((onError) async {
        print(onError.toString());
        await app.delete();
        Get.back();
        Get.snackbar("Error ", onError.message.toString());
        return false;
      });
    } catch (err) {
      // await app.delete();
      Get.back();
      print(err.toString());
      Get.snackbar("Error ", err.toString());
      return false;
    }
  }

//////////////////////////////////////////////////////////create new admin coach
  Future<void> createUser(String name, String email, String password) async {
    firebaseAuth = FirebaseAuth.instance;
    UserModel _user;

    Get.dialog(Center(child: LoadingWidget()), barrierDismissible: false);
    try {
      await firebaseAuth!
          .createUserWithEmailAndPassword(email: email, password: password)
          .then((value) async {
        await value.user!.updateProfile(displayName: name);
        // if (!db.isConnected) await database.getMongoPort(true);
        _user = UserModel(
          id: value.user!.uid,
          displayName: name,
          email: email,
          phone: "",
          photo: "",
          role: "Admin", isActive: true,
        );
        if (await database.addUser(_user))
          Get.offAll(ProfileEdit(
            usrDetails: {
              'name': name,
              'email': email,
            },
            isDrawer: true,
          ));
        U_id = value.user!.uid;
      }).catchError(
        (onError) => Get.snackbar("Error ", onError.toString()),
      );
      update();
    } catch (err) {
      Get.back();
      Get.snackbar("Error ", err.toString());
    }
  }

/////////////////////////////////////////update after edit profile
///////////////////////////////////////////////////////// delete Account////////////////////////////
  void deleteuseraccount(String email, String pass) async {
    User? user = firebaseAuth!.currentUser;

    AuthCredential credential =
        EmailAuthProvider.credential(email: email, password: pass);

    await user!.reauthenticateWithCredential(credential).then((value) {
      value.user!.delete().then((res) {
        Get.offAll(LoginPage());
        Get.snackbar("User Account Deleted ", "Success");
      });
    }).catchError((onError) =>
        Get.snackbar("Credential Error", onError.message.toString()));
  }

  Future<void> sendpasswordresetemail1(String email) async {
    firebaseAuth = FirebaseAuth.instance;
    try {
      await firebaseAuth!.sendPasswordResetEmail(email: email).then((value) {
        Get.offAll(LoginPage());
        Get.snackbar("Password Reset email link is been sent", "Success");
      }).catchError((onError) =>
          Get.snackbar("Error In Email Reset", onError.message.toString()));
    } catch (e) {
      print("Error is: " + e.toString());
    }
  }

  Future<void> signOut() async {
    Get.dialog(Center(child: LoadingWidget()), barrierDismissible: false);

    await firebaseAuth!.signOut();
    update();
    Get.back();

    // Navigate to Login again
    Get.offAll(() => LoginPage());
  }

  @override
  void onClose() {
    super.onClose();
  }
}
