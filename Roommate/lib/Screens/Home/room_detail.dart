import 'dart:async';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:ionicons/ionicons.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Models/room_model.dart';
import 'package:room_buddy/Screens/Home/Widgets/featured_room_list.dart';
import 'package:room_buddy/Screens/Home/Widgets/feature_list_room_card.dart';

import 'Widgets/city_card.dart';

class RoomDetailScreen extends StatefulWidget {
  final Room room;
  const RoomDetailScreen({Key? key, required this.room}) : super(key: key);


  @override
  State<RoomDetailScreen> createState() => _RoomDetailScreenState();
}

class _RoomDetailScreenState extends State<RoomDetailScreen> {


  Completer<GoogleMapController> _controller = Completer();
  int currentPos = 0;
  String descText = "Description Line 1\nDescription Line 2\nDescription Line 3\nDescription Line 4\nDescription Line 5\nDescription Line 6\nDescription Line 7\nDescription Line 8";
  bool descTextShowFlag = false;
  @override
  Widget build(BuildContext context) {
    final List<String> imgList = widget.room.images;
    CameraPosition _kGooglePlex = CameraPosition(
      target: widget.room.location,
      zoom: 14.4746,
    );
    return Scaffold(
      body: CustomScrollView(
        slivers:
           <Widget>[
              SliverAppBar(
                pinned: true,
                backgroundColor: myResources.themeColor,
                title: Text("1 ${widget.room.furnishingType} room in a ${widget.room.bedrooms}BHK apartment"),
                expandedHeight: 272,
               actions: [
                 IconButton(icon: Icon(Ionicons.share_social,color: Colors.white.withOpacity(0.9),), onPressed: () {  },),
                 IconButton(icon: Icon(Ionicons.ellipsis_vertical,color: Colors.white.withOpacity(0.9),), onPressed: () {  },),

               ],
               stretch: true,
               flexibleSpace: FlexibleSpaceBar(
                 stretchModes: <StretchMode>[
                   StretchMode.zoomBackground,
                   StretchMode.fadeTitle,
                 ],
                 background: Stack(
                   children: [
                     CarouselSlider(
                       options: CarouselOptions(
                           height: 1000,
                           autoPlay: true,
                           pauseAutoPlayOnManualNavigate: true,
                           viewportFraction: 1,
                           onPageChanged: (index, reason) {
                             setState(() {
                               currentPos = index;
                             });
                           }
                       ),
                       items: imgList
                           .map((item) => Image.asset(item, fit: BoxFit.cover,))
                           .toList(),
                     ),
                     Column(
                       mainAxisAlignment: MainAxisAlignment.end,
                       children: [
                         Row(
                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
                           children: [
                             Container(
                               margin: EdgeInsets.all(10),
                               height: 25,
                               width: 25,
                               decoration: BoxDecoration(
                                   borderRadius: BorderRadius.circular(15),
                                   color: Colors.white),
                               child: Center(child: Icon(Ionicons.heart_outline,size: 18,color: Colors.black,),),
                             ),
                             Row(
                               mainAxisAlignment: MainAxisAlignment.center,
                               children: imgList.map((url) {
                                 int index = imgList.indexOf(url);
                                 return Container(
                                   width: currentPos == index
                                       ? 8.0:5,
                                   height: currentPos == index
                                       ? 8.0:5,
                                   margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 5.0),
                                   decoration: BoxDecoration(
                                     shape: BoxShape.circle,
                                     color: currentPos == index
                                         ? Colors.white
                                         : Color.fromRGBO(255, 255, 255, 0.4),
                                   ),
                                 );
                               }).toList(),
                             ),
                             SizedBox(width: 45,)
                           ],
                         ),
                       ],
                     ),
                   ],
                 ),
               ) ,
             ),
             SliverList(
               delegate:  SliverChildListDelegate([
                 Padding(
                 padding: EdgeInsets.all(15.0),
                 child: Column(
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                     Text("1 ${widget.room.furnishingType} room in a ${widget.room.bedrooms}BHK apartment",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
                     const SizedBox(height: 10,),
                     Text("${widget.room.city}, ${widget.room.locality} ${widget.room.address}",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                     const SizedBox(height: 10,),
                     Text("Rs. ${widget.room.rent} / month",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
                     const SizedBox(height: 10,),
                     const Text("Posted 2 days ago",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                     const SizedBox(height: 20,),
                     const Divider(),
                     Row(
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       children:  [
                         Row(
                           children: [
                              Padding(
                               padding: EdgeInsets.all(8.0),
                               child: CircleAvatar(backgroundImage: AssetImage(widget.room.ownerDp),radius: 25,),
                             ),
                             Column(
                               crossAxisAlignment: CrossAxisAlignment.start,
                               children: [
                                 Text(widget.room.ownerName,style: TextStyle(fontWeight: FontWeight.w600,fontSize: 15),),
                                 Text("Roommate",style: TextStyle(fontSize: 13,color: Colors.black45),),
                               ],
                             )
                           ],
                         ),
                         Row(
                           children: [
                             Icon(Icons.verified_user_sharp,color: myResources.semiColor,size: 20,),
                             Text(" Verified",style: TextStyle(color: myResources.semiColor,fontSize: 13,fontWeight: FontWeight.w600),)
                           ],
                         )
                       ],
                     ),
                     const SizedBox(height: 20,),
                     Column(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: <Widget>[
                         Text(widget.room.description,
                             maxLines: descTextShowFlag ? 8 : 5,textAlign: TextAlign.start,
                         style: TextStyle(color: Colors.grey),),
                         InkWell(
                           onTap: (){ setState(() {
                             descTextShowFlag = !descTextShowFlag;
                           }); },
                           child: Row(
                             mainAxisAlignment: MainAxisAlignment.start,
                             children: <Widget>[
                               descTextShowFlag ? Padding(
                                 padding: const EdgeInsets.symmetric(vertical: 5.0),
                                 child: Text("Show less",style: TextStyle(color: myResources.themeColor,fontWeight: FontWeight.bold),),
                               ) :  Padding(
                                 padding: const EdgeInsets.symmetric(vertical: 5.0),
                                 child: Text("Continue Reading",style: TextStyle(color: myResources.themeColor,fontWeight: FontWeight.bold),),
                               )
                             ],
                           ),
                         ),
                       ],
                     ),
                     const Divider(),
                     const SizedBox(height: 10,),
                     const Text("Room Overview",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
                     GridView.count(
                       controller: ScrollController(),
                       shrinkWrap: true,
                       childAspectRatio: 7/2,
                       crossAxisSpacing: 10,
                       mainAxisSpacing: 25,
                       crossAxisCount: 2,
                     children: [
                       Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children: [
                           const Text("Rent",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                           const SizedBox(height: 5,),
                           Text("Rs. ${widget.room.rent} / month",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                         ],
                       ),
                       Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children: [
                           const Text("Security Deposit",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                           const SizedBox(height: 5,),
                           Text("Rs. ${widget.room.securityDeposit} (Refundable)",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                         ],
                       ),
                       Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children:  [
                           Text("Bills",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                           SizedBox(height: 5,),
                           Text(widget.room.billIncluded? "Included" : "Not included",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                         ],
                       ),
                       Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children: [
                           const Text("Furnishing",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                           const SizedBox(height: 5,),
                           const Text("Fully Furnished",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                         ],
                       ),
                       Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children: [
                           const Text("Bedroom",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                           const SizedBox(height: 5,),
                            Text("${widget.room.bedrooms} ${widget.room.bedroomType} Bedroom",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                         ],
                       ),
                       Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children: [
                           const Text("Bathroom",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                           const SizedBox(height: 5,),
                            Text("${widget.room.bathroom} ${widget.room.bathroomType} bathroom",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                         ],
                       ),
                       Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children:  [
                           Text("Room Size",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                           SizedBox(height: 5,),
                           Text("${widget.room.roomsize} sq. ft",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                         ],
                       ),
                       Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children: [
                           const Text("Property size",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                           const SizedBox(height: 5,),
                            Text("${widget.room.propertySize} sq. ft",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                         ],
                       ),
                       Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children:  [
                           Text("Availability",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                           SizedBox(height: 5,),
                           Text("${widget.room.availability}",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                         ],
                       ),
                       Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children:  [
                           const Text("Minimum Stay",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                           const SizedBox(height: 5,),
                           Text(widget.room.minStay,style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                         ],
                       ),
                     ],
                     ),
                     const Padding(
                       padding: EdgeInsets.symmetric(vertical: 20.0),
                       child: Divider(),
                     ),
                     const Text("Amenities",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
                     GridView.builder(
                       controller: ScrollController(),
                       shrinkWrap: true,
                       gridDelegate:  const SliverGridDelegateWithMaxCrossAxisExtent(
                         childAspectRatio: 7/2,
                         crossAxisSpacing: 10,
                         mainAxisSpacing: 25,
                         maxCrossAxisExtent: 200,),
                       itemCount: widget.room.amenities.length,
                       itemBuilder: (BuildContext context, int index) {
                         return Row(
                           children: [
                             Container(
                               height: 45,
                               width: 45,
                               decoration: BoxDecoration(
                                   border: Border.all(color: myResources.themeColor,width: 2),
                                   borderRadius: BorderRadius.circular(40)
                               ),
                               child: Icon(Ionicons.shield_checkmark_outline,color:myResources.themeColor,),
                             ),
                             SizedBox(width: 10,),
                             Expanded(child: Text(widget.room.amenities[index],style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),)),
                           ],
                         );
                       },
                     ),
                     const Padding(
                       padding: EdgeInsets.symmetric(vertical: 20.0),
                       child: Divider(),
                     ),
                      Text("About "+ widget.room.ownerName.split(" ").first,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
                     GridView.count(
                       controller: ScrollController(),
                       shrinkWrap: true,
                       childAspectRatio: 8/4,
                       crossAxisSpacing: 35,
                       mainAxisSpacing: 0,
                       crossAxisCount: 2,
                       children: [
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: [
                             const Text("Gender",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                             const SizedBox(height: 5,),
                             Text(widget.room.aboutOwner.gender,style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                           ],
                         ),
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: [
                             const Text("Age",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                             const SizedBox(height: 5,),
                             Text(widget.room.aboutOwner.age.start.toString(),style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                           ],
                         ),
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children:  [
                             Text("Martial Status",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                             SizedBox(height: 5,),
                             Text(widget.room.aboutOwner.martialStatus,style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                           ],
                         ),
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: [
                             const Text("Food choice",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                             const SizedBox(height: 5,),
                             Text(widget.room.aboutOwner.habits.foodChoice,style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                           ],
                         ),
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children:  [
                             Text("Smoking",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                             SizedBox(height: 5,),
                             Text(widget.room.aboutOwner.habits.smoking,style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                           ],
                         ),
                         // Column(
                         //   crossAxisAlignment: CrossAxisAlignment.start,
                         //   children: [
                         //     const Text("Alcohol",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                         //     const SizedBox(height: 5,),
                         //     const Text(widget.room.aboutOwner.habits.,style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                         //   ],
                         // ),
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children:  [
                             Text("Partying",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                             SizedBox(height: 5,),
                             Text(widget.room.aboutOwner.habits.party,style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                           ],
                         ),
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children:  [
                             Text("Guests",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                             SizedBox(height: 5,),
                             Text(widget.room.aboutOwner.habits.guests,style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                           ],
                         ),
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children:  [
                             Text("Pets",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                             SizedBox(height: 5,),
                             Text(widget.room.aboutOwner.habits.pets+" pets",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                           ],
                         ),
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children:  [
                             Text("Occupation",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                             SizedBox(height: 5,),
                             Text(widget.room.aboutOwner.occupation,style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                           ],
                         ),
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children:  [
                             Text("Interests",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                             SizedBox(height: 5,),
                             Expanded(child: Text(widget.room.aboutOwner.interests.toString().substring(1,widget.room.aboutOwner.interests.toString().length-1),style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),)),
                           ],
                         ),
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children:  [
                             Text("Personality Traits",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                             SizedBox(height: 5,),
                             Text(widget.room.aboutOwner.traits.toString().substring(1,widget.room.aboutOwner.traits.toString().length-1),style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                           ],
                         ),
                       ],
                     ),
                     const Padding(
                       padding: EdgeInsets.symmetric(vertical: 20.0),
                       child: Divider(),
                     ),
                     const Text("Compatible with",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
                     GridView.count(
                       controller: ScrollController(),
                       shrinkWrap: true,
                       childAspectRatio: 8/4,
                       crossAxisSpacing: 35,
                       mainAxisSpacing: 0,
                       crossAxisCount: 2,
                       children: [
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: [
                             const Text("Gender",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                             const SizedBox(height: 5,),
                             Text(widget.room.roommatePreferences.gender,style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                           ],
                         ),
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: [
                             const Text("Age",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                             const SizedBox(height: 5,),
                             Text("${widget.room.roommatePreferences.age.start} - ${widget.room.roommatePreferences.age.end}",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                           ],
                         ),
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children:  [
                             Text("Martial Status",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                             SizedBox(height: 5,),
                             Text(widget.room.roommatePreferences.martialStatus,style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                           ],
                         ),
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: [
                             const Text("Food choice",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                             const SizedBox(height: 5,),
                             Text(widget.room.roommatePreferences.habits.foodChoice,style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                           ],
                         ),
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children:  [
                             Text("Smoking",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                             SizedBox(height: 5,),
                             Text(widget.room.roommatePreferences.habits.smoking,style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                           ],
                         ),
                         // Column(
                         //   crossAxisAlignment: CrossAxisAlignment.start,
                         //   children: [
                         //     const Text("Alcohol",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                         //     const SizedBox(height: 5,),
                         //     const Text(widget.room.aboutOwner.habits.,style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                         //   ],
                         // ),
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children:  [
                             Text("Partying",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                             SizedBox(height: 5,),
                             Text(widget.room.roommatePreferences.habits.party,style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                           ],
                         ),
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children:  [
                             Text("Guests",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                             SizedBox(height: 5,),
                             Text(widget.room.roommatePreferences.habits.guests,style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                           ],
                         ),
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children:  [
                             Text("Pets",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                             SizedBox(height: 5,),
                             Text(widget.room.roommatePreferences.habits.pets+" pets",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                           ],
                         ),
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children:  [
                             Text("Occupation",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                             SizedBox(height: 5,),
                             Text(widget.room.roommatePreferences.occupation,style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                           ],
                         ),
                       ],
                     ),
                     const Padding(
                       padding: EdgeInsets.only(bottom: 20.0),
                       child: Divider(color: Colors.black54,),
                     ),
                     const Text("Location",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
                     const SizedBox(height: 10,),
                      Text("${widget.room.city}, ${widget.room.locality} ${widget.room.address}",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                     Container(
                       margin: EdgeInsets.symmetric(vertical: 20),
                       height: 250,
                       child: GoogleMap(
                         mapType: MapType.normal,
                         initialCameraPosition: _kGooglePlex,
                         onMapCreated: (GoogleMapController controller) {
                           _controller.complete(controller);
                         },
                       ),
                     ),
                     const Padding(
                       padding: EdgeInsets.only(bottom: 20.0),
                       child: Divider(color: Colors.black54,),
                     ),
                     const Text("Similar Rooms",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
                     SizedBox(height: 10,),
                     FeaturedRoomList(),
                     SizedBox(height: 50,),
                     Padding(
                       padding: const EdgeInsets.symmetric(vertical: 15.0),
                       child: ElevatedButton(
                         style: ElevatedButton.styleFrom(
                           primary: myResources.themeColor,
                           shape: const RoundedRectangleBorder(
                               borderRadius: BorderRadius.all(Radius.circular(14))),
                         ),
                         onPressed: () {
                           // Get.to(()=> const HabitsForm());
                         },
                         child: Container(
                           padding: const EdgeInsets.symmetric(
                               vertical: 8, horizontal: 8),
                           child: Row(
                             mainAxisAlignment: MainAxisAlignment.center,
                             children: <Widget>[
                               Container(
                               padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                               decoration: BoxDecoration(
                                 borderRadius:
                                 const BorderRadius.all(Radius.circular(20)),
                                 color: myResources.themeColor,
                               ),
                               child: Icon(
                                 Ionicons.chatbox_ellipses,
                                 color: Colors.white,
                                 size: 24,
                               ),
                             ),
                               Text(
                                 'Send a chat Request',
                                 style: TextStyle(color: Colors.white),
                               ),

                             ],
                           ),
                         ),
                       ),
                     ),
                   ],
                 ),
               ),
               ]),
             ),

          ],
        
      ),
    );
  }
}
