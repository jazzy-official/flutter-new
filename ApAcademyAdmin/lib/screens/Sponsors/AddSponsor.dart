import 'dart:io';

import 'package:image_picker/image_picker.dart';
import 'package:ap_academy_admin/ModelView/Controller/SponsorController.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddSponsor extends StatefulWidget {
  @override
  _AddSponsorState createState() => _AddSponsorState();
}

class _AddSponsorState extends State<AddSponsor> {
  TextEditingController sponsorNameController = TextEditingController();
  TextEditingController sponsorDescriptionController = TextEditingController();
  final sponsorController = SponsorController();
  bool isImage = false;
  XFile? image;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyResources.buttonColor,
        brightness: Brightness.light,
        title: Text('ADD SPONSOR'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          children: [
            image != null
                ? Column(

              children: [
                Container(
                  margin: const EdgeInsets.only(top: 10.0,bottom: 10.0),
                  width: 100,
                  height: 100,
                  child: Image.file(
                    File(image!.path),
                    width: 100,
                    height: 100,
                  ),
                ),
                MaterialButton(
                  onPressed: () {
                    image = null;
                    setState(() {});
                  },
                  child: Icon(
                    Icons.delete_outline,
                    color: Colors.red,
                  ),
                )
              ],
            )
                : MaterialButton(
              onPressed: () async {
                image = null;
                image = await sponsorController.imgFromGallery();
                setState(() {
                  setState(() {});
                });
              },
              child: Icon(
                Icons.add_a_photo,
                color: MyResources.buttonColor,
              ),
            ),
            Padding(
              //Add padding around textfield
              padding: EdgeInsets.only(top: 7.0, left: 20, right: 20),
              child: Container(
                height: 50,
                child: TextField(
                  controller: sponsorNameController,
                  decoration: InputDecoration(
                    hintText: 'Sponsor Name',
                    fillColor: Colors.white,
                    //Add th Hint text here.
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              //Add padding around textField
              padding: EdgeInsets.only(top: 7.0, left: 20, right: 20,bottom: 20),
              child: Container(
                height: 100,
                child: TextField(
                  keyboardType: TextInputType.multiline,
                  maxLines: 4,
                  controller: sponsorDescriptionController,
                  decoration: InputDecoration(
                    hintText: 'A Short Description of Sponsor',
                    fillColor: Colors.white,
                    //Add th Hint text here.
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),

            RaisedButton(
              child: Text('SAVE'),
              textColor: Colors.white,
              color: MyResources.buttonColor,
              onPressed: () async {
                Get.dialog(LoadingWidget());
                if (sponsorNameController.text != '') {
                  if (image != null) {
                    String? imgLink =
                        await sponsorController.uploadImageToFirebase(File(image!.path));
                    Map<String, dynamic> data = {
                      'name': sponsorNameController.text,
                      'description':sponsorDescriptionController.text,
                      'imageUrl': imgLink
                    };
                    await sponsorController.addSponsor(data);
                    Get.back();
                    Get.defaultDialog(
                      title: 'Saved',
                      content: Text('You Sponsor is saved'),
                      textConfirm: 'ok',
                      confirmTextColor: Colors.white,
                      buttonColor: MyResources.buttonColor,
                      onConfirm: () {
                        Get.back();
                        Get.back();
                      },
                    );
                  } else {
                    Get.back();
                    Get.rawSnackbar(
                        title: 'Error', message: 'Please select logo');
                  }
                } else {
                  Get.back();
                  Get.rawSnackbar(
                      title: 'Error', message: 'Write name of sponsor');
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
