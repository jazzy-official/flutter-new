import 'dart:convert';

import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:get/get.dart';
import 'package:mongo_dart/mongo_dart.dart';
import 'package:http/http.dart' as http;
import 'package:mongo_dart/mongo_dart.dart' as mongo;
class TeamController extends GetxController {
  Rx<List<Map<String, dynamic>>> teamsList = Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get teams => teamsList.value;

  List<Map<String, dynamic>> userList = <Map<String, dynamic>>[].obs;

  @override
  void onInit() {
    teamsList.bindStream(DatabaseController().teamStream());
    super.onInit();
  }

  final dbH = Get.put(DatabaseController());
  Future<void> getUsers() async {
    userList = (await dbH.getAlluserData())!;
  }


  Future getsheetfun() async {
    await getUsers();
    http.Response response = await http.get(
      Uri.parse('https://sheetdb.io/api/v1/5xeq0u2whmy1p'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
    if (response.statusCode == 200) {
      List<Map<String, dynamic>> _result1SelectMember = [];
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      List<dynamic> sheetData = await jsonDecode(response.body);

      for (var data in sheetData) {
        // userList.where((element) => element["email"]==data["EMAIL ADDRESS"]).toList();
        userList.forEach((element) {
          if (element["name"] == data["NAME"]) {
            _result1SelectMember.add(element);
          }
        });
        print(data);
      }
      Map<String, dynamic> nTeam1 = {
        '_id': mongo.ObjectId(),
        'teamName': "IC ROYALS U13",
        'teamMembers': _result1SelectMember,
        'CreatedTime': DateTime.now(),
      };
      await submitTeams(nTeam1);
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to get payable data');
    }
  }

  Future<void> delTeams(ObjectId id) async {
    await DatabaseController().deletTeam(id).then((value) => refreshTeams());
  }

  refreshTeams() {
    teamsList.bindStream(DatabaseController().teamStream());
  }

  Future<bool> approveTeams(ObjectId id, List t1, List t2,String idTeam1,String idTeam2) async {
    return await dbH.approveMatch(id, t1, t2,idTeam1,idTeam2);
  }




  Future<String?> submitTeams(Map<String, dynamic> t) async {
    return await dbH.addNewTeam(t);
  }


  Future<bool> checkifTeaminMatch(ObjectId id) async {
    return await dbH.checkifMatch(id.id.hexString);
  }
}
