import 'dart:convert';
import 'package:ap_ground_admin/Screens/Weather/models/weather.dart';
import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:ap_ground_admin/Screens/Weather/models/forcast.dart';
import 'package:ap_ground_admin/Screens/Weather/models/location.dart';
import 'package:flutter/material.dart';

class WeatherController {


  Image getWeatherIcon(String _icon) {
    String path = "assets/";
    String imageExtension = ".png";
    return Image.asset(
      path + _icon + imageExtension,
      width: 70,
      height: 70,
    );
  }

  Image getWeatherIconSmall(String _icon) {
    String path = "assets/";
    String imageExtension = ".png";
    return Image.asset(
      path + _icon + imageExtension,
      width: 40,
      height: 40,
    );
  }
  Future getForecast(LocationModel location) async {
    // Forecast forecast;
    String apiKey = "8f0c15aec521bf22ee0af530644ff51d";
    String lat = location.lat;
    String lon = location.lon;
    var url =
    Uri.parse("https://api.openweathermap.org/data/2.5/onecall?lat=$lat&lon=$lon&appid=$apiKey&units=metric");

    final response = await http.get(url);
    print('forecast');
    print(response);

    // if (response.statusCode == 200) {
    Forecast forecast = Forecast.fromJson(jsonDecode(response.body));
    return forecast;
  }
  Future<Position?> _getGeoLocationPosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      await Geolocator.openLocationSettings();
      if(!serviceEnabled){}
      return await Geolocator.getLastKnownPosition();
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        displayToast("Please turn on location");
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }
  Future<void> GetAddressFromLatLong()async {
    Position? position = await _getGeoLocationPosition();
    List<Placemark> placemarks =
    position==null?
        await placemarkFromCoordinates(31.4504, 73.1350):

       await placemarkFromCoordinates(position.latitude, position.longitude);


    print(placemarks.length);
    Placemark place = placemarks[0];
    // setState(() {
    //   Address = '${place.subAdministrativeArea}, ${place.country}';
    // });


  }
  Future<LocationModel> GetLocationModel() async {
    Position? position = await _getGeoLocationPosition();
    List<Placemark> placemarks =
    position==null?
    await placemarkFromCoordinates(31.4504, 73.1350):
    await placemarkFromCoordinates(position.latitude, position.longitude);
    print(placemarks.length);
    Placemark place = placemarks[0];
    LocationModel location = LocationModel(city: place.subAdministrativeArea!, country: place.country!, lat: position==null? 31.4504.toString():position.latitude.toString(), lon: position==null? 73.1350.toString(): position.longitude.toString());
    return location;

  }

  Future<Weather> getCurrentWeather(String city) async {
    String apikey = "8f0c15aec521bf22ee0af530644ff51d";
    String units = "metric";
    final queryParameters = {'q': city, 'appid': apikey, 'units': units};
    var uri = Uri.http(
        'api.openweathermap.org', '/data/2.5/weather', queryParameters);
    http.Response response = await http.get(uri);
    print(response.body);
    // if(response.statusCode == 200){
    Weather weather = Weather.fromJson(jsonDecode(response.body));
    return weather;
    // }else{
    //   // TODO: Throw Error here
    // }
    // return weather;
  }

}