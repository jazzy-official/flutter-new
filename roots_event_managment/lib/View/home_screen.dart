import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:roots_event_managment/View/scan_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff8d398f),
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 25.0,bottom: 10),
              // child: Text("Want to scan ?",style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold,color: Colors.white),),
            ),
            // const Text("ROOTS IVY",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.white),),
            Image.asset("assets/rootstrans.png"),
            const Text("Bilingual Declamation Contest",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Colors.white,fontFamily: "SmoochSans"),),
            Expanded(
              flex: 5,
              child: InkWell(
                onTap: (){
                  Get.to(QRViewExample());
                },
                  child: Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(image: AssetImage("assets/dummy2.png"))
                    ),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Container(
                          height: 45,
                          width: 80,
                          margin: EdgeInsets.only(top: 100,left: 10),
                          decoration: BoxDecoration(
                              color: Colors.white30,
                              borderRadius: BorderRadius.circular(17)
                          ),
                          child: const Center(child: Text("Tap me !",style: TextStyle(color: Colors.white),)),
                        ),
                      )
                  )
              ),
            ),
            Expanded(
              flex: 2,
              child: Align(
                alignment: Alignment.topRight,
                child: Container(
                  margin: EdgeInsets.all(30.0),
                  height: 60,
                  width: Get.width-60,
                  decoration: BoxDecoration(
                      color: Colors.grey.shade200,
                      borderRadius: BorderRadius.circular(17)
                  ),
                  child: const Center(child: Text("Students List",style: TextStyle(color: Colors.black,fontSize: 18,fontWeight: FontWeight.bold),)),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: const Text("Powered by aiksol pvt ltd.",style: TextStyle(color: Colors.white),),
            )

          ],
        ),
      ),
    );
  }
}
