import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'Models/expense_model.dart';

class ExpenseCard extends StatelessWidget {
  final Expense expense;
   ExpenseCard({Key? key, required this.expense}) : super(key: key);
  var formatter = NumberFormat('#,##,000');
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      color: Color(0xffbf953f),
      child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {

          },
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(3),
              color: Color(0xfff1f6f8),
            ),
            margin: EdgeInsets.all(1),
            padding: const EdgeInsets.all(10),
            // height: 200,
            child: Column(
              mainAxisAlignment:
              MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width*0.9,
                  child: Flex(
                    direction: Axis.vertical,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        expense.accounttitle,
                        style: const TextStyle(
                            color:  Color(0xff004251),
                            fontWeight: FontWeight.w700,
                            fontSize: 18),
                      ),
                    ],
                  ),
                ),


                const SizedBox(
                  height: 5,
                ),

                Column(
                  crossAxisAlignment:
                  CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Balance : ' + formatter.format(expense.currentbalance),
                      style: const TextStyle(
                          color:  Color(0xff004251),
                          fontWeight: FontWeight.w500),
                    ),
                    Row(
                      mainAxisAlignment:
                      MainAxisAlignment.spaceBetween,
                      crossAxisAlignment:
                      CrossAxisAlignment.end,
                      children: [
                        Text(
                          expense.accountno,
                          style: const TextStyle(
                            color:  Color(0xff004251),
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        Container(
                          width: 130,
                          child: Flex(
                            direction: Axis.vertical,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                expense.accountdes,
                                style: const TextStyle(
                                    color:  Color(0xff004251),
                                    fontSize: 11),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          )),
    );
  }
}
