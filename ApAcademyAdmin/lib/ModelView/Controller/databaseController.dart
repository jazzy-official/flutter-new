import 'package:ap_academy_admin/Models/UserModel.dart';
import 'package:ap_academy_admin/screens/LoginSignup/signup_screen.dart';
import 'package:ap_academy_admin/screens/Sponsors/AddSponsor.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';
import 'package:mongo_dart/mongo_dart.dart';

import 'db_cred.dart';

class DatabaseController extends GetxController {
//UserModel user=UserModel();
  var mongoUserId;

  ////////////////////////////////////////////////////////------------Database Credentials---------------//////////////////////////////////////

  ////////////////////////////////////////////////////////------------Open Database---------------/////////////////////////////////////////////
  Future<void> openDb() async {
    try {
      await db.open();
      print('Connected to database');
    } catch (ex) {
      printError(info: ex.toString());  print( ex.toString());    print('NOT Connected to database');

    }
    // db.close();
  }

///////////////////////////////////////////////////////---------------create Collection---------/////////////////

///////////////////////////////////////////////////////---------------check if created DB Collection----------////////////////////////////////////

  ////////////////////////////////////////////////////////------------Add User in Database---------------//////////////////////////////
  Future<bool> addUser(UserModel userModel) async {
    try {
      DbCollection usersCollection = db.collection("Admin");
      await usersCollection.save({
        'firebaseId': userModel.id,
        'name': userModel.displayName,
        'email': userModel.email,
        'role': userModel.role,
        'photoUrl': userModel.photo,
        'phone': userModel.phone,
        'approved': userModel.approved,
        'isActive': userModel.isActive,
        'designation': userModel.designation,
      });
      print('[APSPORT] Created User .');

      update();
      return true;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }

  ////////////////////////////////////////////////////////------------Get User Information---------------//////////////////////////////
  Future<bool> checkAdminorUser(
    String firebaseId,
  ) async {
    await openDb();
    try {
      DbCollection usersCollection = db.collection("Admin");
      var _user =
          await usersCollection.findOne(where.eq('firebaseId', firebaseId));
      if ('Admin' == _user!["role"].toString()||'SuperAdmin' == _user["role"].toString()) {
        print("Welcome Admin");
        return true;
      }
    } catch (ex) {
      print('You are not an admin');
      printError(info: ex.toString());
      return false;
    }
    update();
    return true;
  }

  ////////////////////////////////////////////////////////------------Set Parent Detail.---------------//////////////////////////////
  Future<bool> setParentCred(
    String firebaseId,
    String name,
    String email,
  ) async {
    // await openDb();
    try {
      DbCollection usersCollection = db.collection("UserParent");
      await usersCollection
          .insert({'firebaseId': firebaseId, 'name': name, 'email': email});
      print('Connected to Users');
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
    update();
    return true;
  }

////////////////////////////////////////////////////////------------Add Notifications.---------------//////////////////////////////
  Future<bool> addNotifications(Map<String, dynamic> notify) async {
    try {
      //  await db.open();
      DbCollection usersCollection = db.collection("Notifications");
      await usersCollection.insert(notify);
      print('Connected to Notification');
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }

    update();
    return true;
  }

  ////////////////////////////////////////////////////////------------Add news.---------------//////////////////////////////
  Future<bool> addNews(Map<String, dynamic> news) async {
    // await openDb();

    try {
      DbCollection usersCollection = db.collection("News");
      await usersCollection.insert(news);
      print('Connected to News');
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }

    update();

    return true;
  } ////////////////////////////////////////////////////////------------Create Match.---------------//////////////////////////////

  Future<bool> createMatch(Map<String, dynamic> newmatch) async {
    // await openDb();

    try {
      print(newmatch);

      DbCollection matchCollection = db.collection("Match");
      await matchCollection.insertOne(newmatch);
      print('Populated to Match');
      update();
      return true;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }

  Future<bool> updateMatch(Map<String, dynamic> newmatch) async {
    // await openDb();

    try {
      print(newmatch);

      DbCollection matchCollection = db.collection("Match");
      await matchCollection.save(newmatch);
      print('Populated to Match');
      update();
      return true;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }
////////////approve

  Future<bool> approveMatch(ObjectId isaaproved, List team1, List team2,String id1,String id2) async {
    // await openDb();

    try {
      DbCollection matchCollection = db.collection("Match");
      await matchCollection.update(
          where.eq('_id', isaaproved),
          modify
              .set('isApproved', true)
              .set('team1', team1)
              .set('team2', team2)
              .set('team2_id', id2)
              .set('team1_id', id1));
      print('updated to aproved');
      update();
      return true;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }

  Future<bool> updategroupusers(String gData) async {
    try {
      var userlist = await db
          .collection('Users')
          .find(where.eq('groupType', gData))
          .toList();

      await db.collection("Groups").update(
          where.eq('name', gData), modify.set('number', userlist.length));
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }
////////////////////////////////////////////////////////------------Create Event.---------------//////////////////////////////

  Future<bool> createEvent(Map<String, dynamic> newEvent) async {
    try {
      DbCollection eventCollection = db.collection("Events");
      await eventCollection.insert(newEvent);
      print('Populated to Events');
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }

    update();

    return true;
  }

  Future<bool> updateEvent(Map<String, dynamic> newEvent) async {
    try {
      DbCollection eventCollection = db.collection("Events");
      await eventCollection.save(newEvent);
      print('Populated to Events');
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }

    update();

    return true;
  }

////////////////////////////////////////////////////////------------Create Event.---------------//////////////////////////////

  Future<bool> createTrain(Map<String, dynamic> newTrain) async {
    // await openDb();

    try {
      DbCollection eventCollection = db.collection("Training");
      await eventCollection.insert(newTrain);
      print('Populated to Training');
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }

    update();
    // await db.close();
    return true;
  }

  Future<bool> updateTrain(Map<String, dynamic> newTrain) async {
    // await openDb();

    try {
      DbCollection eventCollection = db.collection("Training");
      await eventCollection.save(
        newTrain,
      );
      print('Populated to Training');
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }

    update();
    // await db.close();
    return true;
  }

  Future<bool> checkUserExist(String uid) async {
    try {
      DbCollection eventCollection = db.collection("Users");
      var n = await eventCollection.findOne(where.eq(
        "firebaseId",
        uid,
      ));

      if (n != null) {
        if (n['role'] == 'user') {
          return true;
        } else
          return false;
      } else
        return false;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }

////////////////////////////////////////////////////////------------Create Event.---------------//////////////////////////////

  Future<bool> addTeam(Map<String, dynamic> teamModel) async {
    // await openDb();

    try {
      DbCollection eventCollection = db.collection("Training");
      await eventCollection.insert(teamModel);
      print('Populated to Training');
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }

    update();
    // await db.close();
    return true;
  }
////////////////////////////////////////////////////////------------Create Event.---------------//////////////////////////////

  Future<bool> updateProfile(Map<String, dynamic> userUpdateModel) async {
    // await openDb();

    try {
      DbCollection eventCollection = db.collection("Users");
      await eventCollection.update(
          where.eq("firebaseId", U_id),
          modify
              .set('phone', userUpdateModel['phone'])
              .set("profileImage", userUpdateModel['userUpdateModel']));
      print('Updated to Users');
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }

    update();
    // await db.close();
    return true;
  }

  //////////////////////////////////////////////////////////////////////////////
  Future<bool> updateUser(Map<String, dynamic> userModel) async {
    try {
      DbCollection usersCollection = db.collection("Admin");
      await usersCollection.update(
        where.eq('firebaseId', FirebaseAuth.instance.currentUser!.uid),
        modify.set('phone', userModel['phone']),
      );
      await usersCollection.update(
          where.eq('firebaseId', FirebaseAuth.instance.currentUser!.uid),
          modify.set('dob', userModel['dob']));
      await usersCollection.update(
          where.eq('firebaseId', FirebaseAuth.instance.currentUser!.uid),
          modify.set('name', userModel['name']));
      await usersCollection.update(
          where.eq('firebaseId', FirebaseAuth.instance.currentUser!.uid),
          modify.set('surname', userModel['surname']));
      print('updated to Admin');
      update();
      return true;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }

  ///
  Future<List<Map<String, dynamic>>?> getUsersbygroup(String groupName) async {
    try {
      List<Map<String, dynamic>> n;
      if (groupName != null) {
        DbCollection eventCollection = db.collection("Users");
        int tem = int.parse(groupName.substring(groupName.length - 2));
        n = await eventCollection.find(where.lte('age', tem)).toList();
        return n;
      } else
        return null;
    } catch (ex) {
      printError(info: ex.toString());
      return null;
    }
  }

///////////////////////////////////////////////////////----------------Fetch User Details----------///////////
  Future<Map<String, dynamic>?> getUserDetail() async {
    try {
      DbCollection eventCollection = db.collection("Admin");
      var n = await eventCollection.findOne(
          where.eq("firebaseId", FirebaseAuth.instance.currentUser!.uid));
      return n;
    } catch (ex) {
      printError(info: ex.toString());
      return null;
    }
  }

//////////////////
  Future<List<Map<String, dynamic>>> getTraining() async {
    return await db.collection('Training').find(where.sortBy("time",descending: true)).toList();
  }

  Future<List<Map<String, dynamic>>> getMatch() async {
    return await db.collection('Match').find(where.sortBy("timeCreated",descending: true)).toList();
  }

//////////////////////////////-----------get
  Future<Map<String, dynamic>> getTeamName(
      Map<String, dynamic> matchModel1) async {
    late Map<String, dynamic> nameModel;
    final dbHelper = Get.put(DatabaseController());
    Map<String, dynamic>? t2 =
        await dbHelper.getTeamData(ObjectId.parse(matchModel1['team2']));
    await dbHelper.getTeamData(ObjectId.parse(matchModel1['team1'])).then((t1) {
      nameModel = {'team1': t1, 'team2': t2};
    });
    return nameModel;
  }

////////////////////////////////////////////////////////------------Create Event.---------------//////////////////////////////

  Future<Map<String, dynamic>?> getTeamData(dynamic teamId) async {
    try {
      DbCollection eventCollection = db.collection("Teams");
      var n = await eventCollection.findOne(where.id(teamId));
      return n;
    } catch (ex) {
      printError(info: ex.toString());
      return null;
    }
  }

  Future<Map<String, dynamic>?> getTeamDataByName(dynamic name) async {
    try {
      DbCollection eventCollection = db.collection("Teams");
      var n = await eventCollection.findOne(where.eq("teamName", name));
      return n;
    } catch (ex) {
      printError(info: ex.toString());
      return null;
    }
  }

  ///////////////////////////////////////////------Add Teams---------////////////
  Future<String?> addNewTeam(Map<String, dynamic> newTeam) async {
    try {
      newTeam= await db.collection("Teams").insert(newTeam);
      print('added to Teams');
      return newTeam["_id"];
    } catch (e) {
      print(e);
      return null;
    }
  } ///////////////////////////////////////////------Add Teams---------////////////

  Future<bool> addSponsor(Map<String, dynamic> newTeam) async {
    try {
      await db.collection("Sponsors").insert(newTeam);
      print('added to Sponsors');
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }
///////////////////////////////////////////------Add Teams---------////////////

  Future<bool> addBank(Map<String, dynamic> newBank) async {
    try {
      await db.collection("Banks").insert(newBank);
      print('added to Banks');
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////----------------------/.////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////
  Future<void> addProfileImgPathToDatabase(String imgpath) async {
    try {
      DbCollection eventCollection = db.collection("Admin");
      await eventCollection.update(
          where.eq("firebaseId", FirebaseAuth.instance.currentUser!.uid),
          modify.set("photoUrl", imgpath));
      print('Updated to Users');
    } catch (e) {
      print(e);
    }
  }

///////////////////////////////////////////////////////////////
  Future<bool> addVideoPathToDatabase(
    String vidPath,
  ) async {
    try {
      DbCollection usersCollection = db.collection("Gallery");
      await usersCollection.update(
          where.eq("_id", FirebaseAuth.instance.currentUser!.uid),
          modify.set("albumVideos", vidPath));

      print('update Video to Album');
      update();
      return true;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }

  Future<bool> updatenewPhoto(ObjectId _id, List<String> imgurl) async {
    bool r = true;
    try {
      await db
          .collection('Gallery')
          .update(where.eq("_id", _id), modify.addToSet('albumPhotos', imgurl));
      r= false;
    } catch (ex) {}
    return r;
  }

///////////////////////////////////collect all tekens
  Future<List<String>?> getAllTokens() async {
    DbCollection userCollection = db.collection("Users");
    List<String> tokens = [];
    try {
      await userCollection.find().toList().then((value) {
        for (int i = 0; i < value.length; i++) {
          tokens.add(value[i]['token'].toString());
          tokens.add(value[i]['parentToken'].toString());
        }
      });
      return tokens;
    } catch (ee) {
      print(ee.toString());
      return null;
    }
  }

  ///////////////////////////////
  Future<List<String>?> getTokensbygroup(String groupType) async {
    DbCollection userCollection = db.collection("Users");
    DbCollection teamCollection = db.collection("Groups");
    List<String> tokens = [];
    try {
      await teamCollection
          .findOne(where.eq('teamName', groupType))
          .then((value1)
      {
        for (int i = 0; i < value1!['teamMembers'].length; i++)
        {
          tokens.add(value1['teamMembers'][i]['token']);
          tokens.add(value1['teamMembers'][i]['parentToken']);
        };
      });
      print(tokens);
      return tokens;
    } catch (ee) {
      print("error in db controller "+ee.toString());
      return null;
    }
  } ////////////////////////////////////////

  // Future<List<String>> getTokensbyTeamgroup(String groupType) async {
  //   DbCollection userCollection = db.collection("Users");
  //   DbCollection teamCollection = db.collection("Teams");
  //   List<String> tokens = [];
  //   try {
  //     await teamCollection
  //         .findOne(where.eq('teamName', groupType))
  //         .then((value1) async {
  //       await userCollection
  //           .find(where.lte('age', value1['num']))
  //           .toList()
  //           .then((value2) {
  //         for (int i = 0; i < value2.length; i++) {
  //           tokens.add(value2[i]['token']);
  //         }
  //       });
  //     });
  //     return tokens;
  //   } catch (ee) {
  //     print(ee.toString());
  //     return null;
  //   }
  // }

  Future<List<String>?> getTokensbyTeamgroup(String teamName) async {
    DbCollection userCollection = db.collection("Users");
    DbCollection teamCollection = db.collection("Teams");
    List<String> tokens = [];
    try {
      await teamCollection
          .findOne(where.eq('teamName', teamName))
          .then((value1)
      {
        for (int i = 0; i < value1!['teamMembers'].length; i++)
        {
          tokens.add(value1['teamMembers'][i]['token'],);
          tokens.add(value1['teamMembers'][i]['parentToken'],);
        };
      });
      print(tokens);
      return tokens;
    } catch (ee) {
      print("error in db controller "+ee.toString());
      return null;
    }
  }
  ////////////////////////////////////////Approval done////////////////////

////////////////////////////////////////////////////////------------Create Event.---------------//////////////////////////////

  Future<List<Map<String, dynamic>>?> getAlluserData() async {
    // await openDb();

    try {
      DbCollection eventCollection = db.collection("Users");

      List<Map<String, dynamic>> n = await eventCollection
          .find(where.eq("approved", true).sortBy("name",descending: false))
          .toList();
      return n;
    } catch (ex) {
      printError(info: ex.toString());
      return null;
    }
  }

  Future<List<Map<String, dynamic>>?> getAllCoachesData() async {
    // await openDb();

    try {
      DbCollection eventCollection = db.collection("Admin");

      List<Map<String, dynamic>> n =
          await eventCollection.find(where.eq("role", "Coach")).toList();

      return n;
    } catch (ex) {
      printError(info: ex.toString());
      return null;
    }
  }

  Future<List<Map<String, dynamic>>?> getTeamlistforCreation() async {
    // await openDb();
    try {
      DbCollection teamCollection = db.collection("Teams");
      print('got to Team 1');
      var n = teamCollection.find().toList();
      //  print(n.toString());
      return n;
    } catch (ex) {
      printError(info: ex.toString());
      return null;
    }
  }

//////////////////////////////add new album////////////////

  ////////////////////////////////////////Approval done////////////////////

  Future<String> approveUser(
    dynamic _id,
  ) async {
    DbCollection userCollection = db.collection("Users");
    try {
      await userCollection.update(
          where.eq("_id", _id),
          modify
              .set("approved", true)
              .set("isActive", true)
      );
      return "";
    } catch (ee) {
      print(ee.toString());
      return '';
    }
  }



  // Future<String> calculategroup(age) async {
  //   try {
  //     await db
  //         .collection('Groups')
  //         .find(where.gte('num', age))
  //         .toList()
  //         .then((value) {
  //       value.forEach((element) {});
  //     });
  //   } catch (ee) {
  //     print(ee.toString());
  //     return 'No Group';
  //   }
  // }

  ///////////////////////////////////////////////////////////////////Save Album///////////
  Future<bool> addAlbum(Map<String, dynamic> albumModel) async {
    try {
      DbCollection usersCollection = db.collection("Gallery");
      await usersCollection.insert(albumModel);

      print('inserted to Album');
      update();
      return true;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }
  Future<bool> editAlbum(Map<String, dynamic> albumModel,ObjectId id) async {
    try {
      DbCollection userCollection = db.collection("Gallery");
      await userCollection.replaceOne(where.eq("_id", id),albumModel);

      print('update to Album');
      update();
      return true;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }

//////////////////////////////////delet Event
  Future<bool> fetch(ObjectId uid) async {
    try {
      await db.collection("Notifications").remove(where.eq("_id", uid));
      return true;
    } catch (ee) {
      print(ee.toString());
      return false;
    }
  }

//////////////////////////////////delet Event
  Future<List<Map<String, dynamic>>?> getScheduled() async {
    try {
      return await db
          .collection("Training")
          .find(where.eq("canSchedual", true))
          .toList();
    } catch (ee) {
      print(ee.toString());
      return null;
    }
  }

/////////////////////////delete albumn
  Future<bool> deletAlbumn(ObjectId uid, List<dynamic> link) async {
    try {
      if (link.length > 0) {
        for (int i = 0; i < link.length; i++) {
          await deletFromFirebase(link[i].toString()).then((value) async {
            await db.collection("Gallery").remove(where.eq("_id", uid));
          });
        }

        return true;
      } else {
        await db.collection("Gallery").remove(where.eq("_id", uid));
        return true;
      }
    } catch (ee) {
      print(ee.toString());
      return false;
    }
  }

  Future deletFromFirebase(String link) async {
    try {
      await FirebaseStorage.instance
          .ref()
          .child(link)
          .delete()
          .then((_) => print('Successfully deleted $link storage item'));
    } catch (er) {
      print(er.toString());
    }
  }
////////////////////////////////////////---------------------Delete Controllers--------------///////////////////////
/////////////////////////////////////////////////////========set under in user==========//////////////////

  Future<bool> updateUserType(ObjectId uid, String typ) async {
    try {
      await db.collection("Users").update(
            where.eq("_id", uid),
            modify.set("Type", typ),
          );
      // await db
      //     .collection("Users")
      //     .update(where.eq("_id", uid), modify.set("isActive", true));
      return true;
    } catch (ee) {
      print(ee.toString());
      return false;
    }
  }

  ////////////////////////////
  Future<bool> updateScheduleDate(ObjectId uid, DateTime date) async {
    try {
      await db
          .collection("Training")
          .update(where.eq("_id", uid), modify.set("time", date));
      return true;
    } catch (ee) {
      print(ee.toString());
      return false;
    }
  }

  Future<bool> getMongoPort(bool isFirebase) async {
    try {
      if (!db.isConnected && isFirebase) {
        await FirebaseFirestore.instance
            .collection("Cred")
            .doc("MongoDbPort")
            .get()
            .then((value) => {
                  dbLink = value.data()!['dbLink'],
                });
      }
    } catch (e) {
      print(e.toString());
      Get.snackbar("MongoDb Error", e.toString());
    }
    bool isopened = false;
    try {
      // if (!db.isConnected && db.state.toString() != "State.OPENING") {
      await db.open().then((value) {
        isopened = true;
        return isopened;
      });
      //  }
    } catch (error) {
      isopened = false;
    }
    print("[AP Sports] Database is connected : " + db.isConnected.toString());
    return isopened;
  }

/////////////////////////////////fetch name of team
  Future<Map<String, dynamic>?> fetchName(String uid) async {
    final ma =
        db.collection('Admin').findOne(where.id(ObjectId.fromHexString(uid)));
    return await ma;
  }

//////////////////////////////////check if tema in match
  Future<bool> checkifMatch(String id) async {
    try {
      final t1 = await db.collection("Match").findOne(where.eq("team1", id));
      final t2 = await db.collection("Match").findOne(where.eq("team2", id));
      if (t1 == null && t2 == null)
        return true;
      else
        return false;
    } catch (ee) {
      print(ee.toString());
      return false;
    }
  }

/////////////////////////////
  ///
  ///
  Future<bool> makeInactive(ObjectId gid) async {
    try {
      DbCollection matchCollection = db.collection("Groups");
      await matchCollection.update(
          where.eq('_id', gid), modify.set('isActive', false));

      update();
      return true;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }

  Future<bool> makeactive(ObjectId gid) async {
    try {
      DbCollection matchCollection = db.collection("Groups");
      await matchCollection.update(
          where.eq('_id', gid), modify.set('isActive', true));

      update();
      return true;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }

  Future<bool> makeUseractive(ObjectId gid) async {
    try {
      DbCollection matchCollection = db.collection("Users");
      await matchCollection.update(
          where.eq('_id', gid), modify.set('isActive', true));

      update();
      return true;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }

  Future<bool> makeUserInactive(ObjectId gid) async {
    try {
      DbCollection matchCollection = db.collection("Users");
      await matchCollection.update(
          where.eq('_id', gid), modify.set('isActive', false));

      update();
      return true;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }

  Future<bool> makeAdminactive(ObjectId gid) async {
    try {
      DbCollection matchCollection = db.collection("Admin");
      await matchCollection.update(
          where.eq('_id', gid), modify.set('isActive', true));

      update();
      return true;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }

  Future<bool> makeAdminInactive(ObjectId gid) async {
    try {
      DbCollection matchCollection = db.collection("Admin");
      await matchCollection.update(
          where.eq('_id', gid), modify.set('isActive', false));

      update();
      return true;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }
///////////////////////add new group

  Future<String?> addnewGroup(Map<String, dynamic> newGroup) async {
    try {
      newGroup = await db.collection("Groups").insert(newGroup);
      print('added to Teams');
      return newGroup["_id"];
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<List<Map<String, dynamic>>?> getGroups(ObjectId gId) async {
    try {
      DbCollection matchCollection = db.collection("Groups");

      return await matchCollection.find(where.id(gId)).toList();
    } catch (ex) {
      printError(info: ex.toString());
      return null;
    }
  }

  Future<bool> getisActive(
      String firebaseId,
      ) async {
    // await openDb();
    try {
      DbCollection usersCollection = db.collection("Admin");
      var _user =
      await usersCollection.findOne(where.eq('firebaseId', firebaseId));
      return _user!["isActive"];

    } catch (ex) {
      print('You are not an admin');
      printError(info: ex.toString());
      return false;
    }
    update();
    return true;
  }

/////////////////////checkGroup

  Future<bool> checkGroupinUse(String gid) async {
    try {
      DbCollection matchCollection = db.collection("Users");
      var temp =
          await matchCollection.find(where.eq('groupType', gid)).toList();
      return temp.length > 0;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }

////////////////////del Groups
  Future<bool> delGroups(ObjectId gid) async {
    try {
      DbCollection matchCollection = db.collection("Groups");
      await matchCollection.remove(where.id(gid));
      return true;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }

  /////////////////////del
  Future<bool> updateTeams(List<Map<String, dynamic>> data,String id,String name) async {
    try {
      DbCollection teamCollection = db.collection("Teams");
      await teamCollection.update(
          where.eq("_id", id),
          modify
              .set('teamMembers', data)
              .set('teamName', name)
              );
      print('Updated to teams');



      // await db.collection("Teams").save(data);
      return true;
    } catch (ee) {
      print(ee.toString());
      return false;
    }
  }

//////////////////////////////////delet Event
  Future<bool> deletEvent(ObjectId uid) async {
    try {
      await db.collection("Events").remove(where.eq("_id", uid));
      return true;
    } catch (ee) {
      print(ee.toString());
      return false;
    }
  }

//////////////////////////////////delet Event
  Future<bool> deletbanks(ObjectId uid) async {
    try {
      await db.collection("Banks").remove(where.eq("_id", uid));
      return true;
    } catch (ee) {
      print(ee.toString());
      return false;
    }
  }

//////////////////////////////////delet Event
  Future<bool> deletTeam(ObjectId uid) async {
    try {
      await db.collection("Teams").remove(where.id(uid));
      return true;
    } catch (ee) {
      print(ee.toString());
      return false;
    }
  }

  Future<bool> deletGroup(ObjectId uid) async {
    try {
      await db.collection("Groups").remove(where.id(uid));
      return true;
    } catch (ee) {
      print(ee.toString());
      return false;
    }
  }
//////////////////////////////////delet Event
  Future<bool> deletcoach(ObjectId uid) async {
    try {
      await db.collection("Admin").remove(where.id(uid));
      return true;
    } catch (ee) {
      print(ee.toString());
      return false;
    }
  }

//////////////////////////////del news
  Future<bool> deletnews(ObjectId uid) async {
    try {
      await db.collection("News").remove(where.eq("_id", uid));
      return true;
    } catch (ee) {
      print(ee.toString());
      return false;
    }
  }

  //////////////////////////////////////////////delet Event
  Future<bool> deletMatch(ObjectId uid) async {
    try {
      await db.collection("Match").remove(where.eq("_id", uid));
      return true;
    } catch (ee) {
      print(ee.toString());
      return false;
    }
  }

  //////////////////////////////////////////////delet Event
  Future<bool> deletTraining(ObjectId uid) async {
    try {
      await db.collection("Training").remove(where.eq("_id", uid));
      return true;
    } catch (ee) {
      print(ee.toString());
      return false;
    }
  }

//////////////////////////////////delet Event
  Future<bool> deletSponsor(ObjectId uid) async {
    try {
      await db.collection("Sponsors").remove(where.eq("_id", uid));
      return true;
    } catch (ee) {
      print(ee.toString());
      return false;
    }
  }

//////////////////////////////////delet Event
  Future<bool> deletNotif(ObjectId uid) async {
    try {
      await db.collection("Notifications").remove(where.eq("_id", uid));
      return true;
    } catch (ee) {
      print(ee.toString());
      return false;
    }
  }

////////////////////////////////////////---------------------Stream Controllers--------------///////////////////////
  ////////////////////////////////////////Notification Stream////////////////////
  Stream<List<Map<String, dynamic>>> notificationStream() {
    return db
        .collection("Notifications")
        .find(where.sortBy('timeCreated',descending: true))
        .toList()
        .asStream();
  }

  ////////////////////////////////////////News Stream////////////////////
  Stream<List<Map<String, dynamic>>> newsStream() {
    return db.collection("News").find(where.sortBy('timeCreated',descending: true)).toList().asStream();
  }

  ////////////////////////////////////////News Stream////////////////////
  Stream<List<Map<String, dynamic>>> matchStream() {
    return db
        .collection("Match")
        .find(where.sortBy('matchTime',descending: false),)
        .toList()
        .asStream();
  } ////////////////////////////////////////News Stream////////////////////

  Stream<List<Map<String, dynamic>>> otherEventStream() {
    return db.collection("Events").find(where.sortBy('timeCreated',descending: true)).toList().asStream();
  }

/////////////////////////////////////////////////TeamStream----------////////////////
  ///
  Stream<List<Map<String, dynamic>>> teamStream() {
    return db.collection("Teams").find().toList().asStream();
  } /////////////////////////////////////////////////ApprovalStream----------///////////////////////////

  Stream<List<Map<String, dynamic>>> getAlbumData() {
    return db.collection("Gallery").find().toList().asStream();
  }

//////////////////////////////////////////////////////
  Stream<List<Map<String, dynamic>>> coachStream(String role) {
    return db
        .collection("Admin")
        .find(where.eq('role', 'Coach'))
        .toList()
        .asStream();
  }

//////////////////////////////////////////////////////
  Stream<List<Map<String, dynamic>>> adminStream(String role) {
    return db
        .collection("Admin")
        .find(where.eq('role', 'Admin'))
        .toList()
        .asStream();
  }

  ///
  Stream<List<Map<String, dynamic>>> approvalStream() {
    return db
        .collection("Users")
        .find(where.eq("approved", false).exists("dob"))
        .toList()
        .asStream();
  }

  ///
  Stream<List<Map<String, dynamic>>> groupWiseStream({String? numb}) {
    return db
        .collection("Users")
        .find(numb != null
            ? where.eq('approved', true).eq("groupType", numb)
            : where.eq('approved', true))
        .toList()
        .asStream();
  }

  Stream<List<Map<String, dynamic>>> groupnamesStream({String? numb}) {
    return db
        .collection("Groups")
        .find(where.eq('isActive', true))
        .toList()
        .asStream();
  }

  Stream<List<Map<String, dynamic>>> groupnamesallStream({String? numb}) {
    return db.collection("Groups").find().toList().asStream();
  }

  Stream<List<Map<String, dynamic>>> trainingStream() {
    return db
        .collection("Training")
        .find(where.sortBy('time',descending: true))
        .toList()
        .asStream();
  }

  Stream<List<Map<String, dynamic>>> sponsorStream() {
    return db.collection("Sponsors").find().toList().asStream();
  }

  Stream<List<Map<String, dynamic>>> banksStream() {
    return db.collection("Banks").find().toList().asStream();
  }
}
