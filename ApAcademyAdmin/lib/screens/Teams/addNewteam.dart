import 'package:ap_academy_admin/screens/Teams/groupScreen.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:ionicons/ionicons.dart';
import 'package:ap_academy_admin/ModelView/Controller/GroupController.dart';
import 'package:ap_academy_admin/ModelView/Controller/NotificationController.dart';
import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:ap_academy_admin/ModelView/Controller/matchController.dart';
import 'package:ap_academy_admin/ModelView/Controller/teamsController.dart';

import 'package:ap_academy_admin/screens/Teams/select_members.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:checkbox_grouped/checkbox_grouped.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mongo_dart/mongo_dart.dart' as mongo;


class AddNewTeam extends StatefulWidget {
  final Map<String, dynamic>? matchModel;
  final String? teamname;
  final mongo.ObjectId? id;
  final List<dynamic>? teamMembers;

   AddNewTeam({
    Key? key,
    this.matchModel, this.teamname, this.teamMembers, this.id,
  }) : super(key: key);
  @override
  _AddNewTeamState createState() => _AddNewTeamState();
}

class _AddNewTeamState extends State<AddNewTeam> {
  TextEditingController team1Controller = TextEditingController();
  final mController = Get.put(MatchController());
  final dbC = Get.put(DatabaseController());
  final teamController = Get.put(TeamController());
  final dbController = Get.put(DatabaseController());
  //TextEditingController nameController = TextEditingController();

  String _selectedTypevalue = "";
  List<Map<String, dynamic>> _result1SelectMember = [];
  //List<dynamic> _memberIdList = [];
////////////////////////---------------------------------
  // setMembers() {
  //   _memberIdList = [];

  //   int count = 0;
  //   _resultSelectMember.forEach((mIds) {
  //     _memberIdList.add(mIds['_id']);

  //     count++;
  //   });
  // }
  bool validate() {
    if (team1Controller.text == '') {
      Get.back();
      Get.rawSnackbar(
        title: 'Error',
        message: 'Team name is mandatory.',
      );
      return false;
    } else if (_result1SelectMember.length == 0) {
      Get.back();
      Get.rawSnackbar(
        title: 'Error',
        message: 'Select members for the Team.',
      );
      return false;
    } else
      return true;
  }

  populateData() {
    team1Controller.text = widget.matchModel!=null?widget.matchModel!['team1name']:"";
    if(widget.teamMembers != null){
      widget.teamMembers!.forEach((element) => {_result1SelectMember.add(element) });
    }
    setState(() {
      // _result1SelectMember = widget.teamMembers??[];
    });
  }

  @override
  void initState() {
    super.initState();
    populateData();
    if(widget.teamname!=null){
      team1Controller = TextEditingController(text: widget.teamname);
    }

  }

  @override
  void dispose() {
    team1Controller.dispose();
    super.dispose();
  }

/////////////////////////////////---------------------------
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyResources.backgroundColor,
      appBar: AppBar(
        backgroundColor: MyResources.buttonColor,
        title: Container(
          child: Text(
            'ADD/EDIT Teams',
          ),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            //add team  and select members

            //Team Name Textfield
            Padding(
              padding: const EdgeInsets.only(top: 20.0, left: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    " Team Name:",
                    style: GoogleFonts.roboto(
                        fontSize: 15, fontWeight: FontWeight.w400),
                  ),
                ],
              ),
            ),
            Padding(
              //Add padding around textfield
              padding: EdgeInsets.only(top: 7.0, left: 20, right: 20),
              child: Container(
                color: MyResources.backgroundColor,
                height: 45,
                child: TextField(
                  controller: team1Controller,
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0, left: 20, right: 20),
              child: Container(
                width: Get.width * 0.38,
                height: 35,
                child: RaisedButton(
                  textColor: Colors.white,
                  color: MyResources.buttonColor,
                  child: Text(
                    "ADD MEMBERS",
                  ),
                  onPressed: () async {
                       await teamController.getUsers().then((value) async {
                    var members = await Get.to(SelectMembers(
                        userModel: teamController.userList,
                   )
                      );
                    if (members != null) {
                        members.forEach((ev) {
                          _result1SelectMember.add(ev);
                        });

                    }
                       });

                    //  setMembers();
                    // print(_memberIdList);
                    if (mounted) {
                      setState(() {});
                    }
                  },
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
            _result1SelectMember.length == 0
                ? Container(height: Get.height -400,)
                : Padding(
                    padding: const EdgeInsets.only(right: 20.0, left: 20),
                    child: Container(
                      constraints: BoxConstraints(
                        maxHeight: Get.height*0.55,
                        //minHeight: 50,
                      ),
                      child: ListView.builder(
                        itemCount: _result1SelectMember.length,
                        itemBuilder: (BuildContext context, int index) =>
                            getUserCard(_result1SelectMember[index], index, true),
                      ),
                    ),
                  ),


            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.all(20),
                  width: MediaQuery.of(context).size.width * 0.5,
                  height: 35,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                    primary: MyResources.buttonColor,
                    //color: MyResources.buttonColor,
                    ),
                    child: Text("SUBMIT",
                        style: TextStyle(fontSize: 16)),
                    onPressed: () async {
                      Get.dialog(Container(
                        height: 200,
                        child: LoadingWidget(),
                      ));
                      if (validate()) {
                        mongo.ObjectId tid1=mongo.ObjectId();
                        mongo.ObjectId tid2=mongo.ObjectId();
                        Map<String, dynamic> nTeam1 = {
                          '_id':  tid1,
                          'teamName': team1Controller.text,

                          // 'teamType': _selectedTypevalue,
                          'teamMembers': _result1SelectMember,
                          // 'coach': mController.selectedCoachValue.value,
                          // 'discription': discriptionController.text,
                          'CreatedTime': DateTime.now()
                        };
                        if(widget.teamname==null){
                          await teamController.submitTeams(nTeam1);
                        }else{
                          final teamController = Get.put(TeamController());
                          await teamController
                              .delTeams(
                            widget.id!,
                          ).then((value) async => {
                          await teamController.submitTeams(nTeam1),
                            Get.back(),
                            Get.back(),
                          });
                        };

                        Get.back();
                        Get.back();
                        Get.snackbar(
                          'Saved', '${team1Controller.text}  is saved',
                          snackPosition: SnackPosition.BOTTOM,
                          duration: Duration(seconds: 2, milliseconds: 500),
                        );
                        teamController.refreshTeams();
                        mController.refreshDate();
                      }
                    },
                    // shape: new RoundedRectangleBorder(
                    //   borderRadius: new BorderRadius.circular(10.0),
                    // ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget getUserCard(Map<String, dynamic> userModel, int ind, bool team) {
    return
      Slidable(
        actionPane: SlidableDrawerActionPane(),
        actionExtentRatio: 0.25,
        child:
          Card(
          child: Container(
            height: 40,
            width: Get.width,
            child: Padding(
              padding: const EdgeInsets.all(2.0),
              child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 2,
                      child: Center(
                        child: Text((ind+1).toString(),
                          style: TextStyle(color: MyResources.coachHeading, fontSize: 16),),
                      ),
                    ),
                    Expanded(
                      flex: 5,
                      child: Center(
                        child: Text(userModel['name'],
                          style: TextStyle(color: MyResources.coachHeading, fontSize: 16),),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(calculateAge(userModel['dob'])
                          .toString(),
                          style: TextStyle(color: MyResources.coachHeading, fontSize: 16),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Icon(
                            Ionicons.chevron_back,
                            color: MyResources.buttonColor,
                          ),
                    ),

                  ]),
            ),
          ),
        ),
        secondaryActions: <Widget>[
          Container(
            padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: IconSlideAction(
              color: Colors.red,
              icon: Icons.delete,
              onTap: () => team ? delet1User(ind) :null,
            ),
          ),
        ],
      );

  }

  delet1User(int i) {
    _result1SelectMember.removeAt(i);
    if (mounted) setState(() {});
  }

}
