import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:room_buddy/Constants/util.dart';

class MyListingRoomDetail extends StatelessWidget {
  const MyListingRoomDetail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 70,
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("1 private room in 2BHK apt 1 private room in 2BHK apt",style: TextStyle(color: Colors.black,fontSize: 16),),
            SizedBox(height: 2,),
            Text("Bahria town phase 2",style: TextStyle(color: Colors.grey.shade500,fontSize: 12),),
          ],
        ),
        actions: const [
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Icon(Icons.share),
          ),

        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 0,vertical: 15),
              color: Colors.grey.shade200,
              height: 90,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text("Activated",style: TextStyle(fontSize: 17,fontWeight: FontWeight.bold),),
                        Text("status",style: TextStyle(color: Colors.grey.shade600,fontSize: 13,fontWeight: FontWeight.w600),),
                      ],
                    ),
                  ),
                  Container(height: 80,width: 1,color: Colors.grey,),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text("34",style: TextStyle(fontSize: 17,fontWeight: FontWeight.bold),),
                        Text("views",style: TextStyle(color: Colors.grey.shade600,fontSize: 13,fontWeight: FontWeight.w600),),
                      ],
                    ),
                  ),
                  Container(height: 80,width: 1,color: Colors.grey,),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text("02",style: TextStyle(fontSize: 17,fontWeight: FontWeight.bold),),
                        Text("contacts",style: TextStyle(color: Colors.grey.shade600,fontSize: 13,fontWeight: FontWeight.w600),),
                      ],
                    ),
                  ),
                  ]
              ),
            ),
            SizedBox(height: 15,),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Preview",style: TextStyle(fontSize: 15,fontWeight: FontWeight.w600),),
                  Icon(Ionicons.chevron_forward,size: 20,)
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Edit Listing",style: TextStyle(fontSize: 15,fontWeight: FontWeight.w600),),
                  Icon(Ionicons.chevron_forward,size: 20,)
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Manage Media",style: TextStyle(fontSize: 15,fontWeight: FontWeight.w600),),
                  Icon(Ionicons.chevron_forward,size: 20,)
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 0.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text("Deactivate",style: TextStyle(fontSize: 15,fontWeight: FontWeight.w600),),
                  Container(
                    width: 20,
                    child: Transform.scale(
                      scale: 0.6,
                      child: CupertinoSwitch(
                        value: false,
                        onChanged: (bool value) {

                        },
                      ),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Remove",style: TextStyle(fontSize: 15,fontWeight: FontWeight.w600),),
                  Icon(Ionicons.chevron_forward,size: 20,)
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Text("Boost Listing",style: TextStyle(fontSize: 15,fontWeight: FontWeight.w600),),
                      Text(" ( Paid )",style: TextStyle(fontSize: 12,color: myResources.themeColor),),
                    ],
                  ),
                  Icon(Ionicons.chevron_forward,size: 20,)
                ],
              ),
            ),
            SizedBox(height: 15,),
            Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  color: Colors.blue.shade50.withOpacity(0.8),
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(color: Colors.blue.shade100)),
              child: Row(
                children: const [
                  Padding(
                    padding: EdgeInsets.all(15.0),
                    child: Icon(
                      Ionicons.information_circle,
                      size: 40,
                      color: Color(0xff009cbd),
                    ),
                  ),
                  Expanded(
                      child: Text(
                        "Boost your listings will display them on top in search results, in the feature list and also in recommendations.",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 13.5,
                          fontWeight: FontWeight.w500
                        ),))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
