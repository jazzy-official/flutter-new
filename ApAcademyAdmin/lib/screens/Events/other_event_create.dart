import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:ap_academy_admin/ModelView/Controller/matchController.dart';
import 'package:ap_academy_admin/ModelView/Controller/otherEventModel.dart';
import 'package:ap_academy_admin/screens/Events/events_screen.dart';
import 'package:ap_academy_admin/screens/Events/other_event.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class OtherCreateEvent extends StatefulWidget {
  final Map<String, dynamic>? otherEvent;
  const OtherCreateEvent({Key? key, this.otherEvent}) : super(key: key);

  @override
  _OtherCreateEventState createState() => _OtherCreateEventState();
}

class _OtherCreateEventState extends State<OtherCreateEvent> {
  //////////////////////////-------------------/////////////////////////////
  TextEditingController titleController = TextEditingController();

  TextEditingController venueController = TextEditingController();

  TextEditingController desController = TextEditingController();

  TextEditingController timeController = TextEditingController();
  String selectedcoach = '';
  ////////////////////////-------------------/////////////////////////////
  final eventController = Get.put(OtherEventController());
  final mController = Get.put(MatchController());
  DateTime? time1;

  Future<void> createNewEvent() async {
    if (titleController.text.isNotEmpty &&
        venueController.text.isNotEmpty &&
        selectedcoach != ''&& time1 != null
    ) {
      Map<String, dynamic> otherEventModel = Map();
      final dbHelper = Get.put(DatabaseController());
      otherEventModel['eventName'] = titleController.text;
      otherEventModel['venue'] = venueController.text;
      otherEventModel['time'] = time1;
      otherEventModel['coach'] =
          selectedcoach; //mController.selectedCoachValue.value;
      otherEventModel['description'] = desController.text;
      otherEventModel['timeCreated'] = DateTime.now();
      if (widget.otherEvent == null) {
        bool check = await dbHelper.createEvent(otherEventModel);

        if (check) {
          Get.back();
           Get.snackbar("Saved", "New event is Saved",
              snackPosition: SnackPosition.BOTTOM,
              icon: Icon(
                Icons.check_circle,
                color: Colors.green,
              ));
          // Future.delayed();


          eventController.refreshDate();
        } else {
          Get.snackbar("Not Saved", "New event is not Saved",
              snackPosition: SnackPosition.BOTTOM,
              icon: Icon(
                Icons.cancel_outlined,
                color: Colors.red,
              ));
        }
      } else {
        otherEventModel['_id'] = widget.otherEvent!['_id'];
        bool check = await dbHelper.updateEvent(otherEventModel);
        if (check) {
          Get.back();
          Get.snackbar("Saved", "New event is Updated",
              snackPosition: SnackPosition.BOTTOM,
              icon: Icon(
                Icons.check_circle,
                color: Colors.green,
              ));
          eventController.refreshDate();
        } else {
          Get.snackbar("Not Saved", "New event is not Saved",
              snackPosition: SnackPosition.BOTTOM,
              icon: Icon(
                Icons.cancel_outlined,
                color: Colors.red,
              ));
        }
      }
    } else {
      Get.snackbar("Error", "Fill all required Fields");
    }
  }

  populateData() {
    if (widget.otherEvent != null) {
      titleController.text = widget.otherEvent!['eventName'];
      venueController.text = widget.otherEvent!['venue'];
      time1 = widget.otherEvent!['time'];
      selectedcoach = widget.otherEvent!['coach'] != null
          ? widget.otherEvent!['coach']
          : '';
      timeController.text = DateFormat.yMEd().format(time1!);
      desController.text = widget.otherEvent!['description'];
    }
  }

  @override
  void initState() {
    populateData();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyResources.backgroundColor,
      appBar: AppBar(
        backgroundColor: MyResources.buttonColor,
        title: Container(
          child: Text(
            'EVENTS',
            style: GoogleFonts.roboto(),
          ),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 20.0),
              child: Center(
                child: Text(
                  "CREATE EVENT",
                  style: GoogleFonts.roboto(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 40.0, left: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Title:",
                    style: GoogleFonts.roboto(
                        fontSize: 15, fontWeight: FontWeight.w400),
                  ),
                ],
              ),
            ),
            Padding(
              //Add padding around textfield
              padding: EdgeInsets.only(top: 7.0, left: 10, right: 10),
              child: Container(
                color: MyResources.backgroundColor,
                //   height: 50,
                child: TextField(
                  controller: titleController,
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0, left: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Venue:",
                    style: GoogleFonts.roboto(
                        fontSize: 15, fontWeight: FontWeight.w400),
                  ),
                ],
              ),
            ),
            Padding(
              //Add padding around textfield
              padding: EdgeInsets.only(top: 7.0, left: 10, right: 10),
              child: Container(
                color: MyResources.backgroundColor,
                // height: 50,
                child: TextField(
                  controller: venueController,
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0, left: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Coach:",
                    style: GoogleFonts.roboto(
                        fontSize: 15, fontWeight: FontWeight.w400),
                  ),
                ],
              ),
            ),
            Padding(
              //Add padding around textfield
              padding: EdgeInsets.only(top: 7.0, left: 10, right: 10),
              child: Padding(
                padding: const EdgeInsets.all(0.0),
                child: Container(
                  decoration: BoxDecoration(
                      color: MyResources.backgroundColor,
                      border: Border.all(color: Colors.grey),
                      borderRadius: BorderRadius.circular(10)),
                  //  color: MyResources.backgroundColor,

                  height: 60,
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: DropdownButton(
                      isExpanded: true,
                      underline: Text(""),
                      value: selectedcoach,
                      // focusColor: Colors.blue,
                      elevation: 12,
                      items: mController.coachdropValues.map(
                        (map) {
                          return DropdownMenuItem(
                            child: Text(map['name']),
                            value: map['value'],
                          );
                        },
                      ).toList(),
                      onChanged: (value) {
                        print(value);
                        setState(() {
                          selectedcoach = value.toString();
                        });
                      },
                      hint: Text('Select Item'),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0, left: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Time:",
                    style: GoogleFonts.roboto(
                        fontSize: 15, fontWeight: FontWeight.w400),
                  ),
                ],
              ),
            ),
            Padding(
              //Add padding around textfield
              padding: EdgeInsets.only(top: 7.0, left: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: InkWell(
                      onTap: () {
                        DatePicker.showDateTimePicker(context,
                            showTitleActions: true, onChanged: (time) {
                          print('change $time');
                        }, onConfirm: (time) {
                          timeController.text = DateFormat.yMEd().format(time) +
                              ",  " +
                              DateFormat.Hm().format(time);
                          time1 = time;
                          print('confirm $time');
                        }, currentTime: DateTime.now(), locale: LocaleType.en);
                      },
                      child: Container(
                        decoration: MyResources.roundedBoxDecore,
                        child: TextFormField(
                          enabled: false,
                          readOnly: true,
                          controller: timeController,
                          decoration: InputDecoration(
                            prefixIcon: Icon(Icons.alarm_add_rounded),

                            //Add th Hint text here.
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0, left: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Description:",
                    style: GoogleFonts.roboto(
                        fontSize: 15, fontWeight: FontWeight.w400),
                  ),
                ],
              ),
            ),
            Padding(
              //Add padding around textfield
              padding: EdgeInsets.only(top: 7.0, left: 10, right: 10),
              child: Container(
                height: 120,
                color: MyResources.backgroundColor,
                child: TextField(
                  controller: desController,
                  keyboardType: TextInputType.multiline,
                  maxLines: 5,
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20.0, left: 10, right: 10),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.9,
                height: 45,
                child: RaisedButton(
                  textColor: Colors.white,
                  color: MyResources.buttonColor,
                  child: Text(
                    "SUBMIT",
                  ),
                  onPressed: () async {
                    await createNewEvent().then((value) {

                    });
                  },
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
