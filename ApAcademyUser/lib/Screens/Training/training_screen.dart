import 'package:ap_academy_new/ModelView/controllers/profileController.dart';
import 'package:ap_academy_new/ModelView/controllers/trainingController.dart';

import 'package:ap_academy_new/shared/loading_widget.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_new/Constants/util.dart';
import 'package:get/get.dart';
import 'package:get/route_manager.dart';
import 'package:intl/intl.dart';

class TrainingScreen extends StatefulWidget {
  @override
  _TrainingScreenState createState() => _TrainingScreenState();
}

class _TrainingScreenState extends State<TrainingScreen> {
  bool isSwitched = false;
  final trainControl = Get.put(TrainingController());
  final profileControl = Get.put(ProfileController());
  refreshData() {
    trainControl.refrehData();
  }

  @override
  void initState() {
    refreshData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: myResources.primaryColor,
      body: Material(
        child: GetX<TrainingController>(
          init: Get.put<TrainingController>(TrainingController()),
          builder: (TrainingController trainingController) {
            if (trainingController.training != null) {
              return Stack(children: [
                Center(
                  child: ColorFiltered(
                      colorFilter: ColorFilter.mode(
                          Colors.white.withOpacity(0.3), BlendMode.dstATop),
                      child: Image.asset(
                        'assets/notraining.png',
                        colorBlendMode: BlendMode.hue,
                        width: 200,
                        height: 200,
                      )),
                ),
                ListView.builder(
                    itemCount: trainingController.training.length,
                    itemBuilder: (BuildContext context, int index) {
                      bool isadded = false;
                      List temp =
                          trainingController.training[index]['totalPlayer'];
                      temp.forEach((element) {
                        if (element['firebaseId'] ==
                            FirebaseAuth.instance.currentUser!.uid.toString()||element['ParentId']==profileControl.temp['ParentId']) {
                          isadded = true;
                        }
                      });
                      if (isadded)
                        return getTrainingCard(
                            trainingController.training[index]);
                      else
                        return Container();
                    }),
              ]);
            } else {
              return Container(
                child: LoadingWidget(),
              );
            }
          },
        ),
      ),
    );
    // );
  }

  bool checkIfAttend(List atIds) {
    bool alread = false;
    atIds.forEach((element) {
      if (element.toString() == FirebaseAuth.instance.currentUser!.uid) {
        alread = true;
      }
    });
    return alread;
  }

  Widget getTrainingCard(Map<String, dynamic> trainingModel) {
    bool isAttended = checkIfAttend(trainingModel['attendance']);

    return Padding(
      padding: const EdgeInsets.all(10),
      child: Center(
        child: Stack(
          children: [
            Container(
              height: Get.height*0.2,
              width: Get.width,
              //create the cards for all details of event happening
              child: Card(
                color: myResources.backgroundColor,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          width: Get.width * 0.8,
                          child: Text(trainingModel['session'],
                              //textAlign: TextAlign.center,
                              overflow: TextOverflow.ellipsis,
                              style: myResources.appHeadingStyle),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Coach:", style: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          )
                          ),
                          Container(
                            width: 200,
                            child: Text(
                              trainingModel['coach'],
                              style: TextStyle(color: myResources.coachHeading),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Venue:", style: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          )),
                          Container(
                            width: 200,
                            child: Text(
                              trainingModel['venue'],
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Time:", style:TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          )),
                          Container(
                            width: 200,
                            child: Text(
                              DateFormat.yMEd()
                                  .add_Hm()
                                  .format(trainingModel['time'].toLocal()),
                              style: TextStyle(color: myResources.headingColor),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Availability:",
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              )),
                          SizedBox(
                            width: 30,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Container(
                                  width: 60,
                                  height: 20,
                                  child: RaisedButton(
                                    textColor: Colors.white,
                                    color:
                                        isAttended ? Colors.green : Colors.grey,
                                    child: Text(
                                      "Yes",
                                    ),
                                    onPressed: () async {
                                      Get.dialog(Center(child: LoadingWidget()),
                                          barrierDismissible: false);
                                      await trainControl
                                          .saveAttend(trainingModel['_id'])
                                          .then((value) {
                                        if (value) {
                                          Get.back();
                                          Get.defaultDialog(
                                            buttonColor:
                                                myResources.buttonColor,
                                            title: "Saved",
                                            confirmTextColor: Colors.white,
                                            content: Text(
                                                "Your attendance is saved"),
                                            onConfirm: () {
                                              isAttended = checkIfAttend(
                                                  trainingModel['attendance']);
                                              Get.back();
                                            },
                                          );
                                          trainControl.refrehData();
                                          setState(() {});
                                        }
                                      });
                                      // setState(() {});
                                    },
                                    shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(10.0),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Container(
                                  width: 60,
                                  height: 20,
                                  child: RaisedButton(
                                    textColor: Colors.white,
                                    color:
                                        isAttended ? Colors.grey : Colors.red,
                                    child: Text(
                                      "No",
                                    ),
                                    onPressed: () async {
                                      Get.dialog(Center(child: LoadingWidget()),
                                          barrierDismissible: false);
                                      await trainControl
                                          .deletAttend(trainingModel['_id'])
                                          .then((value) {
                                        if (value) {
                                          Get.back();
                                          Get.snackbar("Available",
                                              'You have benn marked as absent...',
                                              snackPosition:
                                                  SnackPosition.BOTTOM);
                                          isAttended = false;
                                          trainControl.refrehData();
                                          // isAttended = checkIfAttend(
                                          //     trainingModel['attendance']);
                                          setState(() {});
                                        }
                                      });
                                    },
                                    shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(10.0),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
            trainingModel['canSchedual']
                ? Positioned(
              top: 4,
              right: 6,
              child: Container(
                color: Colors.red[500],
                width: 62,
                height: 14,
                child: Padding(
                    padding: const EdgeInsets.only(
                        top: 1.0, bottom: 2.0,left:1.0 ),
                    child: Text(
                        'Scheduled',
                        style: TextStyle(color: Colors.white, fontSize: 12)
                    )),
              ),
            )
                : Container(),
          ],
        ),
      ),
    );
  }
}
