import 'package:meta/meta.dart';

class Document {
  final int? id;
  final String title;
  final String code;
  final double balance;
  final List<Document> childData;
  final List<Document> account;

  Document({
    this.id,
    required this.title,
    required this.code,
    required this.balance ,
    this.account = const <Document>[],
    this.childData = const <Document>[],
  });



  factory Document.fromJson(dynamic json) {
    return Document(
      id: json['accid'],
      title : json['accountTitle']??json['accounttitle'],
      code : json['idcode']??json['accountno'],
      account:json['account']==null||json['account'].isEmpty?[]:json['account'].map<Document>(
            (dynamic item) => Document.fromJson(item),
      ).toList(),
      childData: json['inverseParentAccNoNavigation']==[]||json['inverseParentAccNoNavigation']==null?[]: json['inverseParentAccNoNavigation']
          .map<Document>(
            (dynamic item)  =>   Document.fromJson(item),
      ).toList(),
      balance: (json['balance']??json['currentBalance']).toDouble(),
    );
  }

}

// Document chartAccountfromJson(dynamic json) {
//   Document d = Document(
//     id: json['accid'],
//     title : json['accounttitle'],
//     code : json['accountno'],
//     childData: <Document>[],
//     balance: json['currentBalance'].toDouble(),
//   );
//   return d;
// }