

import 'package:ap_ground_admin/Screens/Grounds/Models/slot_model.dart';

class Template{
  final String? id;
  final String name;
  List<Slot> slots;
  Template({this.id,required this.name,required this.slots});

  factory Template.fromJson(dynamic json,String? id) {
    return Template(
      id: id.toString(),
      name : json['name'],
      slots : List<Slot>.from(json["slots"].map((dynamic item) => Slot.fromJson(item),).toList()),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = this.name;
    data['slots'] = List<dynamic>.from(slots.map((x) => x.toJson()));
    return data;
  }

}