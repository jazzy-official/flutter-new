import 'package:ap_academy_new/ModelView/controllers/auth_controller.dart';
import 'package:ap_academy_new/ModelView/controllers/db_controller.dart';
import 'package:ap_academy_new/ModelView/controllers/notification_controller.dart';
import 'package:ap_academy_new/ModelView/controllers/profileController.dart';
import 'package:ap_academy_new/Screens/Announcements/Notifications_screen.dart';
import 'package:ap_academy_new/Screens/Events/events_screen.dart';
import 'package:ap_academy_new/Screens/Gallery/gallery_screen.dart';
import 'package:ap_academy_new/Screens/Training/training_screen.dart';
import 'package:ap_academy_new/shared/DeletPop.dart';
import 'package:ap_academy_new/shared/loading_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:ap_academy_new/Constants/util.dart';
import 'Banks/BankList.dart';
import 'Setting/profile_edit.dart';
import 'Sponsors/SponsorsList.dart';
import 'dashboard_screen.dart';
import 'package:url_launcher/url_launcher.dart';

RxString sideTitle = "Announcements".obs;
Rx<Widget> selectedWidget = AnnouncementScreen().obs;

class HomeDrawer extends StatefulWidget {
  @override
  _HomeDrawerState createState() => _HomeDrawerState();
}

class _HomeDrawerState extends State<HomeDrawer> {
///////////////////////////////////////////GetXController///////////////
  final authController = Get.put(AuthController());
  final profileController = Get.put(ProfileController());
  final notificationController = Get.put(NotificationController());
  final database = Get.put(DatabaseController());
  TextEditingController _rollnoController = TextEditingController();
  static const _url = 'https://aiksol.com';
  /////////////////////////////////
  @override
  void dispose() {
    sideTitle = "Announcements".toUpperCase().obs;
    selectedWidget = AnnouncementScreen().obs;
    super.dispose();
  }

  Future<void> _launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: false,
        forceWebView: false,
        headers: <String, String>{'my_header_key': 'my_header_value'},
      );
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        backgroundColor: myResources.buttonColor,
        title: Obx(() => Text(sideTitle.value)),
        centerTitle: true,
      ),
      drawer: Theme(
        data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
        child: Drawer(
            child: Column(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Get.to(() => const ProfileEdit(
                      isapproved: true,
                      isDrawer: true,
                    ));
                  },
                  child: Stack(
                    children: [
                      Container(
                        height: 100,
                        width: Get.width,
                        child: SvgPicture.asset(
                          'assets/drawerBack.svg',
                          allowDrawingOutsideViewBox: true,
                          //   width: Get.width,
                          fit: BoxFit.fill,
                        ),
                      ),
                      DrawerHeader(
                          padding: EdgeInsets.all(0),
                          child: ClipRRect(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(35),
                                bottomRight: Radius.circular(35)),
                            child: Container(
                              child: Stack(
                                children: [
                                  Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(left: 8.0),
                                        child: ClipRRect(
                                          borderRadius:
                                          BorderRadius.circular(100.0),
                                          child: profileController != null
                                              ? CachedNetworkImage(
                                            height: 90,
                                            fit: BoxFit.cover,
                                            width: 90,
                                            imageUrl: profileController
                                                .currentUdetail !=
                                                null
                                                ? profileController
                                                .temp['photoUrl']
                                                .toString()
                                                : '',
                                            placeholder: (context, url) =>
                                                LoadingWidget(),
                                            errorWidget:
                                                (context, url, error) =>
                                                Icon(Icons.error),
                                          )
                                              : Container(
                                            width: 90,
                                            height: 90,
                                          ),
                                        ),
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      SizedBox(
                                        // width: 180,
                                        child: Column(
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding:
                                              const EdgeInsets.only(top: 8.0),
                                              child: Center(
                                                child: Text(
                                                    profileController.currentUdetail ==
                                                        null
                                                        ? "Not Defined"
                                                        : profileController
                                                        .currentUdetail['rollNo'],
                                                    style: TextStyle(fontSize: 10)),
                                              ),
                                            ),
                                            Container(
                                              width: 180,
                                              height: 35,
                                              padding:
                                               EdgeInsets.only(top: 8.0),
                                              child: Center(
                                                child: Text(
                                                  profileController.currentUdetail ==
                                                      null
                                                      ? "Not Defined"
                                                      : profileController
                                                      .temp['name'],
                                                  overflow: TextOverflow.fade,
                                                  style:
                                                  myResources.appHeadingStyle,
                                                ),
                                              ),
                                            ),
                                            Center(
                                              child: Text(
                                                profileController.temp != null ?
                                                profileController.temp['email']
                                                    .toString()
                                                    : 'No email',
                                                overflow: TextOverflow.fade,
                                                style: myResources.textStyleprofile,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          )),
                    ],
                  ),
                ),
                if(FirebaseAuth.instance.currentUser!.uid == profileController.temp['ParentId'])
                  Container(
                    color: Colors.grey[50],
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(3)),
                                side: BorderSide(
                                    color: myResources.buttonColor)),
                          ),
                          onPressed: () {
                            // Get.to(()=> const SignupFormTwo());
                          },
                          child: Container(
                            constraints: BoxConstraints(
                                maxWidth: 200
                            ),
                            margin: const EdgeInsets.symmetric(vertical: 0,
                                horizontal: 8),
                            height: 35,
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                DropdownButton<String>(
                                  value: profileController
                                      .temp['name'],
                                  icon: Icon(
                                    CupertinoIcons.chevron_down,
                                    size: 18,
                                    color: myResources.buttonColor,
                                  ),
                                  style: TextStyle(
                                      color: myResources.buttonColor),
                                  underline: Container(
                                    height: 0,
                                    color: Colors.transparent,
                                  ),
                                  onChanged: (String? newValue) async {
                                    await profileController
                                        .setuserdata(newValue!);
                                    await fcmListner();
                                    await database.updateparenttoken(
                                        notificationController.userToken.value!,
                                        profileController.temp['firebaseId']);
                                    setState(() {

                                    });
                                  },
                                  items: <String>[
                                    for(var i in profileController.userDetail)
                                      i['name']
                                  ].map<DropdownMenuItem<String>>((
                                      String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                )
                              ],
                            ),
                          ),
                        ),
                        SizedBox(width: 10,),
                        SizedBox(
                          width: 40,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.all(0),
                              primary: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(3)),
                                  side: BorderSide(
                                      color: myResources.buttonColor)),
                            ),
                            onPressed:
                                () {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      content: Form(
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            Container(
                                              height: 40,
                                              child: Padding(
                                                //Add padding around textfield
                                                padding: EdgeInsets.only(
                                                    top: 0.0,
                                                    left: 10,
                                                    right: 10),
                                                child: TextFormField(
                                                  controller: _rollnoController,
                                                  validator: (val) {
                                                    if (val!.length == 0) {
                                                      return ('Please enter roll no');
                                                    }
                                                    return null;
                                                  },
                                                  decoration: InputDecoration(
                                                    //Add th Hint text here.
                                                    hintText: "roll no",

                                                    hintStyle: myResources
                                                        .hintfontStyle,
                                                    border: OutlineInputBorder(
                                                      borderRadius: BorderRadius
                                                          .circular(10.0),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.all(
                                                  8.0),
                                              child: ElevatedButton(
                                                style: ElevatedButton.styleFrom(
                                                    primary: myResources
                                                        .buttonColor),
                                                child: Text("Link account"),
                                                onPressed: () {
                                                  if (_rollnoController.text
                                                      .length == 0) {
                                                    Get.snackbar('Warning',
                                                        'Please enter roll no',
                                                        snackPosition: SnackPosition
                                                            .BOTTOM);
                                                  } else {
                                                    authController.linkParent(
                                                        _rollnoController.text);
                                                  }
                                                },
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    );
                                  });
                            },
                            child: Container(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 0, horizontal: 0),
                                height: 35,
                                child: Icon(
                                  Icons.add, color: myResources.buttonColor,)
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),

                Flexible(
                  fit: FlexFit.tight,
                  child: Column(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            border:
                            sideTitle.value == "ANNOUNCEMENTS".toUpperCase()
                                ? Border(
                              left: BorderSide(
                                color: myResources.drawerIconColor,
                                width: 5.0,
                              ),
                            )
                                : Border()),
                        child: ListTile(
                          title: Text(
                            "ANNOUNCEMENTS",
                            style: myResources.myTextStyle,
                          ),
                          tileColor:
                          sideTitle.value == "ANNOUNCEMENTS".toUpperCase()
                              ? myResources.drawerIconColor.withOpacity(0.2)
                              : Colors.transparent,
                          leading: Icon(
                            Icons.notifications,
                            color: myResources.drawerIconColor,
                          ),
                          onTap: () {
                            if (mounted)
                              setState(() {
                                selectedWidget = AnnouncementScreen().obs;
                                sideTitle.value = "ANNOUNCEMENTS".toUpperCase();
                              });
                            Get.back();
                          },
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            border: sideTitle.value == "Schedule".toUpperCase()
                                ? Border(
                              left: BorderSide(
                                color: myResources.drawerIconColor,
                                width: 5.0,
                              ),
                            )
                                : Border()),
                        child: ListTile(
                          dense: true,
                          tileColor: sideTitle.value == "Schedule".toUpperCase()
                              ? myResources.drawerIconColor.withOpacity(0.2)
                              : Colors.transparent,
                          /*  selected: true,*/
                          title: Text(
                            "Matches".toUpperCase(),
                            style: myResources.myTextStyle,
                          ),
                          leading: Icon(Icons.event,
                              color: myResources.drawerIconColor),
                          onTap: () {
                            if (mounted)
                              setState(() {
                                selectedWidget = EventScreen().obs;
                                sideTitle = "Schedule".toUpperCase().obs;
                              });
                            Get.back();

                            // Navigator.push(context,
                            //     MaterialPageRoute(builder: (context) => EventScreen()));
                          },
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            border: sideTitle.value == "Trainings".toUpperCase()
                                ? Border(
                              left: BorderSide(
                                color: myResources.drawerIconColor,
                                width: 5.0,
                              ),
                            )
                                : Border()),
                        child: ListTile(
                          dense: true,
                          tileColor:
                          sideTitle.value == "Trainings".toUpperCase()
                              ? myResources.drawerIconColor.withOpacity(0.2)
                              : Colors.transparent,
                          /*  selected: true,*/
                          title: Text(
                            "Trainings".toUpperCase(),
                            style: myResources.myTextStyle,
                          ),
                          leading: Icon(Icons.sports_cricket,
                              color: myResources.drawerIconColor),
                          onTap: () {
                            if (mounted)
                              setState(() {
                                selectedWidget = TrainingScreen().obs;
                                sideTitle.value = "Trainings".toUpperCase();
                              });
                            Get.back();

                            // Navigator.push(context,
                            //     MaterialPageRoute(builder: (context) => EventScreen()));
                          },
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            border: sideTitle.value == "Sponsors".toUpperCase()
                                ? Border(
                              left: BorderSide(
                                color: myResources.drawerIconColor,
                                width: 5.0,
                              ),
                            )
                                : Border()),
                        child: ListTile(
                          dense: true,
                          tileColor: sideTitle.value == "Sponsors".toUpperCase()
                              ? myResources.drawerIconColor.withOpacity(0.2)
                              : Colors.transparent,
                          // selected: true,
                          title: Text(
                            "Sponsors".toUpperCase(),
                            style: myResources.myTextStyle,
                          ),
                          leading: Icon(Icons.sports_kabaddi,
                              color: myResources.drawerIconColor),
                          onTap: () {
                            if (mounted)
                              setState(() {
                                selectedWidget = SponsorList().obs;
                                sideTitle = "Sponsors".toUpperCase().obs;
                              });
                            Get.back();
                          },
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            border: sideTitle.value ==
                                "Payment Methods".toUpperCase()
                                ? Border(
                              left: BorderSide(
                                color: myResources.drawerIconColor,
                                width: 5.0,
                              ),
                            )
                                : Border()),
                        child: ListTile(
                          dense: true,
                          tileColor:
                          sideTitle.value == "Payment Methods".toUpperCase()
                              ? myResources.drawerIconColor.withOpacity(0.2)
                              : Colors.transparent,
                          // selected: true,
                          title: Text(
                            "Payment Methods".toUpperCase(),
                            style: myResources.myTextStyle,
                          ),
                          leading: Icon(Icons.payments_outlined,
                              color: myResources.drawerIconColor),
                          onTap: () {
                            if (mounted)
                              setState(() {
                                selectedWidget = BankList().obs;
                                sideTitle = "Payment Methods".toUpperCase().obs;
                              });
                            Get.back();
                          },
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            border: sideTitle.value == "Gallery".toUpperCase()
                                ? Border(
                              left: BorderSide(
                                color: myResources.drawerIconColor,
                                width: 5.0,
                              ),
                            )
                                : Border()),
                        child: ListTile(
                          dense: true,
                          tileColor: sideTitle.value == "Gallery".toUpperCase()
                              ? myResources.drawerIconColor.withOpacity(0.2)
                              : Colors.transparent,
                          title: Text(
                            "Gallery".toUpperCase(),
                            style: myResources.myTextStyle,
                          ),
                          leading: Icon(Icons.image,
                              color: myResources.drawerIconColor),
                          onTap: () {
                            if (mounted)
                              setState(() {
                                selectedWidget = GalleryScreen().obs;
                                sideTitle = "Gallery".toUpperCase().obs;
                              });
                            Get.back();
                          },
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            border: sideTitle.value == "Log Out".toUpperCase()
                                ? Border(
                              left: BorderSide(
                                color: myResources.drawerIconColor,
                                width: 5.0,
                              ),
                            )
                                : Border()),
                        child: ListTile(
                          dense: true,
                          tileColor: sideTitle.value == "Log Out".toUpperCase()
                              ? myResources.drawerIconColor.withOpacity(0.2)
                              : Colors.transparent,
                          // selected: true,
                          title: Text(
                            "Log out".toUpperCase(),
                            style: myResources.myTextStyle,
                          ),
                          leading: Icon(
                            Icons.exit_to_app_outlined,
                            color: myResources.drawerIconColor,
                          ),
                          onTap: () {
                            setState(
                                  () {
                                // sideTitle = "Log Out".obs;

                                Get.back();
                              },
                            );
                            Get.dialog(Center(
                                child: DeletpopUp(
                                  message: 'Are you sure you want to sign out?',
                                  pressFunc: () async {
                                    sideTitle = "Announcements".obs;
                                    await authController.signOut();
                                  },
                                  isNotDel: true,
                                )));
                          },
                        ),
                      ),
                    ],
                  ),
                ),

                // Container(
                //   decoration: BoxDecoration(
                //       border: sideTitle.value == "Dashboard"
                //           ? Border(
                //               left: BorderSide(
                //                 color: myResources.drawerIconColor,
                //                 width: 5.0,
                //               ),
                //             )
                //           : Border()),
                //   child: ListTile(
                //     dense: true,
                //     tileColor: sideTitle.value == "Dashboard"
                //         ? Color(0xffbe61ca).withOpacity(0.2)
                //         : Colors.transparent,
                //     // selected: true,
                //     title: Text(
                //       "DashBoard",
                //       style: myResources.myTextStyle,
                //     ),
                //     leading: Icon(Icons.dashboard,
                //         color: myResources.drawerIconColor),
                //     onTap: () {
                //       if (mounted)
                //         setState(() {
                //           selectedWidget = DashBoard().obs;
                //           sideTitle = "Dashboard".obs;
                //         });
                //       Get.back();
                //       //Navigator.pushReplacement(context,MaterialPageRoute(builder: (context)=>DashBoard()));
                //       //(context,
                //       //  MaterialPageRoute(builder: (context) => DashBoard()));
                //     },
                //   ),
                // ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 5.0),
                  child: Column(
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 120,
                        width: Get.width,
                        child: Image.asset(
                          "assets/ap.jpg",
                          width: 200,
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                      InkWell(
                        onTap: () async {
                          await _launchInBrowser(_url);
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(left: 15.0),
                          child: Container(
                              height: 14,
                              child: Text(
                                'Powered by AikSol. 2021',
                                style: TextStyle(
                                  fontSize: 12,
                                ),
                                // textAlign: TextAlign.start,
                              )),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            )),
      ),
      body: selectedWidget.value,
    );
  }
}
