import 'package:ap_ground_admin/constants/util.dart';
import 'package:ap_ground_admin/controllers/booking_controller.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../../loading_widget.dart';
import 'Models/booking_model.dart';
import 'booking_card.dart';

class TodayBookingScreen extends StatefulWidget {
  final List<Booking> todayBookings;
  const TodayBookingScreen({Key? key, required this.todayBookings}) : super(key: key);

  @override
  _TodayBookingScreenState createState() => _TodayBookingScreenState();
}

class _TodayBookingScreenState extends State<TodayBookingScreen> {

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xfffefefe),
      child: Column(
        children: [
          widget.todayBookings.isEmpty ? Expanded(child: Center(child: Text("No Result found"),)):
          Expanded(
            child: ListView.builder(
                padding: const EdgeInsets.all(8),
                itemCount: widget.todayBookings.length,
                itemBuilder: (BuildContext context, int index) {
                  Booking booking = widget.todayBookings[index];
                  return BookingCard(booking: booking,);


                }),
          ),
        ],
      ),
    );
  }
}
