import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:rafi_admin/Components/loader_widget.dart';
import 'package:rafi_admin/Controllers/receiveable_controller.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Model/receivable_model.dart';

class ReceivableScreen extends StatefulWidget {
  ReceivableScreen({Key? key,}) : super(key: key);

  @override
  State<ReceivableScreen> createState() => _ReceivableScreenState();
}

class _ReceivableScreenState extends State<ReceivableScreen> {
  @override
  Widget build(BuildContext context) {
    var formatter = NumberFormat('#,##,000');
    return GetBuilder<ReceivableController>(
      autoRemove: false,
      init: ReceivableController(),
      builder: (cont) => Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          Container(
            width: MediaQuery.of(context).size.width - 30,
            // (MediaQuery.of(context).size.width * 1) - 90,
            alignment: Alignment.centerLeft,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8),
                boxShadow: const [
                  BoxShadow(
                      color: Colors.black26,
                      blurRadius: 5,
                      offset: Offset(0,0.5)
                  )
                ]
            ),
            height: 45,
            child: TextFormField(
              onChanged: (val){
                cont.runFilter(val);
              },
              style: const TextStyle(
                color: Colors.black87,
              ),
              decoration: const InputDecoration(
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.only(top: 14,),
                  prefixIcon: Icon(
                    Icons.search,
                    color: Color(0xff7F7C82),
                  ),
                  hintText: 'Search',
                  hintStyle: TextStyle(
                    color: Colors.black38,
                  )
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Flexible(
            child: cont.receivables==null?LoadingWidget(): cont.foundResult.isNotEmpty
                ? RefreshIndicator(
              onRefresh: () async {
                return await cont.refreshAll();
              },
                  child: ListView.builder(
              physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
              itemCount:cont.foundResult.length,
              itemBuilder: (context, index) {
                  final receivable = cont.foundResult[index];
                  return Padding(
                    padding: const EdgeInsets.only(right: 5.0, left: 5),
                    child: Card(
                      color: Color(0xffbf953f),
                      child: InkWell(
                          splashColor: Colors.blue.withAlpha(30),
                          onTap: () {

                          },
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(3),
                              color: Color(0xfff1f6f8),
                            ),
                            padding: const EdgeInsets.all(15),
                            margin: EdgeInsets.all(1),
                            // height: 200,
                            child: Column(
                              mainAxisAlignment:
                              MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'Balance : ' + formatter.format(receivable.currentbalance),
                                          style: const TextStyle(
                                              color: Color(0xff004251),
                                              fontWeight: FontWeight.w500),
                                        ),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                Column(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      receivable.accounttitle,
                                      style: const TextStyle(
                                          color:  Color(0xff004251),
                                          fontWeight: FontWeight.w500,
                                          fontSize: 22),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                      CrossAxisAlignment.end,
                                      children: [
                                        Text(
                                          receivable.accountno,
                                          style: const TextStyle(
                                            color:  Color(0xff004251),
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                        SizedBox(
                                          width: 100,
                                          child: Flex(
                                            mainAxisSize: MainAxisSize.min,
                                            direction: Axis.vertical,
                                            children: [
                                              Flexible(
                                                child: Text(
                                                  receivable.accountdes,
                                                  style: const TextStyle(
                                                      color:  Color(0xff004251),
                                                      fontSize: 11),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          )),
                    ),
                  );
              },
            ),
                )
                : const Center(
              child: Text(
                'No results found',
                style: TextStyle(fontSize: 24),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
