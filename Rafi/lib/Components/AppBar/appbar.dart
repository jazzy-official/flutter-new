import 'package:flutter/material.dart';
class Header extends StatelessWidget {
  final String title;
  const Header({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      title: Text(title, style: const TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.w500,
                        color: Colors.white),),
      backgroundColor: Color(0xff004251),
    )
    //   Container(
    //   child: Row(
    //     crossAxisAlignment: CrossAxisAlignment.center,
    //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //     children: [
    //       TextButton(
    //         onPressed: () => Scaffold.of(context).openDrawer(),
    //         child: Center(
    //           child: Column(
    //             mainAxisAlignment: MainAxisAlignment.center,
    //             children: const [
    //               Icon(Icons.menu,color: Colors.white,),
    //             ],
    //           ),
    //         ),
    //       ),
    //       SizedBox(
    //         child: Center(
    //           child: Text(
    //             title,
    //             style: const TextStyle(
    //                 fontSize: 20.0,
    //                 fontWeight: FontWeight.w500,
    //                 color: Colors.white),
    //           ),
    //         ),
    //       ),
    //       const SizedBox(width: 50,)
    //     ],
    //   ),
    //   decoration:  const BoxDecoration(
    //     color: Color(0xff004251),
    //     // boxShadow: [
    //     //   BoxShadow(
    //     //     color: Colors.grey,
    //     //     blurRadius: 20.0,
    //     //     spreadRadius: 1.0,
    //     //   )
    //     // ]
    //   ),
    // )
    ;
  }
}
