
import 'package:ap_ground_admin/Screens/Canteen/Models/report_model.dart';

class Product{
  final String? id;
  final String name;
  final List<ReportModel> reports;
  final int qty;
  final int price;
  Product({this.id,required this.name,required this.reports, required this.price,required this.qty,});

  factory Product.fromJson(dynamic json,String? id) {
    return Product(
      id: id.toString(),
      name : json['name'],
      reports : json["reports"]==null ? [] : List<ReportModel>.from(json["reports"].map((dynamic item) => ReportModel.fromJson(item),).toList()),
      price: json['price'],
      qty: json['qty'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = name;
    data['reports'] = List<dynamic>.from(reports.map((x) => x.toJson()));
    data['price'] = price;
    data['qty'] = qty;
    return data;
  }

}