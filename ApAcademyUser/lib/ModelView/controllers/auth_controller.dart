import 'dart:convert';
import 'dart:math';
import 'package:ap_academy_new/ModelView/controllers/profileController.dart';
import 'package:ap_academy_new/Models/UserModel.dart';
import 'package:ap_academy_new/Screens/Authentication/login_screen.dart';
import 'package:ap_academy_new/Screens/Authentication/sign_up_screen.dart';
import 'package:ap_academy_new/Screens/Setting/profile_edit.dart';
import 'package:ap_academy_new/Screens/drawer_screen.dart';
import 'package:ap_academy_new/shared/loading_widget.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import '../../Constants/db_cred.dart';
import 'notification_controller.dart';
import 'db_controller.dart';
import 'package:http/http.dart' as http;

class AuthController extends GetxController {
  final database = Get.put(DatabaseController());
  final notificationController = Get.put(NotificationController());
  final profileController = Get.put(ProfileController());
  // Intilize the flutter app
  FirebaseApp? firebaseApp = null;
  User? firebaseUser=null;
  FirebaseAuth? firebaseAuth=null;

  Future<void> initlizeFirebaseApp() async {
    firebaseApp = await Firebase.initializeApp();
  }
  
  Future<bool> checkisActive() async {
    return await database
        .getisActive(FirebaseAuth.instance.currentUser!.uid);
  }


  // Future getsheetfun() async {
  //   http.Response response = await http.get(
  //     Uri.parse('https://sheetdb.io/api/v1/5xeq0u2whmy1p'),
  //     headers: <String, String>{
  //       'Content-Type': 'application/json; charset=UTF-8',
  //     },
  //   );
  //   if (response.statusCode == 200) {
  //     // If the server did return a 201 CREATED response,
  //     // then parse the JSON.
  //     List<dynamic> sheetData = await jsonDecode(response.body);
  //     print(sheetData[1]);
  //     for(var data in sheetData){
  //       print(data["HKID/PASSPORT"]);
  //       await uploaduser(data["NAME"], data["EMAIL ADDRESS"], "12345678", data["CRICHQ ID"], data["HKID/PASSPORT"], DateTime.parse(data["DOB"]), data["CONTACT NO"], data["PARENT NAME"]);
  //     }
  //
  //   } else {
  //     // If the server did not return a 201 CREATED response,
  //     // then throw an exception.
  //     throw Exception('Failed to get payable data');
  //   }
  // }

  Future<void> uploaduser(
      String name,
      String email,
      String password,
      String crichqId,
      String passport,
      DateTime DOB,
      String phone,
      String parentName,
      ) async {
    firebaseAuth = FirebaseAuth.instance;
    UserModel _user;
    try {
      Get.dialog(Center(child: LoadingWidget()), barrierDismissible: false);
      await firebaseAuth!
          .createUserWithEmailAndPassword(email: email, password: password)
          .then((value) async {
        // await value.user!.updateDisplayName(name);
        _user = UserModel(
            id: value.user!.uid,
            displayName: name,
            crichqid: crichqId,
            passport: passport,
            isActive: true,
            email: email,
            dob: DOB,
            parentName: parentName,
            phone: phone,
            photo: "",
            role: "user",
            rollno: await getRandom(),
            token: "not updated yet",
            approved: true);
        if (!db.isConnected) await database.getMongoPort(true);
        await database.addUser(_user).then((addedValue) async {
          if (addedValue) {
            // U_id = value.user!.uid;
          } else {
            // await deleteuseraccount(email, password);
          }
        });
      }).catchError((onError) {
        print(onError);

      });
      update();
    } catch (err) {
      Get.snackbar("Error", err.toString());
    }
  }
  
  Future<Widget?> checkUserLoggedIn() async {
    try {
      if (firebaseApp == null) {
        await initlizeFirebaseApp();
      }
      if (firebaseAuth == null) {
        firebaseAuth = FirebaseAuth.instance;
        update();
      }
    }catch(err)
    {
      print("err "+err.toString());
    }
    print("41AAAAAAAAAAAAAAAAAAAAAAAAA"+firebaseAuth!.currentUser.toString());

    if (firebaseAuth!.currentUser == null) {
      await fcmListner();
      return LoginPage();
    } else {
      firebaseUser = firebaseAuth!.currentUser!;
      print("48AAAAAAAAAAAAAAAAAAAAAAAAA");
      int isUserState= await profileController.initializeUserData();
      print("51AAAAAAAAAAAAAAAAAAAAAAAAA"+isUserState.toString());

      await checkisActive().then((value) => ()async{
        if(value){
          if (isUserState==1) {
            await fcmListner();
            update();
            return HomeDrawer();
          } else if(isUserState==3) {
            signOut();
            return LoginPage();
          }
        }else{
          return LoginPage();
        }
      });

    }
  }

  ////////////////////////////////////////////////////////////Auth with email////////////////////////
  Future<void> signInwithEmail(String emails, String passs) async {
    try {
      Get.dialog(Center(child: LoadingWidget()), barrierDismissible: false);
      print(emails);
      firebaseAuth = FirebaseAuth.instance;

      final userCredentialData = await firebaseAuth!.signInWithEmailAndPassword(
          email: emails, password: passs);
      firebaseUser = userCredentialData.user!;
      U_id = firebaseUser!.uid;
      update();

      await database.getMongoPort(true).then((mongResult) async {
        if (mongResult!) {
          print("Mongoresult"+mongResult.toString());
          await database
              .checkUserExist(firebaseUser!.uid)
              .then((isuserExist) async {
            print("isuserExist"+isuserExist.toString());

            if (isuserExist) {
              Get.back();
              await profileController.initializeUserData(
                  uid: FirebaseAuth.instance.currentUser!.uid);
              bool value = await checkisActive();
              if(value){
                  await database.updateusertoken(notificationController.userToken.value!,FirebaseAuth.instance.currentUser!.uid);
                  Get.offAll(() => HomeDrawer());
                }else{
                  Get.back();
                  Get.defaultDialog(
                      barrierDismissible: false,
                      content: Container(
                        child: Text(
                            'You can not sign in to this application. please contact admin to reset your email'),
                      ),
                      onConfirm: () async {
                        await signOut();
                      });
                }


            } else if (await database
                    .retrieveStudentwithPId(firebaseUser!.uid) !=
                null) {
              await profileController.initializeUserData(
                  uid: await database.retrieveStudentwithPId(firebaseUser!.uid));
              bool value = await checkisActive();
                if(value){
                  await database
                      .updateparenttoken(notificationController.userToken.value!,await database.retrieveStudentwithPId(FirebaseAuth.instance.currentUser!.uid));
                  Get.back();
                  Get.offAll(() => HomeDrawer());
                }else{
                  Get.back();
                  Get.defaultDialog(
                      barrierDismissible: false,
                      content: Container(
                        child: Text(
                            'You can not sign in to this application. please contact admin to reset your email'),
                      ),
                      onConfirm: () async {
                        await signOut();
                      });
                }


            } else {
              Get.back();
              Get.defaultDialog(
                  barrierDismissible: false,
                  content: Container(
                    child: Text(
                        'You can not sign in to this application. please contact admin to reset your email'),
                  ),
                  onConfirm: () async {
                    await signOut();
                  });
            }
          });
        } else {
          Get.back();
          Get.defaultDialog(
              content: Container(
                child: Text(
                    'Mongo DB is not connected. please connect to internet or contact to admin'),
              ),
              onConfirm: () async {
                await signOut();
              });
        }
      });
    } catch (ex) {
      print(ex.toString());
      Get.back();
      Get.snackbar('Sign In Error', ex.toString(),
          duration: Duration(seconds: 5),
          backgroundColor: Colors.black,
          colorText: Colors.white,
          snackPosition: SnackPosition.TOP,
          icon: Icon(
            Icons.error,
            color: Colors.red,
          ));
    }
  }
  // Future<void> signInwithEmailasParent(String emails, String passs) async {
  //   try {
  //     Get.dialog(Center(child: LoadingWidget()), barrierDismissible: false);
  //     print(emails);
  //     firebaseAuth = FirebaseAuth.instance;
  //
  //     final userCredentialData = await firebaseAuth!.signInWithEmailAndPassword(
  //         email: emails, password: passs);
  //     firebaseUser = userCredentialData.user!;
  //     U_id = firebaseUser!.uid;
  //     update();
  //
  //     await database.getMongoPort(true).then((mongResult) async {
  //       if (mongResult!) {
  //         print("Mongoresult"+mongResult.toString());
  //         await database
  //             .checkUserExist(firebaseUser!.uid)
  //             .then((isuserExist) async {
  //           print("isuserExist"+isuserExist.toString());
  //
  //           if (isuserExist) {
  //             Get.back();
  //             await profileController.initializeUserData(
  //                 uid: FirebaseAuth.instance.currentUser!.uid);
  //             bool value = await checkisActive();
  //             if(value){
  //                 await database.updateusertoken(notificationController.userToken.value!,FirebaseAuth.instance.currentUser!.uid);
  //                 await database.updateparenttoken(notificationController.userToken.value!,FirebaseAuth.instance.currentUser!.uid);
  //                 Get.offAll(() => HomeDrawer());
  //               }else{
  //                 Get.back();
  //                 Get.defaultDialog(
  //                     barrierDismissible: false,
  //                     content: Container(
  //                       child: Text(
  //                           'You can not sign in to this application. please contact admin to reset your email'),
  //                     ),
  //                     onConfirm: () async {
  //                       await signOut();
  //                     });
  //               }
  //
  //
  //           } else if (await database
  //                   .retrieveStudentwithPId(firebaseUser!.uid) !=
  //               null) {
  //             await profileController.initializeUserData(
  //                 uid: await database.retrieveStudentwithPId(firebaseUser!.uid));
  //             bool value = await checkisActive();
  //               if(value){
  //                 await database
  //                     .updateusertoken(notificationController.userToken.value!,FirebaseAuth.instance.currentUser!.uid);
  //                 Get.back();
  //                 Get.offAll(() => HomeDrawer());
  //               }else{
  //                 Get.back();
  //                 Get.defaultDialog(
  //                     barrierDismissible: false,
  //                     content: Container(
  //                       child: Text(
  //                           'You can not sign in to this application. please contact admin to reset your email'),
  //                     ),
  //                     onConfirm: () async {
  //                       await signOut();
  //                     });
  //               }
  //
  //
  //           } else {
  //             Get.back();
  //             Get.defaultDialog(
  //                 barrierDismissible: false,
  //                 content: Container(
  //                   child: Text(
  //                       'You can not sign in to this application. please contact admin to reset your email'),
  //                 ),
  //                 onConfirm: () async {
  //                   await signOut();
  //                 });
  //           }
  //         });
  //       } else {
  //         Get.back();
  //         Get.defaultDialog(
  //             content: Container(
  //               child: Text(
  //                   'Mongo DB is not connected. please connect to internet or contact to admin'),
  //             ),
  //             onConfirm: () async {
  //               await signOut();
  //             });
  //       }
  //     });
  //   } catch (ex) {
  //     print(ex.toString());
  //     Get.back();
  //     Get.snackbar('Sign In Error', ex.toString(),
  //         duration: Duration(seconds: 5),
  //         backgroundColor: Colors.black,
  //         colorText: Colors.white,
  //         snackPosition: SnackPosition.TOP,
  //         icon: Icon(
  //           Icons.error,
  //           color: Colors.red,
  //         ));
  //   }
  // }

  Future<String> getRandom() async {
    Random rnd2 = Random(DateTime.now().millisecondsSinceEpoch);

    int min = 100000, max = 999999;
    int rollNumber = min + rnd2.nextInt(max - min);
    if (await database.checkRollExist(rollNumber)) {
      return await getRandom();

    } else {
      return rollNumber.toString();
    }
  }

  Future<void> signInWithGoogle() async {
    try {
      Get.dialog(Center(child: LoadingWidget()), barrierDismissible: false);

      GoogleSignIn _googleSignIn = GoogleSignIn(
        scopes: <String>[
          'email',
          //'https://www.googleapis.com/auth/contacts.readonly',
        ],
      );
      final googleUser = await _googleSignIn.signIn();

      final googleAuth = await googleUser!.authentication;

      final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );

      await FirebaseAuth.instance
          .signInWithCredential(credential)
          .then((value) async {
        firebaseUser = value.user!;

        if (!db.isConnected) await database.getMongoPort(true);
        if (await database.checkdublicate(firebaseUser!.uid) == false) {
          final _user = UserModel(
              id: firebaseUser!.uid,
              isActive: true,
              displayName: firebaseUser!.displayName!,
              email: firebaseUser!.email!,
              phone: '',
              rollno: await getRandom(),
              photo: firebaseUser!.photoURL,
              role: 'user',
              token: notificationController.userToken.value,
              approved: false);
          await database.addUser(_user).then((addedValue) async {
            if (addedValue) {
              final Map<String, dynamic> uDetail = {
                'name': firebaseUser!.displayName,
                'email': firebaseUser!.email,
                'phone': '',
                'rollno': _user.rollno,
                'photoUrl': firebaseUser!.photoURL
              };

              Get.offAll(ProfileEdit(
                usrDetails: uDetail, isapproved: false,
              ));
              U_id = value.user!.uid;
            } else {
              Get.back();
              Get.snackbar(
                  "Error", "Account is not created, Please try again later.");
              //  await deleteuseraccount(email, password);
            }
          });
        } else {
          await database
              .checkUserExist(firebaseUser!.uid)
              .then((isuserExist) async {
            if (isuserExist) {
              await profileController
                  .initializeUserData(
                      uid: FirebaseAuth.instance.currentUser!.uid)
                  .then((isApproved) {
                if (isApproved==1) Get.back();
                Get.offAll(() => HomeDrawer());
              });
            } else  {
              Get.back();
              Get.defaultDialog(
                  barrierDismissible: false,
                  content: Container(
                    child: Text(
                        'You can not sign in to this application. please contact admin to reset you email'),
                  ),
                  onConfirm: () async {
                    await signOut();
                  });
            }
          });

          // update();
          // Get.back();
          // Get.to(HomeDrawer());
        }
      });
    } catch (e) {
      Get.back();
      print(e.toString());
      Get.snackbar('Sign In Error', e.toString(),
          duration: Duration(seconds: 5),
          backgroundColor: Colors.black,
          colorText: Colors.white,
          snackPosition: SnackPosition.TOP,
          icon: Icon(
            Icons.error,
            color: Colors.red,
          ));
    }
  }

  Future<void> sendpasswordresetemail1(String email) async {
    firebaseAuth = FirebaseAuth.instance;
    try {
      await firebaseAuth!.sendPasswordResetEmail(email: email).then((value) {
        Get.offAll(LoginPage());
        Get.snackbar(
            "Password Reset email link has been sent to $email", "Success");
      }).catchError((onError) =>
          Get.snackbar("Error In Email Reset", onError.message.toString()));
    } catch (e) {
      print("Error is: " + e.toString());
    }
  }
//////////////////////check approved

  // ///////////////////////////////////////////function to createuser, login and sign out user

  Future<void> createUser(
    String name,
    String email,
    String password,
    String crichqId,
    String passport,
  ) async {
    firebaseAuth = FirebaseAuth.instance;
    UserModel _user;
    try {
      Get.dialog(Center(child: LoadingWidget()), barrierDismissible: false);
      print('277 TTTTTTTTTTTT');
      await firebaseAuth!
          .createUserWithEmailAndPassword(email: email, password: password)
          .then((value) async {
        print('280 TTTTTTTTTTTT');
        await value.user!.updateDisplayName(name);
        print('282 TTTTTTTTTTTT');
        _user = UserModel(
            id: value.user!.uid,
            displayName: name,
            crichqid: crichqId,
            passport: passport,
            isActive: true,
            email: email,
            phone: "",
            photo: "",
            role: "user",
            rollno: await getRandom(),
            token: notificationController.userToken.value,
            approved: false);
        print('293 TTTTTTTTTTTT');
        if (!db.isConnected) await database.getMongoPort(true);
        await database.addUser(_user).then((addedValue) async {
          print(addedValue.toString()+"293");
          if (addedValue) {
            Map<String, dynamic> uDetail = {
              'name': name,
              'email': email,
              'phone': '',
              'photoUrl': ''
            };
            Get.offAll(ProfileEdit(
              isapproved: false,
              usrDetails: uDetail,
            ));
            U_id = value.user!.uid;
          } else {
            Get.back();
            Get.snackbar(
                "Error", "Account is not created, Please try again later.");
            print('307 TTTTTTTTTTTT');
            await deleteuseraccount(email, password);
          }
        });
      }).catchError((onError) {
        Get.back();
        Get.snackbar("Error", onError.message.toString());
        print('316 TTTTTTTTTTTT');

      });
      update();
    } catch (err) {
      Get.back();
      Get.snackbar("Error", err.toString());
      print('322 TTTTTTTTTTTT');
    }
  }

/////////////////////////////////////////update after edit profile
  Future<void> createParent(
      String name, String email, String password, String rollNo) async {
    firebaseAuth = FirebaseAuth.instance;
    try {
      Get.dialog(Center(child: LoadingWidget()), barrierDismissible: false);
      if (!db.isConnected) await database.getMongoPort(true);
      final stId = await database.retrieveStudentData(rollNo);
      if (stId != null) {
        await firebaseAuth!
            .createUserWithEmailAndPassword(email: email, password: password)
            .then((value) async {
          await database
              .updateParentIdUser(value.user!.uid.toString(), stId)
              .then((addedValue) async {
            if (addedValue) {
              database.updateparenttoken(notificationController.userToken.value??"", stId);
              Get.back();
              await profileController.initializeUserData(uid: value.user!.uid.toString());
              Get.offAll(HomeDrawer());
            } else {
              Get.back();
              Get.snackbar(
                  "Error", "Account is not created, Please try again later.");
              await deleteuseraccount(email, password);
            }
          });
        }).catchError((onError) {
          Get.back();
          Get.snackbar("Error", onError.message.toString());
        });
        update();
      } else {
        Get.back();
        Get.snackbar("Error",
            "Roll no. does-not Exist, Please try again later with correct roll number",
            snackPosition: SnackPosition.BOTTOM);
      }
    } catch (err) {
      Get.back();
      Get.snackbar("Error", err.toString());
    }
  }



  Future<void> linkParent(String rollNo) async {
    firebaseAuth = FirebaseAuth.instance;
    try {
      Get.dialog(Center(child: LoadingWidget()), barrierDismissible: false);
      if (!db.isConnected) await database.getMongoPort(true);
      final stId = await database.retrieveStudentData(rollNo);
      if (stId != null) {
        await database
              .updateParentIdUser(firebaseAuth!.currentUser!.uid.toString(), stId)
              .then((addedValue) async {
            if (addedValue) {
              database.updateparenttoken(notificationController.userToken.value??"", stId);
              Get.back();
              await profileController.initializeUserData(uid: firebaseAuth!.currentUser!.uid);
              Get.offAll(HomeDrawer());
            } else {
              Get.back();
              Get.snackbar(
                  "Error", "Account is not linked, Please try again later.");
            }
          });

        update();
      } else {
        Get.back();
        Get.snackbar("Error",
            "Roll no. does-not Exist, Please try again later with correct roll number",
            snackPosition: SnackPosition.BOTTOM);
      }
    } catch (err) {
      Get.back();
      Get.snackbar("Error", err.toString());
    }
  }

//////////////////////////////////////////
  Future<void> updateUser(String name, String email, String password,String crichq,String passport,) async {
    firebaseAuth = FirebaseAuth.instance;
    UserModel _user;
    try {
      await firebaseAuth!
          .createUserWithEmailAndPassword(email: email, password: password)
          .then((value) async {
        _user = UserModel(
          id: value.user!.uid,
          crichqid: crichq,
          passport: passport,
          displayName: name,
          email: email,
          phone: "",
        );
        await database.addUser(_user);
        U_id = value.user!.uid;
      }).catchError(
        (onError) => Get.snackbar(
            "Error while creating account ", onError.message.toString()),
      );
      update();
    } catch (err) {
      Get.back();
      Get.snackbar("Error while creating account ", err.toString());
    }
  }

///////////////////////////////////////////////////////// delete Account////////////////////////////
  Future deleteuseraccount(String email, String pass) async {
    User? user = firebaseAuth!.currentUser;

    AuthCredential credential =
        EmailAuthProvider.credential(email: email, password: pass);

    await user!.reauthenticateWithCredential(credential).then((value) {
      value.user!.delete().then((res) {
        Get.offAll(LoginPage());
        Get.snackbar("User Account Deleted ", "Success");
      });
    }).catchError((onError) =>
        Get.snackbar("Credential Error", onError.message.toString()));
  }

  Future<void> signOut() async {
    Get.back();
    Get.dialog(Center(child: LoadingWidget()), barrierDismissible: false);

    await firebaseAuth!.signOut();
    update();

    // Navigate to Login again
    Get.offAll(() => LoginPage());
  }
}
