import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:ionicons/ionicons.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Screens/User%20Forms/confirm_profile_page.dart';

class SignupFormFour extends StatefulWidget {
  const SignupFormFour({Key? key}) : super(key: key);

  @override
  State<SignupFormFour> createState() => _SignupFormFourState();
}

class _SignupFormFourState extends State<SignupFormFour> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: myResources.background,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: myResources.background,
        title: Text(
          "Set up Your Profile",
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              children: [
                Container(
                  height: 200,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(3),
                      border: Border.all(color: Colors.black26)),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 20.0, bottom: 20),
                        child: Icon(
                          Icons.add_photo_alternate_outlined,
                          size: 50,
                          color: myResources.themeColor,
                        ),
                      ),
                      Text(
                        "Upload a Profile picture",
                        style:
                        TextStyle(color: myResources.themeColor, fontSize: 16),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 30.0, vertical: 20),
                        child: Text(
                          """Add a Picture with your face completely visible.
avoid adding mirror selfies and group pictures.""",
                          style: TextStyle(color: Colors.black45, fontSize: 13),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 25),
                Text(
                  "Add a bio",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                Padding(
                  //Add padding around textfield
                  padding: const EdgeInsets.only(top: 20.0, bottom: 30),
                  child: SizedBox(
                    height: 100,
                    child: TextField(
                      maxLines: 10,
                      keyboardType: TextInputType.name,
                      onChanged: (val) {},
                      decoration: InputDecoration(
                        //Add th Hint text here.
                        hintText: "Tell us about yourself ...",
                        hintStyle: TextStyle(fontSize: 13),
                        contentPadding: const EdgeInsets.only(top: 15, left: 15,),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(3.0),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: myResources.themeColor,
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(14))),
              ),
              onPressed: () {
                Get.to(()=> const ConfirmProfile());
              },
              child: Container(
                padding: const EdgeInsets.symmetric(
                    vertical: 8, horizontal: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Confirm',
                      style: TextStyle(color: Colors.white),
                    ),
                    Container(
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(20)),
                        color: myResources.themeColor,
                      ),
                      child: Icon(
                        Icons.arrow_forward_ios,
                        color: Colors.white,
                        size: 16,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

