import 'package:cached_network_image/cached_network_image.dart';
import 'package:ap_academy_admin/ModelView/Controller/SponsorController.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:ap_academy_admin/screens/shared/DeletPop.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'AddSponsor.dart';
import 'SponsorDetails.dart';

class SponsorList extends StatefulWidget {
  @override
  _SponsorListState createState() => _SponsorListState();
}

class _SponsorListState extends State<SponsorList> {
  final spC = Get.put(SponsorController());
  @override
  void initState() {
    spC.refreshSponsor();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      //Create an event add button
      child: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "",
                    style: MyResources.appHeadingStyle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    width: Get.width * 0.2,
                    height: 25,
                    child: RaisedButton(
                      textColor: Colors.white,
                      color: MyResources.buttonColor,
                      child: Text(
                        "ADD",
                      ),
                      onPressed: () async {
                        await Get.to(
                          () => AddSponsor(),
                        );
                        spC.refreshSponsor();
                      },
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Container(
              height: Get.height,
              child: GetX<SponsorController>(
                init: Get.put<SponsorController>(SponsorController()),
                builder: (SponsorController sponsorController) {
                  if (sponsorController != null &&
                      sponsorController.sponsors != null) {
                    print(sponsorController.sponsors.toString());
                    return Container(
                      height: Get.height,
                      child: GridView.count(
                        keyboardDismissBehavior:
                            ScrollViewKeyboardDismissBehavior.onDrag,
                        crossAxisCount: 2,
                        children: List.generate(
                          sponsorController.sponsors.length,
                          (index) {
                            return getsponsorCard(
                                sponsorController.sponsors[index]);
                          },
                        ),
                      ),
                    );
                  } else {
                    return Container(
                      child: LoadingWidget(),
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  getsponsorCard(Map<String, dynamic> sponsorModel) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child:Card(
        child: InkWell(
          onTap: () {
            Get.dialog(SponsorDetailCard(
              sponsorDetailData: sponsorModel,
            ));
          },
        child: Container(
          //color: Colors.black12,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(top: 8),
                height:125,
                width:MediaQuery.of(context).size.width*0.4,
                child: ClipRRect(
                  // borderRadius: BorderRadius.circular(15),
                  borderRadius: BorderRadius.circular(3),
                  child: sponsorModel['imageUrl'] != ''
                      ? CachedNetworkImage(
                    height: 80,
                    fit: BoxFit.cover,
                    width: 80,
                    imageUrl: sponsorModel['imageUrl'],
                    placeholder: (context, url) =>
                        LoadingWidget(),
                    errorWidget:
                        (context, url, error) =>
                        Icon(Icons.error),
                  )
                      : Center(
                          child: Icon(
                          Icons.image_rounded,
                          size: 100,
                        )),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(width: 120,
                child: Text(
                  sponsorModel['name'].toString().capitalize!,overflow: TextOverflow.ellipsis,textAlign: TextAlign.center,
                  style: MyResources.titleTextStyle,
                ),
              ),
            ],
          ),
        ),
    ),
      ),
    );
  }
}
