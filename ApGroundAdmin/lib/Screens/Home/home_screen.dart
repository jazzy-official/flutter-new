import 'package:ap_ground_admin/Screens/Attendance/Models/attendance_model.dart';
import 'package:ap_ground_admin/Screens/Attendance/Models/entry_model.dart';
import 'package:ap_ground_admin/Screens/Attendance/attendance_screen.dart';
import 'package:ap_ground_admin/Screens/My%20booking/Models/booking_model.dart';
import 'package:ap_ground_admin/Screens/Weather/models/weather.dart';
import 'package:ap_ground_admin/Screens/Weather/weather_screen.dart';
import 'package:ap_ground_admin/Screens/drawer.dart';
import 'package:ap_ground_admin/constants/util.dart';
import 'package:ap_ground_admin/controllers/attendance_controller.dart';
import 'package:ap_ground_admin/controllers/booking_controller.dart';
import 'package:ap_ground_admin/controllers/weather_controller.dart';
import 'package:ap_ground_admin/loading_widget.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late Weather weather;
  List<Booking> todayBooking = [];
  int totalbookings = 0;
  int fullbooked = 0;
  int present = 0;
  int absent = 0;
  bool isAtt = false;
  bool isLoading = false;
  void refresh() async {
    setState(() {
      isLoading=true;
    });
    List<Booking> temp =
        await BookingController().readBookingDate(DateTime.now());
    setState(() {
      todayBooking = temp;
      totalbookings = todayBooking.length;
    });
    List<Booking> fullpaid = todayBooking
        .where((element) =>
            element.slot.Pslot2 != null &&
            element.slot.Pslot1!.total+element.slot.Pslot2!.total==
                element.slot.Pslot1!.paid + element.slot.Pslot2!.paid)
        .toList();
    weather = await WeatherController().getCurrentWeather(location.city);
    setState(() {
      fullbooked = fullpaid.length;
      isLoading = false;
    });
    checkattendance();
  }

  void checkattendance() async {
    Attendance todayatt =
        await AttendanceController().readAttendancebyDate(DateTime.now());
    if (todayatt == null) {
    } else {
      List<Entry> presentlist =
          todayatt.entries.where((element) => element.isPresent).toList();
      List<Entry> absentlist =
          todayatt.entries.where((element) => !element.isPresent).toList();
      setState(() {
        present = presentlist.length;
        absent = absentlist.length;
        isAtt = true;
      });
    }
  }

  @override
  void initState() {
    refresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
          children: [
            isLoading? Container(height:300,child: LoadingWidget()) :
            GestureDetector(
              onTap: (){
                  setState(() {
                    title = "Weather";
                    selectedWidget = CurrentWeather(context);
                  });
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => NavBar()),
                  );
                  },
              child: Column(
                children: [
                  createAppBar(context, location),
                  weatherBox(weather),
                  weatherDetailsBox(weather),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 12),
              child: Card(
                elevation: 4,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: (Column(
                    children: [
                      const Text(
                        "Today's Bookings",
                        style: TextStyle(fontSize: 20),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: const [
                          Expanded(child: Center(child: Text("Total"))),
                          Expanded(child: Center(child: Text("Full Paid"))),
                          Expanded(child: Center(child: Text("Partial Paid"))),
                        ],
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                              child: Center(
                                  child: Text(
                                    totalbookings.toString(),
                                    style: TextStyle(
                                        color: MyResources.buttonColor,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20),
                                  ))),
                          Expanded(
                              child: Center(
                                  child: Text(
                                    fullbooked.toString(),
                                    style: TextStyle(
                                        color: Colors.green,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20),
                                  ))),
                          Expanded(
                              child: Center(
                                  child: Text(
                                    (totalbookings - fullbooked).toString(),
                                    style: TextStyle(
                                        color: Colors.red,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20),
                                  ))),
                        ],
                      ),
                    ],
                  )),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 12),
              child: Card(
                elevation: 4,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: (Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children:  [
                          const Text(
                            "Today's Attendance",
                            style: TextStyle(fontSize: 20),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 12.0),
                            child: ElevatedButton(
                              onPressed: () {
                                setState(() {
                                  selectedWidget = AttendanceScreen();
                                  title = "Attendance";
                                });
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => NavBar()),
                                ).then((value) => refresh());
                              },
                              child: const Text("Attendance"),
                              style: ElevatedButton.styleFrom(primary: Color(0xff1758B4)),
                            ),
                          ),
                        ],
                      ),
                      if (isAtt)
                        const SizedBox(
                          height: 10,
                        ),
                      if (isAtt)
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: const [
                            Expanded(child: Center(child: Text("Present Staff"))),
                            Expanded(child: Center(child: Text("Absent Staff"))),
                          ],
                        ),
                      if (isAtt)
                        const SizedBox(
                          height: 5,
                        ),
                      isAtt
                          ? Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                              child: Center(
                                  child: Text(
                                    present.toString(),
                                    style: TextStyle(
                                        color: Colors.green,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20),
                                  ))),
                          Expanded(
                              child: Center(
                                  child: Text(
                                    absent.toString(),
                                    style: TextStyle(
                                        color: Colors.red,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20),
                                  ))),
                        ],
                      )
                          : Container(
                        width: double.infinity,
                        padding: const EdgeInsets.symmetric(vertical: 30.0),
                        child: Column(
                          children: [
                            Text("Attendance isn't mark yet",style: TextStyle(color: Colors.red),),
                            SizedBox(height: 5,),
                            Text("Please go to attendance Screen to mark the Attendance",style: TextStyle(fontSize: 10),)
                          ],
                        ),
                      )
                    ],
                  )),
                ),
              ),
            ),
          ],
        ),
    );
  }
}
