import 'package:ap_ground_admin/constants/util.dart';
import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';

class ForgotPasswordScreen extends StatefulWidget {
  const ForgotPasswordScreen({Key? key}) : super(key: key);

  @override
  State<ForgotPasswordScreen> createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  late String emaiil ;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        leading: BackButton(
            color: Colors.black
        ),
        backgroundColor: Colors.white,
        title: Text("FORGOT PASSWORD"),
        titleTextStyle: TextStyle(color: Colors.black,fontSize: 18,fontWeight: FontWeight.w600),
        centerTitle: true,
      ),
      body: Column(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.4,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Image.asset(
                    "assets/logo.png",
                    height: MediaQuery.of(context).size.height*0.35,
                  ),
                  Text(
                    "It's all about Cricket",
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.grey,
                        fontStyle: FontStyle.italic),
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Expanded(
              child: Column(
                children: [
                   Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                    child: TextField(
                      onChanged: (val)  {
                        setState(() {
                          emaiil = val;
                        });
                        print(emaiil);
                      },
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'Email',
                          contentPadding: EdgeInsets.symmetric(horizontal: 10)),
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: TextButton(
                          onPressed: () async {
                              await resetPassword(emaiil,context);
                          },
                          child: Container(
                            height: 50,
                            width: MediaQuery.of(context).size.width - 160,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25),
                              color: MyResources.buttonColor,
                            ),
                            child: Center(
                                child: Text(
                                  "Submit",
                                  style: TextStyle(color: Colors.white, fontSize: 17),
                                )),
                          )),
                    ),
                  ),
                ],
              )),
          Container(
            height: 45,
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide(
                  //                    <--- top side
                  color: Colors.grey,
                  width: 1.0,
                ),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Remember?",
                  style: TextStyle(color: Colors.grey),
                ),
                TextButton(onPressed: () {}, child: Text("Sign in"))
              ],
            ),
          ),
        ],
      ),
    );
  }
}
