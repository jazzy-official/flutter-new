class LocationModel {
  final String city;
  final String country;
  final String lat;
  final String lon;

  LocationModel({required this.city, required this.country, required this.lat, required this.lon});


}