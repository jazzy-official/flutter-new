
class Commit {
  final String des;
  final String task;
  final String taskid;




  Commit({required this.des,required this.task,required this.taskid,});



  factory Commit.fromJson(dynamic json, key) {
    return Commit(
      task: json['task'],
      taskid: json['taskid'],
      des : json['description'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['description'] = des;
    data['task'] = task;
    data['taskid'] = taskid;

    return data;
  }
}