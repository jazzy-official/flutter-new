// import 'package:apacademy_user/ModelView/controllers/dashBoardController.dart';
// import 'package:apacademy_user/ModelView/controllers/dbController.dart';
// import 'package:apacademy_user/ModelView/controllers/matchController.dart';
// import 'package:apacademy_user/ModelView/controllers/otherEventModel.dart';
// import 'package:apacademy_user/ModelView/controllers/trainingController.dart';
// import 'package:apacademy_user/screens/Training/training_screen.dart';
// import 'package:apacademy_user/shared/loading_widget.dart';
// import 'package:flutter/material.dart';
// import 'package:apacademy_user/Constants/util.dart';
// import 'package:get/get.dart';
// import 'package:google_fonts/google_fonts.dart';
// import 'package:intl/intl.dart';

// import 'drawer_sreen.dart';

// class DashBoard extends StatefulWidget {
//   //teams under card
//   @override
//   _DashBoardState createState() => _DashBoardState();
// }

// class _DashBoardState extends State<DashBoard> {
//   final dashBoardController = Get.put(DashBoardController());
//   Future getmatchNames(Map<String, dynamic> matchModel1) async {
//     await dbHelper.getTeamName(matchModel1);
//     // if (this.mounted) {
//     //   setState(() {
//     //     // Your state change code goes here
//     //   });
//     // }
//   }

//   refreshAllDashBoard() {
//     final mCon = Get.put(MatchController());
//     final oCon = Get.put(OtherEventController());
//     final tCon = Get.put(TrainingController());
//     mCon.refreshDate();
//     oCon.refreshData();
//     tCon.refrehData();
//   }

//   Widget matchCard(Map<String, dynamic> matchModel) {
//     //dbHelper.getTeamName(matchModel);
//     return Padding(
//       padding: const EdgeInsets.all(4.0),
//       child: Container(
//           height: 80,
//           color: myResources.primaryColor,
//           child: Padding(
//             padding: const EdgeInsets.all(8.0),
//             child: Column(
//               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: [
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     SizedBox(
//                       width: 20,
//                     ),
//                     FutureBuilder(
//                         future: dashBoardController.getTeamName1(matchModel),
//                         builder:
//                             (BuildContext context, AsyncSnapshot<String> sp) {
//                           if (sp.data != null)
//                             return Container(
//                                 width: 100,
//                                 child: Text(sp.data,
//                                     style: TextStyle(
//                                       color: Color(0xff000074),
//                                     )));
//                           else
//                             return Container(
//                               width: 100,
//                               child: Text(
//                                 'Team 1',
//                                 style: TextStyle(
//                                   color: Color(0xff000074),
//                                 ),
//                               ),
//                             );
//                         }),
//                     SizedBox(
//                       width: 10,
//                     ),
//                     Container(
//                       width: 50,
//                       child: Text(
//                         "vs",
//                         style: myResources.vsStyle,
//                       ),
//                     ),
//                     SizedBox(
//                       width: 10,
//                     ),
//                     FutureBuilder(
//                         future: dashBoardController.getTeamName2(matchModel),
//                         builder:
//                             (BuildContext context, AsyncSnapshot<String> sp) {
//                           if (sp.data != null)
//                             return Container(
//                                 width: 100,
//                                 child: Text(sp.data,
//                                     style: TextStyle(
//                                       color: Color(0xff000074),
//                                     )));
//                           else
//                             return Container(
//                               width: 100,
//                               child: Text(
//                                 'Team 1',
//                                 style: TextStyle(
//                                   color: Color(0xff000074),
//                                 ),
//                               ),
//                             );
//                         }),
//                   ],
//                 ),
//                 SizedBox(
//                   height: 20,
//                 ),
//                 Text(
//                   DateFormat.yMEd().add_Hm().format(matchModel["matchTime"]),
//                   style:
//                       TextStyle(fontSize: 10, color: myResources.headingColor),
//                 ),
//               ],
//             ),
//           )),
//     );
//   }

//   Widget eventCard(Map<String, dynamic> matchModel) {
//     return Padding(
//       padding: const EdgeInsets.only(left: 10.0, top: 10, right: 10),
//       child: Container(
//           height: 40,
//           color: myResources.primaryColor,
//           child: Padding(
//             padding: const EdgeInsets.only(right: 30.0, left: 30),
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: [
//                 Container(
//                   width: 100,
//                   child: Container(
//                     width: 100,
//                     child: Text(matchModel['eventName'].toString(),
//                         overflow: TextOverflow.ellipsis,
//                         style:
//                             TextStyle(color: Color(0xff000074), fontSize: 16)),
//                   ),
//                 ),
//                 SizedBox(
//                   width: 10,
//                 ),
//                 SizedBox(
//                   width: 10,
//                 ),
//                 Row(
//                   children: [
//                     Container(
//                       width: 100,
//                       child: Text(
//                         DateFormat.yMEd().add_Hm().format(matchModel["time"]),
//                         style: TextStyle(fontSize: 12),
//                       ),
//                     ),
//                   ],
//                 ),
//               ],
//             ),
//           )),
//     );
//   }

//   final dbHelper = Get.put(DatabaseController());
//   @override
//   void initState() {
//     refreshAllDashBoard();
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: myResources.primaryColor,
//       appBar: AppBar(
//         toolbarHeight: 0,
//         title: Text(
//           'DashBoard',
//           style: GoogleFonts.roboto(),
//         ),
//         centerTitle: true,
//       ),
//       body: SingleChildScrollView(
//         child: Column(
//           children: [
//             Padding(
//               padding: const EdgeInsets.only(
//                 top: 15.0,
//               ),
//               child: Center(
//                 child: Text(
//                   "Events",
//                   style: myResources.appHeadingStyle,
//                 ),
//               ),
//             ),

//             //create 2 cards for matches and others and click on teams the details of event appears.
//             Padding(
//               padding: const EdgeInsets.only(top: 10.0, left: 10, right: 10),
//               child: Container(
//                 height: 250,
//                 child: Center(
//                   child: InkWell(
//                     onTap: () {
//                       /*Navigator.pushReplacement(
//                           context,
//                           MaterialPageRoute(
//                               builder: (context) => EventScreen()));*/
//                     },
//                     child: Card(
//                       color: myResources.backgroundColor,
//                       child: Column(
//                         children: [
//                           Row(
//                               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                               crossAxisAlignment: CrossAxisAlignment.start,
//                               children: [
//                                 Padding(
//                                   padding:
//                                       const EdgeInsets.only(top: 10, left: 10),
//                                   child: Text(
//                                     "Matches:",
//                                     style: myResources.appTextStyle,
//                                   ),
//                                 ),
//                               ]),
//                           Expanded(
//                             flex: 1,
//                             child: GetX<MatchController>(
//                               init: Get.put<MatchController>(MatchController()),
//                               builder: (MatchController matchController) {
//                                 if (matchController != null &&
//                                     matchController.match != null) {
//                                   // print(matchController.match.toString());
//                                   return ListView.builder(
//                                     itemCount: matchController.match.length,
//                                     itemBuilder: (BuildContext context,
//                                             int index) =>
//                                         matchCard(matchController.match[index]),
//                                   );
//                                 } else {
//                                   return Container(
//                                     child: LoadingWidget(),
//                                   );
//                                 }
//                               },
//                             ),
//                           ),
//                         ],
//                       ),
//                     ),
//                   ),
//                 ),
//               ),
//             ),
//             Padding(
//               padding: const EdgeInsets.only(top: 10.0, left: 10, right: 10),
//               child: Container(
//                 height: 150,
//                 child: Center(
//                   child: InkWell(
//                     onTap: () {
//                       /* Navigator.pushReplacement(
//                             context,
//                             MaterialPageRoute(
//                                 builder: (context) => EventScreen()));*/
//                     },
//                     child: Card(
//                       color: myResources.backgroundColor,
//                       child: Column(
//                         children: [
//                           Row(
//                               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                               crossAxisAlignment: CrossAxisAlignment.start,
//                               children: [
//                                 Padding(
//                                   padding:
//                                       const EdgeInsets.only(top: 10, left: 10),
//                                   child: Text(
//                                     " Others:",
//                                     style: myResources.appTextStyle,
//                                   ),
//                                 ),
//                               ]),
//                           GetX<OtherEventController>(
//                             init: Get.put<OtherEventController>(
//                                 OtherEventController()),
//                             builder:
//                                 (OtherEventController otherEventController) {
//                               if (otherEventController != null &&
//                                   otherEventController.event != null) {
//                                 //  print(otherEventController.event.toString());
//                                 return Expanded(
//                                   flex: 3,
//                                   child: ListView.builder(
//                                     itemCount:
//                                         otherEventController.event.length,
//                                     itemBuilder: (BuildContext context,
//                                             int index) =>
//                                         eventCard(
//                                             otherEventController.event[index]),
//                                   ),
//                                 );
//                               } else {
//                                 return Container(
//                                   child: LoadingWidget(),
//                                 );
//                               }
//                             },
//                           ),
//                         ],
//                       ),
//                     ),
//                   ),
//                 ),
//               ),
//             ),

//             //create 2 cards for trainings and click on teams the details of event appears.
//             Padding(
//               padding: const EdgeInsets.only(top: 15.0, right: 10, left: 10),
//               child: Center(
//                 child: Text(
//                   " Trainings",
//                   style: myResources.appHeadingStyle,
//                 ),
//               ),
//             ),

//             Padding(
//               padding: const EdgeInsets.only(top: 0, left: 10, right: 10),
//               child: Container(
//                 height: 200,
//                 child: InkWell(
//                   onTap: () {
//                     sideTitle = 'Trainings'.obs;
//                     selectedWidget = TrainingScreen().obs;
//                     Get.offAll(() => HomeDrawer());

//                     //  Get.to(() => TrainingScreen());
//                   },
//                   child: Card(
//                     color: myResources.backgroundColor,
//                     child: Column(
//                       children: [
//                         Padding(
//                           padding: const EdgeInsets.only(
//                             top: 10,
//                             left: 10,
//                             right: 10,
//                           ),
//                           child: Row(
//                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                             children: [
//                               Container(
//                                 width: 150,
//                                 child: Text(
//                                   " Session Name",
//                                   style: myResources.appTextStyle,
//                                 ),
//                               ),
//                               Container(
//                                 width: 100,
//                                 child: Text(
//                                   " Time",
//                                   style: myResources.appTextStyle,
//                                 ),
//                               ),
//                             ],
//                           ),
//                         ),
//                         GetX<TrainingController>(
//                           init:
//                               Get.put<TrainingController>(TrainingController()),
//                           builder: (TrainingController trainingController) {
//                             if (trainingController != null &&
//                                 trainingController.training != null) {
//                               return Expanded(
//                                 flex: 3,
//                                 child: ListView.builder(
//                                   itemCount: trainingController.training.length,
//                                   itemBuilder: (BuildContext context,
//                                           int index) =>
//                                       getTraining(
//                                           trainingController.training[index]),
//                                 ),
//                               );
//                             } else {
//                               return Container(
//                                 child: LoadingWidget(),
//                               );
//                             }
//                           },
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }

//   getTraining(Map<String, dynamic> trainingModel) {
//     return Padding(
//       padding: const EdgeInsets.all(8.0),
//       child: Container(
//           height: 60,
//           color: myResources.primaryColor,
//           child: Row(
//             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//             children: [
//               Container(
//                 width: 100,
//                 child: Text(trainingModel["session"],
//                     overflow: TextOverflow.ellipsis,
//                     softWrap: true,
//                     style: TextStyle(color: Color(0xff000074), fontSize: 15)),
//               ),
//               SizedBox(
//                 width: 10,
//               ),
//               Padding(
//                 padding: const EdgeInsets.all(8.0),
//                 child: Text(
//                     DateFormat.yMEd().add_Hm().format(trainingModel["time"]),
//                     style: TextStyle()),
//               ),
//             ],
//           )),
//     );
//   }
// }
