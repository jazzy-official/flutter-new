import 'package:daily_log/Controllers/daily_log_controller.dart';
import 'package:daily_log/Controllers/employeedetailcontroller.dart';
import 'package:daily_log/Models/commit.dart';
import 'package:daily_log/constants/util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:intl/intl.dart';

class EmployeeWeek extends StatefulWidget {
  const EmployeeWeek({Key? key,}) : super(key: key);

  @override
  State<EmployeeWeek> createState() => _EmployeeWeekState();
}

class _EmployeeWeekState extends State<EmployeeWeek> {

  @override
  void initState()  {

     // populatedata();
    super.initState();
  }


  // void populatedata()  {
  //   setState(()  {
  //     logcont.employeefilter(widget.employeeid, number);
  //   });
  //
  // }
  @override
  Widget build(BuildContext context) {
    return GetBuilder<EmployeeDetailController>(
      autoRemove: false,
      init: EmployeeDetailController(),
      builder: (cont) => Column(

        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(onPressed: (){
                cont.number++;
                cont.filtercomments();
              }, icon: const Icon(Icons.chevron_left)),
              Text(DateFormat("MMMM dd, yyyy").format(DateTime.now().subtract(Duration(days: 7 * cont.number)))),
              const Text("  -  "),
              Text(DateFormat("MMMM dd, yyyy").format(DateTime.now().subtract(Duration(days: 7 * (cont.number-1))))),
              IconButton(onPressed: cont.number==1?null: (){
                cont.number--;
                cont.filtercomments();
              }, icon: const Icon(Icons.chevron_right)),
            ],
          ),
          Container(
            height: 40,
            decoration: BoxDecoration(border: Border.all(),borderRadius: BorderRadius.circular(5)),
            margin: const EdgeInsets.all(10.0),
            child: TextFormField(
              onChanged: (val) {
                cont.runFilter(val);
              },
              style: const TextStyle(
                color: Colors.black87,
              ),
              decoration: const InputDecoration(
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.only(
                    top: 5,
                  ),
                  prefixIcon: Icon(
                    Icons.search,
                    color: Color(0xff7F7C82),
                  ),
                  hintText: 'Search',
                  hintStyle: TextStyle(
                    color: Colors.black38,
                  )),
            ),
          ),
          Expanded(
            child:
            cont.foundcomments.isEmpty? const Center(child: Text("No Comments Found")) : ListView.builder(
              physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
              itemCount: cont.foundcomments.length,
              itemBuilder: (context, index) {
                Commit commit = cont.foundcomments[index];
                return Card(child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(commit.task,style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                      Text(commit.des,style: TextStyle(fontSize: 12,color: Colors.grey.shade600),),
                    ],
                  ),
                ));
              },
            ),
          ),
        ],
      ),
    );
  }
}
