import 'dart:convert';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:intl/intl.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Recovery/recovery_detail_page.dart';
import 'package:rafi_admin/Components/loader_widget.dart';
import 'package:rafi_admin/Controllers/recovery_controller.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import 'Modal/recovery_model.dart';

class RecoverySheet extends StatefulWidget {
  const RecoverySheet({Key? key,}) : super(key: key);

  @override
  State<RecoverySheet> createState() => _RecoverySheetState();
}

class _RecoverySheetState extends State<RecoverySheet> {
  String name = '';
  String unit = '';
  String marla = '';
  String date = '';
  var formatter = NumberFormat('#,##,000');
  @override
  initState() {
    // at the beginning, all users are shown
    // getAllData();
    super.initState();
  }

  // This function is called whenever the text field changes


  @override
  Widget build(BuildContext context) {
    return GetBuilder<RecoveryController>(
      autoRemove: false,
      init: RecoveryController(),
      builder: (cont) => Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Name",
                    style: cont.isName
                        ? const TextStyle(
                        decoration: TextDecoration.underline,
                        color: Colors.blue,
                        fontWeight: FontWeight.bold)
                        : const TextStyle()),
                Text("Unit no",
                    style: cont.isUnit
                        ? const TextStyle(
                        decoration: TextDecoration.underline,
                        color: Colors.blue,
                        fontWeight: FontWeight.bold)
                        : const TextStyle()),
                Text("Marla",
                    style: cont.isMarla
                        ? const TextStyle(
                        decoration: TextDecoration.underline,
                        color: Colors.blue,
                        fontWeight: FontWeight.bold)
                        : const TextStyle()),
                Text("Date",
                    style: cont.isDate
                        ? const TextStyle(
                        decoration: TextDecoration.underline,
                        color: Colors.blue,
                        fontWeight: FontWeight.bold)
                        : const TextStyle()),
                ElevatedButton.icon(
                  style: ElevatedButton.styleFrom(
                    primary: const Color(0xfff1f6f8),
                  ),
                  onPressed: () {
                    showModalBottomSheet<void>(
                      isScrollControlled: true,
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(20),
                            topLeft: Radius.circular(20)),
                      ),
                      context: context,

                      builder: (BuildContext context) {
                        return Padding(
                          padding: MediaQuery.of(context).viewInsets,
                          child: Container(
                            height: 250,
                            padding: const EdgeInsets.all(5.0),
                            child: Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  const Padding(
                                    padding: EdgeInsets.only(top: 15.0),
                                    child: Text(
                                      'Filter search',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18),
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Container(
                                        width: (MediaQuery.of(context).size.width *
                                            0.45),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(4),
                                            boxShadow: const [
                                              BoxShadow(
                                                  color: Colors.black26,
                                                  blurRadius: 5,
                                                  offset: Offset(0, 0.5))
                                            ]),
                                        height: 45,
                                        child: TextFormField(
                                          initialValue: name,
                                          onChanged: (val) {
                                            setState(() {
                                              name = val;
                                            });
                                          },
                                          style: const TextStyle(
                                            color: Colors.black87,
                                          ),
                                          decoration: const InputDecoration(
                                              border: InputBorder.none,
                                              contentPadding: EdgeInsets.only(
                                                top: 14,
                                              ),
                                              prefixIcon: Icon(
                                                Icons.perm_identity,
                                                color: Color(0xff7F7C82),
                                              ),
                                              hintText: "Name",
                                              hintStyle: TextStyle(
                                                color: Colors.black38,
                                              )),
                                        ),
                                      ),
                                      Container(
                                        width: (MediaQuery.of(context).size.width *
                                            0.45),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(4),
                                            boxShadow: const [
                                              BoxShadow(
                                                  color: Colors.black26,
                                                  blurRadius: 5,
                                                  offset: Offset(0, 0.5))
                                            ]),
                                        height: 45,
                                        child: TextFormField(
                                          initialValue: unit,
                                          keyboardType: TextInputType.number,
                                          onChanged: (val) {
                                            setState(() {
                                              unit = val;
                                            });
                                          },
                                          style: const TextStyle(
                                            color: Colors.black87,
                                          ),
                                          decoration: const InputDecoration(
                                              border: InputBorder.none,
                                              contentPadding: EdgeInsets.only(
                                                top: 14,
                                              ),
                                              prefixIcon: Icon(
                                                Icons.house,
                                                color: Color(0xff7F7C82),
                                              ),
                                              hintText: "Unit No",
                                              hintStyle: TextStyle(
                                                color: Colors.black38,
                                              )),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Container(
                                        width: (MediaQuery.of(context).size.width *
                                            0.45),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(4),
                                            boxShadow: const [
                                              BoxShadow(
                                                  color: Colors.black26,
                                                  blurRadius: 5,
                                                  offset: Offset(0, 0.5))
                                            ]),
                                        height: 45,
                                        child: TextFormField(
                                          initialValue: marla,
                                          keyboardType: TextInputType.number,
                                          onChanged: (val) {
                                            setState(() {
                                              marla = val;
                                            });
                                          },
                                          style: const TextStyle(
                                            color: Colors.black87,
                                          ),
                                          decoration: const InputDecoration(
                                              border: InputBorder.none,
                                              contentPadding: EdgeInsets.only(
                                                top: 14,
                                              ),
                                              prefixIcon: Icon(
                                                Icons.architecture,
                                                color: Color(0xff7F7C82),
                                              ),
                                              hintText: "Marla",
                                              hintStyle: TextStyle(
                                                color: Colors.black38,
                                              )),
                                        ),
                                      ),
                                      Container(
                                        width: (MediaQuery.of(context).size.width *
                                            0.45),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(4),
                                            boxShadow: const [
                                              BoxShadow(
                                                  color: Colors.black26,
                                                  blurRadius: 5,
                                                  offset: Offset(0, 0.5))
                                            ]),
                                        height: 45,
                                        child: TextButton(
                                          onPressed: () {
                                            showDialog(
                                              context: context,
                                              barrierDismissible: false,
                                              builder: (BuildContext context) => AlertDialog(
                                                  contentPadding: EdgeInsets.only(
                                                      bottom: 20, top: 0, left: 10, right: 10),
                                                  content: Container(
                                                    height: 400,
                                                    child: Column(
                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                      children: [
                                                        Container(
                                                          height: 350,
                                                          width: 250,
                                                          child: Stack(
                                                            children: <Widget>[
                                                              Positioned(
                                                                left: 0,
                                                                top: 0,
                                                                right: 0,
                                                                bottom: 0,
                                                                child: CupertinoDatePicker(
                                                                  onDateTimeChanged: (val) {
                                                                    setState(() {
                                                                      date = DateFormat('MM')
                                                                          .format(val);
                                                                    });
                                                                  },
                                                                  mode: CupertinoDatePickerMode.date,
                                                                  initialDateTime: DateTime.now(),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        Row(
                                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                          children: [
                                                            Container(
                                                                height: 40,
                                                                decoration: BoxDecoration(
                                                                    color: Colors.blue,
                                                                    borderRadius: BorderRadius.circular(5)),
                                                                child: TextButton(
                                                                    onPressed: () {
                                                                      Navigator.of(
                                                                        context,
                                                                      ).pop();
                                                                    },
                                                                    child: const Text(
                                                                      "Confirm",
                                                                      style: TextStyle(color: Colors.white),
                                                                    ))),
                                                            Container(
                                                                height: 40,
                                                                decoration: BoxDecoration(
                                                                    color: Colors.blue,
                                                                    borderRadius: BorderRadius.circular(5)),
                                                                child: TextButton(
                                                                    onPressed: () {
                                                                      setState(() {
                                                                        date = '';
                                                                      });
                                                                      Navigator.of(
                                                                        context,
                                                                      ).pop();
                                                                    },
                                                                    child: const Text(
                                                                      "Cancel",
                                                                      style: TextStyle(color: Colors.white),
                                                                    ))),
                                                          ],
                                                        )
                                                      ],
                                                    ),
                                                  )),
                                            );
                                          },
                                          child: Row(
                                            children:  [
                                              const Icon(Icons.calendar_today_outlined,color: Colors.black54,),
                                              const SizedBox(width: 5,),
                                              Text(date==''?"Month":date,style: TextStyle(color:Colors.black54),)
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  ElevatedButton(
                                    child: const Text('Save'),
                                    onPressed: () async {
                                      cont.results = cont.recoveries!;
                                      await cont.runFilter(name);
                                      await cont.runUnitFilter(unit);
                                      await cont.runMarlaFilter(marla);
                                      await cont.runDateFilter(date);
                                      Navigator.pop(context);
                                    },
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  },
                  icon: const Icon(
                    Icons.filter_list_alt,
                    color: Colors.black,
                    size: 24.0,
                  ),
                  label: const Text(
                    'Filters',
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              ],
            ),
          ),
          Flexible(
            child: cont.recoveries ==null ? LoadingWidget() : cont.foundResult.isNotEmpty
                ? RefreshIndicator(
              onRefresh: () async{
                return await cont.refreshAll();
              },
                  child: ListView.builder(
              physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
              itemCount: cont.foundResult.length,
              itemBuilder: (context, i) {
                  final recovery = cont.foundResult[i];
                  return Container(
                    margin: const EdgeInsets.only(
                        top: 5, bottom: 5, left: 10, right: 10),
                    padding: EdgeInsets.zero,
                    child: Card(
                      color: Color(0xffbf953f),
                      elevation: 5,
                      margin: const EdgeInsets.all(0),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(3),
                          color: const Color(0xfff1f6f8),
                        ),
                        margin: const EdgeInsets.all(1.0),
                        padding: const EdgeInsets.all(5.0),
                        child: InkWell(
                          splashColor: Colors.white,
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => RecoveryDetailPage(
                                      recovery: recovery)),
                            );
                          },
                          child:  Column(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        width: MediaQuery.of(context)
                                            .size
                                            .width *
                                            0.6,
                                        child: Flex(
                                            direction: Axis.vertical,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children:[ Text(
                                              recovery.accountTitle!,
                                              style: const TextStyle(
                                                  fontSize: 18.0,
                                                  fontWeight: FontWeight.bold,
                                                  color: Color(0xff004251)),
                                            ),]
                                        ),
                                      ),
                                      Text(
                                        'Contract date : ' +
                                            DateFormat(' dd-MMM-yyyy').format(DateTime.parse(recovery.contractDate!)),
                                        style: const TextStyle(
                                            color: Color(0xff004251)),
                                      ),
                                    ],
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Unit : ' +
                                            recovery.unitNumber.toString() +
                                            "-" +
                                            recovery.phaseName!,
                                        style: const TextStyle(
                                          color: Color(0xff004251),
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            'Unit Marla : ' +
                                                recovery.unitMarla.toString(),
                                            style: const TextStyle(
                                                color: Color(0xff004251)),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 15,
                              ),
                              Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Rate : ' +
                                            formatter
                                                .format(recovery.saleRate),
                                        style: const TextStyle(
                                          color: Color(0xff004251),
                                        ),
                                      ),
                                      Text(
                                        'Total Price : ' +
                                            formatter.format(
                                                recovery.totalPrice),
                                        style: const TextStyle(
                                            color: Color(0xff004251)),
                                      ),
                                      Text(
                                        'Advance : ' +
                                            formatter.format(
                                                recovery.advancePayment),
                                        style: const TextStyle(
                                            color: Color(0xff004251)),
                                      ),
                                    ],
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Installments : ' +
                                            recovery.noOfInstallments
                                                .toString(),
                                        style: const TextStyle(
                                          color: Color(0xff004251),
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
              },
            ),
                )
                : const Center(
              child: Text(
                'No results found',
                style: TextStyle(fontSize: 24),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
