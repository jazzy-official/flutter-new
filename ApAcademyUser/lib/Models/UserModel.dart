class UserModel {
  final String id;
  final String displayName;
  final String email;
  final String phone;
  final dynamic photo;
  final String? role;
  final bool? approved;
  final bool? isActive;
  final String? rollno;
  final String? token;
  final String? crichqid;
  final String? passport;
  final String? parentName;
  final DateTime? dob;

  const UserModel(
      {
        this.parentName,
        this.dob,
         this.isActive,
        required this.id,
      required this.displayName,
      required this.email,
      required this.phone,
      this.photo,
      this.role,
      this.rollno,
      this.approved,
      this.token,
        this.crichqid,
      this.passport});
}
