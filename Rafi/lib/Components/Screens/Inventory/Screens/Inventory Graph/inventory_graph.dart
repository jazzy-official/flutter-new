import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/Units%20Model/units.dart';
import 'package:rafi_admin/Components/loader_widget.dart';
import 'package:rafi_admin/Controllers/accounts_controller.dart';
import 'package:rafi_admin/Controllers/inventory_controller.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class InventoryGraph extends StatefulWidget {
  InventoryGraph({Key? key}) : super(key: key);

  @override
  State<InventoryGraph> createState() => _InventoryGraphState();
}

class _InventoryGraphState extends State<InventoryGraph> {

  @override
  Widget build(BuildContext context) {
    return GetBuilder<InventoryController>(
      autoRemove: false,
      init: InventoryController(),
      builder: (cont) => cont.unitsData==null ? LoadingWidget() :
      RefreshIndicator(
        onRefresh: (){
          return cont.refreshAll();
        },
          child: buildUnits(context, cont.unitsData!)
      ),
    );
  }

  Widget buildUnits(BuildContext context,Unit unitData) {
    final List<_ChartData> chartData = [
      _ChartData('Reserved',unitData.reserved.toDouble(), const Color.fromRGBO(255,189,57,1)),
      _ChartData('Available', unitData.available.toDouble(), const Color.fromRGBO(9,0,136,1)),
      _ChartData('Sold',  unitData.sold.toDouble(), const Color.fromRGBO(228,0,124,1)),

    ];
    return ListView(
      physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
      children: [
        SizedBox(height: 20,),
        Center(child: Text("Units",style: TextStyle(fontWeight:FontWeight.bold,fontSize: 22))),
        Expanded(
          child: SfCircularChart(
              legend: Legend(isVisible: true,
                textStyle: const TextStyle(
                  // color: Colors.white,
                  // fontSize: 15,
                  // fontStyle: FontStyle.italic,
                  // fontWeight: FontWeight.w900
                ),
                position: LegendPosition.bottom,
                itemPadding: 25,
              ),
              series: <CircularSeries<_ChartData, String>>[
                RadialBarSeries<_ChartData, String>(
                    maximumValue: unitData.all.toDouble(),
                    radius: '100%',
                    gap: '3%',
                    dataSource: chartData,
                    cornerStyle: CornerStyle.bothCurve,
                    xValueMapper: (_ChartData data, _) => data.x,
                    yValueMapper: (_ChartData data, _) => data.y,
                    pointColorMapper: (_ChartData data, _) => data.color)
              ]),
        ),
        Column(children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                height:70,
                width: MediaQuery.of(context).size.width * 0.42,
                decoration: BoxDecoration(
                  color: Color.fromRGBO(147,0,119,1),
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: Center(child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('Total',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 16),),
                    Text(unitData.all.toString(),style: TextStyle(color: Colors.white),),
                  ],
                )),
              ),
              Container(
                height:70,
                width: MediaQuery.of(context).size.width * 0.42,
                decoration: BoxDecoration(
                  color: Color.fromRGBO(9,0,136,1),
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: Center(child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('Available',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 16),),
                    Text(unitData.available.toString(),style: TextStyle(color: Colors.white),),
                  ],
                )),
              ),
            ],
          ),
          const SizedBox(height: 10,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                height:70,
                width: MediaQuery.of(context).size.width * 0.42,
                decoration: BoxDecoration(
                  color: Color.fromRGBO(255,189,57,1),
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: Center(child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('Reserved',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16,color: Colors.black54),),
                    Text(unitData.reserved.toString()),
                  ],
                )),
              ),
              Container(
                height:70,
                width: MediaQuery.of(context).size.width * 0.42,
                decoration: BoxDecoration(
                  color: Color.fromRGBO(228,0,124,1),
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: Center(child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('Sold',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 16),),
                    Text(unitData.sold.toString(),style: TextStyle(color: Colors.white),),
                  ],
                )),
              ),
            ],
          ),
        ],),
        SizedBox(height: 20,),
      ],
    );
  }
}
class _ChartData {
  _ChartData(this.x, this.y, this.color);
  final String x;
  final double y;
  final Color color;
}