import 'package:flutter/material.dart';

class DocsApi{


  static const credentials = r'''
  {
  "type": "service_account",
  "project_id": "dail-log",
  "private_key_id": "de3c069b4c3766feea3e0853f611cde145b6471f",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDy7LVE3bIznFSq\n0jWFyN+2y3g2BeMxjCDqlamjhvFsT5UHaKSxR72awrFcbJ0MfFiyjhO6uJw1BTB1\nwIRB7XsVe8uzU6cEqzXSV/TcL9UA9v4VZgBIvI8z7s4AC3g7RraeqCYBvBuDO8cV\n1D8wNWpRAOr9cYV5G6wHDsvWyeAXFOx25WN3X7FaTevr3msBMClDY+xuZ5fw+5wr\ndYI6PhON658rDh1jot2T66eeIMmvT7qh9U6oJsXWL970bvwTIbrdecWLoXc14nTd\nrd/SpSkP8VL2poUdpEQ8o2kWRG54XOIpIW4BRIpwLMSQ4jgYkBeGEWsvRj3qKODG\n6r8tacxLAgMBAAECggEAde4K+/EiSyLYCYt+F0IStPNnGtbV2h5cdnWMgooGlt21\njAC1fyjEN6bPon5J6Z9OEfSeSFXJ44Vu5qM8saK/z4inNZnlKYOSX9h6UxBXeB3v\n8GRxu93qKTyDvgrTZfdovXIE7W+r7ZQuDH9a46TyxXtMGRiswsP3yf9ZXp7nTlxU\nXcBiNCnp0aRTciCZPaxqozG2MZ5eBx46ecz7oEZJWbUUGzitlCzxAuw46al05KJK\nDFn8eU3KxFW2VdnT6EYKRcealMiYXXOXBEsRA5rn97XyjT8H/lRE0q1WZ1xbaFuI\nL77wxjf8eQGnB4O09+NWnIakHGXvFqp6sGIwnbLouQKBgQD9HFgTDMI54gR6Bp06\n83R2tdSq1CQPsZ+r8sM5RM9tvKIsFvwlXMNW329OcNs4RqIZ18S/Tw2YVT3ymBGz\n9fMOK+Xow0j9C/C/E6ZFFo3DlpN4fjbMjlHPtNOD9xiSrSUiw+emFAFGRz44hAbB\nCZ/EWMgwyN9SxFTClRFcSbUrswKBgQD1spj/cwK03xcohE6srZHKE2FfYz8APERr\nAln24BoYYMp2fd6y585NJ3+9wO+pBjqp7ete1Hm+9yFujQkjdC0jKYKTbK8H8u5t\nbMV28m/3aiKuoMzrnYagdMoENyfQJyoUmr7JpLfpy4EQRGdV7TOpQRgfuMz9vPer\nBx6pZZoxCQKBgQDlTgFouL1ZTS77EfTtzFOtnEufu/MzmJrsX1GIF5PIegV95hDn\nFGQP1vpy2QFxuWPv/69KFidd45PsrOIU5HQfWZu/RPRh86hWo0mV+7nRASB3H/kr\nVqA9iTe6rWhAscYb+dlqd2M0NSTkdI/d4IcyjbBHqiPQSvfzFLOWpVwRywKBgQDU\nu5aNV194i9DZW/VivcYFWVJt7NiXs5bpsFeE0fTGNsgWYFV6Qgr18sU/3tWjW6lf\niIjKyt2+4usndSX8dJLAZyiPzQKft0+Jnyw6GR+o8VSmLsQ/HV17gKOJktf9+msM\n/A9FOEGlRHazRwbR91dLrPjePnv/Fqa6hqedtEMVQQKBgBOhJypDg7i4ChiOapRD\nUyBXu8piPEMjhEYxJRF2KhUq8xIoxrvrbPWZNMgDEsnLkv9fIE343PwRsJexYWk1\nlaup5DEjG5ssYT9fvhVlbLJ+4EJcvUiAOC+Z1A9VmVbz/KUWf5jV4oXVPB6M2noA\n2D8BPA5LMKRKM0zZq7Irjyw5\n-----END PRIVATE KEY-----\n",
  "client_email": "docs-823@dail-log.iam.gserviceaccount.com",
  "client_id": "107935361228437657957",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/docs-823%40dail-log.iam.gserviceaccount.com"
}
  ''';
}