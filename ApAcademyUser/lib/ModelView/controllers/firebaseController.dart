import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';

import 'db_controller.dart';

class FirebaseStorge extends GetxController {
  FirebaseStorage firebaseStorage = FirebaseStorage.instance;
  final dbHelper = Get.put(DatabaseController());
  getImages() {
    firebaseStorage.ref().child('images').child('defaultProfile.png');
  }

  Future<String?> uploadImageToFirebase(
    File image,
  ) async {
    try {
      // Make random image name.

      String imageLocation =
          'profileimages/${FirebaseAuth.instance.currentUser!.uid}.jpg';

      // Upload image to firebase.
      final storageReference =
          FirebaseStorage.instance.ref().child(imageLocation);
      final uploadTask = storageReference.putFile(image);
      await uploadTask.whenComplete(() async {
        uploadTask.snapshot.ref.fullPath;
        await dbHelper.addProfileImgPathToDatabase(imageLocation);
      });
    } catch (e) {
      print(e);
    }
  }
}
