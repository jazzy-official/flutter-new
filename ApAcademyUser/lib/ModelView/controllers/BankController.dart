import 'dart:async';

import 'package:get/get.dart';
import 'package:mongo_dart/mongo_dart.dart';

import 'db_controller.dart';

class BanksController extends GetxController {
  Rx<List<Map<String, dynamic>>> banksList = Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get banks => banksList.value;

  final dbHelp = Get.put(DatabaseController());
  @override
  Future<void> onInit() async {
    //await getTeamDropDown();

    super.onInit();
  }

  refreshBankData() {
    this.banksList.bindStream(DatabaseController().banksStream());
  }
}
