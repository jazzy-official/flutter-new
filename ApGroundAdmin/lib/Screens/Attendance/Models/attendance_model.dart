import 'package:ap_ground_admin/Screens/Attendance/Models/entry_model.dart';
import 'package:ap_ground_admin/Screens/Grounds/Models/slot_model.dart';

class Attendance{
  final String? id;
  final String date;
  final List<Entry> entries;
  Attendance({this.id,required this.date,required this.entries});

  factory Attendance.fromJson(dynamic json,String? id) {
    return Attendance(
      id: id.toString(),
      date : json['date'],
      entries : List<Entry>.from(json["entries"].map((dynamic item) => Entry.fromJson(item),).toList()),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['date'] = this.date;
    data['entries'] = List<dynamic>.from(entries.map((x) => x.toJson()));
    return data;
  }

}