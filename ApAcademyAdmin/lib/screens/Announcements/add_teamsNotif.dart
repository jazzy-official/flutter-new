import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:get/get.dart';

class SelectTeams extends StatefulWidget {
  @override
  _SelectTeamsState createState() => _SelectTeamsState();
}

class _SelectTeamsState extends State<SelectTeams> {
  static const int numItems = 10;
  String? _value;
  List<bool> selected = List<bool>.generate(numItems, (index) => false);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: Container(
          height: Get.height * 0.86,
          width: Get.width * 0.96,
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: 130,
                        child: TextField(
                            decoration: InputDecoration(
                                prefixIcon: Icon(Icons.search),
                                border: InputBorder.none,
                                fillColor: Colors.grey,
                                hintText: "Search Name")),
                      ),
                      Padding(
                        //Add padding around textfield
                        padding: EdgeInsets.only(top: 7.0, left: 10, right: 10),
                        child: Padding(
                          padding: const EdgeInsets.all(0.0),
                          child: Container(
                            decoration: BoxDecoration(
                                color: MyResources.backgroundColor,
                                border: Border.all(color: Colors.blue),
                                borderRadius: BorderRadius.circular(10)),
                            //  color: MyResources.backgroundColor,
                            width: Get.width,
                            height: 35,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: DropdownButton<String>(
                                iconSize: 18,
                                isExpanded: true,
                                underline: Text(""),
                                value: _value,
                                focusColor: Colors.blue,
                                elevation: 12,
                                items: [
                                  DropdownMenuItem<String>(
                                    child: Text('Under 11'),
                                    value: 'Under 11',
                                  ),
                                  DropdownMenuItem<String>(
                                    child: Text('Under 18'),
                                    value: 'Under 18',
                                  ),
                                ],
                                onChanged: (String? value) {
                                  print(value);
                                  setState(() {
                                    _value = value;
                                  });
                                },
                                hint: Text('Select Item'),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 12.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          " Select Teams",
                          style: MyResources.appHeadingStyle,
                        ),
                        Container(
                          width: Get.width * 0.2,
                          height: 35,
                          child: RaisedButton(
                            textColor: Colors.white,
                            color: Colors.blue,
                            child: Text(
                              "Add",
                            ),
                            onPressed: () {
                              //  Get.d AddTeam()));
                            },
                            shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  DataTable(
                    columnSpacing: 120,
                    columns: const <DataColumn>[
                      DataColumn(
                        label: Text(
                          'Teams',
                          style: TextStyle(fontStyle: FontStyle.normal),
                        ),
                      ),
                      DataColumn(
                        label: Text(
                          'Age',
                          style: TextStyle(fontStyle: FontStyle.normal),
                        ),
                      ),
                    ],
                    rows: List<DataRow>.generate(
                      numItems,
                      (index) => DataRow(
                        color: MaterialStateProperty.resolveWith<Color>(
                            (Set<MaterialState> states) {
                          // All rows will have the same selected color.
                          if (states.contains(MaterialState.selected))
                            return Theme.of(context)
                                .colorScheme
                                .primary
                                .withOpacity(0.08);
                          // Even rows will have a grey color.
                          if (index % 2 == 0)
                            return Colors.grey.withOpacity(0.3);
                          return Colors.transparent; // Use default value for other states and odd rows.
                        }),
                        cells: [
                          DataCell(Text('Team  $index')),
                          DataCell(Text(' 11'))
                        ],
                        selected: selected[index],
                        onSelectChanged: (bool? value) {
                          setState(() {
                            selected[index] = value!;
                          });
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
