import 'package:ap_booking_user/Screens/Shared/loading_widget.dart';
import 'package:ap_booking_user/constants/util.dart';
import 'package:ap_booking_user/controllers/booking_controller.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'Models/booking_model.dart';
import 'add_booking.dart';
import 'booking_card.dart';
class UpComingBookings extends StatelessWidget {
  final List<Booking> myBookings;
  const UpComingBookings({Key? key, required this.myBookings}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xfffefefe),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                padding: EdgeInsets.all(5),
                height: 40,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: MyResources.buttonColor,
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => AddBooking(),
                        ),
                      );
                    }, child: Text("Add Booking")),
              )
            ],
          ),
          Expanded(
            child: ListView.builder(
                padding: const EdgeInsets.all(8),
                itemCount: myBookings.length,
                itemBuilder: (BuildContext context, int index) {
                  Booking booking = myBookings[index];
                  return BookingCard(booking: booking,);


                }),
          ),
        ],
      ),
    );
  }
}

