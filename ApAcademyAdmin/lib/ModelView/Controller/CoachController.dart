import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:get/get.dart';
import 'package:mongo_dart/mongo_dart.dart';

class CoachController extends GetxController {
  Rx<List<Map<String, dynamic>>> coachList = Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get coaches => coachList.value;
  Rx<List<Map<String, dynamic>>> adminList = Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get admins => adminList.value;
  List<Map<String, dynamic>> userList = <Map<String, dynamic>>[].obs;

  @override
  void onInit() {
    // coachList.bindStream(DatabaseController().coachStream('coach'));

    super.onInit();
  }

  final dbH = Get.put(DatabaseController());
  Future<void> getUsers() async {
    userList = (await dbH.getAlluserData())!;
  }

  Future<void> delTeams(ObjectId id) async {
    await DatabaseController().deletTeam(id).then((value) => refreshcoach());
  }

  Future<bool> makeAdminactive(ObjectId gid) async {
    return await dbH.makeAdminactive(gid);
  }

  Future<bool> makeAdminInactive(ObjectId gid) async {
    return await dbH.makeAdminInactive(gid);
  }

  refreshcoach() {
    coachList.bindStream(DatabaseController().coachStream('Coach'));
    adminList.bindStream(DatabaseController().adminStream('Admin'));
  }


}
