class InvestmentModel {
  int investmentId;
  double amount;
  double amountPaid;
  String investmentName;
  String accountTitle;
  int accId;
  String investorName;
  int investorId;
  String investorCnic;
  String projectName;
  String dateAdded;
  int unitSold;
  double amountRecovery;
  double amountReceived;
  double contractsAmount;

  InvestmentModel({
    required this.amount,
    required this.projectName,
    required this.investmentName,
    required this.accountTitle,
    required this.accId,
    required this.amountPaid,
    required this.amountReceived,
    required this.amountRecovery,
    required this.contractsAmount,
    required this.dateAdded,
    required this.investmentId,
    required this.investorCnic,
    required this.investorId,
    required this.investorName,
    required this.unitSold,
    InvestmentId,
  });

  factory InvestmentModel.fromJson(Map<String, dynamic> json) {
    return InvestmentModel(
      investorName: json['investorName'],
      projectName: json['projectName'],
      amount: json['amount'],
      amountReceived: json['amountReceived'],
      amountPaid: json['amountPaid'],
      investmentName:json['investmentName'],
      contractsAmount:json['contractsAmount'],
      investorId: json['investorId'],
      accountTitle: json['accounttitle'],
      unitSold: json['unitSold'],
      investmentId:json['id'],
      dateAdded:json['dateAdded'],
      investorCnic:json['investorCnic'],
      accId: json['accid'],
      amountRecovery: json['amountRecovery'],
    );
  }
}
