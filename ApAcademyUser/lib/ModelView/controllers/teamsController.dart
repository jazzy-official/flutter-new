import 'package:get/get.dart';

import 'db_controller.dart';
import '../../Constants/db_cred.dart';

class TeamController extends GetxController {
  Rx<List<Map<String, dynamic>>> teamsList = Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get teams => teamsList.value;
  @override
  Future<void> onInit() async {
    teamsList.bindStream(
        DatabaseController().teamStream()); //id)); //Stream Comming from
    super.onInit();
  }

  @override
  void onClose() {
    //  db.close();
    super.onClose();
  }
}
