import 'package:ap_academy_new/Screens/drawer_screen.dart';
import 'package:ap_academy_new/shared/SplashWidget.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'Constants/db_cred.dart';
import 'ModelView/controllers/auth_controller.dart';
import 'Constants/util.dart';
import 'Constants/db_cred.dart';
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  // if (GetPlatform.isAndroid)
  //   FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);
  ////////////////////////////////////---------Notification Handler--------------------////////////
  //Remove this method to stop OneSignal Debugging
  await getMongoPort();

  runApp(MyApp());
}

Future getMongoPort() async {
  try {
    if (!db.isConnected) {
      try {
        // if (!db.isConnected && db.state.toString() != "State.OPENING") {
        await db.open(secure:true).then((value) {});
        //  }
      } catch (error) {
        print(error.toString());
        print('mongo db is not opening');
      }
    }
  } catch (e) {
    print(e.toString());
    Get.snackbar("MongoDb Error", e.toString());
  }

  print("[AP Sports] Database is connected : " + db.isConnected.toString());
}

class MyApp extends StatelessWidget {
   MyApp({Key? key}) : super(key: key);
  final authController = Get.put(AuthController());
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: myResources.buttonColor,
        textTheme: GoogleFonts.aBeeZeeTextTheme(
          Theme.of(context).textTheme,
        ),
      ),
      home: FutureBuilder(
        future: authController.checkUserLoggedIn(),
        builder: (context,dynamic snapshot) {
          // if (snapshot.hasError) {
          //   return Text('Error Processing');
          // }
          if (snapshot.hasData) {
            print("yes snapshot has data");
            return snapshot.data;
          }
          return SplashWidget();
        },
      ),
    );
  }
}

