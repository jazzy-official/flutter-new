import 'package:chips_choice_null_safety/chips_choice_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Screens/Home/home_screen.dart';
import 'package:room_buddy/navigation.dart';

class TraitsForm extends StatefulWidget {
  TraitsForm({Key? key}) : super(key: key);

  @override
  State<TraitsForm> createState() => _TraitsFormState();
}

class _TraitsFormState extends State<TraitsForm> {
  List<String> tags = [];
  List<String> options = [
    'Calm',
    'Active',
    'Social',
    'Friendly',
    'Caring',
    'Easy Going',
    'Fun',
    'Organized',
    'Passionate',
    'Enthusiastic',
    'Empathetic',
    'Practical',
    'Relaxed',
    'Honest',
    'Optimistic',
    'Flexible',
    'Tolerant',
    'Rational',
    'Creative',
    'Cheerful',
    'Determined',
    'Drug addict',
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: myResources.background,
        title: const Text(
          "Add your personality traits",
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
        iconTheme: const IconThemeData(
          color: Colors.black, //change your color here
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 15.0, left: 20),
            child: const Text(
              "Add maximum 10 Traits",
              style: TextStyle(
                  color: Colors.black45,
                  fontSize: 13,
                  fontWeight: FontWeight.w500),
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: ChipsChoice<String>.multiple(
                value: tags,
                wrapped: true,
                onChanged: (val) {
                  if (val.length < 11) {
                    setState(() => tags = val);
                  } else {
                    Get.snackbar("Alert", "You can maximum select 10",
                        snackPosition: SnackPosition.BOTTOM);
                  }
                },
                choiceItems: C2Choice.listFrom<String, String>(
                  source: options,
                  value: (i, v) => v,
                  label: (i, v) => v,
                ),
                choiceStyle: C2ChoiceStyle(
                  elevation: 2,
                  color: myResources.themeColor,
                ),
                choiceActiveStyle: C2ChoiceStyle(
                  color: myResources.themeColor,
                  brightness: Brightness.dark,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(15),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: myResources.themeColor,
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(14))),
              ),
              onPressed: () {
                Get.offAll(NavigationScreen());
              },
              child: Container(
                padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    const Text(
                      'Confirm',
                      style: TextStyle(color: Colors.white),
                    ),
                    Container(
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(20)),
                        color: myResources.themeColor,
                      ),
                      child: Icon(
                        Icons.arrow_forward_ios,
                        color: Colors.white,
                        size: 16,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
