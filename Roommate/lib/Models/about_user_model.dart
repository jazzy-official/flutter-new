import 'package:flutter/material.dart';
import 'package:room_buddy/Models/habits_modal.dart';

class AboutUser {
  final String gender ;
  final RangeValues age ;
  final String occupation ;
  final String martialStatus;
  final Habits habits;
  final List<String>? interests;
  final List<String>? traits;

  AboutUser( this.gender, this.age, this.occupation, this.martialStatus, this.habits, this.interests, this.traits);

  AboutUser.fromJson(Map<String, dynamic> json)
      : gender = json['name'],
        age = json['email'],
        occupation = json['email'],
        martialStatus = json['email'],
        habits = json['email'],
        interests = json['email'],
        traits = json['email'];

  Map<String, dynamic> toJson() => {
    'name': gender,
    'email': age,
  };
}