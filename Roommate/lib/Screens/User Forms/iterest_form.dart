import 'package:chips_choice_null_safety/chips_choice_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Screens/User%20Forms/traits_form.dart';

class InterestForm extends StatefulWidget {
  InterestForm({Key? key}) : super(key: key);

  @override
  State<InterestForm> createState() => _InterestFormState();
}

class _InterestFormState extends State<InterestForm> {
  List<String> tags = [];
  List<String> options = [
    'News',
    'Entertainment',
    'Politics',
    'Automotive',
    'Sports',
    'Education',
    'Fashion',
    'Travel',
    'Food',
    'Tech',
    'Science',
    'Football',
    'Music',
    'Reading',
    'TV series',
    'BasketBall',
    'Gardening',
    'Cooking',
    'Podcast',
    'Cricket',
    'Theater',
    'Youtube',
    'Swimming',
    'Anime',
    'PUBG',
    'TicTok',
    'Memes',
    'Singing',
    'Comedy',
    'Guitar',
    'Karaoke',
    'Piano',
    'Photography',
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: myResources.background,
        title: const Text(
          "Add your Interests",
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
        iconTheme: const IconThemeData(
          color: Colors.black, //change your color here
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.only(top: 15.0,left: 20),
            child: Text("Add maximum 10 interests",style: TextStyle(color: Colors.black45,fontSize: 13,fontWeight: FontWeight.w500),),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: ChipsChoice<String>.multiple(
                value: tags,
                wrapped: true,
                onChanged: (val) {
                  if(val.length<11){
                    setState(() => tags = val);
                  }else{
                    Get.snackbar("Alert", "You can maximum select 10",snackPosition: SnackPosition.BOTTOM);
                  }
                  },
                choiceItems: C2Choice.listFrom<String, String>(
                  source: options,
                  value: (i, v) => v,
                  label: (i, v) => v,
                ),
                choiceStyle: C2ChoiceStyle(
                  elevation: 2,
                  color: myResources.themeColor,

                ),
                choiceActiveStyle:  C2ChoiceStyle(
                  color: myResources.themeColor,
                  brightness: Brightness.dark,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(15),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: myResources.themeColor,
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(14))),
              ),
              onPressed: () {
                Get.to(()=>  TraitsForm());
              },
              child: Container(
                padding: const EdgeInsets.symmetric(
                    vertical: 8, horizontal: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    const Text(
                      'Confirm',
                      style: TextStyle(color: Colors.white),
                    ),
                    Container(
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                        borderRadius:
                        const BorderRadius.all(Radius.circular(20)),
                        color: myResources.themeColor,
                      ),
                      child: const Icon(
                        Icons.arrow_forward_ios,
                        color: Colors.white,
                        size: 16,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
