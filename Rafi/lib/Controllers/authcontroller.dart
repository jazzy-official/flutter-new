import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:rafi_admin/Authentication/login.dart';
import 'package:rafi_admin/Authentication/modal/user.dart';
import 'package:rafi_admin/Components/Navigation/navigation_bar.dart';
import 'package:rafi_admin/Wrapper/wrapper.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class AuthController extends GetxController {

  // Intilize the flutter app
  // User? firebaseUser;
Future<void> onInit() async {
  super.onInit();
}
Future getToken() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString('token');
}
Future<int?> getProjectId() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return  prefs.getInt('projectId');
}
  Future<Widget> checkUserLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isLogin = (prefs.get('token') == "null" || prefs.get('token') == null ? false : true);
    if(isLogin) {
      String token=await getToken();
      print(token);
      return const Navigation();
    } else {
      return loginScreen();
    }
  }

  Future userLogin(String userName,String password) async {
    http.Response response = await http.post(
      Uri.parse('https://raccounting.azurewebsites.net/api/Auth/Login'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        "UserName":userName,
        "Password":password
      }),
    );
    if (response.statusCode == 200) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      User user = User.fromJson(jsonDecode(response.body));
      print(user.isSuccess);
      user.isSuccess ?
        onLoginSuccess(user.token) :
      Get.to(loginScreen());


      displayToast(user.message, null);

    } else {
      displayToast("Failed to Log in",null);
      throw Exception('Failed to login.');

    }
  }

  void onLoginSuccess(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('token', value);
    await prefs.setInt('projectId', 1);
    Get.to(()=>const Navigation());
  }
  void logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('token');
    Get.to(()=>loginScreen());
  }




  ////////////////////////////////////////////////////////////Auth with email////////////////////////
  // Future<void> signInwithEmail(String emails, String passs) async {
  //   final pController = Get.put(ProfileController());
  //   Get.dialog(Center(child: LoadingWidget()), barrierDismissible: false);
  //   try {
  //     print(emails);
  //     firebaseAuth = FirebaseAuth.instance;
  //
  //     final userCredentialData = await firebaseAuth!.signInWithEmailAndPassword(
  //         email: emails, password: passs);
  //     if (userCredentialData.user!.uid != null) {
  //       if (!db.isConnected) await database.getMongoPort(true);
  //       Get.back();
  //       await checkisActive().then((value) async {
  //         if(value){
  //           await checkUserOrAdmin().then((value) async {
  //             if (value) {
  //               Get.offAll(() => HomeDrawer());
  //               firebaseUser = userCredentialData.user;
  //               U_id = firebaseUser!.uid;
  //               update();
  //             } else {
  //               Get.back();
  //               Get.defaultDialog(
  //                   barrierDismissible: false,
  //                   buttonColor: MyResources.buttonColor,
  //                   confirmTextColor: MyResources.appBarActionsColor,
  //                   content: Container(
  //                     child: Text(
  //                         'You can not sign in to this application. please contact admin to reset your email'),
  //                   ),
  //                   onConfirm: () async {
  //                     await signOut();
  //                   });
  //             }
  //           });
  //         }
  //         else {
  //           Get.back();
  //           Get.defaultDialog(
  //               barrierDismissible: false,
  //               buttonColor: MyResources.buttonColor,
  //               confirmTextColor: MyResources.appBarActionsColor,
  //               content: Container(
  //                 child: Text(
  //                     'You can not sign in to this application. Please contact your admin'),
  //               ),
  //               onConfirm: () async {
  //                 await signOut();
  //               });
  //         }
  //       });
  //     }else{
  //       Get.snackbar("Error", "The user does not exist",icon: Icon(
  //         Icons.error,
  //         color: Colors.red,
  //       ));
  //     }
  //     await pController.refreshDate();
  //   } catch (ex) {
  //     print("me"+ex.toString());
  //
  //     Get.back();
  //     Get.snackbar('Error', "Email or Password is incorrect",
  //         duration: Duration(seconds: 5),
  //         backgroundColor: Colors.black54,
  //         colorText: Colors.white,
  //         snackPosition: SnackPosition.BOTTOM,
  //         icon: Icon(
  //           Icons.error,
  //           color: Colors.red,
  //         ));
  //   }
  // }





  // Future<void> signOut() async {
  //   Get.dialog(Center(child: LoadingWidget()), barrierDismissible: false);
  //
  //   await firebaseAuth!.signOut();
  //   update();
  //   Get.back();
  //
  //   // Navigate to Login again
  //   Get.offAll(() => LoginPage());
  // }


}
