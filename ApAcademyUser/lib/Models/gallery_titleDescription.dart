class GalleryModel {
  final int id;
  final String tittle;
  final String titlename;
  final String descriptionheading;
  final String description;

  const GalleryModel({
    required this.id,
    required this.tittle,
    required this.titlename,
    required this.descriptionheading,
    required this.description,
  });
}
