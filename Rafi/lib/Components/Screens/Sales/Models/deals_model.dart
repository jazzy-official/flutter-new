import 'package:rafi_admin/Components/Screens/Sales/Models/deal_unit_model.dart';

class DealModel {
  List<DealUnit> dealUnits;
  String entryDate;
  String dealName;
  String affiliateName;
  double dealAmount;
  double dealSoldAmount;
  double receivedAmount;

  DealModel({
    required this.affiliateName,
    required this.entryDate,
    required this.dealAmount,
    required this.dealName,
    required this.dealSoldAmount,
    required this.dealUnits,
    required this.receivedAmount,
  });

  factory DealModel.fromJson(Map<String, dynamic> json) {
    return DealModel(
      dealUnits: List<DealUnit>.from(json["DealUnits"]
          .map(
            (dynamic item) => DealUnit.fromJson(item),
          )
          .toList()),
      affiliateName: json['AffiliateName'],
      receivedAmount: json['AmountReceived'],
      dealName: json['DealName'],
      dealSoldAmount:json['DealAmount'],
      entryDate: json['EntryDate'],
      dealAmount: json['TotalDealValue'],
    );
  }
}
