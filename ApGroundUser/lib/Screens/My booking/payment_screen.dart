import 'package:ap_booking_user/Screens/Shared/loading_widget.dart';
import 'package:ap_booking_user/Screens/drawer.dart';
import 'package:ap_booking_user/constants/util.dart';
import 'package:ap_booking_user/controllers/booking_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ionicons/ionicons.dart';

import 'Models/booking_model.dart';

class PaymentScreen extends StatefulWidget {
  final Booking booking;
  final int totalPrice;
  final int mealPrice;
  final bool alreadyBooked;
  final bool meal;
  final int slottype;
  const PaymentScreen(
      {Key? key, required this.booking, required this.totalPrice, required this.alreadyBooked,required this.mealPrice,required this.meal, required this.slottype})
      : super(key: key);

  @override
  State<PaymentScreen> createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {
  bool payfull = false;
  void refresh(){
  }
  void initState() {
    refresh();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.booking.groundName),
        centerTitle: true,
        backgroundColor: MyResources.buttonColor,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Card(
                margin: EdgeInsets.zero,
                elevation: 4,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              const Icon(
                                Ionicons.location_outline,
                                color: Colors.grey,
                              ),
                              const SizedBox(
                                width: 20,
                              ),
                              Text(
                                widget.booking.groundName,
                                style: const TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          Row(
                            children: [
                              const Icon(
                                Ionicons.calendar_outline,
                                color: Colors.grey,
                              ),
                              const SizedBox(
                                width: 20,
                              ),
                              Text(
                                DateFormat('dd-MMM-yyyy')
                                    .format(widget.booking.date),
                                style: const TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          Row(
                            children: [
                              const Icon(
                                Ionicons.time_outline,
                                color: Colors.grey,
                              ),
                              const SizedBox(
                                width: 20,
                              ),
                              Text(
                                DateFormat('h:mm a')
                                        .format(widget.booking.slot.startTime) +
                                    "-" +
                                    DateFormat('h:mm a').format(widget
                                        .booking.slot.startTime
                                        .add(Duration(
                                            hours: widget.booking.slot.hours))),
                                style: const TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              const Icon(
                                Ionicons.cash_outline,
                                color: Colors.grey,
                              ),
                              const SizedBox(
                                width: 20,
                              ),
                              Text(
                                (widget.totalPrice)
                                    .toString(),
                                style: const TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    "Ground Price/Slot",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Text("Rs/ " +
                      ((widget.booking.slot.price)).toString()),
                ],
              ),
              const Divider(
                color: Colors.grey,
                thickness: 0.5,
              ),
              if (widget.meal)
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children:  [
                    Text(
                      "Meal/Slot",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text("Rs/ "+ widget.mealPrice.toString()),
                  ],
                ),
              if (widget.meal)
                const Divider(
                  color: Colors.grey,
                  thickness: 0.5,
                ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Text(
                    "Wicket Price",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Text("Included"),
                ],
              ),
              const Divider(
                color: Colors.grey,
                thickness: 0.5,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Text(
                    "Umpire Price",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Text("Included"),
                ],
              ),
              const Divider(
                color: Colors.grey,
                thickness: 0.5,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 15.0),
                child: Card(
                  color: Colors.green.shade50,
                  margin: EdgeInsets.zero,
                  child: Padding(
                    padding: const EdgeInsets.all(8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          "Total",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text(
                          "Rs/ " + widget.totalPrice.toString(),
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text("Pay Full"),
                  Transform.scale(
                    scale: 0.7,
                    child: CupertinoSwitch(
                      value: payfull,
                      onChanged: (bool value) {
                        setState(() {
                          payfull = value;
                        });
                      },
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: (Column(
                        children: [
                          const Text("Advance Payable"),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            "Rs. " +
                                (payfull
                                        ? widget.totalPrice
                                        : (widget.totalPrice * 0.25).toInt())
                                    .toString() +
                                "/-",
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          )
                        ],
                      )),
                    ),
                  )
                ],
              ),
              const Divider(
                color: Colors.grey,
                thickness: 1,
              ),
              const Padding(
                padding: EdgeInsets.symmetric(vertical: 10.0),
                child: Text(
                  "Booking Policies",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontSize: 20,
                      fontWeight: FontWeight.w600),
                ),
              ),
              const Padding(
                padding: EdgeInsets.symmetric(vertical: 8.0),
                child: Text(
                  "Rescheduling Policy",
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
              ),
              const Text(
                "Rescheduling is not allowed by the club.",
                style: TextStyle(color: Colors.grey),
              ),
              const Padding(
                padding: EdgeInsets.symmetric(vertical: 8.0),
                child: Text(
                  "Cancel Policy",
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
              ),
              const Text(
                """0-24 hrs prior to slot: Cancellations not allowed.
> 24 hrs prior to slot: 10% of Gross amount will be
deducted as cancellation fee""",
                style: TextStyle(color: Colors.grey),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 15),
                child: Row(
                  children: [
                    const Text(
                      "By Continuing, you agree to our",
                      style: TextStyle(color: Colors.grey),
                    ),
                    TextButton(
                        onPressed: () {}, child: const Text("terms of service"))
                  ],
                ),
              ),
              Center(
                child: ElevatedButton(
                  onPressed: () async {
                    showDialog<void>(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext dialogContext) {
                        return
                          LoadingWidget();

                      },
                    );
                    if(widget.alreadyBooked){
                      setState(() {
                      widget.booking.slot.Pslot2!.paid = payfull
                          ? (widget.totalPrice).toDouble()
                          : widget.totalPrice * 0.25;
                    });
                      await BookingController().updateBooking(widget.booking);
                    }
                    else{
                      if(widget.slottype==2) {
                        setState(() {
                          widget.booking.slot.Pslot1!.paid = payfull
                              ? (widget.totalPrice).toDouble() / 2
                              : (widget.totalPrice * 0.25) / 2;
                        });
                      }else{
                        setState(() {
                        widget.booking.slot.Pslot1!.paid = payfull
                            ? (widget.totalPrice).toDouble()
                            : widget.totalPrice * 0.25;
                      });
                      }
                      await BookingController().saveBooking(widget.booking);
                    }
                    Navigator.pushReplacement<void, void>(
                      context,
                      MaterialPageRoute<void>(
                        builder: (BuildContext context) => const NavBar(),
                      ),
                    );
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Rs." +
                          (payfull
                              ? widget.totalPrice.toString()
                              : ((widget.totalPrice * 0.25).toInt())
                                  .toString()) +
                          "/-"),
                      Wrap(
                        children: const [
                          Text("Pay "),
                          Icon(
                            Ionicons.chevron_forward_outline,
                            size: 21,
                          )
                        ],
                      ),
                    ],
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: MyResources.buttonColor,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10, vertical: 10),
                      textStyle: const TextStyle(
                          fontSize: 18, fontWeight: FontWeight.bold)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
