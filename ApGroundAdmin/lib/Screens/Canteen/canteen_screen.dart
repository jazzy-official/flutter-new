import 'package:ap_ground_admin/Screens/Canteen/Inventory/inventory_screen.dart';
import 'package:ap_ground_admin/constants/util.dart';
import 'package:flutter/material.dart';

class CanteenScreen extends StatelessWidget {
  const CanteenScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height,
      child: DefaultTabController(
          length: 3, // length of tabs
          initialIndex: 0,
          child: Scaffold(
            appBar: AppBar(
              toolbarHeight: 0,
              bottom:  PreferredSize(
                preferredSize: Size(double.infinity,40),
                child: ColoredBox(
                  color: Colors.white,
                  child: SizedBox(
                    height: 40,
                    child: TabBar(
                      indicator: BoxDecoration(
                          borderRadius:
                          BorderRadius.circular(0),
                          color: MyResources.buttonColor),
                      labelColor: Colors.white,
                      unselectedLabelColor: Colors.black,
                      tabs: const [
                        Tab(child: Text("Inventory"),),
                        Tab(child: Text("Meals"),),
                        Tab(child: Text("Report"),),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            body: TabBarView(
              children: [
                InventoryScreen(),
                Icon(Icons.directions_bike),
                Icon(Icons.directions_bike),
              ],
            ),
          ),),
    );
  }
}

