import 'package:buttons_tabbar/buttons_tabbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sliding_up_panel/sliding_up_panel_widget.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Screens/Home/Widgets/map_view_page.dart';
import 'package:room_buddy/Screens/Home/room_feed.dart';
import 'package:room_buddy/Screens/Home/roomate_feed.dart';

import '../../controllers/explore_controller.dart';
import 'Filters/room_filter.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {

  late TabController _controller;
  SlidingUpPanelController filterpanelController = SlidingUpPanelController();
  SlidingUpPanelController mappanelController = SlidingUpPanelController();

  @override
  void initState() {

    _controller = TabController(length: 2, vsync: this);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
          body: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Material(
                  elevation: 3,
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        InkWell(
                          onTap: (){
                            Get.to(const MapViewPage());
                          },
                          child: Row(
                            children: const [
                              Text("Faisalabad, Punjab  ",style: TextStyle(fontSize: 17,fontWeight: FontWeight.bold),),
                              Icon(Ionicons.chevron_down,size: 18,)
                            ],
                          ),
                        ),
                        // const Padding(
                        //   padding: EdgeInsets.symmetric(vertical: 8.0),
                        //   child: Text("I'm looking for a",style: TextStyle(color: Colors.black45,fontWeight: FontWeight.w500),),
                        // ),
                        // ButtonsTabBar(
                        //   controller: _controller,
                        //   contentPadding: const EdgeInsets.symmetric(horizontal: 25),
                        //   radius: 5,
                        //   backgroundColor: myResources.themeColor,
                        //   unselectedBackgroundColor: Colors.grey[200],
                        //   unselectedLabelStyle: TextStyle(color: Colors.black),
                        //   labelStyle:
                        //   const TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                        //   tabs:  const [
                        //     Tab(
                        //       text: "Room",
                        //     ),
                        //     Tab(
                        //       text: "Roommate",
                        //     ),
                        //   ],
                        // ),
                        Padding(
                          //Add padding around textfield
                          padding: const EdgeInsets.only(top: 15.0, bottom: 0),
                          child: Row(
                            children: [
                              Expanded(
                                child: SizedBox(
                                  height: 35,
                                  child: TextField(
                                    keyboardType: TextInputType.name,
                                    onChanged: (val) {},
                                    decoration:  InputDecoration(
                                      //Add th Hint text here.
                                      fillColor: Colors.grey[200],
                                      filled: true,
                                      hintText: "Search by locality or landmark",
                                      hintStyle: const TextStyle(color: Colors.black38,fontSize: 13,fontWeight: FontWeight.w500),
                                      prefixIcon: const Icon(Ionicons.search,color: Colors.black45,size: 20,),
                                      contentPadding: const EdgeInsets.only(top: 0, left: 5),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(3),
                                        borderSide: const BorderSide(
                                          width: 0,
                                          style: BorderStyle.none,
                                        ),),
                                      // OutlineInputBorder(
                                      //   borderRadius: BorderRadius.circular(3.0),
                                      // ),
                                    ),
                                  ),
                                ),
                              ),
                              TextButton(
                                  onPressed: () {
                                    filterpanelController.expand();
                                  },
                                  child: Row(
                                    children: [
                                      Icon(
                                        Ionicons.options,
                                        color: Colors.black45,
                                      ),
                                      Text(
                                        " Filters",
                                        style: TextStyle(color: Colors.black45),
                                      )
                                    ],
                                  )
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: RoomFeed(),
                  // TabBarView(
                  //   controller: _controller,
                  //   children: const <Widget>[
                  //     RoomFeed(),
                  //     RoommateFeed(),
                  //   ],
                  // ),
                ),
              ],
            ),
          ),
        ),
        SlidingUpPanelWidget(child: RoomFilters(panelController: filterpanelController,), controlHeight: 0, panelController: filterpanelController,enableOnTap: false,),
      ],
    );
  }
}
