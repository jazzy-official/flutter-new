import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:ionicons/ionicons.dart';
import 'package:room_buddy/Constants/Widgets/info_widget.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Screens/User%20Forms/setup_profile_form.dart';

class SignupFormThree extends StatefulWidget {
  const SignupFormThree({Key? key}) : super(key: key);

  @override
  State<SignupFormThree> createState() => _SignupFormThreeState();
}

class _SignupFormThreeState extends State<SignupFormThree> {
  String dropdownValue = 'ID Card';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: myResources.background,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: myResources.background,
        title: Text(
          "Verify Your Identity",
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 20.0),
              child: Text(
                "Select identity proof",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              ),
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(3)),
                    side: BorderSide(color: myResources.themeColor)),
              ),
              onPressed: () {
                // Get.to(()=> const SignupFormTwo());
              },
              child: Container(
                padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 8),
                height: 35,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    DropdownButton<String>(
                      value: dropdownValue,
                      icon: Icon(
                        CupertinoIcons.chevron_down,
                        size: 18,
                        color: myResources.themeColor,
                      ),
                      style: TextStyle(color: myResources.themeColor),
                      underline: Container(
                        height: 0,
                        color: Colors.transparent,
                      ),
                      onChanged: (String? newValue) {
                        setState(() {
                          dropdownValue = newValue!;
                        });
                      },
                      items: <String>[
                        'ID Card',
                        'Passport',
                        'Driving License',
                      ].map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(height: 25),
            Text(
              "Enter " + dropdownValue + " Number",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
            ),
            Padding(
              //Add padding around textfield
              padding: const EdgeInsets.only(top: 20.0, bottom: 30),
              child: SizedBox(
                height: 35,
                child: TextField(
                  keyboardType: TextInputType.name,
                  onChanged: (val) {},
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    hintText: "xxxxx-xxxxxxx-x",
                    contentPadding: const EdgeInsets.only(top: 10, left: 15),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(3.0),
                    ),
                  ),
                ),
              ),
            ),
            InfoWidget(text: "Verified profile attracts potential roommates make sure you upload an identity proof"),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: myResources.themeColor,
                      shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(14))),
                    ),
                    onPressed: () {
                      Get.to(()=> const SignupFormFour());
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8, horizontal: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            'Confirm',
                            style: TextStyle(color: Colors.white),
                          ),
                          Container(
                            padding: const EdgeInsets.all(8),
                            decoration: BoxDecoration(
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(20)),
                              color: myResources.themeColor,
                            ),
                            child: Icon(
                              Icons.arrow_forward_ios,
                              color: Colors.white,
                              size: 16,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(width: 5,)
          ],
        ),
      ),
    );
  }
}

