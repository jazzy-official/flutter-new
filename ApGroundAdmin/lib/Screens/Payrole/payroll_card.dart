import 'package:ap_ground_admin/Screens/Payrole/models/payroll_model.dart';
import 'package:ap_ground_admin/Screens/Staff/Models/staff_model.dart';
import 'package:ap_ground_admin/constants/util.dart';
import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:ap_ground_admin/controllers/staff_controller.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ionicons/ionicons.dart';

import '../delete_popup.dart';

class PayrollCard extends StatefulWidget {
  final Staff staff;
  final Function() refresh;
  const PayrollCard({Key? key, required this.staff, required this.refresh})
      : super(key: key);

  @override
  State<PayrollCard> createState() => _PayrollCardState();
}

class _PayrollCardState extends State<PayrollCard> {
  void checkDate() async {
    if (widget.staff.payroll.date
        .add(const Duration(days: 30))
        .isBefore(DateTime.now())) {
      await StaffController().updateStaff(Staff(
          id: widget.staff.id,
          designation: widget.staff.designation,
          name: widget.staff.name,
          number: widget.staff.number,
          imgUrl: widget.staff.imgUrl,
          payroll: Payroll(
              date: widget.staff.payroll.date,
              salary: widget.staff.payroll.salary,
              isPaid: false)));
      widget.refresh;
    }
  }

  @override
  void initState() {
    checkDate();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          widget.staff.name,
                          style: const TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w600),
                        ),
                      ),
                      Text(
                        "(" + widget.staff.designation + ")",
                        style: const TextStyle(fontSize: 12),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Row(
                    children: [
                      const Text(
                        "Salary :",
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Text(formatnmbr(widget.staff.payroll.salary)),
                    ],
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: Row(
                          children: [
                            const Text(
                              "Status :",
                              style: TextStyle(fontWeight: FontWeight.w600),
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            Text(
                              widget.staff.payroll.isPaid ? "Paid" : "Unpaid",
                              style: TextStyle(
                                  color: !widget.staff.payroll.isPaid
                                      ? Colors.red
                                      : Colors.green),
                            ),
                          ],
                        ),
                      ),
                      Switch(
                          activeColor: Colors.green,
                          value: widget.staff.payroll.isPaid,
                          onChanged: widget.staff.payroll.isPaid
                              ? null
                              : (val) async {
                            showDialog(
                                context: context,
                                builder: (_) => AlertDialog(
                                  content: DeletepopUp(message: "Confirm Payment?", pressFunc: () async {
                                    await StaffController().updateStaff(Staff(
                                        id: widget.staff.id,
                                        designation: widget.staff.designation,
                                        name: widget.staff.name,
                                        number: widget.staff.number,
                                        imgUrl: widget.staff.imgUrl,
                                        payroll: Payroll(
                                            date: DateTime.now(),
                                            salary: widget.staff.payroll.salary,
                                            isPaid: true)));
                                    displayToast(widget.staff.name + "'s salary paid ");
                                    Navigator.pop(context);
                                    widget.refresh();
                                  },isNotDel: true,),
                                )
                            );


                                })
                    ],
                  ),
                  Row(
                    children: [
                      const Text(
                        "Last Paid :",
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Text(DateFormat('dd-MMM-yyyy')
                          .format(widget.staff.payroll.date)),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
