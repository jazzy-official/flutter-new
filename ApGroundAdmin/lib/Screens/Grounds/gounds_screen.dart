import 'package:ap_ground_admin/Screens/Grounds/Models/ground_models.dart';
import 'package:ap_ground_admin/Screens/Grounds/add_ground.dart';
import 'package:ap_ground_admin/Screens/Grounds/ground_detail.dart';
import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:ap_ground_admin/loading_widget.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ap_ground_admin/controllers/ground_database_controller.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import '../delete_popup.dart';

class GroundScreen extends StatefulWidget {
  const GroundScreen({Key? key}) : super(key: key);

  @override
  State<GroundScreen> createState() => _GroundScreenState();
}

class _GroundScreenState extends State<GroundScreen> {
  final DatabaseReference databaseReference =
  FirebaseDatabase.instance.reference().child("Grounds");



  late List<Ground> grounds;
  bool isLoading = true;
  void refresh() async {
    setState(() {
      isLoading = true;
    });
    grounds = await GroundController().readData();
    setState(() {
      isLoading = false;
    });
  }

  onGoBack(dynamic value) {
    refresh();
    setState(() {});
  }

  @override
  void initState() {
    refresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddGroundForm()),
                ).then((value) => refresh());
              },
              child: Text("Add"),
              style: ElevatedButton.styleFrom(primary: Color(0xff1758B4)),
            ),
          ),
          Expanded(
            child: isLoading
                ? LoadingWidget()
                : ListView.builder(
                padding: const EdgeInsets.all(8),
                itemCount: grounds.length,
                itemBuilder: (BuildContext context, int index) {
                  Ground ground = grounds[index];
                  return InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GroundDetail(
                              ground: ground,
                            )),
                      ).then((value) => refresh());
                    },
                    child:Slidable(
                      actionPane: SlidableDrawerActionPane(),
                      actionExtentRatio: 0.15,
                      child: Container(
                        color: Colors.white,
                        child: Card(
                          child: Column(
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: CircleAvatar(
                                      backgroundImage: ground.imgUrls[0] == "" ? AssetImage('assets/stadiumlogo.png') as ImageProvider :
                                      NetworkImage(ground.imgUrls[0]),
                                      radius: 30,
                                    ),
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            ground.name.toUpperCase(),
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(5.0),
                                          child: Text(ground.address),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding:
                                    const EdgeInsets.only(top: 8.0, right: 8),
                                    child: Text("Slots : 2"),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                      secondaryActions: <Widget>[
                        Container(
                          padding:EdgeInsets.symmetric(vertical: 5),
                          child: IconSlideAction(
                            caption: 'Edit',
                            color: Colors.indigo,
                            icon: Icons.edit,
                            onTap: () async {
                              try{
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddGroundForm(
                                        ground: ground,
                                      )),
                                );
                              }catch(e){
                                displayToast(e.toString());
                              }
                              refresh();

                            },
                          ),
                        ),
                        Container(
                          padding:EdgeInsets.symmetric(vertical: 5),
                          child: IconSlideAction(
                            caption: 'Delete',
                            color: Colors.red,
                            icon: Icons.delete,
                            onTap: () async {
                              try{
                                showDialog(
                                    context: context,
                                    builder: (_) => AlertDialog(
                                      content: DeletepopUp(message: "Are you sure You wanna delete "+ground.name+"?", pressFunc: () async {
                                        await GroundController().deleteData(ground.id!);
                                        Navigator.pop(context);
                                        displayToast(ground.name+" deleted successfully");
                                        refresh();
                                      },),
                                    )
                                );
                              }catch(e){
                                displayToast(e.toString());
                              }
                              refresh();

                            },
                          ),
                        ),
                      ],
                    ),


                  );
                }),
          ),
        ],
      ),
    );
  }
}