import 'package:daily_log_user/Controllers/auth_controller.dart';
import 'package:daily_log_user/View/Login/login_screen.dart';
import 'package:daily_log_user/constants/util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SignUpScreen extends StatelessWidget {
   SignUpScreen({Key? key}) : super(key: key);


  TextEditingController namecontroller = TextEditingController();
  TextEditingController emailcontroller = TextEditingController();
  TextEditingController passcontroller = TextEditingController();
  TextEditingController confirmpasscontroller = TextEditingController();
   var authcontroller = Get.put(AuthController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   leading: const BackButton(color: Colors.black),
      //   backgroundColor: Colors.white,
      //   title: const Text("SIGN UP"),
      //   titleTextStyle: const TextStyle(
      //       color: Colors.black, fontSize: 18, fontWeight: FontWeight.w600),
      //   centerTitle: true,
      // ),
      resizeToAvoidBottomInset: false,
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 60.0,bottom: 30),
            child: Image.asset(
              "assets/logo2.png",
              height: MediaQuery.of(context).size.height * 0.25,
            ),
          ),
          Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                      child: TextField(
                        controller: namecontroller,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            hintText: 'Full Name',
                            contentPadding: EdgeInsets.symmetric(horizontal: 10)),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                      child: TextField(
                        controller: emailcontroller,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            hintText: 'Email',
                            contentPadding: EdgeInsets.symmetric(horizontal: 10)),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                      child: TextField(
                        controller: passcontroller,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          hintText: 'Password',
                          contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                      child: TextField(
                        controller: confirmpasscontroller,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          hintText: 'Confirm Password',
                          contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        ),
                      ),
                    ),
                    SizedBox(height: 20,),
                    TextButton(
                        onPressed: () async {
                          if (emailcontroller.text == '') {
                            Get.snackbar("Alert", "Please enter email",snackPosition: SnackPosition.BOTTOM);
                          } else if (passcontroller.text == '') {
                            Get.snackbar("Alert", "Please enter password",snackPosition: SnackPosition.BOTTOM);
                          } else if (!emailcontroller.text.contains("@")) {
                            Get.snackbar("Alert", "Please enter a valid email address",snackPosition: SnackPosition.BOTTOM);

                          } else if(passcontroller.text!=confirmpasscontroller.text){
                            Get.snackbar("Alert", "Password do no match",snackPosition: SnackPosition.BOTTOM);
                          }else {
                            try {
                              await authcontroller.signUp(emailcontroller.text, passcontroller.text,namecontroller.text );
                            } catch (e) {
                              Get.snackbar("Error", e.toString(),snackPosition: SnackPosition.BOTTOM);

                            }
                          }
                        },
                        child: Container(
                          height: 50,
                          width: MediaQuery.of(context).size.width - 30,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: MyResources.themecolor,
                          ),
                          child: const Center(
                              child: Text(
                                "Sign up",
                                style: TextStyle(color: Colors.white, fontSize: 17),
                              )),
                        )),
                    // Padding(
                    //   padding: const EdgeInsets.all(20.0),
                    //   child: Text(
                    //     "Or Login With",
                    //     style: TextStyle(color: Colors.grey, fontSize: 16),
                    //   ),
                    // ),
                    // Row(
                    //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    //   children: [
                    //     TextButton(
                    //       onPressed: () {},
                    //       child: Container(
                    //           height: 45,
                    //           decoration: BoxDecoration(
                    //               color: Color(0xfff8f2f1),
                    //               borderRadius: BorderRadius.circular(10),
                    //               border: Border.all(
                    //                   color: Color(0xffea4335), width: 1)),
                    //           padding: EdgeInsets.symmetric(
                    //               horizontal: 10, vertical: 10),
                    //           child: Row(
                    //             children: [
                    //               // Icon(
                    //               //   // Ionicons.logo_google,
                    //               //   color: Color(0xffea4335),
                    //               // ),
                    //               SizedBox(
                    //                 width: 15,
                    //               ),
                    //               Text(
                    //                 "Google",
                    //                 style: TextStyle(color: Color(0xffea4335)),
                    //               ),
                    //               SizedBox(
                    //                 width: 15,
                    //               ),
                    //             ],
                    //           )),
                    //     ),
                    //     Container(
                    //       width: 2,
                    //       height: 50,
                    //       color: Colors.grey.shade400,
                    //     ),
                    //     TextButton(
                    //       onPressed: () {},
                    //       child: Container(
                    //           decoration: BoxDecoration(
                    //               color: Color(0xfff1f2f8),
                    //               borderRadius: BorderRadius.circular(10),
                    //               border: Border.all(color: Colors.blue, width: 1)),
                    //           padding: EdgeInsets.symmetric(
                    //               horizontal: 10, vertical: 10),
                    //           child: Row(
                    //             children: [
                    //               // Icon(Ionicons.logo_facebook),
                    //               SizedBox(
                    //                 width: 10,
                    //               ),
                    //               Text("Facebook")
                    //             ],
                    //           )),
                    //     ),
                    //   ],
                    // ),
                  ],
                ),
              )),
          Container(
            height: 45,
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(
                  //                    <--- top side
                  color: Colors.grey,
                  width: 1.0,
                ),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  "Existing User?",
                  style: TextStyle(color: Colors.grey),
                ),
                TextButton(
                    onPressed: () {
                      Get.to( LoginScreen());
                    },
                    child: Text("Sign in"))
              ],
            ),
          )
        ],
      ),
    );
  }
}
