class Account {
  double cashInHand;
  double banks;
  double receivable;
  double payables;

  Account({
    required this.cashInHand,
    required this.banks,
    required this.receivable,
    required this.payables,
  });

  factory Account.fromJson(Map<String, dynamic> json) {
    return Account(
      cashInHand: json['Data']['CashInHand'] as double,
      banks: json['Data']['Banks'] as double,
      receivable: json['Data']['Receivable'] as double,
      payables: json['Data']['Payables'] as double,
    );
  }
}
