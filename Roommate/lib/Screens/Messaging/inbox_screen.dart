import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:get/get.dart';
import 'package:room_buddy/Screens/Messaging/chat_widget.dart';

class InboxScreen extends StatelessWidget {
  const InboxScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
        toolbarHeight: 70,
        leadingWidth: 30,
        title: Row(
          children: [
            const CircleAvatar(
              radius: 25,
              backgroundImage: AssetImage("assets/me.jpg"),
            ),
            const SizedBox(width: 15,),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Muhammad Jahanzaib",style: TextStyle(color: Colors.black,fontSize: 16),),
                SizedBox(height: 2,),
                Text("Last online 6 hours ago",style: TextStyle(color: Colors.grey.shade500,fontSize: 12),),
              ],
            ),
          ],
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: InkWell(
              onTap: (){},
                child:  Icon(Zocial.call)
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: InkWell(
              onTap: (){},
                child: const Icon(Ionicons.ellipsis_vertical)
            ),
          ),
        ],
        // leading: Text("leading"),
      ),
      body: Column(
        children:  [
          Expanded(
              child: ChatWidget()
          ),
        ],
      ),
    );
  }
}
