import 'package:daily_log_user/Models/sub_module.dart';
import 'package:daily_log_user/Models/task_model.dart';
import 'package:daily_log_user/Models/todo_model.dart';
import 'package:daily_log_user/constants/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../Controllers/task_controller.dart';

class TaskDetailScreen extends StatefulWidget {
  final TaskModel task;
  const TaskDetailScreen({Key? key, required this.task}) : super(key: key);

  @override
  State<TaskDetailScreen> createState() => _TaskDetailScreenState();
}

class _TaskDetailScreenState extends State<TaskDetailScreen> {
  var controller = Get.put(TaskController());
  TextEditingController titlecont = TextEditingController();
  TextEditingController descont = TextEditingController();
  SubModule? subModule;
  @override
  void initState() {
    getdata();
    super.initState();
  }

  Future<void> getdata() async {
    if(widget.task.submoduleid!=null){
      SubModule? temp = await controller.getSubModule( widget.task.projectid!, widget.task.moduleid!, widget.task.submoduleid!);
      setState(() {
        subModule = temp;
      });

    }
  }
  @override
  Widget build(BuildContext context) {
    void _showDialog(Widget child) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              content: child,
            );
          });
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.task.name),
        backgroundColor: MyResources.themecolor,
      ),
      floatingActionButton: InkWell(
        onTap: () =>
        {
          _showDialog(
              SizedBox(
                height: 220,
                child: Column(
                  children: [
                    TextFormField(
                      controller: titlecont,
                      decoration: InputDecoration(
                        labelText: "Title",
                      ),
                    ),
                    TextFormField(
                      controller: descont,
                      maxLines: 2,
                      decoration: InputDecoration(
                        labelText: "Description",
                      ),
                    ),
                    Row(
                      children: [
                        TextButton(onPressed: () {
                          widget.task.todos!.add(Todo(des: descont.text, status: false, name: titlecont.text));
                          controller.updateTask(widget.task);
                          controller.readBall();
                          Get.back();

                        }, child: Text("Save")),
                        TextButton(onPressed: () {
                          Get.back();
                        }, child: Text("cancel")),
                      ],
                    )
                  ],
                ),
              )),
        },
        child: Container(
          width: 100,
            height: 40,
            decoration: BoxDecoration(
              color: MyResources.themecolor,
            borderRadius: BorderRadius.circular(10)),

            child: Center(child: Text("Add a todo",style: TextStyle(color: Colors.white,fontWeight: FontWeight.w600,fontSize: 15),))),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0,vertical: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                const SizedBox(
                    width: 80,
                    child: Text(
                      "Title : ",
                      style: TextStyle(fontWeight: FontWeight.w600),
                    )),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.task.name,
                      ),
                    ],
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                const SizedBox(
                    width: 80,
                    child: Text(
                      "About : ",
                      style: TextStyle(fontWeight: FontWeight.w600),
                    )),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.task.des,
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Expanded(
                  child: Row(
                    children: [
                      Text(
                        "Deadline : ",
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),
                      Text(DateFormat("dd MMM yyyy")
                          .format(widget.task.deadline)),
                    ],
                  ),
                ),
                Expanded(
                  child: Row(
                    children: [
                      Text(
                        "Status : ",
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),
                      Text(
                        widget.task.status ? "Completed" : "Pending",
                        style: TextStyle(
                            color:
                                widget.task.status ? Colors.green : Colors.red),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            if(widget.task.project!=null)
            Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            "Project : ",
                            style: TextStyle(fontWeight: FontWeight.w600),
                          ),
                          Column(
                            children: [
                              SizedBox(
                                width:Get.width*0.5 - 65,
                                  child: Text(widget.task.project!)
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    if(widget.task.module != null)
                    Expanded(
                      child: Row(
                        children: [
                          Text(
                            "Module : ",
                            style: TextStyle(fontWeight: FontWeight.w600),
                          ),
                          Text(widget.task.module!),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                if(subModule != null)
                Row(
                  children: [
                    const Text(
                      "Sub-Module : ",
                      style: TextStyle(fontWeight: FontWeight.w600),
                    ),
                    Text(subModule!.name),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                if(subModule != null)
                  Row(
                    children: [
                      const Text(
                        "Document : ",
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        width: Get.width-120,
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: InkWell(

                              onTap: () => launch(subModule!.doc!),

                              child: Text(
                                subModule!.doc ?? "no document found",
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle(color: Colors.blue),
                              )),
                        ),
                      ),
                    ],
                  ),
              ],
            ),
            GetBuilder<TaskController>(
                autoRemove: false,
                init: TaskController(),
                builder: (cont) =>widget.task.todos==null?Container(): Expanded(

                      child: ListView.builder(
                        itemCount: widget.task.todos?.length,
                        itemBuilder: (context, index) {
                          Todo todo = widget.task.todos![index];
                          return Card(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 8.0,left: 8,right: 8),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(todo.name.toUpperCase(),style: TextStyle(fontWeight: FontWeight.w600),),
                                      SizedBox(
                                        height:20,
                                        child: Switch(
                                            value: todo.status,
                                            onChanged: (val) {
                                              setState(() {
                                                todo.status = !todo.status;
                                              });
                                              cont.updateTask(widget.task);
                                              cont.readBall();


                                            }),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(todo.des,style: TextStyle(fontSize: 12,color: Colors.grey),),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                    ))
          ],
        ),
      ),
    );
  }
}
