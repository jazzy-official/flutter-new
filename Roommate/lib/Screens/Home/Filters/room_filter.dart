import 'dart:ui';

import 'package:chips_choice_null_safety/chips_choice_null_safety.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sliding_up_panel/sliding_up_panel_widget.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/controllers/explore_controller.dart';

class RoomFilters extends StatefulWidget {
  final SlidingUpPanelController panelController;
  const RoomFilters({Key? key, required this.panelController})
      : super(key: key);

  @override
  State<RoomFilters> createState() => _RoomFiltersState();
}

class _RoomFiltersState extends State<RoomFilters> {
  List<String> options = [
    'WIFI',
    'Elevator',
    'TV',
    'Security',
    'CookTop',
    'Parking',
    'AC',
    'Geyser',
    'Balcony',
    'Maintenance',
    'Washing Machine',
    'Kitchen',
  ];

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ExplorerController>(
        init: ExplorerController(),
        builder: (cont) => Container(
              margin: EdgeInsets.only(top: 25.0, bottom: 50),
              decoration: ShapeDecoration(
                color: Colors.white,
                shadows: [
                  BoxShadow(
                      blurRadius: 5.0,
                      spreadRadius: 2.0,
                      color: const Color(0x11000000))
                ],
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10.0),
                    topRight: Radius.circular(10.0),
                  ),
                ),
              ),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 5),
                        decoration: BoxDecoration(
                            color: Colors.black26,
                            borderRadius: BorderRadius.circular(10)),
                        height: 5,
                        width: 40,
                      ),
                    ],
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Filters",
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: Colors.grey.shade600),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            left: 8.0,
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            widget.panelController.hide();
                          },
                          child: Text(
                            'Close',
                            style: TextStyle(color: Colors.grey),
                          ),
                        )
                      ],
                    ),
                    height: 50.0,
                  ),
                  Divider(
                    height: 2,
                    color: Colors.grey[300],
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15.0),
                      child: ListView(
                        children: [
                          SizedBox(
                            height: 25,
                          ),
                          // Text(
                          //   "Sort by",
                          //   style: TextStyle(
                          //       color: Colors.grey.shade600,
                          //       fontWeight: FontWeight.w500,
                          //       fontSize: 16),
                          // ),
                          // Row(
                          //   children: [
                          //     ElevatedButton(
                          //       style: ElevatedButton.styleFrom(
                          //         primary: Colors.white,
                          //         shape: RoundedRectangleBorder(
                          //             borderRadius:
                          //             BorderRadius.all(Radius.circular(3)),
                          //             side: BorderSide(color: Colors.grey.shade500)),
                          //       ),
                          //       onPressed: () {
                          //         // Get.to(()=> const SignupFormTwo());
                          //       },
                          //       child: Container(
                          //         padding: const EdgeInsets.symmetric(
                          //             vertical: 0, horizontal: 8),
                          //         height: 35,
                          //         child: DropdownButton<String>(
                          //           value: dropdownValue,
                          //           icon: Icon(
                          //             CupertinoIcons.chevron_down,
                          //             size: 18,
                          //             color: myResources.themeColor,
                          //           ),
                          //           style: TextStyle(color: Colors.black54),
                          //           underline: Container(
                          //             height: 0,
                          //             color: Colors.transparent,
                          //           ),
                          //           onChanged: (String? newValue) {
                          //             setState(() {
                          //               dropdownValue = newValue!;
                          //             });
                          //           },
                          //           items: <String>[
                          //             'Newly Posted',
                          //             'filter 1',
                          //             'filter 2',
                          //           ].map<DropdownMenuItem<String>>((String value) {
                          //             return DropdownMenuItem<String>(
                          //               value: value,
                          //               child: Text(value),
                          //             );
                          //           }).toList(),
                          //         ),
                          //       ),
                          //     ),
                          //   ],
                          // ),
                          // SizedBox(
                          //   height: 15,
                          // ),
                          // Divider(
                          //   height: 2,
                          //   color: Colors.grey[300],
                          // ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 20.0),
                            child: Text(
                              "Budget Range",
                              style: TextStyle(
                                  color: Colors.grey.shade600,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16),
                            ),
                          ),
                          Text(
                            "\$ 0 - \$ 1,00,000",
                            style: TextStyle(
                                color: Colors.grey.shade600,
                                fontWeight: FontWeight.w500,
                                fontSize: 16),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 20.0),
                            child: Slider(
                              value: cont.budget,
                              max: 100000,
                              divisions: 200,
                              label: cont.budget.round().toString(),
                              activeColor: myResources.semiColor,
                              onChanged: (double value) {
                                cont.budget = value;
                                cont.update();
                              },
                            ),
                          ),
                          Divider(
                            height: 2,
                            color: Colors.grey[300],
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 20.0),
                            child: Text(
                              "Room Type",
                              style: TextStyle(
                                  color: Colors.grey.shade600,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16),
                            ),
                          ),
                          Wrap(
                            direction: Axis.horizontal,
                            children: [
                              Container(
                                width: 145,
                                margin: EdgeInsets.symmetric(horizontal: 5),
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: const BorderRadius.all(
                                            Radius.circular(20)),
                                        side: BorderSide(
                                            color:
                                                cont.roomtype == "Private Room"
                                                    ? myResources.themeColor
                                                    : Colors.black54)),
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      cont.roomtype = "Private Room";
                                    });
                                  },
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8, horizontal: 12),
                                    child: Center(
                                        child: Text(
                                      "Private Room",
                                      style: TextStyle(
                                          color: cont.roomtype == "Private Room"
                                              ? myResources.themeColor
                                              : Colors.black54),
                                    )),
                                  ),
                                ),
                              ),
                              Container(
                                width: 145,
                                margin: EdgeInsets.symmetric(horizontal: 5),
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20)),
                                        side: BorderSide(
                                            color:
                                                cont.roomtype == "Shared Room"
                                                    ? myResources.themeColor
                                                    : Colors.black54)),
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      cont.roomtype = "Shared Room";
                                    });
                                  },
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8, horizontal: 12),
                                    child: Center(
                                        child: Text(
                                      'Shared Room',
                                      style: TextStyle(
                                          color: cont.roomtype == "Shared Room"
                                              ? myResources.themeColor
                                              : Colors.black54),
                                    )),
                                  ),
                                ),
                              ),
                              Container(
                                width: 135,
                                margin: EdgeInsets.symmetric(horizontal: 5),
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20)),
                                        side: BorderSide(
                                            color:
                                                cont.roomtype == "Entire Place"
                                                    ? myResources.themeColor
                                                    : Colors.black54)),
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      cont.roomtype = "Entire Place";
                                    });
                                  },
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8, horizontal: 12),
                                    child: Center(
                                        child: Text(
                                      'Entire Place',
                                      style: TextStyle(
                                          color: cont.roomtype == "Entire Place"
                                              ? myResources.themeColor
                                              : Colors.black54),
                                    )),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Divider(
                            height: 2,
                            color: Colors.grey[300],
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 20.0),
                            child: Text(
                              "cont.roomNo",
                              style: TextStyle(
                                  color: Colors.grey.shade600,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16),
                            ),
                          ),
                          Wrap(
                            direction: Axis.horizontal,
                            children: [
                              Container(
                                width: 45,
                                margin: EdgeInsets.symmetric(horizontal: 5),
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: const BorderRadius.all(
                                            Radius.circular(20)),
                                        side: BorderSide(
                                            color: cont.roomNo == 1
                                                ? myResources.themeColor
                                                : Colors.black54)),
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      cont.roomNo = 1;
                                    });
                                  },
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8, horizontal: 0),
                                    child: Center(
                                        child: Text(
                                      "1",
                                      style: TextStyle(
                                          color: cont.roomNo == 1
                                              ? myResources.themeColor
                                              : Colors.black54),
                                    )),
                                  ),
                                ),
                              ),
                              Container(
                                width: 45,
                                margin: EdgeInsets.symmetric(horizontal: 5),
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20)),
                                        side: BorderSide(
                                            color: cont.roomNo == 2
                                                ? myResources.themeColor
                                                : Colors.black54)),
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      cont.roomNo = 2;
                                    });
                                  },
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8, horizontal: 0),
                                    child: Center(
                                        child: Text(
                                      '2',
                                      style: TextStyle(
                                          color: cont.roomNo == 2
                                              ? myResources.themeColor
                                              : Colors.black54),
                                    )),
                                  ),
                                ),
                              ),
                              Container(
                                width: 45,
                                margin: EdgeInsets.symmetric(horizontal: 5),
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20)),
                                        side: BorderSide(
                                            color: cont.roomNo == 3
                                                ? myResources.themeColor
                                                : Colors.black54)),
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      cont.roomNo = 3;
                                    });
                                  },
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8, horizontal: 0),
                                    child: Center(
                                        child: Text(
                                      '3',
                                      style: TextStyle(
                                          color: cont.roomNo == 3
                                              ? myResources.themeColor
                                              : Colors.black54),
                                    )),
                                  ),
                                ),
                              ),
                              Container(
                                width: 45,
                                margin: EdgeInsets.symmetric(horizontal: 5),
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20)),
                                        side: BorderSide(
                                            color: cont.roomNo == 4
                                                ? myResources.themeColor
                                                : Colors.black54)),
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      cont.roomNo = 4;
                                    });
                                  },
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8, horizontal: 0),
                                    child: Center(
                                        child: Text(
                                      '4',
                                      style: TextStyle(
                                          color: cont.roomNo == 4
                                              ? myResources.themeColor
                                              : Colors.black54),
                                    )),
                                  ),
                                ),
                              ),
                              Container(
                                width: 60,
                                margin: EdgeInsets.symmetric(horizontal: 5),
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20)),
                                        side: BorderSide(
                                            color: cont.roomNo == 5
                                                ? myResources.themeColor
                                                : Colors.black54)),
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      cont.roomNo = 5;
                                    });
                                  },
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8, horizontal: 0),
                                    child: Center(
                                        child: Text(
                                      '4 +',
                                      style: TextStyle(
                                          color: cont.roomNo == 5
                                              ? myResources.themeColor
                                              : Colors.black54),
                                    )),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Divider(
                            height: 2,
                            color: Colors.grey[300],
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 20.0),
                            child: Text(
                              "Bathroom Type",
                              style: TextStyle(
                                  color: Colors.grey.shade600,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16),
                            ),
                          ),
                          Wrap(
                            direction: Axis.horizontal,
                            children: [
                              Container(
                                width: 145,
                                margin: EdgeInsets.symmetric(horizontal: 5),
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: const BorderRadius.all(
                                            Radius.circular(20)),
                                        side: BorderSide(
                                            color: cont.bathroomType == "Attach"
                                                ? myResources.themeColor
                                                : Colors.black54)),
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      cont.bathroomType = "Attach";
                                    });
                                  },
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8, horizontal: 12),
                                    child: Center(
                                        child: Text(
                                      "Attach",
                                      style: TextStyle(
                                          color: cont.bathroomType == "Attach"
                                              ? myResources.themeColor
                                              : Colors.black54),
                                    )),
                                  ),
                                ),
                              ),
                              Container(
                                width: 145,
                                margin: EdgeInsets.symmetric(horizontal: 5),
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20)),
                                        side: BorderSide(
                                            color:
                                                cont.bathroomType == "Separate"
                                                    ? myResources.themeColor
                                                    : Colors.black54)),
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      cont.bathroomType = "Separate";
                                    });
                                  },
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8, horizontal: 12),
                                    child: Center(
                                        child: Text(
                                      'Separate',
                                      style: TextStyle(
                                          color: cont.bathroomType == "Separate"
                                              ? myResources.themeColor
                                              : Colors.black54),
                                    )),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Divider(
                            height: 2,
                            color: Colors.grey[300],
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 20.0),
                            child: Text(
                              "Roommate Gender",
                              style: TextStyle(
                                  color: Colors.grey.shade600,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16),
                            ),
                          ),
                          Wrap(
                            direction: Axis.horizontal,
                            children: [
                              Container(
                                width: 145,
                                margin: EdgeInsets.symmetric(horizontal: 5),
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: const BorderRadius.all(
                                            Radius.circular(20)),
                                        side: BorderSide(
                                            color: cont.roommateGender == "Male"
                                                ? myResources.themeColor
                                                : Colors.black54)),
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      cont.roommateGender = "Male";
                                    });
                                  },
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8, horizontal: 12),
                                    child: Center(
                                        child: Text(
                                      "Male",
                                      style: TextStyle(
                                          color: cont.roommateGender == "Male"
                                              ? myResources.themeColor
                                              : Colors.black54),
                                    )),
                                  ),
                                ),
                              ),
                              Container(
                                width: 145,
                                margin: EdgeInsets.symmetric(horizontal: 5),
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20)),
                                        side: BorderSide(
                                            color:
                                                cont.roommateGender == "Female"
                                                    ? myResources.themeColor
                                                    : Colors.black54)),
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      cont.roommateGender = "Female";
                                    });
                                  },
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8, horizontal: 12),
                                    child: Center(
                                        child: Text(
                                      'Female',
                                      style: TextStyle(
                                          color: cont.roommateGender == "Female"
                                              ? myResources.themeColor
                                              : Colors.black54),
                                    )),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Divider(
                            height: 2,
                            color: Colors.grey[300],
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 20.0),
                            child: Text(
                              "Amentities",
                              style: TextStyle(
                                  color: Colors.grey.shade600,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16),
                            ),
                          ),
                          ChipsChoice<String>.multiple(
                            value: cont.amenities,
                            wrapped: true,
                            onChanged: (val) =>
                                setState(() => cont.amenities = val),
                            choiceItems: C2Choice.listFrom<String, String>(
                              source: options,
                              value: (i, v) => v,
                              label: (i, v) => v,
                            ),
                            choiceStyle: C2ChoiceStyle(
                              elevation: 2,
                              color: myResources.themeColor,
                            ),
                            choiceActiveStyle: C2ChoiceStyle(
                              color: myResources.themeColor,
                              brightness: Brightness.dark,
                            ),
                          ),
                          SizedBox(
                            height: 60,
                          ),
                          // GridView.count(
                          //   shrinkWrap: true,
                          //   childAspectRatio: 6/2,
                          //   padding: const EdgeInsets.all(0),
                          //   crossAxisSpacing: 20,
                          //   mainAxisSpacing: 20,
                          //   crossAxisCount: 2,
                          //   controller: ScrollController(),
                          //   children: <Widget>[
                          //     Row(
                          //       children: [
                          //         Container(
                          //           margin: EdgeInsets.only(right: 10),
                          //           height: 45,
                          //           width: 45,
                          //           decoration: BoxDecoration(
                          //             border: Border.all(color: Colors.grey),
                          //             borderRadius: BorderRadius.circular(25)
                          //           ),
                          //           child: Icon(Icons.wifi,color: Colors.black54,),
                          //         ),
                          //         Text("WIFI",style: TextStyle(fontWeight: FontWeight.w600,color: Colors.black54),)
                          //       ],
                          //     ),
                          //   ],
                          // ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    height: 70,
                    decoration: BoxDecoration(
                      border: Border(
                          top: BorderSide(
                              color: Colors.grey.shade300, width: 1)),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          InkWell(
                            onTap: (){
                              cont.amenities =[];
                              cont.bathroomType =null;
                              cont.roomNo = null;
                              cont.roomtype  = null;
                              cont.budget = 0;
                              cont.roommateGender = null;
                              cont.update();
                              cont.runFilter();
                              cont.update();
                            },
                              child: Text(
                            "Clear all",
                            style: TextStyle(
                                color: myResources.themeColor,
                                fontWeight: FontWeight.w500),
                          )),
                          TextButton(
                              onPressed: () {
                                cont.runFilter();
                                cont.update();
                                print(cont.foundResult.length);
                                cont.foundResult.refresh();
                                widget.panelController.hide();
                              },
                              child: Container(
                                padding: EdgeInsets.all(15),
                                decoration: BoxDecoration(
                                    color: myResources.themeColor,
                                    borderRadius: BorderRadius.circular(3)),
                                child: Text(
                                  "SHOW RESULT",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ))
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ));
  }
}
