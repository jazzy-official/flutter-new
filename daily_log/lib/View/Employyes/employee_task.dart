import 'package:daily_log/Controllers/employeedetailcontroller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:intl/intl.dart';

import '../../Controllers/task_controller.dart';
import '../../Models/task_model.dart';

class EmployeeTaskScreen extends StatelessWidget {

  EmployeeTaskScreen({Key? key,}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return  GetBuilder<EmployeeDetailController>(
        autoRemove: false,
        init: EmployeeDetailController(),
        builder: (cont) => RefreshIndicator(
          onRefresh: () { return cont.refreshtask(); },
          child: ListView.builder(
            physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
            itemCount: cont.tasks.length,
            itemBuilder: (context, index) {
              TaskModel task = cont.tasks[index];
              return Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        task.name,
                        style: const TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 16),
                      ),
                      Column(
                        children: [
                          SizedBox(
                            width: Get.width-20,
                            child: Text(
                              task.des,
                              style: TextStyle(fontSize: 12,color: Colors.grey),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Text(
                            "Assign to : ",
                            style: TextStyle(
                                fontWeight: FontWeight.w600),
                          ),
                          Text(task.assignto!),
                        ],
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          const Text(
                            "Deadline : ",
                            style: TextStyle(
                                fontWeight: FontWeight.w600),
                          ),
                          Text(DateFormat("dd MMM yyyy")
                              .format(task.deadline)),
                        ],
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          const Text(
                            "Status : ",
                            style: TextStyle(
                                fontWeight: FontWeight.w600),
                          ),
                          Text(
                            task.status ? "Completed" : "Pending",
                            style: TextStyle(
                                color: task.status
                                    ? Colors.green
                                    : Colors.red),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: [
                          const Text(
                            "Project : ",
                            style: TextStyle(
                                fontWeight: FontWeight.w600),
                          ),
                          Text(
                              task.project.toString()),

                        ],
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ));

  }
}