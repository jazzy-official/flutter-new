import 'package:get/get.dart';
import 'package:mongo_dart/mongo_dart.dart';

import 'databaseController.dart';

class MatchController extends GetxController {
  Rx<List<Map<String, dynamic>>> matchList = Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get match => matchList.value;
  List<Map<String, dynamic>> upcommingmatches = [];
  List<Map<String, dynamic>> pastmatches = [];
  final dbHelp = Get.put(DatabaseController());
  @override
  Future<void> onInit() async {
     matchList.bindStream(DatabaseController().matchStream());
     await getupcommingmatches();
     await getpastmatches();
    await getTeamDropDown();
    super.onInit();
  }

  Rx<Map<String, dynamic>> names = Rx<Map<String, dynamic>>({});
  Map<String, dynamic> get name => names.value;
  RxList<dynamic> teamsList = <dynamic>[].obs;
  RxList<dynamic> teams2List = <dynamic>[].obs;

  // getTeamName(Map<String, dynamic> matchModel) async {
  //   try {
  //     teamsList != null ? teamsList = [].obs : null;
  //     teams2List != null ? teams2List = [].obs : null;
  //     names.value = {
  //       'team1': {'teamName': 'Team 1'},
  //       'team2': {'teamName': 'Team 2'}
  //     };

  //     names.value = await dbHelp.getTeamName(matchModel);
  //     List<dynamic> temp = names.value['team1']['teamMembers'];
  //     temp.forEach((element) {
  //       teamsList.add(element);
  //     });
  //     temp = names.value['team2']['teamMembers'];
  //     temp.forEach((element) {
  //       teams2List.add(element);
  //     });
  //   } catch (e) {
  //     print(e);
  //   }
  // }

  Future<String> getTeamName1(Map<String, dynamic> matchModel1) async {
    Map<String, dynamic>? t1 =
        await dbHelp.getTeamData(ObjectId.parse(matchModel1['team1']));

    return t1!['teamName'];
  }

   getupcommingmatches() async {

    upcommingmatches = [];
    match.forEach((element) {

      if(DateTime.now().isBefore(element["matchTime"])){
        // print(element["matchTime"]);
        upcommingmatches.add(element);
        update();
      };
    });
update();
  }
   getpastmatches() async {
pastmatches=[];
    match.forEach((element) {

      if(DateTime.now().isAfter(element["matchTime"])){
        print(element["matchTime"]);
        pastmatches.add(element);
        update();
      }
      ;

    });

  }

  Future<Map<String, dynamic>?> getTeamsDetail(String tId) async {
    return await dbHelp.getTeamData(ObjectId.fromHexString(tId));
  }

  Future<Map<String, dynamic>?> getTeamsDetailByName(String tId) async {
    return await dbHelp.getTeamDataByName(tId);
  }


  Future<List<Map<String, dynamic>>?> getUserbyGroup(String gId) async {
    return await dbHelp.getUsersbygroup(gId);
  }

  Future<String> getTeamName2(Map<String, dynamic> mModel) async {
    Map<String, dynamic>? t2 =
        await dbHelp.getTeamData(ObjectId.parse(mModel['team2']));

    return t2!['teamName'];
  }

//////////////////////////////////////////////////////////////////////////
////////////////////DropDown/////////////////////////////
//////////////////////////////////////////////////////////////////////////

  RxList<Map<String, dynamic>> teamsdropValues = RxList<Map<String, dynamic>>();
  List<Map<String, dynamic>> get teamDroupVal => teamsdropValues.value;
  List<Map<String, dynamic>> coachdropValues = [
    {'name': 'Select coach', 'value': ''}
  ];
  RxString selectedCoachValue = ''.obs;
  RxString selectedCoachName = ''.obs;
  RxString selectedTeam1 = ''.obs;
  RxString selectedTeam2 = ''.obs;
  Future getTeamDropDown() async {
    teamDroupVal.add({'name': 'Select Team', 'value': ''});
    //if (teamsdropValues.isNotEmpty) teamsdropValues.clear();
    await dbHelp.getTeamlistforCreation().then((value) {
      if (value != null) {
        value.forEach((element) {
          teamsdropValues.value.add({
            'name': element['teamName'],
            'value': element['_id'].toHexString()
          });
        });
      }
    });
    await dbHelp.getAllCoachesData().then((value) {
      if (value != null) {
        value.forEach((element) {
          coachdropValues
              .add({'name': element['name'], 'value': element['name']});
        });
      }
    });
  }

  Future<List<String>?> getTokensByTeamgroup(String groupnum) async {
    return await dbHelp.getTokensbyTeamgroup(groupnum);
  }

///////////////////////////////////////////////////////////////////////////
  Future<bool> delMatch(ObjectId id) async {
    if (await DatabaseController().deletMatch(id)) {
      refreshDate();
      return true;
    } else {
      return false;
    }
  }

  refreshDate() async {
    //await Future.delayed(3.seconds);
    this.matchList.bindStream(DatabaseController().matchStream());
    await getupcommingmatches();
    await getpastmatches();
  }
}
