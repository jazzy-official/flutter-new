import 'package:ap_academy_admin/ModelView/Controller/NotificationController.dart';
import 'package:ap_academy_admin/screens/Announcements/send_notification.dart';
import 'package:ap_academy_admin/screens/shared/DeletPop.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import 'notifDetails.dart';

class NotificationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      //Create an event add button
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20.0, right: 10, left: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "",
                  style: MyResources.appHeadingStyle,
                ),
                Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: Container(
                    width: Get.width * 0.2,
                    height: 25,
                    child: RaisedButton(
                      textColor: MyResources.buttontextColor,
                      color: MyResources.buttonColor,
                      child: Text(
                        "ADD",
                      ),
                      onPressed: () {
                        Get.to(() => SendNotification());
                      },
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          GetX<NotificationController>(
            init: Get.put<NotificationController>(NotificationController()),
            builder: (NotificationController notifyController) {
              if (notifyController != null && notifyController.notify != null) {
                print(notifyController.notify.toString());
                return Expanded(
                  child: ListView.builder(
                    itemCount: notifyController.notify.length,
                    itemBuilder: (BuildContext context, int index) =>
                        getNotificationCard(notifyController.notify[index]),
                  ),
                );
              } else {
                return Container(
                  child: LoadingWidget(),
                );
              }
            },
          ),
        ],
      ),
    );
  }

  Widget getNotificationCard(Map<String, dynamic> notificationModel) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: InkWell(
        onTap: () {
          Get.dialog(NotifyDetails(
            notificationModel: notificationModel,
          ));
        },
        child: Card(
          color: MyResources.backgroundColor,
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding:
                      const EdgeInsets.only(top: 10, left: 20, right: 20),
                  child: Icon(
                    Icons.task_alt,
                    color: Colors.red[400],
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      //constraints: BoxConstraints(maxWidth: Get.width * 0.6),
                      width: Get.width * 0.6,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 2.0),
                            child: Container(
                              width: Get.width,
                              child: Text(notificationModel['title'],
                                  overflow: TextOverflow.ellipsis,
                                  style: MyResources.appHeadingStyle),
                            ),
                          ),
                          Text(notificationModel['description'],
                              overflow: TextOverflow.ellipsis,
                              maxLines: 3,
                              softWrap: false,
                              style: MyResources.appTextStyle),
                        ],
                      ),
                    ),
                    Text(
                        DateFormat.Hm()
                            .add_yMEd()
                            .format(notificationModel['timeCreated']),
                        style: MyResources.timestampStyle),
                  ],
                ),
                IconButton(
                    icon: Icon(
                      Icons.delete_forever,
                      color: Colors.red,
                    ),
                    onPressed: () async {
                      Get.dialog(Center(
                          child: DeletpopUp(
                            message: 'Are you sure you want to delete?',
                            pressFunc: () async {
                                final notifC = Get.put(NotificationController());
                                await notifC
                                    .delnotif(notificationModel['_id'])
                                    .then((value) {
                                  notifC.refreshDate();
                                });
                              Get.back();
                            },
                          )));
                    })
              ],
            ),
          ),
        ),
      ),
    );
  }
}
