import 'package:ap_ground_admin/Screens/Authentication/Models/user_model.dart';
import 'package:ap_ground_admin/Screens/Authentication/signup_screen.dart';
import 'package:ap_ground_admin/Screens/Ball%20Management/ball_management_screen.dart';
import 'package:ap_ground_admin/Screens/Grounds/gounds_screen.dart';
import 'package:ap_ground_admin/Screens/Staff/staff_screen.dart';
import 'package:ap_ground_admin/Screens/Templates/template_screen.dart';
import 'package:ap_ground_admin/Screens/Weather/weather_screen.dart';
import 'package:ap_ground_admin/constants/util.dart';
import 'package:ap_ground_admin/controllers/user_controller.dart';
import 'package:ap_ground_admin/controllers/weather_controller.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/elusive_icons.dart';
import 'package:fluttericon/entypo_icons.dart';
import 'package:fluttericon/linecons_icons.dart';
import 'package:fluttericon/octicons_icons.dart';
import 'package:fluttericon/typicons_icons.dart';
import 'package:ionicons/ionicons.dart';

import '../wrapper.dart';
import 'About/about_screen.dart';
import 'Attendance/attendance_screen.dart';
import 'Canteen/canteen_screen.dart';
import 'Home/home_screen.dart';
import 'My booking/booking_screen.dart';
import 'Payment Method/payment_screen.dart';
import 'Payrole/payroll_screen.dart';
import 'Privacy Policy/policy_screen.dart';
import 'Sponsor/sponsor_screen.dart';
import 'Weather/models/location.dart';


Widget selectedWidget = HomeScreen();
String title = "Home";
class NavBar extends StatefulWidget {
  const NavBar({Key? key}) : super(key: key);

  @override
  State<NavBar> createState() => _NavBarState();
}

class _NavBarState extends State<NavBar> {

  late UserModel user;
  Future<void> _signOut() async {
    await FirebaseAuth.instance.signOut();
  }
  bool isLoading = true;
  void refresh()async{
    setState(() {
      isLoading = true;
    });
    user =await UserController().readData(FirebaseAuth.instance.currentUser!.uid);
    setState(() {
      isLoading = false;
    });
  }

  @override
  void initState() {
    refresh();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyResources.buttonColor,
        title: Text(title),
        centerTitle: true,
      ),
      drawer: Drawer(
        child: Column(

          children: [
            isLoading? Container(
                height:150,child: const Center(child: CircularProgressIndicator())) : GestureDetector(
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => SignupScreen()),
                );
              },
                  child: UserAccountsDrawerHeader(
              accountName:  Text(user.name),
              accountEmail:  Text(user.email),
              currentAccountPicture: CircleAvatar(
                  child: ClipOval(
                    child:user.imgUrl==""? Icon(Ionicons.person,size: 45,) :Image.network(
                      user.imgUrl,
                      fit: BoxFit.cover,
                      width: 90,
                      height: 90,
                    ),
                  ),
              ),
              decoration:  BoxDecoration(
                  color: Color(0xff000000),
                  image: DecorationImage(
                      colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.3), BlendMode.dstATop),
                      fit: BoxFit.fill,
                      image: AssetImage(
                          'assets/ap.jpg')),
              ),
            ),
                ),
            Expanded(

              child: ListView(
                // Remove padding
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                children: [
                  ListTile(
                    tileColor: title == "Home"
                        ? Color(0xff1758B4).withOpacity(0.2)
                        : Colors.transparent,
                    leading: Icon(Ionicons.home,color: Color(0xff1758B4),),
                    title: Text('Home'),
                    onTap: () {
                      if (mounted)
                        setState(() {
                          title = "Home";
                          selectedWidget = HomeScreen();
                        });
                      Navigator.pop(context);

                      // Navigator.push(context,
                      //     MaterialPageRoute(builder: (context) => EventScreen()));
                    },
                  ),
                  ListTile(
                    tileColor: title == "Grounds"
                        ? Color(0xff1758B4).withOpacity(0.2)
                        : Colors.transparent,
                    leading: Image.asset('assets/field.png',height: 25,),
                    title: Text('Grounds'),
                    onTap: () {
                      if (mounted) {
                        setState(() {
                          // selectedWidget = EventScreen().obs;
                          title = "Grounds";
                          selectedWidget = GroundScreen();
                        });
                      }
                      Navigator.pop(context);

                    },
                  ),
                  ListTile(
                    tileColor: title == "Weather"
                        ? Color(0xff1758B4).withOpacity(0.2)
                        : Colors.transparent,
                    leading: Icon(Ionicons.cloudy_night,color: Color(0xff1758B4),),
                    title: Text('Weather'),
                    onTap: () {
                      if (mounted)
                        setState(() {
                          title = "Weather";
                          selectedWidget = CurrentWeather(context);
                        });
                      Navigator.pop(context);

                      // Navigator.push(context,
                      //     MaterialPageRoute(builder: (context) => EventScreen()));
                    },
                  ),
                  ListTile(
                    tileColor: title == "Templates"
                        ? Color(0xff1758B4).withOpacity(0.2)
                        : Colors.transparent,
                    leading: Icon(Ionicons.document_text_outline,color: Color(0xff1758B4),),
                    title: Text('Templates'),
                    onTap: () {
                      if (mounted)
                        setState(() {
                          // selectedWidget = EventScreen().obs;
                          title = "Templates";
                          selectedWidget = TemplatesScreen();
                        });
                      Navigator.pop(context);

                    },
                  ),
                  ListTile(
                    tileColor: title == "Bookings"
                        ? Color(0xff1758B4).withOpacity(0.2)
                        : Colors.transparent,
                    leading: Icon(Ionicons.book,color: Color(0xff1758B4),),
                    title: Text('Bookings'),
                    onTap: () {
                      if (mounted)
                        setState(() {
                          // selectedWidget = EventScreen().obs;
                          title = "Bookings";
                          selectedWidget = MyBookingScreen();
                        });
                      Navigator.pop(context);

                    },
                  ),
                  ListTile(
                    tileColor: title == "Canteen"
                        ? Color(0xff1758B4).withOpacity(0.2)
                        : Colors.transparent,
                    leading: const Icon(Linecons.shop,color:Color(0xff1758B4),),
                    title: Text('Canteen'),
                    onTap: () {
                      if (mounted)
                        setState(() {
                          // selectedWidget = EventScreen().obs;
                          title = "Canteen";
                          selectedWidget = CanteenScreen();
                        });
                      Navigator.pop(context);

                    },
                  ),
                  ListTile(
                    tileColor: title == "Staff"
                        ? Color(0xff1758B4).withOpacity(0.2)
                        : Colors.transparent,
                    leading: Icon(Typicons.users,color: Color(0xff1758B4),),
                    title: Text('Staff'),
                    onTap: () {
                      if (mounted)
                        setState(() {
                          // selectedWidget = EventScreen().obs;
                          title = "Staff";
                          selectedWidget = StaffScreen();
                        });
                      Navigator.pop(context);

                    },
                  ),
                  ListTile(
                    tileColor: title == "Agents"
                        ? Color(0xff1758B4).withOpacity(0.2)
                        : Colors.transparent,
                    leading: Icon(Ionicons.person_outline,color: Color(0xff1758B4),),
                    title: Text('Agents'),
                    onTap: () {
                      if (mounted)
                        setState(() {
                          // selectedWidget = EventScreen().obs;
                          title = "Agents";
                          selectedWidget = MyBookingScreen();
                        });
                      Navigator.pop(context);

                    },
                  ),
                  ListTile(
                    tileColor: title == "Teams"
                        ? Color(0xff1758B4).withOpacity(0.2)
                        : Colors.transparent,
                    leading: Icon(Ionicons.people_circle,color: Color(0xff1758B4),),
                    title: Text('Teams'),
                    onTap: () {
                      if (mounted)
                        setState(() {
                          // selectedWidget = EventScreen().obs;
                          title = "Teams";
                          selectedWidget = MyBookingScreen();
                        });
                      Navigator.pop(context);

                    },
                  ),
                  ListTile(
                    tileColor: title == "Ball Management"
                        ? Color(0xff1758B4).withOpacity(0.2)
                        : Colors.transparent,
                    leading: Icon(Ionicons.baseball_outline,color: Color(0xff1758B4),),
                    title: Text('Ball Management'),
                    onTap: () {
                      if (mounted)
                        setState(() {
                          // selectedWidget = EventScreen().obs;
                          title = "Ball Management";
                          selectedWidget = BallScreen();
                        });
                      Navigator.pop(context);

                    },
                  ),
                  ListTile(
                    tileColor: title == "Payroll"
                        ? Color(0xff1758B4).withOpacity(0.2)
                        : Colors.transparent,
                    leading: Icon(Entypo.credit_card,color: Color(0xff1758B4),),
                    title: Text('Payroll'),
                    onTap: () {
                      if (mounted)
                        setState(() {
                          title = "Payroll";
                          selectedWidget = PayRollScreen();
                        });
                      Navigator.pop(context);

                    },
                  ),
                  ListTile(
                    tileColor: title == "Attendance"
                        ? Color(0xff1758B4).withOpacity(0.2)
                        : Colors.transparent,
                    leading: Icon(Octicons.checklist,color: Color(0xff1758B4),),
                    title: Text('Attendance'),
                    onTap: () {
                      if (mounted)
                        setState(() {
                          // selectedWidget = EventScreen().obs;
                          title = "Attendance";
                          selectedWidget = AttendanceScreen();
                        });
                      Navigator.pop(context);

                    },
                  ),
                  ListTile(
                    tileColor: title == "Live Stream"
                        ? Color(0xff1758B4).withOpacity(0.2)
                        : Colors.transparent,
                    leading: Icon(Ionicons.tv_outline,color: Color(0xff1758B4),),
                    title: Text('Live Stream'),
                    onTap: () {
                      if (mounted)
                        setState(() {
                          // selectedWidget = EventScreen().obs;
                          title = "Live Stream";
                          selectedWidget = MyBookingScreen();
                        });
                      Navigator.pop(context);

                    },
                  ),

                  ListTile(
                    tileColor: title == "Payment Method"
                        ? Color(0xff1758B4).withOpacity(0.2)
                        : Colors.transparent,
                    leading: Icon(Ionicons.cash_outline,color: Color(0xff1758B4),),
                    title: Text('Payment Method'),
                    onTap:  () {
                      if (mounted)
                        setState(() {
                          // selectedWidget = EventScreen().obs;
                          title = "Payment Method";
                          selectedWidget = PaymentScreen();
                        });
                      Navigator.pop(context);
                      // Navigator.push(context,
                      //     MaterialPageRoute(builder: (context) => EventScreen()));
                    },
                  ),
                  ListTile(
                    tileColor: title == "Sponsor"
                        ? Color(0xff1758B4).withOpacity(0.2)
                        : Colors.transparent,
                    leading: Icon(Ionicons.medal_outline,color: Color(0xff1758B4),),
                    title: Text('Sponsor'),
                    onTap:  () {
                      if (mounted)
                        setState(() {
                          // selectedWidget = EventScreen().obs;
                          title = "Sponsor";
                          selectedWidget = SponsorScreen();
                        });
                      Navigator.pop(context);
                      // Navigator.push(context,
                      //     MaterialPageRoute(builder: (context) => EventScreen()));
                    },
                  ),
                  ListTile(
                    tileColor: title == "About"
                        ? Color(0xff1758B4).withOpacity(0.2)
                        : Colors.transparent,
                    leading: Icon(Ionicons.information_circle,color: Color(0xff1758B4),),
                    title: Text('About'),
                    onTap:  () {
                      if (mounted)
                        setState(() {
                          // selectedWidget = EventScreen().obs;
                          title = "About";
                          selectedWidget = AboutScreen();
                        });
                      Navigator.pop(context);
                      // Navigator.push(context,
                      //     MaterialPageRoute(builder: (context) => EventScreen()));
                    },
                  ),
                  ListTile(
                    tileColor: title == "Privacy Policies"
                        ? Color(0xff1758B4).withOpacity(0.2)
                        : Colors.transparent,
                    leading: Icon(Ionicons.document_text_outline,color: Color(0xff1758B4),),
                    title: Text('Privacy Policies'),
                    onTap:  () {
                      if (mounted)
                        setState(() {
                          // selectedWidget = EventScreen().obs;
                          title = "Privacy Policies";
                          selectedWidget = PolicyScreen();
                        });
                      Navigator.pop(context);
                      // Navigator.push(context,
                      //     MaterialPageRoute(builder: (context) => EventScreen()));
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              color: MyResources.buttonColor.withOpacity(0.2),
                              borderRadius: BorderRadius.circular(5)
                          ),

                          child: TextButton(onPressed: ()async {
                            await _signOut();
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => Wrapper()),
                            );
                          }, child: Row(
                            children: [
                              Text('Log out',style: TextStyle(color: MyResources.buttonColor),),
                              SizedBox(width: 10,),
                              Icon(Icons.exit_to_app,color: MyResources.buttonColor,),
                            ],
                          )),
                        )
                      ],
                    ),
                  ),

                ],
              ),
            ),
          ],
        ),
      ),
      body: selectedWidget,
    );
  }
}