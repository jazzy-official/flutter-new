//import 'package:apsportacademy/screens/Events/add_team.dart';
import 'package:ap_academy_admin/ModelView/Controller/GroupController.dart';
import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:ap_academy_admin/ModelView/Controller/teamsController.dart';
import 'package:ap_academy_admin/screens/Teams/add_team.dart';
import 'package:ap_academy_admin/screens/Teams/groupScreen.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class SelectMembers extends StatefulWidget {
  @override
  _SelectMembersState createState() => _SelectMembersState();
}

class _SelectMembersState extends State<SelectMembers> {
  final teamC = Get.put(TeamController());
  final groupC = Get.put(GroupsControler());
  TextEditingController searchTextController = TextEditingController();
  String _value = "Select group";
  List<dynamic> userList = [];
  List<Map<String, dynamic>> selectedUserList = [];
  final databaseController = Get.put(DatabaseController());
  RxString selectedValue = ''.obs;
  @override
  void initState() {
    userList = teamC.userList;
    selectedMember = List<bool>.generate(userList.length, (index) => false);

    super.initState();
  }

  filteruserList() {
    userList = [];
    print(groupC.group.where((element) => element["teamName"]==_value).first["teamMembers"]);
    setState(() {
      userList = groupC.group.where((element) => element["teamName"]==_value).first["teamMembers"];
    });

  }

  searchfilteruserList() {
    userList = [];
    teamC.userList.forEach((element) {
      if (element['name'].contains(searchTextController.text))
        userList.add(element);
    });
  }

  List<bool> selectedMember = [];
//////////////////////////////---ye
  getSelMember() {
    selectedUserList.isNotEmpty ? selectedUserList.clear() : null;
    int count = 0;
    selectedMember.forEach((element) {
      if (element == true) {
        selectedUserList.add(userList[count]);
        print(selectedUserList.length);
      }

      ++count;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: Container(
          height: Get.height * 0.86,
          width: Get.width * 0.96,
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Row(
                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      InkWell(
                          onTap: () {
                            Get.back();
                          },
                          child: Container(
                            child: Icon(Icons.arrow_back_ios),
                          )),
                      Container(
                        width: 300,
                        height: 40,
                        child: TextField(
                            decoration: InputDecoration(
                                prefixIcon: Icon(Icons.search),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                contentPadding: EdgeInsets.only(top: 10),
                                fillColor: Colors.grey.shade200,
                                filled: true,
                                hintText: "Search Name"),
                            controller: searchTextController,
                            onChanged: (valur) {
                              searchfilteruserList();
                              setState(() {});
                            }),
                      ),
                      // Padding(
                      //   //Add padding around textfield
                      //   padding: EdgeInsets.only(top: 7.0, left: 10, right: 10),
                      //   child: Padding(
                      //     padding: const EdgeInsets.all(0.0),
                      //     child: Container(
                      //       decoration: BoxDecoration(
                      //           color: MyResources.backgroundColor,
                      //           border: Border.all(color: Colors.blue),
                      //           borderRadius: BorderRadius.circular(10)),
                      //       //  color: MyResources.backgroundColor,
                      //       width: 130,
                      //       height: 35,
                      //       child: Padding(
                      //         padding: const EdgeInsets.all(8.0),
                      //         child: GetX<GroupsControler>(
                      //           init:
                      //               Get.put<GroupsControler>(GroupsControler()),
                      //           builder: (GroupsControler groupController) {
                      //             if (groupController != null) {
                      //               if (groupController.group.length != 0) {
                      //                 if (selectedValue.value == '')
                      //                   selectedValue.value = groupController
                      //                       .group[0]['teamName']
                      //                       .toString();
                      //                 return DropdownButton<String>(
                      //                   underline: Text(""),
                      //                   isExpanded: true,
                      //                   value: selectedValue.value,
                      //                   focusColor: MyResources.buttonColor,
                      //                   elevation: 12,
                      //                   items: groupController.group
                      //                       .map((map) {
                      //                     return DropdownMenuItem(
                      //                       child: Text(
                      //                         map['teamName'].toString(),
                      //                         style: TextStyle(fontSize: 12),
                      //                       ),
                      //                       value: map['teamName'].toString(),
                      //                     );
                      //                   }).toList(),
                      //                   onChanged: (String? value) {
                      //                     print(value);
                      //
                      //                     setState(() {
                      //                       _value = value!;
                      //                       selectedValue.value = value;
                      //                     });
                      //                     filteruserList();
                      //                   },
                      //                   hint: Text('Select Item'),
                      //                 );
                      //               } else
                      //                 return Container();
                      //             } else
                      //               return Container();
                      //           },
                      //         ),
                      //       ),
                      //     ),
                      //   ),
                      // ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 12.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          " Select Members",
                          style: MyResources.appHeadingStyle,
                        ),
                        Container(
                          width: Get.width * 0.2,
                          height: 35,
                          child: RaisedButton(
                            textColor: Colors.white,
                            color: MyResources.buttonColor,
                            child: Text(
                              "Add",
                            ),
                            onPressed: () {
                              getSelMember();
                              Get.back(result: selectedUserList);

                              //  selectedUserList.clear();
                            },
                            shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  DataTable(
                    columnSpacing: 120,
                    columns: const <DataColumn>[
                      DataColumn(
                        label: Text(
                          'Name',
                          style: TextStyle(fontStyle: FontStyle.normal),
                        ),
                      ),
                      DataColumn(
                        label: Text(
                          'Age',
                          style: TextStyle(fontStyle: FontStyle.normal),
                        ),
                      ),
                    ],
                    rows: List<DataRow>.generate(userList.length, (index) {
                      // if (userList[index]['name']
                      //     .toString()
                      //     .toLowerCase()
                      //     .contains(searchTextController.text.toLowerCase()))
                      return DataRow(
                        color: MaterialStateProperty.resolveWith<Color>(
                            (Set<MaterialState> states) {
                          // All rows will have the same selected color.
                          if (states.contains(MaterialState.selected))
                            return Theme.of(context)
                                .colorScheme
                                .primary
                                .withOpacity(0.08);

                          if (index % 2 == 0)
                            return Colors.grey.withOpacity(0.3);
                          return Colors.transparent; // Use default value for other states and odd rows.
                        }),
                        cells: [
                          DataCell(Text(userList[index]['name'])),
                          DataCell(userList[index]['dob'] != null
                              ? Text(calculateAge(userList[index]['dob']).toString())
                              : Text('N/A')),
                        ],
                        selected: selectedMember[index],
                        onSelectChanged: (bool? value) {
                          selectedMember[index] = value!;
                          setState(() {
                            print(value.toString());
                          });
                        },
                      );
                      // else
                      //   return DataRow(cells: [
                      //     DataCell(Text('')),
                      //     DataCell(
                      //       Text(''),
                      //     )
                      //   ]);
                    }),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
