import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:rafi_admin/Components/Screens/Purchase/Model/investment_model.dart';

class InvestmentCard extends StatelessWidget {
  final InvestmentModel investment;
  InvestmentCard({Key? key, required this.investment}) : super(key: key);
  var formatter = NumberFormat('#,##,000');
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 4,
      child: InkWell(
        onTap: () {
          showDialog<String>(
            context: context,
            builder: (BuildContext context) => AlertDialog(
              title: const Text('Details'),
              content: Container(
                height: 150,
                width: MediaQuery.of(context).size.width,
                child: Column(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Investment Ammount : ",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text(
                          formatter
                              .format(investment.amount),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Contract Ammount : ",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text(
                          formatter
                              .format(investment.contractsAmount),
                        ),
                      ],
                    ),
                    Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Paid Ammount: ",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text(
                          formatter
                              .format(investment.amountPaid),

                        ),
                      ],
                    ),
                    Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Pending Ammount: ",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text(
                          formatter
                              .format(investment.amount - investment.amountPaid),

                        ),
                      ],
                    ),
                    Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Recovery Ammount: ",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text(
                          formatter
                              .format(investment.amountRecovery),

                        ),
                      ],
                    ),
                    Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Received Ammount : ",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text(
                          formatter
                              .format(investment.amountReceived),

                        ),
                      ],
                    ),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  onPressed: () => Navigator.pop(context,),
                  child: const Text('Close'),
                ),
              ],
            ),
          );
        },
        child: Container(
          height: 100,
          padding: EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.6,
                    child: Flex(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      direction: Axis.vertical,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          investment.investmentName,
                          style:
                          TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.6,
                    child: Row(
                      children: [
                        Text("Seller : ",style:TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
                        Flexible(
                            child: Text(
                              investment.accountTitle,
                              style:
                              TextStyle(fontSize: 15,),
                            )),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.65,
                    child: Row(
                      children: [
                        Text("Investor : ",style:TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
                        Flexible(
                            child: Text(
                              investment.investorName,
                              style:
                              TextStyle(fontSize: 15,),
                            )),
                      ],
                    ),
                  ),
                  Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Investor CNIC : ",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text(
                          investment.investorCnic
                      ),
                    ],
                  ),

                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                  DateFormat(' dd-MMM-yyyy').format(DateTime.parse(investment.dateAdded))
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            "Sold units : ",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(
                            investment.unitSold.toString(),
                          ),
                        ],
                      ),
                    ],
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
