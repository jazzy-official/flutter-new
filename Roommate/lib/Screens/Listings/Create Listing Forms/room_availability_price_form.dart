import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:intl/intl.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Screens/Listings/Create%20Listing%20Forms/room_media_description_form.dart';
import 'package:room_buddy/controllers/my_listing_controller.dart';

class RoomAvailabiltyForm extends StatefulWidget {
   RoomAvailabiltyForm({Key? key}) : super(key: key);

  @override
  State<RoomAvailabiltyForm> createState() => _RoomAvailabiltyFormState();
}

class _RoomAvailabiltyFormState extends State<RoomAvailabiltyForm> {

  var myController = Get.put(MyListingController());

  void _showDatePicker(ctx) {
    showCupertinoModalPopup(
        context: ctx,
        builder: (_) => Container(
          height: 353,
          color: const Color.fromARGB(255, 255, 255, 255),
          child: Column(
            children: [
              SizedBox(
                height: 300,
                child: CupertinoDatePicker(
                    initialDateTime: DateTime.now(),
                    mode: CupertinoDatePickerMode.date,
                    onDateTimeChanged: (val) {
                      myController.roomAvalabiltyDate = val;
                      myController.update();

                    }),
              ),

              // Close the modal
              CupertinoButton(
                child: const Text('OK'),
                onPressed: () => Navigator.of(ctx).pop(),
              )
            ],
          ),
        ));
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          iconTheme: const IconThemeData(color: Colors.black),
          title: Text("Step 4: Availability and pricing",style: TextStyle(color: Colors.black),),
          bottom: PreferredSize(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 22),
              child: LinearProgressIndicator(
                color: myResources.themeColor,
                backgroundColor: Colors.grey.shade300,
                minHeight: 6,
                value: 4 / 6,
              ),
            ),
            preferredSize: const Size.fromHeight(50),
          )),
      body:GetBuilder<MyListingController>(
        init: MyListingController(),
        builder: (cont) => Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0,vertical: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Budget Range",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                    Row(
                      children: [
                        Checkbox(
                            activeColor: myResources.themeColor,
                            value: cont.includeBill,
                            onChanged: (e) {
                              cont.includeBill = e!;
                              cont.update();
                            }),
                        Text("Bills included",
                          style: TextStyle(
                            color: Colors.grey.shade600,
                            fontWeight: FontWeight.w500,
                          ),)
                      ],
                    )
                  ],
                ),
              ),
              Text(
                "\$ "+cont.range.round().toString(),
                style: TextStyle(
                    color: myResources.themeColor,
                    fontWeight: FontWeight.w500,
                    fontSize: 16),
              ),
              Slider(
                value: cont.range,
                max: 100000,
                divisions: 200,
                label: cont.range.round().toString(),
                activeColor: myResources.themeColor,
                onChanged: (double value) {
                  cont.range = value.roundToDouble();
                  cont.update();
                   },
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: const Text(
                  "Security deposit",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                ),
              ),
              Text(
                "\$ "+cont.securityDeposit.round().toString(),
                style: TextStyle(
                    color: myResources.themeColor,
                    fontWeight: FontWeight.w500,
                    fontSize: 16),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: Slider(
                  value: cont.securityDeposit,
                  max: 100000,
                  divisions: 200,
                  label: cont.securityDeposit.round().toString(),
                  activeColor: myResources.themeColor,
                  onChanged: (double value) {
                      cont.securityDeposit = value.roundToDouble();
                      cont.update();

                  },
                ),
              ),
              Divider(
                height: 2,
                color: Colors.grey[300],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 15.0),
                        child: Text("Room availability",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 17),),
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                              BorderRadius.all(Radius.circular(3)),
                              side: BorderSide(color: Colors.grey.shade500)),
                        ),
                        onPressed: () {
                          // Get.to(()=> const SignupFormTwo());
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 0, horizontal: 8),
                          height: 35,
                          child: DropdownButton<String>(
                            value: cont.roomAvailability,
                            icon: Icon(
                              CupertinoIcons.chevron_down,
                              size: 18,
                              color: myResources.themeColor,
                            ),
                            style: TextStyle(color: Colors.black),
                            underline: Container(
                              height: 0,
                              color: Colors.transparent,
                            ),
                            onChanged: (String? newValue) {
                              cont.roomAvailability = newValue!;
                              if(newValue =="Immediate"){
                                cont.roomAvalabiltyDate = null;
                              }
                              cont.update();
                            },
                            items: <String>[
                              'Immediate',
                              'Later',
                            ].map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text("Or",style: TextStyle(color: Colors.grey.shade500),),
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                              BorderRadius.all(Radius.circular(3)),
                              side: BorderSide(color: Colors.grey.shade500)),
                        ),
                        onPressed: cont.roomAvailability=="Later"?(){
                          _showDatePicker(context);
                        }:null,
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 0, horizontal: 8),
                          height: 35,
                          child: Row(
                            children: [
                              Text(cont.roomAvalabiltyDate !=null? DateFormat("dd MMM yyyy").format(cont.roomAvalabiltyDate!) : "Set a date  ",style: TextStyle(color: cont.roomAvailability=="Later"? myResources.themeColor:Colors.black38),),
                              Icon(CupertinoIcons.chevron_down,size: 18,color: myResources.themeColor,)
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 15.0),
                        child: Text("Minimum stay period",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 17),),
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                              BorderRadius.all(Radius.circular(3)),
                              side: BorderSide(color: Colors.grey.shade500)),
                        ),
                        onPressed: () {

                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 0, horizontal: 8),
                          height: 35,
                          child: DropdownButton<String>(
                            value: cont.minstay,
                            icon: Icon(
                              CupertinoIcons.chevron_down,
                              size: 18,
                              color: myResources.themeColor,
                            ),
                            style: TextStyle(color: Colors.black),
                            underline: Container(
                              height: 0,
                              color: Colors.transparent,
                            ),
                            onChanged: (String? newValue) {

                                cont.minstay = newValue!;
                                cont.update();
                            },
                            items: <String>[
                              '6 months',
                              '1  year',
                              '1.5  year',
                              '2  year',
                              '3  year',
                              '3 +  year',
                            ].map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
              const Expanded(child: SizedBox()),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: myResources.themeColor,
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(14))),
                ),
                onPressed: () {
                  Get.to(()=>  RoomMediaDescriptionForm());
                },
                child: Container(
                  padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SizedBox(width: 20,),
                      Text(
                        'Proceed',
                        style: TextStyle(color: Colors.white),
                      ),
                      Container(
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                          borderRadius:
                          const BorderRadius.all(Radius.circular(20)),
                          color: myResources.themeColor,
                        ),
                        child: Icon(
                          Icons.arrow_forward_ios,
                          color: Colors.white,
                          size: 16,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
