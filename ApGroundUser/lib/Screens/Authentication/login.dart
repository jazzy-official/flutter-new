import 'package:ap_booking_user/Screens/Authentication/forgot_password_screen.dart';
import 'package:ap_booking_user/Screens/Authentication/signup_screen.dart';
import 'package:ap_booking_user/Screens/drawer.dart';
import 'package:ap_booking_user/constants/util.dart';
import 'package:ap_booking_user/controllers/firebase_controllers.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String email = "";

  String pass = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButton(
            color: Colors.black
        ),
        backgroundColor: Colors.white,
        title: Text("SIGN IN"),
        titleTextStyle: TextStyle(color: Colors.black,fontSize: 18,fontWeight: FontWeight.w600),
        centerTitle: true,
      ),
      resizeToAvoidBottomInset: false,
      body: Column(
        children: [
          Expanded(
              child: SingleChildScrollView(
                child: Column(
            children: [
              Image.asset(
                "assets/logo.png",
                height: MediaQuery.of(context).size.height * 0.32,
              ),
                 Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                  child: TextField(
                    onChanged: (val){
                      setState(() {
                        email = val;
                      });
                    },
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Email',
                        contentPadding: EdgeInsets.symmetric(horizontal: 10)),
                  ),
                ),
                 Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      TextField(
                        onChanged: (val){
                          setState(() {
                            pass = val;
                          });
                        },
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'Password',
                          contentPadding: EdgeInsets.symmetric(horizontal: 10),
                        ),
                      ),
                      TextButton(onPressed: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => ForgotPasswordScreen()),
                        );
                      }, child: Text("Forgot Password?"))
                    ],
                  ),
                ),
                TextButton(
                    onPressed: () {
                      Login(email, pass).then((value) => {
                        if(value) {
                          displayToast("Welcome"),

                      Navigator.pushReplacement<void, void>(
                      context,
                      MaterialPageRoute<void>(
                      builder: (BuildContext context) => const NavBar(),
                      ),
                      )
                        }
                      });
                    },
                    child: Container(
                      height: 50,
                      width: MediaQuery.of(context).size.width - 160,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                        color: MyResources.buttonColor,
                      ),
                      child: Center(
                          child: Text(
                        "Sign in",
                        style: TextStyle(color: Colors.white, fontSize: 17),
                      )),
                    )),
                Padding(
                  padding: const EdgeInsets.all(30.0),
                  child: Text(
                    "Or Login With",
                    style: TextStyle(color: Colors.grey, fontSize: 16),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    TextButton(
                      onPressed: () async {
                        await signInWithGoogle(context);
                      },
                      child: Container(
                          height: 45,
                          decoration: BoxDecoration(
                              color: Color(0xfff8f2f1),
                              borderRadius: BorderRadius.circular(10),
                              border:
                                  Border.all(color: Color(0xffea4335), width: 1)),
                          padding:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                          child: Row(
                            children: [
                              Icon(
                                Ionicons.logo_google,
                                color: Color(0xffea4335),
                              ),
                              SizedBox(
                                width: 15,
                              ),
                              Text(
                                "Google",
                                style: TextStyle(color: Color(0xffea4335)),
                              ),
                              SizedBox(
                                width: 15,
                              ),
                            ],
                          )),
                    ),
                    Container(
                      width: 2,
                      height: 50,
                      color: Colors.grey.shade400,
                    ),
                    TextButton(
                      onPressed: () {},
                      child: Container(
                          decoration: BoxDecoration(
                              color: Color(0xfff1f2f8),
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(color: Colors.blue, width: 1)),
                          padding:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                          child: Row(
                            children: [
                              Icon(Ionicons.logo_facebook),
                              SizedBox(
                                width: 10,
                              ),
                              Text("Facebook")
                            ],
                          )),
                    ),
                  ],
                ),
            ],
          ),
              )),
          Container(
            height: 45,
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide(
                  //                    <--- top side
                  color: Colors.grey,
                  width: 1.0,
                ),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "New to App?",
                  style: TextStyle(color: Colors.grey),
                ),
                TextButton(onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SignupScreen()),
                  );
                }, child: Text("Sign up"))
              ],
            ),
          ),
        ],
      ),
    );
  }
}
