class Payable {
  int accId;
  String accountno;
  String accounttitle;
  String accountcategory;
  String accountdes;
  String balancetype;
  bool tax;
  int heading;
  double openingbalance;
  double currentbalance;
  int projectId;

  Payable({
    required this.accId,
    required this.accountno,
    required this.accounttitle,
    required this.accountcategory,
    required this.accountdes,
    required this.balancetype,
    required this.tax,
    required this.heading,
    required this.openingbalance,
    required this.currentbalance,
    required this.projectId,
  });

  factory Payable.fromJson(Map<String, dynamic> json) {
    return Payable(
      accId: json['accid'] as int,
      accountno: json['accountno'] as String,
      accountdes: json['accountdes'] as String,
      tax: json['tax'] as bool,
      projectId:  json['projectId'] as int,
      accountcategory:json['accountcategory'] as String,
      heading: json['heading'] as int,
      balancetype: json['balancetype'] as String,
      currentbalance:  json['currentBalance'] as double,
      accounttitle:json['accounttitle'] as String,
      openingbalance: json['openingBalance'] as double,
    );
  }
}
