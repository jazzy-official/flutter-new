import 'package:ap_booking_user/Screens/Authentication/login.dart';
import 'package:ap_booking_user/constants/util.dart';
import 'package:ap_booking_user/controllers/firebase_controllers.dart';
import 'package:ap_booking_user/controllers/user_controller.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';

import 'Models/user_model.dart';

class SignupScreen extends StatefulWidget {
  const SignupScreen({Key? key}) : super(key: key);

  @override
  State<SignupScreen> createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  String email = "";

  String pass = "";
  String cpass = "";

  String name = "";

  String role = "User";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButton(color: Colors.black),
        backgroundColor: Colors.white,
        title: const Text("SIGN UP"),
        titleTextStyle: const TextStyle(
            color: Colors.black, fontSize: 18, fontWeight: FontWeight.w600),
        centerTitle: true,
      ),
      resizeToAvoidBottomInset: false,
      body: Column(
        children: [
          Image.asset(
            "assets/logo.png",
            height: MediaQuery.of(context).size.height * 0.25,
          ),
          Expanded(
              child: SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  child: TextField(
                    onChanged: (val) {
                      setState(() {
                        name = val;
                      });
                    },
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Full Name',
                        contentPadding: EdgeInsets.symmetric(horizontal: 10)),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  child: TextField(
                    onChanged: (val) {
                      setState(() {
                        email = val;
                      });
                    },
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Email',
                        contentPadding: EdgeInsets.symmetric(horizontal: 10)),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  child: TextField(
                    onChanged: (val) {
                      setState(() {
                        pass = val;
                      });
                    },
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Password',
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  child: TextField(
                    onChanged: (val) {
                      setState(() {
                        cpass = val;
                      });
                    },
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Confirm Password',
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                    ),
                  ),
                ),
                TextButton(
                    onPressed: () async {
                      if (email == '') {
                        displayToast("Please enter email");
                      } else if (pass == '') {
                        displayToast("Please enter a password");
                      } else if (!email.contains("@")) {
                        displayToast("Please enter a valid email address");
                      } else if(pass!=cpass){
                        displayToast("Password do no match");
                      }else {
                        try {
                          await signup(email, pass, context);
                        } catch (e) {
                          displayToast(e.toString());
                        }

                        await UserController().saveUser(UserModel(
                            name: name,
                            email: email,
                            imgUrl: "",
                            uid: FirebaseAuth.instance.currentUser!.uid,
                            role: role));
                        displayToast("Welcome");
                      }
                    },
                    child: Container(
                      height: 50,
                      width: MediaQuery.of(context).size.width - 160,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                        color: MyResources.buttonColor,
                      ),
                      child: const Center(
                          child: Text(
                        "Sign up",
                        style: TextStyle(color: Colors.white, fontSize: 17),
                      )),
                    )),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(
                    "Or Login With",
                    style: TextStyle(color: Colors.grey, fontSize: 16),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    TextButton(
                      onPressed: () {},
                      child: Container(
                          height: 45,
                          decoration: BoxDecoration(
                              color: Color(0xfff8f2f1),
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                  color: Color(0xffea4335), width: 1)),
                          padding: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          child: Row(
                            children: [
                              Icon(
                                Ionicons.logo_google,
                                color: Color(0xffea4335),
                              ),
                              SizedBox(
                                width: 15,
                              ),
                              Text(
                                "Google",
                                style: TextStyle(color: Color(0xffea4335)),
                              ),
                              SizedBox(
                                width: 15,
                              ),
                            ],
                          )),
                    ),
                    Container(
                      width: 2,
                      height: 50,
                      color: Colors.grey.shade400,
                    ),
                    TextButton(
                      onPressed: () {},
                      child: Container(
                          decoration: BoxDecoration(
                              color: Color(0xfff1f2f8),
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(color: Colors.blue, width: 1)),
                          padding: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          child: Row(
                            children: [
                              Icon(Ionicons.logo_facebook),
                              SizedBox(
                                width: 10,
                              ),
                              Text("Facebook")
                            ],
                          )),
                    ),
                  ],
                ),
              ],
            ),
          )),
          Container(
            height: 45,
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide(
                  //                    <--- top side
                  color: Colors.grey,
                  width: 1.0,
                ),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Existing User?",
                  style: TextStyle(color: Colors.grey),
                ),
                TextButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => LoginScreen()),
                      );
                    },
                    child: Text("Sign in"))
              ],
            ),
          )
        ],
      ),
    );
  }
}
