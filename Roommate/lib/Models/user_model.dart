import 'package:room_buddy/Models/about_user_model.dart';
import 'package:room_buddy/Models/habits_modal.dart';
import 'package:room_buddy/Models/location_model.dart';

class UserModel {
  final int id;
  final String name ;
  final String email ;
  final List<String> languages ;
  final String city ;
  final String identityType;
  final String identityNo;
  final String profilePic;
  final String profileBio;
  final AboutUser aboutUser;
  final List<int> favIds;


  UserModel(this.name, this.languages, this.city, this.identityType, this.identityNo, this.profilePic, this.profileBio, this.aboutUser, this.email, this.favIds, this.id);

  UserModel.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        email = json['email'],
        languages = json['email'],
        city = json['email'],
        identityType = json['email'],
        identityNo = json['email'],
        profilePic = json['email'],
        profileBio = json['email'],
        favIds = json['email'],
        id = json['email'],
        aboutUser = json['email'];

  Map<String, dynamic> toJson() => {
    'name': name,
    'email': email,
  };
}