import 'package:ap_academy_admin/constants/util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class MatchTeamsDetails extends StatelessWidget {
  final Map<String, dynamic>? team1Model;
  final Map<String, dynamic>? team2Model;
  final Map<String, dynamic>? matchModel;

  MatchTeamsDetails(
      {Key? key, this.team1Model, this.team2Model, this.matchModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyResources.buttonColor,
      ),
      body: Padding(

        padding: const EdgeInsets.only(top: 0.0, left: 8, right: 8),
        child: Material(

          color: Colors.transparent,
          child: Stack(
            children: [
              SizedBox(
                //create the cards for all details of event happening
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Card(
                        elevation: 3,
                        color: MyResources.backgroundColor,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Container(
                                height: 35,
                                color: MyResources.buttonColor,
                                child: Center(
                                  child: Text("MATCH DETAILS",
                                      style: MyResources.modelHeadingStyle),
                                ),
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  width: 30,
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      width: 60,
                                    ),
                                    SizedBox(
                                      height: 30,
                                      child: Text("Match:",
                                          style: MyResources.titleTextStyle),
                                    ),
                                    SizedBox(
                                        height: 30,
                                        child: Text("Time:",
                                            style: MyResources.titleTextStyle)),
                                    SizedBox(
                                        height: 30,
                                        child: Text("Venue:",
                                            style: MyResources.titleTextStyle)),
                                    SizedBox(
                                        height: 30,
                                        child: Text("Coach:",
                                            style: MyResources.titleTextStyle)),
                                    SizedBox(
                                        height: 30,
                                        child: Text("Note:",
                                            style: MyResources.titleTextStyle)),
                                  ],
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      height: 30,
                                      width: Get.width*0.7,
                                      child: Text(
                                        matchModel!['match'],
                                        style: MyResources.nameTextStyle,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                    Container(
                                      width: Get.width*0.6,
                                      height: 30,
                                      child: Text(
                                        DateFormat("dd-MMM-yyyy - hh:mm a")
                                            .format(matchModel!['matchTime'].toLocal()),
                                        overflow: TextOverflow.ellipsis,
                                        style: MyResources.nameTextStyle,
                                      ),
                                    ),
                                    Container(
                                      width: Get.width*0.5,
                                      height: 30,
                                      child: Text(
                                        matchModel!['venue'],
                                        style: MyResources.nameTextStyle,
                                      ),
                                    ),
                                    Container(
                                      width: Get.width*0.5,
                                      height: 30,
                                      child: Text(
                                        matchModel!['coach'],
                                        style: MyResources.nameTextStyle,
                                      ),
                                    ),
                                    Container(
                                      width: Get.width*0.5,
                                      constraints:
                                      BoxConstraints(minHeight: 30),
                                      child: Text(
                                        matchModel!['note'],
                                        style: MyResources.nameTextStyle,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 10,
                                right: 100,
                                left: 10,
                              ),
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: [],
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            //members
                          ],
                        ),
                      ),
                      SizedBox(height: 10,),
                      matchModel!["otherteam1"]==true?Card(): Card(
                        elevation: 3,
                        color: MyResources.backgroundColor,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Container(
                                height: 35,
                                color: MyResources.buttonColor,
                                child: Center(
                                  child: Text("Team : "+team1Model!['teamName'].toString().toUpperCase(),
                                      style: MyResources.modelHeadingStyle),
                                ),
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    // Container(
                                    //   child: Text("Team :",
                                    //       style: MyResources.titleTextStyle),
                                    // ),
                                    // Text("Type:",
                                    //     style: MyResources.titleTextStyle),
                                    // Text("Coach:",
                                    //     style: MyResources.titleTextStyle),
                                  ],
                                ),
                                // Column(
                                //   crossAxisAlignment: CrossAxisAlignment.start,
                                //   children: [
                                //     Text(team1Model['teamName'],
                                //         style: MyResources.nameTextStyle),
                                //     Text(team1Model['teamType'],
                                //         style: MyResources.nameTextStyle),
                                //     Text(team1Model['coach'],
                                //         style: MyResources.nameTextStyle),
                                //   ],
                                // ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 10,
                                right: 100,
                                left: 10,
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [],
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            //members
                            // Center(
                            //   child: Text(
                            //     "Members",
                            //     style: MyResources.appHeadingStyle,
                            //   ),
                            // ),
                            // SizedBox(
                            //   height: 20,
                            // ),
                            Column(
                              children: [
                                Container(
                                    width: Get.width *0.9,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(10),
                                            topRight: Radius.circular(10)),
                                        color: MyResources.buttonColor),
                                    height: 35,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Container(
                                          width: 25,
                                          child: Text(
                                            'Sr',
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        ),
                                        Container(
                                          width: 100,
                                          child: Text(
                                            'Name     ',
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        ),
                                        Container(
                                          width: 120,
                                          child: Text('Available',
                                              style: TextStyle(
                                                  color: Colors.white)),
                                        )
                                      ],
                                    )),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 20.0),
                              child: Container(
                                height: 100,
                                constraints: BoxConstraints(minHeight: 10),
                                width: Get.width * 0.9,
                                child: ListView.builder(
                                    itemCount: team1Model!['teamMembers']==null?0: team1Model!['teamMembers'].length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      bool isAttend = false;
                                      matchModel!['attendance']
                                          .forEach((element) {
                                        if (element['firebaseId'] ==
                                            team1Model!['teamMembers'][index]
                                            ['firebaseId']) {
                                          isAttend = true;
                                        }
                                      });
                                      return getTeamMembersCard(
                                          team1Model!['teamMembers'][index],
                                          index + 1,
                                          isAttend);
                                    }),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 10,),
                      matchModel!["otherteam2"]==true?Card():Card(
                        elevation: 3,
                        color: MyResources.backgroundColor,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Container(
                                height: 35,
                                color: MyResources.buttonColor,
                                child: Center(
                                  child: Text("Team : "+team2Model!['teamName'].toString().toUpperCase(),
                                      style: MyResources.modelHeadingStyle),
                                ),
                              ),
                            ),
                            // Row(
                            //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            //   children: [
                            //     Column(
                            //       crossAxisAlignment: CrossAxisAlignment.start,
                            //       children: [
                            //         Container(
                            //           child: Text("Team :",
                            //               style: MyResources.titleTextStyle),
                            //         ),
                            //         Text("Type:",
                            //             style: MyResources.titleTextStyle),
                            //         Text("Coach:",
                            //             style: MyResources.titleTextStyle),
                            //       ],
                            //     ),
                            //     Column(
                            //       crossAxisAlignment: CrossAxisAlignment.start,
                            //       children: [
                            //         Text(team2Model['teamName'],
                            //             style: MyResources.nameTextStyle),
                            //         Text(team2Model['teamType'],
                            //             style: MyResources.nameTextStyle),
                            //         Text(team2Model['coach'],
                            //             style: MyResources.nameTextStyle),
                            //       ],
                            //     ),
                            //   ],
                            // ),
                            // Padding(
                            //   padding: const EdgeInsets.only(
                            //     top: 10,
                            //     right: 100,
                            //     left: 10,
                            //   ),
                            //   child: Row(
                            //     mainAxisAlignment:
                            //         MainAxisAlignment.spaceBetween,
                            //     children: [],
                            //   ),
                            // ),
                            // SizedBox(
                            //   height: 10,
                            // ),
                            //members
                            // Center(
                            //   child: Text(
                            //     "Members",
                            //     style: MyResources.appHeadingStyle,
                            //   ),
                            // ),
                            SizedBox(
                              height: 20,
                            ),
                            Column(
                              children: [
                                SizedBox(),
                                Container(
                                    width: Get.width * 0.9,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10),
                                          topRight: Radius.circular(10)),
                                      color: MyResources.buttonColor,
                                    ),
                                    height: 35,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Container(
                                          width: 25,
                                          child: Text(
                                            'Sr',
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        ),
                                        Container(
                                          width: 100,
                                          child: Text(
                                            'Name',
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        ),
                                        Container(
                                          width: 120,
                                          child: Text('Available',
                                              style: TextStyle(
                                                  color: Colors.white)),
                                        )
                                      ],
                                    )),
                              ],
                            ),
                            Container(

                              height: 100,
                              constraints: BoxConstraints(minHeight: 10),
                              width: Get.width * 0.9,
                              child: ListView.builder(
                                  itemCount: team2Model!['teamMembers']==null?0: team2Model!['teamMembers'].length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    bool isAttend = false;
                                    matchModel!['attendance'].forEach((element) {
                                      if (element['firebaseId'] ==
                                          team2Model!['teamMembers'][index]
                                              ['firebaseId']) {
                                        isAttend = true;
                                      }
                                    });
                                    return getTeamMembersCard(
                                        team2Model!['teamMembers'][index],
                                        index + 1,
                                        isAttend);
                                  }),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
          // Positioned(
          //         left: 10,
          //         top:3,
          //         child:Container(
          //           height: 39,
          //             margin: EdgeInsets.only(top:3),
          //             color: Colors.blueAccent.withOpacity(0.4),
          //             child: BackButton(
          //           color: Colors.white,
          //           onPressed: () {
          //             Get.back();
          //           },
          //         ))),
            ],
          ),
        ),
      ),
    );
  }

  Widget getTeamMembersCard(
      Map<String, dynamic> memeberslist, int i, bool isAttend) {
    return Column(
      children: [
        Container(
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: MyResources.buttonColor,
                    offset: Offset(0, 1),
                  ),
                ],
                borderRadius: BorderRadius.circular(5),
                color: Colors.white.withOpacity(1)),
            height: 40,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(width: 20, child: Text(i.toString())),
                Container(
                    width: 100,
                    child: Text(
                      memeberslist['name'],
                      overflow: TextOverflow.ellipsis,
                    )),
                Container(
                  width: 120,
                  child: isAttend
                      ? Icon(
                          Icons.check,
                          color: Colors.green,
                        )
                      : Icon(Icons.cancel_rounded, color: Colors.red),
                  //  Text(
                  //   memeberslist['email'],
                  //   overflow: TextOverflow.ellipsis,
                  // )
                )
              ],
            )),
        SizedBox(
          height: 5,
        )
      ],
    );
  }
}
