import 'package:ap_ground_admin/Screens/Grounds/Models/ground_models.dart';
import 'package:ap_ground_admin/constants/util.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';

import 'Models/slot_model.dart';
import 'Models/template_model.dart';
import 'add_ground.dart';
class GroundDetail extends StatelessWidget {
  final Ground ground;
  const GroundDetail({Key? key, required this.ground}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        actions: [
          TextButton(
            child: Container(
              margin: const EdgeInsets.only(right: 10),
              padding: const EdgeInsets.all(6),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: MyResources.buttonColor,
              ),
              child: Row(
                children: const [
                  Text(
                    "Edit ",
                    style: TextStyle(color: Colors.white),
                  ),
                  Icon(
                    Icons.edit,
                    color: Colors.white,
                    size: 18,
                  ),
                ],
              ),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => AddGroundForm(
                      ground: ground,
                    )),
              );
            },
          ),
        ],
        backgroundColor: MyResources.buttonColor,
        title: Text(
          ground.name.toUpperCase(),
          style: const TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 16),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            CarouselSlider(
              options: CarouselOptions(
                aspectRatio: 2.0,
                enlargeCenterPage: true,
              ),
              items: ground.imgUrls
                  .map((item) => Center(
                      child: item== "" ? Image.asset("assets/stadiumlogo.png",width: 1000,) :Image.network(
                    item,
                    fit: BoxFit.fill,
                    width: 1000,
                  )))
                  .toList(),
            ),
            Padding(
              padding: const EdgeInsets.all(7.0),
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Address : ",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                      Expanded(
                          child: Text(
                        ground.address,
                        style: const TextStyle(wordSpacing: 5),
                      )),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 7),
              child: Card(
                elevation: 4,
                child: Column(
                  children: [
                    const Padding(
                      padding: EdgeInsets.all(15.0),
                      child: Text(
                        "Weekly Slots Schedule",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                    ),
                    DefaultTabController(
                        length: 7, // length of tabs
                        initialIndex: 0,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              TabBar(
                                indicatorSize: TabBarIndicatorSize.label,
                                indicator: BoxDecoration(
                                    borderRadius: BorderRadius.circular(50),
                                    color: MyResources.buttonColor),
                                labelColor: Colors.white,
                                unselectedLabelColor: MyResources.buttonColor,
                                isScrollable: true,
                                tabs: [
                                  Tab(
                                    child: Container(
                                      width: 48,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(50),
                                          border: Border.all(
                                              color: MyResources.buttonColor,
                                              width: 1)),
                                      child: const Align(
                                        alignment: Alignment.center,
                                        child: Text("Mon"),
                                      ),
                                    ),
                                  ),
                                  Tab(
                                    child: Container(
                                      width: 48,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(50),
                                          border: Border.all(
                                              color: MyResources.buttonColor,
                                              width: 1)),
                                      child: const Align(
                                        alignment: Alignment.center,
                                        child: Text("Tue"),
                                      ),
                                    ),
                                  ),
                                  Tab(
                                    child: Container(
                                      width: 48,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(50),
                                          border: Border.all(
                                              color: MyResources.buttonColor,
                                              width: 1)),
                                      child: const Align(
                                        alignment: Alignment.center,
                                        child: Text("Wed"),
                                      ),
                                    ),
                                  ),
                                  Tab(
                                    child: Container(
                                      width: 48,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(50),
                                          border: Border.all(
                                              color: MyResources.buttonColor,
                                              width: 1)),
                                      child: const Align(
                                        alignment: Alignment.center,
                                        child: Text("Thu"),
                                      ),
                                    ),
                                  ),
                                  Tab(
                                    child: Container(
                                      width: 48,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(50),
                                          border: Border.all(
                                              color: MyResources.buttonColor,
                                              width: 1)),
                                      child: const Align(
                                        alignment: Alignment.center,
                                        child: Text("Fri"),
                                      ),
                                    ),
                                  ),
                                  Tab(
                                    child: Container(
                                      width: 48,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(50),
                                          border: Border.all(
                                              color: MyResources.buttonColor,
                                              width: 1)),
                                      child: const Align(
                                        alignment: Alignment.center,
                                        child: Text("Sat"),
                                      ),
                                    ),
                                  ),
                                  Tab(
                                    child: Container(
                                      width: 48,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(50),
                                          border: Border.all(
                                              color: MyResources.buttonColor,
                                              width: 1)),
                                      child: const Align(
                                        alignment: Alignment.center,
                                        child: Text("Sun"),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                  margin: const EdgeInsets.only(top: 15),
                                  height: 190, //height of TabBarView
                                  child: TabBarView(children: <Widget>[
                                    Column(
                                      children: [
                                        slotList(ground.schedule[0]),
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        slotList(ground.schedule[1]),
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        slotList(ground.schedule[2]),
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        slotList(ground.schedule[3]),
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        slotList(ground.schedule[4]),
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        slotList(ground.schedule[5]),
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        slotList(ground.schedule[6]),
                                      ],
                                    ),
                                  ]))
                            ])),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(7.0),
              child: Card(
                elevation: 4,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        "Amenities",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      padding: const EdgeInsets.all(8),
                      child: Wrap(
                        children: [
                          Container(
                            margin: const EdgeInsets.all(5),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 12.0, vertical: 8),
                            decoration: BoxDecoration(
                                color: MyResources.buttonColor,
                                borderRadius: BorderRadius.circular(5)),
                            child: const Text(
                              "Parking",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(5),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 12.0, vertical: 8),
                            decoration: BoxDecoration(
                                color: MyResources.buttonColor,
                                borderRadius: BorderRadius.circular(5)),
                            child: const Text(
                              "Wickets",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(5),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 12.0, vertical: 8),
                            decoration: BoxDecoration(
                                color: MyResources.buttonColor,
                                borderRadius: BorderRadius.circular(5)),
                            child: const Text(
                              "Umpire",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(5),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 12.0, vertical: 8),
                            decoration: BoxDecoration(
                                color: MyResources.buttonColor,
                                borderRadius: BorderRadius.circular(5)),
                            child: const Text(
                              "Scorrer",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(5),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 12.0, vertical: 8),
                            decoration: BoxDecoration(
                                color: MyResources.buttonColor,
                                borderRadius: BorderRadius.circular(5)),
                            child: const Text(
                              "Washroom",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(5),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 12.0, vertical: 8),
                            decoration: BoxDecoration(
                                color: MyResources.buttonColor,
                                borderRadius: BorderRadius.circular(5)),
                            child: const Text(
                              "Balls",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(5),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 12.0, vertical: 8),
                            decoration: BoxDecoration(
                                color: MyResources.buttonColor,
                                borderRadius: BorderRadius.circular(5)),
                            child: const Text(
                              "Live Steaming",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            )
          ],
        ),
      ),
    );
  }

  Widget slotList(Template day) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 5),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.black, width: 1),
          borderRadius: BorderRadius.circular(3)),
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(3),
              color: Colors.grey.shade300,
            ),
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                Expanded(flex: 2, child: Center(child: Text("Slot no"))),
                Expanded(flex: 3, child: Center(child: Text("price"))),
                Expanded(flex: 3, child: Center(child: Text("Start Time"))),
                Expanded(flex: 2, child: Center(child: Text("Hours"))),
              ],
            ),
          ),
          SizedBox(
            height: 150,
            child: ListView.builder(
                padding: EdgeInsets.zero,
                itemCount: day.slots.length,
                itemBuilder: (BuildContext context, int index) {
                  Slot slot = day.slots[index];
                  return Card(
                    elevation: 4,
                    child: Container(
                      color: const Color(0xfff1f2f8),
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                              flex: 2,
                              child:
                              Center(child: Text((index + 1).toString()))),
                          Expanded(
                              flex: 3,
                              child:
                              Center(child: Text(formatnmbr(slot.price)))),
                          Expanded(
                              flex: 3,
                              child: Center(
                                  child: Text(slot.startTime
                                      .toString()
                                      .substring(10, 16)))),
                          Expanded(
                              flex: 2,
                              child:
                              Center(child: Text((slot.hours).toString()))),
                        ],
                      ),
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }
}
