import 'package:ap_booking_user/Screens/My%20booking/Models/booking_model.dart';
import 'package:ap_booking_user/constants/util.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class BookingConfirmPopUp extends StatelessWidget {
  final Booking booking;
  final VoidCallback pressFunc;
  final bool isNotDel;
  final int totalPrice;
  final bool meal;

  const BookingConfirmPopUp(
      {Key? key, required this.pressFunc, required this.booking, this.isNotDel = false, required this.totalPrice,required this.meal})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(50),
              topRight: Radius.circular(50),
            )),
        width: MediaQuery.of(context).size.width * 0.8,
        constraints: BoxConstraints(maxHeight: 300),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Ground :",style: TextStyle(fontWeight: FontWeight.bold),),
                      Divider(),
                      Text("Date :",style: TextStyle(fontWeight: FontWeight.bold),),
                      Divider(),
                      Text("Time :",style: TextStyle(fontWeight: FontWeight.bold),),
                      Divider(),
                      Text("Include Meal :",style: TextStyle(fontWeight: FontWeight.bold),),
                      Divider(),
                      Text("Price :",style: TextStyle(fontWeight: FontWeight.bold),),
                      Divider(),
                      Text("Advance :",style: TextStyle(fontWeight: FontWeight.bold),),

                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(booking.groundName),
                      Divider(),
                      Row(
                        children: [
                          Text(
                            DateFormat('EEE').format(booking.date) + "  ",
                            style: const TextStyle(
                                color: Colors.blue),
                          ),
                          Text(
                              DateFormat('dd-MMM-yyyy')
                                  .format(booking.date),
                             ),
                        ],
                      ),
                      Divider(),
                      Text(DateFormat('h: mm a')
                          .format(booking.slot.startTime)),
                      Divider(),
                      Text(meal?"Yes":"No"),
                      Divider(),
                      Text((totalPrice).toString()),
                      Divider(),
                      Text(((totalPrice*0.25).toInt()).toString()),
                    ],
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            Colors.green),
                        shape: MaterialStateProperty
                            .all<RoundedRectangleBorder>(RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.transparent)))),
                    onPressed: pressFunc,
                    child: Text(
                      'Confirm',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  ElevatedButton(
                      style: ButtonStyle(
                          backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.red.shade500),
                          shape:
                          MaterialStateProperty.all<RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18.0),
                                  side: BorderSide(
                                      color: Colors.transparent)))),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text('Cancel')),

                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}