import 'package:flutter/material.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Balance%20Sheet/Modals/entity_model.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Balance%20Sheet/entity_card.dart';
import 'package:rafi_admin/Controllers/balancesheet_controller.dart';
class EntityList extends StatelessWidget {
  final List<Entity> listData;
  const EntityList({Key? key, required this.listData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return listData.isNotEmpty
        ? RefreshIndicator(
      onRefresh: () async {
        return await BalanceSheetController().refreshAll();
      },
          child: ListView.builder(
      physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
      itemCount: listData.length,
      itemBuilder: (context, index) {
          final asset = listData[index];
          return EntityCard(entity: asset);
      },
    ),
        )
        : const Center(
      child: Text(
        'No results found',
        style: TextStyle(fontSize: 24),
      ),
    );
  }
}
