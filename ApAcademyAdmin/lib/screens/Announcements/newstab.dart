import 'package:ap_academy_admin/ModelView/Controller/newsController.dart';
import 'package:ap_academy_admin/screens/shared/DeletPop.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'add_news.dart';
import 'notifDetails.dart';

class NewsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      //Create an event add button
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 20.0, right: 10, left: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("", style: MyResources.appHeadingStyle),
                Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: Container(
                    width: Get.width * 0.2,
                    height: 25,
                    child: RaisedButton(
                      textColor: MyResources.buttontextColor,
                      color: MyResources.buttonColor,
                      child: Text(
                        "ADD",
                      ),
                      onPressed: () {
                        Get.to(() => AddNews());
                      },
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          GetX<NewsController>(
            init: Get.put<NewsController>(NewsController()),
            builder: (NewsController newsController) {
              if (newsController != null && newsController.news != null) {
                print(newsController.news.toString());
                return Expanded(
                  child: ListView.builder(
                    itemCount: newsController.news.length,
                    itemBuilder: (BuildContext context, int index) =>
                        getNewsCard(newsController.news[index]),
                  ),
                );
              } else {
                return Container(
                  child: LoadingWidget(),
                );
              }
            },
          ),
        ],
      ),
    );
  }

  Widget getNewsCard(Map<String, dynamic> newsModel) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: InkWell(
        onTap: () {
          Get.dialog(NotifyDetails(notificationModel: newsModel));
        },
        child: Card(
          color: MyResources.backgroundColor,
          child: Padding(
            padding: const EdgeInsets.all(8),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding:
                      const EdgeInsets.only(top: 10, left: 20, right: 20),
                  child: Icon(
                    Icons.mobile_friendly_outlined,
                    color: Colors.deepOrange[400],
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      //constraints: BoxConstraints(maxWidth: Get.width * 0.6),
                      width: Get.width * 0.6,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 4.0),
                            child: Container(
                              width: 120,
                              child: Text(newsModel['title'],
                                  overflow: TextOverflow.ellipsis,
                                  style: MyResources.appHeadingStyle),
                            ),
                          ),
                          Container(
                            width: 200,
                            child: Text(newsModel['description'],
                                overflow: TextOverflow.ellipsis,
                                maxLines: 3,
                                softWrap: false,
                                style: MyResources.appTextStyle),
                          ),
                        ],
                      ),
                    ),
                    Text(
                        DateFormat.Hm()
                            .add_yMEd()
                            .format(newsModel['timeCreated']),
                        style: MyResources.timestampStyle),
                  ],
                ),
                IconButton(
                    icon: Icon(
                      Icons.delete_forever,
                      color: Colors.red,
                    ),
                    onPressed: () async {
                      Get.dialog(Center(
                          child: DeletpopUp(
                            message: 'Are you sure you want to delete?',
                            pressFunc: () async {
                                final newsC = Get.put(NewsController());
                                await newsC.delnews(newsModel['_id']).then((value) {});
                              Get.back();
                            },
                          )));
                    }
                    )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
