import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:ap_academy_admin/screens/drawer_sreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

class PushMessagingExample extends StatefulWidget {
  @override
  _PushMessagingExampleState createState() => _PushMessagingExampleState();
}

class _PushMessagingExampleState extends State<PushMessagingExample> {
  String _homeScreenText = "Waiting for token...";
  String _messageText = "Waiting for message...";

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;

  @override
  void initState() {
    super.initState();
    fcmListner();
  }

  Future onDidReceiveLocalNotification(
      int id, String? title, String? body, String? payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    showDialog(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(title!),
        content: Text(body!),
        actions: [
          CupertinoDialogAction(
            isDefaultAction: true,
            child: Text('Ok'),
            onPressed: () async {
              Navigator.of(context, rootNavigator: true).pop();
              await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => HomeDrawer(),
                ),
              );
            },
          )
        ],
      ),
    );
  }

  openNotification(RemoteMessage msgData) async {
    const AndroidNotificationDetails androidPlatformChannelSpecifics =
    AndroidNotificationDetails(
        'com.aiksol.apacademy', 'AP Sports',
        channelDescription: 'Ap Sports academy ',
        importance: Importance.max,
        priority: Priority.high,
        showWhen: false);
    const NotificationDetails platformChannelSpecifics =
    NotificationDetails(android: androidPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
        0,
        msgData.notification!.title,
        msgData.notification!.body,
        platformChannelSpecifics,
        payload: 'item x');
  }

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  fcmListner() async {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      setState(() {
        _messageText = "Push Messaging message: $message";
      });
      //onDidReceiveLocalNotification(1, message['notification']['title'],
      //      message['notification']['body'], 'fff');
      openNotification(message);
      print("onMessage: $message");
    });
    FirebaseMessaging.onBackgroundMessage((RemoteMessage message) {
      return myBackgroundMessageHandler(message);
    });
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      setState(() {
        _messageText = "Push Messaging message: $message";
      });
      Get.to(() => HomeDrawer());
      print("onLaunch: $message");
    });


    const AndroidInitializationSettings initializationSettingsAndroid =
    AndroidInitializationSettings('@mipmap/ic_launcher');
    final IOSInitializationSettings initializationSettingsIOS =
    IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    final InitializationSettings initializationSettings =
    InitializationSettings(
        android: initializationSettingsAndroid,
        iOS: initializationSettingsIOS);
    flutterLocalNotificationsPlugin
        .initialize(initializationSettings)
        .then((value) {
      print('Received notofocation');
    });

    if (Platform.isIOS) {
      await _firebaseMessaging.requestPermission(
        alert: true,
        announcement: false,
        badge: true,
        carPlay: false,
        criticalAlert: false,
        provisional: false,
        sound: true,
      );
    }

    _firebaseMessaging.getToken().then((String? token) {
      assert(token != null);
      setState(() {
        _homeScreenText = "Push Messaging token: $token";
      });
      print(_homeScreenText);
    });
  }

  Future<dynamic> myBackgroundMessageHandler(
      RemoteMessage message) async {
    if (message.data != null) {
      // Handle data message
      final dynamic data = message.data;
      print(data.toString());
    }

    if (message.notification != null) {
      // Handle notification message
      final dynamic notification = message.notification;
      print(notification.toString());
    }

    // Or do other work.
  }

// Replace with server token from firebase console settings.
  final String serverToken =
      'AAAAjwzCMbM:APA91bGieQ3a0GCtMXTtuBbHYHS5Bgde2QeKMviyUkpAocFWk-5jtc5gVnAWcY59J2Gi7yJ47l6Rpf_LsPFsfyye-7DfLuHqeHQ5OXctscNoPucNRdP8j5rlVhyUxXNs2OLwpPy-YJbN';
  final FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;

  Future<Map<String, dynamic>> sendAndRetrieveMessage() async {
    if (Platform.isIOS) {
      await _firebaseMessaging.requestPermission(
        alert: true,
        announcement: false,
        badge: true,
        carPlay: false,
        criticalAlert: false,
        provisional: false,
        sound: true,
      );
    }

    await http.post(
      Uri.parse('https://fcm.googleapis.com/fcm/send'),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Authorization': 'key=$serverToken',
      },
      body: jsonEncode(
        <String, dynamic>{
          'notification': <String, dynamic>{
            'body': 'this notification for ap academy users',
            'title': 'Team added'
          },
          'priority': 'high',
          'data': <String, dynamic>{
            'click_action': 'FLUTTER_NOTIFICATION_CLICK',
            'id': '3',
            'status': 'done'
          },
          'to': await firebaseMessaging.getToken(),
        },
      ),
    );

    final Completer<Map<String, dynamic>> completer =
    Completer<Map<String, dynamic>>();

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
          (Map<String, dynamic> message) async {
        completer.complete(message);
      };
    });



    return completer.future;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Push Messaging Test'),
        ),
        body: Material(
          child: Column(
            children: <Widget>[
              Center(
                child: Text(_homeScreenText),
              ),
              Row(children: <Widget>[
                Expanded(
                  child: Text(_messageText),
                ),
                Center(
                  child: RaisedButton(
                    child: Text("get"),
                    onPressed: () async {
                      print(await sendAndRetrieveMessage());
                    },
                  ),
                )
              ])
            ],
          ),
        ));
  }
}
