import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';

class InfoWidget extends StatelessWidget {
  final String text;
  const InfoWidget({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          color: Colors.blue.shade50,
          borderRadius: BorderRadius.circular(5),
          border: Border.all(color: Colors.blue.shade100)),
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20.0,horizontal: 12),
            child: Icon(
              Ionicons.information_circle,
              size: 40,
              color: Color(0xff009cbd),
            ),
          ),
          Expanded(
              child: Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Text(
                  text,
                  style: TextStyle(
                      color: Colors.black45,
                      fontSize: 13.5,
                    fontWeight: FontWeight.w600
                  ),),
              ))
        ],
      ),
    );
  }
}
