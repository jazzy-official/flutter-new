import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:ap_ground_admin/controllers/staff_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';

import 'Models/staff_model.dart';

class StaffCard extends StatefulWidget {
  final Staff staff ;
  final Function() refresh;
  const StaffCard({Key? key, required this.staff, required this.refresh}) : super(key: key);

  @override
  State<StaffCard> createState() => _StaffCardState();
}

class _StaffCardState extends State<StaffCard> {
  bool isPresent = true;
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            CircleAvatar(
              backgroundImage: NetworkImage(widget.staff.imgUrl),
              radius: 40,
            ),
            const SizedBox(
              width: 10,
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          widget.staff.name,
                          style: const TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    ],
                  ),
                  Text(
                    "("+widget.staff.designation+")",
                    style: const TextStyle(fontSize: 12),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      const Icon(
                        Ionicons.call,
                        size: 18,
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Text(widget.staff.number),
                    ],
                  ),
                ],
              ),
            ),
            // Container(
            //   child: Switch(
            //     activeColor: Colors.green,
            //       value: widget.staff.ispresent,
            //       onChanged: (val) async {
            //         await StaffController().updateStaff(Staff(id:widget.staff.id,designation: widget.staff.designation, name: widget.staff.name, number: widget.staff.number, imgUrl: widget.staff.imgUrl, ispresent: !widget.staff.ispresent));
            //         displayToast(widget.staff.name+"'s attendance updated ");
            //         widget.refresh();
            //       }),
            // )
          ],
        ),
      ),
    );
  }
}
