import 'package:ap_academy_new/ModelView/controllers/newsController.dart';
import 'package:ap_academy_new/screens/Announcements/notifDetails.dart';
import 'package:ap_academy_new/shared/loading_widget.dart';
import 'package:ap_academy_new/Constants/util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class NewsScreen extends StatelessWidget {
  const NewsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      //Create an event add button
      child: Column(
        children: [
          GetX<NewsController>(
            init: Get.put<NewsController>(NewsController()),
            builder: (NewsController? newsController) {
              if (newsController != null && newsController.news != null) {
                return Expanded(
                  child: ListView.builder(
                    itemCount: newsController.news!.length,
                    itemBuilder: (BuildContext context, int index) =>
                        getNewsCard(newsController.news![index]),
                  ),
                );
              } else {
                return const LoadingWidget();
              }
            },
          ),
        ],
      ),
    );
  }

  Widget getNewsCard(Map<String, dynamic> newsModel) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: SizedBox(
        height: 120,
        //create the cards for all details of event happening
        child: InkWell(
          onTap: () {
            Get.dialog(NotifyDetails(
              notificationModel: newsModel,
            ));
          },
          child: Card(
            elevation: 4,
            color: myResources.backgroundColor,
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 10, left: 20, right: 20),
                    child: Icon(
                      Icons.mobile_friendly_outlined,
                      color: Colors.deepOrange[400],
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        constraints: BoxConstraints(maxWidth: Get.width * 0.6),
                        width: Get.width * 0.5,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(bottom: 4.0),
                              child: SizedBox(
                                width: 120,
                                child: Text(newsModel['title'],
                                    overflow: TextOverflow.ellipsis,
                                    style: myResources.appHeadingStyle),
                              ),
                            ),
                            SizedBox(
                              width: 200,
                              child: Text(newsModel['description'],
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 3,
                                  softWrap: false,
                                  style: myResources.appTextStyle),
                            ),
                          ],
                        ),
                      ),
                      Text(
                          DateFormat.Hm()
                              .add_yMEd()
                              .format(newsModel['timeCreated']),
                          style: myResources.timestampStyle),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
