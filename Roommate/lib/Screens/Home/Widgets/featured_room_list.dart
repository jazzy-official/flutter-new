import 'package:flutter/material.dart';
import 'package:room_buddy/Screens/Home/Widgets/feature_list_room_card.dart';

class FeaturedRoomList extends StatelessWidget {
  const FeaturedRoomList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width-30,
      height: 270,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: 3,
        itemBuilder: (context, index) {
          return const FeatureListRoomCard();
        },
      ),
    );
  }
}
