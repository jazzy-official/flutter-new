class NotificationModel {
  String id;
  String title;
  String type;
  String event;
  String notifyteamId;
  String description;
  String timeCreated;

  NotificationModel(
      {required this.id,
      required this.title,
      required this.type,
      required this.notifyteamId,
      required this.description,
      required this.event,
      required this.timeCreated});

  factory NotificationModel.fromJson(Map<String, dynamic> json) {
    return NotificationModel(id : json['id'],
        title : json['title'],
        type : json['type'],
        event : json['event'],
        notifyteamId : json['notifyteamId'],
        description : json['description'],
        timeCreated : json['timeCreated'],);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['type'] = this.type;
    data['event'] = this.event;
    data['notifyteamId'] = this.notifyteamId;
    data['description'] = this.description;
    data['timeCreated'] = this.timeCreated;
    return data;
  }
}
