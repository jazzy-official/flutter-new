import 'package:get/get.dart';
import 'package:mongo_dart/mongo_dart.dart';

import 'db_controller.dart';

class TrainingController extends GetxController {
  Rx<List<Map<String, dynamic>>> trainingsList =
      Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get training => trainingsList.value;
  final dbHelper = Get.put(DatabaseController());
  @override
  Future<void> onInit() async {
    // await db.open();

    // trainingsList.bindStream(
    //     DatabaseController().trainingStream()); //id)); //Stream Comming from
    // super.onInit();
  }

  refrehData() {
    trainingsList.bindStream(DatabaseController().trainingStream());
  }

  Future<bool> saveAttend(ObjectId trainingId) async {
    try {
      bool attend = await dbHelper.saveAttendance(trainingId);
      return attend;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }

  Future<bool> deletAttend(ObjectId trainingId) async {
    try {
      bool attend = await dbHelper.deletAttendance(trainingId);
      return attend;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}
