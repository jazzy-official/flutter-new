import 'package:ap_academy_new/Constants/util.dart';
import 'package:ap_academy_new/Screens/Gallery/photos_tabs.dart';
import 'package:ap_academy_new/Screens/Gallery/videos_tabs.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class GalleryAlbum extends StatefulWidget {
  final int i;

  const GalleryAlbum({Key? key, required this.i}) : super(key: key);
  @override
  _GalleryAlbumState createState() => _GalleryAlbumState();
}

class _GalleryAlbumState extends State<GalleryAlbum> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: myResources.buttonColor,

          title: Container(
            child: Text(
              'ALBUMS',
              style: GoogleFonts.roboto(),
            ),
          ),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                height: 600,
                child: DefaultTabController(
                    length: 2,
                    child: Scaffold(
                      appBar: AppBar(
                        backgroundColor: myResources.buttonColor,
                        toolbarHeight: 0,
                        bottom: TabBar(tabs: [
                          Tab(
                            text: 'Photos',
                          ),
                          Tab(
                            text: 'Videos',
                          ),
                        ]),
                      ),
                      body: TabBarView(
                        children: [
                          PhotosTab(i: widget.i),
                          VideosTab(i: widget.i, title: '',),
                        ],
                      ),
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
