import 'dart:ui';
import 'package:ap_academy_admin/ModelView/Controller/matchController.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:get/get.dart';

class TeamsDetails extends StatelessWidget {
  final Map<String, dynamic>? team1Model;
  final mController = Get.put(MatchController());

  TeamsDetails({
    Key? key,
    this.team1Model,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 0.0, left: 10, right: 10),
      child: Center(
        child: Container(
          height: Get.height * 0.7,
          //create the cards for all details of event happening
          child: SingleChildScrollView(
            child: Column(
              children: [
                Card(
                  color: MyResources.backgroundColor,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Center(
                          child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          padding: EdgeInsets.all(10),
                          height: 50,
                          color: MyResources.buttonColor,
                          width: Get.width,
                          child: Text(
                            "TEAM DETAILS",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      )),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                child: Text("Team :",
                                    style: MyResources.titleTextStyle),
                              ),
                              Text("Type:", style: MyResources.titleTextStyle),
                              Text("Coach:", style: MyResources.titleTextStyle),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(team1Model!['teamName'],
                                  style: MyResources.nameTextStyle),
                              Text(team1Model!['coach'],
                                  style: MyResources.nameTextStyle),
                            ],
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 10,
                          right: 100,
                          left: 10,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      //members
                      Center(
                        child: Text(
                          "Members",
                          style: MyResources.appHeadingStyle,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Column(
                        children: [
                          Container(
                              width: Get.width * 0.8,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10),
                                      topRight: Radius.circular(10)),
                                  color: MyResources.buttonColor),
                              height: 40,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text(
                                    'Sr#',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  Text(
                                    'name     ',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  Text('email      ',
                                      style: TextStyle(color: Colors.white))
                                ],
                              )),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                          height: Get.height * 0.7,
                          width: Get.width * 0.8,
                          child: ListView.builder(
                              itemCount: team1Model!['teamMembers'] != null
                                  ? team1Model!['teamMembers'].length
                                  : 0,
                              itemBuilder: (BuildContext context, int index) {
                                return getTeamMembersCard(
                                    team1Model!['teamMembers'][index],
                                    index + 1);
                              })),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget getTeamMembersCard(Map<String, dynamic> memeberslist, int i) {
    return Column(
      children: [
        Container(
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.transparent,
                    //  offset: Offset(0, 1),
                  ),
                ],
                borderRadius: BorderRadius.circular(5),
                color: Colors.transparent //blue[100]
                ),
            height: 40,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(i.toString()),
                ),
                Container(width: 100, child: Text(memeberslist['name'])),
                Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Container(
                    width: 100,
                    child: Text(
                      memeberslist['email'],
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                )
              ],
            )),
        SizedBox(
          height: 5,
        )
      ],
    );
  }
}
