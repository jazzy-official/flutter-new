import 'dart:convert';

import 'package:ap_ground_admin/Screens/Authentication/Models/user_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';

import 'fire_base_controller.dart';

class UserController {
  final _auth = FirebaseAuth.instance;
  final DatabaseReference databaseReference =
      FirebaseDatabase.instance.reference().child("Users");

  Future<void> saveUser(UserModel user) async {
    try {
      await databaseReference.push().set(user.toJson());
    } catch (e) {
      displayToast(e.toString());
    }
  }
  Future<void> updateUser(UserModel user) async {
    databaseReference.update({user.id!: user.toJson()});
  }
  Future<void> deleteUser(String id) async {
    databaseReference.child(id).remove();
  }
  Future<UserModel> readData(String uid) async {
    late UserModel user;
    await databaseReference
        .orderByChild("uid")
        .equalTo(uid)
        .once()
        .then((value) => {
              if (value.value != null)
                {
                  Map.of(value.value).forEach((key, value) {
                    user = UserModel.fromJson(value, key);
                  })
                }
              else
                {
                  user = UserModel(
                      name: _auth.currentUser!.displayName!,
                      email: _auth.currentUser!.email!,
                      imgUrl: _auth.currentUser!.photoURL!,
                      uid: _auth.currentUser!.uid,
                      role: "admin"),
                }
            });

    return user;
  }
  Future<List<UserModel>> readAdmins() async {
    List<UserModel> userlist = [];
    late List<UserModel> admins;
    await databaseReference.once().then((DataSnapshot snapshot) {
      print("im in");
      if(snapshot.value != null){
        snapshot.value.forEach((key, value) async {
          UserModel gr = UserModel.fromJson(value,key);
          userlist.add(gr);
           admins = userlist.where((element) => element.role == "Admin").toList();
        });
      }
    });
    return admins;
  }
  Future<bool> checkRole(String email) async {
    late bool isAdmin;
    late UserModel user;
    await databaseReference
        .orderByChild("email")
        .equalTo(email)
        .once()
        .then((value) => {
      if (value.value != null)
        {
          Map.of(value.value).forEach((key, value) {
             user = UserModel.fromJson(value, key);
          }),
          if(user.role == "Admin"){
            isAdmin = true
          }else{
            isAdmin = false
          }
        }
      else
        {
          displayToast("User not exist"),
        }
    });
    return isAdmin;
  }

// void readGround(){
//   databaseReference.child("Grounds").once().then((DataSnapshot snapshot) {
//     print('Data : ${snapshot.value}');
//   });
// }
}
