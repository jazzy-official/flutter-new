import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Screens/Messaging/inbox_screen.dart';

class MessageList extends StatelessWidget {
  const MessageList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
      itemCount: 3,
      itemBuilder: (context, index) {
        return  InkWell(
          onTap: (){
            Get.to(InboxScreen());
          },
          child: Column(
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Row(
                  children:  [
                    const Padding(
                      padding: EdgeInsets.all(10.0),
                      child: CircleAvatar(
                        backgroundImage: AssetImage("assets/me.jpg"),
                        radius: 30,

                      ),
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children:  [
                        Container(
                          padding: EdgeInsets.only(right: 10),
                          width: MediaQuery.of(context).size.width-80,
                          child: Row(
                            children:  [
                              Expanded(child: Text("Muhammad Jahanzaib",style: const TextStyle(fontSize: 16,fontWeight: FontWeight.w600,overflow: TextOverflow.fade),)),
                              Text("04:30 PM",style: TextStyle(color: index==0? myResources.themeColor:Colors.black54,fontWeight: FontWeight.w600,fontSize: 15),)

                            ],
                          ),
                        ),
                        const SizedBox(height: 5,),
                        Container(
                          padding: EdgeInsets.only(right: 10),
                          width: MediaQuery.of(context).size.width-80,
                          child: Row(
                            children:  [
                              const Expanded(child: Text("message message message message message message",style: TextStyle(color: Colors.black54,),overflow: TextOverflow.ellipsis,softWrap: false,)),
                              if(index==0)
                              Icon(Icons.circle,size: 12,color:myResources.themeColor,)
                            ],
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Divider(),
            ],
          ),
        );
      },
    );
  }
}
