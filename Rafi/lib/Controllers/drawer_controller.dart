import 'dart:convert';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'package:rafi_admin/Components/Drawer/ProjectModel/project.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/RecievePaidChecklist/receive_paid_checklist.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/Recovery/recovery.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/account%20model/accounts.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Expenses/Models/expense_model.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Payables/Model/payable_model.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Receivable/Model/receivable_model.dart';
import 'package:rafi_admin/Controllers/authcontroller.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DrawerrController extends GetxController {
  final auth = Get.put(AuthController());

  List<Project>? projects;

  String? token;
  int? number;
  @override
  Future<void> onInit() async {
    await refreshAll();
    super.onInit();
  }

  Future refreshAll() async {
    token = await auth.getToken();
    number = await auth.getProjectId();
    // await setExpense();
    await refreshProjectList();

  }
  Future refreshProjectList() async {

    http.Response response = await http.get(
        Uri.parse(
            'https://raccounting.azurewebsites.net/api/Users/AssignedPorjects'),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        });
    if (response.statusCode == 200) {
      Map<String, dynamic> responseBody =
      await jsonDecode(jsonDecode(response.body));
      List<dynamic> data = responseBody["ProjectsList"];
      List<Project> projectss = data
          .map(
            (dynamic item) => Project.fromJson(item),
      )
          .toList();
      projects =  projectss;
      update();

    } else {
      throw "Unable to retrieve projects.";
    }
  }


}
