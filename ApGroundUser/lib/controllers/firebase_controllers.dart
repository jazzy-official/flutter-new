import 'package:ap_booking_user/Screens/Authentication/Models/user_model.dart';
import 'package:ap_booking_user/Screens/Home/home_screen.dart';
import 'package:ap_booking_user/controllers/user_controller.dart';
import 'package:ap_booking_user/wrapper.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';


final _auth = FirebaseAuth.instance;

Future signup (String e, String p,BuildContext context) async {
  try {
    final newUser = await _auth.createUserWithEmailAndPassword(email: e, password: p,);
    Navigator.pushReplacement<void, void>(
      context,
      MaterialPageRoute<void>(
        builder: (BuildContext context) => const Wrapper(),
      ),
    );
  }
  catch (e)
  {
    displayToast(e.toString());
    print(e);
  }
}

Future<bool> Login(String e,String p)async{
  try {
    final newUser = await _auth.signInWithEmailAndPassword(
        email: e, password: p);

    return (newUser != null) ? true
    //successfully login
    //navigate the user to main page
    // i am just showing toast message here
        : false;

  } catch (e) {
    displayToast(e.toString());
    print(e.toString());
    return false;
  }
}


Future<void> signInWithGoogle(BuildContext context) async {
  // Trigger the authentication flow
  final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

  // Obtain the auth details from the request
  final GoogleSignInAuthentication? googleAuth = await googleUser?.authentication;

  // Create a new credential
  final credential = GoogleAuthProvider.credential(
    accessToken: googleAuth?.accessToken,
    idToken: googleAuth?.idToken,
  );

  // Once signed in, return the UserCredential
  try {
    await FirebaseAuth.instance.signInWithCredential(
        credential).then((value)  async{
      User? firebaseUser = value.user!;
      if (await checkdublicate(firebaseUser.uid) == false) {
        final _user = UserModel(
          email: firebaseUser.email!,
          imgUrl: firebaseUser.photoURL!,
          role: 'User',
          uid: firebaseUser.uid,
          name: firebaseUser.displayName!,

        );
        await UserController().saveUser(_user);
      }

    });
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const Wrapper()),
    );
    displayToast("Welcome");
  } catch(e){
    displayToast(e.toString());
  }
}

Future<bool> checkdublicate(String firebaseId) async {
  try {
    UserModel usr = await UserController().readData(firebaseId);

    if (usr.id != null) {
      print("true");
      return true;
    } else {
      print("false");
      return false;
    }
  } catch (ex) {
    print(ex.toString());
    return false;
  }
}


Future<void> resetPassword(String e,BuildContext context) async {
  try {
    await _auth.sendPasswordResetEmail(email: e);
    displayToast("Verification Email is sent");
    Navigator.pop(context);
  } catch (e) {
    displayToast(e.toString());
  }
}

displayToast(String message){
  Fluttertoast.showToast(msg: message,backgroundColor: Colors.black45,);
}