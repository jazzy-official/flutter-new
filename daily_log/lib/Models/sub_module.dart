class SubModule {
  final String? id;
  final String name;
  String? doc;
  bool status;


  SubModule( {required this.status,required this.name,this.id,this.doc});



  factory SubModule.fromJson(dynamic json, key) {
    return SubModule(
      id: key.toString(),
      name: json['name'],
      status : json['status'],
      doc : json['doc'],

    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['status'] = status;
    data['doc'] = doc;
    return data;
  }

  static Map<dynamic, dynamic> toSubModuleMap(List<SubModule> cartItems) {
    Map<dynamic, dynamic> orderMap =  <dynamic, dynamic>{};
    for (SubModule cart in cartItems) {
      orderMap[cart.id] = {
        'name': cart.name,
        'status': cart.status,
        'doc': cart.doc,

      };
    }
    return orderMap;
  }

  static fromSubModuleMap(var map) {
    Map values = map as Map;
    List<SubModule> cartItem = [];
    values.forEach((key, data) {
      final SubModule connect = SubModule.fromJson(data,key);
      cartItem.add(connect);
    });
    return cartItem;
  }
}