class Product{
  final int   id;
  final String title;
  final String imageUrl;
  final String description;
  final double price;
  int selectedQuantity;
  bool isFavourite;

  Product(
      {required this.id,
        required this.title,
        required this.imageUrl,
        required this.description,
        required this.price,
        required this.selectedQuantity,
        this.isFavourite = false});

}