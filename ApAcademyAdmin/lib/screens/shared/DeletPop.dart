import 'package:ap_academy_admin/constants/util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DeletpopUp extends StatelessWidget {
  final String? message;
  final Function? pressFunc;
  final bool? isNotDel;

  const DeletpopUp(
      {Key? key, this.pressFunc, this.message, this.isNotDel = false})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(50),
              topRight: Radius.circular(50),
            )),
        width: Get.width * 0.8,
        height: 180,
        constraints: BoxConstraints(maxHeight: 200),
        child: Padding(
          padding: const EdgeInsets.only(top:8.0,bottom:10.0),
          child: Column(
            children: [
              Container(
                  child: Icon(
                isNotDel != null ? Icons.warning : Icons.delete_forever,
                color: Colors.red,
                size: 40,
              )),
              Container(
                //width: ,
                child: Text(
                  message!,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18),
                ),
                height: 40,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all<Color>(Colors.green),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                      side: BorderSide(
                                          color: Colors.transparent)))),
                      onPressed: () {
                        Get.back();
                      },
                      child: Text('No')),
                  SizedBox(
                    width: 15,
                  ),
                  ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            MyResources.buttonColor),
                        shape: MaterialStateProperty
                            .all<RoundedRectangleBorder>(RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                                side: BorderSide(color: Colors.transparent)))),
                    onPressed:() => pressFunc!(),
                    child: Text(
                      'Yes',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
