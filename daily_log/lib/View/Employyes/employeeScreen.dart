import 'package:daily_log/Controllers/employeedetailcontroller.dart';
import 'package:daily_log/Controllers/employees_controller.dart';
import 'package:daily_log/Models/user_model.dart';
import 'package:daily_log/View/Employyes/employee_detail.dart';
import 'package:daily_log/View/Employyes/employee_week.dart';
import 'package:daily_log/constants/util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';

import '../../Controllers/daily_log_controller.dart';

class EmployeeScreen extends StatelessWidget {
   EmployeeScreen({Key? key}) : super(key: key);
  var empdetail = Get.put(EmployeeDetailController());
  @override
  Widget build(BuildContext context) {
    return GetBuilder<EmployeeController>(
        autoRemove: false,
        init: EmployeeController(),
        builder: (cont) => RefreshIndicator(
          onRefresh: () { return cont.readUsers(); },
          child: GridView.builder(
            physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 20,
              childAspectRatio: 0.8
            ),
            itemCount: cont.employees.length,
            itemBuilder: (context, index) {
              UserModel employe = cont.employees[index];
              return InkWell(
                onTap: (){
                  empdetail.employeeeid = employe.id;
                  empdetail.employeeename = employe.name;
                  cont.update();
                  empdetail.filtercomments();
                  empdetail.getTasks();
                  Get.to( EmployeeDetail(einame: employe.name,));

                  // logcont.employeefilter(employe.id!);
                  // Get.to(EmployeeWeek(employeeid: employe.id!));
                },
                child: Column(
                   children:  [
                     Padding(
                       padding: EdgeInsets.all(8.0),
                       child: CircleAvatar(
                         radius: 60,
                         backgroundImage: employe.imgUrl != ""?NetworkImage(employe.imgUrl!) : AssetImage("assets/hacker.png") as ImageProvider,
                       ),
                     ),
                     Text(employe.name,style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),overflow: TextOverflow.ellipsis,),
                     Text(employe.email,style: TextStyle(fontSize: 12),),
                   ],
                 ),
              );
            },
          ),
        )
    );
  }
}
