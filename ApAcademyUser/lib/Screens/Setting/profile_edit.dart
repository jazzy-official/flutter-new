import 'dart:io';
import 'package:ap_academy_new/ModelView/controllers/profileController.dart';
import 'package:ap_academy_new/Models/UserModel.dart';
import 'package:ap_academy_new/Screens/drawer_screen.dart';
import 'package:ap_academy_new/shared/loading_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ap_academy_new/Constants/util.dart';
import 'package:intl/intl.dart';

//import 'package:image_picker/image_picker.dart';

class ProfileEdit extends StatefulWidget {
  final Map<String, dynamic>? usrDetails;
  final bool? isDrawer;
  final bool isapproved;

  const ProfileEdit({Key? key, this.usrDetails, this.isDrawer,required this.isapproved})
      : super(key: key);

  @override
  _ProfileEditState createState() => _ProfileEditState();
}

class _ProfileEditState extends State<ProfileEdit> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final profileController = Get.put(ProfileController());
  bool _autoValidate = false;
  late UserModel user;
  late String _error;
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController surnameController = TextEditingController();
  TextEditingController dobController = TextEditingController();

  bool addEmail = false;
  bool addfacebook = false;
  bool addgoogle = false;
  bool addwechat = false;
  bool isImage = false;
  File? _image;
  String imageString ='';
  List<File> images=[];
  List<dynamic>? imageUrls;
  DateTime _dateOfBirth = DateTime(1999);

  changeText(int choice) {
    switch (choice) {
      case 1:
        {
          if (addfacebook) {
            addfacebook = false;
          } else {
            addfacebook = true;
          }
        }
        break;

      case 2:
        {
          if (addgoogle) {
            addgoogle = false;
          } else {
            addgoogle = true;
          }
          setState(() {});
        }
        break;

      case 3:
        {
          if (addwechat) {
            addwechat = false;
          } else {
            addwechat = true;
          }
          setState(() {});
        }
        break;
    }
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    print("ZZZZZZZZZZZZZZZZZZZZ");
    if (widget.usrDetails != null) {
      emailController.text = widget.usrDetails!['email']??"";
      nameController.text = widget.usrDetails!['name']??"";
      surnameController.text = widget.usrDetails!['surname']??"";
      phoneController.text = widget.usrDetails!['phone']??"";
      dobController.text = widget.usrDetails!['dob']??"";
      imageString = widget.usrDetails!['photoUrl']??"";
      imageUrls = widget.usrDetails!['documents']??[];
    } else if (profileController.currentUdetail != null)
    {
      emailController.text = profileController.currentUdetail['email']??"";
      nameController.text = profileController.currentUdetail['name']??"";

      surnameController.text = profileController.currentUdetail['surname']??"";
      phoneController.text = profileController.currentUdetail['phone']??"";
      dobController.text = profileController.currentUdetail['dob'] != null
          ? DateFormat.yMEd().format(profileController.currentUdetail['dob'])
          : "";
      setState(() {
        _dateOfBirth = profileController.currentUdetail['dob'] != null
            ? profileController.currentUdetail['dob']:DateTime(1995);
      });
      imageString = profileController.currentUdetail['photoUrl']??"";
      imageUrls = profileController.currentUdetail['documents']??[];
    }
  }

  Future<void> pickImages() async {
    setState(() {
      isImage = true;
    });
    //List<File> resultList = new List<File>();

    String? error;

    try {
      File result = await profileController.imgFromGallery();

      images.add(result);
    } on PlatformException catch (e) {
      error = e.message!;
      setState(() {
        isImage = false;
      });
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;
    setState(() {
      if (error == null) _error = 'No Error Detected';
    });
  }

  @override
  void dispose() {
    nameController.dispose();
    emailController.dispose();
    dobController.dispose();
    phoneController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: myResources.buttonColor,
        title: Container(
          child: Text(
            'Edit Profile',
            style: GoogleFonts.roboto(),
          ),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              Center(
                child: GestureDetector(
                  onTap: () {
                    _showPicker(context);
                  },
                  child: CircleAvatar(
                    radius: 55,
                    backgroundColor: Colors.blue,
                    child: _image != null
                        ? ClipRRect(
                            borderRadius: BorderRadius.circular(50),
                            child: Image.file(
                              _image!,
                              width: 100,
                              height: 100,
                              fit: BoxFit.cover,
                            ),
                          )
                        : imageString != ''
                            ? ClipRRect(
                                borderRadius: BorderRadius.circular(50),
                                child: Image.network(
                                  imageString,
                                  width: 100,
                                  height: 100,
                                  fit: BoxFit.cover,
                                ),
                              )
                            : Container(
                                decoration: BoxDecoration(
                                    color: Colors.grey[200],
                                    borderRadius: BorderRadius.circular(50)),
                                width: 100,
                                height: 100,
                                child: Icon(
                                  Icons.camera_alt,
                                  color: Colors.grey[800],
                                ),
                              ),
                  ),
                ),
              ),
              if (widget.isapproved == false) const Padding(
                padding: EdgeInsets.symmetric(horizontal: 15,vertical: 5),
                child: Center(child: Text("Your Account is pending Approved."
                    "Please fill all the details wait until admin approves your account",style: TextStyle(color: Colors.red),)),
              ),
              //add the textfields to edit the profile information
              Padding(
                //Add padding around textfield
                padding: const EdgeInsets.only(top: 25.0, left: 10, right: 10),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  validator: (String? arg) {
                    if (arg!.length < 3) {
                      return 'Name must be more than two characters';
                    } else {
                      return null;
                    }
                  },
                  controller: nameController,
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    hintText: "Name",
                    hintStyle: myResources.hintfontStyle,
                    prefixIcon: Icon(Icons.account_circle),

                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
              Padding(
                //Add padding around textfield
                padding: EdgeInsets.only(top: 25.0, left: 10, right: 10),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  validator: (String? arg) {
                    if (arg!.length < 3)
                      return 'Name must be more than Two characters';
                    else
                      return null;
                  },
                  controller: surnameController,
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    hintText: "Parent Name",
                    hintStyle: myResources.hintfontStyle,
                    prefixIcon: Icon(Icons.account_circle),

                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
              Padding(
                //Add padding around textfield
                padding: EdgeInsets.only(top: 25.0, left: 10, right: 10),
                child: TextFormField(
                  keyboardType: TextInputType.phone,
                  validator: (String? value) {
                    if (value!.length == 0) {
                      return 'Please enter your mobile number';
                    } else if (!value.isPhoneNumber) {
                      return 'Please enter a valid mobile number';
                    }
                    return null;
                  },
                  controller: phoneController,
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    hintText: "Phone",
                    hintStyle: myResources.hintfontStyle,
                    prefixIcon: Icon(Icons.phone),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
              MaterialButton(
                onPressed: () async {
                  _dateOfBirth = (await showDatePicker(
                    // initialEntryMode: DatePickerEntryMode.input,
                    context: context,
                    currentDate: DateTime.now(),
                    initialDate: DateTime(2000),
                    lastDate: DateTime.now(),
                    initialDatePickerMode: DatePickerMode.year,
                    firstDate: DateTime(1980),
                  ))!;
                  dobController.text =
                      DateFormat('yyyy-MM-dd').format(_dateOfBirth);
                },
                child: Padding(
                  //Add padding around textfield
                  padding: EdgeInsets.only(top: 25.0, left: 0, right: 0),
                  child: Container(
                    child: TextFormField(
                      enabled: false,
                      keyboardType: TextInputType.text,
                      validator: (String? arg) {
                        if (arg!.length < 3)
                          return 'Select date of Birth';
                        else
                          return null;
                      },
                      readOnly: true,
                      controller: dobController,
                      decoration: InputDecoration(
                        //Add th Hint text here.
                        hintText: "Date of Birth",
                        hintStyle: myResources.hintfontStyle,
                        prefixIcon: Icon(Icons.calendar_today_outlined),

                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              //upload documents
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 0.0, top: 10),
                child: Text(
                  "Upload your documents  ",
                  textAlign: TextAlign.left,
                  style: myResources.appHeadingStyle,
                ),
              ),
              Padding(
                //Add padding around textfield
                padding: EdgeInsets.only(top: 10.0, left: 7, right: 7),
                child: Container(
                  height: 140,
                  width: Get.width,
                  child: Card(
                    color: myResources.backgroundColor,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          height: 100,
                          width: Get.width,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              isImage
                                  ? Expanded(
                                      flex: 3,
                                      child: GridView.count(
                                        scrollDirection: Axis.horizontal,
                                        crossAxisCount: 1,
                                        shrinkWrap: true,
                                        children: List.generate(images.length,
                                            (index) {
                                          return Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Image.file(
                                              File(images[index].path),
                                              width: 100,
                                              height: 100,
                                            ),
                                          );
                                        }),
                                      ),
                                    )
                                  : imageUrls != null
                                      ? Expanded(
                                          flex: 3,
                                          child: GridView.count(
                                            scrollDirection: Axis.horizontal,
                                            crossAxisCount: 1,
                                            shrinkWrap: true,
                                            children: List.generate(
                                                imageUrls!.length, (index) {
                                              return Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Image.network(
                                                  imageUrls![index].toString(),
                                                  width: 100,
                                                  height: 100,
                                                ),
                                              );
                                            }),
                                          ),
                                        )
                                      : Container(),
                              Padding(
                                padding: const EdgeInsets.only(right: 10.0),
                                child: InkWell(
                                  onTap: () async {
                                    if (images.length < 5) {
                                      await pickImages();
                                    } else {
                                      Fluttertoast.showToast(
                                          msg:
                                              'You can only add five document pictures');
                                    }
                                    setState(() {
                                      //     isImage=isImage? false:true;
                                    });
                                  },
                                  child: Icon(
                                    Icons.photo_library,
                                    size: 30,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),

              Padding(
                padding:
                    EdgeInsets.only(top: 15.0, left: 10, right: 10, bottom: 20),
                child: Container(
                  width: Get.width * 0.7,
                  height: 45,
                  child: RaisedButton(
                    textColor: Colors.white,
                    color: myResources.buttonColor,
                    child: Text(
                      "Save",
                    ),
                    onPressed: () async {
                      if (dobController.text.isNotEmpty) {
                        if (_formKey.currentState!.validate()) {
                          Map<String, dynamic> _user = {
                            'email': emailController.text,
                            'phone': phoneController.text,
                            'parentname': surnameController.text,
                            'name': nameController.text,
                            'dob': DateTime(_dateOfBirth.year,
                                _dateOfBirth.month, _dateOfBirth.day + 1),
                          };
                          Get.defaultDialog(
                              title: "Saving Information ",
                              content: LoadingWidget());

                          bool temp = await profileController.saveProfile(
                              _image, images, _user);

                          if (temp) {
                            Get.back();
                            // await profileController.initializeUserData();
                            if (widget.isapproved == true) {
                              Get.offAll(
                                () => HomeDrawer(),
                              );
                            } else {
                              Get.defaultDialog(
                                  title: "Saved",
                                  content: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Column(
                                      children: [
                                        Text(
                                            'Your data is saved , wait for approval and restart application'),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Center(
                                          child: ColorFiltered(
                                              colorFilter: ColorFilter.mode(
                                                  Colors.white.withOpacity(0.3),
                                                  BlendMode.dstATop),
                                              child: Image.asset(
                                                'assets/happy.png',
                                                colorBlendMode: BlendMode.hue,
                                                width: 100,
                                                height: 100,
                                              )),
                                        ),
                                      ],
                                    ),
                                  ));
                            }
                          } else {
                            Get.snackbar(
                                "Error", "User information is not updated");
                          }
                        }
                      } else {
                        Get.rawSnackbar(
                            title: "Error",
                            message: "Select Date of Birth",
                            icon: Icon(
                              Icons.warning,
                              color: Colors.orange[100],
                            ));
                      }
                    },
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () async {
                        Get.back();
                        await profileController.imgFromGallery().then((value) {
                          _image = value;
                          if (mounted) setState(() {});
                        });
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () async {
                      Get.back();
                      await profileController.imgFromCamera().then((value) {
                        _image = value;
                        if (mounted) setState(() {});
                      });
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }
}
