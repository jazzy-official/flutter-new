import 'package:ap_academy_admin/ModelView/Controller/NotificationController.dart';
import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:get/get.dart';
import 'package:mongo_dart/mongo_dart.dart';

class NewsController extends GetxController {
  Rx<List<Map<String, dynamic>>> newsList = Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get news => newsList.value;
  final notifController = Get.put(NotificationController());
  @override
  Future<void> onInit() async {
    // await db.open();
    // String id = Get.find<AuthController>().firebaseUser.uid;
    newsList.bindStream(
        DatabaseController().newsStream()); //id)); //Stream Comming from
    super.onInit();
  }

  Future<bool> delnews(ObjectId id) async {
    if (await DatabaseController().deletnews(id)) {
      refreshDate();
      return true;
    } else {
      return false;
    }
  }

  refreshDate() async {
    this.newsList.bindStream(DatabaseController().newsStream());
  }
}
