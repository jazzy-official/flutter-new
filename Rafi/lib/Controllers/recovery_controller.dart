
import 'dart:convert';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/RecievePaidChecklist/receive_paid_checklist.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/Recovery/recovery.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/account%20model/accounts.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Payables/Model/payable_model.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Receivable/Model/receivable_model.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Recovery/Modal/recovery_model.dart';
import 'package:rafi_admin/Controllers/authcontroller.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RecoveryController extends GetxController {
  final auth = Get.put(AuthController());
  List<Recovery>? recoveries;
  late List<Recovery> foundResult = [];
  String? token;
  int? number;
  List<Recovery> results = [];
  List<Recovery> results2 = [];
  List<Recovery> results3 = [];
  List<Recovery> results4 = [];
  bool isName = false;
  bool isUnit = false;
  bool isMarla = false;
  bool isDate = false;
  @override
  Future<void> onInit() async {
    await refreshAll();
    super.onInit();
  }

  Future refreshAll() async {
    token = await auth.getToken();
    number = await auth.getProjectId();
    await getRcovery();

  }
  Future getRcovery() async {
    http.Response response = await http.get(
      Uri.parse('https://raccounting.azurewebsites.net/api/Reports/Recoverysheet?Projectid=$number'),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );
    print(response);
    if (response.statusCode == 200) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      Map<String, dynamic> recoveryData = await jsonDecode(response.body);
      List<dynamic> data = recoveryData["recoverydata"];
      List<Recovery> recoverydata = data
          .map(
            (dynamic item) => Recovery.fromJson(item),
      ).toList();
      recoveries = recoverydata;
      foundResult = recoveries!;
      update();

    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to get recovery data');
    }
  }

  Future<void> runFilter(String enteredKeyword,) async {
    if (enteredKeyword.isEmpty) {
      // if the search field is empty or only contains white-space, we'll display all users

        isName = false;

      results = recoveries!;
    } else {

        isName = true;

      results = recoveries!
          .where((user) => user.accountTitle!
          .toUpperCase()
          .contains(enteredKeyword.toUpperCase()))
          .toList();
      // we use the toLowerCase() method to make it case-insensitive
    } // Refresh the UI
  }
  Future<void> runUnitFilter(String unit,) async {
    if (unit.isEmpty) {

        isUnit = false;

      // if the search field is empty or only contains white-space, we'll display all users
      results2 = results;
    } else {

        isUnit = true;

      results2 = results
          .where((user) => user.unitNumber
          .toString()
          .toUpperCase()
          .contains(unit.toUpperCase()))
          .toList();
      // we use the toLowerCase() method to make it case-insensitive
    } // Refresh the UI
    // Refresh the UI
  }
  Future<void> runMarlaFilter(String marla,) async {
    if (marla.isEmpty) {

        isMarla = false;

      // if the search field is empty or only contains white-space, we'll display all users
      results3 = results2;
    } else {

        isMarla = true;

      results3 = results2
          .where((user) => user.unitMarla
          .toString()
          .toUpperCase()
          .contains(marla.toUpperCase()))
          .toList();
      // we use the toLowerCase() method to make it case-insensitive
    } // Refresh the UI
    // Refresh the UI
  }
  Future<void> runDateFilter(String date,) async {
    if (date.isEmpty) {

        isDate = false;

      // if the search field is empty or only contains white-space, we'll display all users
      results4 = results3;
    } else {

        isDate = true;

      results4 = results3
          .where((recovery) => (recovery.installmentPlan.where((installment) =>
      installment.dueDate!.substring(5, 7) == date)).isNotEmpty)
          .toList();
      // we use the toLowerCase() method to make it case-insensitive
    } // Refresh the UI
    // Refresh the UI


      foundResult = results4;
    update();
  }


}
