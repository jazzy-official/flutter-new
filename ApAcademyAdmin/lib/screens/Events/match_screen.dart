import 'package:ap_academy_admin/ModelView/Controller/NotificationController.dart';
import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:ap_academy_admin/ModelView/Controller/matchController.dart';
import 'package:ap_academy_admin/screens/Events/create_match.dart';
import 'package:ap_academy_admin/screens/Events/past_matches.dart';
import 'package:ap_academy_admin/screens/Events/teams_details.dart';
import 'package:ap_academy_admin/screens/Events/upcoming_matches.dart';
import 'package:ap_academy_admin/screens/Teams/add_team.dart';
import 'package:ap_academy_admin/screens/shared/DeletPop.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:get/get.dart';
import 'package:get/route_manager.dart';
import 'package:intl/intl.dart';

class MatchScreen extends StatefulWidget {
  @override
  _MatchScreenState createState() => _MatchScreenState();
}

class _MatchScreenState extends State<MatchScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: MyResources.buttonColor,
            toolbarHeight: 0,
            bottom:  TabBar(

                tabs: [
                  Tab(
                    text: 'Upcomming',

                  ),
                  Tab(
                    text: 'Past',
                  ),
                ]),
          ),
          body: TabBarView(
            children: [
              UpcommingMatchScreen(),
              PastMatchScreen(),
            ],
          ),
        ));
  }
}
