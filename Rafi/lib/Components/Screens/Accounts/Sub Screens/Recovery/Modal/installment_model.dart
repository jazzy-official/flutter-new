class Installment {
  int? installmentId;
  int? contractId;
  int? installmentNo;
  String? dueDate;
  String? status;
  double? dueAmount;
  double? pendingamount;
  String? receivedDate;



  Installment({
     this.contractId,
     this.dueAmount,
     this.dueDate,
     this.installmentId,
     this.installmentNo,
     this.pendingamount,
    this.receivedDate,
     this.status,

});


  factory Installment.fromJson(Map<String, dynamic> json) {
    return Installment(
      contractId: json['contractId'],
      dueAmount: json['dueAmount'],
      dueDate: json['dueDate'],
      installmentId: json['installmentId'] ,
      installmentNo: json['installmentNo'],
      pendingamount: json['pendingamount'],
      receivedDate: json['receivedDate'],
      status: json['status'],

    );
  }
}
