import 'package:daily_log/View/DailyLod/log_detail.dart';
import 'package:daily_log/View/DailyLod/defaulters.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:intl/intl.dart';

import '../../Controllers/daily_log_controller.dart';
import '../../Models/daili_log_model.dart';
import '../../constants/util.dart';

class DAilyLogScreen extends StatelessWidget {
  const DAilyLogScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    void _showDialog(Widget child) {
      showCupertinoModalPopup<void>(
          context: context,
          builder: (BuildContext context) => Container(
                height: 216,
                padding: const EdgeInsets.only(top: 6.0),
                // The Bottom margin is provided to align the popup above the system navigation bar.
                margin: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom,
                ),
                // Provide a background color for the popup.
                color: CupertinoColors.systemBackground.resolveFrom(context),
                // Use a SafeArea widget to avoid system overlaps.
                child: SafeArea(
                  top: false,
                  child: child,
                ),
              ));
    }

    return GetBuilder<DailyLogController>(
        autoRemove: false,
        init: DailyLogController(),
        builder: (cont) => Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TextButton(
                        onPressed: () => {
                          Get.to(const DefaulterReportScreen())
                            },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: MyResources.headingColor,
                          ),
                          child: const Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Text(
                              "    Defaulters    ",
                              style: TextStyle(color: Colors.white),
                            ),
                          ), 
                        )),
                    TextButton(
                        onPressed: () => {
                              _showDialog(
                                CupertinoDatePicker(
                                  initialDateTime: cont.date,
                                  mode: CupertinoDatePickerMode.date,
                                  use24hFormat: true,
                                  // This is called when the user changes the date.
                                  onDateTimeChanged: (DateTime newDate) {
                                    cont.date = newDate;
                                    cont.datefilter();
                                    cont.update();
                                  },
                                ),
                              ),
                            },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: MyResources.themecolor,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              DateFormat("MMMM d, yyyy").format(cont.date),
                              style: const TextStyle(color: Colors.white),
                            ),
                          ),
                        )),
                  ],
                ),
                Expanded(
                  child: cont.commitsbydate.isEmpty?const Center(child: Text("No result Found"),) : RefreshIndicator(
                    onRefresh: () { return  cont.getData(); },
                    child: RefreshIndicator(
                      onRefresh: (){
                        return cont.getData();
                      },
                      child: ListView.builder(
                        physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                        itemCount: cont.commitsbydate.length,
                        itemBuilder: (context, index) {
                          DailyLog log = cont.commitsbydate[index];
                          return InkWell(
                            onTap: (){
                              Get.to(LogDetailScreen(log: log));
                            },
                            child: Card(
                              elevation: 3,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        const Text(
                                          "Commit by : ",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              fontSize: 16),
                                        ),
                                        Text(
                                          log.employee!.toUpperCase(),
                                          style: const TextStyle(
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        const Text(
                                          "Date : ",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600),
                                        ),
                                        Text(DateFormat("dd-MMM-yyyy")
                                            .format(log.time)),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ),
              ],
            ));
  }
}
