import 'package:daily_log/View/Employyes/employee_task.dart';
import 'package:daily_log/View/Employyes/employee_week.dart';
import 'package:daily_log/View/Tasks/task_list.dart';
import 'package:daily_log/constants/util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../../Controllers/employeedetailcontroller.dart';

class EmployeeDetail extends StatefulWidget {
  final String einame;
  const EmployeeDetail({Key? key, required this.einame,}) : super(key: key);

  @override
  State<EmployeeDetail> createState() => _EmployeeDetailState();
}

class _EmployeeDetailState extends State<EmployeeDetail> {
  var empdetail = Get.put(EmployeeDetailController());


  static  List<Widget> _pages = <Widget>[
    EmployeeWeek(),
    EmployeeTaskScreen(),
    Icon(
      Icons.chat,
      size: 150,
    ),
  ];
  int _selectedIndex = 0;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.einame),
        centerTitle: true,
        backgroundColor: MyResources.themecolor,
      ),
      body: _pages[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: _onItemTapped,
        currentIndex: _selectedIndex,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.file_copy_outlined),
            label: 'Weekly report',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.fact_check_outlined),
            label: 'Tasks',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.check),
            label: 'Attendance',
          ),
        ],
      ),
    );
  }
}
