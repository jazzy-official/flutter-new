
import 'dart:convert';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/RecievePaidChecklist/receive_paid_checklist.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/Recovery/recovery.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/account%20model/accounts.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Payables/Model/payable_model.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Receivable/Model/receivable_model.dart';
import 'package:rafi_admin/Controllers/authcontroller.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ReceivableController extends GetxController {
  final auth = Get.put(AuthController());
  List<Receivable>? receivables;
  late List<Receivable> foundResult = [];
  String? token;
  int? number;
  @override
  Future<void> onInit() async {
    await refreshAll();
    super.onInit();
  }

  Future refreshAll() async {
    token = await auth.getToken();
    number = await auth.getProjectId();
    await setReceivable();

  }
  Future setReceivable() async {
    http.Response response = await http.get(
      Uri.parse('https://raccounting.azurewebsites.net/api/Reports/AccountReceivables?Projectid=$number'),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );
    if (response.statusCode == 200) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      Map<String, dynamic> receiveData = await jsonDecode(response.body);
      List<dynamic> data = receiveData["accounts"];
      List<Receivable> recoverydata = data
          .map(
            (dynamic item) => Receivable.fromJson(item),
      ).toList();
      receivables = recoverydata.where((recoverydata) => recoverydata.currentbalance != 0||recoverydata.currentbalance != 0).toList();
      foundResult = receivables!;
      update();
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to get recovery data');
    }
  }
  void runFilter(String enteredKeyword) {
    List<Receivable> results = [];
    if (enteredKeyword.isEmpty) {
      // if the search field is empty or only contains white-space, we'll display all users
      results = receivables!;
    } else {
      results = receivables!
          .where((user) => user.accounttitle
          .toUpperCase()
          .contains(enteredKeyword.toUpperCase()))
          .toList();
      // we use the toLowerCase() method to make it case-insensitive
    }

    // Refresh the UI
    foundResult = results;
    update();

  }

}
