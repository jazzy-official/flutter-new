
import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:ap_academy_admin/ModelView/Controller/matchController.dart';
import 'package:ap_academy_admin/ModelView/Controller/teamsController.dart';
import 'package:ap_academy_admin/ModelView/Controller/trainingController.dart';
import 'package:ap_academy_admin/screens/Events/select_members.dart';
import 'package:ap_academy_admin/screens/Teams/groupScreen.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import '../../ModelView/Controller/NotificationController.dart';

// ignore: must_be_immutable

class CreateTraining extends StatefulWidget {
  final Map<String, dynamic>? trainingModel1;

  const CreateTraining({Key? key,  this.trainingModel1}) : super(key: key);
  @override
  _CreateTrainingState createState() => _CreateTrainingState();
}

class _CreateTrainingState extends State<CreateTraining> {
  //////////////////////////-------------------/////////////////////////////
  TextEditingController sessionController = TextEditingController();

  TextEditingController venueController = TextEditingController();

  TextEditingController timeController = TextEditingController();

  TextEditingController desController = TextEditingController();
  bool canscheduled = false;
  final tController = Get.put(TrainingController());
  final mController = Get.put(MatchController());
  final nControl = Get.put(NotificationController());
  ////////////////////////-------------------/////////////////////////////
  ///
  ///
  ///
  checkDubliicate(List<Map<String, dynamic>> members) {
    bool isDouble = false;
    members.forEach((element1) {
      return selectedGroup.forEach((element2) {
        if (element1['firebaseId'] == element2['firebaseId']) isDouble = true;
      });
    });
    return isDouble;
  }

  DateTime? time1;
  Future<void> createTraining() async {
    if (sessionController.text.isNotEmpty &&
        venueController.text.isNotEmpty &&
        timeController.text.isNotEmpty &&
        selectedGroup.length != 0) {
      Map<String, dynamic> trainingModel = Map();
      final dbHelper = Get.put(DatabaseController());

      trainingModel['session'] = sessionController.text;
      trainingModel['venue'] = venueController.text;
      trainingModel['time'] = time1;
      trainingModel['coach'] = mController.selectedCoachValue.value;
      trainingModel['attendance'] = [];
      trainingModel['totalPlayer'] = selectedGroup;
      trainingModel['canSchedual'] = canscheduled;
      //   trainingModel['toTeam'] = _selectedTeam;
      if (widget.trainingModel1 == null) {
        bool check = await dbHelper.createTrain(trainingModel);
        List<String> trainingusers = [];
        selectedGroup.forEach((element) {
          trainingusers.add(element['token']);
          trainingusers.add(element['parentToken']);
        });

        if (check) {
          await nControl.sendAndRetrieveMessage(
              {'title': 'Ap Sports', 'desc': 'You have  training'},
              trainingusers);
          Get.back();
          Get.snackbar("Saved", "New event is Saved",
              snackPosition: SnackPosition.BOTTOM,
              icon: Icon(
                Icons.check_circle,
                color: Colors.green,
              ));
          tController.refreshTraining();
        } else {
          Get.snackbar("Not Saved", "New event is not Saved",
              snackPosition: SnackPosition.BOTTOM,
              icon: Icon(
                Icons.cancel_outlined,
                color: Colors.red,
              ));
        }
      } else {
        trainingModel['_id'] = widget.trainingModel1!['_id'];
        bool check = await dbHelper.updateTrain(trainingModel);
        if (check) {
          Get.back();
          Get.snackbar("Saved", "New event is Saved",
              snackPosition: SnackPosition.BOTTOM,
              icon: Icon(
                Icons.check_circle,
                color: Colors.green,
              ));
          tController.refreshTraining();
        } else {
          Get.snackbar("Not Saved", "New event is not Saved",
              snackPosition: SnackPosition.BOTTOM,
              icon: Icon(
                Icons.cancel_outlined,
                color: Colors.red,
              ));
        }
      }
    } else {
      Get.snackbar("Error", "Fill all required Fields",
          snackPosition: SnackPosition.BOTTOM,
          icon: Icon(
            Icons.cancel_outlined,
            color: Colors.red,
          ));
    }
  }

  List<Map<String, dynamic>> selectedGroup = [];
  populateData() {
    if (widget.trainingModel1 != null) {
      sessionController.text = widget.trainingModel1!['session'];
      venueController.text = widget.trainingModel1!['venue'];
      time1 = widget.trainingModel1!['time'];
      canscheduled = widget.trainingModel1!['canSchedual'];
      selectedGroup =
          List<Map<String, dynamic>>.from(widget.trainingModel1!['totalPlayer']);
      timeController.text = DateFormat.yMEd().format(time1!);
    }
  }

  @override
  void initState() {
    populateData();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyResources.backgroundColor,
      appBar: AppBar(
        backgroundColor: MyResources.buttonColor,
        title: Container(
          child: Text(
            'CREATE TRAINING',
            style: GoogleFonts.roboto(),
          ),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Align(
              alignment: Alignment.topRight,
              child: Padding(
                padding: const EdgeInsets.only(top: 0, right: 10),
                child: ElevatedButton(
                  onPressed: () async {
                    final teamController = Get.put(TeamController());
                    await teamController.getUsers();
                    List<Map<String, dynamic>> temp =
                        (await Get.to(() => SelectMembers()));
                    if (temp != null) if (!checkDubliicate(temp)) {
                      temp.forEach((ev) {
                        selectedGroup.add(ev);
                      });
                    } else {
                      Get.rawSnackbar(
                          title: 'Dublicate',
                          message: 'Dublicate members can not be selected');
                    }
                    if (mounted) setState(() {});
                  },
                  style: ElevatedButton.styleFrom(
                    // textColor: Colors.white,
                    primary: MyResources.buttonColor,
                  ),

                  child: Text(
                    "SELECT MEMBERS",
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 0.0, left: 15),
              child: Text(
                "Session:",
                style: GoogleFonts.roboto(
                    fontSize: 15, fontWeight: FontWeight.w400),
              ),
            ),
            Padding(
              //Add padding around textfield
              padding: EdgeInsets.only(top: 7.0, left: 10, right: 10),
              child: Container(
                decoration: MyResources.roundedBoxDecore,
                height: 50,
                child: TextField(
                  controller: sessionController,
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0, left: 15),
              child: Text(
                "Venue:",
                style: GoogleFonts.roboto(
                    fontSize: 15, fontWeight: FontWeight.w400),
              ),
            ),
            Padding(
              //Add padding around textfield
              padding: EdgeInsets.only(top: 10.0, left: 10, right: 10),
              child: Container(
                decoration: MyResources.roundedBoxDecore,
                height: 50,
                child: TextField(
                  controller: venueController,
                  style: TextStyle(fontSize: 15),
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0, left: 15),
              child: Text(
                "Coach:",
                style: GoogleFonts.roboto(
                    fontSize: 15, fontWeight: FontWeight.w400),
              ),
            ),
            Padding(
              //Add padding around textfield
              padding: EdgeInsets.only(top: 7.0, left: 10, right: 10),
              child: Padding(
                padding: const EdgeInsets.all(0.0),
                child: Container(
                  decoration: BoxDecoration(
                      color: MyResources.backgroundColor,
                      border: Border.all(color: Colors.grey),
                      borderRadius: BorderRadius.circular(10)),
                  //  color: MyResources.backgroundColor,

                  height: 50,
                  child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Obx(
                        () => DropdownButton(
                          isExpanded: true,
                          underline: Text(""),
                          value: mController.selectedCoachValue.value,
                          // focusColor: Colors.black,
                          elevation: 12,
                          items: mController.coachdropValues.map(
                            (map) {
                              return DropdownMenuItem(
                                child: Text(map['name']),
                                value: map['value'],
                              );
                            },
                          ).toList(),
                          onChanged: (value) {
                            print(value);
                            FocusScope.of(context)
                                .requestFocus(new FocusNode());
                            setState(() {
                              mController.selectedCoachValue.value = value.toString();
                            });
                          },
                          hint: Text('Select Item'),
                        ),
                      )),
                ),
              ),
            ),
            //Date and time
            Padding(
              padding: const EdgeInsets.only(top: 10.0, left: 15),
              child: Text(
                "Time:",
                style: GoogleFonts.roboto(
                    fontSize: 15, fontWeight: FontWeight.w400),
              ),
            ),
            Padding(
              //Add padding around textfield
              padding: EdgeInsets.only(top: 7.0, left: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: InkWell(
                      onTap: () {
                        DatePicker.showDateTimePicker(context,
                            showTitleActions: true, onChanged: (time) {
                          FocusScope.of(context).requestFocus(new FocusNode());
                          print('change $time');
                        }, onConfirm: (time) {
                          timeController.text = DateFormat.yMEd().format(time) +
                              ",  " +
                              DateFormat.Hm().format(time);
                          time1 = time;
                          print('confirm $time');
                        }, currentTime: DateTime.now(), locale: LocaleType.en);
                      },
                      child: Container(
                        decoration: MyResources.roundedBoxDecore,
                        child: TextFormField(
                          enabled: false,
                          readOnly: true,
                          controller: timeController,
                          decoration: InputDecoration(
                            prefixIcon: Icon(Icons.alarm_add_rounded),

                            //Add th Hint text here.
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),

            Padding(
              child: Row(
                children: [
                  Checkbox(
                    value: canscheduled,
                    onChanged: (bool? value) {
                      setState(() {
                        this.canscheduled = value!;
                      });
                    },
                  ),
                  Text('Check if you want it scheduled')
                ],
              ),
              padding: EdgeInsets.only(top: 7.0, left: 10, right: 10),
            ),

            Center(
              child: Container(
                height: 300,
                constraints: BoxConstraints(minHeight: 20),
                child: Padding(
                  padding: EdgeInsets.all(8),
                  child: selectedGroup.length != 0
                      ? ListView.builder(
                          itemCount: selectedGroup.length,
                          itemBuilder: (context, index) {
                            if (selectedGroup.length != 0)
                              return Card(
                                child: Container(
                                  height: 50,
                                  padding: EdgeInsets.all(8),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Expanded(
                                        flex:2,
                                        child: Text(
                                          (index+1).toString(),
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 16,
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex:5,
                                        child: Text(
                                          selectedGroup[index]['name'].toString(),
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 16,
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex:2,
                                        child: Text(
                                          calculateAge(selectedGroup[index]['dob']).toString(),
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 16,
                                          ),
                                        ),
                                      ),
                                      IconButton(
                                          icon: Icon(
                                            Icons.delete,
                                            color: Colors.red,
                                          ),
                                          onPressed: () {
                                            deletUser(index);
                                          })
                                    ],
                                  ),
                                ),
                              );
                            else
                              return Container();
                          },
                        )
                      : Container(),
                ),
              ),
            ),
            Center(
              child: Padding(
                padding:
                    EdgeInsets.only(top: 20.0, left: 10, bottom: 20, right: 10),
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.9,
                  height: 45,
                  child: RaisedButton(
                    textColor: Colors.white,
                    color: MyResources.buttonColor,
                    child: Text(
                      "SUBMIT",
                    ),
                    onPressed: () async {
                      await createTraining().then((value) async => {});
                    },
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  deletUser(int i) {
    selectedGroup.removeAt(i);
    if (mounted) setState(() {});
  }
}
