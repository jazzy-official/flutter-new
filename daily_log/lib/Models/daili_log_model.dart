
import 'commit.dart';

class DailyLog {
  final String? id;
  final DateTime time;
  final String? employee;
  final String? employeeid;
  List<Commit> firsthalf;
  List<Commit> secondhalf;

  DailyLog( {
    this.id,
    required this.time,
    required this.firsthalf,
    required this.secondhalf,
    this.employee,
    this.employeeid,
  });

  factory DailyLog.fromJson(dynamic json, key) {
    return DailyLog(
      id: key.toString(),

      employee: json['employee'],
      employeeid: json['employeeid'],
      firsthalf: json["firsthalf"] == null
          ? []
          : List<Commit>.from(json["firsthalf"]
          .map(
            (dynamic item) => Commit.fromJson(item, ""),
      )
          .toList()),
      secondhalf: json["secondhalf"] == null
          ? []
          : List<Commit>.from(json["secondhalf"]
          .map(
            (dynamic item) => Commit.fromJson(item, ""),
      )
          .toList()),
      time:DateTime.parse(json['time']) ,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['time'] = time.toString();
    data['employee'] = employee;

    data['employeeid'] = employeeid;
    data['firsthalf'] = List<dynamic>.from(firsthalf.map((x) => x.toJson()));
    data['secondhalf'] = List<dynamic>.from(secondhalf.map((x) => x.toJson()));
    return data;
  }
}
