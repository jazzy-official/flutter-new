import 'package:ap_booking_user/Screens/Authentication/login.dart';
import 'package:ap_booking_user/Screens/Authentication/signup_screen.dart';
import 'package:ap_booking_user/constants/util.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';

class FirstScreen extends StatelessWidget {
  const FirstScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.5,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Image.asset(
                    "assets/logo.png",
                    height: 350,
                  ),
                  const Text(
                    "It's all about Cricket",
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.grey,
                        fontStyle: FontStyle.italic),
                  )
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          Expanded(
              child: Column(
            children: [
              TextButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const LoginScreen()),
                    );
                  },
                  child: Container(
                    height: 50,
                    width: MediaQuery.of(context).size.width - 60,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: MyResources.buttonColor,
                    ),
                    child: const Center(child: Text("Sign in",style: TextStyle(color: Colors.white,fontSize: 17),)),
                  )),
              TextButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const SignupScreen()),
                    );
                  },
                  child: Container(
                    height: 50,
                    width: MediaQuery.of(context).size.width - 60,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: MyResources.buttonColor,
                    ),
                    child: const Center(child: Text("Sign up",style: TextStyle(color: Colors.white,fontSize: 17),)),

                  )),
              const Padding(
                padding: EdgeInsets.all(30.0),
                child: Text(
                  "Or Login With",
                  style: TextStyle(color: Colors.grey, fontSize: 16),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  TextButton(
                    onPressed: () {},
                    child: Container(
                        height: 45,
                        decoration: BoxDecoration(
                            color: const Color(0xfff8f2f1),
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(color: const Color(0xffea4335),width: 1)
                        ),
                        padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                        child: Row(
                          children: const [
                            Icon(Ionicons.logo_google,color:Color(0xffea4335) ,),
                            SizedBox(
                              width: 15,
                            ),
                            Text("Google",style: TextStyle(color: Color(0xffea4335)),),
                            SizedBox(
                              width: 15,
                            ),
                          ],
                        )),
                  ),
                  Container(
                    width: 2,
                    height: 50,
                    color: Colors.grey.shade400,
                  ),
                  TextButton(
                    onPressed: () {},
                    child: Container(
                        decoration: BoxDecoration(
                            color: const Color(0xfff1f2f8),
                            borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: Colors.blue,width: 1)
                        ),
                        padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                        child: Row(
                      children: const [
                        Icon(Ionicons.logo_facebook),
                        SizedBox(width: 10,),
                        Text("Facebook")
                      ],
                    )),
                  ),
                ],
              )
            ],
          ))
        ],
      ),
    );
  }
}
