import 'package:daily_log/Controllers/employees_controller.dart';
import 'package:daily_log/Controllers/projectscontroller.dart';
import 'package:daily_log/Models/project_model.dart';
import 'package:daily_log/View/Projects/add_module.dart';
import 'package:daily_log/constants/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../Models/module.dart';

class AddProject extends StatefulWidget {
  final ProjectModel? project;
  const AddProject({Key? key, this.project}) : super(key: key);

  @override
  State<AddProject> createState() => _AddProjectState();
}

class _AddProjectState extends State<AddProject> {
  var employeecont = Get.put(EmployeeController());
  TextEditingController titleCont = TextEditingController();
  TextEditingController desCont = TextEditingController();
  DateTime? deadline;
  String? assignto;
  List<Module> modules=[];
  bool isComplete = false;
  @override
  void initState() {
    populatedata();
    super.initState();
  }


  void populatedata(){
    if(widget.project != null){
      setState(() {
        titleCont.text = widget.project!.name;
        desCont.text = widget.project!.des;
        isComplete = widget.project!.status;
        deadline = widget.project!.deadline !=null ? DateTime.parse(widget.project!.deadline!):null;
        assignto = widget.project!.assignto;
        modules = widget.project!.modules;

      });
    }
  }
  @override
  Widget build(BuildContext context) {
    void _showDialog(Widget child) {
      showCupertinoModalPopup<void>(
          context: context,
          builder: (BuildContext context) => Container(
                height: 216,
                padding: const EdgeInsets.only(top: 6.0),
                // The Bottom margin is provided to align the popup above the system navigation bar.
                margin: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom,
                ),
                // Provide a background color for the popup.
                color: CupertinoColors.systemBackground.resolveFrom(context),
                // Use a SafeArea widget to avoid system overlaps.
                child: SafeArea(
                  top: false,
                  child: child,
                ),
              ));
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyResources.themecolor,
        title: const Text("Add Project"),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: GetBuilder<ProjectController>(
          autoRemove: false,
          init: ProjectController(),
          builder: (cont) => ListView(
            children: [
              const SizedBox(
                height: 10,
              ),
              SizedBox(
                height: 40,
                child: TextFormField(
                  controller: titleCont,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Name',
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                height: 100,
                child: TextFormField(
                  controller: desCont,
                  maxLines: 4,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    contentPadding: EdgeInsets.only(top: 20, left: 10),
                    labelText: 'About project...',
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const SizedBox(
                    width: 100,
                      child: Text(
                    "Manage By : ",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 15),
                  )),
                  const SizedBox(
                    width: 20,
                  ),

                  Expanded(
                    child: TextButton(
                        style: TextButton.styleFrom(padding: EdgeInsets.zero),
                        onPressed: () => _showDialog(
                              CupertinoPicker(
                                backgroundColor: Colors.white,
                                itemExtent: 30,
                                scrollController:
                                    FixedExtentScrollController(initialItem: 0),
                                children: [
                                  const Text("select 1"),
                                  for (var i in employeecont.alluser)
                                    Text(i.name),
                                ],
                                onSelectedItemChanged: (value) {
                                  setState(() {
                                    assignto =
                                        employeecont.alluser[value - 1].name;
                                  });
                                },
                              ),
                            ),
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(3),
                              border:
                                  Border.all(color: MyResources.themecolor)),
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                assignto ?? "Select",
                                style: TextStyle(color: MyResources.themecolor),
                              )
                            ],
                          ),
                        )),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const SizedBox(
                    width: 200,
                    child: Text(
                      "Is Completed?  ",
                      style: TextStyle(fontWeight: FontWeight.w600, fontSize: 15),
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Checkbox(value: isComplete, onChanged: (e){
                    setState(() {
                      isComplete = e!;
                    });
                    if(e == true){
                      setState(() {
                        deadline = null;
                      });
                    }

                  })
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              if(!isComplete)
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const SizedBox(
                    width: 100,
                    child: Text(
                      "Deadline : ",
                      style: TextStyle(fontWeight: FontWeight.w600, fontSize: 15),
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    child: TextButton(
                        style: TextButton.styleFrom(padding: EdgeInsets.zero),
                        onPressed: () => {
                              cont.update(),
                              _showDialog(
                                CupertinoDatePicker(
                                  initialDateTime: deadline,
                                  mode: CupertinoDatePickerMode.date,
                                  use24hFormat: true,
                                  // This is called when the user changes the date.
                                  onDateTimeChanged: (DateTime newDate) {
                                    setState(() {
                                      deadline = newDate;
                                    });


                                  },
                                ),
                              ),
                            },
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(3),
                              border:
                                  Border.all(color: MyResources.themecolor)),
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                deadline==null? "Select a date" :
                                DateFormat("dd MMM yyyy")
                                        .format(deadline!),
                                style: TextStyle(color: MyResources.themecolor),
                              )
                            ],
                          ),
                        )),
                  ),
                ],
              ),

              ListView.builder(
                controller: ScrollController(),
                shrinkWrap: true,
                itemCount: modules.length,
                itemBuilder: (context, index) {
                  return Card(
                    elevation: 2,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          Expanded(
                            child: InkWell(
                              onTap: (){
                                Get.to(AddModule(addmodule: (e){},module: modules[index],));
                              },
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(modules[index].name.toUpperCase(),style: const TextStyle(fontWeight: FontWeight.w600),),
                                      Row(
                                        children: [
                                          const Text("Sub-Modules : ",style: TextStyle(color: Colors.grey),),
                                          Text(modules[index].submodules==null ?"0" : modules[index].submodules!.length.toString(),style: const TextStyle(fontWeight: FontWeight.w600),),
                                        ],
                                      ),
                                    ],
                                  ),
                                )
                            ),
                          ),
                          IconButton(onPressed: (){
                            modules.removeAt(index);
                            setState(() {
                            });
                          }, icon: Icon(Icons.clear,color: MyResources.themecolor,))
                        ],
                      ),
                    ),
                  );
                },
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(primary: MyResources.headingColor),
                  onPressed: (){
                  Get.to(AddModule(addmodule: (value) {
                    modules.add(value);
                    setState(() {
                      
                    });
                    },));
                  },
                  child: const Text("Add Module")
              ),
              const SizedBox(
                height: 50,
              ),
              TextButton(
                  style: TextButton.styleFrom(padding: EdgeInsets.zero),
                  onPressed: () async {
                    if (titleCont.text == "") {
                      Get.snackbar("Alert", "Please enter project Name");
                    } else if (desCont.text == "") {
                      Get.snackbar("Alert", "Please enter description");
                    }  else if (assignto == null) {
                      Get.snackbar("Alert", "Please assign the a manager");
                    } else {

                      for (var element in modules) {
                        setState(() {
                          element.status = isComplete;
                        });
                        if(element.submodules!=null && element.submodules!.isNotEmpty) {
                          for (var element2 in element.submodules!) {
                            setState(() {
                              element2.status = isComplete;
                            });
                          }
                        }

                      }


                      if(widget.project == null) {
                        ProjectModel project = ProjectModel(deadline: deadline.toString(), status: isComplete, name: titleCont.text, des: desCont.text,assignto: assignto, modules: modules);
                        await cont.addProject(project);
                      } else{
                        ProjectModel project = ProjectModel(id:widget.project!.id,deadline: deadline.toString(), status: isComplete, name: titleCont.text, des: desCont.text,assignto: assignto, modules: modules);
                        await cont.updateProject(project);
                      }

                    }
                  },
                  child: Container(
                    height: 40,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3),
                      color: MyResources.themecolor,
                    ),
                    child: const Center(
                        child: Text(
                      "Save",
                      style: TextStyle(color: Colors.white, fontSize: 17),
                    )),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
