import 'dart:ui';
import 'package:ap_academy_admin/screens/Teams/groupScreen.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:ap_academy_admin/ModelView/Controller/matchController.dart';
import 'package:ap_academy_admin/ModelView/Controller/teamsController.dart';
import 'package:ap_academy_admin/screens/shared/DeletPop.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import 'addNewteam.dart';

class NewTeamsDetails extends StatefulWidget {
  final Map<String, dynamic> teamModel;

  NewTeamsDetails({
    Key? key,
    required this.teamModel,
  }) : super(key: key);

  @override
  State<NewTeamsDetails> createState() => _NewTeamsDetailsState();
}

class _NewTeamsDetailsState extends State<NewTeamsDetails> {
  final mController = Get.put(MatchController());

  @override
  void initState() {

    widget.teamModel['teamMembers'].sort((a,b) => a['name'].toString().compareTo(b['name'].toString()));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
              onPressed: () {
                Get.dialog(Center(
                    child: DeletpopUp(
                  message: 'Are you sure you want to delete?',
                  pressFunc: () async {
                    final teamController = Get.put(TeamController());
                    await teamController
                        .delTeams(
                          widget.teamModel['_id'],
                        )
                        .then((value) => {Get.back(), Get.back()});
                  },
                )));
              },
              icon: Icon(Icons.delete))
        ],
        centerTitle: true,
        title: Text(
          widget.teamModel['teamName'].toString().toUpperCase(),
          style: TextStyle(
            fontSize: 18,
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
        backgroundColor: MyResources.buttonColor,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                ElevatedButton(
                  onPressed: () {
                    print("Membersss"+widget.teamModel['teamMembers'].toString());
                    Get.to(AddNewTeam(
                      teamname: widget.teamModel['teamName'].toString().toUpperCase(),
                      teamMembers: widget.teamModel['teamMembers'],
                      id: widget.teamModel['_id'],
                      // id:  teamModel['_id'],
                    ));
                  },
                  child: Text("Edit Team"),
                  style: ElevatedButton.styleFrom(
                    primary: MyResources.buttonColor,
                  ),
                ),
              ],
            ),
          ),
          Card(
            color: MyResources.backgroundColor,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Container(
                        width: Get.width,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                                topRight: Radius.circular(10)),
                            color: MyResources.buttonColor),
                        height: 40,
                        child: Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Text(
                                'Sr',
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white),
                              ),
                            ),

                            Expanded(
                              flex: 5,
                              child: Text(
                                'Name',
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                            Expanded(
                              flex: 4,
                              child: Text(
                                'Age',
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                            Expanded(
                              flex: 5,
                              child: Text('Phone',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white)),
                            )
                          ],
                        )),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                    height: Get.height-250,
                    width: Get.width,
                    child: ListView.builder(
                        itemCount: widget.teamModel['teamMembers'] != null
                            ? widget.teamModel['teamMembers'].length
                            : 0,
                        itemBuilder: (BuildContext context, int index) {
                          return getTeamMembersCard(
                              widget.teamModel['teamMembers'][index], index + 1);
                        })),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget getTeamMembersCard(Map<String, dynamic> memeberslist, int i) {
    return InkWell(
      onTap: () => launch("tel://"+memeberslist['phone'].toString()),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.only(top: 10, bottom: 10),
          child: Row(
            children: [
              Expanded(
                flex: 1,
                child: Text(
                  i.toString(),
                  textAlign: TextAlign.center,
                ),
              ),

              Expanded(
                flex: 5,
                child: Text(
                  memeberslist['name'],
                  textAlign: TextAlign.center,
                ),
              ),
              Expanded(
                flex: 4,
                child: Text(memeberslist['dob'] == null?"N/A":calculateAge(memeberslist['dob'])
                    .toString(),
                  textAlign: TextAlign.center,
                ),
              ),
              Expanded(
                flex: 5,
                child: Text(memeberslist['phone'] == null?"N/A":
                  memeberslist['phone'],
                  textAlign: TextAlign.center,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
