import 'package:ap_academy_admin/ModelView/Controller/NotificationController.dart';
import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:ap_academy_admin/ModelView/Controller/matchController.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:ap_academy_admin/screens/Events/teams_details.dart';
import 'package:ap_academy_admin/screens/Teams/add_team.dart';
import 'package:ap_academy_admin/screens/shared/DeletPop.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/get_navigation/src/snackbar/snackbar.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_getx_widget.dart';
import 'package:intl/intl.dart';

import 'create_match.dart';

class PastMatchScreen extends StatefulWidget {
  const PastMatchScreen({Key? key}) : super(key: key);

  @override
  _PastMatchScreenState createState() => _PastMatchScreenState();
}

class _PastMatchScreenState extends State<PastMatchScreen> {
  final dbHelper = Get.put(DatabaseController());
  final m = Get.put(MatchController());

  @override
  void initState() {
    m.refreshDate();
    m.getpastmatches();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      //Create an event add button
      child: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "",
                    style: MyResources.appHeadingStyle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    width: Get.width * 0.2,
                    height: 25,
                    child: RaisedButton(
                      textColor: MyResources.buttontextColor,
                      color: MyResources.buttonColor,
                      child: Text(
                        "ADD",
                      ),
                      onPressed: () {
                        Get.to(
                              () => CreateMatch(),
                        );
                      },
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: Get.height * 0.7,
              child: GetX<MatchController>(
                init: Get.put<MatchController>(MatchController()),
                builder: (MatchController matchController) {
                  if (matchController.match != null) {
                    return Stack(
                      children: [
                        Center(
                          child: ColorFiltered(
                              colorFilter: ColorFilter.mode(
                                  Colors.white.withOpacity(0.3),
                                  BlendMode.dstATop),
                              child: Image.asset(
                                'assets/noMatch.png',
                                colorBlendMode: BlendMode.hue,
                                width: 200,
                                height: 200,
                              )),
                        ),
                        ListView.builder(
                            itemCount: matchController.pastmatches.length,
                            itemBuilder: (BuildContext context, int index) {
                              return getMatchCard(
                                matchController.pastmatches[index],
                              );
                            }),
                        SizedBox(height: 1000,)
                      ],
                    );
                  } else {
                    return Container(
                      child: LoadingWidget(),
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  final mController = Get.put(MatchController());
  Widget getMatchCard(
      Map<String, dynamic> matchModel,
      ) {
    // mController.getTeamName(matchModel);
    return Padding(
      padding: const EdgeInsets.only(top: 4.0, left: 10, right: 10),
      child: Container(
        //constraints: BoxConstraints(minHeight: 210),
        width: Get.width*0.9,
        // height:Get.height*0.29,
        //create the cards for all details of event happening
        child: InkWell(
          onTap: () async {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => MatchTeamsDetails(
                team1Model: // matchModel['otherteam1']
                // ?
                {
                  'teamName': matchModel['team1name'],
                  'teamType': 'Custom',
                  'coach': 'General',
                  'teamMembers':matchModel['team1'],
                }
                //: await mController.getTeamsDetail(matchModel['team1']),
                ,
                team2Model: // matchModel['otherteam2']
                //  ?
                {
                  'teamName': matchModel['team2name'],
                  'teamType': 'Custom',
                  'coach': 'General',
                  'teamMembers': matchModel['team2']
                }
                //   : await mController.getTeamsDetail(matchModel['team2']),
                ,
                matchModel: matchModel,
              ),),
            );
            // Get.dialog(
            //   MatchTeamsDetails(
            //     team1Model: // matchModel['otherteam1']
            //         // ?
            //         {
            //       'teamName': matchModel['team1name'],
            //       'teamType': 'Custom',
            //       'coach': 'General',
            //       'teamMembers': matchModel['team1']
            //     },
            //     // : await mController.getUserbyGroup(matchModel['group1']),
            //     team2Model: //matchModel['otherteam2']
            //         //?
            //         {
            //       'teamName': matchModel['team2name'],
            //       'teamType': 'Custom',
            //       'coach': 'General',
            //       'teamMembers': matchModel['team2']
            //     },
            //     // : await mController.getUserbyGroup(matchModel['group2']),
            //     matchModel: matchModel,
            //   ),
            // );
          },
          child: Stack(
            children: [
              Card(
                color: MyResources.backgroundColor,
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: Column(
                    children: [
                      // Center(
                      //   child: Text(matchModel['match'],
                      //       overflow: TextOverflow.ellipsis,
                      //       style: MyResources.appHeadingStyle),
                      // ),
                      Row(
                        //mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8),
                            child: Container(
                              width: Get.width*0.8,
                              child: Column(children: [
                                SizedBox(
                                  height: 10,
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    // matchModel['otherteam1']
                                    // ?
                                    Container(

                                      child: Text(matchModel['team1name'],
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                              fontSize: 17,
                                              fontWeight: FontWeight.bold,
                                              color: Color(0xff1758B4)
                                          )),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      "vs",
                                      style: MyResources.vsStyle,
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    //  matchModel['otherteam2']
                                    //  ?
                                    Container(
                                      child: Text(
                                        matchModel['team2name'],
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.bold,
                                          color: Color(0xff1758B4),
                                        ),
                                      ),
                                    ),
                                    // : FutureBuilder(
                                    //     future: mController
                                    //         .getTeamName2(matchModel),
                                    //     builder: (BuildContext context,
                                    //         AsyncSnapshot<String> sp) {
                                    //       if (sp.data != null)
                                    //         return Container(
                                    //           child: Text(
                                    //             sp.data,
                                    //             overflow: TextOverflow.ellipsis,
                                    //             style: TextStyle(
                                    //               color: Color(0xff000074),
                                    //             ),
                                    //           ),
                                    //         );
                                    //       else
                                    //         return Container(
                                    //           width: 80,
                                    //           child: Text(
                                    //             '',
                                    //             style: TextStyle(
                                    //               color: Color(0xff000074),
                                    //             ),
                                    //           ),
                                    //         );
                                    //     }),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Text("Venue:",
                                        style: MyResources.titleTextStyle),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Container(
                                      //width: 150,
                                      child: Text(
                                        matchModel['venue'],
                                        style: MyResources.titleTextStyle,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(top: 8.0),
                                      child:Text("Time:",
                                          style: MyResources.titleTextStyle),
                                    ),
                                    SizedBox(
                                      width: 16,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 8.0),
                                      child:Text(
                                        DateFormat("dd-MMM-yyyy - hh:mm a")
                                            .format(matchModel['matchTime'].toLocal()),
                                        style: TextStyle(
                                            color: MyResources.headingColor),
                                      ),
                                    ),
                                  ],
                                ),
                              ]),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          IconButton(
                            tooltip: "Delete",
                            onPressed: () => {
                              Get.dialog(
                                  Center(child: DeletpopUp(
                                    message: 'Are you sure you want to delete?',
                                    pressFunc: () async {
                                      final matchController =
                                      Get.put(MatchController());
                                      await matchController
                                          .delMatch(
                                        matchModel['_id'],
                                      )
                                          .then((value) => Get.back());},
                                  )))
                            },
                            icon: Icon(
                              Icons.dangerous,
                              color: Colors.red[300],
                            ),
                          ),
                          Container(
                            width: 30,
                            child: IconButton(
                              onPressed: () {
                                Get.to(CreateMatch(
                                  matchModel: matchModel,
                                ));
                              },
                              icon: Icon(
                                Icons.edit,
                                color: Colors.green,
                              ),
                            ),
                          ),
                          Container(
                            width: 30,
                            child: IconButton(
                              iconSize: 20.0,
                              visualDensity: VisualDensity.adaptivePlatformDensity,
                              onPressed: () async {
                                List<String> teamusers = [];
                                if (!matchModel['otherteam1']) {
                                  await mController
                                      .getTeamsDetailByName(matchModel['team1name'])
                                      .then((value) {
                                    if (value != null) {
                                      value['teamMembers'].forEach((element) {
                                        teamusers.add(element['token']);
                                      });
                                    }
                                  });
                                }
                                if (!matchModel['otherteam2']) {
                                  await mController
                                      .getTeamsDetailByName(matchModel['team2name'])
                                      .then((value) {
                                    if (value != null) {
                                      value['teamMembers'].forEach((element) {
                                        teamusers.add(element['token']);
                                      });
                                    }
                                  });
                                }
                                await Get.find<NotificationController>()
                                    .sendAndRetrieveMessage({
                                  'title': 'Match : '+matchModel['match'],
                                  'desc': 'This is a reminder for the upcoming match.\nThis match will be on :'+matchModel['matchTime'].toString(),
                                }, teamusers);

                                Map<String, dynamic> notifyModel = {
                                  'title': 'Match: '+matchModel['match'],
                                  'description': 'This is a reminder for the upcoming match.\nThis match will be on: '+DateFormat.yMEd().add_jm().format( matchModel['matchTime']).toString(),
                                  'timeCreated': DateTime.now()
                                };
                                await Get.find<DatabaseController>()
                                    .addNotifications(notifyModel);

                                Get.snackbar("Notification Sent",
                                    "Reminder notification has been sent ",
                                    snackPosition: SnackPosition.BOTTOM);
                              },
                              icon: Icon(
                                Icons.notifications,
                                color: Colors.orange,
                              ),
                            ),
                          ),
                        ],
                      ),
                      !matchModel['isApproved'] && (!matchModel['otherteam2'] || !matchModel['otherteam1'])
                          ? ElevatedButton(
                        child: Text("MAKE TEAMS",
                        ),
                        onPressed: () {
                          Get.to(
                                () => AddTeam(matchModel: matchModel,),
                          );},
                        style: ElevatedButton.styleFrom(
                            primary: Color(0xff1758B4),
                            onPrimary: Colors.white,
                            minimumSize: Size(20,24)
                        ),
                      )
                          : Container(),
                    ],
                  ),
                ),
              ),
              !matchModel['isApproved']
                  ? Positioned(
                top: 8,
                right: 4,
                child: Container(
                  width: 60,
                  height: 20,
                  color: Colors.red[600],
                  child: Center(
                      child: Text(
                        'Awaiting',
                        style: TextStyle(color: Colors.white, fontSize: 12),
                      )),
                ),
              )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }
}
