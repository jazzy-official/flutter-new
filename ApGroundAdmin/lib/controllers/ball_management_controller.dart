import 'package:ap_ground_admin/Screens/Ball%20Management/Models/ball_model.dart';
import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:firebase_database/firebase_database.dart';


class BallController {
  final DatabaseReference databaseReference =
  FirebaseDatabase.instance.reference().child("Ball");

  Future<void> saveBall(Ball ball) async {
    try{ await databaseReference.push().set(ball.toJson());
    }catch(e){
      displayToast(e.toString());
    }
  }

  Future<void> updateBall(Ball ball) async {

    databaseReference.update({
      ball.id! : ball.toJson()
    });

  }
  Future<void> deleteBall(String id) async {
    databaseReference.child(id).remove();

  }

  Future<List<Ball>> readBall() async {
    List<Ball> ball = [];
    await databaseReference.once().then((DataSnapshot snapshot) {
      print("im in");
      if(snapshot.value != null){
        snapshot.value.forEach((key, value) {
          Ball gr = Ball.fromJson(value,key);
          ball.add(gr);
        });
      }

    });
    return ball;
  }

}
