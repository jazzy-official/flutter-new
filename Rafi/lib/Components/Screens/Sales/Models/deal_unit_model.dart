class DealUnit {
  int dealUnitId;
  int unitId;
  double saleRate;
  double totalSaleValue;
  String phase;
  String unitNo;
  String status;
  double netMarla;

  DealUnit({
    required this.status,
    required this.netMarla,
    required this.totalSaleValue,
    required this.phase,
    required this.saleRate,
    required this.dealUnitId,
    required this.unitNo,
    required this.unitId,
  });

  factory DealUnit.fromJson(Map<String, dynamic> json) {
    return DealUnit(
      dealUnitId: json['DealUnitId'],
      unitId: json['Unit']['UnitId'],
      phase: json['Unit']['Block']['PhaseName'],
      unitNo: json['Unit']['UnitNumber'],
      status: json['Unit']['Status'],
      saleRate: json['SaleRate'],
      totalSaleValue: json['TotalSaleValue'],
      netMarla: json['Unit']['NetMarla'],

    );
  }
}
