import 'package:daily_log/Controllers/employees_controller.dart';
import 'package:daily_log/Models/user_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AttendanceScreen extends StatefulWidget {
  const AttendanceScreen({Key? key}) : super(key: key);

  @override
  State<AttendanceScreen> createState() => _AttendanceScreenState();
}

class _AttendanceScreenState extends State<AttendanceScreen> {
  DateTime tempDateTime = DateTime.now();
  DateTime _chosenDateTime = DateTime.now();
  void _showDatePicker(ctx) {
    showCupertinoModalPopup(
        context: ctx,
        builder: (_) => Container(
          height: 353,
          color: Color.fromARGB(255, 255, 255, 255),
          child: Column(
            children: [
              Container(
                height: 300,
                child: CupertinoDatePicker(
                    initialDateTime: _chosenDateTime,
                    mode: CupertinoDatePickerMode.date,
                    onDateTimeChanged: (val) {
                      setState(() {
                        tempDateTime = val;
                      });


                      print(_chosenDateTime);
                    }),
              ),

              // Close the modal
              CupertinoButton(
                  child: Text('OK'),
                  onPressed: () async{
                    if(tempDateTime.isAfter(DateTime.now())){
                      Get.snackbar("Warning", "CantSelect date");
                    }else{
                      setState(() {
                        _chosenDateTime = tempDateTime;
                      });

                    } Navigator.of(ctx).pop();}
              )
            ],
          ),
        ));
  }
  var employeeController = Get.put(EmployeeController());
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(child: ListView.builder(
            physics: BouncingScrollPhysics(
                parent: AlwaysScrollableScrollPhysics()),
            padding: const EdgeInsets.all(8),
            itemCount: employeeController.employees.length,
            itemBuilder: (BuildContext context, int index) {
              UserModel employee = employeeController.employees[index];
              return Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: Column(
                          mainAxisAlignment:
                          MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              employee.name,
                              style: const TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600),
                            ),
                            Text(
                              "(" + employee.role + ")",
                              style: const TextStyle(fontSize: 12),
                            ),
                          ],
                        ),
                      ),
                      Checkbox(value: true, onChanged: (val) {
                        setState(() {

                        });
                      },activeColor: Colors.green,)
                    ],
                  ),
                ),
              );
            }),)
      ],
    );
  }
}
