import 'dart:io';

import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mongo_dart/mongo_dart.dart';

class SponsorController extends GetxController {
  Rx<List<Map<String, dynamic>>> sponsorList = Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get sponsors => sponsorList.value;

  List<Map<String, dynamic>> userList = <Map<String, dynamic>>[].obs;
  final ImagePicker _picker = ImagePicker();
  @override
  void onInit() {
    super.onInit();
  }

  final dbH = Get.put(DatabaseController());
  Future<void> getUsers() async {
    userList = (await dbH.getAlluserData())!;
  }

  Future<void> delTeams(ObjectId id) async {
    await DatabaseController()
        .deletSponsor(id)
        .then((value) => refreshSponsor());
  }

  refreshSponsor() {
    sponsorList.bindStream(DatabaseController().sponsorStream());
  }

  Future<bool> addSponsor(Map<String, dynamic> t) async {
    return await DatabaseController().addSponsor(t);
  }

  Future<XFile?> imgFromGallery() async {
    try {
      return await _picker.pickImage(
          source: ImageSource.gallery, imageQuality: 50);
    } catch (e) {
      return null;
    }
  }

  Future<String?> uploadImageToFirebase(
    File image,
  ) async {
    final DateTime now = DateTime.now();
    final int millSeconds = now.millisecondsSinceEpoch;
    final String month = now.month.toString();
    final String date = now.day.toString();
    final String storageId =
        (millSeconds.toString() + FirebaseAuth.instance.currentUser!.uid);
    final String today = ('$month-$date');
    final Reference storageReference = FirebaseStorage.instance
        .ref()
        .child("SponsorImages")
        .child("Images")
        .child(today)
        .child(storageId);
    try {
      // Make random image name.
      // String imageLocation =
      //     'profileimages/${FirebaseAuth.instance.currentUser.uid}.jpg';
      // Upload image to firebase.
      // final storageReference =
      //     FirebaseStorage.instance.ref().child(imageLocation);
      final uploadTask = storageReference.putFile(image);
      await uploadTask.whenComplete(() => null);
      return await uploadTask.snapshot.ref.getDownloadURL();
    } catch (e) {
      print(e);
      return null;
    }
  }
}
