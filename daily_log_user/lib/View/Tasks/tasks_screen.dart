import 'package:daily_log_user/Controllers/daily_log_controller.dart';
import 'package:daily_log_user/View/Tasks/task_card.dart';
import 'package:daily_log_user/constants/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:intl/intl.dart';

import '../../Controllers/task_controller.dart';
import '../../Models/task_model.dart';

class TasksScreen extends StatelessWidget {
   TasksScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: GetBuilder<TaskController>(
          autoRemove: false,
          init: TaskController(),
          builder: (cont) => RefreshIndicator(
            onRefresh: () => cont.readBall(),
            child: ListView.builder(
              physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
              itemCount: cont.tasks.length,
              itemBuilder: (context, index) {
                TaskModel task = cont.tasks[index];
                return TaskCard(task: task,);
              },
            ),
          )),
    );
  }
}