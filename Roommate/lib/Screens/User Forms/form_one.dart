import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:ionicons/ionicons.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Screens/User%20Forms/location_form.dart';

class SignupFormOne extends StatefulWidget {
  const SignupFormOne({Key? key}) : super(key: key);

  @override
  State<SignupFormOne> createState() => _SignupFormOneState();
}

class _SignupFormOneState extends State<SignupFormOne> {
  int gender = 0;
  DateTime _chosenDateTime = DateTime.now();
  List<String> languages = ["Urdu", "English", "Punjabi", "Spanish"];
  void _showDatePicker(ctx) {
    showCupertinoModalPopup(
        context: ctx,
        builder: (_) => Container(
              height: 353,
              color: const Color.fromARGB(255, 255, 255, 255),
              child: Column(
                children: [
                  SizedBox(
                    height: 300,
                    child: CupertinoDatePicker(
                        initialDateTime: DateTime.now(),
                        mode: CupertinoDatePickerMode.date,
                        onDateTimeChanged: (val) {
                          setState(() {
                            _chosenDateTime = val;
                          });
                        }),
                  ),

                  // Close the modal
                  CupertinoButton(
                    child: const Text('OK'),
                    onPressed: () => Navigator.of(ctx).pop(),
                  )
                ],
              ),
            ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: myResources.background,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: myResources.themeColor, //change your color here
        ),
        backgroundColor: myResources.background,
        title: Text(
          "Enter your Details",
          style: TextStyle(color: myResources.themeColor),
        ),
        centerTitle: true,
      ),
      body:  SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 35,
              ),
              const Text(
                "Full name",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              ),
              Padding(
                //Add padding around textfield
                padding: const EdgeInsets.only(top: 15.0, bottom: 25),
                child: SizedBox(
                  height: 35,
                  child: TextFormField(
                    keyboardType: TextInputType.name,
                    onChanged: (val) {},
                    onSaved: (String? val) {}, // ame Controller
                    decoration: InputDecoration(
                      //Add th Hint text here.
                      contentPadding:
                          const EdgeInsets.only(top: 10, left: 15),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(3.0),
                      ),
                    ),
                  ),
                ),
              ),
              const Text(
                "Gender",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(10)),
                            side: BorderSide(
                                color: gender == 0
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        setState(() {
                          gender = 0;
                        });
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 12),
                        child: Center(
                            child: Text(
                          'Male',
                          style: TextStyle(
                              color: gender == 0
                                  ? myResources.themeColor
                                  : Colors.black54),
                        )),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                            side: BorderSide(
                                color: gender == 1
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        setState(() {
                          gender = 1;
                        });
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 12),
                        child: Center(
                            child: Text(
                          'Female',
                          style: TextStyle(
                              color: gender == 1
                                  ? myResources.themeColor
                                  : Colors.black54),
                        )),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                            side: BorderSide(
                                color: gender == 2
                                    ? myResources.themeColor
                                    : Colors.black54)),
                      ),
                      onPressed: () {
                        setState(() {
                          gender = 2;
                        });
                      },
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8, horizontal: 12),
                        child: Center(
                            child: Text(
                          'Other',
                          style: TextStyle(
                              color: gender == 2
                                  ? myResources.themeColor
                                  : Colors.black54),
                        )),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              const Text(
                "Date of Birth",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              ),
              Container(
                width: 180,
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.white,
                    shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        side: BorderSide(color: Colors.black54)),
                  ),
                  onPressed: () {
                    _showDatePicker(context);
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                      vertical: 8,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          DateFormat('dd MMMM yyyy').format(_chosenDateTime),
                          style: const TextStyle(color: Colors.black),
                        ),
                        Icon(
                          CupertinoIcons.chevron_down,
                          color: myResources.themeColor,
                          size: 14,
                        )
                      ],
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              const Text(
                "Languages",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              ),
              Wrap(
                direction: Axis.horizontal,
                children: [
                  for (var i in languages)
                    Container(
                      margin: const EdgeInsets.all(5),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 12, vertical: 8),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(color: Colors.black54)),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(i),
                          const SizedBox(
                            width: 5,
                          ),
                          InkWell(
                              onTap: () {},
                              child: Icon(
                                Ionicons.close_outline,
                                size: 15,
                                color: myResources.themeColor,
                              ))
                        ],
                      ),
                    ),
                  TextButton(
                    onPressed: () {
                      _settingModalBottomSheet(context);
                    },
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(
                          Ionicons.add,
                          color: myResources.themeColor,
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Text(
                          "Add more",
                          style: TextStyle(color: myResources.themeColor),
                        ),
                      ],
                    ),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 128.0,bottom: 50),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: myResources.themeColor,
                        shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(14))
                        ),
                      ),
                      onPressed: () {
                        Get.to(()=> const SignupFormTwo());
                      },

                      child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text('Confirm', style: TextStyle(color: Colors.white),),
                            Container(
                              padding: const EdgeInsets.all(8),
                              decoration: BoxDecoration(
                                borderRadius: const BorderRadius.all(Radius.circular(20)),
                                color: myResources.themeColor,
                              ),
                              child: Icon(Icons.arrow_forward_ios, color: Colors.white, size: 16,),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      );
  }
}

void _settingModalBottomSheet(context) {
  showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext bc) {
        return Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: Wrap(
            children: <Widget>[
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Add Language",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Padding(
                //Add padding around textfield
                padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: SizedBox(
                  height: 35,
                  child: TextField(
                    keyboardType: TextInputType.name,
                    onChanged: (val) {},
                    decoration: InputDecoration(
                      //Add th Hint text here.
                      contentPadding: const EdgeInsets.only(top: 10, left: 15),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(3.0),
                      ),
                    ),
                  ),
                ),
              ),
              Center(child: ElevatedButton(onPressed: () {}, child: Text("Confirm"),style: ElevatedButton.styleFrom(primary: myResources.themeColor),)),
              SizedBox(height: 20,)
            ],
          ),
        );
      });
}
