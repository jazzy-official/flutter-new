import 'package:daily_log/Models/task_model.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import '../View/loading_widget.dart';

class TaskController extends GetxController {
  List<TaskModel> tasks = [];
  final DatabaseReference databaseReference =
  FirebaseDatabase.instance.ref("Tasks");

  @override
  Future<void> onInit() async {
    // TODO: implement onInit
    await readBall();
    super.onInit();
  }


  Future<void> addTask(TaskModel task) async {
    Get.defaultDialog(title: '', content: const LoadingWidget(),backgroundColor: Colors.transparent,barrierDismissible: false);

      try{
        await databaseReference.push().set(task.toJson());
        await readBall();
        Get.back();
        Get.back();

        Get.snackbar("Done", "Task added");
      } catch(e) {
        Get.back();
        Get.snackbar("Error", e.toString());
      }

  }

  Future<void> updateTask(TaskModel task) async {
    Get.defaultDialog(title: '', content: const LoadingWidget(),backgroundColor: Colors.transparent,barrierDismissible: false);

    try{databaseReference.update({
      task.id!: task.toJson()
    });
      await readBall();
      Get.back();
      Get.back();

      Get.snackbar("Done", "Task updated");
    }
    catch(e){
      Get.back();
      Get.snackbar("Error", e.toString());
    }
  }


  Future<void> deleteTask(String id) async {

    await databaseReference.child(id).remove();

  }

  Future<void> readBall() async {
    List<TaskModel> ball = [];
    await databaseReference.once().then(( data) {
      if(data.snapshot.value != null){
        Map<dynamic,dynamic> values = data.snapshot.value as Map<dynamic,dynamic>;
        values.forEach((key, value) {
          TaskModel gr = TaskModel.fromJson(value,key);
          ball.add(gr);
        });
      }

    });
    tasks = ball;
    update();
  }

}