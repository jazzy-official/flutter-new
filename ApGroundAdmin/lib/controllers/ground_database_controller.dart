
import 'package:ap_ground_admin/Screens/Grounds/Models/ground_models.dart';
import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:firebase_database/firebase_database.dart';


class GroundController {
  final DatabaseReference databaseReference =
  FirebaseDatabase.instance.reference().child("Grounds");

  Future<void> saveGround(Ground ground) async {
   try{ await databaseReference.push().set(ground.toJson());
   }catch(e){
     displayToast(e.toString());
   }
  }

  Future<void> updateGround(Ground ground) async {
    try{
      databaseReference.update({
        ground.id! : ground.toJson()
      });
    }catch(e){
      print("Erorrrrno"+e.toString());
    }


  }
  Future<void> deleteData(String id) async {
    databaseReference.child(id).remove();

  }

  Future<List<Ground>> readData() async {
    List<Ground> grounds = [];
    await databaseReference.once().then((DataSnapshot snapshot) {
      snapshot.value.forEach((key, value) {
        Ground gr = Ground.fromJson(value,key);
        grounds.add(gr);
      });
    });
    return grounds;
  }



  // void readGround(){
  //   databaseReference.child("Grounds").once().then((DataSnapshot snapshot) {
  //     print('Data : ${snapshot.value}');
  //   });
  // }
}
