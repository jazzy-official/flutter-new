import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:rafi_admin/Components/AppBar/appbar.dart';
import 'package:rafi_admin/Components/Drawer/drawer.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Expenses/Models/expense_model.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Recovery/recovery_sheet.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Expenses/expenses.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/ChartsAccount/charts_account.dart';
import 'package:rafi_admin/Components/loader_widget.dart';
import 'package:rafi_admin/Controllers/accounts_controller.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../main.dart';
import 'Sub Screens/Balance Sheet/balance_sheet.dart';
import 'Sub Screens/Dashboard/Model/RecievePaidChecklist/receive_paid_checklist.dart';
import 'Sub Screens/Dashboard/Model/Recovery/recovery.dart';
import 'Sub Screens/Dashboard/Model/account model/accounts.dart';
import 'Sub Screens/Dashboard/dashboard.dart';
import 'Sub Screens/Payables/Model/payable_model.dart';
import 'Sub Screens/Payables/payabale.dart';
import 'Sub Screens/Receivable/receivable.dart';
import 'package:http/http.dart' as http;

class AccountsPage extends StatefulWidget {
  const AccountsPage({Key? key,}) : super(key: key);

  @override
  _AccountsPageState createState() => _AccountsPageState();
}

const List<String> tabNames = <String>[
  'Dashboard',
  'Payable',
  'Receivable',
  'Recovery Sheet',
  'Balance Sheet',
  'Expenses',
  'chart',
];
class _AccountsPageState extends State<AccountsPage> {
  //Function to fetch the values of accounts stored in shared prefrence
  final accountController = Get.put(AccountController());
  // Future getAccountData() async {
  //   SharedPreferences pref = await SharedPreferences.getInstance();
  //   String? data =  pref.getString('account');
  //   print(data);
  //   //getting data from prefs and converting to account model type
  //   accountData =  Account.fromJson(jsonDecode(jsonDecode(data!)));
  // }
  // Future getPaidReceivedData() async {
  //   SharedPreferences pref = await SharedPreferences.getInstance();
  //   String? RPdata = pref.getString('paidreceived');
  //   //getting data from prefs and converting to account model type
  //   Map<String, dynamic> receivedData = await jsonDecode(jsonDecode(RPdata!));
  //   List<dynamic> data = receivedData["Received"];
  //   List<Received> receives = data
  //       .map(
  //         (dynamic item) => Received.fromJson(item),
  //   )
  //       .toList();
  //   received = receives;
  // }
  // Future getRecoveryData() async {
  //   SharedPreferences pref = await SharedPreferences.getInstance();
  //   String? recoveryData =  pref.getString('recovery');
  //   //getting data from prefs and converting to account model type
  //   Map<String, dynamic> recoveryList = await jsonDecode(jsonDecode(recoveryData!));
  //   List<dynamic> data = recoveryList["Data"];
  //   List<RecoveryAmmount> recoverd = data
  //       .map(
  //         (dynamic item) => RecoveryAmmount.fromJson(item),
  //   )
  //       .toList();
  //   recovery = recoverd;
  // }
  Future<int?> getProjectId() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt('projectId');
  }
  late List<Payable> payables = [];
  late List<Expense> expenses = [];
  late List<Received> received;
  late List<RecoveryAmmount> recovery;
  // late Received receivedData;
  bool isLoading = false;
//List of screens shown in bottom slidable navbar
  List<Widget> _buildScreens() {
    return [
      Dashboard(),
      PayableScreen(),
      ReceivableScreen(),
      const RecoverySheet(),
      BalanceSheetPage(),
      ExpensePage(),
      ChartAccountScreen(),
    ];
  }
  // Future<String?> getToken() async {
  //   final SharedPreferences prefs = await SharedPreferences.getInstance();
  //   return  prefs.getString('token');
  // }
  Future getAllData()async {
    setState(() {
      isLoading = true;
    });
    // String? token = await getToken();

    // await accountController.setAccounts(projectid,token!);
    // await getPaidReceivedData();
    // await getRecoveryData();

    setState(() {
      isLoading = false;
    });
  }
  @override
  void initState() {
    getAllData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: tabNames.length,
      child: Scaffold(
        appBar:  AppBar(
          centerTitle: true,
          title: Text("Accounts", style: const TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.w500,
              color: Colors.white),),
          backgroundColor: Color(0xff004251),
        ),
        drawer: Drawer(child: const SideDrawer()),
        body: isLoading
            ? const Center(child: LoadingWidget())
            : TabBarView(
          children: List<Widget>.generate(tabNames.length, (int index) {
            return _buildScreens()[index];
          }),
        ),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.only(bottom: 1.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Material(
                  color: const Color(0xff004251),
                  child: TabBar(
                    indicatorColor: Colors.white,
                    isScrollable: true,
                    tabs: List.generate(tabNames.length, (index) {
                      return Tab(text: tabNames[index].toUpperCase());
                    }),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
