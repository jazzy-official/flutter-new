import 'package:get/get.dart';
import 'package:mongo_dart/mongo_dart.dart';

import 'db_controller.dart';

class DashBoardController extends GetxController {
  final dbHelper = Get.put(DatabaseController());

  Future<String> getTeamName1(Map<String, dynamic> matchModel1) async {
    Map<String, dynamic>? t1 =
        await dbHelper.getTeamData(ObjectId.parse(matchModel1['team1']));

    return t1!['teamName'];
  }

  Future<String> getTeamName2(Map<String, dynamic> mModel) async {
    Map<String, dynamic>? t2 =
        await dbHelper.getTeamData(ObjectId.parse(mModel['team2']));

    return t2!['teamName'];
  }

  @override
  void onClose() {
    // db.close();
    super.onClose();
  }
}
