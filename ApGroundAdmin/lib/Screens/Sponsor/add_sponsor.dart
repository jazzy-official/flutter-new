import 'dart:io';

import 'package:ap_ground_admin/Screens/Sponsor/models/sponsor_models.dart';
import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:ap_ground_admin/controllers/sponsor_controller.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as Path;

import '../../loading_widget.dart';

class AddSponsor extends StatefulWidget {
  final Sponsor? sponsor;
  const AddSponsor({Key? key, this.sponsor}) : super(key: key);

  @override
  _AddSponsorState createState() => _AddSponsorState();
}

class _AddSponsorState extends State<AddSponsor> {

  File? _image = null;
  final picker = ImagePicker();
  String fname = '';
  String description = '';
  String imgUrl = '';
  void checkeditupdate() {
    if (widget.sponsor != null) {
      setState(() {
        fname = widget.sponsor!.name;
        description = widget.sponsor!.description;
        imgUrl = widget.sponsor!.imgUrl;
      });
    }
  }
  @override
  void initState() {
    checkeditupdate();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(bottom: 30),
          child: Stack(
            children: [
              Positioned(
                left: 15,
                top: 30,
                child: BackButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
              Container(
                child: Form(
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                            top: 80, bottom: 0, left: 10, right: 10),
                        child: Container(
                          width: 150,
                          height: 150,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black, width: 1),
                              borderRadius: BorderRadius.circular(100)),
                          child: Container(
                            child: _image == null
                                ? Center(
                              child: IconButton(
                                onPressed: () {
                                  chooseImage();
                                },
                                icon: Icon(
                                  Icons.add_a_photo,
                                  size: 35,
                                ),
                              ),
                            )
                                : Stack(
                              children: [
                                Center(
                                  child: CircleAvatar(
                                    backgroundImage: FileImage(_image!),
                                    radius: 73,
                                  ),
                                ),
                                Positioned(
                                    right: 0,
                                    bottom: 0,
                                    child: IconButton(
                                      icon: Icon(
                                        Icons.add_a_photo,
                                        size: 30,
                                      ),
                                      onPressed: () {
                                        chooseImage();
                                      },
                                    ))
                              ],
                            ),
                          ),
                        ),
                      ),
                      Container(
                        height: 55,
                        //Add padding around textfield
                        padding:
                        EdgeInsets.only(top: 10.0, left: 10, right: 10),
                        child: TextFormField(
                          initialValue: fname,
                          keyboardType: TextInputType.name,
                          onChanged: (val) {
                            setState(() {
                              fname = val;
                            });
                          },
                          onSaved: (String? val) {}, // ame Controller
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(left: 10),
                            //Add th Hint text here.
                            hintText: "Name",
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        height: 110,
                        //Add padding around textfield
                        padding:
                        EdgeInsets.only(top: 10.0, left: 10, right: 10),
                        child: TextFormField(
                          maxLines: 5,
                          initialValue: description,
                          keyboardType: TextInputType.name,
                          onChanged: (val) {
                            setState(() {
                              description = val;
                            });
                          },
                          onSaved: (String? val) {}, // ame Controller
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(left: 10,top: 20),
                            //Add th Hint text here.
                            hintText: "Description",
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10.0, left: 3, right: 3),
                        child: TextButton(
                            onPressed: () async {
                              if(_image==null){
                                displayToast("Please Upload Image");
                              }else{
                                if(fname =="" ){
                                  displayToast("Name cant be empty");
                                }else if(description==""){
                                  displayToast("Please enter description");
                                }else{
                                  showDialog(
                                      context: context,
                                      builder: (context) {
                                        return  LoadingWidget();


                                      });
                                  await uploadfile();
                                  await SponsorController().saveSponsor(Sponsor(description: description, name: fname, imgUrl: imgUrl));
                                  displayToast(
                                      fname + " is saved as ");
                                  Navigator.pop(context);
                                  Navigator.pop(context);
                                }
                              }


                            },
                            child: Container(
                              width: MediaQuery.of(context).size.width * 1,
                              height: 45,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.indigo),
                              child: Center(
                                  child: Text(
                                    "Save",
                                    style: TextStyle(color: Colors.white),
                                  )),
                            )),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future uploadfile() async {
    Reference ref = FirebaseStorage.instance
        .ref()
        .child('images/${Path.basename(_image!.path)}');
    UploadTask uploadTask = ref.putFile(_image!);
    await uploadTask.whenComplete(() async {
      print('File Uploaded');
      await ref.getDownloadURL().then((value) {
        setState(() {
          imgUrl = value;
        });
      });
    });
  }

  chooseImage() async {
    try {
      final pickfile = await picker.pickImage(source: ImageSource.gallery);
      setState(() {
        _image = File(pickfile!.path);
      });
    } catch (e) {
      print(e.toString());
    }
  }
}
