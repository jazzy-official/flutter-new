import 'package:cached_network_image/cached_network_image.dart';
import 'package:ap_academy_admin/ModelView/Controller/SponsorController.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:ap_academy_admin/screens/shared/DeletPop.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';

class SponsorDetailCard extends StatelessWidget {
  final Map<String, dynamic>? sponsorDetailData;

  const SponsorDetailCard({Key? key, this.sponsorDetailData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 10, right: 10),
      child: Center(
        child: Container(
          height: Get.height * 0.6,
          child: Card(
            color: MyResources.backgroundColor,
            child: Stack(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Center(
                      child: Container(
                        height: 60,
                        width: Get.width,
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        color: MyResources.buttonColor,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: Text(
                                sponsorDetailData!['name'].toString().toUpperCase(),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                Get.dialog(Center(
                                    child: DeletpopUp(
                                      message: 'Are you sure you want to delete?',
                                      pressFunc: () async {
                                        await Get.find<SponsorController>()
                                            .delTeams(sponsorDetailData!['_id']);
                                        Get.back();
                                        Get.back();
                                        await Get.find<SponsorController>().refreshSponsor();
                                      },
                                      isNotDel: true,
                                    )));
                              },
                              child: Icon(
                                Icons.delete,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: CachedNetworkImage(
                          // height: Get.width,
                          maxHeightDiskCache:(Get.width*0.8).toInt() ,
                          fit: BoxFit.cover,
                          // width: 280,
                          imageUrl: sponsorDetailData!['imageUrl'],
                          placeholder: (context, url) =>
                              LoadingWidget(),
                          errorWidget:
                              (context, url, error) =>
                              Icon(Icons.error),
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 20,
                                  right: 10,
                                  left: 10,
                                ),
                                child: Container(
                                  width: Get.width,
                                  child: Text(
                                    sponsorDetailData!['description'],
                                    style: TextStyle(
                                        color: MyResources.buttonColor,
                                        fontSize: 16),textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                              // Padding(
                              //   padding: const EdgeInsets.only(
                              //     top: 20,
                              //     right: 10,
                              //     left: 10,
                              //   ),
                              //   child: Container(
                              //     width: 190,
                              //     child: Text(
                              //       detailData['surname'].toString(),
                              //       softWrap: false,
                              //       style: TextStyle(
                              //           color: MyResources.buttonColor,
                              //           fontSize: 16),
                              //     ),
                              //   ),
                              // ),
                              // Padding(
                              //   padding: const EdgeInsets.only(
                              //     top: 20,
                              //     right: 10,
                              //     left: 10,
                              //   ),
                              //   child: Container(
                              //     width: 300,
                              //     height: 20,
                              //     child: Text(
                              //       detailData['email'].toString(),
                              //       style: TextStyle(
                              //           color: MyResources.buttonColor,
                              //           fontSize: 16),
                              //     ),
                              //   ),
                              // ),
                              // Padding(
                              //   padding: const EdgeInsets.only(
                              //     top: 20,
                              //     right: 10,
                              //     left: 10,
                              //   ),
                              //   child: Container(
                              //     width: 190,
                              //     child: Text(
                              //       detailData['role'].toString(),
                              //       softWrap: false,
                              //       style: TextStyle(
                              //           color: MyResources.buttonColor,
                              //           fontSize: 16),
                              //     ),
                              //   ),
                              // ),
                              // Padding(
                              //   padding: const EdgeInsets.only(
                              //     top: 20,
                              //     right: 10,
                              //     left: 10,
                              //   ),
                              //   child: Container(
                              //     width: 190,
                              //     child: Text(
                              //       detailData['phone'].toString(),
                              //       softWrap: false,
                              //       style: TextStyle(
                              //           color: MyResources.buttonColor,
                              //           fontSize: 16),
                              //     ),
                              //   ),
                              // ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                // Align(
                //   alignment: Alignment.bottomLeft,
                //   child: Padding(
                //     padding: const EdgeInsets.only(
                //         top: 10, right: 10, left: 10, bottom: 10),
                //     child: Container(
                //       width: 200,
                //       child: Text(
                //         DateFormat.yMEd().add_jm().format(detailData['dob']),
                //         style: TextStyle(color: Colors.grey),
                //       ),
                //     ),
                //   ),
                // ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
