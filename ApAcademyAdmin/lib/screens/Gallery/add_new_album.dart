import 'dart:io';
// import 'package:image_pickers/image_pickers.dart';
import 'package:ap_academy_admin/ModelView/Controller/AlbumController.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class AddNewAlbum extends StatefulWidget {
  @override
  _AddNewAlbumState createState() => _AddNewAlbumState();
}

class _AddNewAlbumState extends State<AddNewAlbum> {
  final newAlbumController = Get.put(PhotoAlbumController());
  ///////////////////////////////////Controller//////////////////////////////////////
  TextEditingController _titleController = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();
  bool isImage = false;
  bool isVideo = false;
  @override
  void dispose() {
    newAlbumController.photos.clear();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyResources.backgroundColor,
      appBar: AppBar(
        backgroundColor:MyResources.buttonColor,
        title: Container(
          child: Text(
            'ADD ALBUM',
            style: GoogleFonts.roboto(),
          ),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            ////////////////////////////////////////////////////////////////Add album//////////////////////////////////
            Padding(
              padding: const EdgeInsets.only(top: 10.0, right: 10, left: 10),
              child: Text(
                "",
                style: GoogleFonts.roboto(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            /////////////////////////////////////////////////////////////////Add tittle////////////////////////////////
            Padding(
              padding: const EdgeInsets.only(top: 6.0, left: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "  Title:",
                    style: GoogleFonts.roboto(
                        fontSize: 15, fontWeight: FontWeight.w400),
                  ),
                ],
              ),
            ),
            Padding(
              /////////////////////////////////////////////////////////////Add padding around textfield
              padding: EdgeInsets.only(top: 7.0, left: 20, right: 20),
              child: Container(
                color: MyResources.backgroundColor,
                constraints: BoxConstraints(maxHeight: 200),
                child: TextField(
                  controller: _titleController,
                  maxLines: 4,
                  minLines: 1,
                  decoration: InputDecoration(
                    //Add th Hint text here.

                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
            ///////////////////////////////////////////////////////////////Add Description
            Padding(
              padding: const EdgeInsets.only(top: 10.0, left: 18),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    " Description:",
                    style: GoogleFonts.roboto(
                        fontSize: 15, fontWeight: FontWeight.w400),
                  ),
                ],
              ),
            ),
            Padding(
              //////////////////////////////////////////////////////////Add padding around textfield
              padding: EdgeInsets.only(top: 7.0, left: 20, right: 20),
              child: Container(
                height: 120,
                color: MyResources.backgroundColor,
                child: TextField(
                  keyboardType: TextInputType.multiline,
                  maxLines: 5,
                  controller: _descriptionController,
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
            ///////////////////////////////////////////////////////////Add photos
            Padding(
              padding: const EdgeInsets.only(top: 10.0, left: 18),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    " Upload Photos:",
                    style: GoogleFonts.roboto(
                        fontSize: 15, fontWeight: FontWeight.w400),
                  ),
                ],
              ),
            ),
            Padding(
              /////////////////////////////////////////////////////////Add padding around textfield
              padding: EdgeInsets.only(top: 7.0, left: 20, right: 20),
              child: Container(
                height: 140,
                width: Get.width,
                child: Card(
                  color: MyResources.backgroundColor,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10.0),
                          child: isImage
                              ? Obx(() => Container(
                                    width: 200,
                                    height: 100,
                                    child: GridView.count(
                                      scrollDirection: Axis.horizontal,
                                      crossAxisCount: 1,
                                      shrinkWrap: true,
                                      children: List.generate(
                                          newAlbumController.photos.length,
                                          (index) {
                                        return Image.file(
                                          newAlbumController.photos[index],
                                          width: 100,
                                          height: 100,
                                        );
                                      }),
                                    ),
                                  ))
                              : Container(
                                  child: Icon(
                                    Icons.image_outlined,
                                    size: 50,
                                  ),
                                ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 10.0),
                        child: IconButton(
                          onPressed: () async {
                            try {
                              await newAlbumController.geT()
                                  .then((value) => {
                                        setState(() {
                                          if (isImage == false) {
                                            isImage = true;
                                          }

                                          /////////////////////////////////////////////////////IsImage=isImage? false:true;
                                        }),
                                      });
                            } catch (er) {
                              print(er);
                              Get.snackbar('Warning',
                                  'You did not selected any picture!');
                            }
                          },
                          icon: Icon(
                            Icons.upload_sharp,
                            size: 30,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            ///////////////////////////////////////////////////////////////////////Add  videos button
            // Padding(
            //   padding: const EdgeInsets.only(top: 10.0, left: 18),
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //     crossAxisAlignment: CrossAxisAlignment.start,
            //     children: [
            //       Text(
            //         " Upload Videos:",
            //         style: GoogleFonts.roboto(
            //             fontSize: 15, fontWeight: FontWeight.w400),
            //       ),
            //     ],
            //   ),
            // ),
            // Padding(
            //   ////////////////////////////////////////////////////////////////////Add padding around textfield
            //   padding: EdgeInsets.only(top: 7.0, left: 20, right: 20),
            //   child: Container(
            //     height: 150,
            //     width: Get.width,
            //     child: Card(
            //       color: MyResources.backgroundColor,
            //       child: Row(
            //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //         children: [
            //           Padding(
            //             padding: const EdgeInsets.all(10.0),
            //             child: ClipRRect(
            //               borderRadius: BorderRadius.circular(0.0),
            //               child: newAlbumController.videos.length != 0
            //                   ? Obx(() => Container(
            //                         width: Get.width - 160,
            //                         height: 80,
            //                         child: GridView.count(
            //                           scrollDirection: Axis.horizontal,
            //                           crossAxisCount: 1,
            //                           shrinkWrap: true,
            //                           children: List.generate(
            //                               newAlbumController.videos.length,
            //                               (index) {
            //                             return Padding(
            //                               padding: const EdgeInsets.all(8.0),
            //                               child: Container(
            //                                 width: 40,
            //                                 height: 40,
            //                                 child: Image.asset(
            //                                   'assets/myphoto.jpg',
            //                                   fit: BoxFit.cover,
            //                                 ),
            //                               ),
            //                             );
            //                           }),
            //                         ),
            //                       ))
            //                   : Container(
            //                       child: Icon(
            //                         Icons.video_label,
            //                         size: 50,
            //                       ),
            //                     ),
            //             ),
            //           ),
            //           Padding(
            //             padding: const EdgeInsets.only(right: 10.0),
            //             child: IconButton(
            //               onPressed: () async {
            //                 await newAlbumController.pickvideos();
            //                 setState(() {
            //                   if (isVideo == false) {
            //                     isVideo = true;
            //                   }
            //
            //                   //     isImage=isImage? false:true;
            //                 });
            //               },
            //               icon: Icon(
            //                 Icons.upload_sharp,
            //                 size: 30,
            //               ),
            //             ),
            //           ),
            //         ],
            //       ),
            //     ),
            //   ),
            // ),
            Padding(
              padding:
                  EdgeInsets.only(top: 10.0, left: 20, right: 10, bottom: 10),
              child: Container(
                width: Get.width * 0.9,
                height: 45,
                child: RaisedButton(
                  textColor: Colors.white,
                  color: MyResources.buttonColor,
                  child: Text(
                    "Submit",
                  ),
                  onPressed: () async {
                    if (_titleController.text.isNotEmpty) {
                      Get.defaultDialog(
                          barrierDismissible: false,
                          title: "Loading...${newAlbumController.count.value} ",
                          content: Center(
                            child: LoadingWidget(),
                          ));
                      await newAlbumController.multiplePicUpload(
                          _titleController.text, _descriptionController.text);
                      newAlbumController.photos.clear();
                      if (!newAlbumController.videos.length.isEqual(0))
                        newAlbumController.videos.clear();
                      Get.back();
                      Get.back();
                      await newAlbumController.refreshData();
                    } else {
                      Get.snackbar(
                          "Name Error", "Please write name and description");
                    }
                  },
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
