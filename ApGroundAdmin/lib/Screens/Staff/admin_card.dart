import 'package:ap_ground_admin/Screens/Authentication/Models/user_model.dart';
import 'package:ap_ground_admin/Screens/Authentication/signup_screen.dart';
import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:ap_ground_admin/controllers/user_controller.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';

import '../delete_popup.dart';

class AdminCard extends StatelessWidget {
  final UserModel admin;
  final VoidCallback refresh;
   const AdminCard({Key? key, required this.admin, required this.refresh}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
          decoration: BoxDecoration(
            color: Colors.black,
            borderRadius: BorderRadius.circular(10),
            image: DecorationImage(
              colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.5), BlendMode.dstATop),
              image: admin.imgUrl==''?AssetImage('assets/profile.png') as ImageProvider : NetworkImage(admin.imgUrl),
              fit: BoxFit.cover,
            ),
            boxShadow: const [
              BoxShadow(
                color: Colors.black26,
                spreadRadius: 3,
                blurRadius: 5,
                offset: Offset(0, 5), // changes position of shadow
              ),
            ],
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment:MainAxisAlignment.end,
                children: [
                  PopupMenuButton(
                      icon: const Icon(Ionicons.ellipsis_vertical,color: Colors.white,),
                      itemBuilder:(context) => [
                        PopupMenuItem(
                          child: TextButton(onPressed: () async{
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => SignupScreen()),
                            ).then((value) => refresh());
                          },child: Text("Edit"),),
                        ),
                        PopupMenuItem(
                          child: TextButton(onPressed: () {
                            showDialog(
                                context: context,
                                builder: (_) => AlertDialog(
                                  content: DeletepopUp(message: "Are you sure You wanna delete "+admin.name+"?", pressFunc: () async {
                                    await UserController().deleteUser(admin.id!);
                                    displayToast(admin.name+" Deleted");
                                    refresh();
                                  },),
                                )
                            );
                          },child: Text("Delete"),),
                        ),

                      ]
                  )
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(admin.name,style: TextStyle(color: Colors.white,fontWeight: FontWeight.w600,fontSize: 18),),


                ],
              ),
            ],
          )),
    );
  }
}
