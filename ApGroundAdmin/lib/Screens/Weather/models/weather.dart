class Weather {
  final double temp;
  final double feelsLike;
  final double low;
  final double high;
  final double pressure;
  final double humidity;
  final String description;
  final double wind;
  final String icon;

  Weather(
      {required this.temp,
      required this.feelsLike,
      required this.low,
      required this.high,
      required this.pressure,
      required this.humidity,
      required this.description,
      required this.wind,
      required this.icon,
      });

  factory Weather.fromJson(Map<String, dynamic> json) {
    Weather weather= Weather(
      temp: json['main']['temp'].toDouble(),
      feelsLike: json['main']['feels_like'].toDouble(),
      low: json['main']['temp_min'].toDouble(),
      high: json['main']['temp_max'].toDouble(),
      pressure: json['main']['pressure'].toDouble(),
      humidity: json['main']['humidity'].toDouble(),
      description: json['weather'][0]['description'],
        wind: json['wind']['speed'].toDouble(),
        icon: json['weather'][0]['icon'],

    );
    return weather;
  }
}

// class DataService{
//   Future<WeatherResponse> getWeather(String city) async {
//     //api.openweathermap.org/data/2.5/weather?q={city name}&appid={API key}
//     final queryParameters ={'q': city,       'appid': 'b3a2d58bfe39c3aeca0cff66f6f53ee7'};
//     final uri =Uri.http(         'api.openweathermap.org', '/data/2.5/weather', queryParameters);
//     final response =await http.get(uri);
//     print(response.body);
//     final json = jsonDecode(response.body);
//     return WeatherResponse.fromJson(json);    } }
//
//   }



