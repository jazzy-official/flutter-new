import 'dart:math';

import 'package:ap_ground_admin/Screens/Canteen/Models/product_model.dart';
import 'package:ap_ground_admin/Screens/Canteen/Models/report_model.dart';
import 'package:ap_ground_admin/Screens/Canteen/Inventory/product_card.dart';
import 'package:ap_ground_admin/constants/util.dart';
import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:ap_ground_admin/controllers/inventory_controller.dart';
import 'package:ap_ground_admin/loading_widget.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:ionicons/ionicons.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../../delete_popup.dart';

class InventoryScreen extends StatefulWidget {
  const InventoryScreen({Key? key}) : super(key: key);

  @override
  State<InventoryScreen> createState() => _InventoryScreenState();
}

class _InventoryScreenState extends State<InventoryScreen> {

  bool isLoading = true;
  List<Product> products = [];
  List<Product> _foundResult = [];
  String name = '';
  int qty = -1;
  int price = -1;
  List<ChartData> chartData = [];
  late TooltipBehavior _tooltip;
  void _runFilter(String enteredKeyword) {
    List<Product> results = [];
    if (enteredKeyword.isEmpty) {
      // if the search field is empty or only contains white-space, we'll display all users
      results = products;
    } else {
      results = products
          .where((user) => user.name
          .toUpperCase()
          .contains(enteredKeyword.toUpperCase()))
          .toList();
      // we use the toLowerCase() method to make it case-insensitive
    }

    // Refresh the UI
    setState(() {
      _foundResult = results;
    });
  }
  void refresh () async {
    setState(() {
       chartData = [];
       isLoading = true;
    });
    List<Product> temp = await InventoryController().getProducts();
    setState(() {
      products = temp;
    });
    for(var pro in products){
      chartData.add(ChartData(pro.name, pro.qty.toDouble(), Colors.primaries[Random().nextInt(Colors.primaries.length)]));
    }
    setState(() {
      isLoading = false;
    });
    _runFilter("");
  }


  @override
  void initState() {
    refresh();
    _tooltip = TooltipBehavior(enable: true);
    super.initState();
  }
  Future<void> showInformationDialog(BuildContext context) async {
    List<ReportModel> reports = [];
    return await showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            return AlertDialog(
              content: Form(

                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SizedBox(
                        height: 40,
                        child: TextFormField(
                          keyboardType: TextInputType.name,
                          onChanged: (val) {
                            setState(() {
                              name = val;

                            });
                            print(name);
                          },
                          onSaved: (String? val) {}, // ame Controller
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(left: 10),
                            //Add th Hint text here.
                            hintText: "Name",
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 10,),
                      SizedBox(
                        height: 40,
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          onChanged: (val) {
                            setState(() {
                              qty = int.parse(val);
                            });
                          },
                          onSaved: (String? val) {}, // ame Controller
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(left: 10),
                            //Add th Hint text here.
                            hintText: "Quantity",
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 10,),
                      SizedBox(
                        height: 40,
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          onChanged: (val) {
                            setState(() {
                              price = int.parse(val);
                            });
                          },
                          onSaved: (String? val) {}, // ame Controller
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(left: 10),
                            //Add th Hint text here.
                            hintText: "Price/Piece",
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )),
              title: Text('Add Product'),
              actions: <Widget>[
                TextButton(onPressed: () async {
                  if(name == ""){
                    print(name);
                    displayToast("Please enter product name");
                  }else if(qty == -1){
                    displayToast("Please enter quntity");
                  }else if(price==-1){
                    displayToast("Please enter price");
                  }else{
                    try{
                      await InventoryController().saveProduct(Product(name: name, reports: reports, price: price, qty: qty));
                      refresh();
                      displayToast(name+" Added to inventory");
                    }catch(e){
                      displayToast(e.toString());
                    }
                    Navigator.of(context).pop();
                  }

                }, child: Text("ADD")),
                TextButton(onPressed: (){
                  Navigator.of(context).pop();
                }, child: Text("Cancel")),
              ],
            );
          });
        });
  }
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Expanded(
              child: isLoading ? const LoadingWidget() : Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(top: 7),
                      height: 35,
                      child: TextFormField(
                        keyboardType: TextInputType.name,
                        onChanged: (val) {
                          _runFilter(val);
                        },
                        onSaved: (String? val) {}, // ame Controller
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(left: 10),
                          //Add th Hint text here.
                          hintText: "Search",
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: ElevatedButton(
                      onPressed: () {
                        showInformationDialog(context);
                        // Navigator.push(
                        //   context,
                        //   MaterialPageRoute(builder: (context) => AddGroundForm()),
                        // );
                      },
                      child: Text("Add"),
                      style: ElevatedButton.styleFrom(primary: Color(0xff1758B4)),
                    ),
                  ),
                ],
              ),
              Padding(
                padding:
                const EdgeInsets.symmetric(vertical: 2.0,),
                child: Card(
                    child: Container(
                        height: 150,
                        child: SfCircularChart(
                            tooltipBehavior: _tooltip,
                            legend: Legend(isVisible: true,position: LegendPosition.right),
                            series: <CircularSeries>[
                              // Renders doughnut chart
                              DoughnutSeries<ChartData, String>(
                                  dataSource: chartData,
                                  pointColorMapper:(ChartData data,  _) => data.color,
                                  xValueMapper: (ChartData data, _) => data.x,
                                  yValueMapper: (ChartData data, _) => data.y
                              )
                            ]
                        )
                    )),
              ),
              Expanded(
                child: ListView.builder(
                  physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                    padding: const EdgeInsets.symmetric(vertical: 8),
                    itemCount: _foundResult.length,
                    itemBuilder: (BuildContext context, int index) {
                    Product product = _foundResult[index];
                    int tempqty = 0;
                      return Slidable(
                        actionPane: SlidableDrawerActionPane(),
                        actionExtentRatio: 0.15,
                        child: ProductCard(product: product, refresh: refresh,),
                        secondaryActions: <Widget>[
                          Container(
                            padding:EdgeInsets.symmetric(vertical: 5),
                            child: IconSlideAction(
                              caption: 'Delete',
                              color: Colors.red,
                              icon: Icons.delete,
                              onTap: () async {
                                try{
                                  showDialog(
                                      context: context,
                                      builder: (_) => AlertDialog(
                                        content: DeletepopUp(message: "Are you sure You wanna delete "+product.name+"?", pressFunc: () async {
                                          await InventoryController().deleteProduct(product.id!);
                                          refresh();
                                          Navigator.pop(context);
                                          displayToast("deleted successfully");
                                        },),
                                      )
                                  );



                                }catch(e){
                                  displayToast(e.toString());
                                }
                                refresh();
                                },
                            ),
                          ),
                        ],
                      );
                        // ProductCard(product: product, refresh: refresh,);
                    }),
              ),
            ],
          )),
        ],
      ),
    );
  }
}

class ChartData {
  ChartData(this.x, this.y, this.color);
  final String x;
  final double y;
  final Color color;
}