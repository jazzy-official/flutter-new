import 'dart:convert';
import 'package:ap_ground_admin/Screens/Weather/models/location.dart';
import 'package:ap_ground_admin/constants/util.dart';
import 'package:ap_ground_admin/controllers/weather_controller.dart';
import 'package:ap_ground_admin/loading_widget.dart';
import 'package:geocoding/geocoding.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:geolocator/geolocator.dart';
import 'package:ionicons/ionicons.dart';
import 'models/forcast.dart';
import 'models/weather.dart';

late LocationModel location;
class CurrentWeather extends StatefulWidget {
  // const CurrentWeather(Object location, {Key? key}) : super(key: key);
  late BuildContext context;
  CurrentWeather(this.context);



  @override
  _CurrentWeatherState createState() => _CurrentWeatherState();
}

class _CurrentWeatherState extends State<CurrentWeather> {


  late Forecast forecast;
  late Weather weather;
  bool isLoading = true;


  void refresh() async {
    setState(() {
      isLoading = true;
    });
    location = await WeatherController().GetLocationModel();
    forecast = await WeatherController().getForecast(location);
    weather = await WeatherController().getCurrentWeather(location.city);
    setState(() {
      isLoading = false;
    });
  }

  @override
  void initState() {
    refresh();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(

        backgroundColor: Colors.white,
        body: isLoading?LoadingWidget():
        ListView(
            children: <Widget>[
              currentWeatherViews( this.context),
              hourlyBoxes(forecast),
              dailyBoxes(forecast),
            ]
        )
    );
  }


  Widget currentWeatherViews(BuildContext context) {

    return Column(children: [
      createAppBar(context,location),
      weatherBox(weather),
      weatherDetailsBox(weather),
    ]);

  }


}

Widget createAppBar(BuildContext context,LocationModel location) {
  // return GestureDetector(
  //   onTap: () {
  //     showModalBottomSheet<void>(
  //       context: context,
  //       builder: (BuildContext context) {
  //         return Container(
  //             child: Column(children: [
  //               Icon(
  //                 Icons.keyboard_arrow_down_rounded,
  //                 color: Colors.black,
  //                 size: 24.0,
  //                 semanticLabel: 'Tap to change location',
  //               ),
  //               Expanded(
  //                   child: ListView.builder(
  //                       padding: const EdgeInsets.all(8),
  //                       itemCount: 1,
  //                       itemBuilder: (BuildContext context, int index) {
  //                         return GestureDetector(
  //                             onTap: () {
  //                               setState(() {
  //                                 // location = locations[index];
  //                                 // context = context;
  //                                 // location=locations[0];
  //                               });
  //                               Navigator.pop(context, );
  //                             },
  //                             child: Column(
  //                               children: <Widget>[
  //                                 Container(
  //                                   height: 50,
  //                                   child: Center(
  //                                     child: Text.rich(
  //                                       TextSpan(
  //                                         children: <TextSpan>[
  //                                           TextSpan(
  //                                               text:
  //                                               'Faisalabad, ',
  //                                               style: TextStyle(
  //                                                   fontWeight: FontWeight.bold,
  //                                                   fontSize: 16)),
  //                                           TextSpan(
  //                                               text:
  //                                               'Pakistan',
  //                                               style: TextStyle(
  //                                                   fontWeight: FontWeight.normal,
  //                                                   fontSize: 16)),
  //                                         ],
  //                                       ),
  //                                     ),
  //                                   ),
  //                                 ),
  //                                 Divider(),
  //                               ],
  //                             ));
  //                       })),
  //             ]));
  //       },
  //     );
  //   },
  //   child:
  return Container(
      padding:
      const EdgeInsets.only(left: 20, top: 10, bottom: 10, right: 20),
      margin: const EdgeInsets.only(
          top: 15, left: 15.0, bottom: 0, right: 15.0),
      decoration: BoxDecoration(
          color: (MyResources.buttonColor),
          borderRadius: BorderRadius.all(Radius.circular(60)),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.2),
              spreadRadius: 3,
              blurRadius: 3,
              offset: Offset(0, 2,), // changes position of shadow
            )
          ]),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text.rich(
            TextSpan(
              children: <TextSpan>[
                TextSpan(
                    text: location.city+", ",
                    style:
                    TextStyle(fontWeight: FontWeight.bold, fontSize: 16,color: Colors.white)),
                TextSpan(
                    text: location.country,
                    style: TextStyle(
                        fontWeight: FontWeight.normal, fontSize: 16,color: Colors.white)),

              ],
            ),
          ),
          Icon(
            Ionicons.location_outline,
            color: Colors.white,
            size: 16.0,
            semanticLabel: 'Tap to change location',
          ),
        ],
      ));
  // );
}
Widget weatherDetailsBox(Weather _weather) {
  return Container(
    padding: const EdgeInsets.only(left: 15, top: 25, bottom: 25, right: 15),
    margin: const EdgeInsets.only(left: 15, top: 5, bottom: 15, right: 15),
    decoration: BoxDecoration(
        color: Color(0xffe8eef1),
        borderRadius: BorderRadius.all(Radius.circular(20)),
        boxShadow: [
          BoxShadow(
            color: Colors.white.withOpacity(0.3),
            spreadRadius: 2,
            blurRadius: 5,
            offset: Offset(0, 3),
          )
        ]),
    child: Row(
      children: [
        Expanded(
            child: Column(
              children: [
                Container(
                    child: Text(
                      "Wind",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 12,
                          color: Colors.black54),
                    )),
                Container(
                    child: Text(
                      "${_weather.wind} km/h",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 15,
                          color: MyResources.coachHeading),
                    ))
              ],
            )
        ),
        Expanded(
            child: Column(
              children: [
                Container(
                    child: Text(
                      "Humidity",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 12,
                          color: Colors.black54),
                    )),
                Container(
                    child: Text(
                      "${_weather.humidity.toInt()}%",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 15,
                          color: MyResources.coachHeading),
                    ))
              ],
            )
        ),
        Expanded(
            child: Column(
              children: [
                Container(
                    child: Text(
                      "Pressure",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 12,
                          color: Colors.black54),
                    )),
                Container(
                    child: Text(
                      "${_weather.pressure} hPa",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 15,
                          color: MyResources.coachHeading),
                    ))
              ],
            )
        )
      ],
    ),
  );
}
Widget weatherBox(Weather _weather) {
  return Stack(children: [
    Container(
      padding: const EdgeInsets.all(15.0),
      margin: const EdgeInsets.all(15.0),
      height: 160.0,
      decoration: BoxDecoration(
          color: MyResources.headingColor,
          borderRadius: BorderRadius.all(Radius.circular(20))),
    ),
    Container(
        padding: const EdgeInsets.all(15.0),
        margin: const EdgeInsets.all(15.0),
        height: 160.0,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20))),
        child: Row(
          children: [
            Expanded(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      WeatherController().getWeatherIcon(_weather.icon),
                      Container(
                          margin: const EdgeInsets.all(5.0),
                          child: Text(
                            "${_weather.description}",
                            style: TextStyle(
                                fontWeight: FontWeight.normal,
                                fontSize: 16,
                                color: Colors.white),
                          )),
                      Container(
                          margin: const EdgeInsets.all(5.0),
                          child: Text(
                            "H:${_weather.high.toInt()}° L:${_weather.low.toInt()}°",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                fontWeight: FontWeight.normal,
                                fontSize: 13,
                                color: Colors.white),
                          )),
                    ])),
            Column(children: <Widget>[
              Container(
                  child: Text(
                    "${_weather.temp.toInt()}°",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 60,
                        color: Colors.white),
                  )
              ),
              Container(
                  margin: const EdgeInsets.all(0),
                  child: Text(
                    "Feels like ${_weather.feelsLike.toInt()}°",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 13,
                        color: Colors.white),
                  )),
            ])
          ],
        ))
  ]);
}
Widget hourlyBoxes(Forecast _forecast) {
  return Container(
      margin: EdgeInsets.symmetric(vertical: 0.0),
      height: 150.0,
      child: ListView.builder(
          padding: const EdgeInsets.only(left: 8, top: 0, bottom: 0, right: 8),
          scrollDirection: Axis.horizontal,
          itemCount: _forecast.hourly.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
                padding: const EdgeInsets.only(
                    left: 10, top: 15, bottom: 15, right: 10),
                margin: const EdgeInsets.all(5),
                decoration: BoxDecoration(
                    color: MyResources.buttonColor,
                    borderRadius: BorderRadius.all(Radius.circular(18)),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black87.withOpacity(0.1),
                        spreadRadius: 2,
                        blurRadius: 2,
                        offset: Offset(0, 1), // changes position of shadow
                      )
                    ]),
                child: Column(children: [
                  Text(
                    "${_forecast.hourly[index].temp}°",
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 17,
                        color: Colors.white),
                  ),
                  WeatherController().getWeatherIcon(_forecast.hourly[index].icon),
                  Text(
                    "${getTimeFromTimestamp(_forecast.hourly[index].dt)}",
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 12,
                        color: Color(0xffBFD7ED)),
                  ),
                ]));
          }));
}
Widget dailyBoxes(Forecast _forcast) {
  return ListView.builder(
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      padding: const EdgeInsets.only(left: 8, top: 0, bottom: 0, right: 8),
      itemCount: _forcast.daily.length,
      itemBuilder: (BuildContext context, int index) {
        return Container(
            padding: const EdgeInsets.only(
                left: 10, top: 5, bottom: 5, right: 10),
            margin: const EdgeInsets.all(1),
            child: Row(children: [
              Expanded(
                  child: Text(
                    "${getDateFromTimestamp(_forcast.daily[index].dt)}",
                    style: TextStyle(fontSize: 14, color: Colors.black),
                  )),
              Expanded(
                  child: WeatherController().getWeatherIconSmall(_forcast.daily[index].icon)),
              Expanded(
                  child: Text(
                    "${_forcast.daily[index].high.toInt()}/${_forcast.daily[index].low.toInt()}",
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 14, color: Color(0xffBFA2DB)),
                  )),
            ]));
      });
}
String getTimeFromTimestamp(int timestamp) {
  var date = new DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
  var formatter = new DateFormat('h:mm a');
  return formatter.format(date);
}
String getDateFromTimestamp(int timestamp) {
  var date = new DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
  var formatter = new DateFormat('E');
  return formatter.format(date);
}