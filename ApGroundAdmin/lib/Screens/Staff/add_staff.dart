import 'dart:io';
import 'package:ap_ground_admin/Screens/Payrole/models/payroll_model.dart';
import 'package:ap_ground_admin/constants/util.dart';
import 'package:ap_ground_admin/controllers/designation_controller.dart';
import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:ap_ground_admin/loading_widget.dart';
import 'package:path/path.dart' as Path;
import 'package:ap_ground_admin/Screens/Staff/Models/staff_model.dart';
import 'package:ap_ground_admin/controllers/staff_controller.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:ionicons/ionicons.dart';

class AddStaffForm extends StatefulWidget {
  final Staff? staff;
  const AddStaffForm({Key? key, this.staff}) : super(key: key);

  @override
  _AddStaffFormState createState() => _AddStaffFormState();
}

class _AddStaffFormState extends State<AddStaffForm> {
  File? _image = null;
  final picker = ImagePicker();
  String fname = '';
  String designation = '';
  String number = '';
  String imgUrl = '';
  double salary = 0;
  List<String> designations = [];
  void refresh()async{
    setState(() {
      designations = [];
    });
    List<Map<String,String>> desg =  await DesignationController().readDesignation();
    desg.forEach((element) {designations.add(element.values.toString());});
    setState(() {
    });
  }
  void checkeditupdate() {
    if (widget.staff != null) {
      setState(() {
        fname = widget.staff!.name;
        designation = widget.staff!.designation;
        number = widget.staff!.number;
        imgUrl = widget.staff!.imgUrl;
        salary = widget.staff!.payroll.salary;
      });
    }
  }
  Future<void> showInformationDialog(BuildContext context) async {
    String name = '';
    return await showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            return AlertDialog(
              content: Form(

                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SizedBox(
                        height: 40,
                        child: TextFormField(
                          keyboardType: TextInputType.name,
                          onChanged: (val) {
                            setState(() {
                              name = val;

                            });
                            print(name);
                          },
                          onSaved: (String? val) {}, // ame Controller
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(left: 10),
                            //Add th Hint text here.
                            hintText: "Name",
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 10,),
                    ],
                  )),
              title: Text('Add Designation'),
              actions: <Widget>[
                TextButton(onPressed: () async {
                  if(name == ""){
                    print(name);
                    displayToast("Please enter Designation");
                  }else{
                    try{
                      await DesignationController().saveDesignation(name);
                      displayToast(name+" Added to Desination");
                      refresh();
                    }catch(e){
                      displayToast(e.toString());
                    }
                    Navigator.of(context).pop();
                  }

                }, child: Text("OK")),
                TextButton(onPressed: (){
                  Navigator.of(context).pop();
                }, child: Text("Cancel")),
              ],
            );
          });
        });
  }


  @override
  void initState() {
    refresh();
    checkeditupdate();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(bottom: 30),
          child: Stack(
            children: [
              Positioned(
                left: 15,
                top: 30,
                child: BackButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
              Container(
                child: Form(
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                            top: 80, bottom: 0, left: 10, right: 10),
                        child: Container(
                          width: 150,
                          height: 150,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black, width: 1),
                              borderRadius: BorderRadius.circular(100)),
                          child: Container(
                            child: _image == null
                                ? Center(
                                    child: IconButton(
                                      onPressed: () {
                                        chooseImage();
                                      },
                                      icon: Icon(
                                        Icons.add_a_photo,
                                        size: 35,
                                      ),
                                    ),
                                  )
                                : Stack(
                                    children: [
                                      Center(
                                        child: CircleAvatar(
                                          backgroundImage: FileImage(_image!),
                                          radius: 73,
                                        ),
                                      ),
                                      Positioned(
                                          right: 0,
                                          bottom: 0,
                                          child: IconButton(
                                            icon: Icon(
                                              Icons.add_a_photo,
                                              size: 30,
                                            ),
                                            onPressed: () {
                                              chooseImage();
                                            },
                                          ))
                                    ],
                                  ),
                          ),
                        ),
                      ),
                      Container(
                        height: 55,
                        //Add padding around textfield
                        padding:
                            EdgeInsets.only(top: 10.0, left: 10, right: 10),
                        child: TextFormField(
                          initialValue: fname,
                          keyboardType: TextInputType.name,
                          onChanged: (val) {
                            setState(() {
                              fname = val;
                            });
                          },
                          onSaved: (String? val) {}, // ame Controller
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(left: 10),
                            //Add th Hint text here.
                            hintText: "First Name",
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Container(
                              height: 55,
                              //Add padding around textfield
                              padding: EdgeInsets.only(
                                  top: 10.0, left: 10, right: 10),
                              child: Autocomplete<String>(
                                initialValue: TextEditingValue(text: designation),
                                optionsBuilder: (TextEditingValue textEditingValue) {
                                  if (textEditingValue.text == '') {
                                    return const Iterable<String>.empty();
                                  }
                                  return designations.where((String option) {
                                    return option.toLowerCase().contains(textEditingValue.text.toLowerCase());
                                  });
                                },

                                fieldViewBuilder: (
                                    BuildContext context,
                                    TextEditingController fieldTextEditingController,
                                    FocusNode fieldFocusNode,
                                    VoidCallback onFieldSubmitted
                                    ) {
                                  return TextFormField(
                                    controller: fieldTextEditingController,
                                    focusNode: fieldFocusNode,

                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.only(left: 10),
                                      //Add th Hint text here.
                                      hintText: "Designation",
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10.0),
                                      ),
                                    ),
                                  );
                                },

                                onSelected: (String selection) {
                                  setState(() {
                                    designation = selection.substring(1,selection.length-1);
                                  });
                                  print('You just selected $selection');
                                },
                              ),
                            ),
                          ),
                          TextButton(
                              onPressed: () {
                                showInformationDialog(context);
                              },
                              child: Container(
                                margin: EdgeInsets.only(top: 10,right: 10),
                                padding: EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          width: 1,
                                          color: MyResources.buttonColor),
                                      borderRadius: BorderRadius.circular(10)),
                                  child: const Icon(Icons.add)))
                        ],
                      ),
                      Container(
                        height: 55,
                        //Add padding around textfield
                        padding:
                            EdgeInsets.only(top: 10.0, left: 10, right: 10),
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          initialValue: salary.toString()=="0.0"?"":salary.toString(),
                          onChanged: (val) {
                            setState(() {
                              salary = double.parse(val);
                            });
                          },
                          onSaved: (String? val) {}, // ame Controller
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(left: 10),
                            //Add th Hint text here.
                            hintText: "Salary",
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        height: 55,
                        //Add padding around textfield
                        padding:
                            EdgeInsets.only(top: 10.0, left: 10, right: 10),
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          initialValue: number,
                          onChanged: (val) {
                            setState(() {
                              number = val;
                            });
                          },
                          onSaved: (String? val) {}, // ame Controller
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(left: 10),
                            //Add th Hint text here.
                            hintText: "Phone",
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10.0, left: 3, right: 3),
                        child: TextButton(
                            onPressed: () async {
                              showDialog(
                                  context: context,
                                  builder: (context) {
                                      return  LoadingWidget();


                                  });
                              if (widget.staff == null) {
                                await uploadfile();
                                await StaffController().saveStaff(Staff(
                                    designation: designation,
                                    name: fname,
                                    number: number,
                                    imgUrl: imgUrl,
                                    payroll: Payroll(date: DateTime.now(), salary: salary, isPaid: false)));
                                displayToast(
                                    fname + " is saved as a " + designation);
                                Navigator.pop(context);
                                Navigator.pop(context);
                              } else {
                                await StaffController().updateStaff(Staff(
                                    id: widget.staff!.id,
                                    designation: designation,
                                    name: fname,
                                    number: number,
                                    imgUrl: imgUrl,
                                    payroll: widget.staff!.payroll));
                                Navigator.pop(context);
                                Navigator.pop(context);
                              }
                            },
                            child: Container(
                              width: MediaQuery.of(context).size.width * 1,
                              height: 45,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.indigo),
                              child: Center(
                                  child: Text(
                                "Save",
                                style: TextStyle(color: Colors.white),
                              )),
                            )),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future uploadfile() async {
    Reference ref = FirebaseStorage.instance
        .ref()
        .child('images/${Path.basename(_image!.path)}');
    UploadTask uploadTask = ref.putFile(_image!);
    await uploadTask.whenComplete(() async {
      print('File Uploaded');
      await ref.getDownloadURL().then((value) {
        setState(() {
          imgUrl = value;
        });
      });
    });
  }

  chooseImage() async {
    try {
      final pickfile = await picker.pickImage(source: ImageSource.gallery);
      setState(() {
        _image = File(pickfile!.path);
      });
    } catch (e) {
      print(e.toString());
    }
  }
}
