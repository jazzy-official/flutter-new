
import 'package:ap_ground_admin/Screens/Grounds/Models/slot_model.dart';

class Booking {
  final String? id;
  final String groundName;
  final String groundAddress;
  final String groundId;
  final int mealPrice;
  final DateTime date;
  final Slot slot;
   bool isCancel;

  Booking({required this.mealPrice,required this.groundAddress,required this.groundName,this.id,required this.groundId,required this.date, required this.slot,required this.isCancel,});

// NOTE: implementing functionality here in the next step!

  factory Booking.fromJson(dynamic json,String id) {
    return Booking(
      id : id,
      groundId: json['groundId'],
      groundName: json['groundName'],
      slot: Slot.fromJson(json['slot']),
      date: DateTime.parse(json['date']),
      isCancel: json['isCancel'],
      groundAddress: json['groundAddress'],
      mealPrice: json['mealPrice'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['groundName'] = groundName;
    data['groundId'] = groundId;
    data['date'] = date.toString();
    data['slot'] = slot.toJson();
    data['isCancel'] = isCancel;
    data['groundAddress'] = groundAddress;
    data['mealPrice'] = mealPrice;
    return data;
  }
}