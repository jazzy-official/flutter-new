class LocationModel {
  final double lat;
  final double lon ;

  LocationModel( this.lat,  this.lon);

  LocationModel.fromJson(Map<String, dynamic> json)
      : lat = json['name'],
        lon = json['email'];

  Map<String, dynamic> toJson() => {
    'name': lat,
    'email': lon,
  };
}