
import 'package:ap_ground_admin/Screens/Authentication/Models/user_model.dart';
import 'package:ap_ground_admin/Screens/drawer.dart';
import 'package:ap_ground_admin/controllers/user_controller.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../wrapper.dart';

final _auth = FirebaseAuth.instance;

Future signup (String e, String p,BuildContext context) async {
  try {
    final newUser = await _auth.createUserWithEmailAndPassword(email: e, password: p,);
    Navigator.push(context,
      MaterialPageRoute(
          builder: (context) => const Wrapper()),
    );
  }
  catch (e)
  {
    displayToast(e.toString());
    print(e);
  }
}

Future<bool> Login(String e,String p)async{
  bool isAdmin = await UserController().checkRole(e);
  if(isAdmin){
    try {
      final newUser = await _auth.signInWithEmailAndPassword(
          email: e, password: p);

      return (newUser != null) ? true
      //successfully login
      //navigate the user to main page
      // i am just showing toast message here
          : false;

    } catch (e) {
      displayToast(e.toString());
      print(e.toString());
      return false;
    }
  }else{
    displayToast("You are not an admin");
    return false;
  }


}


Future<void> signInWithGoogle(BuildContext context) async {
  // Trigger the authentication flow
  final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

  // Obtain the auth details from the request
  final GoogleSignInAuthentication? googleAuth = await googleUser?.authentication;

  // Create a new credential
  final credential = GoogleAuthProvider.credential(
    accessToken: googleAuth?.accessToken,
    idToken: googleAuth?.idToken,
  );

  // Once signed in, return the UserCredential
  try {
     await FirebaseAuth.instance.signInWithCredential(
        credential);
     Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const Wrapper()),
    );
    displayToast("Welcome");
  } catch(e){
displayToast(e.toString());
  }
}

Future<void> resetPassword(String e,BuildContext context) async {
  try {
    await _auth.sendPasswordResetEmail(email: e);
    displayToast("Varification Email is sent");
    Navigator.pop(context);
  } catch (e) {
    displayToast(e.toString());
  }
}

displayToast(String message){
  Fluttertoast.showToast(msg: message,backgroundColor: Colors.black45,);
}