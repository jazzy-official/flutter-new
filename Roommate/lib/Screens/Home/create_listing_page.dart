// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_sliding_up_panel/flutter_sliding_up_panel.dart';
// import 'package:get/get.dart';
// import 'package:ionicons/ionicons.dart';
// import 'package:room_buddy/Constants/util.dart';
// import 'package:room_buddy/Screens/Home/Filters/room_filter.dart';
// import 'package:room_buddy/Screens/Home/Widgets/room_list.dart';
// import 'package:room_buddy/Screens/Home/Widgets/map_view_page.dart';
// import 'package:room_buddy/Screens/Home/room_detail.dart';
//
// class ListingPage extends StatefulWidget {
//   const ListingPage({Key? key}) : super(key: key);
//
//   @override
//   State<ListingPage> createState() => _ListingPageState();
// }
//
// class _ListingPageState extends State<ListingPage> {
//
//   bool listview = true;
//   late ScrollController scrollController;
//
//   ///The controller of sliding up panel
//   SlidingUpPanelController panelController = SlidingUpPanelController();
//
//   @override
//   void initState() {
//     scrollController = ScrollController();
//     scrollController.addListener(() {
//       if (scrollController.offset >=
//           scrollController.position.maxScrollExtent &&
//           !scrollController.position.outOfRange) {
//         panelController.expand();
//       } else if (scrollController.offset <=
//           scrollController.position.minScrollExtent &&
//           !scrollController.position.outOfRange) {
//         panelController.anchor();
//       } else {}
//     });
//     super.initState();
//   }
//   @override
//   Widget build(BuildContext context) {
//     return Stack(
//       children: [
//         Scaffold(
//           appBar: AppBar(
//               backgroundColor: myResources.background,
//               iconTheme: IconThemeData(
//                 color: Colors.black, //change your color here
//               ),
//               title: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: const [
//                   Text(
//                     "Showing layout for HSR layout",
//                     style: TextStyle(color: Colors.black, fontSize: 16),
//                   ),
//                   Text(
//                     "Showing 23 results",
//                     style: TextStyle(fontSize: 13, color: Colors.black45),
//                   ),
//                 ],
//               ),
//               bottom: PreferredSize(
//                 preferredSize: const Size.fromHeight(48.0),
//                 child: Padding(
//                   //Add padding around textfield
//                   padding: const EdgeInsets.only(
//                       top: 0, bottom: 10, right: 15, left: 15),
//                   child: Row(
//                     mainAxisSize: MainAxisSize.min,
//                     children: [
//                       Expanded(
//                         child: SizedBox(
//                           height: 35,
//                           child: TextFormField(
//                             keyboardType: TextInputType.name,
//                             onChanged: (val) {},
//                             initialValue: "Faisalabad",
//                             decoration: InputDecoration(
//                               //Add th Hint text here.
//                               fillColor: Colors.grey[300],
//                               filled: true,
//                               hintText: "Search by locality or landmark",
//                               hintStyle: const TextStyle(
//                                   color: Colors.black38,
//                                   fontSize: 13,
//                                   fontWeight: FontWeight.w500),
//                               prefixIcon: const Icon(
//                                 Ionicons.location_outline,
//                                 color: Colors.black,
//                                 size: 20,
//                               ),
//                               contentPadding:
//                                   const EdgeInsets.only(top: 0, left: 0),
//                               border: OutlineInputBorder(
//                                 borderRadius: BorderRadius.circular(3),
//                                 borderSide: const BorderSide(
//                                   width: 0,
//                                   style: BorderStyle.none,
//                                 ),
//                               ),
//                               // OutlineInputBorder(
//                               //   borderRadius: BorderRadius.circular(3.0),
//                               // ),
//                             ),
//                           ),
//                         ),
//                       ),
//                       TextButton(
//                           onPressed: () {
//                             panelController.expand();
//                           },
//                           child: Row(
//                             children: [
//                               Icon(
//                                 Ionicons.options,
//                                 color: Colors.black45,
//                               ),
//                               Text(
//                                 " Filters",
//                                 style: TextStyle(color: Colors.black45),
//                               )
//                             ],
//                           )
//                       )
//                     ],
//                   ),
//                 ),
//               )),
//           floatingActionButton: listview ?
//           FloatingActionButton(
//             onPressed: () {
//               setState(() {
//                 listview = false;
//               });
//             },
//             child: Column(
//               children: [
//                 Container(height: 9,),
//                 Icon(
//                   CupertinoIcons.map_fill,
//                   color: myResources.semiColor,
//                 ),
//                 Text("Map",style: TextStyle(color: myResources.semiColor,fontSize: 11),)
//               ],
//             ),
//             backgroundColor: myResources.background,
//           ):null,
//           body: AnimatedSwitcher(
//             transitionBuilder: (widget, animation) => SlideTransition(
//               position: Tween<Offset>(begin: Offset(0.0, 1.0), end: Offset(0.0, 0.0)).animate(animation),
//               child: widget,
//             ) ,
//             duration: Duration(milliseconds: 1000),
//             child: listview?  Padding(
//               padding: EdgeInsets.all(10.0),
//               child: RoomList(route: () { Get.to(RoomDetailScreen()); },),
//             ):Container()
//             // MapViewPage(toggleView: () { setState(() {
//             //   listview = true;
//             // }); },),
//           )
//         ),
//         SlidingUpPanelWidget(child: RoomFilters(panelController: panelController,), controlHeight: 0, panelController: panelController,enableOnTap: false,)
//       ],
//     );
//   }
// }
