import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:state_management/Controllers/order_controller.dart';

import 'Widgets/order_item.dart';
import 'app_drawer.dart';
class OrderScreen extends StatelessWidget {
  var orderController = Get.put(OrderController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Yours Orders"),
      ),
      drawer: const AppDrawer(),
      body: ListView.builder(
          itemCount: orderController.orders.length,
          itemBuilder: (context, index) =>
              OrderItem(orderController.orders[index])),
    );
  }
}