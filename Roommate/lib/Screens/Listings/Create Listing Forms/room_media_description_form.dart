import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:image_picker/image_picker.dart';
import 'package:ionicons/ionicons.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Screens/Listings/Create%20Listing%20Forms/addroom_roommateprefrence_form.dart';

import '../../../controllers/my_listing_controller.dart';

class RoomMediaDescriptionForm extends StatelessWidget {
   RoomMediaDescriptionForm({Key? key}) : super(key: key);

  List<XFile>? _imageFileList;
  final ImagePicker _picker = ImagePicker();
  TextEditingController titlecontroller = TextEditingController();
  TextEditingController descontroller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          iconTheme: const IconThemeData(color: Colors.black),
          title: Text("Step 5: Media & description",style: TextStyle(color: Colors.black),),
          bottom: PreferredSize(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 22),
              child: LinearProgressIndicator(
                color: myResources.themeColor,
                backgroundColor: Colors.grey.shade300,
                minHeight: 6,
                value: 5 / 6,
              ),
            ),
            preferredSize: const Size.fromHeight(50),
          )),
      body: GetBuilder<MyListingController>(
        init: MyListingController(),
        builder: (cont) => Padding(
          padding: const EdgeInsets.all(15.0),
          child: ListView(
            children: [
              Container(
                height: 150,
                padding: EdgeInsets.all(10),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(3),
                    border: Border.all(color: Colors.black26)),
                child: cont.images.isEmpty ?InkWell(
                  onTap: () async {
                    _imageFileList = await _picker.pickMultiImage();
                    if(_imageFileList !=null){
                      _imageFileList!.forEach((element) { cont.images.add(element.path);});

                    }
                    cont.update();
                    print("working");
                  },
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 15.0, bottom: 15),
                        child: Icon(
                          Icons.add_photo_alternate_outlined,
                          size: 40,
                          color: myResources.themeColor,
                        ),
                      ),
                      Text(
                        "Upload pictures of room",
                        style:
                        TextStyle(color: myResources.themeColor, fontSize: 16),
                      ),
                      SizedBox(height: 5,),
                      const SizedBox(
                        width: 200,
                        child: Text(
                          "Add atleast 4 photos of the room you're listing",
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.black45, fontSize: 13,fontWeight: FontWeight.w600),
                        ),
                      ),
                    ],
                  ),
                ):GridView.builder(

                    gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                        maxCrossAxisExtent: 120,
                        childAspectRatio: 8 / 6,
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 10),
                    itemCount: cont.images.length+1,
                    itemBuilder: (BuildContext ctx, index)  {
                      if (index == 0) {
                        return InkWell(
                          onTap: () async {
                            _imageFileList = null;
                            _imageFileList = await _picker.pickMultiImage();
                            if(_imageFileList!=null){
                              _imageFileList!.forEach((element) {
                                cont.images.add(element.path);
                              });

                            };
                            cont.update();
                          },
                          child: Icon(
                            Icons.add_photo_alternate_outlined,
                            size: 40,
                            color: myResources.themeColor,
                          ),
                        );
                      }
                      final image = cont.images[index-1];
                      return Container(
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            color: Colors.amber,
                            borderRadius: BorderRadius.circular(3),
                            image: DecorationImage(image: FileImage(File(image)),fit: BoxFit.cover)
                        ),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children:  [
                                InkWell(
                                    onTap: (){
                                      cont.images.removeAt(index-1);
                                      cont.update();
                                    },
                                    child: Icon(Ionicons.remove_circle,color: Colors.red,)
                                )
                              ],
                            ),
                          ],
                        ),
                      );
                    }),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 25.0),
                child: Divider(height: 2, color: Colors.grey.shade600,),
              ),
              const Text(
                "Add title for your listing",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              ),
              Padding(
                //Add padding around textfield
                padding: const EdgeInsets.only(top: 20.0, bottom: 20),
                child: TextField(
                  controller: titlecontroller,
                  keyboardType: TextInputType.name,
                  onChanged: (val) {},
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    hintText: "title ...",
                    hintStyle: TextStyle(fontSize: 13),
                    contentPadding: const EdgeInsets.only(top: 15, left: 15,),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(3.0),
                    ),
                  ),
                ),
              ),
              const Text(
                "Add a description",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              ),
              Padding(
                //Add padding around textfield
                padding: const EdgeInsets.only(top: 20.0, bottom: 30),
                child: SizedBox(
                  height: 100,
                  child: TextField(
                    controller: descontroller,
                    maxLines: 10,
                    keyboardType: TextInputType.name,
                    onChanged: (val) {},
                    decoration: InputDecoration(
                      //Add th Hint text here.
                      hintText: "Tell us about room ...",
                      hintStyle: TextStyle(fontSize: 13),
                      contentPadding: const EdgeInsets.only(top: 15, left: 15,),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(3.0),
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: myResources.themeColor,
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(14))),
                ),
                onPressed: () {
                  if(cont.images.isEmpty||cont.images.length<4) {
                    Get.snackbar("Warning", "Please add atleast 4 images of your room",snackPosition: SnackPosition.BOTTOM);
                  } else if(titlecontroller.text==""){
                    Get.snackbar("Warning", "Please add title",snackPosition: SnackPosition.BOTTOM);
                  } else if(descontroller.text==""){
                    Get.snackbar("Warning", "Please add description",snackPosition: SnackPosition.BOTTOM);
                  } else{
                    cont.title = titlecontroller.text;
                    cont.description = descontroller.text;
                    Get.to( RoommatePreferenceForm());
                  }


                },
                child: Container(
                  padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SizedBox(width: 20,),
                      Text(
                        'Proceed',
                        style: TextStyle(color: Colors.white),
                      ),
                      Container(
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                          borderRadius:
                          const BorderRadius.all(Radius.circular(20)),
                          color: myResources.themeColor,
                        ),
                        child: Icon(
                          Icons.arrow_forward_ios,
                          color: Colors.white,
                          size: 16,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

