import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Screens/Authentication/enter_mobile.dart';

class FirstScreen extends StatelessWidget {
  const FirstScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(top: 50),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  Image.asset("assets/onboard3.png"),
                  SizedBox(
                    height: 50,
                  ),
                  Text(
                    "Welcome to Room Buddy !",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  const Text(
                    "Let's get started",
                    style: TextStyle(color: Colors.grey),
                  ),
                ],
              ),
              Column(
                children: [
                  TextButton(
                      onPressed: () {
                        Get.to(() => EnterMobile());
                      },
                      child: Container(
                        height: 40,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            color: myResources.themeColor,
                            borderRadius: BorderRadius.circular(3)),
                        child: const Center(
                            child: Text(
                          "Sign Up",
                          style: TextStyle(color: Colors.white),
                        )),
                      )),
                  TextButton(
                      onPressed: () {},
                      child: Container(
                        height: 40,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(3),
                            border: Border.all(color: myResources.themeColor)),
                        child: Center(
                            child: Text(
                          "Log in",
                          style: TextStyle(color: myResources.themeColor),
                        )),
                      )),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: RichText(
                  text:  TextSpan(
                    children: [
                       const TextSpan(
                        text: 'By signing up you agree to our ',
                        style:  TextStyle(color: Colors.black,fontWeight: FontWeight.w500),
                      ),
                       TextSpan(
                        text: 'Terms&Conditions',
                        style:  TextStyle(color: myResources.themeColor,fontWeight: FontWeight.w500),
                        recognizer:  TapGestureRecognizer()
                          ..onTap = () {},
                      ),
                      const TextSpan(
                        text: ' and acknowledge our',
                        style:  TextStyle(color: Colors.black,fontWeight: FontWeight.w500),
                      ),
                       TextSpan(
                        text: ' Privacy Policy',
                        style:  TextStyle(color: myResources.themeColor,fontWeight: FontWeight.w500),
                        recognizer:  TapGestureRecognizer()
                          ..onTap = () {},
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
