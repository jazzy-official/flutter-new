class User {
  final String token;
  final String message;
  final bool isSuccess;
  final String? errors;
  final String expireDate;
  User(
      {required this.token,
      required this.message,
      required this.isSuccess,
      this.errors,
      required this.expireDate});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      token: json['token'].toString(),
      message: json['message'].toString(),
      isSuccess: json['isSuccess'],
      errors: json['errors'].toString(),
      expireDate: json['expireDate'].toString(),
    );
  }
}

// class User {
//   final String token;
//   final String message;
//   final bool isSuccess;
//   final String? errors;
//   final String expireDate;
//
//   User({required this.token, required this.message,required this.isSuccess, this.errors, required this.expireDate});
//
//   factory User.fromJson(Map<String, dynamic> json) {
//     return User(
//       token: json['token'],
//       message: json['message'],
//       isSuccess: json['isSuccess'],
//       errors:json['errors'],
//       expireDate:json['expireDate'],
//
//     );
//   }
//
// }
