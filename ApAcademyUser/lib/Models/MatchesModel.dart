class MatchModel {
  final int id;
  final String match;
  final String coach;
  final String venue;
  final String time;
  final String team1;
  final String team2;

  const MatchModel({required this.id,required this.match, required this.coach, required this.venue, required this.time,required this.team1,required this.team2});
}
