import 'package:flutter/material.dart';

class myResources {
  static Color themeColor = const Color(0xff00be8e);
  static Color semiColor = const Color(0xff009cbd);
  static Color background = const Color(0xfff1f2f6);
  static TextStyle appTextStyle = TextStyle(
    fontSize: 14,
    color: Colors.black,
    // fontWeight: FontWeight.bold,
  );
  static TextStyle titleTextStyle = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.bold,
    color: Colors.black,
  );
  static TextStyle appHeadingStyle = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.bold,
  );
  static TextStyle timestampStyle = TextStyle(
    fontSize: 12,
    color: Colors.black54,
  );

  static TextStyle modelHeadingStyle = TextStyle(
    fontSize: 20,
    color: Colors.white,
    fontWeight: FontWeight.w700,
  );

  static TextStyle linkStyle = TextStyle(fontSize: 14, color: Colors.blue);

  static TextStyle textStyleprofile = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w500,
  );
  static TextStyle myTextStyle = TextStyle(
    fontSize: 16,
    // fontWeight: FontWeight.w700,
  );
}
