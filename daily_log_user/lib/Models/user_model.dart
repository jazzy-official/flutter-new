class UserModel {
  final String? id;
  final String name;
  final String uid;
  final String email;
  final String? imgUrl;
  final String role;

  UserModel({this.id,required this.name, required this.email,this.imgUrl,required this.uid,required this.role,});



  factory UserModel.fromJson(dynamic json, key) {
    return UserModel(
      id: key.toString(),
      name : json['name'],
      email : json['email'],
      role : json['role'],
      uid : json['uid'],
      imgUrl: json['imgUrl'] ?? [''],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = name;
    data['email'] = email;
    data['imgUrl'] = imgUrl??"";
    data['role'] = role;
    data['uid'] = uid;
    return data;
  }
}