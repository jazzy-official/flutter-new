class Unit {
  int sold;
  int available;
  int reserved;
  int all;

  Unit({
    required this.sold,
    required this.available,
    required this.reserved,
    required this.all,
  });

  factory Unit.fromJson(Map<String, dynamic> json) {
    return Unit(
      sold: json['Sold'] as int,
      available: json['Available'] as int,
      reserved: json['Reserved'] as int,
      all: json['All'] as int,
    );
  }
}
