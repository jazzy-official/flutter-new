import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ap_academy_new/Constants/util.dart';
import 'package:intl/intl.dart';

class TeamsDetails extends StatelessWidget {
  final Map<String, dynamic> team1Model;
  final Map<String, dynamic> team2Model;
  final Map<String, dynamic> matchModel;

  const TeamsDetails({Key? key, required this.team1Model, required this.team2Model, required this.matchModel})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: myResources.buttonColor,
        title: Text("Details"),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SizedBox(
            height: Get.height / 1.1,
            //create the cards for all details of event happening
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Card(
                    color: myResources.backgroundColor,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            height: 40,
                            color: myResources.buttonColor.withOpacity(0.9),
                            child: Center(
                              child: Text("MATCH DETAILS",
                                  style: myResources.modelHeadingStyle),
                            ),
                          ),
                        ),

                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height: 30,
                                    child: Text("Match:",
                                        style: myResources.appTextStyle),
                                  ),
                                  SizedBox(
                                      height: 30,
                                      child: Text("Time:",
                                          style: myResources.appTextStyle)),
                                  SizedBox(
                                      height: 30,
                                      child: Text("Venue:",
                                          style: myResources.appTextStyle)),
                                  SizedBox(
                                      height: 30,
                                      child: Text("Coach:",
                                          style: myResources.appTextStyle)),
                                  SizedBox(
                                      height: 30,
                                      child: Text("Note:",
                                          style: myResources.appTextStyle)),
                                ],
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height: 30,
                                    width: 120,
                                    child: Text(
                                      matchModel['match'],
                                      style: const TextStyle(
                                          color: myResources.headingColor),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 120,
                                    height: 30,
                                    child: Text(
                                      DateFormat.yMEd()
                                          .add_jm()
                                          .format(matchModel['matchTime']),
                                      overflow: TextOverflow.ellipsis,
                                      style: const TextStyle(
                                          color: myResources.headingColor),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 120,
                                    height: 30,
                                    child: Text(
                                      matchModel['venue'],
                                      style: const TextStyle(
                                          color: myResources.headingColor),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 120,
                                    height: 30,
                                    child: Text(
                                      matchModel['coach'],
                                      style: TextStyle(
                                          color: myResources.coachHeading),
                                    ),
                                  ),
                                  Container(
                                    width: 120,
                                    constraints: const BoxConstraints(minHeight: 30),
                                    child: Text(
                                      matchModel['note'],
                                      style: TextStyle(
                                          color: myResources.coachHeading),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),

                        const SizedBox(
                          height: 10,
                        ),
                        //members
                      ],
                    ),
                  ),
                  Card(
                    color: myResources.backgroundColor,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            height: 40,
                            color: myResources.buttonColor.withOpacity(0.9),
                            child: Center(
                              child: Text("Team 1",
                                  style: myResources.modelHeadingStyle),
                            ),
                          ),
                        ),

                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Team :",
                                      style: myResources.appTextStyle),
                                  Text("Type:", style: myResources.appTextStyle),
                                  Text("Coach:", style: myResources.appTextStyle),
                                ],
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    team1Model['teamName'].toString(),
                                    style: const TextStyle(
                                        color: myResources.headingColor),
                                  ),
                                  Text(
                                    team1Model['teamType'],
                                    style: const TextStyle(
                                        color: myResources.headingColor),
                                  ),
                                  Text(
                                    team1Model['coach'],
                                    style: TextStyle(
                                        color: myResources.coachHeading),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        //members
                        Center(
                          child: Text(
                            "Members",
                            style: myResources.appHeadingStyle,
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Column(
                          children: [
                            Container(
                                width: Get.width * 0.8,
                                decoration: BoxDecoration(
                                    borderRadius: const BorderRadius.only(
                                        topLeft: Radius.circular(10),
                                        topRight: Radius.circular(10)),
                                    color: myResources.buttonColor
                                        .withOpacity(0.7)),
                                height: 40,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: const [
                                    SizedBox(
                                      width: 25,
                                      child: Text(
                                        'Sr#',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 100,
                                      child: Text(
                                        'name     ',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 120,
                                      child: Text('email      ',
                                          style:
                                              TextStyle(color: Colors.white)),
                                    )
                                  ],
                                )),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 20.0),
                          child: Container(
                            height: 100,
                            constraints: const BoxConstraints(minHeight: 10),
                            width: Get.width * 0.8,
                            child: ListView.builder(
                                itemCount: team1Model['teamMembers'].length,
                                itemBuilder: (BuildContext context, int index) {
                                  return getTeamMembersCard(
                                      team1Model['teamMembers'][index],
                                      index + 1);
                                }),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Card(
                    color: myResources.backgroundColor,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            height: 40,
                            color: myResources.buttonColor.withOpacity(0.9),
                            child: Center(
                              child: Text("Team 2",
                                  style: myResources.modelHeadingStyle),
                            ),
                          ),
                        ),

                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Team :",
                                      style: myResources.appTextStyle),
                                  Text("Type:", style: myResources.appTextStyle),
                                  Text("Coach:", style: myResources.appTextStyle),
                                ],
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    team2Model['teamName'],
                                    style: const TextStyle(
                                        color: myResources.headingColor),
                                  ),
                                  Text(
                                    team2Model['teamType'],
                                    style: const TextStyle(
                                        color: myResources.headingColor),
                                  ),
                                  Text(
                                    team2Model['coach'],
                                    style: TextStyle(
                                        color: myResources.coachHeading),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            top: 10,
                            right: 100,
                            left: 10,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: const [],
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        //members
                        Center(
                          child: Text(
                            "Members",
                            style: myResources.appHeadingStyle,
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Column(
                          children: [
                            const SizedBox(),
                            Container(
                                width: Get.width * 0.8,
                                decoration: BoxDecoration(
                                  borderRadius: const BorderRadius.only(
                                      topLeft: Radius.circular(10),
                                      topRight: Radius.circular(10)),
                                  color:
                                      myResources.buttonColor.withOpacity(0.7),
                                ),
                                height: 40,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: const [
                                    SizedBox(
                                      width: 25,
                                      child: Text(
                                        'Sr#',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 100,
                                      child: Text(
                                        'name',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 120,
                                      child: Text('email',
                                          style:
                                              TextStyle(color: Colors.white)),
                                    )
                                  ],
                                )),
                          ],
                        ),
                        Container(
                          height: 100,
                          constraints: const BoxConstraints(minHeight: 10),
                          width: Get.width * 0.8,
                          child: ListView.builder(
                              itemCount: team2Model['teamMembers'].length,
                              itemBuilder: (BuildContext context, int index) {
                                return getTeamMembersCard(
                                    team2Model['teamMembers'][index],
                                    index + 1);
                              }),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget getTeamMembersCard(Map<String, dynamic> memeberslist, int i) {
    return Column(
      children: [
        Container(
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: myResources.buttonColor,
                    offset: const Offset(0, 1),
                  ),
                ],
                borderRadius: BorderRadius.circular(5),
                color: Colors.white.withOpacity(1)),
            height: 40,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SizedBox(width: 20, child: Text(i.toString())),
                SizedBox(
                    width: 100,
                    child: Text(
                      memeberslist['name'],
                      overflow: TextOverflow.ellipsis,
                    )),
                SizedBox(
                    width: 120,
                    child: Text(
                      memeberslist['email'],
                      overflow: TextOverflow.ellipsis,
                    ))
              ],
            )),
        const SizedBox(
          height: 5,
        )
      ],
    );
  }
}
