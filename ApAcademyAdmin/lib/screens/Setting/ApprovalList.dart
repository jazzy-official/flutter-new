import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:ap_academy_admin/ModelView/Controller/GroupController.dart';
import 'package:ap_academy_admin/ModelView/Controller/dashboardController.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:ap_academy_admin/screens/drawer_sreen.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

class ApprovalListPage extends StatefulWidget {
  final bool? isAppBar;

  const ApprovalListPage({Key? key, this.isAppBar}) : super(key: key);
  @override
  _ApprovalListPageState createState() => _ApprovalListPageState();
}

class _ApprovalListPageState extends State<ApprovalListPage> {
  final dashBoardController = Get.put(DashBoardController());

  @override
  void initState() {
    refrehApprovals();
    // TODO: implement initState
    super.initState();
  }

  refrehApprovals() {
    dashBoardController.pendingRefresh();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: EdgeInsets.all(8),
          child: GetX<DashBoardController>(
            init: Get.put<DashBoardController>(DashBoardController()),
            builder: (DashBoardController dashController) {
              if (dashController != null &&
                  dashController.pendingApproval != null) {
                if (dashController.pendingApproval.length > 0) {
                  return ListView.builder(
                      scrollDirection: Axis.vertical,
                      itemCount: dashController.pendingApproval.length,
                      itemBuilder: (BuildContext context, int index) {
                        return _pendingApproval(
                            dashController.pendingApproval[index]);
                      });
                } else {
                  return Align(
                    alignment: Alignment.center,
                    child: Text(
                      "No Pending Approvals",
                      style: TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.w500,
                          color: Colors.green.shade600),
                    ),
                  );
                }
              } else {
                return Container(
                  child: LoadingWidget(),
                );
              }
            },
          ),
        ),
      ),
    );
  }

  String selectedValue = '';
  Widget _pendingApproval(Map<String, dynamic> approvel) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: Card(
        elevation: 20,
        shadowColor: Colors.black,
        child: Padding(
          padding: const EdgeInsets.only(top: 8, left: 8, right: 8),
          child: Container(
              height: Get.height * 0.35,
              //constraints: BoxConstraints(minHeight: 250),
              //color: MyResources.primaryColor,
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      CircleAvatar(
                        radius: Get.width * 0.1,
                        backgroundImage:
                            NetworkImage(approvel['photoUrl'].toString()),
                        backgroundColor: Colors.transparent,
                      ),
                      SizedBox(width: Get.width * 0.45),
                      Ink(
                        child: IconButton(
                          icon: Icon(CupertinoIcons.book),
                          iconSize: Get.width * 0.15,
                          color: Colors.black54,
                          onPressed: () {
                            if (approvel['documents'] != null &&
                                approvel['documents'].length > 0)
                              Get.to(PhotoViewGallery.builder(
                                scrollPhysics: const BouncingScrollPhysics(),
                                builder: (BuildContext context, int index) {
                                  return PhotoViewGalleryPageOptions(
                                    imageProvider: NetworkImage(
                                        approvel['documents'][index]
                                            .toString()),
                                    initialScale:
                                        PhotoViewComputedScale.contained,
                                    // heroAttributes: PhotoViewHeroAttributes(tag: approvel['documents'][index]),
                                  );
                                },
                                itemCount: approvel['documents'].length,
                                loadingBuilder: (context, event) => Center(
                                  child: Container(
                                    width: 20.0,
                                    height: 20.0,
                                    child: CircularProgressIndicator(
                                      value: event == null
                                          ? 0
                                          : event.cumulativeBytesLoaded /
                                              event.expectedTotalBytes!.toInt(),
                                    ),
                                  ),
                                ),
                                // backgroundDecoration: widget.backgroundDecoration,
                                // pageController: widget.pageController,
                                // onPageChanged: onPageChanged,
                              ));
                          },
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    //mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        //mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text(
                              "Name :",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text(
                              "Email :",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text(
                              "D.O.B :",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text(
                              "Age :",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text(
                              "Phone :",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text(
                              approvel['name'].toString(),
                              style: MyResources.appTextStyle,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text(
                              approvel['email'].toString(),
                              style: MyResources.appTextStyle,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text(
                              approvel['dob'] != null
                                  ? DateFormat('yyyy-MM-dd')
                                      .format(approvel['dob'])
                                  : "Not Defined",
                              style: MyResources.appTextStyle,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text(
                              approvel['dob'] != null
                                  ? (getAge(approvel['dob']).inDays ~/ 365)
                                      .toInt()
                                      .toString()
                                  : "Not Defined",
                              style: MyResources.appTextStyle,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text(
                              approvel['phone'].toString(),
                              style: MyResources.appTextStyle,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  // Padding(
                  //     //Add padding around textfield
                  //     padding: EdgeInsets.only(top: 7.0, right: 20, left: 20),
                  //     child: Container(
                  //       decoration: MyResources.roundedBoxDecore,
                  //       //  color: MyResources.backgroundColor,
                  //       // width: 350,
                  //       height: 50,
                  //       child: Padding(
                  //         padding: const EdgeInsets.all(12.0),
                  //         child: Container(
                  //           width: Get.width,
                  //  child:

                  //  GetX<GroupsControler>(
                  //   init: Get.put<GroupsControler>(GroupsControler()),
                  //   builder: (GroupsControler groupController) {
                  //     if (groupController != null) {
                  //       if (groupController.groupName.length != 0) {
                  //         if (selectedValue == '')
                  //           selectedValue = groupController
                  //               .groupName[0]['name']
                  //               .toString();
                  //         return DropdownButton<String>(
                  //           underline: Text(""),
                  //           isExpanded: true,
                  //           value: selectedValue,
                  //           focusColor: MyResources.buttonColor,
                  //           elevation: 12,
                  //           items:
                  //               groupController.groupName.map((map) {
                  //             return DropdownMenuItem(
                  //               child: Text(
                  //                 map['name'].toString(),
                  //                 style: TextStyle(fontSize: 12),
                  //               ),
                  //               value: map['name'].toString(),
                  //             );
                  //           }).toList(),
                  //           onChanged: (String value) {
                  //             print(value);
                  //             selectedValue = value;
                  //             setState(() {});
                  //           },
                  //           hint: Text('Select Item'),
                  //         );
                  //       } else
                  //         return Container();
                  //     } else
                  //       return Container();
                  //   },
                  // ),
                  //         ),
                  //       ),
                  //     )),
                  // Container(
                  //   child: approvel['documents'] != null
                  //       ? Padding(
                  //           padding: const EdgeInsets.all(15.0),
                  //           child: Container(
                  //             height: 40,
                  //             child: GridView.count(
                  //               scrollDirection: Axis.horizontal,
                  //               crossAxisCount: 1,
                  //               shrinkWrap: true,
                  //               children: List.generate(
                  //                   approvel['documents'].length, (index) {
                  //                 return InkWell(
                  //                   onTap: () {
                  //                     Get.to(PhotViewer(
                  //                       imageString: approvel['documents']
                  //                               [index]
                  //                           .toString(),
                  //                     ));
                  //                   },
                  //                   child: Padding(
                  //                     padding: const EdgeInsets.all(8.0),
                  //                     child: Image.network(
                  //                       approvel['documents'][index].toString(),
                  //                       fit: BoxFit.cover,
                  //                       width: Get.width,
                  //                       height: 20,
                  //                     ),
                  //                   ),
                  //                 );
                  //               }),
                  //             ),
                  //           ),
                  //         )
                  //       : Container(),
                  // ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      IconButton(
                        onPressed: () async {
                          // if (selectedValue != '') {
                          bool isApproved = await dashBoardController
                              .approveUser(approvel['_id'],);
                          if (isApproved) {
                            Get.defaultDialog(
                                title: "Approved",
                                content: Text("User is approved by admin"),
                                barrierDismissible: false,
                                confirmTextColor: Colors.white,
                                buttonColor: MyResources.buttonColor,
                                onConfirm: () {
                                  dashBoardController.pendingRefresh();
                                  Get.back();
                                  if (mounted) setState(() {});
                                  // Get.offAll(HomeDrawer());
                                });
                          } else {
                            Get.defaultDialog(
                                title: "Error",
                                content: Text("User is not approved "));
                          }
                          // } else {
                          //   Get.snackbar(
                          //       "Approval error", "Please select group First",
                          //       snackPosition: SnackPosition.BOTTOM);
                          // }
                        },
                        icon: Icon(
                          Icons.check_circle_outlined,
                          color: Colors.green,
                        ),
                      ),
                      IconButton(
                        onPressed: () {},
                        icon: Icon(
                          Icons.cancel_outlined,
                          color: Colors.red,
                        ),
                      ),
                    ],
                  ),
                ],
              )),
        ),
      ),
    );
  }

  Duration getAge(DateTime dob) {
    DateTime today = DateTime.now();
    return today.difference(dob);
  }
}

class PhotViewer extends StatefulWidget {
  final imageString;

  const PhotViewer({Key? key, this.imageString}) : super(key: key);
  @override
  _PhotViewerState createState() => _PhotViewerState();
}

class _PhotViewerState extends State<PhotViewer> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          child: PhotoView(
        imageProvider: NetworkImage(widget.imageString),
      )),
    );
  }
}
