import 'package:ap_academy_admin/constants/util.dart';
import 'package:ap_academy_admin/screens/shared/DeletPop.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../ModelView/Controller/BankController.dart';
import './NewBank.dart';

class BankList extends StatefulWidget {
  @override
  _BankListState createState() => _BankListState();
}

class _BankListState extends State<BankList> {
  final bankController = Get.put(BanksController());
  @override
  void initState() {
    bankController.refreshBankData();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      //Create an event add button
      child: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "",
                    style: MyResources.appHeadingStyle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    width: Get.width * 0.2,
                    height: 25,
                    child: RaisedButton(
                      textColor: Colors.white,
                      color: MyResources.buttonColor,
                      child: Text(
                        "ADD",
                      ),
                      onPressed: () async {
                        await Get.to(AddBanks())!.then((value) {
                          bankController.refreshBankData();
                        });
                      },
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Container(
              height: Get.height,
              child: GetX<BanksController>(
                init: Get.put<BanksController>(BanksController()),
                builder: (BanksController banksController) {
                  if (banksController != null &&
                      banksController.banks != null) {
                    return Container(
                      height: Get.height,
                      child: Stack(
                        children: [
                          ListView.builder(
                            itemCount: banksController.banks.length,
                            itemBuilder: (BuildContext context, int index) {
                              return getBankCard(banksController.banks[index]);
                            },
                          ),
                        ],
                      ),
                    );
                  } else {
                    return LoadingWidget();
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  getBankCard(Map<String, dynamic> bankModel) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                flex: 6,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Bank Name:  ",
                          style: MyResources.titleTextStyle,
                        ),
                        Container(
                            width: 150,
                            child: Text(bankModel['bankName'],
                                style:
                                    TextStyle(color: MyResources.buttonColor))),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Account Name:  ",
                          style: MyResources.titleTextStyle,
                        ),
                        Container(
                            width: 150,
                            child: Text(bankModel['accName'],
                                style:
                                    TextStyle(color: MyResources.buttonColor))),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Account Number:  ",
                          style: MyResources.titleTextStyle,
                        ),
                        Container(
                            width: 150,
                            child: Text(bankModel['BankAcc'],
                                style:
                                    TextStyle(color: MyResources.buttonColor))),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "FPS ID:  ",
                          style: MyResources.titleTextStyle,
                        ),
                        Container(
                            width: 150,
                            child: Text(bankModel['fpsid'],
                                style:
                                    TextStyle(color: MyResources.buttonColor))),
                      ],
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 1,
                child: IconButton(
                    icon: Icon(Icons.delete_forever_outlined, color: Colors.red),
                    onPressed: ()  {
                      Get.dialog(Center(
                          child: DeletpopUp(
                        message: 'Are you sure you want to delete?',
                        pressFunc: () async {
                          await bankController.delBank(bankModel['_id']);
                          bankController.refreshBankData();
                          Get.back();
                        },
                      )));
                    }
                    ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
