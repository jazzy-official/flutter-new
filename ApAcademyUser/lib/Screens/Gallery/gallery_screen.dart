import 'package:ap_academy_new/ModelView/controllers/AlbumController.dart';
import 'package:ap_academy_new/screens/Gallery/gallery_album.dart';
import 'package:ap_academy_new/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ap_academy_new/Constants/util.dart';
import 'package:intl/intl.dart';

class GalleryScreen extends StatefulWidget {
  GalleryScreen({Key? key, this.title}) : super(key: key);
  final String? title;
  @override
  _GalleryScreenState createState() => _GalleryScreenState();
}

class _GalleryScreenState extends State<GalleryScreen> {
  final galeyController = Get.put(PhotoAlbumController());
  int page = 1;
  bool isLoading = false;
  //List<String> items = ['item 1', 'item 2', ];
  Future _loadData() async {
    // perform fetching data delay
    await new Future.delayed(new Duration(seconds: 2));
    print("load more");
    await galeyController.refreshData();
    // update data and loading status
    setState(() {
      /*items.addAll( ['item 1']);
      print('items: '+ items.toString());*/
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification scrollInfo) {
                if (!isLoading &&
                    scrollInfo.metrics.pixels ==
                        scrollInfo.metrics.maxScrollExtent) {
                  _loadData();
                  // start loading data
                  setState(() {
                    isLoading = true;
                  });
                }
                return isLoading;
              },
              child: SingleChildScrollView(
                child: Container(
                    constraints: BoxConstraints(maxHeight: Get.height * 0.9),
                    // height: Get.height * 0.9,
                    child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child:
                            OrientationBuilder(builder: (context, orientation) {
                          return Obx(() {
                            if (galeyController.album != null &&
                                galeyController.album.isNotEmpty) {
                              return GridView.count(
                                  shrinkWrap: true,
                                  keyboardDismissBehavior:
                                      ScrollViewKeyboardDismissBehavior.onDrag,
                                  // Create a grid with 2 columns. If you change the scrollDirection to
                                  // horizontal, this produces 2 rows.
                                  crossAxisCount:
                                      orientation == Orientation.portrait
                                          ? 1
                                          : 2,
                                  // Generate 100 widgets that display their index in the List.
                                  children: List.generate(
                                      galeyController.album.length, (index) {
                                    return InkWell(
                                      onTap: () {
                                        Get.to(
                                          GalleryAlbum(i: index),
                                        );
                                      },
                                      child: Container(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            galeyController.album[index]
                                                            ['albumPhotos']
                                                        .toList()
                                                        .length !=
                                                    0
                                                ? Container(
                                              decoration: BoxDecoration(
                                                border: Border.all(color: Colors.black38)
                                              ),
                                                  child: Image.network(
                                                      galeyController.album[index]
                                                          ['albumPhotos'][0],
                                                      width: Get.width,
                                                      height: 200,
                                                      fit: BoxFit.cover,
                                                    ),
                                                )
                                                : Container(
                                                    child: Center(
                                                      child: Icon(
                                                        Icons.image_outlined,
                                                        size: 200,
                                                      ),
                                                    ),
                                                  ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    constraints: BoxConstraints(
                                                        minWidth: 100,
                                                        maxWidth: 100),
                                                    child: Container(
                                                      child: Text(
                                                        galeyController
                                                                .album[index]
                                                            ['albumName'],
                                                        textAlign:
                                                            TextAlign.left,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 16,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height: 20,
                                                  ),
                                                  Text(
                                                    'Total Pictures : ${galeyController.album[index]['albumPhotos'].length}',
                                                    style: TextStyle(
                                                        color: Colors.grey),
                                                  ),
                                                  Text(
                                                    'Created at ${DateFormat.yMEd().format(galeyController.album[index]['CreatedAt'])}',
                                                    style: TextStyle(
                                                        color: Colors.grey),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  }));
                            } else
                              return Container();
                          });
                        }))),
              ),
            ),
          ),
          Container(
            height: isLoading ? 50.0 : 0,
            color: Colors.transparent,
            child: Center(
              child: new LoadingWidget(),
            ),
          ),
        ],
      ),
    );
  }
}
