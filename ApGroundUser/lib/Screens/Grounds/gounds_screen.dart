
import 'package:ap_booking_user/Screens/Shared/loading_widget.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'Models/ground_models.dart';
import 'ground_detail.dart';

class GroundScreen extends StatefulWidget {
  const GroundScreen({Key? key}) : super(key: key);

  @override
  State<GroundScreen> createState() => _GroundScreenState();
}

class _GroundScreenState extends State<GroundScreen> {
  final DatabaseReference databaseReference =
  FirebaseDatabase.instance.reference().child("Grounds");

  Future<List<Ground>> readData() async {
    List<Ground> grounds = [];
    await databaseReference.once().then((DataSnapshot snapshot) {
      snapshot.value.forEach((key, value) {
        Ground gr = Ground.fromJson(value,key);
        grounds.add(gr);
      });
    });
    return grounds;
  }

  late List<Ground> grounds;
  bool isLoading = true;
  void refresh() async {
    setState(() {
      isLoading = true;
    });
    grounds = await readData();
    print(grounds[0].schedule[0].slots[0].Pslot1);
    setState(() {
      isLoading = false;
    });
  }


  @override
  void initState() {
    refresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Expanded(
            child: isLoading
                ? LoadingWidget()
                : ListView.builder(
                padding: const EdgeInsets.all(8),
                itemCount: grounds.length,
                itemBuilder: (BuildContext context, int index) {
                  Ground ground = grounds[index];
                  return InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GroundDetail(
                              ground: ground,
                            )),
                      ).then((value) => refresh());
                    },
                    child:Container(
                      color: Colors.white,
                      child: Card(
                        child: Column(
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: CircleAvatar(
                                    backgroundImage: ground.imgUrls[0] == "" ? AssetImage('assets/stadiumlogo.png') as ImageProvider :
                                    NetworkImage(ground.imgUrls[0]),
                                    radius: 30,
                                  ),
                                ),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          ground.name.toUpperCase(),
                                          style: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.w600),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(5.0),
                                        child: Text(ground.address),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                  const EdgeInsets.only(top: 8.0, right: 8),
                                  child: Text("Slots : 2"),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),



                    );


                }),
          ),
        ],
      ),
    );
  }
}