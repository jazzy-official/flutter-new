import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Balance%20Sheet/Modals/entity_model.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Recovery/Modal/installment_model.dart';

class BalanceSheet {
  List<Entity> currentAsset;
  List<Entity> fixedAsset;
  List<Entity> currentLiabilities;
  List<Entity> longTermLiabilities;
  List<Entity> drawingList;
  List<Entity> capitalList;
  List<Entity> otherEquity;
  double dutiesAndTaxesPositiveSum;
  double dutiesAndTaxesNegativeSum;
  double currentAssetSum;
  double fixedAssetSum;
  double currentLiabilitiesSum;
  double longTermLiabilitiesSum;
  double drawingListSum;
  double capitalListSum;
  double otherEquitySum;
  double retainedEarning;

  BalanceSheet({
    required this.capitalList,
    required this.capitalListSum,
    required this.currentAsset,
    required this.currentAssetSum,
    required this.currentLiabilities,
    required this.currentLiabilitiesSum,
    required this.drawingList,
    required this.drawingListSum,
    required this.dutiesAndTaxesNegativeSum,
    required this.dutiesAndTaxesPositiveSum,
    required this.fixedAsset,
    required this.fixedAssetSum,
    required this.longTermLiabilities,
    required this.longTermLiabilitiesSum,
    required this.otherEquity,
    required this.otherEquitySum,
    required this.retainedEarning,
  });

  factory BalanceSheet.fromJson(Map<String, dynamic> json) {
    return BalanceSheet(
      currentAsset: List<Entity>.from(json["CurrentAssets"].map((dynamic item) => Entity.fromJson(item),).toList()),
      longTermLiabilities: List<Entity>.from(json["LongTermLiabilities"].map((dynamic item) => Entity.fromJson(item),).toList()),
      otherEquity:List<Entity>.from(json["OtherEquityList"].map((dynamic item) => Entity.fromJson(item),).toList()),
      capitalList: List<Entity>.from(json["CapitalList"].map((dynamic item) => Entity.fromJson(item),).toList()),
      currentLiabilities: List<Entity>.from(json["CurrentLiabilities"].map((dynamic item) => Entity.fromJson(item),).toList()),
      drawingList: List<Entity>.from(json["DrawingList"].map((dynamic item) => Entity.fromJson(item),).toList()),
      fixedAsset:  List<Entity>.from(json["FixedAssets"].map((dynamic item) => Entity.fromJson(item),).toList()),
      dutiesAndTaxesNegativeSum: json['DutiesAndTaxesNegativeSum'],
      retainedEarning: json['RetainedEarning'],
      capitalListSum: json['CapitalListSum'],
      otherEquitySum: json['OtherEquitiesListSum'],
      currentAssetSum:  json['CurrentAssetsSum'],
      currentLiabilitiesSum: json['CurrentLiabilitiesSum'],
      fixedAssetSum: json['FixedAssetSum'],
      dutiesAndTaxesPositiveSum: json['DutiesAndTaxesPositiveSum'],
      longTermLiabilitiesSum:  json['LongTermLiabilitiesSum'],
      drawingListSum: json['DrawingListSum'],
    );
  }
}
