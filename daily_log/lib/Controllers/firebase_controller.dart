import 'dart:core';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';

class FirebaseController extends GetxController {



  @override
  Future<void> onInit() async {
    super.onInit();
  }



  final _auth = FirebaseAuth.instance;

  Future<User?> signup (String e, String p) async {
    try {
      final newUser = await _auth.createUserWithEmailAndPassword(email: e, password: p,);
      return newUser.user;
    }
    catch (e)
    {
      Get.snackbar("alert", e.toString());
    }
  }


}
