import 'dart:core';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:room_buddy/Models/about_user_model.dart';
import 'package:room_buddy/Models/habits_modal.dart';
import 'package:room_buddy/Models/location_model.dart';
import 'package:room_buddy/Models/room_model.dart';
import 'package:room_buddy/Models/user_model.dart';

class ExplorerController extends GetxController {
  List<Room> roomsdata = [
    Room(
      id: 1,
        ownerId: 1,
        address: "phase 2, street 3, p-4876",
        location: LatLng(74.94994, 35.6363698),
        images: ["assets/room1.jpg","assets/room2.jpg"],
        city: "Faisalabad",
        locality: "DHA",
        datePosted: DateTime(2022,),
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque efficitur posuere. Curabitur tincidunt placerat diam ac efficitur. Cras rutrum egestas nisl vitae pulvinar. Donec id mollis diam, id hendrerit neque. Donec accumsan efficitur libero, vitae feugiat odio fringilla ac. Aliquam a turpis bibendum, varius erat dictum, feugiat libero. Nam et dignissim nibh. Morbi elementum varius elit, at dignissim ex accumsan a",
        rent: 18000,
        securityDeposit: 25000,
        billIncluded: true,
        furnishingType: "Fully furnished",
        bedrooms: 2,
        bedroomType: "Private",
        bathroom: 3,
        bathroomType: "Attach",
        roomsize: 150,
        propertySize: 2500,
        availability: "Immediately",
        minStay: "1 year",
        amenities: ["Security","AC","WIFI"],
        ownerName: "Muhammad Jahanzaib",
        aboutOwner: AboutUser(
          "Male",
          RangeValues(22,0),
          "Professional",
          "Single",
          Habits("Daily", "Non-Veg", "Occasionally", "Occasionally", "Have"),
          [
            "Swimming",
            "Football",
            "Travel",
            "Food",
          ],
          [
            "Calm",
            "Active",
            "Fun",
            "Organized",
          ],
        ),
        roommatePreferences: AboutUser(
          "Male",
          RangeValues(18,30),
          "Professional",
          "Single",
          Habits("Daily", "Non-Veg", "Occasionally", "Occasionally", "Have"),
          [
            "Swimming",
            "Football",
            "Travel",
            "Food",
          ],
          [
            "Calm",
            "Active",
            "Fun",
            "Organized",
          ],
        ), ownerDp: 'assets/me.jpg',
    ),
    Room(
      ownerId: 3,
      id: 2,
        address: "phase 2, street 3, p-4876",
        location: LatLng(74.94994,35.6363698),
        images: ["assets/room3.jpg","assets/room4.jpg"],
        city: "Faisalabad",
        locality: "DHA",
        datePosted: DateTime(2022,),
        description: "A long long long long long paragraph",
        rent: 18000,
        securityDeposit: 25000,
        billIncluded: true,
        furnishingType: "Fully furnished",
        bedrooms: 2,
        bedroomType: "Private",
        bathroom: 3,
        bathroomType: "Attach",
        roomsize: 150,
        propertySize: 2500,
        availability: "Immediately",
        minStay: "1 year",
        amenities: ["Security","AC","WIFI","Cooktop","TV","Cleaner"],
        ownerName: "Muhammad Jahanzaib",
        aboutOwner: AboutUser(
          "Male",
          RangeValues(22,0),
          "Professional",
          "Single",
          Habits("Daily", "Non-Veg", "Occasionally", "Occasionally", "Have"),
          [
            "Swimming",
            "Football",
            "Travel",
            "Food",
          ],
          [
            "Calm",
            "Active",
            "Fun",
            "Organized",
          ],
        ),
        roommatePreferences: AboutUser(
          "Male",
          RangeValues(18,30),
          "Professional",
          "Single",
          Habits("Daily", "Non-Veg", "Occasionally", "Occasionally", "Have"),
          [
            "Swimming",
            "Football",
            "Travel",
            "Food",
          ],
          [
            "Calm",
            "Active",
            "Fun",
            "Organized",
          ],
        ), ownerDp: 'assets/me.jpg',
    ),
    Room(
      id: 3,
        ownerId: 4,
        address: "phase 2, street 3, p-4876",
        location: LatLng(74.94994,35.6363698),
        images: ["assets/room5.jpg","assets/room6.jpg"],
        city: "Faisalabad",
        locality: "DHA",
        datePosted: DateTime(2022,),
        description: "A long long long long long paragraph",
        rent: 18000,
        securityDeposit: 25000,
        billIncluded: true,
        furnishingType: "Fully furnished",
        bedrooms: 2,
        bedroomType: "Private",
        bathroom: 3,
        bathroomType: "Attach",
        roomsize: 150,
        propertySize: 2500,
        availability: "Immediately",
        minStay: "1 year",
        amenities: ["Security","AC","WIFI"],
        ownerName: "Muhammad Jahanzaib",
        aboutOwner: AboutUser(
          "Male",
          RangeValues(22,0),
          "Professional",
          "Single",
          Habits("Daily", "Non-Veg", "Occasionally", "Occasionally", "Have"),
          [
            "Swimming",
            "Football",
            "Travel",
            "Food",
          ],
          [
            "Calm",
            "Active",
            "Fun",
            "Organized",
          ],
        ),
        roommatePreferences: AboutUser(
          "Male",
          RangeValues(18,30),
          "Professional",
          "Single",
          Habits("Daily", "Non-Veg", "Occasionally", "Occasionally", "Have"),
          [
            "Swimming",
            "Football",
            "Travel",
            "Food",
          ],
          [
            "Calm",
            "Active",
            "Fun",
            "Organized",
          ],
        ), ownerDp: 'assets/me.jpg',
    ),
    Room(
      id: 4,
        ownerId: 5,
        address: "phase 2, street 3, p-4876",
        location: LatLng(74.94994,35.6363698),
        images: ["assets/room7.jpg","assets/room8.jpg"],
        city: "Faisalabad",
        locality: "DHA",
        datePosted: DateTime(2022,),
        description: "A long long long long long paragraph",
        rent: 18000,
        securityDeposit: 25000,
        billIncluded: true,
        furnishingType: "Fully furnished",
        bedrooms: 2,
        bedroomType: "Private",
        bathroom: 3,
        bathroomType: "Attach",
        roomsize: 150,
        propertySize: 2500,
        availability: "Immediately",
        minStay: "1 year",
        amenities: ["Security","AC","WIFI"],
        ownerName: "Muhammad Jahanzaib",
        aboutOwner: AboutUser(
          "Male",
          RangeValues(22,0),
          "Professional",
          "Single",
          Habits("Daily", "Non-Veg", "Occasionally", "Occasionally", "Have"),
          [
            "Swimming",
            "Football",
            "Travel",
            "Food",
          ],
          [
            "Calm",
            "Active",
            "Fun",
            "Organized",
          ],
        ),
        roommatePreferences: AboutUser(
          "Male",
          RangeValues(18,30),
          "Professional",
          "Single",
          Habits("Daily", "Non-Veg", "Occasionally", "Occasionally", "Have"),
          [
            "Swimming",
            "Football",
            "Travel",
            "Food",
          ],
          [
            "Calm",
            "Active",
            "Fun",
            "Organized",
          ],
        ), ownerDp: 'assets/me.jpg',
    ),
  ];

  List<Room> rooms = [

  ];



  double budget=0;
  String? roomtype;
  int? roomNo;
  String? bathroomType;
  String? roommateGender;
  List<String> amenities=[];

  RxList<Room> foundResult = <Room>[].obs;
  List<Room> result1 = [];
  List<Room> result2 = [];
  List<Room> result3 = [];
  List<Room> result4 = [];
  List<Room> result5 = [];
  List<Room> result6 = [];

  @override
  Future<void> onInit() async {
    fetchrooms();
    super.onInit();
  }

  Future<void> fetchrooms() async {
    await Future.delayed(Duration(seconds: 2));
    rooms = roomsdata;
    foundResult = rooms.obs;
    update();
  }

  void runFilter(){
    budgetfilter();
    roomTypeFilter();
    roomNoFilter();
    bathroomTypeFilter();
    genderFilter();
    amenitiesFilter();

    foundResult = result6.obs;
    update();
  }

  budgetfilter(){
    if(budget==0){
      result1 = rooms;
    }
    else {
      result1 = rooms.where((element) => element.rent < (budget+3000) ||
          element.rent > budget-3000).toList();
      update();
    }
  }

  roomTypeFilter(){
    if(roomtype == null){
      result2 = result1;
    }else{
      result2 = result1.where((element) => element.bedroomType == roomtype ).toList();
    }
  }

  roomNoFilter(){
    if(roomNo==null){
      result3 = result2;
    }else{
      result3 = result2.where((element) => element.bedrooms==roomNo).toList();
    }
  }
  bathroomTypeFilter(){
    if(bathroomType == null){
      result4 = result3;
    }else{
      result4 = result3.where((element) => element.bathroomType == bathroomType ).toList();
    }
  }

  genderFilter(){
    if(roommateGender == null){
      result5 = result4;
    }else{
      result5 = result4.where((element) => element.aboutOwner.gender == roommateGender ).toList();
    }
  }

  amenitiesFilter(){
    if(amenities.isEmpty){
      result6 = result5;
    }else{
      result6 = result5.where((element)  {if (amenities.every((item) => element.amenities.contains(item))) {
          return true;
          } else {
          return false;
          }} ).toList();
    }
  }




}
