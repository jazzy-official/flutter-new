import 'package:ap_ground_admin/Screens/Sponsor/add_sponsor.dart';
import 'package:ap_ground_admin/Screens/Sponsor/models/sponsor_models.dart';
import 'package:ap_ground_admin/Screens/delete_popup.dart';
import 'package:ap_ground_admin/constants/util.dart';
import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:ap_ground_admin/controllers/sponsor_controller.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';

class SponsorScreen extends StatefulWidget {
   SponsorScreen({Key? key}) : super(key: key);

  @override
  State<SponsorScreen> createState() => _SponsorScreenState();
}

class _SponsorScreenState extends State<SponsorScreen> {
  List<Sponsor> sponsorlist=[];

  void refresh()async{
    List<Sponsor> temp=[];
    temp = await SponsorController().readSponsor();
    setState(() {
      sponsorlist = temp;
    });
  }
  @override
  void initState() {
    refresh();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
          Padding(
            padding: const EdgeInsets.only(right: 12.0),
            child: ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddSponsor()),
                ).then((value) => refresh());
              },
              child: const Text("Add"),
              style: ElevatedButton.styleFrom(primary: Color(0xff1758B4)),
            ),
          ),
        ],),

        Expanded(
          child: ListView.builder(
              physics: const BouncingScrollPhysics(
                  parent: AlwaysScrollableScrollPhysics()),
              padding: const EdgeInsets.all(8),
              itemCount: sponsorlist.length,
              itemBuilder: (BuildContext context, int index) {
                Sponsor sponsor = sponsorlist[index];
                return Center(
                  child: Container(
                      height: 500,
                      width: MediaQuery.of(context).size.width*0.96,
                      margin: EdgeInsets.only(bottom: 20),
                      decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.circular(10),
                        image: DecorationImage(
                          colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.5), BlendMode.dstATop),
                          image: NetworkImage(sponsor.imgUrl),
                          fit: BoxFit.cover,
                        ),
                        boxShadow: const [
                          BoxShadow(
                            color: Colors.black26,
                            spreadRadius: 3,
                            blurRadius: 5,
                            offset: Offset(0, 5), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              mainAxisAlignment:MainAxisAlignment.end,
                              children: [
                                PopupMenuButton(
                                    icon: const Icon(Ionicons.ellipsis_vertical,color: Colors.white,),
                                    itemBuilder:(context) => [
                                      PopupMenuItem(
                                        child: TextButton(onPressed: () async{
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(builder: (context) => AddSponsor(sponsor: sponsor,)),
                                          ).then((value) => refresh());
                                        },child: Text("Edit"),),
                                      ),
                                      PopupMenuItem(
                                        child: TextButton(onPressed: () {
                                          showDialog(
                                              context: context,
                                              builder: (_) => AlertDialog(
                                                content: DeletepopUp(message: "Are you sure You wanna delete "+sponsor.name+"?", pressFunc: () async {
                                                  await SponsorController().deleteSponsor(sponsor.id!);
                                                  displayToast(sponsor.name+" Deleted");
                                                  refresh();
                                                },),
                                              )
                                          );
                                          },child: Text("Delete"),),
                                      ),

                                    ]
                                )
                              ],
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(sponsor.name,style: TextStyle(color: Colors.white,fontWeight: FontWeight.w600,fontSize: 18),),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(sponsor.description,style: TextStyle(color: Colors.white,fontSize: 13),),
                                )

                              ],
                            ),
                          ],
                        ),
                      )),
                );
              }),
        ),
      ],
    );
  }
}
