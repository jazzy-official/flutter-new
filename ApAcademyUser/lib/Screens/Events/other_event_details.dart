
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ap_academy_new/Constants/util.dart';
import 'package:intl/intl.dart';

class OtherEventDetails extends StatelessWidget {
  final Map<String, dynamic> otherEvent;

  const OtherEventDetails({Key? key, required this.otherEvent}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 0.0, left: 10, right: 10),
      child: Center(
        child: Container(
          height: Get.height / 1.5,
          child: Card(
            color: myResources.backgroundColor,
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.bottomCenter,
                  child: ColorFiltered(
                      colorFilter: ColorFilter.mode(
                          Colors.white.withOpacity(0.3), BlendMode.dstATop),
                      child: Image.asset(
                        'assets/event.png',
                        colorBlendMode: BlendMode.hue,
                        width: 200,
                        height: 200,
                      )),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        height: 50,
                        color: myResources.buttonColor.withOpacity(0.7),
                        child: Center(
                          child: Text(
                            "Event Details",
                            style: myResources.modelHeadingStyle,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 40,
                        right: 75,
                        left: 10,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 10,
                        right: 10,
                        left: 10,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              height: 50,
                              width: 100,
                              child: Text("Event Name:",
                                  style: myResources.appTextStyle)),
                          Container(
                            height: 50,
                            width: 200,
                            child: Text(
                              otherEvent['eventName'],
                              style: TextStyle(color: myResources.buttonColor),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 10,
                        right: 10,
                        left: 10,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              height: 50,
                              width: 100,
                              child: Text("Venue:",
                                  style: myResources.appTextStyle)),
                          Container(
                            height: 50,
                            width: 200,
                            child: Text(
                              otherEvent['venue'],
                              style: TextStyle(color: myResources.headingColor),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 10, right: 10, left: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text("Time:", style: myResources.appTextStyle),
                          /*SizedBox(
                             width: 30,
                           ),*/
                          Container(
                            width: 200,
                            child: Text(
                              DateFormat.yMEd()
                                  .add_jm()
                                  .format(otherEvent['time']),
                              style: TextStyle(color: myResources.headingColor),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
