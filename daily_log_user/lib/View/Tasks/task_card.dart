import 'package:daily_log_user/Controllers/task_controller.dart';
import 'package:daily_log_user/Models/task_model.dart';
import 'package:daily_log_user/View/Tasks/task_detail.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:intl/intl.dart';

import '../../Controllers/daily_log_controller.dart';
import '../../constants/util.dart';
import '../delete_popup.dart';

class TaskCard extends StatelessWidget {
  TaskModel task;
  TaskCard({Key? key, required this.task}) : super(key: key);
  var log = Get.put(DailyLogController());
  TextEditingController logcontroller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        Get.to(()=> TaskDetailScreen(task: task));
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Card(
          elevation: 6,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Text(
                    task.name.toUpperCase(),
                    style: const TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
                  ),
                ),
                Text(
                  task.des,
                  style: const TextStyle(fontSize: 13),
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Expanded(
                      child: Row(
                        children: [
                          const Text(
                            "Deadline : ",
                            style: TextStyle(fontWeight: FontWeight.w600),
                          ),
                          Text(DateFormat("dd MMM yyyy").format(task.deadline)),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Row(
                        children: [
                          Text(
                            "Status : ",
                            style: TextStyle(fontWeight: FontWeight.w600),
                          ),
                          Text(
                            task.status ? "Completed" : "Pending",
                            style: TextStyle(
                                color: task.status ? Colors.green : Colors.red),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Text(
                      "Project : ",
                      style: TextStyle(fontWeight: FontWeight.w600),
                    ),
                    Text(task.project.toString()),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                if(task.todos!=null)
                Row(
                  children: [
                    Text(
                      "Todos : ",
                      style: TextStyle(fontWeight: FontWeight.w600),
                    ),
                    Text(task.todos!.length.toString()+ " / "),
                    Text(task.todos!.where((element) => element.status==true).toList().length.toString()),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
