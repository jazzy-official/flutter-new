class Ball {
  final String? id;
  final int newball;
  final int oldball;
  final int lostball;


  Ball({required this.newball,  required this.oldball,required this.lostball, this.id});

// NOTE: implementing functionality here in the next step!

  factory Ball.fromJson(dynamic json,String id) {
    return Ball(
      id: id,
      newball : json['newball'],
      oldball : int.parse(json['oldball'].toString()),
      lostball: json['lostball'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['newball'] = this.newball;
    data['oldball'] = this.oldball;
    data['lostball'] = this.lostball;
    return data;
  }
}