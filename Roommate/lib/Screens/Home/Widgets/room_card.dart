import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Models/room_model.dart';
import 'package:room_buddy/Screens/Home/room_detail.dart';
import 'package:room_buddy/controllers/explore_controller.dart';
import 'package:room_buddy/controllers/favourites_controller.dart';
import 'package:room_buddy/controllers/profile_controller.dart';

class RoomCard extends StatelessWidget {
  final Room room;
   RoomCard({Key? key,  required this.room}) : super(key: key);
   var profilecont = Get.put(ProfileController());
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        Get.to(() =>  RoomDetailScreen(room: room,));
      },
      child: Card(
        elevation: 3,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 150,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(3),
                  image:  DecorationImage(
                      image: AssetImage(
                        room.images.first,
                      ),
                      fit: BoxFit.cover)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                 profilecont.user!.id == room.ownerId?const SizedBox():
                 GetBuilder<ProfileController>(
                   init: ProfileController(),
                   builder: (cont) => BouncingWidget(
                     scaleFactor: 5,
                     onPressed: (){
                       if(cont.user!.favIds.contains(room.id)){
                         cont.user!.favIds.removeWhere((element) => element == room.id);
                       }else{
                         cont.user!.favIds.add(room.id!);
                       }
                       cont.update();
                     },
                     child: Container(
                       margin: EdgeInsets.all(10),
                       height: 25,
                       width: 25,
                       decoration: BoxDecoration(
                           borderRadius: BorderRadius.circular(15),
                           color: Colors.white),
                       child: Center(
                         child: Icon(
                           cont.user!.favIds.contains(room.id)?Ionicons.heart:Ionicons.heart_outline,
                           size: 18,
                           color: cont.user!.favIds.contains(room.id)? Colors.red:Colors.grey.shade700,
                         ),
                       ),
                     ),
                   ),
                 ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                   Text(
                    room.bedroomType + " room in " + room.bedrooms.toString()+"BHK apartment" ,
                    softWrap: false,
                    overflow: TextOverflow.fade,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                   Text(
                    room.locality+" "+room.address,
                    style: TextStyle(
                        fontSize: 14,
                        color: Colors.black54,
                        fontWeight: FontWeight.w600),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                       Text(
                        "For "+room.roommatePreferences.gender+" ",
                        style: TextStyle(
                          fontSize: 13,
                          color: Colors.black45,
                        ),
                      ),
                      Row(
                        children:  [
                          Icon(
                            Ionicons.ellipse,
                            size: 6,
                            color: Colors.black45,
                          ),
                          Text(
                            "  ${room.bedroomType} room  ",
                            style: TextStyle(
                              fontSize: 13,
                              color: Colors.black45,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children:  [
                          Icon(
                            Ionicons.ellipse,
                            size: 6,
                            color: Colors.black45,
                          ),
                          Text(
                            "  ${room.availability}",
                            style: TextStyle(
                              fontSize: 13,
                              color: Colors.black45,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                   Text(
                    "\$ ${room.rent}",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 15),
                    height: 39,
                    child:ListView.builder(
                        itemCount: room.amenities.length,
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (BuildContext context,int index){
                          return Container(
                            margin: EdgeInsets.all(5),
                            padding: EdgeInsets.symmetric(horizontal: 15,vertical: 5),
                            height: 25,
                            child: Text(room.amenities[index],style: TextStyle(color: myResources.semiColor,fontWeight: FontWeight.w600),),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3),
                                color: myResources.semiColor.withOpacity(0.1)),
                          );
                        }
                    ),
                  ),
                  Divider(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children:  [
                      Row(
                        children: [
                           Padding(
                            padding: EdgeInsets.all(8.0),
                            child: CircleAvatar(backgroundImage: AssetImage(room.ownerDp),radius: 25,),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(room.ownerName,style: TextStyle(fontWeight: FontWeight.w600,fontSize: 15),),
                              Text("Roommate",style: TextStyle(fontSize: 13,color: Colors.black45),),
                            ],
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Icon(Icons.verified_user_sharp,color: myResources.semiColor,size: 20,),
                          Text(" Verified",style: TextStyle(color: myResources.semiColor,fontSize: 13,fontWeight: FontWeight.w600),)
                        ],
                      )
                    ],
                  ),
                  SizedBox(height: 10,)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
