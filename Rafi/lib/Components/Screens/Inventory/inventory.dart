import 'package:flutter/material.dart';
import 'package:rafi_admin/Components/AppBar/appbar.dart';
import 'package:rafi_admin/Components/Drawer/drawer.dart';
import 'Screens/Inventory Graph/inventory_graph.dart';
import 'Screens/Units List/units_page.dart';

class InventoryPage extends StatefulWidget {
  InventoryPage({
    Key? key,
  }) : super(key: key);

  @override
  _InventoryPageState createState() => _InventoryPageState();
}

const List<String> tabNames = <String>['Dashboard', 'Units',];
List<Widget> _buildScreens() {
  return [
    InventoryGraph(),
    const UnitsPage(),

  ];
}

class _InventoryPageState extends State<InventoryPage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: tabNames.length,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Inventory", style: const TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.w500,
              color: Colors.white),),
          backgroundColor: Color(0xff004251),
        ),
        drawer: const Drawer(child: SideDrawer()),
        body: TabBarView(
          children: List<Widget>.generate(tabNames.length, (int index) {
            return _buildScreens()[index];
          }),
        ),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.only(bottom: 1.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Material(
                color: const Color(0xff004251),
                child: TabBar(
                  indicatorColor: Colors.white,
                  isScrollable: true,
                  tabs: List.generate(tabNames.length, (index) {
                    return Container( width:(MediaQuery.of(context).size.width+100)/(tabNames.length+1),child: Tab(text: tabNames[index].toUpperCase()));
                  }),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
