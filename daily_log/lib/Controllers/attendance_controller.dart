
import 'package:daily_log/Models/attendance.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';



class AttendanceController extends GetxController {
  final DatabaseReference databaseReference =
  FirebaseDatabase.instance.ref("Attendance");


  @override
  Future<void> onInit() async {
    // TODO: implement onInit
    // await getData();

    super.onInit();
  }


  Future<void> addAttendance(Attendance attendance) async {
    try{ await databaseReference.push().set(attendance.toJson());
    update();
    }catch(e){
      Get.snackbar("Error", e.toString());
    }
  }





  Future<Attendance?> getAttendancebyDate(DateTime date) async {
    String dateString = DateTime(date.year,date.month,date.day).toString();
    var data = await databaseReference.orderByChild("date").equalTo(dateString).once();
    if(data.snapshot.value == null){
      return null;
    }else{
      Attendance attendance = Attendance.fromJson(
          data.snapshot.value, data.snapshot.key);
      return attendance;
    }
  }

}