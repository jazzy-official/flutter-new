class Project {
  final int Id;
  final int ProjectId;
  final String ProjectName;

  Project(
      {required this.Id,
        required this.ProjectId,
        required this.ProjectName
      });

  factory Project.fromJson(Map<String, dynamic> json) {
    return Project(
      Id: json['Id'] as int,
      ProjectId: json['Project']['ProjectId'] as int,
      ProjectName: json['Project']['ProjectName'] as String,
    );
  }
}