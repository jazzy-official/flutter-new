import 'dart:io';

import 'package:ap_academy_new/Constants/db_cred.dart';
import 'package:ap_academy_new/Constants/util.dart';
import 'package:ap_academy_new/Screens/Authentication/login_screen.dart';
import 'package:ap_academy_new/Screens/Setting/profile_edit.dart';
import 'package:ap_academy_new/Screens/drawer_screen.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'db_controller.dart';
import 'package:image_picker/image_picker.dart';

class ProfileController extends GetxController {
  Rx<List<Map<String, dynamic>>> usersDtails = Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get userDetail => usersDtails.value;
  Map<String, dynamic> get currentUdetail => userDetail.first;
  late Map<String, dynamic> temp = currentUdetail;
  final dbHelper = Get.put(DatabaseController());
  @override
  Future<void> onInit() async {
    // TODO: implement onInit
    super.onInit();
  }

  Future<int> initializeUserData({String? uid}) async {
    try {
      print("111");
      usersDtails.value = await dbHelper.getUserDetail(uid);
      print("222");
      Map<String, dynamic> userDtails = usersDtails.value.first;
      setuserdata(userDtails['name']);
print("userrr"+userDtails.toString());
      // await dbHelper
      //     .checkVerified(uid ?? FirebaseAuth.instance.currentUser.uid)
      //     .then((value) async {
      //       print("xxxxxxxxxxxx"+value.toString());
        if (userDtails != null) {
          if(userDtails['approved']==true && userDtails['isActive']==true) {
            Get.offAll(() => HomeDrawer());
          } else if(userDtails['isActive']==false){
            Get.defaultDialog(
              confirmTextColor: Colors.white,
                buttonColor: myResources.buttonColor,
                barrierDismissible: false,
                content: Container(
                  child: Text(
                      'You can not sign in to this application. please contact admin to active your profile'),
                ),
                onConfirm: () async {
                   Get.back();
                });
            // await Get.offAll(() => LoginPage());

          }else {
            await Get.offAll(() => ProfileEdit(isapproved: userDtails['approved'],));
          }

          // await Get.offAll(() => ApprovalWarn());
         return 2;
        }
      // });

      return 1;
    } catch (errr) {
      print("errrrrrrrr"+errr.toString());
      return 3;
    }
  }

  setuserdata(String name){
    temp = userDetail.where((element) => element['name'] == name).toList().first;
    update();
  }


  FirebaseStorage firebaseStorage = FirebaseStorage.instance;
  getImages() {
    firebaseStorage.ref().child('images').child('defaultProfile.png');
  }

  Future<String?> uploadImageToFirebase(
      File image, bool multi, int index) async {
    try {
      // Make random image name.
      String imageLocation;
      if (!multi)
        imageLocation =
            'profileimages/${FirebaseAuth.instance.currentUser!.uid}.jpg';
      else
        imageLocation =
            'profileimages/${FirebaseAuth.instance.currentUser!.uid}_$index.jpg';

      // Upload image to firebase.
      final storageReference =
          FirebaseStorage.instance.ref().child(imageLocation);
      final uploadTask = storageReference.putFile(image);
      await uploadTask.whenComplete(() => null);
      String imgLink = await uploadTask.snapshot.ref.getDownloadURL();
      if (!multi) {
        await dbHelper.addProfileImgPathToDatabase(imgLink);
        await FirebaseAuth.instance.currentUser!
            .updateProfile(photoURL: imgLink);
        print(imgLink);
        return imgLink;
      } else {
        print(imgLink);
        return imgLink;
      }
    } catch (e) {
      print(e);
    }
  }

  Future<bool> saveProfile(
      File? _image, List<File>? imagesList, Map<String, dynamic> userData,) async {
    List<String> urls = [];
    urls.map((e) => print(e));
    if (_image != null) {
      await uploadImageToFirebase(_image, false, 0);
    }
    if (imagesList != null && !imagesList.isEmpty) {
      for (int i = 0; i <= imagesList.length - 1; i++) {
        await uploadImageToFirebase(imagesList[i], true, i).then((value) {
          urls.add(value!);
        });
      }
    }

    bool isOk;

    isOk = await dbHelper.updateUser(userData).then((value) async {
      if (value) {
        usersDtails.value = (await dbHelper.getUserDetail( FirebaseAuth.instance.currentUser!.uid));
        await dbHelper.addProfiledocsPathToDatabase(urls);
        await initializeUserData();
        return true;
      } else {
        return false;
      }
    });
    return isOk;
  }

/////////////////////////////////////////

  late File _image;
  // profile image picke from camra r gallery and set it to new profile pic
  Future<File> imgFromCamera() async {
    PickedFile? image = await ImagePicker()
        .getImage(source: ImageSource.camera, imageQuality: 50);

    _image = File(image!.path);
    return _image;
  }

  Future<File> imgFromGallery() async {
    final image = await ImagePicker()
        .getImage(source: ImageSource.gallery, imageQuality: 50);

    _image = File(image!.path);
    return _image;
  }

  @override
  void onClose() {
    // db.close();
    super.onClose();
  }
}

class ApprovalWarn extends StatefulWidget {
  @override
  _ApprovalWarnState createState() => _ApprovalWarnState();
}

class _ApprovalWarnState extends State<ApprovalWarn> {
  @override
  Widget build(BuildContext context) {
    return StatefulBuilder(builder: (context, setState) {
      return WillPopScope(
          onWillPop: () async => false,
          child: ProfileEdit(
            isapproved: false,
          ));
    });
  }
}
