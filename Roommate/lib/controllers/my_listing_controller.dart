import 'dart:core';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:room_buddy/Models/about_user_model.dart';
import 'package:room_buddy/Models/habits_modal.dart';
import 'package:room_buddy/Models/location_model.dart';
import 'package:room_buddy/Models/room_model.dart';
import 'package:room_buddy/Models/user_model.dart';
import 'package:room_buddy/Screens/Listings/listing_screen.dart';
import 'package:room_buddy/controllers/explore_controller.dart';
import 'package:room_buddy/controllers/profile_controller.dart';
import 'package:room_buddy/navigation.dart';

class MyListingController extends GetxController {
  var expcont = Get.put(ExplorerController());
  var profilecont = Get.put(ProfileController());
  List<Room> get myRooms => expcont.rooms
      .where((element) => element.ownerId == profilecont.user!.id)
      .toList();
  late String city;
  late String locality;
  late String address;
  int roomNo = 1;
  String roomType = "Any";
  int bathroomNo = 1;
  String bathroomType = "Any";
  String furnishedType = "Any";
  LatLng location = LatLng(35.3435645, 76.453653);
  List<String> amenities = [];
  List<String> options = [
    'WIFI',
    'Elevator',
    'TV',
    'Security',
    'CookTop',
    'Parking',
    'A.C',
    'Geyser',
    'Balcony',
    'Maintenance',
    'Washing Machine',
    'Kitchen',
  ];
  bool includeBill = false;
  double range = 0;
  double securityDeposit = 0;
  double? roomSize;
  double? propertySize;
  String roomAvailability = "Immediate";
  DateTime? roomAvalabiltyDate;
  String minstay = "6 months";
  late String title;
  late String description;
  List<String> images = [];
  String smoking = "Never";

  String alcohol = "Never";

  String partying = "Weekend";

  String guests = "Occasionally";

  String pets = "May have";

  String gender = "Female";

  String martialStatus = "Unmarried";

  String occupation = "Professional";

  String foodchoice = "Non-veg";

  var selectedrange = RangeValues(22, 30);

  @override
  Future<void> onInit() async {
    super.onInit();
  }

  addroom() {
    Habits habits = Habits(smoking, foodchoice, partying, guests, pets);
    AboutUser aboutuser = AboutUser(
        gender, selectedrange, occupation, martialStatus, habits, null, null);
    Room room = Room(
        address: address,
        location: location,
        images: images,
        city: city,
        locality: locality,
        datePosted: DateTime.now(),
        description: description,
        rent: range,
        securityDeposit: securityDeposit,
        billIncluded: includeBill,
        furnishingType: furnishedType,
        bedrooms: roomNo,
        bedroomType: roomType,
        bathroom: bathroomNo,
        bathroomType: bathroomType,
        roomsize: roomSize!,
        propertySize: propertySize!,
        availability: roomAvailability,
        minStay: minstay,
        amenities: amenities,
        ownerName: profilecont.user!.name,
        aboutOwner: profilecont.user!.aboutUser,
        roommatePreferences: aboutuser,
        ownerDp: profilecont.user!.profilePic,
        ownerId: profilecont.user!.id);

    expcont.rooms.add(room);
    Get.offAll(NavigationScreen());
    update();
  }
}
