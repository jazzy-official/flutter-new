import 'dart:core';
import 'package:daily_log/Controllers/firebase_controller.dart';
import 'package:daily_log/Controllers/profilecontroller.dart';
import 'package:daily_log/View/Home/home.dart';
import 'package:daily_log/View/drawer_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../Models/user_model.dart';
import '../View/Login/login_screen.dart';
import '../View/loading_widget.dart';


class AuthController extends GetxController {



  @override
  Future<void> onInit() async {
    super.onInit();
  }

  var firebaseController = Get.put(FirebaseController());
  var prof = Get.put(ProfileController());
  FirebaseApp? firebaseApp;
  User? firebaseUser;
  FirebaseAuth? firebaseAuth;

  Future<void> initlizeFirebaseApp() async {
    firebaseApp = await Firebase.initializeApp();
    update();
  }

  final DatabaseReference databaseReference =
  FirebaseDatabase.instance.reference().child("Users");

  Future<void> signInwithEmail(String emails, String passs) async {
    try {
      // Get.dialog(Center(child: LoadingWidget()), barrierDismissible: false);
      Get.defaultDialog(title: '', content: const LoadingWidget(),backgroundColor: Colors.transparent,barrierDismissible: false);
      firebaseAuth = FirebaseAuth.instance;

      final userCredentialData = await firebaseAuth!.signInWithEmailAndPassword(
          email: emails, password: passs);
      firebaseUser = userCredentialData.user!;
      if(await checkadmin(firebaseUser!.uid)){
      prof.initializeUserData(firebaseUser!.uid);
      update();
      Get.offAll(() =>  HomeDrawer());
      }else{
        signOut();
      }

    } catch (ex) {
      print(ex.toString());
      Get.back();
      signOut();
      Get.snackbar('Sign In Error', ex.toString(),
          duration: Duration(seconds: 5),
          backgroundColor: Colors.black,
          colorText: Colors.white,
          snackPosition: SnackPosition.TOP,
          icon: const Icon(
            Icons.error,
            color: Colors.red,
          ));
    }
  }
  Future<void> signUp(String email,String pass,String name) async {
    try {
      Get.defaultDialog(title: '', content: const LoadingWidget(),backgroundColor: Colors.transparent,barrierDismissible: false);
      User? fireuser = await firebaseController.signup(email, pass,);
      UserModel user = UserModel(name: name, email: email, uid: fireuser!.uid, role: "Admin");
      await databaseReference.push().set(user.toJson());
      await prof.initializeUserData(fireuser.uid);
      Get.offAll(() =>  HomeDrawer());
    } catch (e) {
      // displayToast(e.toString());
      print(e.toString());
    }
  }
  Future<Widget?> checkUserLoggedIn() async {
    try {
      if (firebaseApp == null) {
        await initlizeFirebaseApp();
      }
      if (firebaseAuth == null) {
        firebaseAuth = FirebaseAuth.instance;
        update();
      }
    }catch(err)
    {
      print("err "+err.toString());
    }
    if (firebaseAuth!.currentUser == null) {
      return LoginScreen();
    } else {
      firebaseUser = firebaseAuth!.currentUser!;
      await prof.initializeUserData(firebaseUser!.uid);
      return HomeDrawer();
      // int isUserState= await profileController.initializeUserData();
    }
  }
  Future<void> signOut() async {

    await firebaseAuth!.signOut();
    update();

    // Navigate to Login again
    Get.offAll(() =>  LoginScreen());
  }


  Future<bool> checkadmin(String uid) async {
    late UserModel? user;
    await databaseReference
        .orderByChild("uid")
        .equalTo(uid)
        .once()
        .then((value) => {
      print(value.snapshot.value),
      if (value.snapshot.value != null)
        {
          Map.of(value.snapshot.value as Map<dynamic, dynamic>).forEach((key, value) {
            user = UserModel.fromJson(value, key);
          })
        }
    });
    if(user == null){
      Get.snackbar("Error", "User not found");
      return false;
    }else {
      if (user!.role == "Admin") {
        return true;
      } else {
        Get.snackbar("Error", "You are not an admin");
        return false;
      }
    }
  }

}
