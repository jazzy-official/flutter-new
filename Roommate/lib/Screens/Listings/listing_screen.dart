import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Screens/Listings/Create%20Listing%20Forms/room_location_form.dart';
import 'package:room_buddy/controllers/my_listing_controller.dart';
import '../../controllers/explore_controller.dart';
import '../Home/Widgets/room_list.dart';
import '../Home/Widgets/roommate_list.dart';
import '../Home/roommate_detail.dart';
import 'myListing_roomdetail.dart';

class ListingScreen extends StatelessWidget {
   ListingScreen({Key? key}) : super(key: key);
  var mylistingcont = Get.put(MyListingController());
  @override
  Widget build(BuildContext context) {
    return
      // DefaultTabController(
      // length: 2,
      // child:
      Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          // bottom:  TabBar(
          //   indicatorWeight: 4,
          //   indicatorColor: myResources.themeColor,
          //   tabs: const [
          //     Padding(
          //       padding: EdgeInsets.all(8.0),
          //       child: Text("Rooms",style: TextStyle(color: Colors.black,fontWeight: FontWeight.w600,fontSize: 16),),
          //     ),
          //     Padding(
          //       padding: EdgeInsets.all(8.0),
          //       child: Text("Wanted Rooms",style: TextStyle(color: Colors.black,fontWeight: FontWeight.w600,fontSize: 16),),
          //     ),
          //   ],
          // ),
          title: const Text('My Listings',style: TextStyle(color: Colors.black),),
        ),
        body: ListView(
          children: [
            RoomList(rooms: mylistingcont.myRooms,),
            const SizedBox(height: 100,)
          ],
        ),
        // TabBarView(
        //   children: [
        //     RoomList(route: () { Get.to(const MyListingRoomDetail());  },),
        //     RoommateList(route: () { Get.to(()=>RoommateDetailScreen());  },),
        //   ],
        // ),
        floatingActionButton: Padding(
          padding: const EdgeInsets.only(bottom: 50.0),
          child: FloatingActionButton(
            backgroundColor: myResources.themeColor,
            onPressed: () {
              Get.to(()=> RoomLocationForm());
            },
            child: const Icon(Icons.add),
          ),
        ),
      );
    // );
  }
}
