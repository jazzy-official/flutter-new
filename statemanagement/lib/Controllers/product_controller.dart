import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:state_management/Models/product_model.dart';

class ProductController extends GetxController {
  final List<Product> _items = [
    Product(
      id: 1,
      title: 'Sport Shoe',
      description: 'Made for you Check it out!',
      price: 7000.00,
      selectedQuantity: 0,
      imageUrl:
      'https://cdn.shopify.com/s/files/1/0419/1525/products/1024x1024-Cavalier-Black-1.jpg?v=1589391819',
    ),
    Product(
        id: 2,
        title: 'Legend',
        description:
        'Built to last forever, StormKing™ lug rubber outsoles and a flexible elastic goring, this can only be for the Legends and i bet you, you have not seen it anywhere.',
        price: 63000.00,
        selectedQuantity: 0,
        imageUrl:
        'https://cdn.shopify.com/s/files/1/0419/1525/products/1024x1024-Men-Legend-BlackMatte-3.4_672x672.jpg?v=1600886623'),
    Product(
        id: 3,
        title: 'The Chelsea',
        description: 'Functional and Fashionable.',
        price: 49.00,
        selectedQuantity: 0,
        imageUrl:
        'https://cdn.shopify.com/s/files/1/0419/1525/products/1024x1024-Cavalier-Black-1.jpg?v=1589391819'),
    Product(
        id: 4,
        title: 'Men\'s Sneakers',
        description: 'Clean & Comfortable Sneakers made with classic Leathers.',
        price: 49.99,
        selectedQuantity: 0,
        imageUrl:
        'https://cdn.shopify.com/s/files/1/0419/1525/products/1024x1024-Men-PremierLowTop-Black-3.4.jpg?v=1600270679'),
    Product(
        id: 5,
        title: 'The Chelsea',
        description:
        'Comfortable as you\'d expect.This can only be found at Resilient collection.',
        price: 49.99,
        selectedQuantity: 0,
        imageUrl:
        'https://cdn.shopify.com/s/files/1/0419/1525/products/1024x1024-Captain-Natural-3.jpg?v=1584114360'),
    Product(
        id: 6,
        title: 'Men\'s Sneakers',
        description: 'Clean & Comfortable Sneakers made with classic Leathers.',
        price: 49.99,
        selectedQuantity: 0,
        imageUrl:
        'https://cdn.shopify.com/s/files/1/0419/1525/products/1024x1024-Men-PremierLowTop-Black-3.4.jpg?v=1600270679'),
    Product(
        id: 7,
        title: 'The Chelsea',
        description:
        'Made for the men who understand what classic means, every bit was carefully selected so you can go the extra mile with confidence and alacrity.',
        price: 49.99,
        selectedQuantity: 0,
        imageUrl:
        'https://cdn.shopify.com/s/files/1/0419/1525/products/1024x1024-Men-Cavalier-Toffee-210402-2.jpg?v=1618424894'),
    Product(
        id: 8,
        title: 'Men\'s Sneakers',
        description: 'Clean & Comfortable Sneakers made with classic Leathers.',
        price: 49.99,
        selectedQuantity: 0,
        imageUrl:
        'https://cdn.shopify.com/s/files/1/0419/1525/products/1024x1024-Men-Cavalier-Toffee-210402-3.jpg?v=1618424894'),
    Product(
        id: 9,
        title: 'The Chelsea',
        description: 'Functional and Fashionable.',
        price: 49.99,
        selectedQuantity: 0,
        imageUrl:
        'https://cdn.shopify.com/s/files/1/0419/1525/products/1024x1024-Cavalier-Black-1.jpg?v=1589391819'),
    Product(
        id: 10,
        title: 'Men\'s Sneakers',
        description: 'Clean & Comfortable Sneakers made with classic Leathers.',
        price: 49.99,
        selectedQuantity: 0,
        imageUrl:
        'https://cdn.shopify.com/s/files/1/0419/1525/products/1024x1024-Men-PremierLowTop-Black-3.4.jpg?v=1600270679'),
  ];

  List<Product> get items {
    return [..._items];
  }

  void addItem(int productId,) {
    _items.where((element) => element.id==productId).first.selectedQuantity++ ;
      update();
  }

  List<Product> get favouriteItems {
    return _items.where((productItem) => productItem.isFavourite).toList();
  }

  Product findProductById(int id) {
    return _items.firstWhere((element) => element.id == id);
  }


  void toggleFavouriteStatus(int id) {
    items[id].isFavourite = !items[id].isFavourite;
    update();
  }
}