import 'cart.dart';

class Order {
  final String id;
  final String uid;
  final int timestamp;
  final bool? hasPromoCode;
  final String? promoCode;
  final num? promoDiscount;
  final num deliveryCharges;
  final num totalAmount;
  final List<Cart> items;

  Order({
    this.promoCode,
    required this.id,
    required this.uid,
    required this.timestamp,
    required this.deliveryCharges,
    required this.totalAmount,
    required this.items,
    this.hasPromoCode,
    this.promoDiscount,
  });

  factory Order.fromMap(Map<dynamic, dynamic> map) {
    return  Order(
      id: map['id'] as String,
      uid: map['uid'] as String,
      timestamp: map['timestamp'] as int,
      deliveryCharges: map['deliveryCharges'],
      totalAmount: map['totalAmount'],
      items: map['items'] == null ? [] :Cart.toOrderItemList(map['items']),
    );
  }

  Map<dynamic, dynamic> toSetMap() {
    return {
      'id': this.id,
      'uid': this.uid,
      'timestamp': this.timestamp,
      'deliveryCharges': this.deliveryCharges,
      'totalAmount': this.totalAmount,
      'items': Cart.toOrderMap(this.items),
    } as Map<dynamic, dynamic>;
  }
}