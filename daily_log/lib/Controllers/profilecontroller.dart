import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

import '../Models/user_model.dart';

class ProfileController extends GetxController {
  Rx<UserModel> usersDtails = Rx<UserModel>(UserModel(email: '', role: '', name: '', uid: ''));
  UserModel get userDetail => usersDtails.value;

  final DatabaseReference databaseReference =
  FirebaseDatabase.instance.ref().child("Users");
  final _auth = FirebaseAuth.instance;

  @override
  Future<void> onInit() async {
    // TODO: implement onInit
    super.onInit();
  }

  Future<UserModel?> initializeUserData(String uid) async {
    late UserModel user;
    await databaseReference
        .orderByChild("uid")
        .equalTo(uid)
        .once()
        .then((value) => {
          print(value.snapshot.value),
      if (value.snapshot.value != null)
        {
          Map.of(value.snapshot.value as Map<dynamic, dynamic>).forEach((key, value) {
            user = UserModel.fromJson(value, key);
          })
        }
      // else
      //   {
      //     user = UserModel(
      //         name: _auth.currentUser!.displayName!,
      //         email: _auth.currentUser!.email!,
      //         imgUrl: _auth.currentUser!.photoURL!,
      //         uid: _auth.currentUser!.uid,
      //         role: "admin"),
      //   }
    });
    usersDtails = user.obs;
    refresh();
    update();
    return user;
  }
}