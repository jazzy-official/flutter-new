
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:intl/intl.dart';

import '../../Controllers/projectscontroller.dart';
import '../../Models/module.dart';
import '../../Models/project_model.dart';
import '../../constants/util.dart';
import 'add_module.dart';

class AddProject extends StatefulWidget {
  final ProjectModel? project;
  AddProject({Key? key, this.project}) : super(key: key);

  @override
  State<AddProject> createState() => _AddProjectState();
}

class _AddProjectState extends State<AddProject> {

  TextEditingController titleCont = TextEditingController();
  TextEditingController desCont = TextEditingController();
  DateTime deadline = DateTime.now();
  String? assignto;
  List<Module> modules=[];
  bool isComplete = false;

  @override
  void initState() {
    populatedata();
    super.initState();
  }


  void populatedata(){
    if(widget.project != null){
      setState(() {
        titleCont.text = widget.project!.name;
        desCont.text = widget.project!.des;
        assignto = widget.project!.assignto;
        modules = widget.project!.modules;
        isComplete = widget.project!.status;
        if(widget.project!.deadline != null) {
          deadline = widget.project!.deadline!;
        }
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    void _showDialog(Widget child) {
      showCupertinoModalPopup<void>(
          context: context,
          builder: (BuildContext context) => Container(
                height: 216,
                padding: const EdgeInsets.only(top: 6.0),
                // The Bottom margin is provided to align the popup above the system navigation bar.
                margin: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom,
                ),
                // Provide a background color for the popup.
                color: CupertinoColors.systemBackground.resolveFrom(context),
                // Use a SafeArea widget to avoid system overlaps.
                child: SafeArea(
                  top: false,
                  child: child,
                ),
              ));
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyResources.themecolor,
        title: const Text("Add Modules"),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: GetBuilder<ProjectController>(
          autoRemove: false,
          init: ProjectController(),
          builder: (cont) => ListView(
            children: [

              ListView.builder(
                shrinkWrap: true,
                itemCount: modules.length,
                itemBuilder: (context, index) {
                  return Card(
                    child: Padding(
                      padding: const EdgeInsets.all(6.0),
                      child: Row(
                        children: [
                          Expanded(
                            child: InkWell(
                              onTap: (){
                                Get.to(AddModule(addmodule: (e){
                                  setState(() {
                                    modules[index] = e;
                                  });

                                },module: modules[index],));
                              },
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(modules[index].name.toUpperCase(),style: const TextStyle(fontWeight: FontWeight.w600),),
                                )
                            ),
                          ),
                          IconButton(onPressed: (){
                            modules.removeAt(index);
                            setState(() {
                            });
                          }, icon: Icon(Icons.clear,color: MyResources.themecolor,))
                        ],
                      ),
                    ),
                  );
                },
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(primary: MyResources.headingColor),
                  onPressed: (){
                  Get.to(AddModule(addmodule: (value) {
                    modules.add(value);
                    setState(() {
                      
                    });
                    },));
                  },
                  child: const Text("Add Module")
              ),
              const SizedBox(
                height: 50,
              ),
              TextButton(
                  style: TextButton.styleFrom(padding: EdgeInsets.zero),
                  onPressed: () async {
                    if (titleCont.text == "") {
                      Get.snackbar("Alert", "Please enter project Name");
                    } else if (desCont.text == "") {
                      Get.snackbar("Alert", "Please enter description");
                    }  else if (assignto == null) {
                      Get.snackbar("Alert", "Please assign the a manager");
                    } else {
                      if(widget.project == null) {
                        ProjectModel project = ProjectModel(deadline: deadline, status: isComplete, name: titleCont.text, des: desCont.text,assignto: assignto, modules: modules);
                        await cont.addProject(project);
                      } else{
                        ProjectModel project = ProjectModel(id:widget.project!.id,deadline: deadline, status: isComplete, name: titleCont.text, des: desCont.text,assignto: assignto, modules: modules);
                        await cont.updateProject(project);
                      }

                    }
                  },
                  child: Container(
                    height: 40,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3),
                      color: MyResources.themecolor,
                    ),
                    child: const Center(
                        child: Text(
                      "Save",
                      style: TextStyle(color: Colors.white, fontSize: 17),
                    )),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}

