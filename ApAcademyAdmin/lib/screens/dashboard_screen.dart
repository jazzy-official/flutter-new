import 'package:ap_academy_admin/ModelView/Controller/CalenderController.dart';
import 'package:ap_academy_admin/ModelView/Controller/GroupController.dart';
import 'package:ap_academy_admin/ModelView/Controller/NotificationController.dart';
import 'package:ap_academy_admin/ModelView/Controller/dashboardController.dart';
import 'package:ap_academy_admin/ModelView/Controller/matchController.dart';
import 'package:ap_academy_admin/ModelView/Controller/teamsController.dart';
import 'package:ap_academy_admin/screens/eventList.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:intl/intl.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'CalendarEventList.dart';

class DashBoard extends StatefulWidget {
  //teams under card
  @override
  _DashBoardState createState() => _DashBoardState();
}

class _DashBoardState extends State<DashBoard> {
  //AnimationController _animationController = new AnimationController();
  final cController = Get.put(CalController());
  final mC = Get.put(MatchController());
  List<Meeting> tEvent = <Meeting>[];
  List birthdayList = [];
  static final DateFormat formatter = DateFormat('MMM');
  // CalendarController _calendarController = new CalendarController();
  final dashBoardC = Get.put(DashBoardController());
  final teamC = Get.put(TeamController());
  // List<ChartData> Data = [];
  // final List<Color> colors = <Color>[
  //   Colors.red,
  //   Colors.blue,
  //   Colors.green,
  //   Colors.yellow,
  //   Colors.orange,
  //   Color.fromRGBO(9, 0, 136, 1),
  //   Color.fromRGBO(147, 0, 119, 1),
  //   Color.fromRGBO(228, 0, 124, 1),
  //   Color.fromRGBO(255, 189, 57, 1)
  // ];
  // Map<String, double> piechartData = {'No Groups Available ': 0.0};
  // getGroupdata() async {
  //   setState(() {
  //     Data = [];
  //   });
  //   Get.put(GroupsControler()).refreshGroups();
  //   await Future.delayed(Duration(milliseconds: 3000));
  //   Map<String, double> _map = Map();
  //   int i = 0;
  //   if (Get.put(GroupsControler()).group != null) {
  //     if (Get.put(GroupsControler()).group.length != 0) {
  //       Get.put(GroupsControler()).group.forEach((element) {
  //         Data.add(ChartData(
  //             element['name'], element['number'].toDouble(), colors[i]));
  //         i++;
  //       });
  //       print(_map.toString());
  //
  //       print(Data[2].color);
  //       piechartData = _map;
  //       setState(() {});
  //       return _map;
  //     } else {
  //       piechartData = {'No Groups Available ': 0.0}.obs;
  //       return {'No Groups Available ': 0.0};
  //     }
  //   } else {
  //     piechartData = {'No Groups Available ': 0.0}.obs;
  //     return {'No Groups Available ': 0.0};
  //   }
  // }

  @override
  void initState() {
    refreshAll();
    getusers();
    super.initState();
  }
  void getusers () async {
    await teamC.getUsers().then((value) async{
      teamC.userList.forEach((element) {
        if(element['dob']!=null){
        if(formatter.format(element['dob'])==formatter.format(DateTime.now())){
          birthdayList.add(element);
        }
        }
      });
      print(birthdayList.length);
      birthdayList.sort((a, b) => a['dob'].day.compareTo(b['dob'].day));
      setState(() {
        birthdayList;
      });

    });
  }
  refreshAll() async {
    dashBoardC.pendingRefresh();
    List<Meeting> eventList = <Meeting>[];
    var tEvents = await cController.getTrainingEvents();
    var mEvents = mC.match;
    print(mEvents.length);

    mEvents.forEach((element) {
      eventList.add(Meeting(
        eventName: "Match",
        description: element['team1name']+" vs "+element['team2name'].toString(),
        background: Colors.blue,
        from:  element['matchTime'],
        to:null,
        recurrenceRule:'FREQ=DAILY;INTERVAL=7;COUNT=1',
      ));
    });
    tEvents.forEach((element) {
      eventList.add(Meeting(
        eventName: "Training",
        description: element['session'].toString(),
        background: Colors.blue,
        from: element['time'],
        to:element['time'],
        recurrenceRule: element['canSchedual'] ?'FREQ=DAILY;INTERVAL=7;COUNT=52':"",
      ));
    });
    setState(() {
      tEvent = eventList;
    });
  }

  @override
  void dispose() {
    // _animationController.dispose();
    // _calendarController.dispose();
    super.dispose();
  }

  bool isOpenPanel = false;
  void _onDaySelected(DateTime day, List<Meeting> events, List holidays) {
    setState(() {
      selectedEvents = events;
      isOpenPanel = events.toString() == '[]' ? false : true;
    });
  }

  Duration getAge(DateTime dob) {
    DateTime today = DateTime.now();
    return today.difference(dob);
  }

  // void _onVisibleDaysChanged(
  //     DateTime first, DateTime last, CalendarFormat format) {
  //   print('CALLBACK: _onVisibleDaysChanged');
  // }
  //
  // void _onCalendarCreated(
  //     DateTime first, DateTime last, CalendarFormat format) {
  //   print('CALLBACK: _onCalendarCreated');
  // }

  RxInt selectedValue = 0.obs;
  Widget _pendingApproval(Map<String, dynamic> approvel) {
    return Container(
      //  height: 80,
      width: Get.width - 36,
      color: Colors.transparent,
      child: Card(
          margin: EdgeInsets.only(right: 0, left: 8, bottom: 08),
          child: Padding(
            padding: const EdgeInsets.only(top: 8.0, right: 8, left: 8),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Text(
                            "Name:",
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Text(
                            "Email:",
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Text(
                            "DoB:",
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Text(
                            "Age:",
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Text(
                            "Phone:",
                          ),
                        ),
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Container(
                            width: 200,
                            child: Text(
                              approvel['name'].toString(),
                              overflow: TextOverflow.fade,
                              style: MyResources.appTextStyle,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Container(
                            width: 200,
                            child: Text(
                              approvel['email'].toString(),
                              overflow: TextOverflow.fade,
                              style: MyResources.appTextStyle,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Container(
                            width: 200,
                            child: Text(
                              approvel['dob'] != null
                                  ? DateFormat('yyyy-MM-dd')
                                      .format(approvel['dob'])
                                  : "Not Defined",
                              overflow: TextOverflow.fade,
                              style: MyResources.appTextStyle,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Text(
                            approvel['dob'] != null
                                ? (getAge(approvel['dob']).inDays ~/ 365)
                                    .toInt()
                                    .toString()
                                : "Not Defined",
                            style: MyResources.appTextStyle,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Container(
                            width: 200,
                            child: Text(
                              approvel['phone'].toString(),
                              overflow: TextOverflow.fade,
                              style: MyResources.appTextStyle,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    IconButton(
                      onPressed: () async {
                        // if (selectedValue.value != '') {
                        bool isApproved = await dashBoardC.approveUser(approvel['_id']);
                        if (isApproved) {
                          Get.defaultDialog(
                              title: "Approved",
                              content: Text("User is approved by admin"),
                              barrierDismissible: false,
                              buttonColor: MyResources.buttonColor,
                              confirmTextColor: Colors.white,
                              onConfirm: () {
                                dashBoardC.pendingRefresh();
                                Get.back();
                                if (mounted) setState(() {});
                                // Get.offAll(HomeDrawer());
                              });
                          // } else {
                          //   Get.defaultDialog(
                          //       title: "Error",
                          //       content: Text("User is not approved "));
                          // }
                        } else {
                          Get.snackbar(
                              "Approval error", "Please select group First",
                              snackPosition: SnackPosition.BOTTOM);
                        }
                      },
                      icon: Icon(
                        Icons.check_circle_outlined,
                        color: Colors.green,
                      ),
                    ),
                    IconButton(
                      onPressed: () {},
                      icon: Icon(
                        Icons.cancel_outlined,
                        color: Colors.red,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )),
    );
  }

  bool isApprovalPending = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyResources.backgroundColor,
      appBar: AppBar(
        toolbarHeight: 0,
        title: Text(
          'DashBoard',
          //TODO:  style: GoogleFonts.roboto(),
        ),
        // centerTitle: true,
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                // create calendar card
                Container(
                  height: 550,
                  padding:
                      const EdgeInsets.only(top: 10.0, left: 10, right: 10),
                  margin: EdgeInsets.only(top: 20),
                  child: Card(
                    elevation: 5,
                    shadowColor: Colors.grey,
                    color: MyResources.appBarActionsColor,
                    child: _buildTableCalendar(),
                  ),
                ),
                // //card of paending approvels that are not yet approved(e.g someone sinup login not approved)
                Padding(
                  padding:
                      const EdgeInsets.only(top: 10.0, left: 10, right: 10),
                  child: Center(
                    child: InkWell(
                      onTap: () {},
                      child: Container(
                        constraints: BoxConstraints(minHeight: 180),
                        child: Card(
                          elevation: 5,
                          shadowColor: Colors.black,
                          color: Colors.white,
                          child: Column(
                            children: [
                              Center(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    "Pending Approvals",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                              Container(
                                child: GetX<DashBoardController>(
                                  init: Get.put<DashBoardController>(
                                      DashBoardController()),
                                  builder:
                                      (DashBoardController dashController) {
                                    if (dashController != null &&
                                        dashController.pendingApproval !=
                                            null) {
                                      if (dashController
                                              .pendingApproval.length >
                                          0) {
                                        return Container(
                                          height: 300,
                                          child: ListView.builder(
                                              scrollDirection: Axis.horizontal,
                                              itemCount: dashController
                                                  .pendingApproval.length,
                                              itemBuilder:
                                                  (BuildContext context,
                                                      int index) {
                                                return _pendingApproval(
                                                    dashController
                                                            .pendingApproval[
                                                        index]);
                                              }),
                                        );
                                      } else {
                                        return Align(
                                          alignment: Alignment.bottomCenter,
                                          child: Container(
                                            width: Get.width,
                                            height: 105,
                                            child: Center(
                                                child: Container(
                                                    // width: 190,
                                                    child: Text(
                                              "No Pending Approvals",
                                              style: TextStyle(
                                                  color: Colors.green.shade400,
                                                  fontSize: 22),
                                            ))),
                                          ),
                                        );
                                      }
                                    } else {
                                      return Container(
                                        child: LoadingWidget(),
                                      );
                                    }
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 10.0, left: 10, right: 10),
                  child: Center(
                    child: InkWell(
                      onTap: () {},
                      child: Container(
                        constraints: BoxConstraints(minHeight: 180),
                        child: Column(
                          children: [
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  DateFormat("MMMM").format(DateTime.now())+" Birthdays",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                      SizedBox(
                        height: 400,
                        child: ListView.builder(
                          scrollDirection: Axis.vertical,
                          itemCount: birthdayList.length,
                          itemBuilder:
                              (BuildContext context,
                              int index) {
                            var student = birthdayList[index];
                            return _birthdayCard(student);
                          }),
                      ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                //card of pichart under which category (under 11 ,18 etc)
                SizedBox(
                  height: 10,
                ),
                // Card(
                //   margin: EdgeInsets.only(left: 13, right: 13, bottom: 20),
                //   elevation: 5,
                //   //  shadowColor: MyResources.headingColor,
                //   color: MyResources.appBarActionsColor,
                //   child: Column(
                //     children: [
                //       Padding(
                //         padding: const EdgeInsets.all(8.0),
                //         child: Text(
                //           "Groups",
                //           style: MyResources.appHeadingStyle,
                //         ),
                //       ),
                //       SfCircularChart(
                //           margin: EdgeInsets.zero,
                //           legend: Legend(
                //               isVisible: true, position: LegendPosition.auto),
                //           series: <CircularSeries>[
                //             // Renders doughnut chart
                //             DoughnutSeries<ChartData, String>(
                //                 dataSource: Data,
                //                 pointColorMapper: (ChartData data, _) =>
                //                     data.color,
                //                 xValueMapper: (ChartData data, _) => data.x,
                //                 yValueMapper: (ChartData data, _) => data.y)
                //           ])
                //     ],
                //   ),
                // ),
              ],
            ),
          ),
          _buildEventList(),
        ],
      ),
    );
  }

  Widget _buildTableCalendar() {
    return SfCalendar(
      view: CalendarView.month,
      dataSource: MeetingDataSource(tEvent),
      // by default the month appointment display mode set as Indicator, we can
      // change the display mode as appointment using the appointment display
      // mode property
      onTap: (CalendarTapDetails details) {
        DateTime date = details.date!;
        List<Meeting> appointment =
        tEvent.where((element) => DateTime.parse(DateFormat('yyyy-MM-dd').format(element.from)) == DateTime.parse(DateFormat('yyyy-MM-dd').format(date))).toList();
//             details.appointments!.forEach((element) {
// appointment.add(Meeting(from: element.from, to: element.to, background: element.background,eventName: element.eventName));
//         });
        _onDaySelected(date,appointment,[]);
      },
      monthViewSettings: const MonthViewSettings(
          appointmentDisplayMode: MonthAppointmentDisplayMode.appointment),
    );
    // Calendar(
    //   // calendarController: _calendarController,
    //     isExpanded:true,
    //     events: tEvent,
    //     initialDate: DateTime.now(),
    //     eventColor: MyResources.buttonColor,
    //     startOnMonday: true,
    //     selectedColor: Color(0xfff2bc5e),
    //     todayColor: MyResources.headingColor,
    //     onDateSelected: (date) => _onDaySelected(date,[tEvent[DateTime.parse(DateFormat('yyyy-MM-dd').format(date))]],[])
    // );
  }

  Widget _buildEventList() {
    return SlidingUpPanel(
      isDraggable: true,
      backdropEnabled: true,
      defaultPanelState: PanelState.CLOSED,
      onPanelClosed: () {
        isOpenPanel = false;
        setState(() {});
      },
      maxHeight: 420,
      minHeight: isOpenPanel ? 120 : 0,
      borderRadius: const BorderRadius.only(
          topRight: Radius.circular(30.0), topLeft: Radius.circular(30.0)),
      panelBuilder: (ScrollController sc) => _scrollingList(selectedEvents),
    );
  }

  Widget _birthdayCard(var student){
    return Card(
      color: DateFormat('MMM-dd').format(student['dob'])==DateFormat('MMM-dd').format(DateTime.now())?Colors.amber.shade100:null,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const CircleAvatar(
              backgroundColor: MyResources.headingColor,
              radius: 25,
              child: Icon(Icons.cake),
            ),
            SizedBox(width: 10,),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(student['name'],style: TextStyle(color: MyResources.buttonColor,fontSize: 16,fontWeight: FontWeight.w600),),
                  Text(DateFormat("MMMM dd").format(student['dob']),style: TextStyle(color: Colors.grey),),
                ],
              ),
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Colors.purple,
                elevation: 3,
                shape:  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                ),
              ),
                onPressed: () async {
                  Map<String, dynamic> notification = {
                    'title': "\u{1f382} Happy Birthday..!",
                    // 'type': _value.toString(),
                    'desc': "We wish you a very happy birthday " + student['name'],
                    'timeCreated': DateTime.now(),
                  };
                await NotificationController().sendAndRetrieveMessage(notification,[student['token']]);
                  Get.snackbar("Notification Sent", "Birthday notification sent to "+ student['name'],
                      snackPosition: SnackPosition.BOTTOM);
                },
                child: Text("Wish")
            )
          ],
        ),
      ),
    );
  }
}

Widget _scrollingList(List<Meeting> _selectedEvents) {
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: EventList(event: _selectedEvents,),
  );
}

class ChartData {
  ChartData(this.x, this.y, this.color);
  final String x;
  final double y;
  final Color? color;
}

class MeetingDataSource extends CalendarDataSource {
  MeetingDataSource(List<Meeting> source){
    appointments = source;
  }

  @override
  DateTime getStartTime(int index) {
    return appointments![index].from;
  }

  // @override
  // DateTime getEndTime(int index) {
  //   return appointments![index].to;
  // }

  @override
  bool isAllDay(int index) {
    return appointments![index].isAllDay;
  }

  @override
  String getSubject(int index) {
    return appointments![index].eventName;
  }

  @override
  Color getColor(int index) {
    return appointments![index].background;
  }

  @override
  String getRecurrenceRule(int index) {
    return appointments![index].recurrenceRule;
  }
}

class Meeting {
  Meeting(
      {this.eventName = '',
      this.description = '',
       required this.from,
      required this.to,
      required this.background,
      this.isAllDay = false,
      this.recurrenceRule});

  String eventName;
  String description;
  DateTime from;
  DateTime? to;
  Color background;
  bool isAllDay;
  String? recurrenceRule;
}