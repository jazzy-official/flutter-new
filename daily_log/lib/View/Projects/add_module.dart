import 'package:daily_log/Controllers/projectscontroller.dart';
import 'package:daily_log/Models/sub_module.dart';
import 'package:daily_log/constants/util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:uuid/uuid.dart';

import '../../Models/module.dart';
import '../delete_popup.dart';

class AddModule extends StatefulWidget {
  final Module? module;
  final Function(Module) addmodule;
  const AddModule({Key? key, this.module, required this.addmodule}) : super(key: key);

  @override
  State<AddModule> createState() => _AddModuleState();
}

class _AddModuleState extends State<AddModule> {
  List<SubModule> submodules = [];
  var id = const Uuid();
  TextEditingController titlecont = TextEditingController();
  TextEditingController subtitleCont = TextEditingController();
  var projectcont = Get.put(ProjectController());

  @override
  void initState() {
    populatedata();
    super.initState();
  }


  void populatedata(){
    if(widget.module != null){
      setState(() {
        titlecont.text = widget.module!.name;
        if(widget.module!.submodules!=null) {
          submodules = widget.module!.submodules!;
        }
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyResources.themecolor,
        title: const Text("Add Modules"),
        centerTitle: true,
        actions: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: ElevatedButton(
              onPressed: () {
                if(titlecont.text==""){
                  Get.snackbar("Alert", "Enter a title");
                }else {
                  widget.addmodule(Module(status: submodules.isEmpty?true:false, name: titlecont.text,submodules: submodules,id: id.v1()));
                  // projectcont.modules.add(Module(status: false, name: titlecont.text,submodules: submodules,id: id.v1()));
                  // projectcont.update();
                  Get.back();
                }

              },
              child: Text(
                "Save",
                style: TextStyle(color: MyResources.themecolor),
              ),
              style: ElevatedButton.styleFrom(primary: Colors.white,),
            ),
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Stack(
          children: [
            Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 40,
                  child: TextFormField(
                    controller: titlecont,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Title',
                    ),
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                    
                    itemCount: submodules.length,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(submodules[index].name.toUpperCase(),style: const TextStyle(fontWeight: FontWeight.w600),),
                            IconButton(onPressed: (){
                              submodules.removeAt(index);
                              setState(() {
                              });
                            }, icon: Icon(Icons.clear,color: MyResources.themecolor,))
                          ],
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(
                  height: 100,
                ),


              ],
            ),
            Positioned(
              bottom: 0,
              width: Get.width,
              child: Container(
                color: Colors.white,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      width: Get.width - 20,
                      height: 50,
                      padding: const EdgeInsets.only(bottom: 8.0, right: 10),
                      child: TextField(
                        keyboardType: TextInputType.multiline,
                        controller: subtitleCont,
                        decoration: InputDecoration(
                          labelText: "Sub-Module title",
                          fillColor: Colors.white,
                          border: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Colors.black, width: 2.0),
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: ElevatedButton(
                          onPressed: () {
                            submodules.add(SubModule(status: false, name: subtitleCont.text,id: id.v1()));
                            subtitleCont.clear();
                            setState(() {
                              submodules;
                            });
                            print(submodules.length);
                          },
                          style: ElevatedButton.styleFrom(
                              primary: MyResources.themecolor),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: const [
                              Text("Add Sub-Module"),
                            ],
                          )),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
