import 'package:flutter/material.dart';

import 'city_card.dart';

class LocalitiesList extends StatelessWidget {
  const LocalitiesList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width-30,
      height: 340,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: 3,
        itemBuilder: (context, index) {
          return const CityCard();
        },
      ),
    );
  }
}
