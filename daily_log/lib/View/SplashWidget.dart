import 'package:animate_do/animate_do.dart';
import 'package:daily_log/constants/util.dart';
import 'package:flutter/material.dart';

class SplashWidget extends StatefulWidget {
  @override
  _SplashWidgetState createState() => _SplashWidgetState();
}

class _SplashWidgetState extends State<SplashWidget> {
  late AnimationController animateController;
  @override
  Widget build(BuildContext context) {
    return Container(
        color: const Color(0xfff1f2f3),
        child: ElasticIn(
          // (optional) if true, will not fire the animation on load
          manualTrigger: true,
          child: Image.asset(
            "assets/logo.png",
            width: 200,
            height: 200,
          ),
          duration: const Duration(seconds: 5),
          //(optional, but mandatory if you use manualTrigger:true) This callback exposes the AnimationController used for the selected animation. Then you can call animationController.forward() to trigger the animation wherever you like manually.
          controller: (controller) => animateController = controller,

          //  child: Image.asset('assets'),
        ));
  }
}
