import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:ionicons/ionicons.dart';
import 'package:room_buddy/Constants/util.dart';

class ChatWidget extends StatefulWidget {
   ChatWidget({Key? key}) : super(key: key);

  @override
  State<ChatWidget> createState() => _ChatWidgetState();
}

class _ChatWidgetState extends State<ChatWidget> {
  List<ChatMessage> messages = [
    ChatMessage(messageContent: "Hello, Will", messageType: "receiver"),
    ChatMessage(messageContent: "How have you been?", messageType: "receiver"),
    ChatMessage(messageContent: "Hey Kriss, I am doing fine dude. wbu?", messageType: "sender"),
    ChatMessage(messageContent: "ehhhh, doing OK.", messageType: "receiver"),
    ChatMessage(messageContent: "Is there any thing wrong?", messageType: "sender"),
    ChatMessage(messageContent: "Hello, Will", messageType: "receiver"),
    ChatMessage(messageContent: "How have you been?", messageType: "receiver"),
    ChatMessage(messageContent: "Hey Kriss, I am doing fine dude. wbu?", messageType: "sender"),
    ChatMessage(messageContent: "ehhhh, doing OK.", messageType: "receiver"),
    ChatMessage(messageContent: "Is there any thing wrong?", messageType: "sender"),
  ];
  final ScrollController _scrollController = ScrollController();

  _scrollToBottom() {
    if (_scrollController.hasClients) {
      _scrollController.animateTo(_scrollController.position.maxScrollExtent,
          duration: Duration(milliseconds: 250), curve: Curves.easeOut);
    } else {
      Timer(Duration(milliseconds: 400), () => _scrollToBottom());
    }
  }
  TextEditingController message = TextEditingController();
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance?.addPostFrameCallback((_) => _scrollToBottom());
    return Stack(
      children: <Widget>[
        ListView.builder(
          itemCount: messages.length,
          controller: _scrollController,
          shrinkWrap: true,
          padding: EdgeInsets.only(top: 10,bottom: 60),
          // physics: NeverScrollableScrollPhysics(),
          itemBuilder: (context, index){
            return Container(
              padding: const EdgeInsets.only(left: 14,right: 14,top: 10,bottom: 10),
              child: Align(
                alignment: (messages[index].messageType == "receiver"?Alignment.topLeft:Alignment.topRight),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: (messages[index].messageType  == "receiver"?Colors.grey.shade200:myResources.themeColor),
                  ),
                  padding: const EdgeInsets.all(16),
                  child: Text(messages[index].messageContent, style: TextStyle(fontSize: 15,color: (messages[index].messageType  == "receiver"?Colors.black:Colors.white),),),
                ),
              ),
            );
          },
        ),
        Align(
          alignment: Alignment.bottomLeft,
          child: Container(
            padding: const EdgeInsets.only(left: 10,bottom: 10,top: 10),
            height: 60,
            width: double.infinity,
            color: Colors.white,
            child: Row(
              children: <Widget>[
                GestureDetector(
                  onTap: (){
                  },
                  child: Container(
                    height: 35,
                    width: 35,
                    decoration: BoxDecoration(
                      color: myResources.themeColor,
                      borderRadius: BorderRadius.circular(30),
                    ),
                    child: const Icon(Entypo.attachment, color: Colors.white, size: 20, ),
                  ),
                ),
                const SizedBox(width: 15,),
                 Expanded(
                  child: TextField(
                    controller: message,
                    decoration: const InputDecoration(
                        hintText: "Write message...",
                        hintStyle: TextStyle(color: Colors.black54),
                        border: InputBorder.none
                    ),
                  ),
                ),
                const SizedBox(width: 15,),
                FloatingActionButton(
                  onPressed: () {
                     messages.add(ChatMessage(messageContent: message.text, messageType: "sender"));
                    setState(() {
                    });
                    message.clear();
                    _scrollToBottom();
                    // FocusManager.instance.primaryFocus?.unfocus();
                  },
                  child: const Icon(Icons.send,color: Colors.white,size: 20,),
                  backgroundColor: myResources.themeColor,
                  elevation: 0,
                ),
              ],

            ),
          ),
        ),
      ],
    );
  }
}

class ChatMessage{
  String messageContent;
  String messageType;
  ChatMessage({required this.messageContent, required this.messageType});
}