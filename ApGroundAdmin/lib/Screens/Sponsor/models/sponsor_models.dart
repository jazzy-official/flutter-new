import 'package:ap_ground_admin/Screens/Payrole/models/payroll_model.dart';

class Sponsor {
  final String? id;
  final String description;
  final String name;
  final String imgUrl;


  Sponsor({this.id,required this.description, required this.name,required this.imgUrl});

// NOTE: implementing functionality here in the next step!

  factory Sponsor.fromJson(dynamic json,String id) {
    return Sponsor(
      id: id.toString(),
      name : json['name'],
      description : json['description'],
      imgUrl: json['imgurl'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = name.toString();
    data['description'] = description;
    data['imgurl'] = imgUrl;
    return data;
  }
}