import 'package:daily_log/Controllers/employees_controller.dart';
import 'package:daily_log/Controllers/projectscontroller.dart';
import 'package:daily_log/Controllers/task_controller.dart';
import 'package:daily_log/Models/module.dart';
import 'package:daily_log/Models/project_model.dart';
import 'package:daily_log/Models/sub_module.dart';
import 'package:daily_log/Models/task_model.dart';
import 'package:daily_log/constants/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:intl/intl.dart';

class AddTask extends StatefulWidget {
  final TaskModel? task;
  const AddTask({Key? key, this.task}) : super(key: key);

  @override
  State<AddTask> createState() => _AddTaskState();
}

class _AddTaskState extends State<AddTask> {
  var employeecont = Get.put(EmployeeController());
  var projectcont = Get.put(ProjectController());
  TextEditingController titlecont = TextEditingController();
  TextEditingController descriptioncont = TextEditingController();
  DateTime? deadline;
  String? assignto;
  String? projectname;
  String? projectId;
  late ProjectModel? project = null;
  Module? module;
  SubModule? submodule;

  @override
  void initState() {
    populatedata();
    super.initState();
  }

  void populatedata() {
    if (widget.task != null) {
      setState(() {
        titlecont.text = widget.task!.name;
        descriptioncont.text = widget.task!.des;
        deadline = widget.task!.deadline;
        assignto = widget.task!.assignto;
        projectname = widget.task!.project;
        projectId = widget.task!.projectid;
        if(widget.task!.projectid!=null) {
          project = projectcont.projects.where((element) => element.id == widget.task!.projectid).single;
        }
        if(widget.task!.moduleid!=null) {
          module = project!.modules.where((element) => element.id == widget.task!.moduleid).single;
        }
        if(widget.task!.submoduleid!=null) {
          submodule = module!.submodules!.where((element) => element.id == widget.task!.submoduleid).single;
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    void _showDialog(Widget child) {
      showCupertinoModalPopup<void>(
          context: context,
          builder: (BuildContext context) => Container(
                height: 216,
                padding: const EdgeInsets.only(top: 6.0),
                // The Bottom margin is provided to align the popup above the system navigation bar.
                margin: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom,
                ),
                // Provide a background color for the popup.
                color: CupertinoColors.systemBackground.resolveFrom(context),
                // Use a SafeArea widget to avoid system overlaps.
                child: SafeArea(
                  top: false,
                  child: child,
                ),
              ));
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyResources.themecolor,
        title: const Text("Add Task"),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: GetBuilder<TaskController>(
          autoRemove: false,
          init: TaskController(),
          builder: (cont) => ListView(
            children: [
              const SizedBox(
                height: 10,
              ),
              SizedBox(
                height: 40,
                child: TextFormField(
                  controller: titlecont,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Title',
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                height: 100,
                child: TextFormField(
                  controller: descriptioncont,
                  maxLines: 4,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    contentPadding: EdgeInsets.only(top: 15, left: 10),
                    labelText: 'Description...',
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: TextButton(
                        style: TextButton.styleFrom(padding: EdgeInsets.zero),
                        onPressed: () => {
                              deadline = deadline ?? DateTime.now(),
                              _showDialog(
                                CupertinoDatePicker(
                                  initialDateTime: deadline ?? DateTime.now(),
                                  mode: CupertinoDatePickerMode.date,
                                  use24hFormat: true,
                                  // This is called when the user changes the date.
                                  onDateTimeChanged: (DateTime newDate) {
                                    setState(() {
                                      deadline = newDate;
                                    });
                                  },
                                ),
                              ),
                            },
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(3),
                              border:
                                  Border.all(color: MyResources.themecolor)),
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                deadline != null
                                    ? DateFormat("dd MMM yyyy")
                                        .format(deadline!)
                                    : "Deadline",
                                style: TextStyle(color: MyResources.themecolor),
                              )
                            ],
                          ),
                        )),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    child: TextButton(
                        style: TextButton.styleFrom(padding: EdgeInsets.zero),
                        onPressed: () => _showDialog(
                              CupertinoPicker(
                                backgroundColor: Colors.white,
                                itemExtent: 30,
                                scrollController:
                                    FixedExtentScrollController(initialItem: 0),
                                children: [
                                  Text("select 1"),
                                  for (var i in employeecont.employees)
                                    Text(i.name),
                                ],
                                onSelectedItemChanged: (value) {
                                  assignto =
                                      employeecont.employees[value - 1].name;
                                  cont.update();
                                },
                              ),
                            ),
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(3),
                              border:
                                  Border.all(color: MyResources.themecolor)),
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                assignto ?? "Assign to",
                                style: TextStyle(color: MyResources.themecolor),
                              )
                            ],
                          ),
                        )),
                  ),
                ],
              ),
              TextButton(
                  style: TextButton.styleFrom(padding: EdgeInsets.zero),
                  onPressed: () => _showDialog(
                        CupertinoPicker(
                          backgroundColor: Colors.white,
                          itemExtent: 30,
                          scrollController:
                              FixedExtentScrollController(initialItem: 0),
                          children: [
                            const Text("select 1"),
                            for (var i in projectcont.projects) Text(i.name),
                          ],
                          onSelectedItemChanged: (value) {
                            setState(() {
                              projectname =
                                  projectcont.projects[value - 1].name;
                              projectId = projectcont.projects[value - 1].id;
                              project = projectcont.projects[value - 1];
                            });

                            cont.update();
                          },
                        ),
                      ),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(3),
                        border: Border.all(color: MyResources.themecolor)),
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          projectname ?? "Project",
                          style: TextStyle(color: MyResources.themecolor),
                        )
                      ],
                    ),
                  )),
              if (project != null)
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Expanded(
                      child: TextButton(
                          style: TextButton.styleFrom(padding: EdgeInsets.zero),
                          onPressed: () => _showDialog(
                                CupertinoPicker(
                                  backgroundColor: Colors.white,
                                  itemExtent: 30,
                                  scrollController: FixedExtentScrollController(
                                      initialItem: 0),
                                  children: [
                                    Text("select 1"),
                                    for (var i in project!.modules)
                                      Text(i.name),
                                  ],
                                  onSelectedItemChanged: (value) {
                                    setState(() {
                                      module = project!.modules[value - 1];
                                    });
                                  },
                                ),
                              ),
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3),
                                border:
                                    Border.all(color: MyResources.themecolor)),
                            padding: EdgeInsets.symmetric(
                                horizontal: 20, vertical: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  module == null ? "Module" : module!.name,
                                  style:
                                      TextStyle(color: MyResources.themecolor),
                                )
                              ],
                            ),
                          )),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Expanded(
                      child: TextButton(
                          style: TextButton.styleFrom(padding: EdgeInsets.zero),
                          onPressed: module == null
                              ? () {
                                  Get.snackbar("Alert", "Choose a Module first",
                                      snackPosition: SnackPosition.BOTTOM);
                                }
                              : () => _showDialog(
                                    CupertinoPicker(
                                      backgroundColor: Colors.white,
                                      itemExtent: 30,
                                      scrollController:
                                          FixedExtentScrollController(
                                              initialItem: 0),
                                      children: [
                                        Text("select 1"),
                                        for (var i in module!.submodules!)
                                          Text(i.name),
                                      ],
                                      onSelectedItemChanged: (value) {
                                        setState(() {
                                          submodule =
                                              module!.submodules![value - 1];
                                        });
                                      },
                                    ),
                                  ),
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3),
                                border:
                                    Border.all(color: MyResources.themecolor)),
                            padding: EdgeInsets.symmetric(
                                horizontal: 20, vertical: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  submodule == null
                                      ? "Sub-Module"
                                      : submodule!.name,
                                  style:
                                      TextStyle(color: MyResources.themecolor),
                                )
                              ],
                            ),
                          )),
                    ),
                  ],
                ),
              SizedBox(
                height: 50,
              ),
              TextButton(
                  style: TextButton.styleFrom(padding: EdgeInsets.zero),
                  onPressed: () async {
                    if (titlecont.text == "") {
                      Get.snackbar("Alert", "Please enter title");
                    } else if (descriptioncont.text == "") {
                      Get.snackbar("Alert", "Please enter description");
                    } else if (deadline == null) {
                      Get.snackbar("Alert", "Please define deadline");
                    } else if (assignto == null) {
                      Get.snackbar("Alert", "Please assign the task");
                    } else {
                      if (widget.task == null) {
                        TaskModel task = TaskModel(
                            project: projectname,
                            projectid: projectId,
                            deadline: deadline!,
                            des: descriptioncont.text,
                            status: false,
                            name: titlecont.text,
                            assignto: assignto,
                            todos: [],
                            module: module?.name,
                            moduleid: module?.id,
                            submodule: submodule?.name,
                            submoduleid: submodule?.id);
                        await cont.addTask(task);
                      } else {
                        TaskModel task = TaskModel(
                            id: widget.task!.id,
                            project: projectname,
                            projectid: projectId,
                            deadline: deadline!,
                            des: descriptioncont.text,
                            status: false,
                            name: titlecont.text,
                            assignto: assignto,
                            todos: [],
                            module: module?.name ?? widget.task?.module,
                            moduleid: module?.id ?? widget.task?.moduleid,
                            submodule:
                                submodule?.name ?? widget.task?.submodule,
                            submoduleid:
                                submodule?.id ?? widget.task?.submoduleid);
                        await cont.updateTask(task);
                      }
                    }
                  },
                  child: Container(
                    height: 40,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3),
                      color: MyResources.themecolor,
                    ),
                    child: const Center(
                        child: Text(
                      "Save",
                      style: TextStyle(color: Colors.white, fontSize: 17),
                    )),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
