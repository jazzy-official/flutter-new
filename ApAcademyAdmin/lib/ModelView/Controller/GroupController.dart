import 'dart:convert';

import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:ap_academy_admin/ModelView/Controller/trainingController.dart';
import 'package:get/get.dart';
import 'package:mongo_dart/mongo_dart.dart';
import 'package:http/http.dart' as http;
import 'package:mongo_dart/mongo_dart.dart' as mongo;

class GroupsControler extends GetxController {
  Rx<List<Map<String, dynamic>>> groupsList =
      Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get group => groupsList.value;

  List<Map<String, dynamic>> userList = <Map<String, dynamic>>[].obs;

  @override
  void onInit() {
    groupsList.bindStream(DatabaseController().groupnamesStream());
    super.onInit();
  }

  final dbH = Get.put(DatabaseController());
  Future<void> getUsers() async {
    userList = (await dbH.getAlluserData())!;

  }

  Future getsheetfun() async {
    await getUsers();
    http.Response response = await http.get(
      Uri.parse('https://sheetdb.io/api/v1/5xeq0u2whmy1p'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
    if (response.statusCode == 200) {
       List<Map<String, dynamic>> _result1SelectMember = [];
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      List<dynamic> sheetData = await jsonDecode(response.body);
      print(sheetData.length);

      for (var data in sheetData) {
        if (data["TRAINING GROUP"].contains("SUNDAY")) {
          // userList.where((element) => element["email"]==data["EMAIL ADDRESS"]).toList();
          userList.forEach((element) {
            if (element["email"] == data["EMAIL ADDRESS"]) {
              _result1SelectMember.add(element);
            }
          });
        }
        print(data);
      }
      Map<String, dynamic> nTeam1 = {
        '_id': mongo.ObjectId(),
        'teamName': "SUNDAY",
        'teamMembers': _result1SelectMember,
      };
      await submitGroup(nTeam1);
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to get payable data');
    }
  }
  Future uploadtraining() async {
    await getUsers();
    http.Response response = await http.get(
      Uri.parse('https://sheetdb.io/api/v1/5xeq0u2whmy1p'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
    if (response.statusCode == 200) {
       List<Map<String, dynamic>> _result1SelectMember = [];
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      List<dynamic> sheetData = await jsonDecode(response.body);
      // print(sheetData.length);

      for (var data in sheetData) {
        if (data["TRAINING GROUP"].contains("SUNDAY")) {
          // userList.where((element) => element["email"]==data["EMAIL ADDRESS"]).toList();
          userList.forEach((element) {
            if (element["email"] == data["EMAIL ADDRESS"]) {
              _result1SelectMember.add(element);
              print(element);
            }
          });
        }
        print(data);
      }
      Map<String, dynamic> trainingModel = Map();
       trainingModel['session'] = "SUNDAY";
       trainingModel['venue'] = "INDIA CLUB";
       trainingModel['time'] = DateTime.now();
       trainingModel['coach'] = "Usman Khan";
       trainingModel['attendance'] = [];
       trainingModel['totalPlayer'] = _result1SelectMember;
       trainingModel['canSchedual'] = true;

       await dbH.createTrain(trainingModel);
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to get payable data');
    }
  }

  Future<void> delGroups(ObjectId id) async {
    await DatabaseController().deletGroup(id).then((value) => refreshGroups());
  }

  refreshGroups() {
    groupsList.bindStream(DatabaseController().groupnamesallStream());
  }

  Future<bool> approveTeams(
      ObjectId id, List t1, List t2, String idTeam1, String idTeam2) async {
    return await dbH.approveMatch(id, t1, t2, idTeam1, idTeam2);
  }

  Future<String?> submitGroup(Map<String, dynamic> t) async {
    return await dbH.addnewGroup(t);
  }

  Future<bool> checkifTeaminMatch(ObjectId id) async {
    return await dbH.checkifMatch(id.id.hexString);
  }

  Future<bool> makeUseractive(ObjectId gid) async {
    return await dbH.makeUseractive(gid);
  }

  Future<bool> makeUserInactive(ObjectId gid) async {
    return await dbH.makeUserInactive(gid);
  }
}
// import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
// import 'package:get/get.dart';
// import 'package:mongo_dart/mongo_dart.dart';
//
// class GroupsControler extends GetxController {
//   Rx<List<Map<String, dynamic>>> groupteamsList =
//       Rx<List<Map<String, dynamic>>>([]);
//   List<Map<String, dynamic>> get groupteams => groupteamsList.value;
//
//   List<Map<String, dynamic>> userList = <Map<String, dynamic>>[].obs;
//   Rx<List<Map<String, dynamic>>> groupsNames = Rx<List<Map<String, dynamic>>>([]);
//   List<Map<String, dynamic>> get groupName => groupsNames.value;
//
//   Rx<List<Map<String, dynamic>>> groupsAllNames =
//       Rx<List<Map<String, dynamic>>>([]);
//   List<Map<String, dynamic>> get groupallName => groupsAllNames.value;
//
//   @override
//   void onInit() {
//     refreshTeams();
//     super.onInit();
//   }
//
//   final dbH = Get.put(DatabaseController());
//   Future<void> getUsers() async {
//     userList = (await dbH.getAlluserData())!;
//   }
//
//   refreshTeams() {
//     this.groupteamsList.bindStream(DatabaseController().groupWiseStream());
//     groupsNames.bindStream(DatabaseController().groupnamesStream());
//     groupsAllNames.bindStream(DatabaseController().groupnamesallStream());
//   }
//
//   Future addNewGrou(String nameofGroup) async {
//     await dbH.addnewGroup({
//       'name': 'Under ' + nameofGroup,
//       'num': int.parse(nameofGroup),
//       'number': 0.0,
//       'isActive': true,
//       'CreationDate': DateTime.now()
//     });
//   }
//
//   Future<bool> delGroup(ObjectId gid) async {
//     try {
//       await dbH.delGroups(gid);
//       return true;
//     } catch (er) {
//       return false;
//     }
//   }
//
//   Future<bool> checkGroupinUse(String id) async {
//     return await dbH.checkGroupinUse(id);
//   }
//
//   Future<bool> makeInactive(ObjectId gid) async {
//     return await dbH.makeInactive(gid);
//   }
//
//   Future<bool> makeactive(ObjectId gid) async {
//     return await dbH.makeactive(gid);
//   }
//
//   Future<bool> makeUseractive(ObjectId gid) async {
//     return await dbH.makeUseractive(gid);
//   }
//
//   Future<bool> makeUserInactive(ObjectId gid) async {
//     return await dbH.makeUserInactive(gid);
//   }
//
//   refreshgroupallNames() async {
//     this.groupsNames.bindStream(DatabaseController().groupnamesallStream());
//   }
//
//   refreshgroupNames() async {
//     groupsAllNames.bindStream(DatabaseController().groupnamesallStream());
//     groupsNames.bindStream(DatabaseController().groupnamesStream());
//   }
//
//   int calculateGroup(String gr) {
//     try {
//       return int.parse(gr.substring(gr.length - 2, gr.length));
//     } catch (er) {
//       print(er);
//       return 0;
//     }
//   }
// }
