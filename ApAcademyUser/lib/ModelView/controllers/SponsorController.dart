import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mongo_dart/mongo_dart.dart';

import 'db_controller.dart';

class SponsorController extends GetxController {
  Rx<List<Map<String, dynamic>>> sponsorList = Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get sponsors => sponsorList.value;

  @override
  void onInit() {
    super.onInit();
  }

  refreshSponsor() {
    sponsorList.bindStream(DatabaseController().sponsorStream());
  }
}
