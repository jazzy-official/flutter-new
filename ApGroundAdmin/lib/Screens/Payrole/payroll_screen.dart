import 'package:ap_ground_admin/Screens/Payrole/payroll_card.dart';
import 'package:ap_ground_admin/Screens/Staff/Models/staff_model.dart';
import 'package:ap_ground_admin/controllers/designation_controller.dart';
import 'package:ap_ground_admin/controllers/staff_controller.dart';
import 'package:ap_ground_admin/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';

class PayRollScreen extends StatefulWidget {
  const PayRollScreen({Key? key}) : super(key: key);

  @override
  _PayRollScreenState createState() => _PayRollScreenState();
}

class _PayRollScreenState extends State<PayRollScreen> {
  late List<Staff> staff;
  List<Staff> _foundResult = [];
  List<Staff> Result1 = [];
  List<String> designations = ["All"];
  String search= "";
  bool isLoading = true;
  void refresh() async {
    setState(() {
      isLoading = true;
    });
    setState(() {
      designations = ["All"];
    });
    staff = await StaffController().readStaff();
    List<Map<String,String>> desg =  await DesignationController().readDesignation();
    desg.forEach((element) {designations.add(element.values.toString());});
    _runDesgFilter("");

    setState(() {
      isLoading = false;
    });
  }
  void _runFilter(String enteredKeyword) {
    List<Staff> results = [];
    if (enteredKeyword.isEmpty) {
      // if the search field is empty or only contains white-space, we'll display all users
      results = Result1;
    } else {
      results = Result1
          .where((user) => user.name
          .toUpperCase()
          .contains(enteredKeyword.toUpperCase()))
          .toList();
      // we use the toLowerCase() method to make it case-insensitive
    }

    // Refresh the UI
    setState(() {
      _foundResult = results;
    });
  }
  void _runDesgFilter(String enteredKeyword) {
    List<Staff> results = [];
    if (enteredKeyword=="All"||enteredKeyword=="") {
      // if the search field is empty or only contains white-space, we'll display all users
      results = staff;
    } else {
      results = staff
          .where((user) => user.designation == enteredKeyword.substring(1,enteredKeyword.length-1))
          .toList();
      // we use the toLowerCase() method to make it case-insensitive
    }

    // Refresh the UI
    setState(() {
      Result1 = results;
    });
    _runFilter(search);
  }

  String dropdownValue = "All";
  @override
  void initState() {
    refresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 5.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 12),
                  height: 45,
                  child: TextFormField(
                    keyboardType: TextInputType.name,
                    onChanged: (val) {
                      setState(() {
                        search=val;
                      });
                      _runFilter(search);
                    },
                    onSaved: (String? val) {}, // ame Controller
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(left: 10),
                      //Add th Hint text here.
                      hintText: "Search",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(right: 10,top: 1),
                child: DropdownButton<String>(
                  value: dropdownValue,
                  icon: const Icon(Ionicons.chevron_down),
                  iconSize: 18,
                  elevation: 10,
                  style: const TextStyle(color: Colors.deepPurple),
                  // underline: Container(
                  //   height: 2,
                  //   color: Colors.deepPurpleAccent,
                  // ),
                  onChanged: (String? newValue) {
                    _runDesgFilter(newValue!);
                    setState(() {
                      dropdownValue = newValue;
                    });
                  },
                  items: designations
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: isLoading
              ? LoadingWidget()
              : ListView.builder(
              physics: BouncingScrollPhysics(
                  parent: AlwaysScrollableScrollPhysics()),
              padding: const EdgeInsets.all(8),
              itemCount: _foundResult.length,
              itemBuilder: (BuildContext context, int index) {
                return PayrollCard(
                  staff: _foundResult[index],
                  refresh: refresh,
                );
              }),
        ),
      ],
    );
  }
}
