import 'package:flutter_clean_calendar/clean_calendar_event.dart';
import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:get/get.dart';
import 'package:ap_academy_admin/screens/dashboard_screen.dart';

class CalController extends GetxController {
  final dbHelp = Get.put(DatabaseController());
  Map<DateTime, List<Meeting>> tEvent = Map<DateTime, List<Meeting>>();

  @override
  Future<void> onInit() async {
    print("init");
    // await getTrainingEvents();
    super.onInit();
  }

  Future<List<Map<String,dynamic>>> getTrainingEvents() async {
    print("getTrainingEvents");
    var tEvents = await dbHelp.getTraining();

    return tEvents;

  }
}
