
import 'package:ap_ground_admin/Screens/Grounds/Models/playing_slot_model.dart';

class Slot {
  DateTime startTime;
  double price;
  int hours;
  PlayingSlot? Pslot1;
  PlayingSlot? Pslot2;

  Slot({required this.startTime, required this.price, required this.hours,this.Pslot1,this.Pslot2});

// NOTE: implementing functionality here in the next step!

  factory Slot.fromJson(dynamic json) {
    return Slot(
      startTime : DateTime.parse(json['startTime']),
      price : double.parse(json['price'].toString()),
      hours : int.parse(json['hours'].toString()),
      Pslot1:json['Pslot1']!=null? PlayingSlot.fromJson(json['Pslot1']):null,
      Pslot2: json['Pslot2']!=null? PlayingSlot.fromJson(json['Pslot2']):null,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['startTime'] = this.startTime.toString();
    data['price'] = this.price;
    data['hours'] = this.hours;
    data['Pslot1'] = Pslot1!=null?Pslot1!.toJson():null;
    data['Pslot2'] = Pslot2!=null?Pslot2!.toJson():null;
    return data;
  }
}