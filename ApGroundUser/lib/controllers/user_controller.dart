import 'dart:convert';

import 'package:ap_booking_user/Screens/Authentication/Models/user_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'firebase_controllers.dart';
import 'package:firebase_database/firebase_database.dart';

class UserController {
  final _auth = FirebaseAuth.instance;
  final DatabaseReference databaseReference =
      FirebaseDatabase.instance.reference().child("Users");

  Future<void> saveUser(UserModel user) async {
    try {
      await databaseReference.push().set(user.toJson());
    } catch (e) {
      displayToast(e.toString());
      print(e.toString());
    }
  }

  Future<void> updateUser(UserModel user) async {
    databaseReference.update({user.id!: user.toJson()});
  }

  Future<void> deleteUser(String id) async {
    databaseReference.child(id).remove();
  }

  Future<UserModel> readData(String uid) async {
    late UserModel user;
    await databaseReference
        .orderByChild("uid")
        .equalTo(uid)
        .once()
        .then((value) => {
              if (value.value != null)
                {
                  Map.of(value.value).forEach((key, value) {
                    user = UserModel.fromJson(value, key);
                  })
                }
              else
                {
                  user = UserModel(
                      name: _auth.currentUser!.displayName!,
                      email: _auth.currentUser!.email!,
                      imgUrl: _auth.currentUser!.photoURL!,
                      uid: _auth.currentUser!.uid,
                      role: "admin"),
                }
            });

    return user;
  }



}
