import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:intl/intl.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Expenses/Models/expense_model.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Expenses/expense_card.dart';
import 'package:http/http.dart' as http;
import 'package:rafi_admin/Controllers/expense_controller.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../loader_widget.dart';
class ExpensePage extends StatefulWidget {

  ExpensePage({Key? key,}) : super(key: key);

  @override
  State<ExpensePage> createState() => _ExpensePageState();
}

class _ExpensePageState extends State<ExpensePage> {
  var formatter = NumberFormat('#,##,000');



  @override
  initState() {
    // at the beginning, all users are shown

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body:GetBuilder<ExpenseController>(
          autoRemove: false,
          init: ExpenseController(),
          builder: (cont) => cont.expenses==null ? LoadingWidget() :  Column(
            children: [
              const SizedBox(
                height: 10,
              ),
              Container(
                width: MediaQuery.of(context).size.width - 30,
                // (MediaQuery.of(context).size.width * 1) - 90,
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8),
                    boxShadow: const [
                      BoxShadow(
                          color: Colors.black26,
                          blurRadius: 5,
                          offset: Offset(0, 0.5))
                    ]),
                height: 45,
                child: TextFormField(
                  onChanged: (val) {
                    cont.runFilter(val);
                  },
                  style: const TextStyle(
                    color: Colors.black87,
                  ),
                  decoration: const InputDecoration(
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.only(
                        top: 14,
                      ),
                      prefixIcon: Icon(
                        Icons.search,
                        color: Color(0xff7F7C82),
                      ),
                      hintText: 'Search',
                      hintStyle: TextStyle(
                        color: Colors.black38,
                      )),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Expanded(
                child:RefreshIndicator(
                  onRefresh: () async {
                    return await cont.refreshAll();
                  },
                  child: ListView.builder(
                    physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                    itemCount: cont.foundResult.length,
                    itemBuilder: (context, i) {
                      final expense = cont.foundResult[i];
                      return ExpenseCard(expense: expense,);
                    },
                  ),
                ),
              ),
            ],
          ),
        ),);

  }
}