import 'dart:async';

import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:ap_academy_admin/ModelView/Controller/db_cred.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:mongo_dart/mongo_dart.dart';

class TrainingController extends GetxController {
  Rx<List<Map<String, dynamic>>> trainingsList =
      Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get training => trainingsList.value;
  RxString selectedCoachValue = ''.obs;
  RxList<Map<String, dynamic>> coachdropValues =
      <Map<String, dynamic>>[].obs;

  final dbHelp = Get.put(DatabaseController());
  @override
  Future<void> onInit() async {
    //await getTeamDropDown();
    await resetSchedules();
    super.onInit();
  }

  Future delTraining(ObjectId id) async {
    await DatabaseController()
        .deletTraining(id)
        .then((value) => refreshTraining());
  }

  refreshTraining() {
    resetSchedules();
    this.trainingsList.bindStream(DatabaseController().trainingStream());
  }

  Future<void> resetSchedules() async {
    DateTime temp = DateTime.now();
    List<Map<String, dynamic>>? scheduled = await dbHelp.getScheduled();

    for (int i = 0; i < scheduled!.length; i++) {
      DateTime valEnd = scheduled[i]['time']; //.add(Duration(days: 7));
      DateTime date = DateTime.now();
      bool valDate = date.isBefore(valEnd);
      print(valDate);
      if (!valDate) {
        DateTime newDate = scheduled[i]['time'].add(Duration(days: 7));
        await dbHelp.updateScheduleDate(scheduled[i]['_id'], newDate);
        this.trainingsList.bindStream(DatabaseController().trainingStream());
      }

      // print(DateFormat.yMMMMEEEEd().format(scheduled[i]['time'])); //
    }
  }

  Future getTeamDropDown() async {
    try {
      await dbHelp.getAllCoachesData().then((value) {
        if (value != null) {
          value.forEach((element) {
            coachdropValues.add(element);
          });
          selectedCoachValue = value[0]['firebaseId'].obs;
        }
      });
    } catch (ed) {
      print(ed.toString());
    }
  }
}
