class ReportModel {
  final DateTime date;
  final int? inc;


  ReportModel({required this.date,  this.inc,});

// NOTE: implementing functionality here in the next step!

  factory ReportModel.fromJson(dynamic json) {
    return ReportModel(
      date : DateTime.parse(json['date']),
      inc : int.parse(json['inc'].toString()),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this.date.toString();
    data['inc'] = this.inc;
    return data;
  }
}