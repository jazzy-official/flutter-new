import 'package:ap_academy_admin/ModelView/Controller/NotificationController.dart';
import 'package:ap_academy_admin/ModelView/Controller/trainingController.dart';
import 'package:ap_academy_admin/screens/Events/trainings_details.dart';
import 'package:ap_academy_admin/screens/shared/DeletPop.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:get/get.dart';
import 'package:get/route_manager.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import 'createTraining.dart';

class TrainingScreen extends StatefulWidget {
  @override
  _TrainingScreenState createState() => _TrainingScreenState();
}

class _TrainingScreenState extends State<TrainingScreen> {
  final tC = Get.put(TrainingController());

  @override
  void initState() {
    tC.refreshTraining();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyResources.backgroundColor,
      body: Material(
        child: SingleChildScrollView(
          child: Container(
            height: Get.height* 0.8,
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "",
                        style: MyResources.appHeadingStyle,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        width: Get.width * 0.2,
                        height: 25,
                        child: RaisedButton(
                          textColor: MyResources.buttontextColor,
                          color: MyResources.buttonColor,
                          child: Text(
                            "ADD",
                          ),
                          onPressed: () {
                            Get.to(
                              () => CreateTraining(),
                            );
                          },
                          shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  height: Get.height * 0.7,
                  child: GetX<TrainingController>(
                    init: Get.put<TrainingController>(TrainingController()),
                    builder: (TrainingController trainingController) {
                      if (trainingController != null &&
                          trainingController.training != null) {
                        return Stack(children: [
                          // Center(
                          //   child: ColorFiltered(
                          //       colorFilter: ColorFilter.mode(
                          //           Colors.white.withOpacity(0.3),
                          //           BlendMode.dstATop),
                          //       child: Image.asset(
                          //         'assets/notraining.png',
                          //         colorBlendMode: BlendMode.hue,
                          //         width: 200,
                          //         height: 200,
                          //       )),
                          // ),
                          ListView.builder(
                            itemCount: trainingController.training.length,
                            itemBuilder: (BuildContext context, int index) =>
                                getTrainingCard(
                                    trainingController.training[index], index),
                          ),
                        ]);
                      } else {
                        return Container(
                          child: LoadingWidget(),
                        );
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
    // );
  }

  Widget getTrainingCard(Map<String, dynamic> trainingModel, int i) {
    return Padding(
      padding: const EdgeInsets.only(top: 4.0, left: 8, right: 8),
      child: Stack(
          children: [
            Container(
      //constraints: BoxConstraints(minHeight: 200),
      width: Get.width,
      //create the cards for all details of event happening
      child: InkWell(
        onTap: () {
          Get.dialog(TrainingDetails(
            trainingModel: trainingModel,
          ));},
        child: Card(
          color: MyResources.backgroundColor,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top:18.0),
                  child: Center(
                    child: Text(trainingModel['session'],
                        style: MyResources.appHeadingStyle),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: Text("Coach:",
                                  style: MyResources.titleTextStyle),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 6.0),
                              child: Text("Venue:",
                                  style: MyResources.titleTextStyle),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 6.0),
                              child: Text("Time:",
                                  style: MyResources.titleTextStyle),
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(top: 8.0, left: 9),
                              child: Container(
                                width: Get.width * 0.4,
                                child: Text(trainingModel['coach'],
                                    overflow: TextOverflow.ellipsis,
                                    style: MyResources.nameTextStyle),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 6.0, left: 9),
                              child: Container(
                                width: Get.width * 0.4,
                                child: Text(
                                  trainingModel['venue'],
                                  overflow: TextOverflow.ellipsis,
                                  softWrap: true,
                                  style: MyResources.nameTextStyle,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 9.0, left: 9),
                              child: Container(
                                width: Get.width * 0.4,
                                child: Text(
                                  DateFormat("dd-MMM-yyyy - hh:mm a")
                                      .format(trainingModel['time'].toLocal()),
                                  style: TextStyle(
                                      color: MyResources.headingColor,
                                      fontSize: 12),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 8.0),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.person,
                              color: Colors.orange,
                            ),
                            Text(
                              "${trainingModel['attendance'].length}/" +
                                  "${trainingModel['totalPlayer'].length}",
                              style: GoogleFonts.roboto(fontSize: 13),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  // mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconButton(
                      tooltip: "Delete",
                            onPressed: () async {
                              Get.dialog(Center(
                                  child: DeletpopUp(
                                message: 'Are you sure you want to delete?',
                                pressFunc: () async {
                                  Get.dialog(LoadingWidget());

                                  final otherEventController =
                                      Get.put(TrainingController());
                                  await otherEventController
                                      .delTraining(
                                        trainingModel['_id'],
                                      )
                                      .then((value) => Get.back());
                                  Get.back();
                                },
                              )));
                            },
                            icon: Icon(
                              Icons.dangerous,
                              color: Colors.red[300],
                            ),
                          ),
                          Container(
                            width: 30,
                            child: IconButton(
                              onPressed: () {
                                Get.to(
                                  CreateTraining(
                                    trainingModel1: trainingModel,
                                  ),
                                );
                              },
                              icon: Icon(
                                Icons.edit,
                                color: Colors.green,
                              ),
                            ),
                          ),
                          Container(
                            width: 30,
                            child: IconButton(
                              onPressed: () async {
                                List<String> trainingusers = [];
                                trainingModel['totalPlayer'].forEach((element) {
                                  trainingusers.add(element['token']);
                                  trainingusers.add(element['parentToken']);
                                });

                                await Get.find<NotificationController>()
                                    .sendAndRetrieveMessage({
                                  'title': 'Reminder',
                                  'desc': 'You have training'
                                }, trainingusers);

                                Get.snackbar("Notification Sent",
                                    "Reminder notification has been sent ",
                                    snackPosition: SnackPosition.BOTTOM);
                              },
                              icon: Icon(
                                Icons.notifications,
                                size: 20,
                                color: Colors.orange,
                              ),
                            ),
                          ),
                    Container(
                      width: 30,
                      child: IconButton(
                        onPressed: () async {
                          List<String> trainingusers = [];
                          trainingModel['totalPlayer'].forEach((element) {
                            trainingusers.add(element['token']);
                            trainingusers.add(element['parentToken']);
                          });

                          await Get.find<NotificationController>()
                              .sendAndRetrieveMessage({
                            'title': 'Reminder',
                            'desc': 'Your training for this week has been cancelled'
                          }, trainingusers);

                          Get.snackbar("Notification Sent",
                              "Reminder notification has been sent ",
                              snackPosition: SnackPosition.BOTTOM);
                        },
                        icon: Icon(
                          Icons.warning_rounded,
                          size: 20,
                          color: Colors.red,
                        ),
                      ),
                    ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          trainingModel['canSchedual']
              ? Positioned(
            top: 8,
            right: 4,
            child: Container(
                color: Colors.red,
                width: 65,
                height: 16,
                child: Padding(
                  padding: const EdgeInsets.only(top: 1.0, bottom: 1.0,),
                  child: Center(
                    child: Text('Scheduled', style: TextStyle(color: Colors.white, fontSize: 12)
                    ),
                  ),
                )
            ),
          )
              : Container(),
        ],
      ),
    );
  }
}
