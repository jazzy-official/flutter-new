import 'package:ap_academy_new/ModelView/controllers/notification_controller.dart';
import 'package:ap_academy_new/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_new/Constants/util.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import 'notifDetails.dart';

class NotificationScreen extends StatefulWidget {
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  @override
  Widget build(BuildContext context) {
    return Material(
        //Create an event add button
        child: GetX<NotificationController>(
            init: Get.put<NotificationController>(NotificationController()),
            builder: (NotificationController notifyController) {
              if (notifyController != null && notifyController.notify != null) {
                return ListView.builder(
                  itemCount: notifyController.notify.length,
                  itemBuilder: (BuildContext context, int index) =>
                      getNotificationCard(notifyController.notify[index]),
                );
              } else {
                return Container(
                  child: LoadingWidget(),
                );
              }
            }));
  }

  Widget getNotificationCard(Map<String, dynamic> notificationModel) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        // height: 130,
        //create the cards for all details of event happening
        child: InkWell(
          onTap: () {
            Get.dialog(NotifyDetails(
              notificationModel: notificationModel,
            ));
          },
          child: Card(
            elevation: 4,
            color: myResources.backgroundColor,
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 5, left: 10, right: 10),
                    child: Icon(
                      Icons.notification_important_sharp,
                      color: myResources.drawerIconColor,
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        constraints: BoxConstraints(maxWidth: Get.width * 0.6),
                        width: Get.width * 0.7,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(bottom: 4.0),
                              child: Container(
                                width: Get.width*0.8,
                                child: Text(notificationModel['title'],
                                    overflow: TextOverflow.fade,
                                    style: myResources.appHeadingStyle),
                              ),
                            ),
                            Text(notificationModel['description'],
                                overflow: TextOverflow.ellipsis,
                                maxLines: 3,
                                softWrap: false,
                                style: TextStyle(fontSize: 12,
                                  color: Colors.black,)),
                          ],
                        ),
                      ),
                      SizedBox(height: 10,),
                      Text(
                          DateFormat.Hm()
                              .add_yMEd()
                              .format(notificationModel['timeCreated']),
                          style: TextStyle(
                              fontSize: 12,
                              color: Colors.black54,
                            //contentPadding: EdgeInsets.all(20.0),
                          )
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
