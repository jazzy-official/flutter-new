import 'dart:io';
import 'package:numberpicker/numberpicker.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:mongo_dart/mongo_dart.dart';
import "package:images_picker/images_picker.dart";

import 'databaseController.dart';

class PhotoAlbumController extends GetxController {
  final dbHelper = Get.put(DatabaseController());
  RxList<File> photos =  <File>[].obs;
  RxList<File> videos = <File>[].obs;
  Rx<List<Map<String, dynamic>>> albums = Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get album => albums.value;

//////////////////////////////////////////////////////////////////
  @override
  void onInit() {
    // albums.bindStream(dbHelper.getAlbumData());

    super.onInit();
  }

  refreshData() {
    albums.bindStream(dbHelper.getAlbumData());
  }

  Future<bool> geT() async {
    String error;
    try {

      List<Media>? res = await ImagesPicker.pick(
        count: 10,
        pickType: PickType.image,
      );
      res!.forEach((element) {
        photos.add(File(element.path));
     });

      print(photos.toString());
      return true;
    } on PlatformException catch (e) {
      error = e.toString();
      print(error);
      return false;
    } catch (e) {
      print("[AP Sports Error]" + e.toString());
      photos.clear();
      return false;
    }
  }
  Future<bool> pickImages() async {
    String error;
    try {
      print("hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh");
      // photos.clear();
      FilePickerResult? result = await FilePicker.platform.pickFiles(
        type: FileType.custom,
        allowMultiple: true,
        allowedExtensions: ['jpg', 'png'],
      );
      result!.paths.forEach((element) {
        photos.add(File(element!));
      });

      return true;
    } on PlatformException catch (e) {
      error = e.toString();
      print(error);
      return false;
    } catch (e) {
      print("[AP Sports Error]" + e.toString());
      photos.clear();
      return false;
    }
  }

  List<dynamic> imageUrls = [], vidUrls = [];

  Future multiplePicUpload(String name, String des) async {
    imageUrls = [];
    vidUrls = [];
    Map<String, dynamic> albumMap;
    if (photos.length > 0) {
      for(int v=0;v<photos.length;v++)
        {
          imageUrls.add(await uploadPics(photos[v]));
        }
    }
    if (videos.length > 0)
      for (int v = 0; v < videos.length; v++) {
        vidUrls.add(await uploadVid(videos[v]));
      }

    albumMap = {
      'albumName': name,
      'desciption': des,
      'albumPhotos': imageUrls,
      'albumVideos': vidUrls,
      'CreatedAt': DateTime.now()
    };
    await saveImageListtoDB(albumMap);
  }
  Future editAlbum(String name, String des,ObjectId id,List<dynamic> photo) async {
    imageUrls = photo;
    vidUrls = [];
    Map<String, dynamic> albumMap;
    if (photos.length > 0) {
      for(int v=0;v<photos.length;v++)
        {
          imageUrls.add(await uploadPics(photos[v]));
        }
    }

    albumMap = {
      'albumName': name,
      'desciption': des,
      'albumPhotos': imageUrls,
      'albumVideos': vidUrls,
      'CreatedAt': DateTime.now()
    };
    await editalbumindatabase(albumMap,id);
  }
  //////////////////////////////////////////////////////////////////

  RxDouble count = 0.0.obs;
///////////////////////////////////////////////////////////////////////
  Future<String> uploadPics(File element) async {
    final DateTime now = DateTime.now();
    final int millSeconds = now.millisecondsSinceEpoch;
    final String month = now.month.toString();
    final String date = now.day.toString();
    final String storageId =
    (millSeconds.toString() + FirebaseAuth.instance.currentUser!.uid);
    final String today = ('$month-$date');
    final Reference storageReference = FirebaseStorage.instance
        .ref()
        .child("Album")
        .child("Images")
        .child(today)
        .child(storageId);

    final UploadTask uploadTask = storageReference.putFile(
        element, SettableMetadata(contentType: 'image/jpeg'));

    await uploadTask.whenComplete(() {});
    return await storageReference.getDownloadURL();
  }

////////////////////////////////////////////////
  Future saveImageListtoDB(
    Map<String, dynamic> vLink,
  ) async {
    await dbHelper.addAlbum(vLink);
  }
  Future editalbumindatabase(
    Map<String, dynamic> vLink,ObjectId id
  ) async {
    await dbHelper.editAlbum(vLink,id);
  }

  Future pickvideos() async {
    try {
      videos.clear();
      FilePickerResult? result = await FilePicker.platform.pickFiles(
        type: FileType.video,
        allowMultiple: true,
       // allowedExtensions: ['mp4', 'mkv'],
      );
      result!.paths.forEach((element) {
        videos.add(File(element!));
      });

      return true;
    } catch (error) {
      print(error);
      //  Get.defaultDialog(title: "", content: Text(error.toString()));
      return null;
    }
  }

  Future uploadVid(File file) async {
    String? url;
    final DateTime now = DateTime.now();
    final int millSeconds = now.millisecondsSinceEpoch;
    final String month = now.month.toString();
    final String date = now.day.toString();
    final String storageId =
        (millSeconds.toString() + FirebaseAuth.instance.currentUser!.uid);
    final String today = ('$month-$date');
    Reference ref = FirebaseStorage.instance
        .ref()
        .child("Album")
        .child("video")
        .child(today)
        .child(storageId);
    UploadTask uploadTask = ref.putFile(
        file,
        SettableMetadata(
            contentType: 'video/mp4')); //<- this content type does the trick
    await uploadTask.whenComplete(() async {
      url = await uploadTask.snapshot.ref.getDownloadURL();
    });

    print(url);

    return url;
  }

////////////////////////////////////////////////
  Future saveVideoToDb(
    String vLink,
  ) async {
    await dbHelper.addVideoPathToDatabase(vLink);
  }

  uploadupdatedPic(File file) async {
    String? url;
    final DateTime now = DateTime.now();
    final int millSeconds = now.millisecondsSinceEpoch;
    final String month = now.month.toString();
    final String date = now.day.toString();
    final String storageId =
        (millSeconds.toString() + FirebaseAuth.instance.currentUser!.uid);
    final String today = ('$month-$date');
    Reference ref = FirebaseStorage.instance
        .ref()
        .child("Album")
        .child("Images")
        .child(today)
        .child(storageId);
    UploadTask uploadTask = ref.putFile(
      file,
    ); //<- this content type does the trick
    await uploadTask.whenComplete(() async {
      url = await uploadTask.snapshot.ref.getDownloadURL();
    });

    print(url);

    return url;
  }

  updatenewPhoto(ObjectId _id, List<String> imgurl) async {
    await dbHelper.updatenewPhoto(_id, imgurl).then((value) {
      if (value) {
        print("Updated");
      } else
        print("Not Updated");
    });
  }

//////////////////////////////////////////////////////////////////
  Future<bool> delAlbumn(ObjectId uid, List<dynamic> link) async {
    return await dbHelper.deletAlbumn(uid, link);
  }
}
