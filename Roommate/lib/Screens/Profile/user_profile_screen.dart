import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Screens/Home/roommate_detail.dart';

import '../../controllers/profile_controller.dart';

class ProfileScreen extends StatelessWidget {
   ProfileScreen({Key? key}) : super(key: key);

  var pcont = Get.put(ProfileController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: ListView(
          children: [
            SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                     Text(
                      pcont.user!.name,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                    SizedBox(height: 5,),
                    Text(
                      pcont.user!.email,
                      style:
                          TextStyle(fontSize: 14, color: Colors.grey.shade600),
                    ),
                    TextButton(
                        onPressed: () {},
                        style: TextButton.styleFrom(
                          padding: EdgeInsets.zero
                        ),
                        child: Row(
                          children: [
                            Text(
                              "Edit profile ",
                              style: TextStyle(
                                  color: myResources.themeColor,
                                  fontWeight: FontWeight.w600),
                            ),
                            Icon(
                              Icons.edit,
                              size: 18,
                              color: myResources.themeColor,
                            ),
                          ],
                        ))
                  ],
                ),
                CircleAvatar(
                  radius: 40,
                  backgroundImage: AssetImage(pcont.user!.profilePic),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15.0),
              child: Divider(height: 2,color: Colors.grey.shade600,),
            ),
            InkWell(
              onTap: (){
                Get.to(RoommateDetailScreen(id: pcont.user!.id,));
              },
              child: Row(
                children: [
                  Icon(Ionicons.person_circle_outline,color: Colors.grey.shade600,),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10.0),
                    child: Text("View public profile",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                  ),
                  Expanded(child: SizedBox()),
                  Icon(Ionicons.chevron_forward,color: Colors.grey.shade600,)
                ],
              ),
            ),
            SizedBox(height: 20,),
            Row(
              children: [
                Icon(Ionicons.settings_outline,color: Colors.grey.shade600,),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Text("Settings",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                ),
                Expanded(child: SizedBox()),
                Icon(Ionicons.chevron_forward,color: Colors.grey.shade600,)
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15.0),
              child: Divider(height: 2,color: Colors.grey.shade600,),
            ),
            Row(
              children: [
                Icon(Ionicons.send_outline,color: Colors.grey.shade600,),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Text("Invite a friend",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                ),
                Expanded(child: SizedBox()),
                Icon(Ionicons.chevron_forward,color: Colors.grey.shade600,)
              ],
            ),
            SizedBox(height: 20,),
            Row(
              children: [
                Icon(Icons.star_border,color: Colors.grey.shade600,),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Text("Rate us on app store",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                ),
                Expanded(child: SizedBox()),
                Icon(Ionicons.chevron_forward,color: Colors.grey.shade600,)
              ],
            ),
            SizedBox(height: 20,),
            Row(
              children: [
                Icon(Ionicons.mail_unread_outline,color: Colors.grey.shade600,),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Text("Send feedback",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                ),
                Expanded(child: SizedBox()),
                Icon(Ionicons.chevron_forward,color: Colors.grey.shade600,)
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15.0),
              child: Divider(height: 2,color: Colors.grey.shade600,),
            ),
            Row(
              children: [
                Icon(Ionicons.shield_outline,color: Colors.grey.shade600,),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Text("Privacy",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                ),
                Expanded(child: SizedBox()),
                Icon(Ionicons.chevron_forward,color: Colors.grey.shade600,)
              ],
            ),
            SizedBox(height: 20,),
            Row(
              children: [
                Icon(Icons.support_agent_outlined,color: Colors.grey.shade600,),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Text("Help & support",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                ),
                Expanded(child: SizedBox()),
                Icon(Ionicons.chevron_forward,color: Colors.grey.shade600,)
              ],
            ),
            SizedBox(height: 20,),
            Row(
              children: [
                Icon(Ionicons.document_text_outline,color: Colors.grey.shade600,),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Text("Terms & conditions",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                ),
                Expanded(child: SizedBox()),
                Icon(Ionicons.chevron_forward,color: Colors.grey.shade600,)
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 16.0,bottom: 5),
              child: Divider(height: 2,color: Colors.grey.shade600,),
            ),
            TextButton(
                onPressed: (){},
                style: TextButton.styleFrom(
                  padding: EdgeInsets.zero
                ),
                child: Row(
                  children: [
                    Icon(Ionicons.log_out_outline,color: myResources.themeColor,),
                    SizedBox(width: 10,),
                    Text("Log out",style:TextStyle(color: myResources.themeColor,fontWeight: FontWeight.w600) ,),
                  ],
                )
            )
          ],
        ),
      ),
    );
  }
}
