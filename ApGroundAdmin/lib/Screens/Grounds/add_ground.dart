import 'package:ap_ground_admin/constants/util.dart';
import 'package:bouncing_widget/bouncing_widget.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'dart:math';
import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:ap_ground_admin/controllers/template_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:ionicons/ionicons.dart';
import 'package:path/path.dart' as Path;
import '../../loading_widget.dart';
import 'Models/ground_models.dart';
import 'Models/slot_model.dart';
import 'Models/template_model.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:ap_ground_admin/controllers/ground_database_controller.dart';

class AddGroundForm extends StatefulWidget {
  final Ground? ground;
  const AddGroundForm({Key? key, this.ground}) : super(key: key);

  @override
  State<AddGroundForm> createState() => _AddGroundFormState();
}

class _AddGroundFormState extends State<AddGroundForm> {
  String name = "";
  String address = "";
  double mealPrice = 0;
  List<String> imgUrls = [];
  DateTime? _chosenDateTime = DateTime.now();
  int? hours = 0;
  double? price = 0;
  List<Template> schedual = [
    Template(name: "Mon", slots: []),
    Template(name: "Tue", slots: []),
    Template(name: "Wed", slots: []),
    Template(name: "Thu", slots: []),
    Template(name: "Fri", slots: []),
    Template(name: "Sat", slots: []),
    Template(name: "Sun", slots: []),
  ];
  List<Template> templates = [];

  Future<File> urlToFile(String imageUrl) async {
// generate random number.
    var rng = new Random();
// get temporary directory of device.
    Directory tempDir = await getTemporaryDirectory();
// get temporary path from temporary directory.
    String tempPath = tempDir.path;
// create a new file in temporary path with random file name.
    File file = new File('$tempPath' + (rng.nextInt(100)).toString() + '.png');
// call http.get method and pass imageUrl into it to get response.
    http.Response response = await http.get(Uri.parse(imageUrl));
// write bodyBytes received in response to file.
    await file.writeAsBytes(response.bodyBytes);
// now return the file which is created with random name in
// temporary directory and image bytes from response is written to // that file.
    return file;
  }

  void checkeditadd() {
    if (widget.ground != null) {
      setState(() {
        name = widget.ground!.name;
        address = widget.ground!.address;
        schedual = widget.ground!.schedule;
        imgUrls = widget.ground!.imgUrls;
        mealPrice = widget.ground!.mealPrice;
      });
    }
  }

  void loadtemplates() async {
    List<Template> temp = await TemplateController().readTemplate();
    setState(() {
      templates = temp;
    });
  }

  void _showDatePicker(ctx) {
    showCupertinoModalPopup(
        context: ctx,
        builder: (_) => Container(
              height: 353,
              color: Color.fromARGB(255, 255, 255, 255),
              child: Column(
                children: [
                  Container(
                    height: 300,
                    child: CupertinoDatePicker(
                        initialDateTime: DateTime.now(),
                        mode: CupertinoDatePickerMode.time,
                        onDateTimeChanged: (val) {
                          setState(() {
                            _chosenDateTime = val;
                          });
                          print(_chosenDateTime);
                        }),
                  ),

                  // Close the modal
                  CupertinoButton(
                    child: Text('OK'),
                    onPressed: () => Navigator.of(ctx).pop(),
                  )
                ],
              ),
            ));
  }

  List<File> _image = [];
  final picker = ImagePicker();
  @override
  void initState() {
    loadtemplates();
    checkeditadd();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(bottom: 30),
          child: Stack(
            children: [
              Positioned(
                left: 15,
                top: 30,
                child: BackButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
              Form(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 80, bottom: 0, left: 10, right: 10),
                      child: Container(
                        decoration: BoxDecoration(
                            border:
                                Border.all(color: Colors.black, width: 1)),
                        width: MediaQuery.of(context).size.width,
                        child: (GridView.builder(
                          shrinkWrap: true,
                          itemCount: _image.length + 1,
                          gridDelegate:
                              const SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 3),
                          itemBuilder: (context, index) {
                            return index == 0
                                ? Center(
                                    child: IconButton(
                                      onPressed: () {
                                        chooseImage();
                                      },
                                      icon: const Icon(
                                        Icons.add_a_photo,
                                        size: 35,
                                      ),
                                    ),
                                  )
                                : Stack(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.all(3),
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: FileImage(
                                                  _image[index - 1]),
                                              fit: BoxFit.cover),
                                        ),
                                      ),
                                      Positioned(
                                          right: 5,
                                          child: IconButton(
                                            icon: const Icon(
                                              Ionicons.close_circle_outline,
                                              color: Colors.redAccent,
                                            ),
                                            onPressed: () {
                                              _image.removeAt(index - 1);
                                              setState(() {});
                                            },
                                          ))
                                    ],
                                  );
                          },
                        )),
                      ),
                    ),
                    Padding(
                      //Add padding around textfield
                      padding:
                          const EdgeInsets.only(top: 10.0, left: 10, right: 10),
                      child: SizedBox(
                        height: 40,
                        child: TextFormField(
                          initialValue: name,
                          keyboardType: TextInputType.name,
                          onChanged: (val) {
                            setState(() {
                              name = val;
                            });
                          },
                          onSaved: (String? val) {}, // ame Controller
                          decoration: InputDecoration(
                            //Add th Hint text here.
                            hintText: "Name",
                            contentPadding: const EdgeInsets.only(top: 10,left: 10),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      //Add padding around textfield
                      padding: const EdgeInsets.only(
                          top: 15.0, left: 10, right: 10, bottom: 10),
                      child: SizedBox(
                        height: 70,
                        child: TextFormField(
                          maxLines: 3,
                          initialValue: address,
                          keyboardType: TextInputType.name,
                          onChanged: (val) {
                            setState(() {
                              address = val;
                            });
                          },
                          onSaved: (String? val) {}, // ame Controller
                          decoration: InputDecoration(
                            //Add th Hint text here.
                            hintText: "Address",
                            contentPadding: EdgeInsets.only(top: 10,left: 10),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      //Add padding around textfield
                      padding: const EdgeInsets.only(
                          top: 5.0, left: 10, right: 10, bottom: 15),
                      child: SizedBox(
                        height: 40,
                        child: TextFormField(
                          initialValue: mealPrice.toString(),
                          keyboardType: TextInputType.name,
                          onChanged: (val) {
                            setState(() {
                              mealPrice = double.parse(val);
                            });
                          },
                          onSaved: (String? val) {}, // ame Controller
                          decoration: InputDecoration(
                            //Add th Hint text here.
                            hintText: "Meal Price",
                            contentPadding: const EdgeInsets.only(top: 10,left: 10),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 7),
                      child: Column(
                        children: [
                          const Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Text(
                              "Weekly Slots Schedule",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                          ),
                          DefaultTabController(
                              length: 7, // length of tabs
                              initialIndex: 0,
                              child: Column(
                                  children: <Widget>[
                                    TabBar(
                                      padding: EdgeInsets.zero,
                                      labelPadding:
                                          const EdgeInsets.symmetric(horizontal: 3),
                                      // indicatorPadding: EdgeInsets.zero,
                                      indicatorSize:
                                          TabBarIndicatorSize.label,
                                      indicator: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(50),
                                          color: MyResources.buttonColor
                                              .withOpacity(0.45)),
                                      labelColor: Colors.white,
                                      unselectedLabelColor: Colors.black,
                                      isScrollable: true,
                                      tabs: [
                                        Tab(
                                          child: Container(
                                            width: 48,
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(50),
                                                border: Border.all(
                                                    color: schedual[0].slots.isEmpty
                                                        ? Colors.redAccent
                                                        : Colors.green,
                                                    width: 1)),
                                            child: const Align(
                                              alignment: Alignment.center,
                                              child: Text("Mon"),
                                            ),
                                          ),
                                        ),
                                        Tab(
                                          child: Container(
                                            width: 48,
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(50),
                                                border: Border.all(
                                                    color: schedual[1].slots.isEmpty
                                                        ? Colors.redAccent
                                                        : Colors.green,
                                                    width: 1)),
                                            child: const Align(
                                              alignment: Alignment.center,
                                              child: Text("Tue"),
                                            ),
                                          ),
                                        ),
                                        Tab(
                                          child: Container(
                                            width: 48,
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(50),
                                                border: Border.all(
                                                    color: schedual[2].slots.isEmpty
                                                        ? Colors.redAccent
                                                        : Colors.green,
                                                    width: 1)),
                                            child: const Align(
                                              alignment: Alignment.center,
                                              child: Text("Wed"),
                                            ),
                                          ),
                                        ),
                                        Tab(
                                          child: Container(
                                            width: 48,
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(50),
                                                border: Border.all(
                                                    color: schedual[3].slots.isEmpty
                                                        ? Colors.redAccent
                                                        : Colors.green,
                                                    width: 1)),
                                            child: const Align(
                                              alignment: Alignment.center,
                                              child: Text("Thu"),
                                            ),
                                          ),
                                        ),
                                        Tab(
                                          child: Container(
                                            width: 48,
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(50),
                                                border: Border.all(
                                                    color: schedual[4].slots.isEmpty
                                                        ? Colors.redAccent
                                                        : Colors.green,
                                                    width: 1)),
                                            child: const Align(
                                              alignment: Alignment.center,
                                              child: Text("Fri"),
                                            ),
                                          ),
                                        ),
                                        Tab(
                                          child: Container(
                                            width: 48,
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(50),
                                                border: Border.all(
                                                    color: schedual[5].slots.isEmpty
                                                        ? Colors.redAccent
                                                        : Colors.green,
                                                    width: 1)),
                                            child: const Align(
                                              alignment: Alignment.center,
                                              child: Text("Sat"),
                                            ),
                                          ),
                                        ),
                                        Tab(
                                          child: Container(
                                            width: 48,
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(50),
                                                border: Border.all(
                                                    color: schedual[6].slots.isEmpty
                                                        ? Colors.redAccent
                                                        : Colors.green,
                                                    width: 1)),
                                            child: const Align(
                                              alignment: Alignment.center,
                                              child: Text("Sun"),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 10,),
                                    SizedBox(
                                      height: 270,
                                      child: TabBarView(
                                          children: <Widget>[
                                            slotList(schedual[0], (List<Slot> tem) => setState(() => schedual[0].slots = tem),),
                                            slotList(schedual[1], (List<Slot> tem) => setState(() => schedual[1].slots = tem),),
                                            slotList(schedual[2], (List<Slot> tem) => setState(() => schedual[2].slots = tem),),
                                            slotList(schedual[3], (List<Slot> tem) => setState(() => schedual[3].slots = tem),),
                                            slotList(schedual[4], (List<Slot> tem) => setState(() => schedual[4].slots = tem),),
                                            slotList(schedual[5], (List<Slot> tem) => setState(() => schedual[5].slots = tem),),
                                            slotList(schedual[6], (List<Slot> tem) => setState(() => schedual[6].slots = tem),),
                                          ]),
                                    )
                                  ])),
                        ],
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(top: 10.0, left: 10, right: 10),
                      child: TextButton(
                          onPressed: () async {
                            await uploadfile();
                            if (name == '') {
                              displayToast("Please enter ground name");
                            } else if (address == '') {
                              displayToast("Please enter adsress");
                            } else if (
                                schedual[0].slots.isEmpty ||
                                schedual[1].slots.isEmpty ||
                                schedual[2].slots.isEmpty ||
                                schedual[3].slots.isEmpty ||
                                schedual[4].slots.isEmpty ||
                                schedual[5].slots.isEmpty ||
                                schedual[6].slots.isEmpty) {
                              displayToast(
                                  "Please enter complete weekly schedule");
                            } else {
                              showDialog(
                                  context: context,
                                  builder: (context) {
                                    return  const LoadingWidget();


                                  });
                              if (widget.ground == null) {
                                final Ground ground = Ground(
                                  name: name,
                                  address: address,
                                  imgUrls: imgUrls,
                                  schedule: schedual,
                                  mealPrice: mealPrice,
                                );
                                await GroundController().saveGround(ground);
                                Navigator.pop(context);
                                Navigator.pop(context);
                                displayToast("Ground Added Successfully");
                              } else {
                                final Ground ground = Ground(
                                  id: widget.ground!.id,
                                  name: name,
                                  address: address,
                                  imgUrls: imgUrls,
                                  schedule: schedual, mealPrice: mealPrice,
                                );
                                await GroundController()
                                    .updateGround(ground)
                                    .then((value) => {
                                          Navigator.pop(context),
                                          displayToast(
                                              "Ground Updated Successfully"),
                                          Navigator.pop(context),
                                          Navigator.pop(context),
                                        });
                              }
                            }
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.9,
                            height: 45,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.indigo),
                            child: const Center(
                                child: Text(
                              "Save",
                              style: TextStyle(color: Colors.white),
                            )),
                          )),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget slotForm(Template day, ValueChanged<Template> onChangedTemplate) {
    int value = 0;
    return Padding(
        //Add padding around textfield
        padding: const EdgeInsets.only(top: 15.0, left: 7, right: 7),
        child: Column(
          children: [
            const Text(
              " Add Slot",
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TextButton(
                    onPressed: () {
                      _showDatePicker(context);
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.indigo),
                      child: Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Text(
                          _chosenDateTime != null
                              ? _chosenDateTime.toString().substring(10, 16)
                              : "Start time",
                          style: const TextStyle(color: Colors.white),
                        ),
                      ),
                    )),
                SizedBox(
                  width: 100,
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    onChanged: (val) {
                      setState(() {
                        hours = int.parse(val);
                      });
                    },
                    decoration: InputDecoration(
                      //Add th Hint text here.
                      hintText: "Shift Hours",
                      contentPadding: const EdgeInsets.only(left: 10),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                ),
                Container(
                  width: 100,
                  margin: const EdgeInsets.symmetric(horizontal: 10),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    onChanged: (val) {
                      setState(() {
                        price = double.parse(val);
                      });
                    },
                    decoration: InputDecoration(
                      //Add th Hint text here.
                      hintText: "Price",
                      contentPadding: const EdgeInsets.only(left: 10),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              children: [
                TextButton(
                    onPressed: () {
                      day.slots.add(Slot(
                          startTime: _chosenDateTime!,
                          price: price!,
                          hours: hours!));
                      setState(() {});
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.25,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.indigo),
                      child: const Padding(
                        padding: EdgeInsets.symmetric(vertical: 15),
                        child: Center(
                            child: Text(
                          "Add",
                          style: TextStyle(color: Colors.white),
                        )),
                      ),
                    )),
                TextButton(
                    onPressed: () async {
                      showCupertinoModalPopup(
                          context: context,
                          builder: (_) => Container(
                                height: 353,
                                color: Color.fromARGB(255, 255, 255, 255),
                                child: Column(
                                  children: [
                                    Container(
                                      height: 300,
                                      child: CupertinoPicker(
                                          children: [
                                            Text("Cancel"),
                                            for (var tem in templates)
                                              Text(tem.name)
                                          ],
                                          onSelectedItemChanged: (val) {
                                            setState(() {
                                              value = val;
                                            });
                                          },
                                          itemExtent: 25,
                                          diameterRatio: 1,
                                          useMagnifier: true,
                                          magnification: 1.3,
                                          squeeze: 2),
                                    ),

                                    // Close the modal
                                    CupertinoButton(
                                      child: const Text('OK'),
                                      onPressed: () => {
                                        if (value != 0)
                                          {
                                            onChangedTemplate(
                                                templates[value - 1]),
                                          },
                                        Navigator.of(context).pop(),
                                      },
                                    )
                                  ],
                                ),
                              ));
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.65 - 29,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.indigo),
                      child: const Padding(
                        padding: EdgeInsets.symmetric(vertical: 15),
                        child: Center(
                            child: Text(
                          "Add template",
                          style: TextStyle(color: Colors.white),
                        )),
                      ),
                    )),
              ],
            ),
          ],
        ));
  }

  chooseImage() async {
    try {
      final pickfile = await picker.pickImage(source: ImageSource.gallery);
      setState(() {
        _image.add(File(pickfile!.path));
      });
      if (pickfile == null) retrieveLostData();
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> retrieveLostData() async {
    final LostDataResponse response = await picker.retrieveLostData();
    if (response.isEmpty) {
      return;
    }
    if (response.file != null) {
      setState(() {
        _image.add(File(response.file!.path));
      });
    } else {
      print(response.file);
    }
  }

  Future uploadfile() async {
    for (var img in _image) {
      Reference ref = FirebaseStorage.instance
          .ref()
          .child('images/${Path.basename(img.path)}');
      UploadTask uploadTask = ref.putFile(img);
      await uploadTask.whenComplete(() async {
        print('File Uploaded');
        await ref.getDownloadURL().then((value) {
          imgUrls.add(value);
          print(imgUrls.length);
        });
      });
    }
  }

  Widget slotList(Template day, ValueChanged<List<Slot>> onChangedTemplate) {
    int value = 0;
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(3),
            color: Colors.grey.shade300,
          ),
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(flex: 1, child: Center(child: Text("Slots"))),
              Expanded(flex: 3, child: Center(child: Text("Price"))),
              Expanded(flex: 3, child: Center(child: Text("Start Time"))),
              Expanded(flex: 2, child: Center(child: Text("Hours"))),
              Expanded(
                  flex: 1,
                  child: Center(
                      child: BouncingWidget(
                          child: Container(
                            padding: EdgeInsets.symmetric(horizontal:6),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: MyResources.buttonColor,
                            ),
                              child: Text(
                            "+",
                            style: TextStyle(fontSize: 20,color: Colors.white),
                          )),
                        onPressed: () {
                          day.slots.add(Slot(
                              startTime: DateTime.now(),
                              price: 0,
                              hours: 0,));
                          setState(() {});
                        },))),
            ],
          ),
        ),
        Container(
          height: 200,
          child: day.slots.isEmpty
              ? Center(
                  child: IconButton(
                      onPressed: () async {
                        showCupertinoModalPopup(
                            context: context,
                            builder: (_) => Container(
                                  height: 353,
                                  color: Color.fromARGB(255, 255, 255, 255),
                                  child: Column(
                                    children: [
                                      Container(
                                        height: 300,
                                        child: CupertinoPicker(
                                            children: [
                                              Text("Cancel"),
                                              for (var tem in templates)
                                                Text(tem.name)
                                            ],
                                            onSelectedItemChanged: (val) {
                                              setState(() {
                                                value = val;
                                              });
                                            },
                                            itemExtent: 25,
                                            diameterRatio: 1,
                                            useMagnifier: true,
                                            magnification: 1.3,
                                            squeeze: 2),
                                      ),

                                      // Close the modal
                                      CupertinoButton(
                                        child: Text('OK'),
                                        onPressed: () async => {
                                          loadtemplates(),
                                          if (value != 0)
                                            {
                                              onChangedTemplate(
                                                  templates[value - 1].slots),
                                            },
                                          Navigator.of(context).pop(),
                                        },
                                      )
                                    ],
                                  ),
                                ));
                      },
                      icon: Icon(Icons.add)),
                )
              : ListView.builder(
                  shrinkWrap: true,
                  itemCount: day.slots.length,
                  itemBuilder: (BuildContext context, int index) {
                    Slot slot = day.slots[index];
                    return Container(
                      height: 50,
                      decoration: BoxDecoration(
                        color: const Color(0xfff1f2f8),
                        border: Border(bottom: BorderSide(color: Colors.black54,width:1)),
                      ),
                      
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                              flex: 1,
                              child: Center(
                                  child: Text((index + 1).toString()))),
                          Expanded(
                              flex: 3,
                              child: Center(
                                  child: TextFormField(
                                keyboardType: TextInputType.name,
                                initialValue: formatnmbr(slot.price),
                                onChanged: (val) {
                                  slot.price = double.parse(val);
                                },
                                onSaved: (String? val) {}, // ame Controller
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.only(left: 10),
                                  //Add th Hint text here.
                                  border: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.circular(5.0),
                                  ),
                                ),
                              ))),
                          Expanded(
                              flex: 3,
                              child: Center(
                                  child:TextButton(
                                    style: TextButton.styleFrom(padding: EdgeInsets.zero),
                                      onPressed: () {
                                        showCupertinoModalPopup(
                                            context: context,
                                            builder: (_) => Container(
                                              height: 353,
                                              color: Color.fromARGB(255, 255, 255, 255),
                                              child: Column(
                                                children: [
                                                  Container(
                                                    height: 300,
                                                    child: CupertinoDatePicker(
                                                        initialDateTime: DateTime.now(),
                                                        mode: CupertinoDatePickerMode.time,
                                                        onDateTimeChanged: (val) {
                                                          setState(() {
                                                            slot.startTime = val;
                                                          });
                                                        }),
                                                  ),

                                                  // Close the modal
                                                  CupertinoButton(
                                                    child: Text('OK'),
                                                    onPressed: () => Navigator.of(context).pop(),
                                                  )
                                                ],
                                              ),
                                            ));
                                      },
                                      child: Container(
                                        padding: EdgeInsets.symmetric(horizontal: 10,vertical: 8.5),
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(5),
                                            color: Colors.indigo),
                                        child: Text(
                                            DateFormat('hh:mm a').format(slot.startTime),
                                            style: TextStyle(color: Colors.white),
                                        ),
                                      )))),
                          Expanded(
                              flex: 2,
                              child: Center(
                                  child: TextFormField(
                                    keyboardType: TextInputType.number,
                                    initialValue: slot.hours.toString(),
                                    onChanged: (val) {
                                      slot.hours = int.parse(val);
                                    },
                                    onSaved: (String? val) {}, // ame Controller
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.only(left: 10),
                                      //Add th Hint text here.
                                      border: OutlineInputBorder(
                                        borderRadius:
                                        BorderRadius.circular(5.0),
                                      ),
                                    ),
                                  ))),
                          Expanded(
                              flex: 1,
                              child: IconButton(
                                padding: EdgeInsets.all(0),
                                onPressed: () async {
                                  await day.slots.removeAt(index);
                                  onChangedTemplate(day.slots);
                                },
                                icon: Icon(
                                  Icons.delete,
                                  color: Colors.red,
                                  size: 22,
                                ),
                              )),
                        ],
                      ),
                    );
                  }),
        ),
      ],
    );
  }
}
