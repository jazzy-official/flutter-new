import 'package:daily_log_user/Controllers/profilecontroller.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import '../Models/sub_module.dart';
import '../Models/task_model.dart';
import '../loading_widget.dart';


class TaskController extends GetxController {
  List<TaskModel> tasks = [];
  List<TaskModel> pendingtasks = [];
  final DatabaseReference databaseReference =
  FirebaseDatabase.instance.ref("Tasks");

  var profileController = Get.put(ProfileController());
  String? id;
  String title = "";
  String description = "";
  DateTime? deadline ;
  String? assignto;

  @override
  Future<void> onInit() async {
    // TODO: implement onInit
    readBall();
    super.onInit();
  }


  Future<void> addTask(task) async {
    Get.defaultDialog(title: '', content: const LoadingWidget(),backgroundColor: Colors.transparent,barrierDismissible: false);

    try{
          await databaseReference.push().set(task.toJson());
          await readBall();
          Get.back();
          Get.back();

          Get.snackbar("Done", "Task added");
        }catch(e){
          Get.snackbar("Error", e.toString());
        }
      await readBall();
  }

  Future<void> updateTask(TaskModel ball) async {
    databaseReference.update({
      ball.id! : ball.toJson()
    });
    update();
    readBall();
  }


  // Future<void> deleteTask(String id) async {
  //   databaseReference.child(id).remove();
  //
  // }

  Future<void> readBall() async {
    List<TaskModel> ball = [];
    await databaseReference.once().then(( data) {

      if(data.snapshot.value != null){
        Map<dynamic,dynamic> values = data.snapshot.value as Map<dynamic,dynamic>;
        values.forEach((key, value) {
          TaskModel gr = TaskModel.fromJson(value,key);
          if(gr.assignto == profileController.userDetail.name) {
            ball.add(gr);
          }
        });
      }

    });
    tasks = ball;
    pendingtasks = tasks.where((element) => element.status == false).toList();
    update();
  }

  Future<SubModule?> getSubModule(String projectid,String moduleid,String submoduleId) async {
    SubModule submodule;
    final DatabaseReference Reference =
    FirebaseDatabase.instance.ref("Projects/$projectid/modules/$moduleid/submodules/$submoduleId");
    try {
      var data = await Reference.once();
      submodule = SubModule.fromJson(data.snapshot.value, data.snapshot.key);
      print(submodule);
      return submodule;
    }catch(e){
      Get.snackbar("Done", e.toString(),snackPosition: SnackPosition.BOTTOM,);
      return null;

    }

  }


}