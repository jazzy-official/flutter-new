import 'package:ap_academy_admin/ModelView/Controller/NotificationController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'Notificationtab.dart';
import 'newstab.dart';

class AnnouncementScreen extends StatefulWidget {
  @override
  _AnnouncementScreenState createState() => _AnnouncementScreenState();
}

class _AnnouncementScreenState extends State<AnnouncementScreen> {
  @override
  void initState() {
    Get.find<NotificationController>().refreshDate();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            toolbarHeight: 10,
            backgroundColor: MyResources.buttonColor,
            bottom: TabBar(tabs: [
              Tab(
                text: 'NOTIFICATIONS',
              ),
              Tab(
                text: 'NEWS',
              ),
            ]),
          ),
          body: TabBarView(
            children: [
              NotificationScreen(),
              NewsScreen(),
            ],
          ),
        ));
  }
}
