import 'package:flutter/material.dart';
import 'package:rafi_admin/Authentication/login.dart';
import 'package:rafi_admin/Components/Navigation/navigation_bar.dart';
import 'package:rafi_admin/Components/loader_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Wrapper extends StatefulWidget {
  Wrapper({Key? key}) : super(key: key);

  @override
  State<Wrapper> createState() => _WrapperState();
}

class _WrapperState extends State<Wrapper> {
  bool isLoading = false;
  bool _isLogin = false;
  _checkLogin() async {
    setState(() {
      isLoading = true;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isLogin = (prefs.get('token') == "null" || prefs.get('token') == null ? false : true);

    setState(() {
      _isLogin = isLogin;
    });
    setState(() {
      isLoading = false;
    });

  }
  @override
  void initState() {
    _checkLogin();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return isLoading ?  const Center(child: LoadingWidget()) :
   _isLogin ? const Navigation() : loginScreen();

  }
}
