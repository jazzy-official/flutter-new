import 'package:ap_academy_admin/ModelView/Controller/CoachController.dart';
import 'package:ap_academy_admin/ModelView/Controller/profileController.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:ap_academy_admin/screens/shared/DeletPop.dart';
import 'package:url_launcher/url_launcher.dart';

class DetailAdmincard extends StatefulWidget {
  final Map<String, dynamic> detailData;

   DetailAdmincard({Key? key, required this.detailData}) : super(key: key);

  @override
  State<DetailAdmincard> createState() => _DetailAdmincardState();
}

class _DetailAdmincardState extends State<DetailAdmincard> {
  final profileController = Get.put(ProfileController());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 10, right: 10),
      child: Center(
        child: Container(
          height: Get.height * 0.6,
          child: Card(
            color: MyResources.backgroundColor,
            child: Stack(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 10.0, right: 10, left: 10),
                      child: Center(
                        child: Container(
                          height: 40,
                          width: Get.width,
                          color: MyResources.buttonColor,
                          child: Center(
                            child: Text(
                              'DETAILS',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: widget.detailData['photoUrl'] != ""? Image.network(
                          widget.detailData['photoUrl'],
                          width: 130,
                          height: 130,
                          fit: BoxFit.cover,
                        ):Container(),
                      ),
                    ),
                    Row(
                      children: [
                        Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 20,
                                right: 20,
                                left: 20,
                              ),
                              child: Container(
                                width: 100,
                                child: Text(
                                  'Name :',
                                  style: MyResources.titleTextStyle,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 20,
                                right: 10,
                                left: 10,
                              ),
                              child: Container(
                                width: 100,
                                child: Text(
                                  'Email :',
                                  style: MyResources.titleTextStyle,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 20,
                                right: 10,
                                left: 10,
                              ),
                              child: Container(
                                width: 100,
                                child: Text(
                                  'Designation :',
                                  style: MyResources.titleTextStyle,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 20,
                                right: 10,
                                left: 10,
                              ),
                              child: Container(
                                width: 100,
                                child: Text(
                                  'Phone :',
                                  style: MyResources.titleTextStyle,
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 20,
                                right: 10,
                                left: 10,
                              ),
                              child: Container(
                                width: 100,
                                child: Text(
                                  'Status :',
                                  style: MyResources.titleTextStyle,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          width: 190,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 20,
                                  right: 10,
                                  left: 10,
                                ),
                                child: Container(
                                  width: 190,
                                  child: Text(
                                    widget.detailData['name'],
                                    style: TextStyle(
                                        color: MyResources.buttonColor,
                                        fontSize: 16),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 20,
                                  right: 10,
                                  left: 10,
                                ),
                                child: Container(
                                  width: 300,
                                  height: 20,
                                  child: Text(
                                    widget.detailData['email'].toString(),
                                    style: TextStyle(
                                        color: MyResources.buttonColor,
                                        fontSize: 16),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 20,
                                  right: 10,
                                  left: 10,
                                ),
                                child: Container(
                                  width: 190,
                                  child: Text(
                                    widget.detailData['designation'].toString(),
                                    softWrap: false,
                                    style: TextStyle(
                                        color: MyResources.buttonColor,
                                        fontSize: 16),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  top: 20,
                                  right: 10,
                                  left: 10,
                                ),
                                child: GestureDetector(
                                  onTap: () => launch("tel://"+widget.detailData['phone'].toString(),),
                                  child: Container(
                                    width: 190,
                                    child: Text(
                                      widget.detailData['phone'].toString()==""?"N/A":widget.detailData['phone'].toString(),
                                      softWrap: false,
                                      style: TextStyle(
                                          color: MyResources.buttonColor,
                                          fontSize: 16),
                                    ),
                                  ),
                                ),
                              ),
                              if(profileController.userDetail['role']=="SuperAdmin"||(profileController.userDetail['role']=="Admin"&&widget.detailData['role']=="Coach"))
                                Switch(
                                  value: widget.detailData['isActive'],
                                  onChanged: (value) {
                                    widget.detailData['isActive']
                                        ? Get.dialog(Center(
                                        child: DeletpopUp(
                                            message: 'Make this user inactive?',
                                            pressFunc: () async {
                                              await Get.find<CoachController>()
                                                  .makeAdminInactive(widget.detailData['_id'])
                                                  .then((value) {});
                                              Get.back();
                                              Get.find<CoachController>()
                                                  .refreshcoach();
                                              Get.find<CoachController>().refreshcoach();
                                              Get.back();

                                              if (mounted) setState(() {});
                                            },
                                            isNotDel: true)))
                                        : Get.dialog(Center(
                                        child: DeletpopUp(
                                            message: 'Make this user active?',
                                            pressFunc: () async {
                                              await Get.find<CoachController>()
                                                  .makeAdminactive(widget.detailData['_id'])
                                                  .then((value) {
                                                Get.find<CoachController>()
                                                    .refreshcoach();
                                                Get.find<CoachController>()
                                                    .refreshcoach();
                                                Get.back();
                                                if (mounted) setState(() {});
                                              });
                                              Get.back();
                                            },
                                            isNotDel: true)));
                                  },
                                  activeTrackColor: Colors.lightGreenAccent,
                                  activeColor: Colors.green,
                                )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                // Align(
                //   alignment: Alignment.bottomLeft,
                //   child: Padding(
                //     padding: const EdgeInsets.only(
                //         top: 10, right: 10, left: 10, bottom: 10),
                //     child: Container(
                //       width: 200,
                //       child: Text(
                //         DateFormat.yMEd().add_jm().format(detailData['dob']),
                //         style: TextStyle(color: Colors.grey),
                //       ),
                //     ),
                //   ),
                // ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
