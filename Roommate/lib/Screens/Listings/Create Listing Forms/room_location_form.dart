import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:room_buddy/Constants/Widgets/info_widget.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Screens/Listings/Create%20Listing%20Forms/room_overview_form.dart';
import 'package:room_buddy/controllers/my_listing_controller.dart';

class RoomLocationForm extends StatelessWidget {
   RoomLocationForm({Key? key}) : super(key: key);

  var mylistingcont = Get.put(MyListingController());
  TextEditingController citycontroller = TextEditingController();
  TextEditingController localitycontroller = TextEditingController();
  TextEditingController addresscontroller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
          backgroundColor: Colors.white,
          iconTheme: const IconThemeData(color: Colors.black),
          title: Text("Step 1: Enter Location",style: TextStyle(color: Colors.black),),
          bottom: PreferredSize(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 22),
              child: LinearProgressIndicator(
                color: myResources.themeColor,
                backgroundColor: Colors.grey.shade300,
                minHeight: 6,
                value: 1 / 6,
              ),
            ),
            preferredSize: const Size.fromHeight(50),
          )),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              "City",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
            ),
            Padding(
              //Add padding around textfield
              padding: const EdgeInsets.only(top: 15.0, bottom: 20),
              child: SizedBox(
                height: 35,
                child: TextField(
                  keyboardType: TextInputType.name,
                  onChanged: (val) {},
                  controller: citycontroller,
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    hintText: "Enter a City",
                    prefixIcon: const Icon(
                      Ionicons.search_outline,
                      size: 22,
                    ),
                    contentPadding: const EdgeInsets.only(top: 10, left: 15),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(3.0),
                    ),
                  ),
                ),
              ),
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(3)),
                    side: BorderSide(color: myResources.themeColor)),
              ),
              onPressed: () {
                // Get.to(()=> const SignupFormTwo());
              },
              child: Container(
                padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.my_location_outlined,
                      color: myResources.themeColor,
                      size: 20,
                    ),
                    Text(
                      '  Set Current Location',
                      style: TextStyle(color: myResources.themeColor),
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            const Text(
              "Locality",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
            ),
            Padding(
              //Add padding around textfield
              padding: const EdgeInsets.only(top: 15.0, bottom: 20),
              child: SizedBox(
                height: 35,
                child: TextField(
                  controller: localitycontroller,
                  keyboardType: TextInputType.name,
                  onChanged: (val) {},
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    // hintText: "Enter a City",
                    // prefixIcon: const Icon(Ionicons.search_outline,size: 22,),
                    contentPadding: const EdgeInsets.only(top: 10, left: 15),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(3.0),
                    ),
                  ),
                ),
              ),
            ),
            const Text(
              "Street Address",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
            ),
            Padding(
              //Add padding around textfield
              padding: const EdgeInsets.only(top: 15.0, bottom: 20),
              child: SizedBox(
                height: 35,
                child: TextField(
                  keyboardType: TextInputType.name,
                  controller: addresscontroller,
                  onChanged: (val) {},
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    // hintText: "Enter a City",
                    // prefixIcon: const Icon(Ionicons.search_outline,size: 22,),
                    contentPadding: const EdgeInsets.only(top: 10, left: 15),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(3.0),
                    ),
                  ),
                ),
              ),
            ),
            Expanded(child: SizedBox()),
             const InfoWidget(
               text:
                   "Your complete address won't be shared. Complete address can only be shard after you accept the chat request.",
             ),
            Expanded(child: SizedBox()),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: myResources.themeColor,
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(14))),
              ),
              onPressed: () {
                if(citycontroller.text == ""){
                  Get.snackbar("Warning", "Please enter a city name",snackPosition: SnackPosition.BOTTOM);
                }else if(localitycontroller==""){
                  Get.snackbar("Warning", "Please enter a locality",snackPosition: SnackPosition.BOTTOM);
                } else if(addresscontroller.text==""){
                  Get.snackbar("Warning", "Please enter street address",snackPosition: SnackPosition.BOTTOM);
                }else {
                  mylistingcont.city = citycontroller.text;
                  mylistingcont.locality = localitycontroller.text;
                  mylistingcont.address = addresscontroller.text;

                    Get.to(RoomOverviewForm());

                }

              },
              child: Container(
                padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    SizedBox(width: 20,),
                    Text(
                      'Proceed',
                      style: TextStyle(color: Colors.white),
                    ),
                    Container(
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(20)),
                        color: myResources.themeColor,
                      ),
                      child: Icon(
                        Icons.arrow_forward_ios,
                        color: Colors.white,
                        size: 16,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
