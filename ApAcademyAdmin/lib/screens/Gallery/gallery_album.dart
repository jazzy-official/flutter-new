import 'package:ap_academy_admin/ModelView/Controller/AlbumController.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:ap_academy_admin/screens/Gallery/addphotos_videos_inalbum.dart';
import 'package:ap_academy_admin/screens/Gallery/photos_tabs.dart';
import 'package:ap_academy_admin/screens/Gallery/videos_tabs.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class GalleryAlbum extends StatefulWidget {
  final int? i;

  const GalleryAlbum({Key? key, this.i}) : super(key: key);
  @override
  _GalleryAlbumState createState() => _GalleryAlbumState();
}

class _GalleryAlbumState extends State<GalleryAlbum> {
  final galeyController = Get.put(PhotoAlbumController());
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyResources.buttonColor,
        actions: [
          IconButton(onPressed: (){
            Get.to(EditAlbum(album: galeyController.album[widget.i!],));
          }, icon: Icon(Icons.edit))
        ],
        title: Container(
          child: Text(
            'ALBUMS',
            style: GoogleFonts.roboto(),
          ),
        ),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Container(
            height: 600,
            child: DefaultTabController(
                length: 1,
                child: Scaffold(
                  appBar: AppBar(
                    toolbarHeight: 0,
                    backgroundColor: MyResources.buttonColor,
                    bottom: TabBar(tabs: [
                      Tab(
                        text: 'PHOTOS',
                      ),
                      // Tab(
                      //   text: 'VIDEOS',
                      // ),
                    ]),
                  ),
                  body: TabBarView(
                    children: [
                      PhotosTab(i: widget.i!),
                      // VideosTab(i: widget.i!),
                    ],
                  ),
                )),
          ),
        ],
      ),
    );
  }
}
