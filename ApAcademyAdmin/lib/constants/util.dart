import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MyResources {
  //static Color primaryColor = Color(0xfff5f5f5);
  static Color backgroundColor = Color(0xfff5f5f5);
  static Color appBarActionsColor = Colors.white;
  static const Color headingColor = Colors.orange;
  static Color coachHeading = Color(0xff3c4da9);
  static Color drawerIconColor = Color(0xff1758B4); // Color(0xfffe0000);
  static Color buttonColor = Color(0xff1758B4);
  static Color sideBarColor = Color(0xfff5f5f5);
  static Color buttontextColor = Color(0xffebebeb);
  static Color hintColor = Color(0xffd2d2d2);
  static TextStyle vsStyle = GoogleFonts.monoton(
    color: Colors.red,
    fontSize: 20,
    fontWeight: FontWeight.w500,
  );
  static TextStyle titleTextStyle = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.bold,
    color: Colors.black,
  );
  static TextStyle nameTextStyle = TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.w300,
    color: Color(0xff1758B4),
  );
  static TextStyle appTextStyle = TextStyle(
    fontSize: 14,
    color: Colors.black,
  );
  static TextStyle appTextStyle2 = TextStyle(
    fontSize: 14,
    color: Colors.black,
    fontWeight: FontWeight.bold,
  );
  static TextStyle appHeadingStyle = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w600,
  );
  static TextStyle timestampStyle = TextStyle(
    fontSize: 12,
    color: Colors.black54,
  );
  static TextStyle modelHeadingStyle = TextStyle(
    fontSize: 20,
    color: Colors.white,
    fontWeight: FontWeight.w700,
  );

  static TextStyle linkColor =
      GoogleFonts.roboto(fontSize: 14, color: Colors.blue);

  static TextStyle hintfontStyle =
      GoogleFonts.roboto(fontSize: 14, color: hintColor);
  static TextStyle textStyleprofile = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w400,
  );
  static TextStyle myTextStyle = GoogleFonts.roboto(
    fontSize: 16,
    //fontWeight: FontWeight.bold,
  );

  ///////////////////////Input Decoration////////////////
  static InputDecoration textFieldBorder = InputDecoration(
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(10.0),
    ),
  );
  static BoxDecoration roundedBoxDecore = BoxDecoration(
      color: backgroundColor,
      border: Border.all(color: Colors.transparent),
      borderRadius: BorderRadius.circular(10));
  static BoxDecoration roundeddropdown = BoxDecoration(
      color: backgroundColor,
      border: Border.all(color: Colors.grey),
      borderRadius: BorderRadius.circular(10));
}
