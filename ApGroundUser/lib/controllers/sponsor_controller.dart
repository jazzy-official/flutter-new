import 'package:ap_booking_user/Screens/Sponsor/models/sponsor_models.dart';
import 'package:firebase_database/firebase_database.dart';

import 'firebase_controllers.dart';


class SponsorController {
  final DatabaseReference databaseReference =
  FirebaseDatabase.instance.reference().child("Sponsor");

  Future<void> saveSponsor(Sponsor sponsor) async {
    try{ await databaseReference.push().set(sponsor.toJson());
    }catch(e){
      displayToast(e.toString());
    }
  }

  Future<void> updateSponsor(Sponsor sponsor) async {
    databaseReference.update({
      sponsor.id! : sponsor.toJson()
    });
  }



  Future<void> deleteSponsor(String id) async {
    databaseReference.child(id).remove();

  }

  Future<List<Sponsor>> readSponsor() async {
    List<Sponsor> sponsor = [];
    await databaseReference.once().then((DataSnapshot snapshot) {
      print("im in");
      if(snapshot.value != null){
        snapshot.value.forEach((key, value) {
          Sponsor gr = Sponsor.fromJson(value,key);
          sponsor.add(gr);
        });
      }

    });
    return sponsor;
  }

}
