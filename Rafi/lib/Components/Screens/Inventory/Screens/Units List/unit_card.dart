import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:rafi_admin/Components/Screens/Inventory/Models/Unit%20Model/unit_model.dart';

class UnitCard extends StatelessWidget {
  final UnitModel unit;
  UnitCard({Key? key, required this.unit}) : super(key: key);
  var formatter = NumberFormat('#,##,000');
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 5.0,right: 5),
      child: Card(
        elevation: 4,
        child: InkWell(
            splashColor: Colors.blue.withAlpha(50),
            onTap: () { if(unit.buyerName!= null) {
              showDialog<String>(
                context: context,
                builder: (BuildContext context) => AlertDialog(
                  title: const Text('Details'),
                  content: Container(
                    height: 140,
                    width: MediaQuery.of(context).size.width,
                    child: Column(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text('Buyers Name : ',style: TextStyle(fontWeight: FontWeight.bold),),
                            Expanded(child: Text(unit.buyerName!)),
                          ],
                        ),
                        if(unit.affiliateName != null) Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text('Affiliate Name : ',style: TextStyle(fontWeight: FontWeight.bold),),
                            Expanded(child: Text(unit.affiliateName!)),
                          ],
                        ),
                        Row(
                          children: [
                            const Text('Ammount : ',style: TextStyle(fontWeight: FontWeight.bold),),
                            Text(formatter.format(unit.totalamount),),
                          ],
                        ),
                        Row(
                          children: [
                            const Text('Contract Date : ',style: TextStyle(fontWeight: FontWeight.bold),),
                            Expanded(child: Text(unit.contractDate.toString().substring(
                                0,
                                min(unit.contractDate!.length, 10)))),
                          ],
                        ),
                      ],
                    ),
                  ),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () => Navigator.pop(context,),
                      child: const Text('Close'),
                    ),
                  ],
                ),
              );
            }
            },
            child: Container(
                padding: EdgeInsets.all(10),
                child: Container(
                  padding: EdgeInsets.only(right: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                crossAxisAlignment:
                                CrossAxisAlignment.center,
                                children: [
                                  const Text(
                                    "Unit : ",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold),
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    unit.unitNo + "-" +unit.phase,
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Row(
                                crossAxisAlignment:
                                CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    "Unit Marla : ",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    unit.netMarla.toString(),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Row(
                                crossAxisAlignment:
                                CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    "Sqft : ",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    unit.sqft.toString(),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(children: [
                                Text("Status : ",style: TextStyle(fontWeight: FontWeight.bold),),
                                Text(unit.status),
                              ],),
                              SizedBox(
                                height: 5,
                              ),
                              Row(
                                crossAxisAlignment:
                                CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    "Marla Amount : ",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 10,
                                        color: Colors.grey),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    formatter.format(unit.rate),
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 10,
                                        color: Colors.red),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Row(
                                crossAxisAlignment:
                                CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    "Net Amount : ",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 10,
                                        color: Colors.grey),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    formatter.format(unit.netAmount),
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 10,
                                        color: Colors.red),
                                  ),
                                ],
                              ),

                            ],
                          )
                        ],
                      ),

                      SizedBox(
                        height: 5,
                      ),
                    ],
                  ),
                ))),
      ),
    );
  }
}
