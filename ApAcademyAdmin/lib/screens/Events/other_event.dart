import 'package:ap_academy_admin/ModelView/Controller/otherEventModel.dart';
import 'package:ap_academy_admin/screens/Events/other_event_create.dart';
import 'package:ap_academy_admin/screens/Events/other_event_details.dart';
import 'package:ap_academy_admin/screens/shared/DeletPop.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:get/get.dart';
import 'package:get/route_manager.dart';
import 'package:intl/intl.dart';

class OtherScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      //Create an event add button
      child: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "",
                    style: MyResources.appHeadingStyle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    width: Get.width * 0.2,
                    height: 25,
                    child: RaisedButton(
                      textColor: MyResources.buttontextColor,
                      color: MyResources.buttonColor,
                      child: Text(
                        "ADD",
                      ),
                      onPressed: () {
                        Get.to(
                          OtherCreateEvent(),
                        );
                      },
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Container(
              height: Get.height * 0.7,
              child: GetX<OtherEventController>(
                init: Get.put<OtherEventController>(OtherEventController()),
                builder: (OtherEventController otherEventController) {
                  if (otherEventController != null &&
                      otherEventController.event != null) {
                    print(otherEventController.event.toString());
                    return Stack(
                      children: [
                        // Center(
                        //   child: ColorFiltered(
                        //       colorFilter: ColorFilter.mode(
                        //           Colors.white.withOpacity(0.3),
                        //           BlendMode.dstATop),
                        //       child: Image.asset(
                        //         'assets/otherevent.png',
                        //         width: 200,
                        //         height: 200,
                        //       )),
                        // ),
                        ListView.builder(
                          itemCount: otherEventController.event.length,
                          itemBuilder: (BuildContext context, int index) =>
                              getEventCard(otherEventController.event[index]),
                        ),
                      ],
                    );
                  } else {
                    return Container(
                      child: LoadingWidget(),
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getEventCard(Map<String, dynamic> otherEventModel) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(left:8,right:8),
        child: Container(
          //constraints: BoxConstraints(minHeight: 200),
          width: Get.width,
          //create the cards for all details of event happening
          child: InkWell(
            onTap: () {
              Get.dialog(
                OtherEventDetails(
                  otherEvent: otherEventModel,
                ),
              );
            },
            child: Card(
              color: MyResources.backgroundColor,
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  children: [
                    Center(
                      child: Container(
                        width: Get.width * 0.9,
                        child: Text(otherEventModel['eventName'].toString().toUpperCase(),
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w600,

                            )),
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            //width: Get.width * 0.56,
                            child: Column(children: [
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("Coach:",
                                      style: MyResources.titleTextStyle),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Container(
                                    //width: 100,
                                    child: Text(
                                      otherEventModel['coach'],
                                      overflow: TextOverflow.ellipsis,
                                      style: MyResources.nameTextStyle,
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("Venue:",
                                      style: MyResources.titleTextStyle),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Container(
                                    //width: 100,
                                    child: Text(
                                      otherEventModel['venue'],
                                      overflow: TextOverflow.ellipsis,
                                      style: MyResources.nameTextStyle,
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("Time:",
                                      style: MyResources.titleTextStyle),
                                  SizedBox(
                                    width: 16,
                                  ),
                                  Container(
                                    //width: 100,
                                    child: Text(
                                      DateFormat.yMEd()
                                          .add_jm()
                                          .format(otherEventModel['time'].toLocal()),
                                      style: TextStyle(
                                          color: MyResources.headingColor),
                                    ),
                                  ),
                                ],
                              ),
                            ]),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 0),
                          // width: 30,
                          child: IconButton(
                            onPressed: () async {
                              Get.dialog(Center(
                                  child: DeletpopUp(
                                message: 'Are you sure you want to delete?',
                                pressFunc: () async {
                                  final otherEventController =
                                      Get.put(OtherEventController());
                                  await otherEventController
                                      .delEvent(
                                        otherEventModel['_id'],
                                      )
                                      .then((value) => Get.back());
                                },
                              )));
                            },
                            icon: Icon(
                              Icons.dangerous,
                              color: Colors.red[300],
                              semanticLabel: 'Delete',
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 0),
                          // width: 30,
                          child: IconButton(
                            onPressed: () {
                              Get.to(OtherCreateEvent(
                                otherEvent: otherEventModel,
                              ));
                            },
                            icon: Icon(
                              Icons.edit,
                              color: Colors.green,
                              semanticLabel: 'Edit',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
