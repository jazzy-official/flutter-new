import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:get/get.dart';

class TrainingDetails extends StatelessWidget {
  final Map<String, dynamic>? trainingModel;

  const TrainingDetails({Key? key, this.trainingModel}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Align(
        //alignment: Alignment.center,
        child: Container(
          color: Colors.transparent,
          height: Get.height*0.8,
          width:Get.width*0.9,
          //constraints: BoxConstraints(minHeight: 500),
          //create the cards for all details of event happening
          child: Stack(
            children: [
              Card(
                color: MyResources.backgroundColor,
                child: Column(
                  //crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding:
                      const EdgeInsets.all(4),
                      child: Row(
                        //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        //crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: Get.width*0.855,
                            color: MyResources.buttonColor,
                            height: 35,
                            child: Center(
                              child: Text(
                                "TRAININGS",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 7.0),
                          //   child: Container(
                          //     width: MediaQuery.of(context).size.width * 0.35,
                          //     height: 35,
                          //     // child: RaisedButton(
                          //     //   textColor: Colors.white,
                          //     //   color: Colors.blue,
                          //     //   child: Text(
                          //     //     "Add Members",
                          //     //   ),
                          //     //   onPressed: () {
                          //     //     Navigator.pop(context);
                          //     //     Get.dialog(
                          //     //       SelectMembers(),
                          //     //     );
                          //     //   },
                          //     //   shape: new RoundedRectangleBorder(
                          //     //     borderRadius: new BorderRadius.circular(10.0),
                          //     //   ),
                          //     // ),
                          //   ),
                          // ),
                        ],
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.only(top: 10, left: 8,),
                      child: Row(
                        //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child:
                            Text("Team :", style: MyResources.titleTextStyle),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                            width: Get.width*0.7,
                            child: Text(
                              trainingModel!["session"],
                              overflow: TextOverflow.ellipsis,
                              style:TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w300,
                                color: Color(0xff1758B4),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10, left: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              child:
                              Text("Venue:",
                                  overflow: TextOverflow.ellipsis,
                                  style: MyResources.titleTextStyle)),
                          Container(
                            width: Get.width*0.7,
                            child: Text(
                              trainingModel!["venue"],
                              style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w300,
                                color: Color(0xff1758B4),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10, left: 8,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Coach:", style: MyResources.titleTextStyle),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                            width: Get.width*0.7,
                            child: Text(
                              trainingModel!["coach"],
                              style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w300,
                                color: Color(0xff1758B4),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    //members
                    Center(
                      // child: Text(
                      //   "Members",
                      //   style: MyResources.appHeadingStyle,
                      // ),
                    ),
                    // Padding(
                    //   padding: const EdgeInsets.only(left: 8.0,right:8),
                    //   child:
                      Row(
                       // height: Get.height*0.5,
                      //  child: SingleChildScrollView(
                          children:[ SizedBox(
                            width: 10,
                          ),

                            Expanded(
                              flex: 2,
                              child: Text(
                                'Sr',
                                style: MyResources.titleTextStyle,
                              ),
                            ),
                            
                            Expanded(
                              flex: 6,
                              child: Text(
                                '     Name',
                                style: MyResources.titleTextStyle,
                              ),
                            ),

                            // ),
                              // DataColumn(label:
                            // Expanded(
                            //   flex: 5,
                            //   child: Text(
                            //     'Age',
                            //     style: MyResources.titleTextStyle,
                            //   ),
                            // ),

                              // ),
                              // DataColumn(
                              //   label:
                            Expanded(
                              flex: 2,
                              child: Center(
                                child: Text(
                                    'Att',
                                    style: MyResources.titleTextStyle,
                                  ),
                              ),
                            ),
                              // ),
                            ],

                         // ),
                        ),

                    Expanded(
                      child: ListView.builder(
                        itemCount: trainingModel!['totalPlayer'].length,
                        itemBuilder: (context, index) {
                          return getTeamMembersCard(index,
                                                  trainingModel!['totalPlayer'][index]['name'],
                                                  trainingModel!['totalPlayer'][index]['firebaseId'],
                                                  trainingModel!['attendance'],
                                                  context);
                        },
                      ),
                    )

          // Column(
          //               children:List<Row>.generate(
          //                   trainingModel!['totalPlayer'].length, (index)
          //               {
          //                 return getTeamMembersCard(index,
          //                     trainingModel!['totalPlayer'][index]['name'],
          //                     trainingModel!['totalPlayer'][index]['firebaseId'],
          //                     trainingModel!['attendance'],
          //                     context);
          //               }
          //               ),
          //             )
                      //),
                    // )
                  ],
                ),
              ),
              Positioned(
                  left: 10,
                  top: 4,
                  child: BackButton(
                    color: Colors.white,
                    onPressed: () {
                      Get.back();
                    },
                  )),
            ],
          ),
        ),
      ),
    );
  }

  Row getTeamMembersCard(int index,String name, String fId,
      List<dynamic> attendeeIds, BuildContext context) {
    bool attendance = false;
    attendeeIds.forEach((element) {
      if (fId == element.toString()) {
        attendance = true;
      }
    });
    return Row(
      children:[
    SizedBox(
    width: Get.width*0.04,),
        Expanded(
          flex: 2,
            child: Text(index.toString())),
         Expanded(
           flex: 5,
             child: Text(name)
         ),
        
    // SizedBox(
    // width: Get.width*0.15,child:Text(type)),
  Expanded(
    flex: 2,
    child: Center(
            child: InkWell(
              child: Icon(
                attendance ? CupertinoIcons.checkmark_alt_circle : CupertinoIcons.clear_circled,
                color: attendance ? Colors.green : Colors.redAccent,
              ),
              onTap: () {},
            )),
  ),
        
      ],
    );
  }
}
