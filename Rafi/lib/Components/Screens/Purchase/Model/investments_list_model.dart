import 'package:rafi_admin/Components/Screens/Purchase/Model/investment_model.dart';

class InvestmentPageModel {
  List<InvestmentModel> investments;
  int pageNo;
  int pageSize;
  String firstPage;
  String lastPage;
  String? nextPage;
  String? previousPage;
  int totalPage;
  int totalRecords;


  InvestmentPageModel({
    required this.lastPage,
    this.previousPage,
    this.nextPage,
    required this.totalRecords,
    required this.pageSize,
    required this.firstPage,
    required this.investments,
    required this.pageNo,
    required this.totalPage,
  });

  factory InvestmentPageModel.fromJson(Map<String, dynamic> json) {
    return InvestmentPageModel(
      investments: List<InvestmentModel>.from(json["data"]
          .map(
            (dynamic item) => InvestmentModel.fromJson(item),
      )
          .toList()),
      totalPage: json['totalPages'],
      pageNo: json['pageNumber'],
      firstPage: json['firstPage'],
      lastPage:json['lastPage'],
      nextPage: json['nextPage'],
      previousPage: json['previousPage'],
      pageSize: json['pageSize'],
      totalRecords: json['totalRecords'],
    );
  }
}
