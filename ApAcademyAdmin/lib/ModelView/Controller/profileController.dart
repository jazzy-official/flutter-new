import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import 'databaseController.dart';
import 'db_cred.dart';

class ProfileController extends GetxController {
  Rx<Map<String, dynamic>> userDtails = Rx<Map<String, dynamic>>({});
  Map<String, dynamic> get userDetail => userDtails.value;
  final dbHelper = Get.put(DatabaseController());
  final ImagePicker _picker = ImagePicker();
  @override
  Future<void> onInit() async {
    if (!db.isConnected) {
      try {
        // if (!db.isConnected && db.state.toString() != "State.OPENING") {
        await db.open(secure:true).then((value) {});
        //  }
      } catch (error) {
        Get.snackbar("error", "Connection error try to restart Application");
        print('mongo db is not opening');
        print(error.toString());
      }
    }
    userDtails.value = (await dbHelper.getUserDetail())!;
    // TODO: implement onInit
    print(userDtails.value);
    super.onInit();
  }

  FirebaseStorage firebaseStorage = FirebaseStorage.instance;

  getImages() {
    firebaseStorage.ref().child('images').child('defaultProfile.png');
  }

  Future<void> uploadImageToFirebase(
    File image,
  ) async {
    try {
      // Make random image name.

      String imageLocation =
          'profileimages/${FirebaseAuth.instance.currentUser!.uid}.jpg';

      // Upload image to firebase.
      final storageReference =
          FirebaseStorage.instance.ref().child(imageLocation);
      final uploadTask = storageReference.putFile(image);
      await uploadTask.whenComplete(() => null);
      String imgLink = await uploadTask.snapshot.ref.getDownloadURL();
      await dbHelper.addProfileImgPathToDatabase(imgLink);
      await FirebaseAuth.instance.currentUser!.updateProfile(photoURL: imgLink);
      print(imgLink);
    } catch (e) {
      print(e);
    }
  }

  Future<bool> saveProfile(File? _image, Map<String, dynamic> userData) async {
    if (_image != null) await uploadImageToFirebase(_image);
    bool isOk = await dbHelper.updateUser(userData);
    return isOk;
  }

/////////////////////////////////////////

  // profile image picke from camra r gallery and set it to new profile pic
  Future<XFile?> imgFromCamera() async {
    try {
      return await _picker.pickImage(
          source: ImageSource.camera, imageQuality: 50);
    } catch (e) {
      return null;
    }
  }

  Future<XFile?> imgFromGallery() async {
    try {
      return await _picker.pickImage(
          source: ImageSource.gallery, imageQuality: 50);
    } catch (e) {
      return null;
    }
  }

  Future<void> refreshDate() async {
    userDtails.value = (await dbHelper.getUserDetail())!;
  }
}
