import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class SponsorDetailCard extends StatelessWidget {
  final Map<String, dynamic> sponsorDetailData;

  const SponsorDetailCard({Key? key, required this.sponsorDetailData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 10, right: 10),
      child: Center(
        child: Container(
          height: Get.height * 0.5,
          child: Card(
            color: Color(0xfff5f5f5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding:
                  const EdgeInsets.only(top: 20.0, right: 10, left: 10,bottom: 20),
                  child: Center(
                    child: Container(
                      height: 40,
                      width: Get.width,
                      color: Color(0xff1758B4),
                      child: Center(
                        child: Text(
                          sponsorDetailData['name'],
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: Image.network(
                      sponsorDetailData['imageUrl'],
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 20,
                        right: 10,
                        left: 10,
                        bottom: 20,
                      ),
                      child: Container(
                        width: Get.width*0.8,
                        child: Text(
                          sponsorDetailData['description'],
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 16),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
