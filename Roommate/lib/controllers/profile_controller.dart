import 'dart:core';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:room_buddy/Models/about_user_model.dart';
import 'package:room_buddy/Models/habits_modal.dart';
import 'package:room_buddy/Models/location_model.dart';
import 'package:room_buddy/Models/user_model.dart';

class ProfileController extends GetxController {
  UserModel? user = UserModel(
      "Muhammad Jahanzaib",
      ["English", "Urdu"],
      "Faisalabad",
      "Id card",
      "33100-8226343-7",
      "assets/me.jpg",
      "A long very long paragraph to show a long vast paragraph just a dummy data to show long paragraph.",
      AboutUser(
        "Male",
        RangeValues(22,0),
        "Professional",
        "Single",
        Habits("Daily", "Non-Veg", "Occasionally", "Occasionally", "Have"),
        [
          "Swimming",
          "Football",
          "Travel",
          "Food",
        ],
        [
          "Calm",
          "Active",
          "Fun",
          "Organized",
        ],
      ),
      "Jahanzaib0150@outlook.com",
      [2,4],
      1
  );

  @override
  Future<void> onInit() async {
    super.onInit();
  }
}
