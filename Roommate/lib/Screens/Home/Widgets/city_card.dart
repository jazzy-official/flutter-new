import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:room_buddy/Constants/util.dart';

class CityCard extends StatelessWidget {
  const CityCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3,
      child: SizedBox(
        width: 220,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 240,
              width: 220,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(3),
                  image: const DecorationImage(
                      image: AssetImage(
                        "assets/newyork.jpg",
                      ),
                      fit: BoxFit.cover)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.all(10),
                    height: 25,
                    width: 25,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.white),
                    child: Center(child: Icon(Ionicons.heart,size: 18,color: Colors.red,),),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: const Text("New York",softWrap: false,
                          overflow: TextOverflow.fade,style: TextStyle(fontSize: 16,fontWeight: FontWeight.w600),),
                      ),
                      InkWell(
                          onTap: (){},
                          child: Row(
                            children: [
                              Text(
                                "Explore",
                                style: TextStyle(
                                    color: myResources.themeColor, fontWeight: FontWeight.w600),
                              ),
                              Icon(Ionicons.chevron_forward_outline,color: myResources.themeColor,size: 18,)
                            ],
                          ))
                    ],
                  ),
                  const SizedBox(height: 5,),
                  const Text("200+ rooms",style: TextStyle(fontSize: 14,color: Colors.black54,fontWeight: FontWeight.w600),),
                  const SizedBox(height: 5,),
                  const Text("5 km away",style: TextStyle(fontSize: 13,color: Colors.black45,),),

                ],
              ),
            )

          ],
        ),
      ),
    );
  }
}
