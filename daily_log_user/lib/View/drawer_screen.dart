import 'package:daily_log_user/View/Tasks/tasks_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';

import '../Controllers/auth_controller.dart';
import '../Controllers/profilecontroller.dart';
import '../constants/util.dart';
import 'DailyLod/daily_log_screen.dart';
import 'Projects/projects_screen.dart';

RxString sideTitle = "Tasks".obs;
Rx<Widget> selectedWidget = TasksScreen().obs;


class HomeDrawer extends StatefulWidget {
  HomeDrawer({Key? key}) : super(key: key);

  @override
  State<HomeDrawer> createState() => _HomeDrawerState();
}

class _HomeDrawerState extends State<HomeDrawer> {
  final profileController = Get.put(ProfileController());
  final authcont = Get.put(AuthController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: Column(
          children: [
            SafeArea(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0,vertical: 20),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CircleAvatar(
                      radius: 40,
                      backgroundImage: NetworkImage(profileController.userDetail.imgUrl!),
                    ),
                    SizedBox(width: 20,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(profileController.userDetail.name.toUpperCase(),style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),

                        Container(width:180,child: Text(profileController.userDetail.email,style: TextStyle(fontSize: 12),overflow: TextOverflow.ellipsis,)),
                      ],
                    )
                  ],
                ),
              ),
            ),
            const Divider(),
            Flexible(
              fit: FlexFit.tight,
              child: Column(
                children: [

                  Container(
                    decoration: BoxDecoration(
                        border: sideTitle.value == "Tasks".toUpperCase()
                            ? Border(
                          left: BorderSide(
                            color: MyResources.themecolor,
                            width: 5.0,
                          ),
                        )
                            : Border()),
                    child: ListTile(
                      dense: true,
                      tileColor: sideTitle.value == "Tasks".toUpperCase()
                          ? MyResources.themecolor.withOpacity(0.2)
                          : Colors.transparent,
                      /*  selected: true,*/
                      title: Text(
                        "Tasks".toUpperCase(),
                        // style: myResources.myTextStyle,
                      ),
                      leading: Icon(Icons.fact_check_outlined,
                          color: MyResources.themecolor),
                      onTap: () {
                        if (mounted) {
                          setState(() {
                            selectedWidget = TasksScreen().obs;
                            sideTitle = "Tasks".toUpperCase().obs;
                          });
                        }
                        Get.back();

                        // Navigator.push(context,
                        //     MaterialPageRoute(builder: (context) => EventScreen()));
                      },
                    ),
                  ),

                  Container(
                    decoration: BoxDecoration(
                        border: sideTitle.value == "Daily Log".toUpperCase()
                            ? Border(
                          left: BorderSide(
                            color: MyResources.themecolor,
                            width: 5.0,
                          ),
                        )
                            : Border()),
                    child: ListTile(
                      dense: true,
                      tileColor:
                      sideTitle.value == "Daily Log".toUpperCase()
                          ? MyResources.themecolor.withOpacity(0.2)
                          : Colors.transparent,
                      /*  selected: true,*/
                      title: Text(
                        "Daily Log".toUpperCase(),
                        // style: myResources.myTextStyle,
                      ),
                      leading: Icon(Ionicons.clipboard_outline,
                          color: MyResources.themecolor),
                      onTap: () {
                        if (mounted)
                          setState(() {
                            selectedWidget = DAilyLogScreen().obs;
                            sideTitle.value = "Daily Log".toUpperCase();
                          });
                        Get.back();

                        // Navigator.push(context,
                        //     MaterialPageRoute(builder: (context) => EventScreen()));
                      },
                    ),
                  ),

                  Container(
                    decoration: BoxDecoration(
                        border: sideTitle.value == "Projects".toUpperCase()
                            ? Border(
                          left: BorderSide(
                            color: MyResources.themecolor,
                            width: 5.0,
                          ),
                        )
                            : const Border()),
                    child: ListTile(
                      dense: true,
                      tileColor:
                      sideTitle.value == "Projects".toUpperCase()
                          ? MyResources.themecolor.withOpacity(0.2)
                          : Colors.transparent,
                      /*  selected: true,*/
                      title: Text(
                        "Projects".toUpperCase(),
                        // style: myResources.myTextStyle,
                      ),
                      leading: Icon(Ionicons.ios_file_tray_stacked,
                          color: MyResources.themecolor),
                      onTap: () {
                        if (mounted) {
                          setState(() {
                            selectedWidget = const ProjectsScreen().obs;
                            sideTitle.value = "Projects".toUpperCase();
                          });
                        }
                        Get.back();

                        // Navigator.push(context,
                        //     MaterialPageRoute(builder: (context) => EventScreen()));
                      },
                    ),
                  ),

                ],
              ),
            ),
            const Expanded(child: SizedBox()),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButton(
                    onPressed: (){
                      authcont.signOut();
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: MyResources.themecolor,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: const [
                            Text("Log out",style: TextStyle(color: Colors.white),),
                            SizedBox(width: 10,),
                            Icon(Icons.logout,color: Colors.white,)
                          ],
                        ),
                      ),
                    )),
              ],
            )
          ],
        ),
      ),
      appBar: AppBar(
        title: Text(sideTitle.value),
        centerTitle: true,
        backgroundColor: MyResources.themecolor,
      ),
      body: selectedWidget.value,
    );
  }
}