import 'package:daily_log_user/View/Signup/signup.dart';
import 'package:daily_log_user/constants/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:get/get.dart';

import '../../Controllers/auth_controller.dart';

class LoginScreen extends StatelessWidget {
   LoginScreen({Key? key}) : super(key: key);
  TextEditingController emailcontroller = TextEditingController();
  TextEditingController passcontroller = TextEditingController();
   var authcontroller = Get.put(AuthController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   leading: const BackButton(
      //       color: Colors.black
      //   ),
      //   backgroundColor: Colors.white,
      //   title: Text("SIGN IN"),
      //   titleTextStyle: TextStyle(color: Colors.black,fontSize: 18,fontWeight: FontWeight.w600),
      //   centerTitle: true,
      // ),
      resizeToAvoidBottomInset: false,
      body: Column(
        children: [
          Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 50.0,bottom: 30),
                      child: Container(
                        height: 200,
                        width: 200,
                        decoration: BoxDecoration(
                          image: DecorationImage(image: AssetImage("assets/logo.png"),fit: BoxFit.contain)
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                      child: TextField(
                        controller: emailcontroller,
                        decoration:  InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            hintText: 'Email',
                            contentPadding: const EdgeInsets.symmetric(horizontal: 10)),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          TextField(
                            controller: passcontroller,
                            decoration:  InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              hintText: 'Password',
                              contentPadding: EdgeInsets.symmetric(horizontal: 10),
                            ),
                          ),
                          TextButton(onPressed: (){
                            //
                          }, child: const Text("Forgot Password?"))
                        ],
                      ),
                    ),
                    TextButton(
                        onPressed: () {
                          if(emailcontroller.text=="" || passcontroller.text == "" ||!emailcontroller.text.contains("@")){
                            Get.snackbar("Alert", "Please fill credentials");
                          }else{
                            authcontroller.signInwithEmail(emailcontroller.text, passcontroller.text);
                          }

                        },
                        child: Container(
                          height: 50,
                          width: MediaQuery.of(context).size.width - 40,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: MyResources.themecolor,
                          ),
                          child: const Center(
                              child: Text(
                                "Sign in",
                                style: TextStyle(color: Colors.white, fontSize: 17),
                              )),
                        )),
                    Text("Or"),

                    TextButton(
                        onPressed: () async {

                            await authcontroller.signInWithGoogle(context);


                        },
                        child: Container(
                          height: 50,
                          width: MediaQuery.of(context).size.width - 40,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Color(0xffea4333),
                          ),
                          child:  Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(Ionicons.logo_google,color: Colors.white,),
                              SizedBox(width: 10,),
                              Text(
                                "Sign in with Google",
                                style: TextStyle(color: Colors.white, fontSize: 17),
                              ),
                            ],
                          ),
                        )),
                  ],
                ),
              )),
          Container(
            height: 45,
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide(
                  //                    <--- top side
                  color: Colors.grey,
                  width: 1.0,
                ),
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "New to App?",
                  style: TextStyle(color: Colors.grey),
                ),
                TextButton(onPressed: () {
                  Get.to( SignUpScreen());
                  print("done");
                }, child: const Text("Sign up"))
              ],
            ),
          ),
        ],
      ),
    );
  }
}
