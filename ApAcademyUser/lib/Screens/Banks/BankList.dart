import 'package:ap_academy_new/ModelView/controllers/BankController.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../Constants/util.dart';
import '../../shared/loading_widget.dart';

class BankList extends StatefulWidget {
  @override
  _BankListState createState() => _BankListState();
}

class _BankListState extends State<BankList> {
  final bankController = Get.put(BanksController());
  @override
  void initState() {
    bankController.refreshBankData();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      //Create an event add button
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                " BANK ACCOUNTS",
                style: myResources.appHeadingStyle,
              ),
            ),
            Container(
              height: Get.height,
              child: GetX<BanksController>(
                init: Get.put<BanksController>(BanksController()),
                builder: (BanksController banksController) {
                  if (banksController != null &&
                      banksController.banks != null) {
                    return Container(
                      height: Get.height,
                      child: Stack(
                        children: [
                          ListView.builder(
                            itemCount: banksController.banks.length,
                            itemBuilder: (BuildContext context, int index) {
                              return getBankCard(banksController.banks[index]);
                            },
                          ),
                        ],
                      ),
                    );
                  } else {
                    return Container(
                      child: LoadingWidget(),
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  getBankCard(Map<String, dynamic> bankModel) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 4,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,

                  children: [
                    Text(
                      "Bank Name:  ",
                      style: myResources.titleTextStyle,
                    ),
                    SizedBox(height: 10,),
                    Text(
                      "Account Name:  ",
                      style: myResources.titleTextStyle,
                    ),
                    SizedBox(height: 10,),
                    Text("Account Number:  ",
                        style: myResources.titleTextStyle),
                    SizedBox(height: 10,),
                    Text("FPS ID:  ",
                            style: myResources.titleTextStyle),
                  ],
                ),
              ),
              Expanded(
                flex: 6,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(bankModel['bankName'],
                        style:
                        TextStyle(color: myResources.buttonColor)),
                    SizedBox(height: 10,),
                    Text(bankModel['accName'],
                        style:
                        TextStyle(color: myResources.buttonColor)),
                    SizedBox(height: 10,),
                    Text(bankModel['BankAcc'],
                        style:
                        TextStyle(color: myResources.buttonColor)),
                    SizedBox(height: 10,),
                    Text(bankModel['fpsid'],
                        style:
                        TextStyle(color: myResources.buttonColor)),
                  ],
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
