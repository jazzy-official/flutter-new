class Entity {
  int id;
  String name;
  double amount;
  double amountTwo;
  String type;



  Entity({
    required this.id,
    required this.amount,
    required this.name,
    required this.amountTwo,
    required this.type,

  });


  factory Entity.fromJson(Map<String, dynamic> json) {
    return Entity(
      id: json['Id'],
      type:json['Type'],
      amountTwo: json['AmountTwo'],
      name: json['Name'],
      amount:json['Amount'],
    );
  }
}
