import 'dart:async';
import 'dart:io';

import 'package:ap_academy_new/Screens/drawer_screen.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';

import 'db_controller.dart';

class NotificationController extends GetxController {
  Rx<List<Map<String, dynamic>>> notifList = Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get notify => notifList.value;
  Rx<String?> userToken = RxString("");
  @override
  Future<void> onInit() async {
    // await db.open();
    // String id = Get.find<AuthController>().firebaseUser.uid;
    notifList.bindStream(DatabaseController()
        .notificationStream()); //id)); //Stream Comming from
    super.onInit();
  }

  @override
  void onClose() {
    // db.close();
    super.onClose();
  } /////////////////////////////////////////////////////notification section

  ///
  final String serverToken =
      'AAAAjwzCMbM:APA91bF3fX844AxrlF7-HhYj2twvl9yQR3i_csFs9pDOwUIiBA0YDCRWsxLEtrlnx7ZW3f_tPdA1bTwnW-bU6JfqACPl_7gG8iD9tAHDSA3rG-4aHiqxKqwV4UT8ploK2MLT2TfsPJr7';

//  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  Future<void> openNotification(Map<String, dynamic> msgData) async {
    const AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails(
            'FLUTTER_NOTIFICATION_CLICK', 'Ap Sports academy ',
            importance: Importance.high,
            priority: Priority.high,
            showWhen: true,
            playSound: true);
    const NotificationDetails platformChannelSpecifics =
        NotificationDetails(android: androidPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
        1,
        msgData['notification']['title'],
        msgData['notification']['body'],
        platformChannelSpecifics,
        payload: 'item x');
  }

  // fcmListner() {
  //   try {
  //     _firebaseMessaging.configure(
  //       onBackgroundMessage: (Map<String, dynamic> message) async {
  //         await openNotification(message);
  //         print("onBackground: $message");
  //       },
  //       onMessage: (Map<String, dynamic> message) async {
  //         await openNotification(message);
  //         print("onMessage: $message");
  //       },
  //       onLaunch: (Map<String, dynamic> message) async {
  //         Get.to(HomeDrawer());
  //         print("onLaunch: $message");
  //       },
  //       onResume: (Map<String, dynamic> message) async {
  //         print("onResume: $message");
  //       },
  //     );
  //     const AndroidInitializationSettings initializationSettingsAndroid =
  //         AndroidInitializationSettings('@mipmap/ic_launcher');
  //     final IOSInitializationSettings initializationSettingsIOS =
  //         IOSInitializationSettings(
  //             onDidReceiveLocalNotification: onDidReceiveLocalNotification);
  //     final InitializationSettings initializationSettings =
  //         InitializationSettings(
  //             android: initializationSettingsAndroid,
  //             iOS: initializationSettingsIOS);
  //     flutterLocalNotificationsPlugin
  //         .initialize(initializationSettings)
  //         .then((value) {
  //       print('initialized notoication');
  //     });

  //     _firebaseMessaging.requestNotificationPermissions(
  //         const IosNotificationSettings(
  //             sound: true, badge: true, alert: true, provisional: false));

  //     _firebaseMessaging.onIosSettingsRegistered
  //         .listen((IosNotificationSettings settings) {
  //       print("Settings registered: $settings");
  //     });
  //     _firebaseMessaging.getToken().then((String token) {
  //       assert(token != null);

  //       print(token.toString());
  //     });
  //   } catch (ex) {
  //     print(ex.toString());
  //   }
  // }

  Future onDidReceiveLocalNotification(
      int id, String? title, String? body, String? payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    Get.defaultDialog(
      title: title!,
      content: Text(body!),
      actions: [
        CupertinoDialogAction(
          isDefaultAction: true,
          child: Text('Ok'),
          onPressed: () async {
            Get.to(HomeDrawer());
          },
        )
      ],
    );
  }
}

final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;

fcmListner() async {
  final localNotif = Get.put(NotificationController());
  try {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      (Map<String, dynamic> message) async {
        await localNotif.openNotification(message);

        print("onMessage: $message");
      };
    });
    FirebaseMessaging.onBackgroundMessage((RemoteMessage message) {
      print("onBackgroundMessage: $message");
      return myBackgroundMessageHandler(message.data);
    });
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      (Map<String, dynamic> message) async {
        Get.to(HomeDrawer());
        print("onLaunch: $message");
      };
      print("onMessageOpenedApp: $message");
    });

    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    final IOSInitializationSettings initializationSettingsIOS =
        IOSInitializationSettings(
            onDidReceiveLocalNotification:
                localNotif.onDidReceiveLocalNotification);
    final InitializationSettings initializationSettings =
        InitializationSettings(
            android: initializationSettingsAndroid,
            iOS: initializationSettingsIOS);
    localNotif.flutterLocalNotificationsPlugin
        .initialize(initializationSettings)
        .then((value) {
      print('initialized notification');
    });

    if (Platform.isIOS) {
      NotificationSettings settings = await _firebaseMessaging.requestPermission(
        alert: true,
        announcement: false,
        badge: true,
        carPlay: false,
        criticalAlert: false,
        provisional: false,
        sound: true,
      );
      if (settings.authorizationStatus == AuthorizationStatus.authorized) {
        print('User granted permission');
      } else if (settings.authorizationStatus == AuthorizationStatus.provisional) {
        print('User granted provisional permission');
      } else {
        print('User declined or has not accepted permission');
      }
    }

    await _firebaseMessaging.getToken().then((value) {
      String? token = value;
      assert(token != null);
      localNotif.userToken = token.obs;
      print(token.toString());
    });
  } catch (ex) {
    print(ex.toString());
  }
}

Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async {
  final localNotif = Get.put(NotificationController());
  if (message != null) {
    await localNotif.openNotification(message);
  } else {
    print('sorry');
  }

  // Or do other work.
}

abstract class NotificationAction {
  NotificationAction(this.notification);

  Notification notification;
  late bool isInForeground;
}
