
import 'package:ap_ground_admin/Screens/Canteen/Models/product_model.dart';
import 'package:ap_ground_admin/Screens/Grounds/Models/ground_models.dart';
import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:firebase_database/firebase_database.dart';


class InventoryController {
  final DatabaseReference databaseReference =
  FirebaseDatabase.instance.reference().child("Inventory");

  Future<void> saveProduct(Product product) async {
    try{ await databaseReference.push().set(product.toJson());
    }catch(e){
      displayToast(e.toString());
    }
  }

  Future<void> updateProduct(Product product) async {

    databaseReference.update({
      product.id! : product.toJson()
    });

  }
  Future<void> deleteProduct(String id) async {
    databaseReference.child(id).remove();

  }

  Future<List<Product>> getProducts() async {
    List<Product> products = [];
    await databaseReference.once().then((DataSnapshot snapshot) {
      snapshot.value.forEach((key, value) {
        Product gr = Product.fromJson(value,key);
        products.add(gr);
      });
    });
    return products;
  }



// void readGround(){
//   databaseReference.child("Grounds").once().then((DataSnapshot snapshot) {
//     print('Data : ${snapshot.value}');
//   });
// }
}
