import 'package:daily_log_user/Controllers/task_controller.dart';
import 'package:daily_log_user/Models/daili_log_model.dart';
import 'package:daily_log_user/Models/task_model.dart';
import 'package:daily_log_user/View/Tasks/add_task.dart';
import 'package:daily_log_user/View/delete_popup.dart';
import 'package:daily_log_user/constants/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:intl/intl.dart';

import '../../Controllers/daily_log_controller.dart';
import '../../Models/commit.dart';

class DAilyLogScreen extends StatefulWidget {
  const DAilyLogScreen({Key? key}) : super(key: key);

  @override
  State<DAilyLogScreen> createState() => _DAilyLogScreenState();
}

class _DAilyLogScreenState extends State<DAilyLogScreen> {
  String value = "1st Half";
  TaskModel? task;
  List<Commit> first = [];
  List<Commit> second = [];

  TextEditingController commentcont = TextEditingController();
  var taskcont = Get.put(TaskController());

  @override
  Widget build(BuildContext context) {
    void _showDialog(Widget child) {
      showCupertinoModalPopup<void>(
          context: context,
          builder: (BuildContext context) => Container(
                height: 216,
                padding: const EdgeInsets.only(top: 6.0),
                // The Bottom margin is provided to align the popup above the system navigation bar.
                margin: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom,
                ),
                // Provide a background color for the popup.
                color: CupertinoColors.systemBackground.resolveFrom(context),
                // Use a SafeArea widget to avoid system overlaps.
                child: SafeArea(
                  top: false,
                  child: child,
                ),
              ));
    }

    return GetBuilder<DailyLogController>(
        autoRemove: false,
        init: DailyLogController(),
        builder: (cont) => Stack(
              children: [
                SizedBox(
                  height: Get.height,
                  child: ListView(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          TextButton(
                              onPressed: () => {
                                    _showDialog(
                                      CupertinoDatePicker(
                                        initialDateTime: cont.date,
                                        mode: CupertinoDatePickerMode.date,
                                        use24hFormat: true,
                                        // This is called when the user changes the date.
                                        onDateTimeChanged: (DateTime newDate) {
                                          cont.date = newDate;
                                          cont.datefilter();
                                          cont.update();
                                        },
                                      ),
                                    ),
                                  },
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: MyResources.themecolor,
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    DateFormat("MMMM d, yyyy")
                                        .format(cont.date),
                                    style: const TextStyle(color: Colors.white),
                                  ),
                                ),
                              ))
                        ],
                      ),
                      const Center(
                          child: Text("First Half",
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                              ))),
                      cont.logbydate != null
                          ? ListView.builder(
                              controller: ScrollController(),
                              shrinkWrap: true,
                              itemCount: cont.logbydate!.firsthalf.length,
                              itemBuilder: (context, index) {
                                Commit commit =
                                    cont.logbydate!.firsthalf[index];
                                return Card(
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            children: [
                                              Text(
                                                "Task : ",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w600),
                                              ),
                                              Text(
                                                commit.task,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w600),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              const Text("Comment : ",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w600),),
                                              Column(
                                                children: [
                                                  Text(commit.des),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ));
                              },
                            )
                          : const Center(
                              child: Padding(
                                padding: EdgeInsets.all(15.0),
                                child: Text("Empty"),
                              ),
                            ),
                      const Center(
                          child: Text("Second Half",
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                              ))),
                      cont.logbydate != null
                          ? ListView.builder(
                              controller: ScrollController(),
                              shrinkWrap: true,
                              itemCount: cont.logbydate!.secondhalf.length,
                              itemBuilder: (context, index) {
                                Commit commit =
                                    cont.logbydate!.secondhalf[index];
                                return Card(
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            children: [
                                              const Text(
                                                "Task : ",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w600),
                                              ),
                                              Text(
                                                commit.task,
                                                style: const TextStyle(
                                                    fontWeight: FontWeight.w600),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              const Text("Comment : ",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w600),),
                                              Column(
                                                children: [
                                                  Text(commit.des),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ));
                              },
                            )
                          : const Center(
                              child: Padding(
                                padding: EdgeInsets.all(15.0),
                                child: Text("Empty"),
                              ),
                            ),
                      SizedBox(height: 220,),

                    ],
                  ),
                ),
                if (cont.date.day == DateTime.now().day &&
                    cont.date.month == DateTime.now().month &&
                    cont.date.year == DateTime.now().year)
                  Positioned(
                    bottom: 0,
                    // height: 150,
                    width: Get.width,
                    child: Container(
                      color: Colors.white,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: TextField(
                                    keyboardType: TextInputType.multiline,
                                    controller: commentcont,
                                    maxLines: 2,
                                    decoration: InputDecoration(
                                      labelText: "Comment",
                                      fillColor: Colors.white,
                                      border: OutlineInputBorder(
                                        borderSide: const BorderSide(
                                            color: Colors.black, width: 2.0),
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                DropdownButton(
                                    value: value,
                                    items: <String>["1st Half", "2nd Half"]
                                        .map<DropdownMenuItem<String>>(
                                            (String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                                    onChanged: (e) {
                                      setState(() {
                                        value = e.toString();
                                      });
                                    }),
                              ],
                            ),
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: InkWell(
                                    onTap: () => _showDialog(
                                      CupertinoPicker(
                                        backgroundColor: Colors.white,
                                        itemExtent: 30,
                                        scrollController: FixedExtentScrollController(
                                            initialItem: 0),
                                        children: [
                                          Text("select 1"),
                                          for (var i in taskcont.tasks)
                                            Text(i.name),
                                        ],
                                        onSelectedItemChanged: (value) {
                                          setState(() {
                                            task = taskcont.tasks[value - 1];
                                          });
                                        },
                                      ),
                                    ),
                                    child: Container(
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                            color: MyResources.themecolor,
                                            width: 1.5),
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                      child: Row(
                                        children: [
                                          Expanded(
                                            child: Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: Text(
                                                task == null
                                                    ? "Select Task"
                                                    : task!.name,
                                                style: TextStyle(
                                                    color: MyResources.themecolor),
                                              ),
                                            ),
                                          ),
                                          if (task != null)
                                            Row(
                                              children: [
                                                Transform.scale(
                                                  scale: 0.75,
                                                  child: CupertinoSwitch(
                                                      value: task!.status,
                                                      onChanged: (e) {
                                                              showDialog(
                                                                  context: context,
                                                                  builder:
                                                                      (_) =>
                                                                          AlertDialog(
                                                                            content:
                                                                                DeletepopUp(
                                                                              message:
                                                                                  "Are you sure?",
                                                                              pressFunc:
                                                                                  () async {
                                                                                setState(
                                                                                    () {
                                                                                  task!.status =
                                                                                      e;
                                                                                });
                                                                                try {
                                                                                  await taskcont.updateTask(task!);
                                                                                  Get.snackbar("Completed",
                                                                                      "This task mark "+ (e? "completed":"Incomplete"),
                                                                                      snackPosition: SnackPosition.BOTTOM);
                                                                                } catch (ee) {
                                                                                  setState(() {
                                                                                    task!.status = !e;
                                                                                  });
                                                                                }

                                                                                Navigator.pop(
                                                                                    context);
                                                                              },
                                                                              isNotDel:
                                                                                  true,
                                                                            ),
                                                                          ));
                                                            }),
                                                ),
                                                const Text(
                                                  """Mark as 
Completed""",
                                                  style: TextStyle(fontSize: 11),
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                              ],
                                            )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(primary: MyResources.headingColor),
                                    onPressed: (){
                                  Get.to(AddTask());
                                }, child: Icon(Icons.add)),
                              ),
                            ],
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 8.0),
                            child: ElevatedButton(
                                onPressed: () {
                                  if (task == null) {
                                    Get.snackbar(
                                        "Warning", "Please select a task");
                                  } else if (commentcont.text.length < 10) {
                                    Get.snackbar("Warning",
                                        "Please write a comment of minimum 10 letters");
                                  } else {
                                    showDialog(
                                        context: context,
                                        builder: (_) => AlertDialog(
                                              content: DeletepopUp(
                                                message: "Are you sure?",
                                                pressFunc: () async {
                                                  if (cont.todaylog == null) {
                                                    if (value == "1st Half") {
                                                      first.add(Commit(
                                                          des: commentcont.text,
                                                          task: task!.name,
                                                          taskid: task!.id!));
                                                    } else if (value ==
                                                        "2nd Half") {
                                                      second.add(Commit(
                                                          des: commentcont.text,
                                                          task: task!.name,
                                                          taskid: task!.id!));
                                                    }
                                                    await cont.addCommit(
                                                        task!.id!,
                                                        task!.name,
                                                        first,
                                                        second);
                                                  } else {
                                                    if (value == "1st Half") {
                                                      cont.todaylog!.firsthalf
                                                          .add(Commit(
                                                              des: commentcont
                                                                  .text,
                                                              task: task!.name,
                                                              taskid:
                                                                  task!.id!));
                                                    } else if (value ==
                                                        "2nd Half") {
                                                      cont.todaylog!.secondhalf
                                                          .add(Commit(
                                                              des: commentcont
                                                                  .text,
                                                              task: task!.name,
                                                              taskid:
                                                                  task!.id!));
                                                    }
                                                    await cont.updateCommit(
                                                        cont.todaylog!);
                                                  }

                                                  commentcont.clear();
                                                  Navigator.pop(context);
                                                },
                                                isNotDel: true,
                                              ),
                                            ));
                                  }
                                },
                                style: ElevatedButton.styleFrom(
                                    primary: MyResources.themecolor),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Text("Save"),
                                  ],
                                )),
                          ),
                        ],
                      ),
                    ),
                  ),
              ],
            ));
  }
}
