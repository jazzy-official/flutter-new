
import 'dart:convert';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/RecievePaidChecklist/receive_paid_checklist.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/Recovery/recovery.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/Units%20Model/units.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/account%20model/accounts.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Payables/Model/payable_model.dart';
import 'package:rafi_admin/Components/Screens/Inventory/Models/Unit%20List/unit_list_model.dart';
import 'package:rafi_admin/Components/Screens/Inventory/Models/Unit%20Model/unit_model.dart';
import 'package:rafi_admin/Controllers/authcontroller.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UnitsController extends GetxController {
  final auth = Get.put(AuthController());
  String url = 'https://raccounting.azurewebsites.net/api/Units?pageNumber=1&pageSize=100';
  UnitListModel? unitListModel;
  String? token;
  int? number;
  List<UnitModel> results = [];
  List<UnitModel> results1 = [];
  List<UnitModel> results2 = [];
  List<UnitModel> results3 = [];
  List<UnitModel> results4 = [];
  List<UnitModel> results5 = [];
  bool isStatus = false;
  bool isUnit = false;
  bool isMarla = false;
  bool isRate = false;
  bool isNetRate = false;
  @override
  Future<void> onInit() async {
    await refreshAll();
    super.onInit();
  }

  Future refreshAll() async {
    token = await auth.getToken();
    number = await auth.getProjectId();
    await getList();

  }


  Future getList() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String token  = prefs.getString("token").toString();
    http.Response response = await http.get(
      Uri.parse(url + "&&projectId=$number"),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        "Authorization" : "Bearer "+token,
      },
    );
    if (response.statusCode == 200) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      unitListModel = UnitListModel.fromJson(jsonDecode(response.body));
      results = unitListModel!.units;
      update();
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to get recovery data');
    }
  }

  Future<void> runStatusFilter(String enteredKeyword) async {
    if (enteredKeyword.isEmpty) {
      // if the search field is empty or only contains white-space, we'll display all users

        isStatus = false;

      results1 = unitListModel!.units;
    } else {

        isStatus = true;

      results1 = unitListModel!.units
          .where((unit) => unit.status
          .toUpperCase()
          .contains(enteredKeyword.toUpperCase()))
          .toList();
      // we use the toLowerCase() method to make it case-insensitive
    }
    // Refresh the UI
  }
  Future<void> runUnitFilter(String unit,) async {
    if (unit.isEmpty) {

        isUnit = false;

      // if the search field is empty or only contains white-space, we'll display all users
      results2 = results1;
    } else {

        isUnit = true;

      results2 = results1
          .where((user) => user.unitNo
          .toString()
          .toUpperCase()
          == unit.toUpperCase())
          .toList();
      // we use the toLowerCase() method to make it case-insensitive
        update();
    } // Refresh the UI
    // Refresh the UI
  }
  Future<void> runMarlaFilter(String unit,) async {
    if (unit.isEmpty) {

        isMarla = false;

      // if the search field is empty or only contains white-space, we'll display all users
      results3 = results2;
    } else {

        isMarla = true;

      double marlaVal =  double.parse(unit);
      results3 = results2
          .where((user) =>  marlaVal - 1 <= user.unitMarla && user.unitMarla <= marlaVal +1)
          .toList();
      // we use the toLowerCase() method to make it case-insensitive
        update();
    } // Refresh the UI
    // Refresh the UI
  }
  Future<void> runRateFilter(String unit,) async {
    if (unit.isEmpty) {

        isRate = false;

      // if the search field is empty or only contains white-space, we'll display all users
      results4 = results3;
    } else {

        isRate = true;

      double rateVal =  double.parse(unit);
      results4 = results3
          .where((user) =>  rateVal - 100000 <= user.rate && user.rate <= rateVal +100000)
          .toList();
      // we use the toLowerCase() method to make it case-insensitive
    } // Refresh the UI
    results = results4;
    update();
    // Refresh the UI
  }
  Future<void> runNetRateFilter(String unit,) async {
    if (unit.isEmpty) {

        isNetRate = false;

      // if the search field is empty or only contains white-space, we'll display all users
      results5 = results4;
    } else {

        isNetRate = true;

      double rateVal =  double.parse(unit);
      results5 = results4
          .where((user) =>  rateVal - 500000 <= user.netAmount && user.netAmount <= rateVal +500000)
          .toList();
      // we use the toLowerCase() method to make it case-insensitive
    } // Refresh the UI
    results = results5;
    update();
    // Refresh the UI
  }

}
