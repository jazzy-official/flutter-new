import 'dart:async';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:ionicons/ionicons.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Screens/Home/Widgets/feature_list_room_card.dart';
import 'package:room_buddy/controllers/user_detail_controller.dart';
import 'package:shimmer/shimmer.dart';

import '../../Models/user_model.dart';

class RoommateDetailScreen extends StatefulWidget {
  final int id;
  const RoommateDetailScreen({Key? key, required this.id}) : super(key: key);


  @override
  State<RoommateDetailScreen> createState() => _RoommateDetailScreenState();
}

class _RoommateDetailScreenState extends State<RoommateDetailScreen> {
  int currentPos = 0;
  bool descTextShowFlag = false;
  bool isLoading = true;
  late UserModel user;
  var usercont = Get.put(UserDetailController());



  @override
  void initState() {
    loaduser();
    super.initState();
  }

  Future<void> loaduser() async {
    setState(() {
      isLoading = true;
    });
    user = await usercont.fetchuser(widget.id);
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:
      isLoading ?
      Shimmer.fromColors(
        baseColor: Colors.grey.shade300,
        highlightColor: Colors.grey.shade100,
        enabled: true,
        child: ListView(

          children: [
            Container(
              height: 250,
              width: double.infinity,
              color: Colors.white,
            ),
            Padding(
              padding: EdgeInsets.all(15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                   Container(
                    height: 12,
                    width: 150,
                    color: Colors.white,
                  ),
                  const SizedBox(height: 10,),
                  Container(
                    height: 15,
                    width: double.infinity,
                    color: Colors.white,
                  ),
                  const SizedBox(height: 5,),
                  Container(
                    height: 15,
                    width: 50,
                    color: Colors.white,
                  ),
                  const SizedBox(height: 5,),
                  Container(
                    height: 15,
                    width: 200,
                    color: Colors.white,
                  ),
                  const SizedBox(height: 5,),
                  Container(
                    height: 15,
                    width: 200,
                    color: Colors.white,
                  ),
                  const SizedBox(height: 15,),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        height: 15,
                        margin: EdgeInsets.only(bottom: 7),
                        width: double.infinity,
                        color: Colors.white,
                      ),
                      Container(
                        height: 15,
                        margin: EdgeInsets.only(bottom: 7),
                        width: double.infinity,
                        color: Colors.white,
                      ),
                      Container(
                        height: 15,
                        margin: EdgeInsets.only(bottom: 7),
                        width: double.infinity,
                        color: Colors.white,
                      ),
                      Container(
                        height: 15,
                        margin: EdgeInsets.only(bottom: 7),
                        width: double.infinity,
                        color: Colors.white,
                      ),
                      Container(
                        height: 15,
                        margin: EdgeInsets.only(bottom: 7),
                        width: double.infinity,
                        color: Colors.white,
                      ),

                    ],
                  ),
                  const Divider(),
                  const SizedBox(height: 10,),
                  Container(
                    height: 15,
                    width: double.infinity,
                    color: Colors.white,
                  ),
                  SizedBox(height: 5,),
                  GridView.count(
                    controller: ScrollController(),
                    shrinkWrap: true,
                    childAspectRatio: 7/2,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 25,
                    crossAxisCount: 2,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: 12,
                            width: 80,
                            color: Colors.white,
                          ),
                          const SizedBox(height: 5,),
                          Container(
                            height: 15,
                            width: 150,
                            color: Colors.white,
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: 12,
                            width: 80,
                            color: Colors.white,
                          ),
                          const SizedBox(height: 5,),
                          Container(
                            height: 15,
                            width: 150,
                            color: Colors.white,
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: 12,
                            width: 80,
                            color: Colors.white,
                          ),
                          const SizedBox(height: 5,),
                          Container(
                            height: 15,
                            width: 150,
                            color: Colors.white,
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: 12,
                            width: 80,
                            color: Colors.white,
                          ),
                          const SizedBox(height: 5,),
                          Container(
                            height: 15,
                            width: 150,
                            color: Colors.white,
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: 12,
                            width: 80,
                            color: Colors.white,
                          ),
                          const SizedBox(height: 5,),
                          Container(
                            height: 15,
                            width: 150,
                            color: Colors.white,
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: 12,
                            width: 80,
                            color: Colors.white,
                          ),
                          const SizedBox(height: 5,),
                          Container(
                            height: 15,
                            width: 150,
                            color: Colors.white,
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: 12,
                            width: 80,
                            color: Colors.white,
                          ),
                          const SizedBox(height: 5,),
                          Container(
                            height: 15,
                            width: 150,
                            color: Colors.white,
                          ),
                        ],
                      ),

                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      )
          :SafeArea(
        child: CustomScrollView(
          slivers:
          <Widget>[
            SliverAppBar(
              pinned: true,
              backgroundColor: myResources.themeColor,
              title: Text(user.name),
              expandedHeight: 250,
              actions: [
                IconButton(icon: Icon(Ionicons.share_social,color: Colors.white.withOpacity(0.9),), onPressed: () {  },),
                IconButton(icon: Icon(Ionicons.ellipsis_vertical,color: Colors.white.withOpacity(0.9),), onPressed: () {  },),

              ],
              stretch: true,
              flexibleSpace: FlexibleSpaceBar(
                stretchModes: <StretchMode>[
                  StretchMode.zoomBackground,
                  StretchMode.fadeTitle,
                ],
                background: Stack(
                  children: [
                    Container(
                      height: 250,
                      width: Get.width,
                      decoration:  BoxDecoration(
                        image:DecorationImage(image: AssetImage(user.profilePic),fit: BoxFit.cover,alignment: Alignment.topCenter) ,
                      ),
                    ),
                    // CarouselSlider(
                    //   options: CarouselOptions(
                    //       height: 1000,
                    //       autoPlay: true,
                    //       pauseAutoPlayOnManualNavigate: true,
                    //       viewportFraction: 1,
                    //       onPageChanged: (index, reason) {
                    //         setState(() {
                    //           currentPos = index;
                    //         });
                    //       }
                    //   ),
                    //   items: imgList
                    //       .map((item) => Image.network(item, fit: BoxFit.cover,))
                    //       .toList(),
                    // ),

                  ],
                ),
              ) ,
            ),
            SliverList(
              delegate:  SliverChildListDelegate([
                Padding(
                  padding: EdgeInsets.all(15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                       Text("${user.city},",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                      const SizedBox(height: 10,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                           Text("${user.name}, 22",style: const TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
                          Row(
                            children: [
                              Icon(Icons.verified_user_sharp,color: myResources.semiColor,size: 20,),
                              Text(" Verified",style: TextStyle(color: myResources.semiColor,fontSize: 13,fontWeight: FontWeight.w600),)
                            ],
                          )
                        ],
                      ),
                      const SizedBox(height: 5,),
                      const Text("Male",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                      const SizedBox(height: 5,),
                      Row(
                        children: const [
                          Icon(Ionicons.briefcase,color: Colors.grey,size: 20,),
                          Text("  Software Engineer",style: TextStyle(fontWeight: FontWeight.w600,color: Colors.grey,),),
                        ],
                      ),
                      const SizedBox(height: 5,),
                      Row(
                        children: [
                          Icon(Ionicons.globe,color: Colors.grey,size: 20,),
                          const Text("  Speak English & Urdu",style: TextStyle(fontWeight: FontWeight.w600,color: Colors.grey,),),
                        ],
                      ),
                      const SizedBox(height: 15,),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(user.profileBio,
                            maxLines: descTextShowFlag ? 8 : 5,textAlign: TextAlign.start,
                            style: TextStyle(color: Colors.grey),),
                          InkWell(
                            onTap: (){ setState(() {
                              descTextShowFlag = !descTextShowFlag;
                            }); },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                descTextShowFlag ? Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                                  child: Text("Show less",style: TextStyle(color: myResources.themeColor,fontWeight: FontWeight.bold),),
                                ) :  Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                                  child: Text("Continue Reading",style: TextStyle(color: myResources.themeColor,fontWeight: FontWeight.bold),),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                      const Divider(),
                      const SizedBox(height: 10,),
                      const Text("Room Overview",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
                      GridView.count(
                        controller: ScrollController(),
                        shrinkWrap: true,
                        childAspectRatio: 7/2,
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 25,
                        crossAxisCount: 2,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text("Budget",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              const SizedBox(height: 5,),
                              const Text("Rs. 20,000 / month",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text("Availability",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              SizedBox(height: 5,),
                              Text("20 Feb",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text("Room requirement",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              const SizedBox(height: 5,),
                              const Text("1 private Bedrrom",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text("Bathrooms",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              const SizedBox(height: 5,),
                              const Text("1 attach bathroom",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text("Furnishing",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              SizedBox(height: 5,),
                              Text("fully furnished",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text("Stay Period",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              SizedBox(height: 5,),
                              Text("6 months",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text("Prefered locations",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              const SizedBox(height: 5,),
                              const Text("Los Angeles, Paris",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),

                        ],
                      ),

                      const Padding(
                        padding: EdgeInsets.symmetric(vertical: 20.0),
                        child: Divider(),
                      ),
                      const Text("About Me",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
                      GridView.count(
                        controller: ScrollController(),
                        shrinkWrap: true,
                        childAspectRatio: 8/4,
                        crossAxisSpacing: 35,
                        mainAxisSpacing: 0,
                        crossAxisCount: 2,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text("Gender",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              const SizedBox(height: 5,),
                              const Text("Male",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text("Age",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              const SizedBox(height: 5,),
                              const Text("22",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text("Martial Status",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              SizedBox(height: 5,),
                              Text("Single",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text("Food choice",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              const SizedBox(height: 5,),
                              const Text("Non-vegetarian",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text("Smoking",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              SizedBox(height: 5,),
                              Text("Occasionally",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text("Alcohol",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              const SizedBox(height: 5,),
                              const Text("Never",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text("Partying",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              SizedBox(height: 5,),
                              Text("Occassionally",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text("Guests",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              SizedBox(height: 5,),
                              Text("Occasionally",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text("Pets",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              SizedBox(height: 5,),
                              Text("Pet friendly",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text("Occupation",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              SizedBox(height: 5,),
                              Text("Working Professional",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text("Interests",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              SizedBox(height: 5,),
                              Expanded(child: Text("Football,anime,game,music,movies,memes,singing,guitar",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),)),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text("Personality Traits",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              SizedBox(height: 5,),
                              Text("Calm, organized, friendly, fun, caring, easy going",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                        ],
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(vertical: 20.0),
                        child: Divider(),
                      ),
                      // const Text("Desired Amenities",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
                      // GridView.count(
                      //   controller: ScrollController(),
                      //   shrinkWrap: true,
                      //   childAspectRatio: 7/2,
                      //   crossAxisSpacing: 10,
                      //   mainAxisSpacing: 25,
                      //   crossAxisCount: 2,
                      //   children: [
                      //     Row(
                      //       children: [
                      //         Container(
                      //           height: 45,
                      //           width: 45,
                      //           decoration: BoxDecoration(
                      //               border: Border.all(color: myResources.themeColor,width: 2),
                      //               borderRadius: BorderRadius.circular(40)
                      //           ),
                      //           child: Icon(Ionicons.shield_checkmark_outline,color:myResources.themeColor,),
                      //         ),
                      //         SizedBox(width: 10,),
                      //         Expanded(child: Text("WIFI",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),)),
                      //       ],
                      //     ),
                      //     Row(
                      //       children: [
                      //         Container(
                      //           height: 45,
                      //           width: 45,
                      //           decoration: BoxDecoration(
                      //               border: Border.all(color: myResources.themeColor,width: 2),
                      //               borderRadius: BorderRadius.circular(40)
                      //           ),
                      //           child: Icon(Ionicons.shield_checkmark_outline,color:myResources.themeColor,),
                      //         ),
                      //         SizedBox(width: 10,),
                      //         Text("Elevator",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                      //       ],
                      //     ),
                      //     Row(
                      //       children: [
                      //         Container(
                      //           height: 45,
                      //           width: 45,
                      //           decoration: BoxDecoration(
                      //               border: Border.all(color: myResources.themeColor,width: 2),
                      //               borderRadius: BorderRadius.circular(40)
                      //           ),
                      //           child: Icon(Ionicons.shield_checkmark_outline,color:myResources.themeColor,),
                      //         ),
                      //         SizedBox(width: 10,),
                      //         Text("Cooktop",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                      //       ],
                      //     ),
                      //     Row(
                      //       children: [
                      //         Container(
                      //           height: 45,
                      //           width: 45,
                      //           decoration: BoxDecoration(
                      //               border: Border.all(color: myResources.themeColor,width: 2),
                      //               borderRadius: BorderRadius.circular(40)
                      //           ),
                      //           child: Icon(Ionicons.shield_checkmark_outline,color:myResources.themeColor,),
                      //         ),
                      //         SizedBox(width: 10,),
                      //         Text("A.C",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                      //       ],
                      //     ),
                      //     Row(
                      //       children: [
                      //         Container(
                      //           height: 45,
                      //           width: 45,
                      //           decoration: BoxDecoration(
                      //               border: Border.all(color: myResources.themeColor,width: 2),
                      //               borderRadius: BorderRadius.circular(40)
                      //           ),
                      //           child: Icon(Ionicons.shield_checkmark_outline,color:myResources.themeColor,),
                      //         ),
                      //         SizedBox(width: 10,),
                      //         Text("Parking",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                      //       ],
                      //     ),
                      //
                      //
                      //   ],
                      // ),
                      // const Padding(
                      //   padding: EdgeInsets.symmetric(vertical: 20.0),
                      //   child: Divider(),
                      // ),
                      const Text("Compatible with",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
                      GridView.count(
                        controller: ScrollController(),
                        shrinkWrap: true,
                        childAspectRatio: 8/4,
                        crossAxisSpacing: 35,
                        mainAxisSpacing: 0,
                        crossAxisCount: 2,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text("Gender",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              SizedBox(height: 5,),
                              Text("Female",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text("Age",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              const SizedBox(height: 5,),
                              const Text("20 - 30 years old",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text("Martial Status",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              SizedBox(height: 5,),
                              Text("Single",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text("Food choice",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              const SizedBox(height: 5,),
                              const Text("Non-vegetarian",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text("Smoking",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              SizedBox(height: 5,),
                              Text("Occasionally",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text("Alcohol",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              const SizedBox(height: 5,),
                              const Text("Never",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text("Partying",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              SizedBox(height: 5,),
                              Text("Occassionally",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text("Guests",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              SizedBox(height: 5,),
                              Text("Occasionally",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text("Pets",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              SizedBox(height: 5,),
                              Text("Not having pets",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text("Occupation",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w600),),
                              SizedBox(height: 5,),
                              Text("Working Professional",style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16),),
                            ],
                          ),
                        ],
                      ),
                      const Padding(
                        padding: EdgeInsets.only(bottom: 20.0),
                        child: Divider(color: Colors.black54,),
                      ),
                      SizedBox(height: 50,),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 15.0),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: myResources.themeColor,
                            shape: const RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(14))),
                          ),
                          onPressed: () {
                            // Get.to(()=> const HabitsForm());
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 8),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                                  decoration: BoxDecoration(
                                    borderRadius:
                                    const BorderRadius.all(Radius.circular(20)),
                                    color: myResources.themeColor,
                                  ),
                                  child: Icon(
                                    Ionicons.chatbox_ellipses,
                                    color: Colors.white,
                                    size: 24,
                                  ),
                                ),
                                Text(
                                  'Send a chat Request',
                                  style: TextStyle(color: Colors.white),
                                ),

                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ]),
            ),

          ],

        ),
      ),
    );
  }
}
