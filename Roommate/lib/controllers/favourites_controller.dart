import 'dart:core';
import 'package:get/get.dart';
import 'package:room_buddy/Models/about_user_model.dart';
import 'package:room_buddy/Models/habits_modal.dart';
import 'package:room_buddy/Models/location_model.dart';
import 'package:room_buddy/Models/room_model.dart';
import 'package:room_buddy/Models/user_model.dart';
import 'package:room_buddy/controllers/profile_controller.dart';

import 'explore_controller.dart';

class FavouritesController extends GetxController {
  var expcont = Get.put(ExplorerController());
  var profilecont = Get.put(ProfileController());


  List<Room>? get favrooms  => expcont.rooms.where((element) => profilecont.user!.favIds.contains(element.id)).toList();


  @override
  Future<void> onInit() async {
    super.onInit();
  }



}
