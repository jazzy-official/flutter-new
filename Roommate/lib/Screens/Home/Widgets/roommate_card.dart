import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:room_buddy/Screens/Home/roommate_detail.dart';

class RoommateCard extends StatelessWidget {
  const RoommateCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      // onTap: (){Get.to(()=> const RoommateDetailScreen());},
      child: Card(
        elevation: 3,
        child: SizedBox(
          width: 250,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 200,
                width: 250,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(3),
                    image: const DecorationImage(
                        image: AssetImage(
                          "assets/me.jpg",
                        ),
                        fit: BoxFit.cover)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.all(10),
                      height: 25,
                      width: 25,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: Colors.white),
                      child: Center(child: Icon(Ionicons.heart,size: 18,color: Colors.red,),),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 10,),
                    const Text("Muhhammad Jahanzaib, 22",softWrap: false,
                      overflow: TextOverflow.fade,style: TextStyle(fontSize: 16,fontWeight: FontWeight.w600),),
                    const SizedBox(height: 5,),
                    const Text("Bahria town phase 2",style: TextStyle(fontSize: 14,color: Colors.black54,fontWeight: FontWeight.w600),),
                    const SizedBox(height: 5,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text("Private Room  ",style: TextStyle(fontSize: 13,color: Colors.black45,),),
                        Row(children: const [
                          Icon(Ionicons.ellipse,size: 6,color: Colors.black45,),
                          Text("  Immediate",style: TextStyle(fontSize: 13,color: Colors.black45,),),
                        ],),
                      ],
                    ),
                    const SizedBox(height: 5,),
                    Row(
                      children: [
                        Text("Budget : ",style: TextStyle(fontSize: 14,color: Colors.black45,fontWeight: FontWeight.w500),),
                        const Text("\$15,000 / month",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold,),),
                      ],
                    ),

                  ],
                ),
              )

            ],
          ),
        ),
      ),
    );
  }
}
