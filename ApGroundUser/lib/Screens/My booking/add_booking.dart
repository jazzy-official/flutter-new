import 'package:ap_booking_user/Screens/Authentication/Models/user_model.dart';
import 'package:ap_booking_user/Screens/Grounds/Models/ground_models.dart';
import 'package:ap_booking_user/Screens/Grounds/Models/playing_slot_model.dart';
import 'package:ap_booking_user/Screens/Grounds/Models/slot_model.dart';
import 'package:ap_booking_user/Screens/Grounds/Models/template_model.dart';
import 'package:ap_booking_user/Screens/My%20booking/confirm_booking_dialogue.dart';
import 'package:ap_booking_user/Screens/My%20booking/payment_screen.dart';
import 'package:ap_booking_user/Screens/My%20booking/slot_card.dart';
import 'package:ap_booking_user/Screens/Payment%20Method/payment_screen.dart';
import 'package:ap_booking_user/Screens/Shared/loading_widget.dart';
import 'package:ap_booking_user/constants/util.dart';
import 'package:ap_booking_user/controllers/booking_controller.dart';
import 'package:ap_booking_user/controllers/firebase_controllers.dart';
import 'package:ap_booking_user/controllers/user_controller.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'Models/booking_model.dart';

class AddBooking extends StatefulWidget {
  const AddBooking({Key? key}) : super(key: key);

  @override
  _AddBookingState createState() => _AddBookingState();
}

class _AddBookingState extends State<AddBooking> {
  void _showDatePicker(ctx) {
    showCupertinoModalPopup(
        context: ctx,
        builder: (_) => Container(
              height: 353,
              color: Color.fromARGB(255, 255, 255, 255),
              child: Column(
                children: [
                  Container(
                    height: 300,
                    child: CupertinoDatePicker(
                        initialDateTime: _chosenDateTime,
                        mode: CupertinoDatePickerMode.date,
                        onDateTimeChanged: (val) {
                          // print(val);
                          print(DateTime.parse(DateFormat('yyyy-MM-dd').format(DateTime.now())));
                          if(val.isBefore(DateTime.parse(DateFormat('yyyy-MM-dd').format(DateTime.now()))) ){
                            displayToast("Can't select this date");
                          }else{
                            setState(() {
                              _chosenDateTime = val;
                            });
                            updateslots();
                            updateground();
                          }


                          // print(
                          //   DateFormat('EEE').format(_chosenDateTime!),
                          // );
                        }),
                  ),

                  // Close the modal
                  CupertinoButton(
                    child: Text('OK'),
                    onPressed: () => Navigator.of(ctx).pop(),
                  )
                ],
              ),
            ));
  }

  final DatabaseReference databaseReference =
      FirebaseDatabase.instance.reference().child("Grounds");

  Future<List<Ground>> readData() async {
    List<Ground> grounds = [];
    await databaseReference.once().then((DataSnapshot snapshot) {
      snapshot.value.forEach((key, value) {
        Ground gr = Ground.fromJson(value, key);
        grounds.add(gr);
      });
    });
    return grounds;
  }
  String bookingId = "";
  late UserModel user;
  late List<Ground> grounds;
  Ground? selectedGround = null;
  List<Booking> tembookings=[];
  List<Slot>? slots;
  List<Slot>? bookedSlots;
  int totalprice = 0;
  int slotvalue = 1;
  bool isLoading = true;
  void refresh() async {
    setState(() {
      isLoading = true;
    });
    grounds = await readData();
    user =
        await UserController().readData(FirebaseAuth.instance.currentUser!.uid);
    if(selectedGround==null) {
      setState(() {
        selectedGround = grounds.first;
      });
    } else{
      setState(() {
        selectedGround = grounds.where((element) => element.id==selectedGround!.id).first;
      });
    }
    updateground();
    setState(() {
      isLoading = false;
    });
  }

  void updateslots() {
    setState(() {
      slots = selectedGround!.schedule[_chosenDateTime!.weekday - 1].slots;
    });
  }

  Future<void> getBookedSlots() async {
    bookedSlots = [];
    List<Booking>? todayBookings = [];
    List<Booking> bookings = await BookingController().readAllBookings();
    todayBookings = bookings
        .where(
          (element) =>
              DateFormat('dd-MMM-yyyy').format(element.date) ==
                  DateFormat('dd-MMM-yyyy').format(_chosenDateTime!) &&
              element.groundName == selectedGround!.name,
        )
        .toList();
    if (!todayBookings.isEmpty) {
      todayBookings.forEach((element) {
        bookedSlots!.add(element.slot);
      });
      setState(() {
        tembookings=todayBookings!;
      });
    }

    print(todayBookings.length);
  }

  Future<void> updateground() async {
    await getBookedSlots();
    updateslots();
    matchslots();
    setState(() {
      selectedSlot = null;
    });
  }
void getBookingid(){
    List<Booking> book = tembookings.where((element) => element.slot.startTime==selectedSlot!.startTime).toList();
    bookingId = book[0].id!;
}
  void matchslots() {
    if (bookedSlots != null) {
      for (int i = 0; i < bookedSlots!.length; i++) {
        for (int j = 0; j < slots!.length; j++) {
          if (bookedSlots![i].startTime == slots![j].startTime) {
            setState(() {
              slots![j] = bookedSlots![i];
            });
            break;
          }
        }
      }
    }
    if(slotvalue==1){
      slots!.removeWhere((element) => !(element.Pslot2==null||element.Pslot1==null));

    }else if(slotvalue==2){
      slots!.removeWhere((element) => !(element.Pslot2==null&&element.Pslot1==null));
    }
  }

  @override
  void initState() {
    refresh();
    super.initState();
  }

  DateTime? _chosenDateTime = DateTime.now();
  int value = 0;
  bool meal = false;
  Slot? selectedSlot=null;
  late Booking booking;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyResources.buttonColor,
        title: Text("Booking"),
        centerTitle: true,
      ),
      body: isLoading
          ? LoadingWidget()
          : SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        TextButton(
                            onPressed: () async {
                              showCupertinoModalPopup(
                                  context: context,
                                  builder: (_) => Container(
                                        height: 353,
                                        color:
                                            Color.fromARGB(255, 255, 255, 255),
                                        child: Column(
                                          children: [
                                            Container(
                                              height: 300,
                                              child: CupertinoPicker(
                                                  children: [
                                                    Text("Cancel"),
                                                    for (var tem in grounds)
                                                      Text(tem.name)
                                                  ],
                                                  onSelectedItemChanged: (val) {
                                                    setState(() {
                                                      setState(() {
                                                        value = val;
                                                      });
                                                    });
                                                  },
                                                  itemExtent: 25,
                                                  diameterRatio: 1,
                                                  useMagnifier: true,
                                                  magnification: 1.3,
                                                  squeeze: 2),
                                            ),

                                            // Close the modal
                                            CupertinoButton(
                                              child: Text('OK'),
                                              onPressed: () async => {
                                                if (value != 0)
                                                  {
                                                    setState(() {
                                                      selectedGround =
                                                          grounds[value - 1];
                                                    }),
                                                  },
                                                updateground(),
                                                Navigator.of(context).pop(),
                                              },
                                            )
                                          ],
                                        ),
                                      ));
                            },
                            child: Container(
                              width:
                                  MediaQuery.of(context).size.width * 0.65 - 29,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                    color: MyResources.buttonColor, width: 2),
                              ),
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 10),
                                child: Center(
                                    child: Text(
                                  selectedGround == null
                                      ? "Select Ground"
                                      : selectedGround!.name,
                                  style:
                                      TextStyle(color: MyResources.buttonColor),
                                )),
                              ),
                            )),
                        TextButton(
                            onPressed: () {
                              _showDatePicker(context);
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                    color: MyResources.buttonColor, width: 2),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text(
                                  DateFormat('dd-MMM-yyyy')
                                      .format(_chosenDateTime!),
                                  style:
                                      TextStyle(color: MyResources.buttonColor),
                                ),
                              ),
                            )),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: Row(
                        children: [
                          const Text(
                            "Slots : ",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          Expanded(
                            child: ListTile(
                              title: const Text('One'),
                              leading: Radio<int>(
                                value: 1,
                                groupValue: slotvalue,
                                onChanged: (value) {
                                  setState(() {
                                    slotvalue = value!;
                                  });
                                  refresh();
                                },
                              ),
                            ),
                          ),
                          Expanded(
                            child: ListTile(
                              title: const Text('Two'),
                              leading: Radio<int>(
                                value: 2,
                                groupValue: slotvalue,
                                onChanged: (value) {
                                  setState(() {
                                    slotvalue = value!;
                                  });
                                  refresh();
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Column(
                      children: [
                        Row(
                          children: [
                            Text(
                              " Available Slots on :     ",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 15),
                            ),
                            Text(
                              DateFormat('EEE').format(_chosenDateTime!) + "  ",
                              style: const TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color: Colors.blue),
                            ),
                            Text(
                                DateFormat('dd-MMM-yyyy')
                                    .format(_chosenDateTime!),
                                style: TextStyle(fontWeight: FontWeight.w500)),
                          ],
                        ),
                        (slots == null||slots!.isEmpty)
                            ? Container(
                                height: 100,
                                child: Center(child: Text("No slot Available try another date")),
                              )
                            : slotList(slots!),
                        Container()
                      ],
                    ),
                    TextButton(
                        onPressed: () {
                          setState(() {
                            meal = !meal;
                          });
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border.all(
                                width: 1, color: MyResources.buttonColor),
                            borderRadius: BorderRadius.circular(6),
                            color: meal
                                ? MyResources.buttonColor
                                : Colors.transparent,
                          ),
                          padding:
                              EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                          child: Text(
                            "Include Meal",
                            style: TextStyle(
                                color: meal
                                    ? Colors.white
                                    : MyResources.buttonColor),
                          ),
                        )),
                    if (selectedSlot != null)
                      Container(
                        padding: EdgeInsets.all(8),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  slotvalue.toString() + " slot",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text("Rs/ " +
                                    ((selectedSlot!.price) * slotvalue)
                                        .toString()),
                              ],
                            ),
                            Divider(
                              color: Colors.grey,
                              thickness: 0.5,
                            ),
                            if (meal)
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Meal",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  Text("Rs/ "+(selectedGround!.mealPrice*slotvalue).toString()),
                                ],
                              ),
                            if (meal)
                              Divider(
                                color: Colors.grey,
                                thickness: 0.5,
                              ),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Total",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  "Rs/ " +
                                      (((selectedSlot!.price) * slotvalue) +
                                              (meal ? selectedGround!.mealPrice*slotvalue : 0))
                                          .toString(),
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    Center(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 25.0),
                        child: ElevatedButton(
                          onPressed: () {
                            if(selectedSlot == null){
                              displayToast("Please select a slot first");
                            }else {
                              PlayingSlot pslot = PlayingSlot(
                                  paid: 0,
                                  total: meal ? selectedSlot!.price +
                                      selectedGround!.mealPrice : selectedSlot!
                                      .price,
                                  teamId: user.uid,
                                  teamName: user.name,
                                  teamRole: user.role,
                                  meal: meal);
                              bool isAlreadybook = selectedSlot!.Pslot1 != null;
                              late Slot slot;
                              if (slotvalue == 1) {
                                slot = isAlreadybook ? Slot(
                                  startTime: selectedSlot!.startTime,
                                  price: selectedSlot!.price,
                                  hours: selectedSlot!.hours,
                                  Pslot1: selectedSlot!.Pslot1,
                                  Pslot2: pslot,
                                ) : Slot(
                                  startTime: selectedSlot!.startTime,
                                  price: selectedSlot!.price,
                                  hours: selectedSlot!.hours,
                                  Pslot1: pslot,
                                );
                                if (isAlreadybook) {
                                  getBookingid();
                                }
                              } else {
                                slot = Slot(
                                  startTime: selectedSlot!.startTime,
                                  price: selectedSlot!.price,
                                  hours: selectedSlot!.hours,
                                  Pslot1: pslot,
                                  Pslot2: pslot,
                                );
                              }
                              booking = isAlreadybook
                                  ? Booking(
                                  id: bookingId,
                                  groundName: selectedGround!.name,
                                  groundId: selectedGround!.id!,
                                  date: _chosenDateTime!,
                                  slot: slot,
                                  isCancel: false,
                                  groundAddress: selectedGround!.address,
                                  mealPrice: selectedGround!.mealPrice.toInt())
                                  : Booking(
                                  groundName: selectedGround!.name,
                                  groundId: selectedGround!.id!,
                                  date: _chosenDateTime!,
                                  slot: slot,
                                  isCancel: false,
                                  groundAddress: selectedGround!.address,
                                  mealPrice: selectedGround!.mealPrice.toInt());
                              totalprice = ((selectedSlot!.price) * slotvalue)
                                  .toInt() +
                                  (meal ? (selectedGround!.mealPrice).toInt() *
                                      slotvalue : 0);
                              showDialog(
                                  context: context,
                                  builder: (_) =>
                                      AlertDialog(
                                          content: Material(
                                              child: BookingConfirmPopUp(
                                                pressFunc: () {
                                                  Navigator.pop(context);
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            PaymentScreen(
                                                              alreadyBooked: isAlreadybook,
                                                              booking: booking,
                                                              totalPrice: totalprice,
                                                              mealPrice: selectedGround!
                                                                  .mealPrice
                                                                  .toInt(),
                                                              meal: meal,
                                                              slottype: slotvalue,
                                                            )),
                                                  );
                                                },
                                                booking: booking,
                                                totalPrice: totalprice,
                                                meal: meal,
                                              ))));
                            }
                          },
                          child: Text("Confirm"),
                          style: ElevatedButton.styleFrom(
                              primary: MyResources.buttonColor,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 100, vertical: 10),
                              textStyle: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold)),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    )
                  ],
                ),
              ),
            ),
    );
  }

  Widget slotList(List<Slot> slots) {
    return Column(
      children: [
        ListView.builder(
            shrinkWrap: true,
            padding: EdgeInsets.zero,
            physics: BouncingScrollPhysics(),
            itemCount: slots.length,
            itemBuilder: (BuildContext context, int index) {
              Slot slot = slots[index];
              return SlotCard(
                slot: slot,
                temSlot: selectedSlot,
                onSlotSelect: (Slot? value) {
                  setState(() {
                    selectedSlot = value;
                  });
                },
              );
            }),
      ],
    );
  }
}
