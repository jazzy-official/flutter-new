/**
 * File: directory_widget.dart
 * Package:
 * Project: tree_view
 * Author: Ajil Oommen (ajil@altorumleren.com)
 * Description:
 * Date: 06 January, 2019 2:04 PM
 */

import 'package:flutter/material.dart';

import '../utils/utils.dart';


class DirectoryWidget extends StatelessWidget {
  final String tilte;
  final String code;
  final double balance;
  final VoidCallback? onPressedNext;

  DirectoryWidget({
    required this.tilte,
    required this.code,
    required this.balance,
    this.onPressedNext,
  });

  @override
  Widget build(BuildContext context) {
    Widget titleWidget = Text(code+" | "+tilte);

    Widget folderIcon =  Container( width:5,child: Center(child: Icon(Icons.circle,size: 10,)),);

    IconButton expandButton = IconButton(
      icon: Icon(Icons.navigate_next),
      onPressed: onPressedNext,
    );

    Widget lastModifiedWidget = Text(
      "Balance: "+ balance.toString(),
    );

    return ListTile(
      minLeadingWidth: 15,
      leading: folderIcon,
      title: titleWidget,
      subtitle: lastModifiedWidget,
      trailing: expandButton,
      onTap: (()=> print(code)),
    );
  }
}