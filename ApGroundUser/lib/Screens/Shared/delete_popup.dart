import 'package:ap_booking_user/constants/util.dart';
import 'package:flutter/material.dart';

class DeletepopUp extends StatelessWidget {
  final String message;
  final VoidCallback pressFunc;
  final bool isNotDel;

  const DeletepopUp(
      {Key? key, required this.pressFunc, required this.message, required this.isNotDel})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(50),
              topRight: Radius.circular(50),
            )),
        width: MediaQuery.of(context).size.width * 0.8,
        height: 180,
        constraints: BoxConstraints(maxHeight: 200),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Icon(
                isNotDel ? Icons.warning : Icons.delete,
                color: Colors.red,
                size: 50,
              ),
              SizedBox(
                //width: ,
                child: Text(
                  message,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18),
                ),
                height: 50,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                      style: ButtonStyle(
                          backgroundColor:
                          MaterialStateProperty.all<Color>(const Color(0xfff1f2f8)),
                          shape:
                          MaterialStateProperty.all<RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18.0),
                                  side: const BorderSide(
                                      color: Colors.transparent)))),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text('No',style: TextStyle(color: Colors.black),)),
                  SizedBox(
                    width: 10,
                  ),
                  ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            MyResources.buttonColor),
                        shape: MaterialStateProperty
                            .all<RoundedRectangleBorder>(RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.transparent)))),
                    onPressed: pressFunc,
                    child: Text(
                      'Yes',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
