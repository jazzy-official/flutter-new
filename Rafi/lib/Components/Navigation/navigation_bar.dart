import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:http/http.dart';
import 'package:rafi_admin/Components/AppBar/appbar.dart';
import 'package:rafi_admin/Components/Drawer/ProjectModel/project.dart';
import 'package:rafi_admin/Components/Drawer/drawer.dart';
import 'package:rafi_admin/Components/Screens/Accounts/accounts_tab.dart';
import 'package:rafi_admin/Components/Screens/Inventory/inventory.dart';
import 'package:rafi_admin/Components/Screens/Purchase/purchase.dart';
import 'package:rafi_admin/Components/Screens/Sales/Models/deals_model.dart';
import 'package:rafi_admin/Components/Screens/Sales/Screens/sales.dart';
import 'package:rafi_admin/Components/loader_widget.dart';
import 'package:rafi_admin/Controllers/accounts_controller.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class Navigation extends StatefulWidget {
  final int? Id;
  const Navigation({Key? key, this.Id}) : super(key: key);

  @override
  State<Navigation> createState() => _NavigationState();
}

class _NavigationState extends State<Navigation> {
  final accountController = Get.put(AccountController());
  late String? token;
  // Future setAccounts(int projectId) async {
  //   http.Response response = await http.get(
  //     Uri.parse('https://raccounting.azurewebsites.net/api/Reports/AccountsChecklist?Projectid=$projectId'),
  //     headers: {
  //       'Content-Type': 'application/json',
  //       'Accept': 'application/json',
  //       'Authorization': 'Bearer $token',
  //     },
  //   );
  //   if (response.statusCode == 200) {
  //     // If the server did return a 201 CREATED response,
  //     // then parse the JSON.
  //     SharedPreferences pref = await SharedPreferences.getInstance();
  //     String account = response.body;
  //     pref.setString('account', account);
  //     // Account account = Account.fromJson(jsonDecode(response.body));
  //     // return user;
  //   } else {
  //     // If the server did not return a 201 CREATED response,
  //     // then throw an exception.
  //     throw Exception('Failed to get account check list');
  //   }
  // }
  // Future setUnitsData(int projectId) async {
  //
  //   Response response = await get(
  //       Uri.parse(
  //           'https://raccounting.azurewebsites.net/api/Units/Count?ProjectId=1'),
  //       headers: {
  //         'Content-Type': 'application/json',
  //         'Accept': 'application/json',
  //         'Authorization': 'Bearer $token',
  //       });
  //   // print(response.statusCode);
  //   if (response.statusCode == 200) {
  //     // If the server did return a 201 CREATED response,
  //     // then parse the JSON.
  //     SharedPreferences pref = await SharedPreferences.getInstance();
  //     String unitData = jsonDecode(response.body);
  //     pref.setString('unitsData', unitData);
  //     // Account account = Account.fromJson(jsonDecode(response.body));
  //     // return user;
  //   } else {
  //     // If the server did not return a 201 CREATED response,
  //     // then throw an exception.
  //     throw Exception('Failed to set unit data');
  //   }
  // }
  // Future setRecovery(int projectId) async {
  //   http.Response response = await http.get(
  //     Uri.parse('https://raccounting.azurewebsites.net/api/Reports/DashBoardRecoverySheet?Projectid=$projectId'),headers: {
  //     'Content-Type': 'application/json',
  //     'Accept': 'application/json',
  //     'Authorization': 'Bearer $token',
  //   },
  //   );
  //   if (response.statusCode == 200) {
  //     // If the server did return a 201 CREATED response,
  //     // then parse the JSON.
  //     SharedPreferences pref = await SharedPreferences.getInstance();
  //     String recovery = response.body;
  //     pref.setString('recovery', recovery);
  //     // Account account = Account.fromJson(jsonDecode(response.body));
  //     // return user;
  //   } else {
  //     // If the server did not return a 201 CREATED response,
  //     // then throw an exception.
  //     throw Exception('Failed to get recovery data');
  //   }
  // }
  // Future setPaidReceivedAmmount(int projectId) async {
  //   http.Response response = await http.get(
  //     Uri.parse('https://raccounting.azurewebsites.net/api/Reports/ReceivedPaidChecklist?ProjectId=$projectId'),
  //       headers: {
  //         'Content-Type': 'application/json',
  //         'Accept': 'application/json',
  //         'Authorization': 'Bearer $token',
  //       },
  //   );
  //   if (response.statusCode == 200) {
  //     // If the server did return a 201 CREATED response,
  //     // then parse the JSON.
  //     SharedPreferences pref = await SharedPreferences.getInstance();
  //     // print(jsonDecode(response.body));
  //     String data = response.body;
  //     pref.setString('paidreceived', data);
  //     // Account account = Account.fromJson(jsonDecode(response.body));
  //     // return user;
  //   } else {
  //     // If the server did not return a 201 CREATED response,
  //     // then throw an exception.
  //     throw Exception('Failed to login.');
  //   }
  // }
  // Future refreshAll() async {
  //   setState(() {
  //     isLoading = true;
  //   });
  //   token = await getToken();
  //   projectss = await refreshProjectList();
  //   number = widget.Id ?? projectss[0].ProjectId;
  //   if(widget.Id == null){
  //     SharedPreferences prefs = await SharedPreferences.getInstance();
  //     await prefs.setInt('projectId', number);
  //   }
  //
  //   await setUnitsData(number);
  //   setState(() {
  //     isLoading = false;
  //   });
  // }

  bool isLoading = false;
  late List<Project> projectss;
  late int number;
  int _selectedIndex = 0;
  @override
  void initState() {
    // refreshAll();
    super.initState();
  }
  static  final List<Widget> _widgetOptions = <Widget>[
    const AccountsPage(),
    InventoryPage(),
    SalesPage(),
    // PurchasePage(),
    PurchasePage(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
  @override
  Widget build(BuildContext context) {
    return isLoading
        ? const Scaffold(
            backgroundColor: Color(0xff205370),
            body: Center(child: LoadingWidget()))
        : Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.chart_bar_alt_fill),
            label: 'Accounts',
            backgroundColor: Color(0xff004251),
          ),
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.cube_box_fill),
            label: 'Inventory',
            backgroundColor: Color(0xff004251),
          ),
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.money_dollar),
            label: 'Sales',
            backgroundColor: Color(0xff004251),
          ),
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.cart_badge_plus),
            label: 'Purchase',
            backgroundColor: Color(0xff004251),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.white70,
        onTap: _onItemTapped,
      ),
            body: _widgetOptions[_selectedIndex],
        );
  }
}
Future<String?> getToken() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString('token');
}
Future<List<Project>> refreshProjectList() async {

  String? token = await getToken();
  Response response = await get(
      Uri.parse(
          'https://raccounting.azurewebsites.net/api/Users/AssignedPorjects'),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      });
  print(response.statusCode);
  if (response.statusCode == 200) {
    Map<String, dynamic> responseBody =
    await jsonDecode(jsonDecode(response.body));
    print(responseBody);
    List<dynamic> data = responseBody["ProjectsList"];
    List<Project> projects = data
        .map(
          (dynamic item) => Project.fromJson(item),
    )
        .toList();
    return projects;

  } else {
    throw "Unable to retrieve posts.";
  }
}
