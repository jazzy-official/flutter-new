import 'package:ap_academy_new/ModelView/controllers/auth_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ap_academy_new/Constants/util.dart';
import 'login_screen.dart';


class Forgetpassword extends StatelessWidget {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  late String emailfp;
  TextEditingController emailControllerfp = TextEditingController();
  final _authController = Get.put(AuthController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: myResources.backgroundColor,
      appBar: AppBar(
        backgroundColor: myResources.buttonColor,
        title: Container(
          child: Text(
            'VERIFICATION',
            style: GoogleFonts.roboto(),
          ),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Form(
          autovalidateMode: AutovalidateMode.always, key: _formKey,
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 20, bottom: 0),
                child: Container(
                  child: Image.asset(
                    "assets/logo.png",
                    width: 300,
                    height: 200,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15.0),
                child: Center(
                  child: Text(
                    "Forgot Password?",
                    style: myResources.appHeadingStyle,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0, left: 10, right: 10),
                child: Text(
                  "Enter the email address associated with your account.",
                  style: myResources.appTextStyle,
                ),
              ),
              Padding(
                //Add padding around textfield
                padding: const EdgeInsets.only(top: 25.0, left: 10, right: 10),
                child: TextFormField(
                  controller: emailControllerfp,
                  //Email Controller
                  keyboardType: TextInputType.emailAddress,
                  validator: (val) {
                    if (val!.isEmpty) {
                      return "Please enter email";
                    } else if (!val.contains("@")) {
                      return "Please enter valid email";
                    } else {
                      return null;
                    }
                  },
                  onSaved: (val) => emailfp = val!,
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    hintText: "Email",
                    hintStyle: myResources.hintfontStyle,
                    prefixIcon: Icon(Icons.email),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 50.0, left: 10, right: 10),
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.9,
                  height: 40,
                  child: RaisedButton(
                    textColor: Colors.white,
                    color: myResources.buttonColor,
                    child: Text(
                      "Send Verification Code",
                    ),
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        await _authController
                            .sendpasswordresetemail1(emailControllerfp.text);

                        // Navigator.push(
                        //     context,
                        //     MaterialPageRoute(
                        //         builder: (context) => VerificationCode()));
                      }
                    },
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
