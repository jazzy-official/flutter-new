import 'package:flutter/material.dart';
import 'package:rafi_admin/Components/AppBar/appbar.dart';
import 'package:rafi_admin/Components/Drawer/drawer.dart';
import 'package:rafi_admin/Components/Screens/Sales/Models/deal_unit_model.dart';
import 'package:rafi_admin/Components/Screens/Sales/widgets/unit_deal_card.dart';

class DealUnitList extends StatefulWidget {
  final List<DealUnit> dealunits;
  DealUnitList({Key? key, required this.dealunits}) : super(key: key);

  @override
  State<DealUnitList> createState() => _DealUnitListState();
}

class _DealUnitListState extends State<DealUnitList> {

  late List<DealUnit> _foundResult = [];

  @override
  initState() {
    // at the beginning, all users are shown
    _foundResult = widget.dealunits;

    super.initState();
  }

  void _runFilter(String enteredKeyword) {
    List<DealUnit> results = [];
    if (enteredKeyword.isEmpty) {
      // if the search field is empty or only contains white-space, we'll display all users
      results = widget.dealunits;
    } else {
      results = widget.dealunits
          .where((user) => (user.unitNo+"-"+user.phase)
          .toUpperCase()
          .contains(enteredKeyword.toUpperCase()))
          .toList();
      // we use the toLowerCase() method to make it case-insensitive
    }

    // Refresh the UI
    setState(() {
      _foundResult = results;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Units", style: const TextStyle(
            fontSize: 20.0,
            fontWeight: FontWeight.w500,
            color: Colors.white),),
        backgroundColor: Color(0xff004251),
      ),
      drawer: Drawer(child: const SideDrawer()),
      body: Column(
        children: [
      const SizedBox(
      height: 10,
      ),
      Container(
        width: MediaQuery.of(context).size.width - 15,
        // (MediaQuery.of(context).size.width * 1) - 90,
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            boxShadow: const [
              BoxShadow(
                  color: Colors.black26,
                  blurRadius: 5,
                  offset: Offset(0, 0.5))
            ]),
        height: 40,
        child: TextFormField(
          onChanged: (val) {
            _runFilter(val);
          },
          style: const TextStyle(
            color: Colors.black87,
          ),
          decoration: const InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(
                top: 7,
              ),
              prefixIcon: Icon(
                Icons.search,
                color: Color(0xff7F7C82),
              ),
              hintText: 'Search',
              hintStyle: TextStyle(
                color: Colors.black38,
              )),
        ),
      ),
      const SizedBox(
        height: 10,
      ),
          Expanded(
            child: ListView.builder(
              physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
              itemCount: _foundResult.length,
              itemBuilder: (context, i) {
                final deal = _foundResult[i];
                return DealUnitCard(dealUnit: deal);
              },
            ),
          ),
        ],
      ),
    );
  }
}
