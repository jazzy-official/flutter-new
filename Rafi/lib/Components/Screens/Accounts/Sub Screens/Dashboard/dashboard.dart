import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:intl/intl.dart';
import 'package:rafi_admin/Components/loader_widget.dart';
import 'package:rafi_admin/Controllers/accounts_controller.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'Model/RecievePaidChecklist/receive_paid_checklist.dart';
import 'Model/Recovery/recovery.dart';
import 'Model/Units Model/units.dart';
import 'Model/account model/accounts.dart';
class Dashboard extends StatefulWidget {

 Dashboard({Key? key,}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}


class _DashboardState extends State<Dashboard> {
  final accountController = Get.put(AccountController());
  var formatter = NumberFormat('#,##,000');
  bool isLoading = true;

  final TrackballBehavior _trackballBehavior = TrackballBehavior(
  enable: true,
  // Display mode of trackball tooltip
  tooltipDisplayMode: TrackballDisplayMode.nearestPoint
  );

  final TrackballBehavior _trackballBehavior2 = TrackballBehavior(
      enable: true,
      // Display mode of trackball tooltip
      tooltipDisplayMode: TrackballDisplayMode.nearestPoint
  );
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {


    return RefreshIndicator(
      onRefresh: ()  async {
         return await accountController.refreshAll();
      },
      child: GetBuilder<AccountController>(
        init: AccountController(),
        builder: (cont) => ListView(
            physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
          children: [
            //Accounts Card
            buildAccounts(context),
            buildReceivedCheckList(context),
            // buildUnits(context, widget.unit),
            buildRecovery(context),
          ],
        ),
      ),
    );
  }
  Widget buildReceivedCheckList(BuildContext context,) {
    return Container(
      height: 500,
      margin: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: const Color(0xfff1f6f8),
        borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
            bottomLeft: Radius.circular(10),
            bottomRight: Radius.circular(10)
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Center(
          child: Column(
            children: [
              SizedBox(height: 20,),
              Text("Received Checklist",style: TextStyle(fontWeight:FontWeight.bold,fontSize: 22),),
              SizedBox(height: 20,),
              Expanded(
                child: accountController.received !=null ? SfCartesianChart(
                    trackballBehavior: _trackballBehavior2,
                  // zoomPanBehavior: _zoomPanBehavior,
                    // legend: Legend(isVisible: true,
                    //   textStyle: const TextStyle(
                    //     // color: Colors.white,
                    //     // fontSize: 15,
                    //     // fontStyle: FontStyle.italic,
                    //     // fontWeight: FontWeight.w900
                    //   ),
                    //   position: LegendPosition.bottom,
                    //   itemPadding: 25,
                    // ),
                    primaryXAxis: DateTimeAxis(),
                    series: <ChartSeries>[
                      // Renders line chart
                      LineSeries<Received, DateTime>(
                          dataSource: accountController.received!,
                          xValueMapper: (Received sales, _) =>DateTime.parse(sales.date) ,
                          yValueMapper: (Received sales, _) => sales.ammount
                      )
                    ]
                ):LoadingWidget(),
              ),
            ],
          )
      ),
    );

  }
  Widget buildAccounts(BuildContext context,) {
    final List<ChartData> chartData = [
      ChartData('Banks', accountController.acccountslist?.banks??0, Color.fromRGBO(147,0,119,1)),
      ChartData('Payable', accountController.acccountslist?.payables??0, Color.fromRGBO(9,0,136,1)),
      ChartData('Cash in hand', accountController.acccountslist?.cashInHand??0, Color.fromRGBO(228,0,124,1)),
      ChartData('Receivable',  accountController.acccountslist?.receivable??0, Color.fromRGBO(255,189,57,1))
    ];
    return Container(
      height: 500,
      margin: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: const Color(0xfff1f6f8),
        borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
            bottomLeft: Radius.circular(10),
            bottomRight: Radius.circular(10)
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: const Offset(0, 3), // changes position of shadow
          ),
        ],
      ),

      child: Center(
          child: Column(
            children: [
              const SizedBox(height: 20,),
              Text("Accounts",style: const TextStyle(fontWeight:FontWeight.bold,fontSize: 22),),
              Expanded(
                child: accountController.acccountslist != null ? SfCircularChart(
                    legend: Legend(isVisible: true,
                      textStyle: const TextStyle(
                        // color: Colors.white,
                        // fontSize: 15,
                        // fontStyle: FontStyle.italic,
                        // fontWeight: FontWeight.w900
                      ),
                      position: LegendPosition.bottom,
                      overflowMode: LegendItemOverflowMode.wrap,

                      itemPadding: 10,
                    ),

                    series: <CircularSeries>[
                      // Renders doughnut chart
                      DoughnutSeries<ChartData, String>(
                        dataSource: chartData,
                        pointColorMapper:(ChartData data,  _) => data.color,
                        xValueMapper: (ChartData data, _) => data.x,
                        yValueMapper: (ChartData data, _) => data.y,
                        // explode: true,
                        // explodeIndex: 0,
                      )
                    ]
                ):LoadingWidget(),
              ),
              SizedBox(height: 15,),
              Column(children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      height:50,
                      width: MediaQuery.of(context).size.width * 0.40,
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(147,0,119,1),
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Center(child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          const Text('Banks',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 16),),
                          Text( formatter.format(accountController.acccountslist?.banks??0),style: TextStyle(color: Colors.white),),
                        ],
                      )),
                    ),
                    Container(
                      height:50,
                      width: MediaQuery.of(context).size.width * 0.40,
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(9,0,136,1),
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Center(child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text('Payables',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 16),),
                          Text(formatter.format(accountController.acccountslist?.payables??0),style: TextStyle(color: Colors.white),),
                        ],
                      )),
                    ),
                  ],
                ),
                SizedBox(height: 10,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      height:50,
                      width: MediaQuery.of(context).size.width * 0.40,
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(255,189,57,1),
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Center(child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text('Receivable',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),
                          Text(formatter.format(accountController.acccountslist?.receivable??0)),
                        ],
                      )),
                    ),
                    Container(
                      height:50,
                      width: MediaQuery.of(context).size.width * 0.4,
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(228,0,124,1),
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Center(child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text('Cash In Hand',style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 16),),
                          Text(formatter.format(accountController.acccountslist?.cashInHand??0),style: TextStyle(color: Colors.white),),
                        ],
                      )),
                    ),
                  ],
                ),
              ],),
              SizedBox(height: 20,)
            ],
          )
      ),
    );
  }
  Widget buildRecovery(BuildContext context,) {
    final List<SalesData> data = [
      if(accountController.recovery != null)
      for(var i=0 ; i < accountController.recovery!.length; i++)
        SalesData(((DateTime.now().month+i-1)%12+1).toString(), accountController.recovery![i].amount),


    ];
    return Container(
      height: 500,
      margin: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: const Color(0xfff1f6f8),
        borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(10),
            topRight: Radius.circular(10),
            bottomLeft: Radius.circular(10),
            bottomRight: Radius.circular(10)
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Column(
          children: [
            SizedBox(height: 20,),
            const Text("Recovery",style: TextStyle(fontWeight:FontWeight.bold,fontSize: 22)),
            Expanded(
              child: accountController.recovery!=null? SfCartesianChart(
                  trackballBehavior: _trackballBehavior,
                  primaryXAxis: CategoryAxis(),
                  series: <ChartSeries<SalesData, String>>[
                    // Renders column chart
                    ColumnSeries<SalesData, String>(
                        dataSource: data,
                        xValueMapper: (SalesData sales, _) => sales.year,
                        yValueMapper: (SalesData sales, _) => sales.amount
                    )
                  ]
              ):LoadingWidget(),
            ),
          ],
        ),
    );


  }
}



class ChartData {
  ChartData(this.x, this.y, this.color);
  final String x;
  final double y;
  final Color color;
}
class SalesData {

  SalesData(this.year, this.amount,);

  final String year;
  final double amount;
}
