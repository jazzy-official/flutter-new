class NewsModel {
  final int? id;
  final String title;
  final String description;
  final String type;
  const NewsModel({
    this.id,
    required this.title,
    required this.type,
    required this.description,
  });
  Map toJson() => {
        'title': title,
        'type': type,
        'description': description,
      };
}
