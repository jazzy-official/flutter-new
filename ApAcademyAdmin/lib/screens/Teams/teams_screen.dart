import 'package:flutter/cupertino.dart';
import 'package:ap_academy_admin/ModelView/Controller/teamsController.dart';
import 'package:ap_academy_admin/screens/Teams/NewTeamsDetails.dart';
import 'package:ap_academy_admin/screens/shared/DeletPop.dart';
import 'package:ap_academy_admin/screens/Teams/addNewteam.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:get/get.dart';
import 'package:get/route_manager.dart';

class TeamsScreen extends StatefulWidget {
  @override
  _TeamsScreenState createState() => _TeamsScreenState();
}

class _TeamsScreenState extends State<TeamsScreen> {
  final teamController = Get.put(TeamController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyResources.backgroundColor,
      body: Material(
        //Create an event add button
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "",
                    style: MyResources.appHeadingStyle,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(0),
                    child: Container(
                      width: Get.width * 0.2,
                      height: 25,
                      child: RaisedButton(
                        textColor: MyResources.buttontextColor,
                        color: MyResources.buttonColor,
                        child: Text(
                          "Add",
                        ),
                        onPressed: () {
                          Get.to(AddNewTeam());
                        },
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            //Card
            GetX<TeamController>(
              init: Get.put<TeamController>(TeamController()),
              builder: (TeamController teamController) {
                if (teamController != null && teamController.teams != null) {
                  print(teamController.teams.toString());
                  return Expanded(
                    flex: 1,
                    child: ListView.builder(
                      itemCount: teamController.teams.length,
                      itemBuilder: (BuildContext context, int index) =>
                          getTeamCard(teamController.teams[index]),
                    ),
                  );
                } else {
                  return Container(
                    child: LoadingWidget(),
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget getTeamCard(Map<String, dynamic> team) {
    return Padding(
      padding: const EdgeInsets.only( left: 4, right:4),
      child: Center(
        child: InkWell(
          onTap: () {
            Get.to(NewTeamsDetails(
              teamModel: team,
            ));
          },
          child: Container(
            height: 70,
            //create the cards for all details of event happening
            child: Card(
              color: MyResources.backgroundColor,
              child: Padding(
                padding: const EdgeInsets.all(4),
                child: Row(
                  //crossAxisAlignment: CrossAxisAlignment.start,
                  //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text("TEAM : ", style: MyResources.titleTextStyle),
                          // SizedBox(
                          //   width: 20,
                          // ),
                          // Text("Type:", style: MyResources.titleTextStyle),
                          // SizedBox(
                          //   height: 20,
                          // ),
                          // Text(" Coach:", style: MyResources.titleTextStyle),
                        ],
                      ),
                    ),
                    Container(
                      width:Get.width*0.5,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(team['teamName'].toString().toUpperCase(),
                              style: MyResources.nameTextStyle),
                          // SizedBox(
                          //   height: 20,
                          // ),
                          // // Text(
                          // //   team['teamType'],
                          // // ),
                          // SizedBox(
                          //   height: 20,
                          // ),
                          // Text(
                          //   team['coach'],
                          // ),
                        ],
                      ),
                    ),
                    Container(
                      alignment:Alignment.centerRight,
                      //padding: const EdgeInsets.only(left:80),
                      child:
                      // Column(
                      //   mainAxisAlignment: MainAxisAlignment.end,
                      //   children: [
                      MaterialButton(
                        // onPressed: () {
                        //   Get.dialog(
                        //     TeamsDetails(
                        //       team1Model: team,
                        //     ),
                        //   );},
                        onPressed: () {  },
                        child: Icon(
                          CupertinoIcons.person_3_fill,
                          color: Colors.blueAccent,
                          size: 20,
                        ),
                      ),

                    ),
                    Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(

                           team['teamMembers'] != null
                               ? team['teamMembers'].length.toString()
                               : '0',
                         ),
                          // SizedBox(
                          //   height: 20,
                          // ),
                          // // Text(
                          // //   team['teamType'],
                          // // ),
                          // SizedBox(
                          //   height: 20,
                          // ),
                          // Text(
                          //   team['coach'],
                          // ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
