import 'dart:async';

import 'package:ap_academy_new/ModelView/controllers/profileController.dart';
import 'package:ap_academy_new/Models/UserModel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:mongo_dart/mongo_dart.dart';

import '../../Constants/db_cred.dart';

class DatabaseController extends GetxController {
  var mongoUserId;
  @override
  Future<void> onInit() async {
    // await getMongoPort();
    // await db.open();
    // String id = Get.find<AuthController>().firebaseUser.uid;
    //id)); //Stream Comming from
    super.onInit();
  }

  Future<bool> checkUserExist(String uid) async {
    try {
      DbCollection eventCollection = db.collection("Users");
      var n = await eventCollection.findOne(where.eq(
        "firebaseId",
        uid,
      ));
      if(n==null){
        n = await eventCollection.findOne(where.eq(
          "ParentId",
          uid,
        ));
      }

      if (n != null) {
        if (n['role'] == 'user') {
          return true;
        } else
          return false;
      } else
        return false;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }

  Future<bool> checkRollExist(int r) async {
    try {
      DbCollection eventCollection = db.collection("Users");
      var n = await eventCollection.findOne(where.eq(
        "rollno",
        r,
      ));

      if (n != null) {
        if (n['role'] == 'user') {
          return true;
        } else
          return false;
      } else
        return false;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }

  Future<bool?> getMongoPort(bool isFirebase) async {
    try {
      if (!db.isConnected && isFirebase) {
        await FirebaseFirestore.instance
            .collection("Cred")
            .doc("MongoDbPort")
            .get()
            .then((value) => {
                  dbLink = value.data()!['dbLink'],
                });
      } else if (!db.isConnected && !isFirebase) {}
    } catch (e) {
      print(e.toString());
      Get.back();
      Get.snackbar("MongoDb Error", e.toString());
    }
    bool? isopened;
    try {
      await db.open().then((value) {
        isopened = true;
        return isopened;
      });
      //  }
    } catch (error) {
      isopened = false;
    }
    print("[AP Sports] Database is connected : " + db.isConnected.toString());
    return isopened;
  }

  Future<Map<String, dynamic>?> getTeamName(
      Map<String, dynamic> matchModel1) async {
    Map<String, dynamic>? nameModel;
    final dbHelper = Get.put(DatabaseController());
    Map<String, dynamic>? t2 = await dbHelper.getTeamData(matchModel1['team2']);
    await dbHelper.getTeamData(matchModel1['team1']).then((t1) {
      nameModel = {'team1': t1, 'team2': t2};
    });
    return nameModel;
  }


///////////////////////////////////////////////////////----------------Fetch User Details----------///////////
  Future<List<Map<String, dynamic>>> getUserDetail(String? uid) async {
    try {
      DbCollection eventCollection = await db.collection("Users");
      late List<Map<String, dynamic>> n;
       n = await eventCollection.find(
          where.eq("firebaseId", uid ?? FirebaseAuth.instance.currentUser!.uid)).toList();
      print(n);
      if (n.isEmpty || n == []) {
        print("nnnnnnnnnnnnnnnnn");
        DbCollection eventCollection = await db.collection("Users");
        n = await eventCollection.find(
            where.eq("ParentId", uid ?? FirebaseAuth.instance.currentUser!.uid)).toList();
      }
      print(n);
      return n;

    } catch (ex) {
      print("get user error");
      printError(info: ex.toString());
      return [];
    }
  }



  Future<bool> getisActive(
      String firebaseId,
      ) async {
    // await db.open(secure:true);
    try {
      DbCollection usersCollection = db.collection("Users");
      var _user =
      await usersCollection.findOne(where.eq('firebaseId', firebaseId))??await usersCollection.findOne(where.eq('ParentId', firebaseId));


      return _user!["isActive"];

    } catch (ex) {
      print('You are inactive by admin');
      printError(info: ex.toString());
      return false;
    }
    update();
    return true;
  }
//////////////////////////////////
  Future<void> updateusertoken(String token,String fid) async {
    try {
      await db.collection('Users').update(
          where.eq('firebaseId', FirebaseAuth.instance.currentUser!.uid),
          modify.set('token', token));

     List<Map<String, dynamic>> teams= await db.collection('Teams').find(
          {"teamMembers.firebaseId": fid}).toList();
     for(var i=0;i<teams.length;i++){
       for(var j=0;j<teams[i]['teamMembers'].length;j++)
         {
           if(teams[i]['teamMembers'][j]['firebaseId']==FirebaseAuth.instance.currentUser!.uid) {
             teams[i]['teamMembers'][j]['token']=token;
           }
         }
       db.collection('Teams').updateOne(
           where.eq('_id', teams[i]['_id']),
           modify.set('teamMembers', teams[i]['teamMembers']));

     }


      List<Map<String, dynamic>> trainings= await db.collection('Training').find(
          {"totalPlayer.firebaseId": fid}).toList();
      for(var i=0;i<trainings.length;i++){
        for(var j=0;j<trainings[i]['totalPlayer'].length;j++)
        {
          if(trainings[i]['totalPlayer'][j]['firebaseId']==FirebaseAuth.instance.currentUser!.uid) {
            trainings[i]['totalPlayer'][j]['token']=token;
          }
        }
        db.collection('Training').updateOne(
            where.eq('_id', trainings[i]['_id']),
            modify.set('totalPlayer', trainings[i]['totalPlayer']));

      }

      List<Map<String, dynamic>> groups= await db.collection('Groups').find(
          {"teamMembers.firebaseId": fid}).toList();
      for(var i=0;i<groups.length;i++){
        for(var j=0;j<groups[i]['teamMembers'].length;j++)
        {
          if(groups[i]['teamMembers'][j]['firebaseId']==FirebaseAuth.instance.currentUser!.uid) {
            groups[i]['teamMembers'][j]['token']=token;
          }
        }
        db.collection('Groups').updateOne(
            where.eq('_id', groups[i]['_id']),
            modify.set('teamMembers', groups[i]['teamMembers']));

      }


    } catch (ex) {
      print("token  not update");
    }
  }
  Future<void> updateparenttoken(String token,String fid) async {
    try {
      await db.collection('Users').update(
          where.eq('firebaseId', fid),
          modify.set('parentToken', token));

     List<Map<String, dynamic>> teams= await db.collection('Teams').find(
          {"teamMembers.firebaseId": fid}).toList();
     for(var i=0;i<teams.length;i++){
       for(var j=0;j<teams[i]['teamMembers'].length;j++)
         {
           if(teams[i]['teamMembers'][j]['firebaseId']==FirebaseAuth.instance.currentUser!.uid) {
             teams[i]['teamMembers'][j]['parentToken']=token;
           }
         }
       db.collection('Teams').updateOne(
           where.eq('_id', teams[i]['_id']),
           modify.set('teamMembers', teams[i]['teamMembers']));

     }


      List<Map<String, dynamic>> trainings= await db.collection('Training').find(
          {"totalPlayer.firebaseId": fid}).toList();
      for(var i=0;i<trainings.length;i++){
        for(var j=0;j<trainings[i]['totalPlayer'].length;j++)
        {
          if(trainings[i]['totalPlayer'][j]['firebaseId']==FirebaseAuth.instance.currentUser!.uid) {
            trainings[i]['totalPlayer'][j]['parentToken']=token;
          }
        }
        db.collection('Training').updateOne(
            where.eq('_id', trainings[i]['_id']),
            modify.set('totalPlayer', trainings[i]['totalPlayer']));

      }

      List<Map<String, dynamic>> groups= await db.collection('Groups').find(
          {"teamMembers.firebaseId": fid}).toList();
      for(var i=0;i<groups.length;i++){
        for(var j=0;j<groups[i]['teamMembers'].length;j++)
        {
          if(groups[i]['teamMembers'][j]['firebaseId']==FirebaseAuth.instance.currentUser!.uid) {
            groups[i]['teamMembers'][j]['parentToken']=token;
          }
        }
        db.collection('Groups').updateOne(
            where.eq('_id', groups[i]['_id']),
            modify.set('teamMembers', groups[i]['teamMembers']));

      }


    } catch (ex) {
      print("token  not update");
    }
  }
////////////////////////////////////////////////////////------------Create Event.---------------//////////////////////////////

  Future<Map<String, dynamic>?> getTeamData(ObjectId teamId) async {
    try {
      DbCollection eventCollection = db.collection("Teams");
      var n = await eventCollection.findOne(where.id(teamId));
      return n;
    } catch (ex) {
      printError(info: ex.toString());
      return null;
    }
  }

///////////////////////////////////////////////
  ///
  Future<List<Map<String, dynamic>>?> getUsersbygroup(String groupName) async {
    try {
      List<Map<String, dynamic>> n;
      if (groupName != null) {
        DbCollection eventCollection = db.collection("Users");
        int tem = int.parse(groupName.substring(groupName.length - 2));
        n = await eventCollection.find(where.lte('age', tem)).toList();
        return n;
      } else
        return null;
    } catch (ex) {
      printError(info: ex.toString());
      return null;
    }
  }
  Future<List<dynamic>?> getUsersbyteam(String teamName) async {
    try {
      Map<String, dynamic>? n;
      if (teamName != null) {
        DbCollection eventCollection = db.collection("Teams");
        n = (await eventCollection.findOne(where.eq('teamName', teamName)));
        if(n==null){
          return null;
        }else {
          return n['teamMembers'];
        }
      } else
        return null;
    } catch (ex) {
      printError(info: ex.toString());
      return null;
    }
  }
///////////////////////////////////////////////////////---------------check if created DB Collection----------////////////////////////////////////

  Future<bool> saveMatchAttendance(ObjectId mId) async {
    final profileController = Get.put(ProfileController());
    try {
      await db
          .collection('Users')
          .find(
            where.eq('firebaseId', await profileController.temp['firebaseId']),
          )
          .first
          .then((uData) async {
        await db
            .collection('Match')
            .update(where.eq('_id', mId), modify.addToSet("attendance", uData));
      });
      return true;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }

//////////
  Future<bool> delMatchAttendance(ObjectId mId) async {
    final profileController = Get.put(ProfileController());
    bool isdone = false;
    try {
      await db
          .collection('Users')
          .find(
            where.eq('firebaseId', await profileController.temp['firebaseId']),
          )
          .first
          .then((uData) async {
        await db
            .collection('Match')
            .update(where.eq('_id', mId), modify.pull("attendance", uData))
            .then((value) {
          isdone = true;
        });
      });
      return isdone;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }

//////////
  Future<void> addProfileImgPathToDatabase(String imgpath) async {

    try {
      DbCollection eventCollection = db.collection("Users");
      await eventCollection.update(
          where.eq("firebaseId", FirebaseAuth.instance.currentUser!.uid),
          modify.set("photoUrl", imgpath));
      print('Updated to Users');
    } catch (e) {
      print(e);
    }
  }

//////////
  Future<void> addProfiledocsPathToDatabase(List<String> imgpath) async {
    final profileController = Get.put(ProfileController());
    try {
      DbCollection eventCollection = db.collection("Users");
      await eventCollection.update(
          where.eq("firebaseId", profileController.temp['firebaseId']),
          modify.set("documents", imgpath));
      print('  to Users');
    } catch (e) {
      print(e);
    }
  }

  ////////////////////////////////////////////////////////------------Add User in Database---------------//////////////////////////////
  Future<bool> addUser(UserModel userModel) async {
    // await openDb();
    try {
      DbCollection usersCollection = db.collection("Users");
      await usersCollection.insert(
        {
          'firebaseId': userModel.id,
          'name': userModel.displayName,
          'email': userModel.email,
          'role': userModel.role,
          'photoUrl': userModel.photo,
          'phone': userModel.phone,
          'rollNo': userModel.rollno,
          'token': userModel.token,
          'approved': userModel.approved,
          'passport':userModel.passport,
          'chricHqId':userModel.crichqid,
          'isActive': userModel.isActive,
          'dob': userModel.dob,
          'parentname': userModel.parentName,
          'parentToken': "",
          'ParentId': "",
        },
      );
      print('[APSPORT] Created User.');
      update();
      return true;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  } ////////////////////////////////////////////////////////------------Add User in Database---------------//////////////////////////////

  Future<bool> checkdublicate(String firebaseId) async {
    try {
      final usr = await db
          .collection('Users')
          .findOne(where.eq('firebaseId', firebaseId));
      if (usr != null)
        return true;
      else
        return false;
    } catch (ex) {
      print(ex.toString());
      return false;
    }
  }

  Future<String?> retrieveStudentData(String roll) async {
    try {
      final usr =
          await db.collection('Users').findOne(where.eq('rollNo', roll));
      if (usr != null)
        return usr['firebaseId'];
      else
        return null;
    } catch (ex) {
      print(ex.toString());
      return null;
    }
  }

  Future<String> retrieveStudentwithPId(String pId) async {
    try {
      final usr =
          await db.collection('Users').findOne(where.eq('ParentId', pId));
      if (usr != null)
        return usr['firebaseId'];
      else
        return "null";
    } catch (ex) {
      print(ex.toString());
      return "null";
    }
  }

//////////////////////updat roll no
  Future<bool> updateParentIdUser(String pId, String sId) async {
    try {
      DbCollection usersCollection = db.collection("Users");

      await usersCollection.update(
          where.eq('firebaseId', sId), modify.set('ParentId', pId));

      print('updated to Users');
      update();
      return true;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }

///////////////////
  Future<bool> updateUser(Map<String, dynamic> userModel) async {
    final profileController = Get.put(ProfileController());
    try {
      DbCollection usersCollection = db.collection("Users");

      await usersCollection.update(
          where.eq('firebaseId', profileController.temp['firebaseId']),
          modify.set('phone', userModel['phone']));
      await usersCollection.update(
          where.eq('firebaseId', profileController.temp['firebaseId']),
          modify.set('dob', userModel['dob']));
      await usersCollection.update(
          where.eq('firebaseId', profileController.temp['firebaseId']),
          modify.set('name', userModel['name']));
      await usersCollection.update(
          where.eq('firebaseId', profileController.temp['firebaseId']),
          modify.set('surname', userModel['surname']));

      print('updated to Users');
      update();
      return true;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }

  ////////////////////////////////////////////////////////------------Get User Information---------------//////////////////////////////
  Future<bool> retriveUser(String firebaseId) async {
    // await openDb();
    try {
      DbCollection usersCollection = db.collection("Users");
      var _user =
          await usersCollection.findOne(where.eq('firebaseId', firebaseId));
      mongoUserId = _user!["_id"];
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
    update();
    return true;
  }

///////////////////////
  ///
  Future<bool> checkVerified(String fbId) async {
    bool a = false;
    try {
      a = await db
          .collection("Users")
          .findOne(where.eq('firebaseId', fbId))
          .then((value) {
        return value!['approved'];
      });
    } catch (e) {
      print(e.toString());
    }
    return a;
  }

////////////////////////////////////////////////////////------------Save Attendance.---------------//////////////////////////////
  Future<bool> saveAttendance(ObjectId trId) async {
    final profileController = Get.put(ProfileController());
    try {
      DbCollection usersCollection = db.collection("Training");
      await usersCollection.update(where.eq('_id', trId),
          modify.addToSet("attendance", profileController.temp['firebaseId']));

      update();
      return true;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }

  Future<bool> deletAttendance(ObjectId trId) async {
    final profileController = Get.put(ProfileController());
    try {
      DbCollection usersCollection = db.collection("Training");
      await usersCollection.update(where.eq('_id', trId),
          modify.pull("attendance", profileController.temp['firebaseId']));

      update();
      return true;
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }
  }

  Future<bool> getParentCred(
    String firebaseId,
    String name,
    String email,
  ) async {
    // await openDb();
    try {
      DbCollection usersCollection = db.collection("UserParent");
      await usersCollection
          .insert({'firebaseId': firebaseId, 'name': name, 'email': email});
      print('Connected to UserParent');
    } catch (ex) {
      printError(info: ex.toString());
      return false;
    }

    update();
    return true;
  }

  ////////////////////////////////////////////////////////------------Update User Detail.---------------//////////////////////////////

////////////////////////////////////////---------------------Stream Controllers--------------///////////////////////
  ////////////////////////////////////////Notification Stream////////////////////
  Stream<List<Map<String, dynamic>>> notificationStream() {
    return db.collection("Notifications").find(where.sortBy('timeCreated',descending: true)).toList().asStream();
  }

  ////////////////////////////////////////News Stream////////////////////
  Stream<List<Map<String, dynamic>>> newsStream() {
    return db.collection("News").find(where.sortBy('timeCreated',descending: true)).toList().asStream();
  }

  ////////////////////////////////////////News Stream////////////////////
  Stream<List<Map<String, dynamic>>> matchStream() {
    return db.collection("Match").find(where.sortBy('timeCreated',descending: false)).toList().asStream();
  } ////////////////////////////////////////News Stream////////////////////

  Stream<List<Map<String, dynamic>>> otherEventStream() {
    return db.collection("Events").find(where.sortBy('timeCreated',descending: true)).toList().asStream();
  }

  Stream<List<Map<String, dynamic>>> teamStream() {
    return db.collection("Teams").find(where.sortBy('CreatedTime',descending: true)).toList().asStream();
  }

  Stream<List<Map<String, dynamic>>> getAlbumData() {
    return db.collection("Gallery").find().toList().asStream();
  }

  Stream<List<Map<String, dynamic>>> trainingStream() {
    return db.collection("Training").find().toList().asStream();
  }

  Stream<List<Map<String, dynamic>>> sponsorStream() {
    return db.collection("Sponsors").find().toList().asStream();
  }

  Stream<List<Map<String, dynamic>>> banksStream() {
    return db.collection("Banks").find().toList().asStream();
  }
}
