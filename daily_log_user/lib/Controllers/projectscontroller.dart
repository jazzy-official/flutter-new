
import 'dart:convert';
import 'dart:io';

import 'package:daily_log_user/Controllers/profilecontroller.dart';
import 'package:daily_log_user/Models/sub_module.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:path/path.dart' as path;
import '../Models/module.dart';
import '../Models/project_model.dart';
import 'package:http/http.dart' as http;
import '../loading_widget.dart';

class ProjectController extends GetxController {

  var prof = Get.put(ProfileController());
  List<ProjectModel> projects = [];
  List<ProjectModel> allprojects = [];
  final DatabaseReference databaseReference =
  FirebaseDatabase.instance.ref("Projects");
  FirebaseStorage storage = FirebaseStorage.instance;
  @override
  Future<void> onInit() async {
    // TODO: implement onInit
    readBall();
    super.onInit();
  }


  Future<void> addProject(ProjectModel project) async {
     try{
       Get.defaultDialog(title: '', content: const LoadingWidget(),backgroundColor: Colors.transparent,barrierDismissible: false);

       await databaseReference.push().set(project.toJson());
        await readBall();
        Get.back();
        Get.back();
        Get.snackbar("Done", "Project added");
      } catch(e) {
        Get.snackbar("Error", e.toString(),snackPosition: SnackPosition.BOTTOM);
      }

    update();


  }

  Future<void> updateProject(ProjectModel ball) async {
    Get.defaultDialog(title: '', content: const LoadingWidget(),backgroundColor: Colors.transparent,barrierDismissible: false);
    databaseReference.update({
      ball.id! : ball.toJson()
    });
    Get.back();
    Get.back();
    Get.snackbar("Done", "Project Updated",snackPosition: SnackPosition.BOTTOM);
  }


  Future<void> deleteProject(String id) async {
    databaseReference.child(id).remove();

  }

  Future<void> readBall() async {
    List<ProjectModel> temprojects = [];
    List<ProjectModel> temprojects2 = [];
    await databaseReference.once().then(( data) {

      if(data.snapshot.value != null){
        Map<dynamic,dynamic> values = data.snapshot.value as Map<dynamic,dynamic>;
        values.forEach((key, value) {
          ProjectModel gr = ProjectModel.fromJson(value,key);
          temprojects2.add(gr);
          if(gr.assignto == prof.userDetail.name) {
            temprojects.add(gr);
          }
        });
      }

    });
    projects = temprojects;
    allprojects = temprojects2;
    update();
  }

  Future<ProjectModel> getProjectById(String id) async {
    Map<dynamic,dynamic> values = (await FirebaseDatabase.instance.reference().child("Projects/$id").once()).snapshot.value as Map<dynamic,dynamic>;
    ProjectModel gr = ProjectModel.fromJson(values,id);
    return gr;
  }

  Future<void> updateSubModule(SubModule ball,String projectid,String moduleid) async {
    final DatabaseReference Reference =
    FirebaseDatabase.instance.ref("Projects/$projectid/modules/$moduleid/submodules");
    await Reference.update({
      ball.id! : ball.toJson()
    });
  }


  Future<void> updateModule(Module ball,String projectid,) async {
    final DatabaseReference Reference =
    FirebaseDatabase.instance.ref("Projects/$projectid/modules");
    await Reference.update({
      ball.id! : ball.toJson()
    });

  }


  Future<void> deletedoc(String ref) async {
    await storage.ref().child('doc/$ref').delete();
    // Rebuild the UI
    update;
  }


  Future<void> downloadFile(String tempref) async {
    var snapshot = await storage.ref()
        .child('doc/$tempref');
    var url = await snapshot.getDownloadURL();

    // final String url = await ref.get;
    final http.Response downloadData = await http.get(Uri.parse(url));
    final Directory systemTempDir = Directory.systemTemp;
    final File tempFile = File('${systemTempDir.path}/tmp.jpg');
    if (tempFile.existsSync()) {
      await tempFile.delete();
    }
    await tempFile.create();
    final DownloadTask task = snapshot.writeToFile(tempFile);
    final int byteCount = (await task).totalBytes;
    var bodyBytes = downloadData.bodyBytes;
    final String name = await snapshot.name;
    final String path = await snapshot.fullPath;
    print(
      'Success!\nDownloaded $name \nUrl: $url'
          '\npath: $path \nBytes Count :: $byteCount',
    );
    // _scaffoldKey.currentState.showSnackBar(
    //   SnackBar(
    //     backgroundColor: Colors.white,
    //     content: Image.memory(
    //       bodyBytes,
    //       fit: BoxFit.fill,
    //     ),
    //   ),
    // );
  }

  Future<http.Response> createdoc(String title) {
    return http.post(
      Uri.parse('https://docs.googleapis.com/v1/documents'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'title': title,
      }),
    );
  }

}