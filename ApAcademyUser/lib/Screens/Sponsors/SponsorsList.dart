import 'package:ap_academy_new/ModelView/controllers/SponsorController.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../Constants/util.dart';
import '../../shared/loading_widget.dart';
import 'SponsorDetails.dart';

class SponsorList extends StatefulWidget {
  @override
  _SponsorListState createState() => _SponsorListState();
}

class _SponsorListState extends State<SponsorList> {
  final spC = Get.put(SponsorController());
  @override
  void initState() {
    spC.refreshSponsor();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      //Create an event add button
      child: Container(
        child: GetX<SponsorController>(
          init: Get.put<SponsorController>(SponsorController()),
          builder: (SponsorController sponsorController) {
            if (sponsorController != null &&
                sponsorController.sponsors != null) {
              print(sponsorController.sponsors.toString());
              return GridView.count(
                keyboardDismissBehavior:
                    ScrollViewKeyboardDismissBehavior.onDrag,
                crossAxisCount: 1,
                children: List.generate(
                  sponsorController.sponsors.length,
                  (index) {
                    return getsponsorCard(
                        sponsorController.sponsors[index]);
                  },
                ),
              );
            } else {
              return Container(
                child: LoadingWidget(),
              );
            }
          },
        ),
      ),
    );
  }

  getsponsorCard(Map<String, dynamic> sponsorModel) {
    return InkWell(
      onTap: () {
        Get.dialog(SponsorDetailCard(
          sponsorDetailData: sponsorModel,
        ));
      },
      child: Container(

        margin: EdgeInsets.symmetric(horizontal: 12,vertical: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(10),
            topLeft: Radius.circular(10),
              bottomLeft: Radius.circular(2),
              bottomRight: Radius.circular(2)
          ),
          color: Colors.black12,
        ),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.black,
            borderRadius: BorderRadius.circular(10),
            image: DecorationImage(
              colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.5), BlendMode.dstATop),
              image: NetworkImage(sponsorModel["imageUrl"]),
              fit: BoxFit.contain,
            ),
            boxShadow: const [
              BoxShadow(
                color: Colors.black26,
                spreadRadius: 3,
                blurRadius: 5,
                offset: Offset(0, 5), // changes position of shadow
              ),
            ],
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Text(
                  sponsorModel['name'].toString().capitalize!,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
          // child: ClipRRect(
          //   borderRadius: BorderRadius.only(
          //     topRight: Radius.circular(10),
          //     topLeft: Radius.circular(10),
          //   ),
          //   child: sponsorModel['imageUrl'] != ''
          //       ? Image.network(
          //           sponsorModel['imageUrl'],
          //           // width: Get.width,
          //           // height:100,
          //           fit: BoxFit.fitHeight,
          //         )
          //       : Center(
          //           child: Icon(
          //           Icons.image_rounded,
          //           size: 100,
          //         )),
          // ),
        ),
      ),
    );
  }
}
