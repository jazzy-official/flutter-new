import 'package:ap_booking_user/Screens/My%20booking/add_booking.dart';
import 'package:ap_booking_user/constants/util.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ionicons/ionicons.dart';

import 'Models/ground_models.dart';
import 'Models/slot_model.dart';
import 'Models/template_model.dart';
class GroundDetail extends StatefulWidget {
  final Ground ground;
  const GroundDetail({Key? key, required this.ground}) : super(key: key);

  @override
  State<GroundDetail> createState() => _GroundDetailState();
}

class _GroundDetailState extends State<GroundDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyResources.buttonColor,
        title: Text(
          widget.ground.name.toUpperCase(),
          style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 16),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            CarouselSlider(
              options: CarouselOptions(
                aspectRatio: 2.0,
                enlargeCenterPage: true,
              ),
              items: widget.ground.imgUrls
                  .map((item) => Container(
                        child: Center(
                            child: item== "" ? Image.asset("assets/stadiumlogo.png",width: 1000,) :Image.network(
                          item,
                          fit: BoxFit.fill,
                          width: 1000,
                        )),
                      ))
                  .toList(),
            ),
            Padding(
              padding: const EdgeInsets.all(7.0),
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Address : ",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                      Expanded(
                          child: Text(
                        widget.ground.address,
                        style: TextStyle(wordSpacing: 5),
                      )),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 7),
              child: Card(
                elevation: 4,
                child: Column(
                  children: [
                    const Padding(
                      padding: EdgeInsets.all(15.0),
                      child: Text(
                        "Weekly Slots Schedule",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                    ),
                    DefaultTabController(
                        length: 7, // length of tabs
                        initialIndex: DateTime.now().weekday-1,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Container(
                                child: TabBar(
                                  indicatorSize: TabBarIndicatorSize.label,
                                  indicator: BoxDecoration(
                                      borderRadius: BorderRadius.circular(50),
                                      color: MyResources.buttonColor),
                                  labelColor: Colors.white,
                                  unselectedLabelColor: MyResources.buttonColor,
                                  isScrollable: true,
                                  tabs: [
                                    Tab(
                                      child: Container(
                                        width: 48,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(50),
                                            border: Border.all(
                                                color: MyResources.buttonColor,
                                                width: 1)),
                                        child: Align(
                                          alignment: Alignment.center,
                                          child: Text("Mon"),
                                        ),
                                      ),
                                    ),
                                    Tab(
                                      child: Container(
                                        width: 48,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(50),
                                            border: Border.all(
                                                color: MyResources.buttonColor,
                                                width: 1)),
                                        child: Align(
                                          alignment: Alignment.center,
                                          child: Text("Tue"),
                                        ),
                                      ),
                                    ),
                                    Tab(
                                      child: Container(
                                        width: 48,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(50),
                                            border: Border.all(
                                                color: MyResources.buttonColor,
                                                width: 1)),
                                        child: Align(
                                          alignment: Alignment.center,
                                          child: Text("Wed"),
                                        ),
                                      ),
                                    ),
                                    Tab(
                                      child: Container(
                                        width: 48,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(50),
                                            border: Border.all(
                                                color: MyResources.buttonColor,
                                                width: 1)),
                                        child: Align(
                                          alignment: Alignment.center,
                                          child: Text("Thu"),
                                        ),
                                      ),
                                    ),
                                    Tab(
                                      child: Container(
                                        width: 48,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(50),
                                            border: Border.all(
                                                color: MyResources.buttonColor,
                                                width: 1)),
                                        child: Align(
                                          alignment: Alignment.center,
                                          child: Text("Fri"),
                                        ),
                                      ),
                                    ),
                                    Tab(
                                      child: Container(
                                        width: 48,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(50),
                                            border: Border.all(
                                                color: MyResources.buttonColor,
                                                width: 1)),
                                        child: Align(
                                          alignment: Alignment.center,
                                          child: Text("Sat"),
                                        ),
                                      ),
                                    ),
                                    Tab(
                                      child: Container(
                                        width: 48,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(50),
                                            border: Border.all(
                                                color: MyResources.buttonColor,
                                                width: 1)),
                                        child: Align(
                                          alignment: Alignment.center,
                                          child: Text("Sun"),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                  margin: const EdgeInsets.only(top: 15),
                                  height: 190, //height of TabBarView
                                  child: TabBarView(children: <Widget>[
                                    for(var tem in widget.ground.schedule)
                                    Column(
                                      children: [
                                        slotList(tem),
                                      ],
                                    ),
                                  ]))
                            ])),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(7.0),
              child: Card(
                elevation: 4,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        "Amenities",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      padding: const EdgeInsets.all(8),
                      child: Wrap(
                        children: [
                          Container(
                            margin: const EdgeInsets.all(5),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 12.0, vertical: 5),
                            decoration: BoxDecoration(
                                color: MyResources.headingColor,
                                borderRadius: BorderRadius.circular(8)),
                            child: Text(
                              "Parking",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.all(5),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 12.0, vertical: 5),
                            decoration: BoxDecoration(
                                color: MyResources.headingColor,
                                borderRadius: BorderRadius.circular(8)),
                            child: Text(
                              "Wickets",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.all(5),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 12.0, vertical: 5),
                            decoration: BoxDecoration(
                                color: MyResources.headingColor,
                                borderRadius: BorderRadius.circular(8)),
                            child: Text(
                              "Umpire",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.all(5),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 12.0, vertical: 5),
                            decoration: BoxDecoration(
                                color: MyResources.headingColor,
                                borderRadius: BorderRadius.circular(8)),
                            child: Text(
                              "Scorrer",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.all(5),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 12.0, vertical: 5),
                            decoration: BoxDecoration(
                                color: MyResources.headingColor,
                                borderRadius: BorderRadius.circular(8)),
                            child: Text(
                              "Washroom",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.all(5),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 12.0, vertical: 5),
                            decoration: BoxDecoration(
                                color: MyResources.headingColor,
                                borderRadius: BorderRadius.circular(8)),
                            child: Text(
                              "Balls",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.all(5),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 12.0, vertical: 5),
                            decoration: BoxDecoration(
                                color: MyResources.headingColor,
                                borderRadius: BorderRadius.circular(8)),
                            child: Text(
                              "Live Steaming",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 15.0),
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AddBooking()
                         ),
                  );
                },
                child: Text("Book now"),
                style: ElevatedButton.styleFrom(
                    primary: MyResources.buttonColor,
                    padding:
                        EdgeInsets.symmetric(horizontal: 100, vertical: 10),
                    textStyle:
                        TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
              ),
            ),
            SizedBox(
              height: 20,
            )
          ],
        ),
      ),
    );
  }

  Widget slotList(Template day) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 5),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.black, width: 1),
          borderRadius: BorderRadius.circular(3)),
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(3),
              color: Colors.grey.shade300,
            ),
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children:  const [
                Expanded(flex: 2, child: Center(child: Text("Slot no"))),
                Expanded(flex: 3, child: Center(child: Text("price"))),
                Expanded(flex: 3, child: Center(child: Text("Start Time"))),
                Expanded(flex: 2, child: Center(child: Text("Hours"))),
              ],
            ),
          ),
          Container(
            height: 150,
            child: ListView.builder(
              padding: EdgeInsets.zero,
              physics: BouncingScrollPhysics(),
                itemCount: day.slots.length,
                itemBuilder: (BuildContext context, int index) {
                  Slot slot = day.slots[index];
                  return Card(
                    elevation: 4,
                    child: Container(
                      color: const Color(0xfff1f2f8),
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                              flex: 2,
                              child:
                              Center(child: Text((index + 1).toString()))),
                          Expanded(
                              flex: 3,
                              child:
                              Center(child: Text(slot.price.toString()))),
                          Expanded(
                              flex: 3,
                              child: Center(
                                  child: Text(DateFormat('hh:mm a')
                                      .format(slot.startTime)))),
                          Expanded(
                              flex: 2,
                              child:
                              Center(child: Text((slot.hours).toString()))),
                        ],
                      ),
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }
}
