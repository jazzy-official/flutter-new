import 'installment_model.dart';

class Recovery {
  double? receivable;
  int? contractId;
  String? contractDate;
  double? totalPrice;
  double? advancePayment;
  int? buyerAccount;
  double? noOfInstallments;
  double? saleRate;
  int? unitId;
  String? unitNumber;
  String? phaseName;
  double? netMarla;
  double? netSqFoot;
  double? unitMarla;
  String? accountTitle;
  String? status;
  List<Installment> installmentPlan;
  String? clientCnic;
  int? clientId;
  String? clientName;
  String? clientCity;
  String? clientCountry;
  String? clientZipcode;
  String? clientAddress;
  String? clientCell;

  Recovery({
    this.status,
    this.contractId,
    this.receivable,
    this.unitId,
    this.accountTitle,
    this.advancePayment,
    this.buyerAccount,
    this.contractDate,
    this.netMarla,
    this.netSqFoot,
    this.noOfInstallments,
    this.phaseName,
    this.saleRate,
    this.totalPrice,
    this.unitMarla,
    this.unitNumber,
    required this.installmentPlan,
    this.clientAddress,
    this.clientCell,
     this.clientCity,
     this.clientCnic,
     this.clientCountry,
     this.clientId,
     this.clientName,
     this.clientZipcode,
  });

  factory Recovery.fromJson(Map<String, dynamic> json) {
    return Recovery(
      receivable: json['receivable'],
      contractId: json['contractId'],
      contractDate: json['contractDate'],
      totalPrice: json['totalPrice'],
      advancePayment: json['advancePayment'],
      buyerAccount: json['buyerAccount'],
      noOfInstallments: json['noOfInstallments'],
      saleRate: json['saleRate'],
      unitId: json['unitId'],
      unitNumber: json['unitNumber'],
      phaseName: json['phaseName'],
      netMarla: json['netMarla'],
      netSqFoot: json['netSqft'],
      unitMarla: json['unitMarla'],
      accountTitle: json['accounttitle'],
      status: json['status'],
      installmentPlan: List<Installment>.from(json["installmentPlan"]
          .map(
            (dynamic item) => Installment.fromJson(item),
          )
          .toList()),
      clientCountry: json['client']['clientCnicNavigation']['clientCountry'],
      clientCity: json['client']['clientCnicNavigation']['clientCity'],
      clientZipcode: json['client']['clientCnicNavigation']['clientZipcode'],
      clientName: json['client']['clientCnicNavigation']['clientName'],
      clientId:json['client']['clientCnicNavigation']['clientId'],
      clientCnic: json['client']['clientCnicNavigation']['clientCnic'],
      clientAddress: json['client']['clientCnicNavigation']['clientAddress'],
      clientCell: json['client']['clientCnicNavigation']['clientCell'],
    );
  }
}
