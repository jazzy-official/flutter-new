
import 'dart:convert';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/ChartsAccount/Model/document_modal.dart';
import 'package:rafi_admin/Controllers/authcontroller.dart';


class ChartAccountController extends GetxController {
  final auth = Get.put(AuthController());
  List<Document>? accounts;
  String? token;
  int? number;
  @override
  Future<void> onInit() async {
    await refreshAll();
    super.onInit();
  }

  Future refreshAll() async {
    token = await auth.getToken();
    number = await auth.getProjectId();
    await getCharts();

  }
  Future getCharts() async {
    http.Response response = await http.get(
      Uri.parse('https://raccounting.azurewebsites.net/api/Headings/GetHeadings?projectid=$number'),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );
    if (response.statusCode == 200) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      List<dynamic> data = await jsonDecode(response.body) ;
      List<Document> account = data
          .map(
            (dynamic item) => Document.fromJson(item),
      ).toList();

      accounts = account;
      update();

    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to get recovery data');
    }
  }

}
