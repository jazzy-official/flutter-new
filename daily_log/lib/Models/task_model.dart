import 'package:daily_log/Models/daili_log_model.dart';
import 'package:daily_log/Models/todo_model.dart';

class TaskModel {
  final String? id;
  final String des;
  final String name;
  final bool status;
  final DateTime deadline;
  final String? assignto;
  final String? project;
  final String? projectid;
  final String? module;
  final String? moduleid;
  final String? submodule;
  final String? submoduleid;
  final DateTime? completedDate;
  final List<Todo> todos;

  TaskModel(
      {this.id,
      this.assignto,
      this.completedDate,
      required this.deadline,
      required this.des,
      required this.status,
      required this.name,
      required this.todos,
      this.project,
      this.projectid,
        this.module,
        this.submodule,
        this.moduleid,
        this.submoduleid,
      });

  factory TaskModel.fromJson(dynamic json, key) {
    return TaskModel(
      id: key.toString(),
      name: json['name'],
      des: json['description'],
      status: json['status'],
      deadline: DateTime.parse(json['deadline']),
      assignto: json['assignto'],
      project: json['project'],
      projectid: json['projectid'],
      module: json['module'],
      moduleid: json['moduleid'],
      submoduleid: json['submoduleid'],
      submodule: json['submodule'],
      completedDate: json['completedDate'] == "null"
          ? null
          : DateTime.parse(json["completedDate"]),
      todos: json["todo"] == null
          ? []
          : List<Todo>.from(json["todo"]
              .map(
                (dynamic item) => Todo.fromJson(item, ""),
              )
              .toList()),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['description'] = des;
    data['name'] = name;
    data['status'] = status;
    data['deadline'] = deadline.toString();
    data['assignto'] = assignto;
    data['project'] = project;
    data['projectid'] = projectid;
    data['module'] = module;
    data['moduleid'] = moduleid;
    data['submoduleid'] = submoduleid;
    data['submodule'] = submodule;
    data['todo'] = List<dynamic>.from(todos.map((x) => x.toJson()));
    data['completedDate'] = completedDate.toString();
    return data;
  }
}
