import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

import '../Models/user_model.dart';

class EmployeeController extends GetxController {
  List<UserModel> employees = [];
  List<UserModel> alluser = [];

  final DatabaseReference databaseReference =
  FirebaseDatabase.instance.ref("Users");

  @override
  Future<void> onInit() async {
    // TODO: implement onInit
    readUsers();
    super.onInit();
  }


  Future<void> readUsers() async {
    List<UserModel> employee = [];
    List<UserModel> allusers = [];
    await databaseReference.once().then((data) {
      if(data.snapshot.value != null){
        Map<dynamic,dynamic> values = data.snapshot.value as Map<dynamic,dynamic>;
        values.forEach((key, value) {
          UserModel gr = UserModel.fromJson(value,key);
          allusers.add(gr);
          if(gr.role == "Employee"){
            employee.add(gr);
          }
        });
      }
    });
    employees = employee;
    alluser = allusers;
    update();
  }

}