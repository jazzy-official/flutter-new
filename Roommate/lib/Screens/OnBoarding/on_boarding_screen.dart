import 'package:flutter/material.dart';
import 'package:onboarding/onboarding.dart';

import 'first_screen.dart';
class OnBoardingScreen extends StatelessWidget {
final onboardingPagesList = [
  PageModel(
    widget: Column(
      children: [
        Container(
            padding: EdgeInsets.only(bottom: 45.0),
            child: Image.asset('assets/onboard1.png')),
        Container(
            width: double.infinity,
            child: Text('SECURED BACKUP', style: pageTitleStyle)),
        Container(
          width: double.infinity,
          child: Text(
            'Keep your files in closed safe so you can\'t lose them',
            style: pageInfoStyle,
          ),
        ),
      ],
    ),
  ),
  PageModel(
    widget: Column(
      children: [
        Image.asset('assets/onboard2.png'),
        Text('CHANGE AND RISE', style: pageTitleStyle),
        Text(
          'Give others access to any file or folder you choose',
          style: pageInfoStyle,
        )
      ],
    ),
  ),
  PageModel(
    widget: Column(
      children: [
        Image.asset('assets/onboard3.png',),
        Text('EASY ACCESS', style: pageTitleStyle),
        Text(
          'Reach your files anytime from any devices anywhere',
          style: pageInfoStyle,
        ),
      ],
    ),
  ),
];

@override
Widget build(BuildContext context) {
 return Onboarding(
      proceedButtonStyle: ProceedButtonStyle(
        proceedpButtonText: Text("Get Started",style: TextStyle(color: Colors.white),),
        proceedButtonRoute: (context) {
          return Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
              builder: (context) => const FirstScreen(),
            ),
                (route) => false,
          );
        },
      ),

      pages: onboardingPagesList,
      indicator: Indicator(
        indicatorDesign: IndicatorDesign.line(
          lineDesign: LineDesign(
            lineType: DesignType.line_nonuniform,
          ),
        ),
      ),
      //-------------Other properties--------------
      //Color background,
      //EdgeInsets pagesContentPadding
      //EdgeInsets titleAndInfoPadding
      //EdgeInsets footerPadding
      //SkipButtonStyle skipButtonStyle
    );

}
}