import 'dart:io';
import 'dart:math';

import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:ap_academy_admin/screens/LoginSignup/signup_screen.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';

class firebaseStoge extends GetxController {
  FirebaseStorage firebaseStorage = FirebaseStorage.instance;
  final dbHelper = Get.put(DatabaseController());
  getImages() {
    firebaseStorage.ref().child('images').child('defaultProfile.png');
  }

  Future<String?> uploadImage(File image, String name) async {
    try {
      // Make random image name.
      int randomNumber = Random().nextInt(100000);
      String imageLocation = 'images/User/name${randomNumber}.jpg';

      // Upload image to firebase.
      final Reference storageReference =
          FirebaseStorage.instance.ref().child(imageLocation);
      final uploadTask = storageReference.putFile(image);
      await uploadTask.whenComplete(() async {
        final ref = FirebaseStorage.instance.ref().child(imageLocation);
        String _uploadedFileURL = await ref.getDownloadURL();
        if (_uploadedFileURL.isNotEmpty)
          return _uploadedFileURL;
        else
          return "false";
      });
    } catch (e) {
      print(e);
      Get.snackbar("Error ", "Photo not added . please retry");
    }

    // Get image URL from firebase
  }
}
