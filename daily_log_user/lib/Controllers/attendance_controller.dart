import 'package:daily_log_user/Controllers/profilecontroller.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import '../Models/commit.dart';
import '../Models/daili_log_model.dart';
import '../Models/task_model.dart';


class AttendanceController extends GetxController {

  List<DailyLog> commits = [];
  DailyLog? logbydate;
  DailyLog? todaylog;
  final DatabaseReference databaseReference =
  FirebaseDatabase.instance.ref("DailyLog");
  var profileController = Get.put(ProfileController());
  DateTime date = DateTime.now();
  String? description;
  @override
  Future<void> onInit() async {
    // TODO: implement onInit
    await getData();
    datefilter();
    super.onInit();
  }


  Future<void> addCommit(String taskid,String taskname,List<Commit> first,List<Commit> second) async {
    DailyLog commit = DailyLog(time: DateTime.now(), employee: profileController.userDetail.name,  secondhalf: second, employeeid: profileController.userDetail.id,  firsthalf: first);
    try{ await databaseReference.push().set(commit.toJson());
    await getData();
    update();
    }catch(e){
      Get.snackbar("Error", e.toString());
    }
  }

  datefilter(){
    update();
    try {
      logbydate = commits
          .where((element) =>
      element.time.day == date.day && element.time.month == date.month &&
          element.time.year == date.year)
          .toList()
          .first;
    } catch (e) {
      logbydate = null;
      update();
      print(e.toString());
    }

    try{
      todaylog = commits
          .where((element) =>
      element.time.day == DateTime
          .now()
          .day && element.time.month == DateTime
          .now()
          .month && element.time.year == DateTime
          .now()
          .year)
          .toList()
          .first;
    } catch (e) {
      todaylog = null;
      update();
    }
    update();

  }


  Future<void> updateCommit(DailyLog ball) async {
    databaseReference.update({
      ball.id! : ball.toJson()
    });
    await getData();
    update();
  }
  //
  //
  // Future<void> deleteTask(String id) async {
  //   databaseReference.child(id).remove();
  //
  // }
  //
  Future<void> getData() async {
    List<DailyLog> ball = [];
    await databaseReference.once().then(( data) {

      if(data.snapshot.value != null){
        Map<dynamic,dynamic> values = data.snapshot.value as Map<dynamic,dynamic>;
        values.forEach((key, value) {
          DailyLog gr = DailyLog.fromJson(value,key);
          if(gr.employee == profileController.userDetail.name) {
            ball.add(gr);
          }
        });
      }

    });
    commits = ball;
    datefilter();
    update();
  }

}