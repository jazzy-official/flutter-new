import 'package:daily_log/Models/daili_log_model.dart';
import 'package:daily_log/constants/util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../Models/commit.dart';

class LogDetailScreen extends StatelessWidget {
  final DailyLog log;
  const LogDetailScreen({Key? key, required this.log}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyResources.themecolor,
        title: Text(log.employee!),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: [
            const Center(
                child: Text("First Half",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                    ))),
            ListView.builder(
              controller: ScrollController(),
              shrinkWrap: true,
              itemCount: log.firsthalf.length,
              itemBuilder: (context, index) {
                Commit commit =
                log.firsthalf[index];
                return Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment:
                        CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text(
                                "Task : ",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600),
                              ),
                              Expanded(
                                child: Column(
                                  children: [
                                    Text(commit.task.toUpperCase(),style: TextStyle(
                                        fontWeight: FontWeight.w600),),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 10,),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text("Comment : ",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600),),
                              Expanded(
                                child: Column(
                                  children: [
                                    Text(commit.des),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ));
              },
            ),

            const Center(
                child: Text("Second Half",
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                    ))),
           ListView.builder(
              controller: ScrollController(),
              shrinkWrap: true,
              itemCount: log.secondhalf.length,
              itemBuilder: (context, index) {
                Commit commit =
                log.secondhalf[index];
                return Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment:
                        CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text(
                                "Task : ",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600),
                              ),
                              Expanded(
                                child: Column(
                                  children: [
                                    Text(commit.task.toUpperCase(),style: TextStyle(
                                        fontWeight: FontWeight.w600),),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 10,),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text("Comment : ",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600),),
                              Expanded(
                                child: Column(
                                  children: [
                                    Text(commit.des),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ));
              },
            )
          ],
        ),
      ),
    );
  }
}
