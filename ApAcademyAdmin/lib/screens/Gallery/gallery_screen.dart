import 'package:ap_academy_admin/ModelView/Controller/AlbumController.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:ap_academy_admin/screens/Gallery/add_new_album.dart';
import 'package:ap_academy_admin/screens/Gallery/addphotos_videos_inalbum.dart';
import 'package:ap_academy_admin/screens/shared/DeletPop.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import 'gallery_album.dart';

class GalleryScreen extends StatefulWidget {
  GalleryScreen({Key? key, this.title, this.i}) : super(key: key);
  final String? title;
  final int? i;
  @override
  _GalleryScreenState createState() => _GalleryScreenState();
}

class _GalleryScreenState extends State<GalleryScreen> {
  final galeyController = Get.put(PhotoAlbumController());

  int page = 1;
  bool isLoading = false;

  Future _loadData() async {
    await new Future.delayed(new Duration(seconds: 2));
    print("load more");
    galeyController.refreshData();
    setState(() {
      isLoading = false;
    });
  }

  @override
  void initState() {
    galeyController.refreshData();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 10.0, right: 10, left: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "",
                  style: MyResources.appHeadingStyle,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    width: Get.width * 0.3,
                    height: 25,
                    child: MaterialButton(
                      textColor: Colors.white,
                      color: MyResources.buttonColor,
                      child: Text(
                        "ADD ALBUM",
                      ),
                      onPressed: () {
                        Get.to(() => AddNewAlbum());
                      },
                      shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification scrollInfo) {
                if (!isLoading &&
                    scrollInfo.metrics.pixels ==
                        scrollInfo.metrics.maxScrollExtent) {
                  _loadData();
                  galeyController.refreshData();
                  // start loading data
                  setState(() {
                    isLoading = true;
                  });
                }
                return isLoading;
              },
              child: SingleChildScrollView(
                child: Container(
                    constraints: BoxConstraints(maxHeight: Get.height * 0.8),
                    // height: Get.height * 0.9,
                    child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child:
                            OrientationBuilder(builder: (context, orientation) {
                          return Obx(() {
                            if (galeyController.album != null &&
                                galeyController.album.isNotEmpty) {
                              return GridView.count(
                                  shrinkWrap: true,
                                  keyboardDismissBehavior:
                                      ScrollViewKeyboardDismissBehavior.onDrag,
                                  // Create a grid with 2 columns. If you change the scrollDirection to
                                  // horizontal, this produces 2 rows.
                                  crossAxisCount:
                                      orientation == Orientation.portrait
                                          ? 1
                                          : 2,
                                  // Generate 100 widgets that display their index in the List.
                                  children: List.generate(
                                      galeyController.album.length, (index) {
                                    return InkWell(
                                      onTap: () {
                                        Get.to(
                                          GalleryAlbum(i: index),
                                        );
                                      },
                                      child: Container(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            galeyController.album[index]
                                                            ['albumPhotos']
                                                        .toList()
                                                        .length !=
                                                    0
                                                ? Container(
                                              decoration: BoxDecoration(
                                                border: Border.all(color: Colors.black38,width: 1)
                                              ),
                                                  child: Image.network(
                                                      galeyController.album[index]
                                                          ['albumPhotos'][0],
                                                      width: Get.width,
                                                      height: 200,
                                                      fit: BoxFit.cover,
                                                    ),
                                                )
                                                : Container(
                                                    child: Center(
                                                      child: Icon(
                                                        Icons.image_outlined,
                                                        size: 200,
                                                      ),
                                                    ),
                                                  ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Container(
                                                        constraints:
                                                            BoxConstraints(
                                                                minWidth: 100,
                                                                maxWidth: 100),
                                                        child: Text(
                                                          galeyController
                                                                  .album[index]
                                                              ['albumName'],
                                                          textAlign:
                                                              TextAlign.left,
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 16,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        ),
                                                      ),
                                                      MaterialButton(
                                                        onPressed: () async {
                                                          Get.dialog(Center(
                                                              child: DeletpopUp(
                                                            message:
                                                                'Are you sure you want to delete?',
                                                            pressFunc:
                                                                () async {
                                                              await galeyController
                                                                  .delAlbumn(
                                                                      galeyController
                                                                              .album[index]
                                                                          [
                                                                          '_id'],
                                                                      galeyController
                                                                              .album[index]
                                                                          [
                                                                          'albumPhotos'])
                                                                  .then((value) =>
                                                                      galeyController
                                                                          .refreshData());
                                                              Get.back();
                                                            },
                                                          )));
                                                        },
                                                        child: Icon(
                                                          Icons.delete_forever,
                                                          semanticLabel:
                                                              'Delet Album',
                                                          color:
                                                              Colors.redAccent,
                                                        ),
                                                      ),

                                                    ],
                                                  ),
                                                  SizedBox(
                                                    height: 20,
                                                  ),
                                                  Text(
                                                    'Total Pictures : ${galeyController.album[index]['albumPhotos'].length}',
                                                    style: TextStyle(
                                                        color: Colors.grey),
                                                  ),
                                                  Text(
                                                    'Created at ${DateFormat.yMEd().format(galeyController.album[index]['CreatedAt'])}',
                                                    style: TextStyle(
                                                        color: Colors.grey),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  }));
                            } else {
                              return Container();
                            }
                          });
                        }))),
              ),
            ),
          ),
          Container(
            height: isLoading ? 50.0 : 0,
            color: Colors.transparent,
            child: Center(
              child: new LoadingWidget(),
            ),
          ),
        ],
      ),
    );
  }
}
