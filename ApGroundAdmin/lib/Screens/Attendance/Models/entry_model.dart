class Entry {
  final String staffId;
  final String name;
  final String designation;
  bool isPresent;


  Entry({required this.staffId,required this.name,required this.designation,required this.isPresent});

// NOTE: implementing functionality here in the next step!

  factory Entry.fromJson(dynamic json) {
    return Entry(
      staffId: json['staffId'],
      name : json['name'],
      designation : json['designation'].toString(),
      isPresent: json['isPresent']
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['designation'] = this.designation;
    data['isPresent'] = this.isPresent;
    data['staffId'] = this.staffId;
    return data;
  }
}