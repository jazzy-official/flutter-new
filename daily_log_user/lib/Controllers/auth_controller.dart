import 'dart:core';
import 'package:daily_log_user/Controllers/firebase_controller.dart';
import 'package:daily_log_user/Controllers/profilecontroller.dart';
import 'package:daily_log_user/View/Home/home.dart';
import 'package:daily_log_user/View/drawer_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';

import '../Models/user_model.dart';
import '../View/Login/login_screen.dart';


class AuthController extends GetxController {



  @override
  Future<void> onInit() async {
    super.onInit();
  }

  var firebaseController = Get.put(FirebaseController());
  var prof = Get.put(ProfileController());
  FirebaseApp? firebaseApp;
  User? firebaseUser;
  FirebaseAuth? firebaseAuth;

  Future<void> initlizeFirebaseApp() async {
    firebaseApp = await Firebase.initializeApp();
    update();
  }

  final DatabaseReference databaseReference =
  FirebaseDatabase.instance.reference().child("Users");

  Future<void> signInwithEmail(String emails, String passs) async {
    try {
      // Get.dialog(Center(child: LoadingWidget()), barrierDismissible: false);

      firebaseAuth = FirebaseAuth.instance;

      final userCredentialData = await firebaseAuth!.signInWithEmailAndPassword(
          email: emails, password: passs);
      firebaseUser = userCredentialData.user!;
      await prof.initializeUserData(firebaseUser!.uid);
      update();
      Get.offAll(() =>  HomeDrawer());

    } catch (ex) {
      print(ex.toString());
      Get.back();
      signOut();
      Get.snackbar('Sign In Error', ex.toString(),
          duration: Duration(seconds: 5),
          backgroundColor: Colors.black,
          colorText: Colors.white,
          snackPosition: SnackPosition.TOP,
          icon: const Icon(
            Icons.error,
            color: Colors.red,
          ));
    }
  }
  Future<void> signInWithGoogle(BuildContext context) async {
    // Trigger the authentication flow
    final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

    // Obtain the auth details from the request
    final GoogleSignInAuthentication? googleAuth = await googleUser?.authentication;

    // Create a new credential
    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth?.accessToken,
      idToken: googleAuth?.idToken,
    );

    // Once signed in, return the UserCredential
    try {
      await FirebaseAuth.instance.signInWithCredential(
          credential).then((value)  async{
        User? firebaseUser = value.user!;
        if (await checkdublicate(firebaseUser.uid) == false) {
          final _user = UserModel(
            email: firebaseUser.email!,
            imgUrl: firebaseUser.photoURL!,
            role: 'Employee',
            uid: firebaseUser.uid,
            name: firebaseUser.displayName!,

          );
          await databaseReference.push().set(_user.toJson());
          await prof.initializeUserData(firebaseUser.uid);
          prof.update();
          prof.refresh();
          update();
          Get.offAll(HomeDrawer());
        }else{
          await prof.initializeUserData(firebaseUser.uid);
          prof.update();
          prof.refresh();
          update();
          Get.offAll(HomeDrawer());
        }
          });

    } catch(e){
      Get.snackbar("error",e.toString());
    }
  }
  Future<void> signUp(String email,String pass,String name) async {
    try {
      User? fireuser = await firebaseController.signup(email, pass,);
      UserModel user = UserModel(name: name, email: email, uid: fireuser!.uid, role: "Employee");
      await databaseReference.push().set(user.toJson());
      await prof.initializeUserData(fireuser.uid);
      Get.offAll(() =>  HomeDrawer());
    } catch (e) {
      // displayToast(e.toString());
      print(e.toString());
    }
  }
  Future<Widget?> checkUserLoggedIn() async {
    try {
      if (firebaseApp == null) {
        await initlizeFirebaseApp();
      }
      if (firebaseAuth == null) {
        firebaseAuth = FirebaseAuth.instance;
        update();
      }
    }catch(err)
    {
      print("err "+err.toString());
    }
    if (firebaseAuth!.currentUser == null) {
      return LoginScreen();
    } else {
      firebaseUser = firebaseAuth!.currentUser!;
      await prof.initializeUserData(firebaseUser!.uid);
      return HomeDrawer();
      // int isUserState= await profileController.initializeUserData();
    }
  }
  Future<void> signOut() async {

    await firebaseAuth!.signOut();
    update();

    // Navigate to Login again
    Get.offAll(() =>  LoginScreen());
  }


  Future<bool> checkdublicate(String firebaseId) async {
    try {
      UserModel? usr = await readData(firebaseId);

      if (usr != null) {
        print("true");
        return true;
      } else {
        print("false");
        return false;
      }
    } catch (ex) {
      print(ex.toString());
      return false;
    }
  }
  Future<UserModel?> readData(String uid) async {
    late UserModel? user;
    await databaseReference
        .orderByChild("uid")
        .equalTo(uid)
        .once()
        .then((value) => {
      if (value.snapshot.value != null)
        {
          Map.of(value.snapshot.value as Map<dynamic,dynamic>).forEach((key, value) {
            user = UserModel.fromJson(value, key);
          })
        }
    });

    return user;
  }
}
