
import 'dart:convert';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/RecievePaidChecklist/receive_paid_checklist.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/Recovery/recovery.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/Units%20Model/units.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/account%20model/accounts.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Payables/Model/payable_model.dart';
import 'package:rafi_admin/Components/Screens/Inventory/Models/Unit%20List/unit_list_model.dart';
import 'package:rafi_admin/Components/Screens/Inventory/Models/Unit%20Model/unit_model.dart';
import 'package:rafi_admin/Components/Screens/Purchase/Model/investment_model.dart';
import 'package:rafi_admin/Components/Screens/Purchase/Model/investments_list_model.dart';
import 'package:rafi_admin/Components/Screens/Sales/Models/deals_model.dart';
import 'package:rafi_admin/Controllers/authcontroller.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PurchaseController extends GetxController {
  final auth = Get.put(AuthController());
  String url = 'https://raccounting.azurewebsites.net/api/Projects/Project/ProjectInvestment?pageNumber=1&pageSize=100';
  InvestmentPageModel? investmentsData;
  late List<InvestmentModel> foundResult = [];
  int? value = 1;
  String? token;
  int? number;

  @override
  Future<void> onInit() async {
    await refreshAll();
    super.onInit();
  }

  Future refreshAll() async {
    token = await auth.getToken();
    number = await auth.getProjectId();
    await getInvestmentData();

  }

  Future getInvestmentData() async {
    http.Response response = await http.get(
      Uri.parse(url + "&projectId=$number"),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        "Authorization" : "Bearer "+token!,
      },
    );
    if (response.statusCode == 200) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      investmentsData = InvestmentPageModel.fromJson(jsonDecode(response.body));
      foundResult = investmentsData!.investments;
      update();
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to get recovery data');
    }
  }

  void runFilter(String enteredKeyword) {
    List<InvestmentModel> results = [];
    if (enteredKeyword.isEmpty) {
      // if the search field is empty or only contains white-space, we'll display all users
      results = investmentsData!.investments;
    } else {
      results = investmentsData!.investments
          .where((user) => user.investmentName
          .toUpperCase()
          .contains(enteredKeyword.toUpperCase()))
          .toList();


      // we use the toLowerCase() method to make it case-insensitive
    }

    // Refresh the UI
    foundResult = results;
    update();
  }


}
