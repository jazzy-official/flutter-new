class TrainingModel {
  final dynamic id;
  final String session;
  final String coach;
  final String venue;
  final String time;
  final int attendance;
  final int totalPlayer;

  const TrainingModel(
      {this.id,
      required this.session,
      required this.coach,
      required this.venue,
      required this.time,
      required this.attendance,
      required this.totalPlayer});
}
