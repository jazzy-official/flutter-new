import 'package:daily_log/Models/daili_log_model.dart';
import 'package:daily_log/Models/module.dart';
import 'package:daily_log/Models/project_model.dart';
import 'package:daily_log/Models/task_model.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

import '../Models/sub_module.dart';
import '../Models/user_model.dart';
import '../View/loading_widget.dart';

class ProjectController extends GetxController {
  List<ProjectModel> projects = [];
  final DatabaseReference databaseReference =
  FirebaseDatabase.instance.ref("Projects");

  @override
  Future<void> onInit() async {
    // TODO: implement onInit
    readBall();
    super.onInit();
  }


  Future<void> addProject(ProjectModel project) async {
     try{
       Get.defaultDialog(title: '', content: const LoadingWidget(),backgroundColor: Colors.transparent,barrierDismissible: false);

       await databaseReference.push().set(project.toJson());
        await readBall();
        Get.back();
        Get.back();
        Get.snackbar("Done", "Project added");
      } catch(e) {
        Get.snackbar("Error", e.toString(),snackPosition: SnackPosition.BOTTOM);
      }

    update();


  }

  Future<void> updateProject(ProjectModel ball) async {
    Get.defaultDialog(title: '', content: const LoadingWidget(),backgroundColor: Colors.transparent,barrierDismissible: false);
    databaseReference.update({
      ball.id! : ball.toJson()
    });
    Get.back();
    Get.back();
    Get.snackbar("Done", "Project Updated",snackPosition: SnackPosition.BOTTOM);
  }


  Future<void> deleteProject(String id) async {
    databaseReference.child(id).remove();

  }

  Future<void> readBall() async {
    List<ProjectModel> temprojects = [];
    await databaseReference.once().then(( data) {

      if(data.snapshot.value != null){
        Map<dynamic,dynamic> values = data.snapshot.value as Map<dynamic,dynamic>;
        values.forEach((key, value) {
          ProjectModel gr = ProjectModel.fromJson(value,key);
          temprojects.add(gr);
        });
      }

    });
    projects = temprojects;
    update();
  }

  Future<ProjectModel> getProjectById(String id) async {
    Map<dynamic,dynamic> values = (await FirebaseDatabase.instance.reference().child("Projects/$id").once()).snapshot.value as Map<dynamic,dynamic>;
    ProjectModel gr = ProjectModel.fromJson(values,id);
    return gr;
  }


  Future<void> updateSubModule(SubModule ball,String projectid,String moduleid) async {
    final DatabaseReference Reference =
    FirebaseDatabase.instance.ref("Projects/$projectid/modules/$moduleid/submodules");
    try {
      await Reference.update({
        ball.id!: ball.toJson()
      });
    }catch(e){
      await Get.snackbar("Done", e.toString(),snackPosition: SnackPosition.BOTTOM,);

    }

  }


  Future<void> updateModule(Module ball,String projectid,) async {
    final DatabaseReference Reference =
    FirebaseDatabase.instance.ref("Projects/$projectid/modules");
    await Reference.update({
      ball.id! : ball.toJson()
    });
  }



}