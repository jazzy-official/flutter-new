import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:intl/intl.dart';

class OtherEventDetails extends StatelessWidget {
  final Map<String, dynamic>? otherEvent;

  const OtherEventDetails({Key? key, this.otherEvent}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 0.0, left: 10, right: 10),
      child: Center(
        child: Container(
          height: Get.height / 1.5,
          child: Card(
            color: MyResources.backgroundColor,
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.bottomCenter,
                  child: ColorFiltered(
                      colorFilter: ColorFilter.mode(
                          Colors.white.withOpacity(0.3), BlendMode.dstATop),
                      child: Image.asset(
                        'assets/event.png',
                        colorBlendMode: BlendMode.hue,
                        width: 200,
                        height: 200,
                      )),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        height: 35,
                        color: MyResources.buttonColor.withOpacity(0.7),
                        child: Center(
                          child: Text(
                            "EVENT DETAILS",
                            style: MyResources.modelHeadingStyle,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 40,
                        right: 75,
                        left: 10,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 10,
                        right: 10,
                        left: 10,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              height: 50,
                              width: 100,
                              child: Text("Event Name:",
                                  style: MyResources.titleTextStyle)),
                          Container(
                            height: 50,
                            width: 200,
                            child: Text(
                              otherEvent!['eventName'],
                              style: MyResources.nameTextStyle,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 10,
                        right: 10,
                        left: 10,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                              height: 50,
                              width: 100,
                              child: Text("Venue:",
                                  style: MyResources.titleTextStyle)),
                          Container(
                            height: 50,
                            width: 200,
                            child: Text(
                              otherEvent!['venue'],
                              style: MyResources.nameTextStyle,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 10, right: 10, left: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text("Time:", style: MyResources.titleTextStyle),
                          /*SizedBox(
                             width: 30,
                           ),*/
                          Container(
                            width: 200,
                            child: Text(
                              DateFormat.yMEd()
                                  .add_jm()
                                  .format(otherEvent!['time'].toLocal()),
                              style: TextStyle(color: MyResources.headingColor),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
                Positioned(
                    left: 10,
                    top: 4,
                    child: BackButton(
                      color: Colors.white,
                      onPressed: () {
                        Get.back();
                      },
                    )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
