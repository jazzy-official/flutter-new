import 'package:flutter/cupertino.dart';
import 'package:ap_academy_admin/ModelView/Controller/auth_controller.dart';
import 'package:ap_academy_admin/ModelView/Controller/profileController.dart';
import 'package:ap_academy_admin/screens/Gallery/gallery_screen.dart';
import 'package:ap_academy_admin/screens/Coaches/adminCoachTabPage.dart';
import 'package:ap_academy_admin/screens/Setting/profile_edit.dart';
import 'package:ap_academy_admin/screens/Sponsors/SponsorsList.dart';
import 'package:ap_academy_admin/screens/shared/DeletPop.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ap_academy_admin/screens/Teams/teams_screen.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import 'Announcements/Notifications_screen.dart';

import 'Banks/BankList.dart';
import 'Setting/ApprovalList.dart';
import 'Teams/teamsTabsScreen.dart';
import 'dashboard_screen.dart';
import 'Events/events_screen.dart';

import 'package:url_launcher/url_launcher.dart';

RxString sideTitle = "Dashboard".toUpperCase().obs;

class HomeDrawer extends StatefulWidget {
  @override
  _HomeDrawerState createState() => _HomeDrawerState();
}

class _HomeDrawerState extends State<HomeDrawer> {
  //bool _selected;

  final profileController = Get.put(ProfileController());
  static const _url = 'https://aiksol.com';
  Widget selectedWidget = DashBoard();

  final _auth = Get.put(AuthController());
  Future<void> _launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: false,
        forceWebView: false,
        headers: <String, String>{'my_header_key': 'my_header_value'},
      );
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyResources.buttonColor,
        brightness: Brightness.dark,
        title: Obx(() => Text(sideTitle.value)),
        centerTitle: true,
      ),
      drawer: Theme(
        data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
        child: Drawer(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            InkWell(
              onTap: () {
                Get.to(() => ProfileEdit(
                      isDrawer: true,
                    ));
              },
              child: Stack(
                children: [
                  Container(
                    height: 180,
                    width: Get.width,
                    child: SvgPicture.asset(
                      'assets/drawerBack.svg',
                      allowDrawingOutsideViewBox: true,
                      //   width: Get.width,
                      fit: BoxFit.fill,
                    ),
                  ),
                  DrawerHeader(
                      padding: EdgeInsets.all(0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(35),
                            bottomRight: Radius.circular(35)),
                        child: Container(
                          height: 150,
                          child: Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 8.0),
                                child: Obx(() => ClipRRect(
                                      borderRadius:
                                          BorderRadius.circular(100.0),
                                      child:
                                          profileController.userDetail['photoUrl'] != null
                                              ? CachedNetworkImage(
                                                  height: 80,
                                                  fit: BoxFit.cover,
                                                  width: 80,
                                                  imageUrl: profileController
                                                      .userDetail['photoUrl'],
                                                  placeholder: (context, url) =>
                                                      LoadingWidget(),
                                                  errorWidget:
                                                      (context, url, error) =>
                                                          Icon(Icons.error),
                                                )
                                              : Container(
                                                  width: 80,
                                                  height: 80,
                                                ),
                                    )),
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top: 8.0),
                                    child: Container(
                                      width: 170,
                                      child: Obx(() => Text(
                                            profileController.userDetail ==
                                                    null
                                                ? "Not Defined"
                                                : profileController
                                                    .userDetail['name']
                                                    .toString(),
                                            style: MyResources.appHeadingStyle,
                                            overflow: TextOverflow.fade,
                                          )),
                                    ),
                                  ),
                                  Container(
                                      width: 170,
                                      child: Obx(
                                        () => Text(
                                            profileController.userDetail == null
                                                ? "Not Defined"
                                                : profileController
                                                    .userDetail['email']
                                                    .toString(),
                                            textAlign: TextAlign.left,
                                            style: TextStyle(fontSize: 13)),
                                      )),
                                ],
                              ),
                            ],
                          ),
                        ),
                      )),
                ],
              ),
            ),
            Flexible(
              fit: FlexFit.tight,
              child: Container(
                // constraints: BoxConstraints(
                //     minHeight: Get.height * 0.46,
                //     maxHeight: GetPlatform.isAndroid
                //         ? Get.height * 0.5
                //         : Get.height * 0.5),
                child: SingleChildScrollView(
                  child: Container(
                      // constraints:
                      //     BoxConstraints.expand(height: Get.height * 0.45),
                      child: Column(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            border: sideTitle.value == "Dashboard".toUpperCase()
                                ? Border(
                                    left: BorderSide(
                                      color: MyResources.drawerIconColor,
                                      width: 5.0,
                                    ),
                                  )
                                : Border()),
                        child: ListTile(
                          dense: true,
                          tileColor:
                              sideTitle.value == "Dashboard".toUpperCase()
                                  ? MyResources.drawerIconColor.withOpacity(0.2)
                                  : Colors.transparent,
                          // selected: true,
                          title: Text(
                            "DashBoard".toUpperCase(),
                            style: MyResources.myTextStyle,
                          ),
                          leading: Icon(Icons.dashboard,
                              color: MyResources.drawerIconColor),
                          onTap: () {
                            if (mounted)
                              setState(() {
                                selectedWidget = DashBoard();
                                sideTitle = "Dashboard".toUpperCase().obs;
                              });
                            Get.back();
                          },
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            border: sideTitle.value == "Schedule".toUpperCase()
                                ? Border(
                                    left: BorderSide(
                                      color: MyResources.drawerIconColor,
                                      width: 5.0,
                                    ),
                                  )
                                : Border()),
                        child: ListTile(
                          dense: true,
                          tileColor: sideTitle.value == "Schedule".toUpperCase()
                              ? MyResources.drawerIconColor.withOpacity(0.2)
                              : Colors.transparent,
                          /*  selected: true,*/
                          title: Text(
                            "Schedule".toUpperCase(),
                            style: MyResources.myTextStyle,
                          ),
                          leading: Icon(Icons.event,
                              color: MyResources.drawerIconColor),
                          onTap: () {
                            if (mounted)
                              setState(() {
                                selectedWidget = EventScreen();
                                sideTitle = "Schedule".toUpperCase().obs;
                              });
                            Get.back();

                            // Navigator.push(context,
                            //     MaterialPageRoute(builder: (context) => EventScreen()));
                          },
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            border: sideTitle.value ==
                                "TEAMS".toUpperCase()
                                ? Border(
                              left: BorderSide(
                                color: MyResources.drawerIconColor,
                                width: 5.0,
                              ),
                            )
                                : Border()),
                        child: ListTile(
                          dense: true,
                          tileColor:
                          sideTitle.value == "TEAMS".toUpperCase()
                              ? MyResources.drawerIconColor.withOpacity(0.2)
                              : Colors.transparent,
                          // selected: true,
                          title: Text(
                            "TEAMS".toUpperCase(),
                            style: MyResources.myTextStyle,
                          ),
                          leading: Icon(CupertinoIcons.group,
                              color: MyResources.drawerIconColor),
                          onTap: () {
                            if (mounted)
                              setState(() {
                                selectedWidget = TeamsScreen();
                                sideTitle = "TEAMS".toUpperCase().obs;
                              });
                            Get.back();
                          },
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            border:
                                sideTitle.value == "Students/Groups".toUpperCase()
                                    ? Border(
                                        left: BorderSide(
                                          color: MyResources.drawerIconColor,
                                          width: 5.0,
                                        ),
                                      )
                                    : Border()),
                        child: ListTile(
                          dense: true,
                          tileColor:
                              sideTitle.value == "Students/Groups".toUpperCase()
                                  ? MyResources.drawerIconColor.withOpacity(0.2)
                                  : Colors.transparent,
                          // selected: true,
                          title: Text(
                            "Groups".toUpperCase(),
                            style: MyResources.myTextStyle,
                          ),
                          leading: Icon(Icons.group,
                              color: MyResources.drawerIconColor),
                          onTap: () {
                            if (mounted)
                              setState(() {
                                selectedWidget = TeamGroupTabs();
                                sideTitle = "Students/Groups".toUpperCase().obs;
                              });
                            Get.back();
                          },
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            border: sideTitle.value ==
                                    "STAFF".toUpperCase()
                                ? Border(
                                    left: BorderSide(
                                      color: MyResources.drawerIconColor,
                                      width: 5.0,
                                    ),
                                  )
                                : Border()),
                        child: ListTile(
                          dense: true,
                          tileColor:
                              sideTitle.value == "STAFF".toUpperCase()
                                  ? MyResources.drawerIconColor.withOpacity(0.2)
                                  : Colors.transparent,
                          // selected: true,
                          title: Text(
                            "STAFF".toUpperCase(),
                            style: MyResources.myTextStyle,
                          ),
                          leading: Icon(CupertinoIcons.person_2_square_stack,
                              color: MyResources.drawerIconColor),
                          onTap: () {
                            if (mounted)
                              setState(() {
                                selectedWidget = AdminPage();
                                sideTitle = "STAFF".toUpperCase().obs;
                              });
                            Get.back();
                          },
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            border: sideTitle.value == "Sponsors".toUpperCase()
                                ? Border(
                                    left: BorderSide(
                                      color: MyResources.drawerIconColor,
                                      width: 5.0,
                                    ),
                                  )
                                : Border()),
                        child: ListTile(
                          dense: true,
                          tileColor: sideTitle.value == "Sponsors".toUpperCase()
                              ? MyResources.drawerIconColor.withOpacity(0.2)
                              : Colors.transparent,
                          // selected: true,
                          title: Text(
                            "Sponsors".toUpperCase(),
                            style: MyResources.myTextStyle,
                          ),
                          leading: Icon(Icons.person_search_sharp,
                              color: MyResources.drawerIconColor),
                          onTap: () {
                            if (mounted)
                              setState(() {
                                selectedWidget = SponsorList();
                                sideTitle = "Sponsors".toUpperCase().obs;
                              });
                            Get.back();
                          },
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            border: sideTitle.value ==
                                    "Payment Methods".toUpperCase()
                                ? Border(
                                    left: BorderSide(
                                      color: MyResources.drawerIconColor,
                                      width: 5.0,
                                    ),
                                  )
                                : Border()),
                        child: ListTile(
                          dense: true,
                          tileColor:
                              sideTitle.value == "Payment Methods".toUpperCase()
                                  ? MyResources.drawerIconColor.withOpacity(0.2)
                                  : Colors.transparent,
                          // selected: true,
                          title: Text(
                            "Payment Methods".toUpperCase(),
                            style: MyResources.myTextStyle,
                          ),
                          leading: Icon(Icons.account_balance_outlined,
                              color: MyResources.drawerIconColor),
                          onTap: () {
                            if (mounted)
                              setState(() {
                                selectedWidget = BankList();
                                sideTitle = "Payment Methods".toUpperCase().obs;
                              });
                            Get.back();
                          },
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            border: sideTitle.value == "Gallery".toUpperCase()
                                ? Border(
                                    left: BorderSide(
                                      color: MyResources.drawerIconColor,
                                      width: 5.0,
                                    ),
                                  )
                                : Border()),
                        child: ListTile(
                          dense: true,
                          tileColor: sideTitle.value == "Gallery".toUpperCase()
                              ? MyResources.drawerIconColor.withOpacity(0.2)
                              : Colors.transparent,
                          title: Text(
                            "Gallery".toUpperCase(),
                            style: MyResources.myTextStyle,
                          ),
                          leading: Icon(Icons.image,
                              color: MyResources.drawerIconColor),
                          onTap: () {
                            if (mounted)
                              setState(() {
                                selectedWidget = GalleryScreen();
                                sideTitle = "Gallery".toUpperCase().obs;
                              });
                            Get.back();
                          },
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            border: sideTitle.value == "Announcements"
                                ? Border(
                                    left: BorderSide(
                                      color: MyResources.drawerIconColor,
                                      width: 5.0,
                                    ),
                                  )
                                : Border()),
                        child: ListTile(
                          title: Text(
                            "Announcements".toUpperCase(),
                            style: MyResources.myTextStyle,
                          ),
                          tileColor:
                              sideTitle.value == "Announcements".toUpperCase()
                                  ? MyResources.drawerIconColor.withOpacity(0.2)
                                  : Colors.transparent,
                          leading: Icon(
                            Icons.notifications,
                            color: MyResources.drawerIconColor,
                          ),
                          onTap: () {
                            if (mounted)
                              setState(() {
                                selectedWidget = AnnouncementScreen();
                                sideTitle = "Announcements".toUpperCase().obs;
                              });
                            Get.back();
                          },
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            border: sideTitle.value == "Pending Approvals"
                                ? Border(
                                    left: BorderSide(
                                      color: MyResources.drawerIconColor,
                                      width: 5.0,
                                    ),
                                  )
                                : Border()),
                        child: ListTile(
                          dense: true,
                          tileColor: sideTitle.value ==
                                  "Pending Approvals".toUpperCase()
                              ? MyResources.drawerIconColor.withOpacity(0.2)
                              : Colors.transparent,
                          title: Text(
                            "Pending Approvals".toUpperCase(),
                            style: MyResources.myTextStyle,
                          ),
                          leading: Icon(Icons.approval,
                              color: MyResources.drawerIconColor),
                          onTap: () {
                            if (mounted)
                              setState(() {
                                selectedWidget = ApprovalListPage(
                                  isAppBar: false,
                                );
                                sideTitle =
                                    "Pending Approvals".toUpperCase().obs;
                              });
                            Get.back();
                          },
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            border: sideTitle.value == "Log Out"
                                ? Border(
                                    left: BorderSide(
                                      color: MyResources.drawerIconColor,
                                      width: 5.0,
                                    ),
                                  )
                                : Border()),
                        child: ListTile(
                          dense: true,
                          tileColor: sideTitle.value == "Log Out"
                              ? MyResources.drawerIconColor.withOpacity(0.2)
                              : Colors.transparent,
                          // selected: true,
                          title: Text(
                            "Log out".toUpperCase(),
                            style: MyResources.myTextStyle,
                          ),
                          leading: Icon(
                            Icons.exit_to_app_outlined,
                            color: MyResources.drawerIconColor,
                          ),
                          onTap: () async {
                            setState(
                              () {
                                Get.back();
                              },
                            );
                            Get.dialog(Center(
                                child: DeletpopUp(
                              message: 'Are you sure you want to Sign out?',
                              pressFunc: () async {
                                await _auth.signOut();
                              },
                              isNotDel: true,
                            )));
                          },
                        ),
                      ),
                    ],
                  )),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 14.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 150,
                    width: Get.width,
                    child: Image.asset(
                      "assets/ap.jpg",
                      width: 200,
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                  InkWell(
                    onTap: () async {
                      await _launchInBrowser(_url);
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(left: 15.0),
                      child: Container(
                          height: 14,
                          child: Text(
                            'Powered by AikSol. 2021',
                            style: TextStyle(
                              fontSize: 12,
                            ),
                            // textAlign: TextAlign.start,
                          )),
                    ),
                  )
                ],
              ),
            ),
          ],
        )),
      ),
      body: Material(child: selectedWidget),
    );
  }
}
