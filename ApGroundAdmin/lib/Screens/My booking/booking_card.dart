import 'package:ap_ground_admin/constants/util.dart';
import 'package:ap_ground_admin/controllers/booking_controller.dart';
import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ionicons/ionicons.dart';

import '../../main.dart';
import '../delete_popup.dart';
import 'Models/booking_model.dart';

class BookingCard extends StatefulWidget {
  final Booking booking;
   BookingCard({Key? key, required this.booking}) : super(key: key);

  @override
  State<BookingCard> createState() => _BookingCardState();
}

class _BookingCardState extends State<BookingCard> {
  late bool isCancel;
  void checkSlots() {
    isCancel=widget.booking.isCancel;
  }

  @override
  void initState() {
    checkSlots();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 4,
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              widget.booking.groundName,
              style: TextStyle(fontWeight: FontWeight.w500, fontSize: 18),
            ),
            SizedBox(
              height: 5,
            ),
            SizedBox(
                width: MediaQuery.of(context).size.width * 0.7,
                child: Text(
                  widget.booking.groundAddress,
                  style: TextStyle(color: Colors.grey),
                )),
            SizedBox(
              height: 5,
            ),
            Divider(
              thickness: 1,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Wrap(
                  direction: Axis.horizontal,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    Icon(
                      Ionicons.calendar_outline,
                      color: Colors.black54,
                      size: 20,
                    ),
                    Text(
                      DateFormat(' dd-MMM-yyyy').format(widget.booking.date),
                      style: TextStyle(color: Colors.black54),
                    )
                  ],
                ),
                Wrap(
                  direction: Axis.horizontal,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    Icon(
                      Ionicons.time,
                      color: Colors.black54,
                      size: 20,
                    ),
                    Text(
                      DateFormat(' hh:mm a')
                          .format(widget.booking.slot.startTime),
                      style: TextStyle(color: Colors.black54),
                    )
                  ],
                ),
                Wrap(
                  direction: Axis.horizontal,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    Icon(
                      Ionicons.ellipse,
                      color: widget.booking.isCancel
                          ? Colors.red
                          : Colors.greenAccent,
                      size: 10,
                    ),
                    Text(
                      widget.booking.isCancel ? " Canceled" : " Confirmed",
                      style: TextStyle(color: Colors.black87),
                    )
                  ],
                ),
              ],
            ),
            Divider(
              thickness: 1,
            ),
            widget.booking.slot.Pslot2 != null && widget.booking.slot.Pslot1!.teamId==widget.booking.slot.Pslot2!.teamId ?
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Wrap(
                  children: [
                    const Text(
                      "Slots : 2",
                      style: TextStyle(color: Colors.black54),
                    ),
                  ],
                ),
                Wrap(
                  children: [
                    Text(
                      "Total: ",
                      style: TextStyle(color: Colors.black54),
                    ),
                    Text(formatnmbr((widget.booking.slot.Pslot1!.total+widget.booking.slot.Pslot2!.total)),
                        style: TextStyle(color: Colors.black54)),
                  ],
                ),
                Wrap(
                  children: [
                    Text(
                      "Remaining: ",
                      style: TextStyle(color: Colors.black54),
                    ),
                    Text(
                        formatnmbr(((widget.booking.slot.Pslot1!.total+widget.booking.slot.Pslot2!.total) -
                          (widget.booking.slot.Pslot1!.paid+widget.booking.slot.Pslot2!.paid))
                          ),
                      style: TextStyle(color: Colors.black54),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    TextButton(
                        onPressed: () {
                          showDialog(
                              context: context,
                              builder: (_) => AlertDialog(
                                content: DeletepopUp(message: "Are you sure You want to cancel this Booking?", pressFunc: () async {
                                  setState(() {
                                    widget.booking.isCancel= true;
                                  });
                                  try{
                                    await BookingController().updateBooking(widget.booking);
                                    displayToast("Booking Canceled");
                                  }catch(e){
                                    displayToast(e.toString());
                                    setState(() {
                                      widget.booking.isCancel=false;
                                    });
                                  }

                                  Navigator.pop(context);

                                },),
                              )
                          );
                        },
                        child: Container(
                            width: MediaQuery.of(context).size.width *
                                0.35,
                            decoration: BoxDecoration(
                                color: Color(0xfff1f2f8),
                                borderRadius:
                                BorderRadius.circular(8)),
                            padding: EdgeInsets.all(10),
                            child: Center(
                                child: Text("Cancel",style: TextStyle(color: Colors.black),)
                            ))),
                    TextButton(
                        onPressed: widget.booking.slot.Pslot1!.total +widget.booking.slot.Pslot2!.total == widget.booking.slot.Pslot1!.paid + widget.booking.slot.Pslot2!.paid ? null : () {
                          showDialog(
                              context: context,
                              builder: (_) => AlertDialog(
                                content: DeletepopUp(message: "Confirm receive remaining Payment" , pressFunc: () async {
                                  setState(() {
                                    widget.booking.slot.Pslot1!.paid= widget.booking.slot.Pslot1!.total;
                                    widget.booking.slot.Pslot2!.paid= widget.booking.slot.Pslot2!.total;
                                  });
                                  checkSlots();
                                  try{
                                    await BookingController().updateBooking(widget.booking);
                                    displayToast("Confirm Payment");
                                  }catch(e){
                                    displayToast(e.toString());
                                  }

                                  Navigator.pop(context);

                                }, isNotDel: true,),
                              )
                          );
                        },
                        child: Container(
                            width: MediaQuery.of(context).size.width *
                                0.35,
                            decoration: BoxDecoration(
                                color: widget.booking.slot.Pslot1!.total +widget.booking.slot.Pslot2!.total == widget.booking.slot.Pslot1!.paid + widget.booking.slot.Pslot2!.paid
                                    ? MyResources.buttonColor
                                    .withOpacity(0.4)
                                    : MyResources.buttonColor,
                                borderRadius:
                                BorderRadius.circular(8)),
                            padding: EdgeInsets.all(10),
                            child: Center(
                                child: Text("Receive",style: TextStyle(color: Colors.white),)))),
                  ],
                ),

              ],
            ) :
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Wrap(
                        direction: Axis.horizontal,
                        crossAxisAlignment: WrapCrossAlignment.center,
                        children: const [
                          Icon(
                            Ionicons.tennisball_outline,
                            color: Colors.blue,
                            size: 15,
                          ),
                          Text(
                            " Team 1",
                            style: TextStyle(
                                color: Colors.blue,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      Wrap(
                        children: [
                          const Text(
                            "Name: ",
                            style: TextStyle(color: Colors.black54),
                          ),
                          Text(
                            widget.booking.slot.Pslot1!.teamName,
                            style: TextStyle(color: Colors.black54),
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [

                          Wrap(
                            children: [
                              Text(
                                " Total: ",
                                style: TextStyle(color: Colors.black54),
                              ),
                              Text(formatnmbr(widget.booking.slot.Pslot1!.total),
                                  style: TextStyle(color: Colors.black54)),
                            ],
                          ),
                          Wrap(
                            children: [
                              Text(
                                " Remaining: ",
                                style: TextStyle(color: Colors.black54),
                              ),
                              Text(
                               formatnmbr(widget.booking.slot.Pslot1!.total -
                                    widget.booking.slot.Pslot1!.paid),
                                style: TextStyle(color: Colors.black54),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              TextButton(
                                  onPressed: () {
                                    showDialog(
                                        context: context,
                                        builder: (_) => AlertDialog(
                                          content: DeletepopUp(message: "Are you sure You want to cancel this Booking?", pressFunc: () async {
                                            setState(() {
                                              widget.booking.isCancel= true;
                                            });
                                            try{
                                              await BookingController().updateBooking(widget.booking);
                                              displayToast("Booking Canceled");
                                            }catch(e){
                                              displayToast(e.toString());
                                              setState(() {
                                                widget.booking.isCancel=false;
                                              });
                                            }

                                            Navigator.pop(context);

                                          }, isNotDel: true,),
                                        )
                                    );
                                  },
                                  child: Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.15,
                                      decoration: BoxDecoration(
                                          color: Color(0xfff1f2f8),
                                          borderRadius:
                                          BorderRadius.circular(8)),
                                      padding: EdgeInsets.all(5),
                                      child: Center(
                                          child: Icon(Ionicons.ban,color: Colors.red,)
                                      ))),
                              TextButton(
                                  onPressed: widget.booking.slot.Pslot1!.paid == widget.booking.slot.Pslot1!.total ? null : () {
                                    showDialog(
                                        context: context,
                                        builder: (_) => AlertDialog(
                                          content: DeletepopUp(message: "Confirm receive remaining Payment" , pressFunc: () async {
                                            setState(() {
                                              widget.booking.slot.Pslot1!.paid= widget.booking.slot.Pslot1!.total;
                                            });
                                            checkSlots();
                                            try{
                                              await BookingController().updateBooking(widget.booking);
                                              displayToast("Confirm Payment");
                                            }catch(e){
                                              displayToast(e.toString());
                                            }

                                            Navigator.pop(context);

                                          }, isNotDel: true,),
                                        )
                                    );
                                  },
                                  child: Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.15,
                                      decoration: BoxDecoration(
                                          color: widget.booking.slot.Pslot1!.paid == widget.booking.slot.Pslot1!.total
                                              ? MyResources.buttonColor
                                              .withOpacity(0.4)
                                              : MyResources.buttonColor,
                                          borderRadius:
                                          BorderRadius.circular(8)),
                                      padding: EdgeInsets.all(5),
                                      child: Center(
                                          child: Icon(Ionicons.cash,color: Colors.white,)))),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  width: 1,
                  height: 100,
                  color: Colors.black54,
                ),
                widget.booking.slot.Pslot2 != null
                    ? Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Wrap(
                        direction: Axis.horizontal,
                        crossAxisAlignment: WrapCrossAlignment.center,
                        children: const [
                          Icon(
                            Ionicons.tennisball_outline,
                            color: Colors.red,
                            size: 15,
                          ),
                          Text(
                            " Team 2",
                            style: TextStyle(
                                color: Colors.red,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      Wrap(
                        children: [
                          const Text(
                            "Name: ",
                            style: TextStyle(color: Colors.black54),
                          ),
                          Text(
                            widget.booking.slot.Pslot2!.teamName,
                            style: TextStyle(color: Colors.black54),
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [

                          Wrap(
                            children: [
                              Text(
                                "Total: ",
                                style: TextStyle(color: Colors.black54),
                              ),
                              Text(formatnmbr(widget.booking.slot.Pslot2!.total),
                                  style: TextStyle(color: Colors.black54)),
                            ],
                          ),
                          Wrap(
                            children: [
                              Text(
                                "Remaining: ",
                                style: TextStyle(color: Colors.black54),
                              ),
                              Text(
                                formatnmbr((widget.booking.slot.Pslot2!.total -
                                    widget.booking.slot.Pslot2!.paid))
                                    ,
                                style: TextStyle(color: Colors.black54),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              TextButton(
                                  onPressed: () {
                                    showDialog(
                                        context: context,
                                        builder: (_) => AlertDialog(
                                          content: DeletepopUp(message: "Are you sure You want to cancel this Booking?", pressFunc: () async {
                                            setState(() {
                                              widget.booking.isCancel= true;
                                            });
                                            try{
                                              await BookingController().updateBooking(widget.booking);
                                              displayToast("Booking Canceled");
                                            }catch(e){
                                              displayToast(e.toString());
                                              setState(() {
                                                widget.booking.isCancel=false;
                                              });
                                            }

                                            Navigator.pop(context);

                                          }, isNotDel: true,),
                                        )
                                    );
                                  },
                                  child: Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.15,
                                      decoration: BoxDecoration(
                                          color: Color(0xfff1f2f8),
                                          borderRadius:
                                          BorderRadius.circular(8)),
                                      padding: EdgeInsets.all(5),
                                      child: Center(
                                          child: Icon(Ionicons.ban,color: Colors.red,)
                                      ))),
                              TextButton(
                                  onPressed: widget.booking.slot.Pslot2!.paid == widget.booking.slot.Pslot2!.total ? null : () {
                                    showDialog(
                                        context: context,
                                        builder: (_) => AlertDialog(
                                          content: DeletepopUp(message: "Confirm receive remaining Payment" , pressFunc: () async {
                                            setState(() {
                                              widget.booking.slot.Pslot2!.paid = widget.booking.slot.Pslot2!.total;
                                            });
                                            checkSlots();
                                            try{
                                              await BookingController().updateBooking(widget.booking);
                                              displayToast("Confirm Payment");
                                            }catch(e){
                                              displayToast(e.toString());
                                            }

                                            Navigator.pop(context);

                                          }, isNotDel: true,),
                                        )
                                    );
                                  },
                                  child: Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.15,
                                      decoration: BoxDecoration(
                                          color: widget.booking.slot.Pslot2!.paid == widget.booking.slot.Pslot2!.total
                                              ? MyResources.buttonColor
                                              .withOpacity(0.4)
                                              : MyResources.buttonColor,
                                          borderRadius:
                                          BorderRadius.circular(8)),
                                      padding: EdgeInsets.all(5),
                                      child: Center (
                                          child: Icon(Ionicons.cash,color: Colors.white,)))),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ) : Expanded(
                  child: Center(
                    child: const Text(
                      "Slot not booked yet",
                      style: TextStyle(color: Colors.red),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
