
class Todo {
  final String? id;
  final String des;
  final String name;
  final bool status;


  Todo( {required this.des,required this.status,required this.name,this.id});



  factory Todo.fromJson(dynamic json, key) {
    return Todo(
      id: key.toString(),
      name: json['name'],
      des : json['description'],
      status : json['status'],

    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['description'] = des;
    data['name'] = name;
    data['status'] = status;
    return data;
  }
}