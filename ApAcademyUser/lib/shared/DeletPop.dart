import 'package:ap_academy_new/Constants/util.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DeletpopUp extends StatelessWidget {
  final String message;
  final VoidCallback pressFunc;
  final bool isNotDel;

  const DeletpopUp(
      {Key? key, required this.pressFunc, required this.message, this.isNotDel = false})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(50),
              topRight: Radius.circular(50),
            )),
        width: Get.width * 0.8,
        height: 180,
        constraints: BoxConstraints(maxHeight: 200),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Container(
                  child: Icon(
                isNotDel ? Icons.warning : Icons.delete_forever,
                color: Colors.red,
                size: 50,
              )),
              Container(
                //width: ,
                child: Text(
                  message,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18),
                ),
                height: 50,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all<Color>(Colors.green),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                      side: BorderSide(
                                          color: Colors.transparent)))),
                      onPressed: () {
                        Get.back();
                      },
                      child: Text('No')),
                  SizedBox(
                    width: 10,
                  ),
                  ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            myResources.buttonColor),
                        shape: MaterialStateProperty
                            .all<RoundedRectangleBorder>(RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                                side: BorderSide(color: Colors.transparent)))),
                    onPressed: pressFunc,
                    child: Text(
                      'Yes',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
