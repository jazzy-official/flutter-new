import 'package:daily_log/Controllers/projectscontroller.dart';
import 'package:daily_log/Models/project_model.dart';
import 'package:daily_log/View/Projects/add_project.dart';
import 'package:daily_log/View/Projects/project_detail.dart';
import 'package:daily_log/constants/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:intl/intl.dart';

import '../delete_popup.dart';

class ProjectsScreen extends StatelessWidget {
  const ProjectsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(onPressed: (){Get.to(AddProject());},backgroundColor: MyResources.themecolor,child: Icon(Icons.add),),
      body:GetBuilder<ProjectController>(
          autoRemove: false,
          init: ProjectController(),
          builder: (cont) => RefreshIndicator(
            onRefresh: () { return cont.readBall(); },
            child: ListView.builder(
              physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
              itemCount: cont.projects.length,
              itemBuilder: (context, index) {
                ProjectModel project = cont.projects[index];
                int total = 0;
                int completed = 0;
                project.modules.forEach((element) {element.submodules?.forEach((element) { total++;if(element.status==true){completed++;} });});
                return Slidable(
                  // Specify a key if the Slidable is dismissible.
                  key: const ValueKey(0),

                  // The start action pane is the one at the left or the top side.

                  // The end action pane is the one at the right or the bottom side.
                  endActionPane: ActionPane(
                    // dismissible: DismissiblePane(onDismissed: () {}),
                    motion: const ScrollMotion(),
                    children: [
                      SlidableAction(
                        onPressed: (BuildContext context) async {
                          showDialog(
                              context: context,
                              builder: (_) => AlertDialog(
                                content: DeletepopUp(message: "Are you sure?",pressFunc: () async {
                                  await cont.deleteProject(project.id!);
                                  Get.back();
                                  Get.snackbar("Successful", "Task Deleted");
                                  await cont.readBall();
                                  cont.update();

                                },),
                              ));

                        },
                        backgroundColor: const Color(0xFFFE4A49),
                        foregroundColor: Colors.white,
                        icon: Icons.delete,
                        label: 'Delete',
                      ),
                      SlidableAction(
                        onPressed: (BuildContext context) async {
                          Get.to(()=> AddProject(project: project,));
                        },
                        backgroundColor: const Color(0xFF0392CF),
                        foregroundColor: Colors.white,
                        icon: Icons.edit,
                        label: 'Edit',
                      ),
                    ],
                  ),

                  // The child of the Slidable is what the user sees when the
                  // component is not dragged.
                  child:  InkWell(
                    onTap: (){
                      cont.getProjectById(project.id!);
                      Get.to(ProjectDetail(project: project));
                    },
                    child: Card(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(project.name,style: const TextStyle(fontSize: 17,fontWeight: FontWeight.w600),),
                              ],
                            ),
                            SizedBox(height: 10,),
                            Row(
                              children: [
                                SizedBox(width:100,child: const Text("About : ",style: TextStyle(fontWeight: FontWeight.w600),)),
                                Expanded(child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(project.des),
                                  ],
                                )),
                              ],
                            ),
                            const SizedBox(height: 5,),
                            if(project.deadline!=null)
                            Row(
                              children: [
                                SizedBox(width:100,child: const Text("Deadline : ",style: TextStyle(fontWeight: FontWeight.w600),)),
                                Text(DateFormat("MMMM dd, yyyy.").format(DateTime.parse(project.deadline!))),
                              ],
                            ),
                            SizedBox(height: 5,),
                            Row(
                              children: [
                                const SizedBox(width:100,child: Text("Manage By : ",style: TextStyle(fontWeight: FontWeight.w600),)),
                                Text(project.assignto!),
                              ],
                            ),
                            SizedBox(height: 5,),
                            Row(
                              children: [
                                const SizedBox(width:100,child: Text("Status: ",style: TextStyle(fontWeight: FontWeight.w600),)),
                                Text(((total==0?1 : (completed/total))*100).round().toString()+" % completed",style: TextStyle(color: (completed/total)*100<40?Colors.red:(completed/total)*100<65?Colors.orange:Colors.green ),),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          )),
    );
  }
}
