import 'package:ap_academy_new/ModelView/controllers/profileController.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:mongo_dart/mongo_dart.dart';

import 'db_controller.dart';

class MatchController extends GetxController {
  Rx<List<Map<String, dynamic>>> matchList = Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get match => matchList.value;
  final dbHelp = Get.put(DatabaseController());
  @override
  Future<void> onInit() async {
    //  matchList.bindStream(DatabaseController().matchStream());
    super.onInit();
  }


 // filtermatches(List<Map<String, dynamic>> matches) async {
 //    filmatches = [];
 //
 //    matches.forEach((element) async {
 //
 //      if(!element['otherteam1']){
 //        late List< dynamic>? students;
 //        students = await getUserbyTeamName(element["team1name"]);
 //        if(students!=null) {
 //          students.forEach((e) {
 //            if (
 //            e['firebaseId'] ==
 //                FirebaseAuth.instance
 //                    .currentUser!.uid
 //            ) {
 //              filmatches.add(element);
 //            }
 //          });
 //        }
 //      }
 //      if(!element['otherteam2']){
 //        late List< dynamic>? students;
 //        students = await getUserbyTeamName(element["team2name"]);
 //        if(students!=null){
 //          students.forEach((e) {
 //            if(
 //            e['firebaseId'] ==
 //                FirebaseAuth.instance
 //                    .currentUser!.uid
 //            ){
 //              filmatches.add(element);
 //            }
 //          });
 //        }
 //
 //      }
 //    });
 //    refresh();
 //  }


  Rx<Map<String, dynamic>> names = Rx<Map<String, dynamic>>({});
  Map<String, dynamic> get name => names.value;
  RxList<dynamic> teamsList = <dynamic>[].obs;
  getTeamName(Map<String, dynamic> matchModel) async {
    try {
      teamsList.isNotEmpty ? teamsList = [].obs : null;
      names.value = {
        'team1': {'teamName': 'Team 1'},
        'team2': {'teamName': 'Team 2'}
      };
      names.value = (await dbHelp.getTeamName(matchModel))!;
      List<dynamic> temp = names.value['team1']['teamMembers'];
      temp.forEach((element) {
        print(element.toString());
        teamsList.add(element);
      });
      print(teamsList);
    } catch (e) {
      print(e);
    }
  }

  refreshDate() {
    matchList.bindStream(DatabaseController().matchStream());
    refresh();

  }

  final dbHelper = Get.put(DatabaseController());

  // Future<String> getTeamName1(Map<String, dynamic> matchModel1) async {
  //   Map<String, dynamic>? t1 =
  //       await dbHelper.getTeamData(ObjectId.parse(matchModel1['team1']));
  //
  //   return t1!['teamName'];
  // }

  // Future<String> getTeamName2(Map<String, dynamic> mModel) async {
  //   Map<String, dynamic>? t2 =
  //       await dbHelper.getTeamData(ObjectId.parse(mModel['team2']));
  //
  //   return t2!['teamName'];
  // }

  // Future<List<Map<String, dynamic>>?> getUserbyGroup(String gId) async {
  //   return await dbHelper.getUsersbygroup(gId);
  // }
  Future<List< dynamic>?> getUserbyTeamName(String teamname1,String teamname2) async {
    print("fffffffffffffffffffffffffffffff");
    List? x1= await dbHelper.getUsersbyteam(teamname1);
    List? x2= await dbHelper.getUsersbyteam(teamname2);
    late List? x;
    if(x1 == null){
      x = x2;
    }else if (x2 == null){
      x = x1;
    }else{
      x = x1+x2;
    }
    return x;
  }

  Future<Map<String, dynamic>?> matchTeamId(String tId) async {
    return await dbHelper.getTeamData(ObjectId.fromHexString(tId));
  }

  Future<bool> saveMatchAttendance(ObjectId mId) async {
    return await dbHelper.saveMatchAttendance(mId);
  }

  Future<bool> delMatchAttendance(ObjectId mId) async {
    return await dbHelper.delMatchAttendance(mId);
  }

  bool checkIfAttend(List atIds) {
    final profileController = Get.put(ProfileController());
    bool alread = false;
    try {
      atIds.forEach((element) async {
        if (element['firebaseId'].toString() ==
             profileController.temp['firebaseId']) {
          alread = true;
        }
      });
    } catch (ex) {
      alread = false;
    }
    return alread;
  }

  returnUserList() {}
  @override
  void onClose() {
    //db.close();\
    teamsList.clear();
    super.onClose();
  }
}
