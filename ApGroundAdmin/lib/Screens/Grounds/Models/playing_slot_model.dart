class PlayingSlot {
  String teamName;
  double paid;
  double total;
  String teamRole;
  String teamId;
  bool meal;

  PlayingSlot({required this.paid,required this.teamId,required this.teamName,required this.teamRole,required this.meal,required this.total});

// NOTE: implementing functionality here in the next step!

  factory PlayingSlot.fromJson(dynamic json) {
    return PlayingSlot(
      teamName : json['teamName'],
      paid: json['paid'].toDouble(),
      total: json['total'].toDouble(),
      teamRole: json['teamRole'],
      teamId: json['teamId'],
      meal: json['meal'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['teamName'] = teamName;
    data['paid'] = paid;
    data['total'] = total;
    data['teamRole'] = teamRole;
    data['teamId'] = teamId;
    data['meal'] = meal;
    return data;
  }
}