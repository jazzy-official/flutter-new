
import 'dart:convert';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Balance%20Sheet/Modals/balance_sheet_modal.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/RecievePaidChecklist/receive_paid_checklist.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/Recovery/recovery.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/account%20model/accounts.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Payables/Model/payable_model.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Receivable/Model/receivable_model.dart';
import 'package:rafi_admin/Controllers/authcontroller.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BalanceSheetController extends GetxController {
  final auth = Get.put(AuthController());
  BalanceSheet? balanceSheet;
  String startDate = DateFormat('yyyy-MM-01').format(DateTime.now());
  String endDate = DateFormat('yyyy-MM-dd').format(DateTime.now());
  double get assets => (balanceSheet?.currentAssetSum ?? 0.0) + (balanceSheet?.fixedAssetSum ?? 0.0);
  double get liability => (balanceSheet?.currentLiabilitiesSum ?? 0.0 )+
      (balanceSheet?.longTermLiabilitiesSum ?? 0.0);
  double get equity => (balanceSheet?.capitalListSum ?? 0.0) +
      (balanceSheet?.otherEquitySum??0.0) +
      (balanceSheet?.retainedEarning ?? 0.0) +
      (balanceSheet?.drawingListSum??0.0);
  String? token;
  int? number;
  @override
  Future<void> onInit() async {
    await refreshAll();
    super.onInit();
  }

  Future refreshAll() async {
    token = await auth.getToken();
    number = await auth.getProjectId();
    await getList();
  }
  Future getList() async {
    http.Response response = await http.get(
      Uri.parse(
          "https://raccounting.azurewebsites.net/api/Reports/BalanceSheet?Projectid=$number&From=" +
              startDate +
              "&To=" +
              endDate),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );
    if (response.statusCode == 200) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.

        balanceSheet = BalanceSheet.fromJson(jsonDecode(jsonDecode(response.body)));
      update();

    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to get Balance sheet data');
    }
  }


}
