
import 'package:rafi_admin/Components/Screens/Inventory/Models/Unit%20Model/unit_model.dart';

class UnitListModel {
  int pageNumber;
  int pageSize;
  String firstPage;
  String lastPage;
  int totalPages;
  int totalRecords;
  String? nextPage;
  String? previousPage;

  List<UnitModel> units;


  UnitListModel({
    required this.units,
    required this.firstPage,
    required this.lastPage,
    this.nextPage,
    required this.pageNumber,
    required this.pageSize,
    this.previousPage,
    required this.totalPages,
    required this.totalRecords,

  });

  factory UnitListModel.fromJson(Map<String, dynamic> json) {
    return UnitListModel(
      units: List<UnitModel>.from(json["data"]
          .map(
            (dynamic item) => UnitModel.fromJson(item),
      ).toList()),
      pageNumber: json['pageNumber'],
      pageSize: json['pageSize'],
      firstPage: json['firstPage'].toString(),
      lastPage: json['lastPage'].toString(),
      totalPages: json['totalPages'],
      totalRecords: json['totalRecords'],
      nextPage: json['nextPage'].toString(),
      previousPage: json['previousPage'].toString(),

    );
  }
}
