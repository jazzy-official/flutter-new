import 'dart:convert';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Expenses/Models/expense_model.dart';
import 'package:rafi_admin/Controllers/authcontroller.dart';

class ExpenseController extends GetxController {
  final auth = Get.put(AuthController());
  late List<Expense> foundResult = [];
  List<Expense>? expenses;

  String? token;
  int? number;
  @override
  Future<void> onInit() async {
    await refreshAll();
    super.onInit();
  }

  Future refreshAll() async {
    token = await auth.getToken();
    number = await auth.getProjectId();
    await setExpense();

  }
  Future setExpense() async {
    http.Response response = await http.get(
      Uri.parse('https://raccounting.azurewebsites.net/api/Reports/ExpenseAccounts?ProjectId=$number'),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        }
    );
    if (response.statusCode == 200) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      Map<String, dynamic> expenseData = await jsonDecode(jsonDecode(response.body));
      List<dynamic> data = expenseData["ExpenseAccounts"];
      List<Expense> expensesdata = data
          .map(
            (dynamic item) => Expense.fromJson(item),
      ).toList();
      expenses = expensesdata.where((payabledata) => payabledata.currentbalance != 0||payabledata.currentbalance != 0).toList();
      foundResult = expenses!;
      print(expenses);
      update();
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to get expense data');
    }
  }
  void runFilter(String enteredKeyword) {
    List<Expense> results = [];
    if (enteredKeyword.isEmpty) {
      // if the search field is empty or only contains white-space, we'll display all users
      results = expenses!;
    } else {
      results = expenses!
          .where((user) => user.accounttitle
          .toUpperCase()
          .contains(enteredKeyword.toUpperCase()))
          .toList();
      // we use the toLowerCase() method to make it case-insensitive
    }

    // Refresh the UI
      foundResult = results;
      update();

  }


}
