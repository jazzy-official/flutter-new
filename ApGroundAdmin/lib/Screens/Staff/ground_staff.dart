import 'package:ap_ground_admin/Screens/Staff/add_staff.dart';
import 'package:ap_ground_admin/Screens/Staff/staff_card.dart';
import 'package:ap_ground_admin/controllers/designation_controller.dart';
import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:ap_ground_admin/controllers/staff_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:ionicons/ionicons.dart';
import 'package:url_launcher/url_launcher.dart';
import '../delete_popup.dart';
import 'Models/staff_model.dart';

class GroundStaffScreen extends StatefulWidget {
  GroundStaffScreen({Key? key}) : super(key: key);

  @override
  State<GroundStaffScreen> createState() => _GroundStaffScreenState();
}

class _GroundStaffScreenState extends State<GroundStaffScreen> {
  late List<Staff> staff;
  List<Staff> _foundResult = [];
  List<Staff> Result1 = [];
  List<String> designations = ["All"];
  String search= "";
  bool isLoading = true;
  void refresh() async {
    setState(() {
      isLoading = true;
    });
    setState(() {
      designations = ["All"];
    });
    staff = await StaffController().readStaff();
    List<Map<String,String>> desg =  await DesignationController().readDesignation();
    desg.forEach((element) {designations.add(element.values.toString());});
    _runDesgFilter("");

    setState(() {
      isLoading = false;
    });
  }
  void _runFilter(String enteredKeyword) {
    List<Staff> results = [];
    if (enteredKeyword.isEmpty) {
      // if the search field is empty or only contains white-space, we'll display all users
      results = Result1;
    } else {
      results = Result1
          .where((user) => user.name
          .toUpperCase()
          .contains(enteredKeyword.toUpperCase()))
          .toList();
      // we use the toLowerCase() method to make it case-insensitive
    }

    // Refresh the UI
    setState(() {
      _foundResult = results;
    });
  }
  void _runDesgFilter(String enteredKeyword) {
    List<Staff> results = [];
    if (enteredKeyword=="All"||enteredKeyword=="") {
      // if the search field is empty or only contains white-space, we'll display all users
      results = staff;
    } else {
      results = staff
          .where((user) => user.designation == enteredKeyword.substring(1,enteredKeyword.length-1))
          .toList();
      // we use the toLowerCase() method to make it case-insensitive
    }

    // Refresh the UI
    setState(() {
      Result1 = results;
    });
    _runFilter(search);
  }

  String dropdownValue = "All";
  @override
  void initState() {
    refresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 5.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 12),
                  height: 45,
                  child: TextFormField(
                    keyboardType: TextInputType.name,
                    onChanged: (val) {
                      setState(() {
                        search=val;
                      });
                      _runFilter(search);
                    },
                    onSaved: (String? val) {}, // ame Controller
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(left: 10),
                      //Add th Hint text here.
                      hintText: "Search",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(right: 10,top: 1),
                child: DropdownButton<String>(
                  value: dropdownValue,
                  icon: const Icon(Ionicons.chevron_down),
                  iconSize: 18,
                  elevation: 10,
                  style: const TextStyle(color: Colors.deepPurple),
                  // underline: Container(
                  //   height: 2,
                  //   color: Colors.deepPurpleAccent,
                  // ),
                  onChanged: (String? newValue) {
                    _runDesgFilter(newValue!);
                    setState(() {
                      dropdownValue = newValue;
                    });
                  },
                  items: designations
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 12.0),
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => AddStaffForm()),
                    ).then((value) => refresh());
                  },
                  child: Text("Add"),
                  style: ElevatedButton.styleFrom(primary: Color(0xff1758B4)),
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: isLoading
              ? Center(child: CircularProgressIndicator())
              : ListView.builder(
              physics: BouncingScrollPhysics(
                  parent: AlwaysScrollableScrollPhysics()),
              padding: const EdgeInsets.all(8),
              itemCount: _foundResult.length,
              itemBuilder: (BuildContext context, int index) {
                return InkWell(
                  onTap: () => launch("tel://" + _foundResult[index].number),
                  child: Slidable(
                    actionPane: SlidableDrawerActionPane(),
                    actionExtentRatio: 0.15,
                    child: StaffCard(
                      staff: _foundResult[index],
                      refresh: refresh,
                    ),
                    secondaryActions: <Widget>[
                      Container(
                        padding: EdgeInsets.symmetric(vertical: 5),
                        child: IconSlideAction(
                          caption: 'Edit',
                          color: Colors.blue,
                          icon: Icons.edit,
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AddStaffForm(
                                    staff: _foundResult[index],
                                  )),
                            ).then((value) => refresh());
                          },
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(vertical: 5),
                        child: IconSlideAction(
                          caption: 'Delete',
                          color: Colors.red,
                          icon: Icons.delete,
                          onTap: () async {
                            try {
                              showDialog(
                                  context: context,
                                  builder: (_) => AlertDialog(
                                    content: DeletepopUp(message: "Are you sure You wanna delete "+_foundResult[index].name +"?", pressFunc: () async {
                                      await StaffController()
                                          .deleteStaff(_foundResult[index].id!);
                                      refresh();
                                      Navigator.pop(context);
                                      displayToast("Staff deleted successfully");

                                    },),
                                  )
                              );


                            } catch (e) {
                              displayToast(e.toString());
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                );
              }),
        ),
      ],
    );
  }
}
