import 'package:ap_academy_new/ModelView/controllers/db_controller.dart';
import 'package:ap_academy_new/ModelView/controllers/matchController.dart';
import 'package:ap_academy_new/ModelView/controllers/profileController.dart';
import 'package:ap_academy_new/screens/Events/teams_details.dart';
import 'package:ap_academy_new/shared/loading_widget.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_new/Constants/util.dart';
import 'package:get/get.dart';
import 'package:get/route_manager.dart';
import 'package:intl/intl.dart';

class MatchScreen extends StatefulWidget {
  const MatchScreen({Key? key}) : super(key: key);

  @override
  _MatchScreenState createState() => _MatchScreenState();
}

class _MatchScreenState extends State<MatchScreen> {
  @override
  void initState() {
    refrsh();
    // TODO: implement initState
    super.initState();
  }
  refrsh() {
    final matchC = Get.put(MatchController());
    matchC.refreshDate();
  }

  final dbHelp = Get.put(DatabaseController());
  bool isattandance = false;
  @override
  Widget build(BuildContext context) {
    return Material(
      //Create an event add button
      child: GetX<MatchController>(
        init: Get.put<MatchController>(MatchController()),
        builder: (MatchController matchController) {
          //   matchController.refreshDate();
          if (matchController != null && matchController.match != null) {
            return Stack(
              children: [
                Center(
                  child: ColorFiltered(
                      colorFilter: ColorFilter.mode(
                          Colors.white.withOpacity(0.3), BlendMode.dstATop),
                      child: Image.asset(
                        'assets/notraining.png',
                        colorBlendMode: BlendMode.hue,
                        width: 200,
                        height: 200,
                      )),
                ),
                ListView.builder(
                    itemCount: matchController.match.length,
                    itemBuilder: (BuildContext context, int index) {
                      return getMatchCard(
                        matchController.match[index],
                      );
                    }),
              ],
            );
          } else {
            return const LoadingWidget();
          }
        },
      ),
    );
  }

  final mController = Get.put(MatchController());
  final pController = Get.put(ProfileController());
  Widget getMatchCard(Map<String, dynamic> matchModel,) {
    bool isAttended = mController.checkIfAttend(matchModel['attendance']);

    // mController.getTeamName(matchModel);
    return FutureBuilder<List< dynamic>?>(
        future: mController.getUserbyTeamName(matchModel['team1name'],matchModel['team2name']),
        builder: (context, AsyncSnapshot<
                List<dynamic>?>
            snap) {
          // print("snap data"+snap.data.toString());
          if (snap.hasData) {
            bool isattandance = false;
            snap.data!.forEach((element) {
              if (element['firebaseId'] ==
                  pController.temp['firebaseId']) {
                isattandance = true;
              }
            });
            return isattandance ? Padding(
              padding: const EdgeInsets.all(10),
              child: SizedBox(
                //constraints: BoxConstraints(minHeight: 120),
                //height: 250,
                width: Get.width*0.9,
                //create the cards for all details of event happening
                child: InkWell(
                  onTap: () async {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => TeamsDetails(
                        team1Model: // matchModel['otherteam1']
                        // ?
                        {
                          'teamName': matchModel['team1name'],
                          'teamType': 'Custom',
                          'coach': 'General',
                          'teamMembers':matchModel['team1']??[],
                        }
                        //: await mController.getTeamsDetail(matchModel['team1']),
                        ,
                        team2Model: // matchModel['otherteam2']
                        //  ?
                        {
                          'teamName': matchModel['team2name'],
                          'teamType': 'Custom',
                          'coach': 'General',
                          'teamMembers': matchModel['team2']??[],
                        }
                        //   : await mController.getTeamsDetail(matchModel['team2']),
                        ,
                        matchModel: matchModel,
                      ),),
                    );
                    // Get.dialog(
                    //   TeamsDetails(
                    //     team1Model:
                    //         await mController.getTeamsDetail(matchModel['team1']),
                    //     team2Model:
                    //         await mController.getTeamsDetail(matchModel['team2']),
                    //     matchModel: matchModel,
                    //   ),
                    // );
                  },
                  child: Stack(
                    children: [
                      Card(
                        elevation: 4,
                        color: myResources.backgroundColor,
                        child: Padding(
                          padding: const EdgeInsets.only(top:18),
                          child: Column(
                            children: [
                              Center(
                                child: Text(matchModel['match'],
                                    overflow: TextOverflow.fade,
                                    style: const TextStyle(fontSize: 18,
                                      fontWeight: FontWeight.bold,

                                    )
                                ),
                              ),
                              Row(
                                //mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: SizedBox(
                                      width: Get.width * 0.8,
                                      child: Column(children: [
                                        const SizedBox(
                                          height: 6,
                                        ),
                                        Column(
                                          children: [
                                            // matchModel['otherteam1']
                                            // ?
                                            Text(matchModel['team1name'],
                                                overflow: TextOverflow.ellipsis,
                                                style: const TextStyle(fontSize: 16,
                                                  fontWeight: FontWeight.w300,
                                                  color: Color(0xff1758B4),))
                                            // : FutureBuilder(
                                            //     future: mController
                                            //         .getTeamName1(matchModel),
                                            //     builder: (BuildContext context,
                                            //         AsyncSnapshot<String> sp) {
                                            //       if (sp.data != null)
                                            //         return Container(
                                            //           child: Text(
                                            //             sp.data,
                                            //             overflow: TextOverflow.ellipsis,
                                            //             style: TextStyle(
                                            //               color: Color(0xff000074),
                                            //             ),
                                            //           ),
                                            //         );
                                            //       else
                                            //         return Container(
                                            //           width: 80,
                                            //           child: Text(
                                            //             '',
                                            //             style: TextStyle(
                                            //               color: Color(0xff000074),
                                            //             ),
                                            //           ),
                                            //         );
                                            //     }),
                                            ,
                                            const SizedBox(
                                              height: 5,
                                            ),
                                            Text(
                                              "vs",
                                              style: myResources.vsStyle,
                                            ),
                                            const SizedBox(
                                              height: 5,
                                            ),
                                            //  matchModel['otherteam2']
                                            //  ?
                                            Container(
                                              child: Text(
                                                matchModel['team2name'],
                                                overflow: TextOverflow.ellipsis,
                                                style: const TextStyle(fontSize: 16,
                                                  fontWeight: FontWeight.w300,
                                                  color: Color(0xff1758B4),),
                                              ),
                                            ),
                                            // : FutureBuilder(
                                            //     future: mController
                                            //         .getTeamName2(matchModel),
                                            //     builder: (BuildContext context,
                                            //         AsyncSnapshot<String> sp) {
                                            //       if (sp.data != null)
                                            //         return Container(
                                            //           child: Text(
                                            //             sp.data,
                                            //             overflow: TextOverflow.ellipsis,
                                            //             style: TextStyle(
                                            //               color: Color(0xff000074),
                                            //             ),
                                            //           ),
                                            //         );
                                            //       else
                                            //         return Container(
                                            //           width: 80,
                                            //           child: Text(
                                            //             '',
                                            //             style: TextStyle(
                                            //               color: Color(0xff000074),
                                            //             ),
                                            //           ),
                                            //         );
                                            //     }),
                                          ],
                                        ),
                                        Row(
                                          children: [
                                            const Text("Venue:",
                                                style: TextStyle(fontSize: 14,
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold,)),
                                            const SizedBox(
                                              width: 10,
                                            ),
                                            SizedBox(
                                              width: 120,
                                              child: Text(
                                                matchModel['venue'],
                                                style: const TextStyle(fontSize: 14,
                                                  color: Colors.black,
                                                ),
                                                overflow: TextOverflow.ellipsis,
                                              ),

                                            ),
                                          ],
                                        ),
                                        Row(
                                          children: [
                                            const Text("Date:",
                                                style: TextStyle(fontSize: 14,
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold,)),
                                            const SizedBox(
                                              width: 16,
                                            ),
                                            Text(
                                              DateFormat('yyyy-MM-dd').format(matchModel['matchTime'].toLocal()),
                                              style: const TextStyle(
                                                color: Colors.orange,),
                                            ),
                                            const SizedBox(
                                              width: 16,
                                            ),
                                            const Text("Time:",
                                                style: TextStyle(fontSize: 14,
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold,)),
                                            const SizedBox(
                                              width: 16,
                                            ),
                                            Text(
                                              DateFormat('hh:mm a').format(matchModel['matchTime'].toLocal()),
                                              style: const TextStyle(
                                                color: Colors.orange,),
                                            ),

                                          ],
                                        ),
                                      ]),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  if(!matchModel['otherteam1'] && !matchModel['isApproved'])
                                    Container(
                                      child: availabiltyWidget(
                                        isAttended: isAttended,
                                        onyes: () async {
                                          Get.back();
                                          //  Get.dialog(
                                          //  Center(
                                          // child:
                                          //     LoadingWidget()),
                                          //  barrierDismissible:
                                          //      false);
                                          await mController
                                              .saveMatchAttendance(
                                              matchModel[
                                              '_id'])
                                              .then((value) {
                                            Get.back();
                                            Get.defaultDialog(
                                              buttonColor:
                                              myResources
                                                  .buttonColor,
                                              title: "Saved",
                                              confirmTextColor:
                                              Colors.white,
                                              content: const Text(
                                                "Your attendance is saved",
                                              ),
                                              onConfirm: () {
                                                mController
                                                    .refreshDate();
                                                Get.back();
                                              },
                                            );
                                            mController
                                                .refreshDate();
                                          });
                                        },
                                        onNo: () async {
                                          Get.back();
                                          Get.dialog(
                                              const Center(
                                                  child:
                                                  LoadingWidget()),
                                              barrierDismissible:
                                              false);
                                          await mController
                                              .delMatchAttendance(
                                              matchModel[
                                              '_id'])
                                              .then((value) {
                                            if (value) {
                                              Get.back();
                                              Get.snackbar(
                                                  "Available",
                                                  'You have benn marked as absent...',
                                                  snackPosition:
                                                  SnackPosition
                                                      .BOTTOM);
                                              isAttended = false;
                                              mController
                                                  .refreshDate();
                                              setState(() {});
                                            }
                                          });
                                        },
                                      )
                                    ),
                                  if(!matchModel['otherteam2'] && !matchModel['isApproved'])
                                    Container(
                                      child: availabiltyWidget(
                                        isAttended: isAttended,
                                        onyes: () async {
                                          Get.back();
                                          Get.dialog(
                                              const Center(
                                                  child:
                                                  LoadingWidget()),
                                              barrierDismissible:
                                              false);
                                          await mController
                                              .saveMatchAttendance(
                                              matchModel[
                                              '_id'])
                                              .then((value) {
                                            Get.back();
                                            Get.defaultDialog(
                                              buttonColor:
                                              myResources
                                                  .buttonColor,
                                              title: "Saved",
                                              confirmTextColor:
                                              Colors.white,
                                              content: const Text(
                                                  "Your attendance has been saved"),
                                              onConfirm: () {
                                                mController
                                                    .refreshDate();
                                                Get.back();
                                              },
                                            );
                                            mController
                                                .refreshDate();
                                          });
                                        },
                                        onNo: () async {
                                          Get.back();
                                          Get.dialog(
                                              const Center(
                                                  child:
                                                  LoadingWidget()),
                                              barrierDismissible:
                                              false);
                                          await mController
                                              .delMatchAttendance(
                                              matchModel[
                                              '_id'])
                                              .then((value) {
                                            if (value) {
                                              Get.back();
                                              Get.snackbar(
                                                  "Available",
                                                  'You are marked as absent.',
                                                  snackPosition:
                                                  SnackPosition
                                                      .BOTTOM);
                                              isAttended = false;
                                              mController
                                                  .refreshDate();
                                              setState(() {});
                                            }
                                          });
                                        },
                                      )
                                    )
                                  else Container()],
                              ),
                            ],
                          ),
                        ),
                      ),
                      !matchModel['isApproved']
                          ? Positioned(
                        top: 6,
                        right: 4,

                        child: Container(
                          width: 55,
                          height: 18,
                          color: Colors.red[500],

                          child: const Center(
                              child: Text(
                                'Awaiting',
                                style: TextStyle(color: Colors.white, fontSize: 12),
                              )
                          ),
                        ),
                      )
                          : Container(),
                    ],
                  ),
                ),
              ),
            ):Container();
          }
          return Container(width: 50,);
        });
  }

  Widget availabiltyWidget({required bool isAttended, required VoidCallback onyes, required VoidCallback onNo}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text("Availability:", style: myResources.appTextStyle),
        const SizedBox(
          width: 10,
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              SizedBox(
                width: 60,
                height: 20,
                child: RaisedButton(
                  textColor: Colors.white,
                  color: isAttended ? Colors.green : Colors.grey,
                  child: const Text(
                    "Yes",
                  ),
                  onPressed: onyes,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
              const SizedBox(
                width: 5,
              ),
              SizedBox(
                width: 60,
                height: 20,
                child: RaisedButton(
                  textColor: Colors.white,
                  color: isAttended ? Colors.grey : Colors.red,
                  child: const Text(
                    "No",
                  ),
                  onPressed: onNo,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
