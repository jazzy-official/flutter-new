import 'package:ap_academy_new/ModelView/controllers/otherEventModel.dart';
import 'package:ap_academy_new/screens/Events/other_event_details.dart';
import 'package:ap_academy_new/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_new/Constants/util.dart';
import 'package:get/get.dart';
import 'package:get/route_manager.dart';
import 'package:intl/intl.dart';

class OtherScreen extends StatefulWidget {
  @override
  _OtherScreenState createState() => _OtherScreenState();
}

class _OtherScreenState extends State<OtherScreen> {
  refrehData() {
    final oControl = Get.put(OtherEventController());
    oControl.refreshData();
  }

  @override
  void initState() {
    refrehData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: //Create an event add button
          GetX<OtherEventController>(
        init: Get.put<OtherEventController>(OtherEventController()),
        builder: (OtherEventController otherEventController) {
          if (otherEventController != null &&
              otherEventController.event != null) {
            return Stack(
              children: [
                Center(
                  child: ColorFiltered(
                      colorFilter: ColorFilter.mode(
                          Colors.white.withOpacity(0.3), BlendMode.dstATop),
                      child: Image.asset(
                        'assets/otherevent.png',
                        width: 200,
                        height: 200,
                      )),
                ),
                ListView.builder(
                  itemCount: otherEventController.event.length,
                  itemBuilder: (BuildContext context, int index) =>
                      getEventCard(otherEventController.event[index]),
                ),
              ],
            );
            //    } else {

            //  }
          } else {
            return Container(
              child: LoadingWidget(),
            );
          }
        },
      ),
    );
  }

  Widget getEventCard(Map<String, dynamic> otherEventModel) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Container(
          //constraints: BoxConstraints(minHeight: 200),
          width: Get.width,
          //create the cards for all details of event happening
          child: InkWell(
            onTap: () {
              Get.dialog(
                OtherEventDetails(
                  otherEvent: otherEventModel,
                ),
              );
            },
            child: Card(
              elevation: 4,
              color: myResources.backgroundColor,
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  children: [
                    Center(
                      child: Container(
                        width: Get.width * 0.8,
                        child: Text(otherEventModel['eventName'],
                            overflow: TextOverflow.ellipsis,
                            style: myResources.appHeadingStyle),
                      ),
                    ),
                    Column(
                      //crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            width: Get.width * 0.85,
                            child: Column(children: [
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                //mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("Coach:",
                                      style: myResources.appTextStyle),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Container(
                                    width: 100,
                                    child: Text(
                                      otherEventModel['coach'],
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          color: myResources.coachHeading),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("Venue:",
                                      style: myResources.appTextStyle),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Container(
                                    width: 100,
                                    child: Text(
                                      otherEventModel['venue'],
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("Time:",
                                      style: myResources.appTextStyle),
                                  SizedBox(
                                    width: 16,
                                  ),
                                  Container(
                                    width: Get.width*0.6,
                                    child: Text(
                                      DateFormat.yMEd()
                                          .add_jm()
                                          .format(otherEventModel['time'].toLocal()),
                                      style: TextStyle(
                                          color: myResources.headingColor),
                                    ),
                                  ),
                                ],
                              ),
                            ]),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
