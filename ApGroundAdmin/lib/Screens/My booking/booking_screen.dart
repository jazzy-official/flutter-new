import 'package:ap_ground_admin/Screens/My%20booking/past_bookings.dart';
import 'package:ap_ground_admin/Screens/My%20booking/today_booking_screen.dart';
import 'package:ap_ground_admin/Screens/My%20booking/upcomming_bookings.dart';
import 'package:ap_ground_admin/constants/util.dart';
import 'package:ap_ground_admin/controllers/booking_controller.dart';
import 'package:ap_ground_admin/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'Models/booking_model.dart';

class MyBookingScreen extends StatefulWidget {
  const MyBookingScreen({Key? key}) : super(key: key);

  @override
  State<MyBookingScreen> createState() => _MyBookingScreenState();
}

class _MyBookingScreenState extends State<MyBookingScreen> {
  void refresh() async {
    setState(() {
      isLoading=true;
    });
    myBookings = await BookingController().readAllBookings();
    setState(() {
      todayBookings = myBookings.where((element) => DateFormat('yyyy:MM:dd').format(element.date) == DateFormat('yyyy:MM:dd').format(DateTime.now())).toList();
      upcomingBookings = myBookings.where((element) => element.date.isAfter(DateTime.parse(DateFormat('yyyy-MM-dd').format(DateTime.now().add(Duration(days: 1)))).subtract(Duration(hours: 1)))).toList();
      pastBookings = myBookings.where((element) => element.date.isBefore(DateTime.parse(DateFormat('yyyy-MM-dd').format(DateTime.now())))).toList();

      isLoading=false;
    });
  }
  bool isLoading = true;
  late List<Booking> myBookings;
  List<Booking> todayBookings=[];
  List<Booking> upcomingBookings=[];
  List<Booking> pastBookings=[];

  @override
  void initState() {
    refresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          toolbarHeight: 0,
          bottom:  PreferredSize(
            preferredSize: Size(double.infinity,40),
            child: ColoredBox(
              color: Colors.white,
              child: SizedBox(
                height: 40,
                child: TabBar(
                  indicator: BoxDecoration(
                      borderRadius:
                      BorderRadius.circular(0),
                      color: MyResources.buttonColor),
                  labelColor: Colors.white,
                  unselectedLabelColor: Colors.black,
                  tabs: const [
                    Tab(child: Text("Today"),),
                    Tab(child: Text("Upcoming"),),
                    Tab(child: Text("Past"),),
                    Tab(child: Text("Canceled"),),
                  ],
                ),
              ),
            ),
          ),
        ),
        body: isLoading?Center(child: LoadingWidget(),) :TabBarView(
          children: [
            TodayBookingScreen(todayBookings: todayBookings,),
            UpcomingBookingScreen(upcomingBookings: upcomingBookings),
            PastBookingScreen(pastBookings: pastBookings),
            Icon(Icons.directions_bike),
          ],
        ),
      ),
    );
  }
}
