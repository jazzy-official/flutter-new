class Received {
  String date;
  double ammount;

  Received({
    required this.date,
    required this.ammount,
  });

  factory Received.fromJson(Map<String, dynamic> json) {
    return Received(
      date: json['Date'] as String,
      ammount: json['Amount'] as double,
    );
  }
}
