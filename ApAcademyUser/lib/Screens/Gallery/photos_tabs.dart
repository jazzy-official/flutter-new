import 'package:ap_academy_new/Constants/util.dart';
import 'package:ap_academy_new/ModelView/controllers/AlbumController.dart';
import 'package:ap_academy_new/Models/gallery_titleDescription.dart';
import 'package:ap_academy_new/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:photo_view/photo_view.dart';

class PhotosTab extends StatefulWidget {
  PhotosTab({Key? key, this.title, required this.i}) : super(key: key);
  final String? title;
  final int i;
  @override
  _PhotosTabState createState() => _PhotosTabState();
}

class _PhotosTabState extends State<PhotosTab> {
  final galeyController = Get.put(PhotoAlbumController());
  int page = 1;
  bool isLoading = false;
  //List<String> items = ['item 1', 'item 2', ];
  Future _loadData() async {
    // perform fetching data delay
    await new Future.delayed(new Duration(seconds: 2));
    print("load more");
    await galeyController.refreshData();
    // update data and loading status
    setState(() {
      /*items.addAll( ['item 1']);
      print('items: '+ items.toString());*/
      isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            constraints: new BoxConstraints(
              minHeight: 100,
            ),
            child: Card(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Obx(
                          () => Container(
                            child: Text(
                              galeyController.album[widget.i]['albumName']
                                  .toString(),
                              style: TextStyle(
                                  color: Color(0xff000074),
                                  fontSize: 16,
                                  fontWeight: FontWeight.w900),
                              textAlign: TextAlign.left,
                              overflow: TextOverflow.fade,
                            ),
                            constraints: new BoxConstraints(
                                maxHeight: 500, maxWidth: 500),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Obx(
                        () => Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: Text(
                              galeyController.album[widget.i]['desciption']
                                  .toString(),
                              textAlign: TextAlign.center,
                              style: TextStyle(color: Colors.blue),
                              overflow: TextOverflow.visible,
                            ),
                            constraints: new BoxConstraints(
                                maxHeight: 500, maxWidth: Get.width * 0.9),
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: NotificationListener<ScrollNotification>(
              onNotification: (ScrollNotification scrollInfo) {
                if (!isLoading &&
                    scrollInfo.metrics.pixels ==
                        scrollInfo.metrics.maxScrollExtent) {
                  _loadData();
                  // start loading data
                  setState(() {
                    isLoading = true;
                  });
                }
                return isLoading;
              },
              child: Container(
                height: Get.height * 0.7,
                child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Obx(
                      () => GridView.count(
                        shrinkWrap: true,
                        // Create a grid with 2 columns. If you change the scrollDirection to
                        // horizontal, this produces 2 rows.
                        crossAxisCount: 3,
                        // Generate 100 widgets that display their index in the List.
                        children: List.generate(
                            galeyController.album[widget.i]['albumPhotos']
                                .length, (index) {
                          return InkWell(
                            onTap: () => Get.to(PhotViewer(
                              imageString: galeyController.album[widget.i]
                                      ['albumPhotos'][index]
                                  .toString(),
                            )),
                            child: Card(
                              margin: EdgeInsets.all(10),
                              color: Colors.white60,
                              child: Container(
                                width: Get.width,
                                height: Get.height,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                      colorFilter: new ColorFilter.mode(
                                          Colors.black.withOpacity(0.8),
                                          BlendMode.dstATop),
                                      fit: BoxFit.cover,
                                      image: NetworkImage(
                                        galeyController.album[widget.i]
                                                ['albumPhotos'][index]
                                            .toString(),
                                      )),
                                ),
                              ),
                            ),
                          );
                        }),
                      ),
                    )),
              ),
            ),
          ),
          Container(
            height: isLoading ? 50.0 : 0,
            color: Colors.transparent,
            child: Center(
              child: new LoadingWidget(),
            ),
          ),
        ],
      ),
    );
  }
}

class PhotViewer extends StatefulWidget {
  final imageString;

  const PhotViewer({Key? key, this.imageString}) : super(key: key);
  @override
  _PhotViewerState createState() => _PhotViewerState();
}

class _PhotViewerState extends State<PhotViewer> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          child: PhotoView(
        imageProvider: NetworkImage(widget.imageString),
      )),
    );
  }
}
