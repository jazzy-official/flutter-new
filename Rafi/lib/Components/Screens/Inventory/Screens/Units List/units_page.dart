import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:rafi_admin/Authentication/login.dart';
import 'package:rafi_admin/Components/Screens/Inventory/Models/Unit%20List/unit_list_model.dart';
import 'package:rafi_admin/Components/Screens/Inventory/Screens/Units%20List/unit_list.dart';
import 'package:rafi_admin/Controllers/units_controller.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class UnitsPage extends StatefulWidget {
  const UnitsPage({Key? key}) : super(key: key);

  @override
  _UnitsPageState createState() => _UnitsPageState();
}

class _UnitsPageState extends State<UnitsPage> {
  // String url = 'https://raccounting.azurewebsites.net/api/Units?pageNumber=1&pageSize=100';
  // late UnitListModel _unitListModel;
  // bool isLoading = false;
  // Future<int?> getProjectId() async {
  //   final SharedPreferences prefs = await SharedPreferences.getInstance();
  //   return prefs.getInt('projectId');
  // }



  // Future getAllData(String val) async {
  //   setState(() {
  //     isLoading = true;
  //   });
  //   int? number = await getProjectId();
  //   await getList(number!, val);
  //   setState(() {
  //     isLoading = false;
  //   });
  // }

  @override
  void initState() {
    // getAllData(url);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: Container(
          height: 35,
          width: 170,
          decoration: BoxDecoration(
            color: Color(0xff205370),
            borderRadius: BorderRadius.circular(20)
          ),
          child: GetBuilder<UnitsController>(
            init: UnitsController(),
            builder: (cont) => cont.unitListModel ==null ? const Center(child: Text("- - -",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),)) : Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                IconButton(
                  padding: EdgeInsets.zero,
                  constraints: BoxConstraints(),
                  icon: const Icon(CupertinoIcons.backward_end_alt_fill,size: 13,color: Colors.white,),
                  onPressed: () {
                    cont.unitListModel!.previousPage=='null'?displayToast("You're already on first place", context):
                    {

                        cont.url = cont.unitListModel!.firstPage,

                      cont.getList(),
                    };
                  },
                ),
                IconButton(
                  icon: const Icon(Icons.arrow_back_ios,size: 13,color: Colors.white,),
                  padding: EdgeInsets.zero,
                  constraints: BoxConstraints(),
                  onPressed: () {
                    cont.unitListModel!.previousPage=="null"?displayToast("You're already on first place", context):
                    {

                        cont.url = cont.unitListModel!.previousPage!,

                      cont.getList(),
                    };
                  },
                ),
                SizedBox(
                  child:Center(
                    child: Text(
                      cont.unitListModel!.pageNumber.toString(),
                      style: const TextStyle(
                          color: Colors.white, fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),

                IconButton(
                  padding: EdgeInsets.zero,
                  constraints: BoxConstraints(),
                  icon: const Icon(Icons.arrow_forward_ios,size: 13,color: Colors.white,),
                  onPressed: () {
                    cont.unitListModel!.nextPage=="null"? displayToast("You're already on last place", context):
                    {

                        cont.url = cont.unitListModel!.nextPage!,

                      cont.getList(),
                    };
                  },
                ),

                IconButton(
                  padding: EdgeInsets.zero,
                  constraints: BoxConstraints(),
                  icon: const Icon(CupertinoIcons.forward_end_alt_fill,size: 13,color: Colors.white,),
                  onPressed: () {
                    cont.unitListModel!.nextPage=="null"? displayToast("You're already on last place", context):
                    {

                        cont.url = cont.unitListModel!.lastPage,

                      cont.getList(),
                    };
                  },
                ),
              ],
            ),
          ),
        ),
        body:UnitList());
  }
}
