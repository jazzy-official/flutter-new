import 'package:flutter/material.dart';
import 'list_roommate_card.dart';

class RoommateList extends StatelessWidget {
  final VoidCallback route;
   RoommateList({Key? key, required this.route}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
      itemCount: 3,
      itemBuilder: (context, index) {
        return  RoommateListCard(route: () => route(),);
      },
    );
  }
}
