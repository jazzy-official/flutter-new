import 'package:ap_ground_admin/Screens/Staff/Models/staff_model.dart';
import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:firebase_database/firebase_database.dart';


class StaffController {
  final DatabaseReference databaseReference =
  FirebaseDatabase.instance.reference().child("Staff");

  Future<void> saveStaff(Staff staff) async {
    try{ await databaseReference.push().set(staff.toJson());
    }catch(e){
      displayToast(e.toString());
    }
  }

  Future<void> updateStaff(Staff staff) async {
    databaseReference.update({
      staff.id! : staff.toJson()
    });
  }

  Future<void> updateAttendance(List<Staff> updatelist) async {
    updatelist.forEach((element) async {
      try{ await databaseReference.update({
        element.id! : element.toJson()
      });
      }catch(e){
        displayToast(e.toString());
      }
    });
  }


  Future<void> deleteStaff(String id) async {
    databaseReference.child(id).remove();

  }

  Future<List<Staff>> readStaff() async {
    List<Staff> staff = [];
    await databaseReference.once().then((DataSnapshot snapshot) {
      print("im in");
      if(snapshot.value != null){
        snapshot.value.forEach((key, value) {
          Staff gr = Staff.fromJson(value,key);
          staff.add(gr);
        });
      }

    });
    return staff;
  }

}
