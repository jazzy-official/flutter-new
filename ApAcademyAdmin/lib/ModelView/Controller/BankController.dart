import 'dart:async';

import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:get/get.dart';
import 'package:mongo_dart/mongo_dart.dart';

class BanksController extends GetxController {
  Rx<List<Map<String, dynamic>>> banksList = Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get banks => banksList.value;

  final dbHelp = Get.put(DatabaseController());
  @override
  Future<void> onInit() async {
    //await getTeamDropDown();

    super.onInit();
  }

  Future<bool?> delBank(ObjectId id) async {
    return await DatabaseController()
        .deletbanks(id)
        .then((value) => refreshBankData());
  }

  Future<bool> addBanks(Map<String, dynamic> t) async {
    return await DatabaseController().addBank(t);
  }

  refreshBankData() {
    this.banksList.bindStream(DatabaseController().banksStream());
  }
}
