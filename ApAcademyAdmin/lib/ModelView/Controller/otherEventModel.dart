import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:get/get.dart';
import 'package:mongo_dart/mongo_dart.dart';

class OtherEventController extends GetxController {
  Rx<List<Map<String, dynamic>>> eventList = Rx<List<Map<String, dynamic>>>([]);
  List<Map<String, dynamic>> get event => eventList.value;
  @override
  Future<void> onInit() async {
    //  await db.open();
    // String id = Get.find<AuthController>().firebaseUser.uid;
    eventList.bindStream(
        DatabaseController().otherEventStream()); //id)); //Stream Comming from
    super.onInit();
  }

  Future<bool> delEvent(ObjectId id) async {
    if (await DatabaseController().deletEvent(id)) {
      refreshDate();
      return true;
    } else {
      return false;
    }
  }

  Future refreshDate() async {
    //await Future.delayed(3.seconds);
    this.eventList.bindStream(DatabaseController().otherEventStream());
  }
}
