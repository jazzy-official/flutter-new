import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:ionicons/ionicons.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Screens/User%20Forms/verify_identity_form.dart';

class SignupFormTwo extends StatefulWidget {
  const SignupFormTwo({Key? key}) : super(key: key);

  @override
  State<SignupFormTwo> createState() => _SignupFormTwoState();
}

class _SignupFormTwoState extends State<SignupFormTwo> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: myResources.background,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        backgroundColor: myResources.background,
        title: Text(
          "Enter your Location",
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
      ),
      body:  SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                //Add padding around textfield
                padding: const EdgeInsets.only(top: 25.0, bottom: 30),
                child: SizedBox(
                  height: 35,
                  child: TextField(
                    keyboardType: TextInputType.name,
                    onChanged: (val) {},
                    decoration: InputDecoration(
                      //Add th Hint text here.
                      hintText: "Enter a City",
                      prefixIcon: Icon(Ionicons.search_outline,size: 22,),
                      contentPadding:
                      const EdgeInsets.only(top: 10, left: 15),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(3.0),
                      ),
                    ),
                  ),
                ),
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.white,
                  shape:   RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(3)),
                      side: BorderSide(color: myResources.themeColor)
                  ),
                ),
                onPressed: () {
                  // Get.to(()=> const SignupFormTwo());
                },

                child: Container(
                  padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.my_location_outlined,color: myResources.themeColor,size: 20,),
                      Text('  Set Current Location', style: TextStyle(color: myResources.themeColor),),

                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              const Text(
                "Popular Cities",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 25.0),
                child: GridView.builder(
                  shrinkWrap: true,
                    controller: ScrollController(),
                    gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                        maxCrossAxisExtent: 250,
                        childAspectRatio: 11 / 6,
                        crossAxisSpacing: 20,
                        mainAxisSpacing: 20),
                    itemCount: 6,
                    itemBuilder: (BuildContext ctx, index) {
                      return Container(
                        alignment: Alignment.center,
                        child: const Text("Faisalabad",style: TextStyle(color: Colors.black54),),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.black45,),
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(3)),
                      );
                    }),
              ),
               ElevatedButton(
                 style: ElevatedButton.styleFrom(
                   primary: myResources.themeColor,
                   shape: const RoundedRectangleBorder(
                       borderRadius: BorderRadius.all(Radius.circular(14))
                   ),
                 ),
                 onPressed: () {
                   Get.to(()=> const SignupFormThree());
                 },

                 child: Container(
                   padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                   child: Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children: <Widget>[
                       Text('Confirm', style: TextStyle(color: Colors.white),),
                       Container(
                         padding: const EdgeInsets.all(8),
                         decoration: BoxDecoration(
                           borderRadius: const BorderRadius.all(Radius.circular(20)),
                           color: myResources.themeColor,
                         ),
                         child: Icon(Icons.arrow_forward_ios, color: Colors.white, size: 16,),
                       )
                     ],
                   ),
                 ),
               ),

            ],
          ),
        ),
      ),
    );
  }
}

