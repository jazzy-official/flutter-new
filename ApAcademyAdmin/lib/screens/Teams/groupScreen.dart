import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:ionicons/ionicons.dart';
import 'package:ap_academy_admin/ModelView/Controller/GroupController.dart';
import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:ap_academy_admin/ModelView/Controller/teamsController.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:ap_academy_admin/screens/Teams/allGroups.dart';
import 'package:ap_academy_admin/screens/shared/DeletPop.dart';
import 'package:ap_academy_admin/screens/shared/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';


calculateAge(DateTime birthDate) {
  DateTime currentDate = DateTime.now();
  int age = currentDate.year - birthDate.year;
  int month1 = currentDate.month;
  int month2 = birthDate.month;
  if (month2 > month1) {
    age--;
  } else if (month1 == month2) {
    int day1 = currentDate.day;
    int day2 = birthDate.day;
    if (day2 > day1) {
      age--;
    }
  }
  return age;
}

class GroupScreen extends StatefulWidget {
  @override
  _GroupScreenState createState() => _GroupScreenState();
}

class _GroupScreenState extends State<GroupScreen> {
  TextEditingController searchTextController = TextEditingController();
  final teamC = Get.put(TeamController());

  List userList = [];
  List selectedUserList = [];
  final databaseController = Get.put(DatabaseController());
  RxString selectedValue = ''.obs;
  @override
  void initState() {
    // teamC.userList;
    getusers();
    super.initState();
  }
  
  void getusers () async {
    await teamC.getUsers().then((value) async{
       setState(() {
        userList =  teamC.userList;
      });
       userList.sort((a, b) => a['name'].toLowerCase().compareTo(b["name"].toLowerCase()));
       setState(() {

       });
    });
  }
  void _runFilter(String enteredKeyword) {
    List results = [];
    if (enteredKeyword.isEmpty) {
      // if the search field is empty or only contains white-space, we'll display all users
      results = teamC.userList;
    } else {
      if(_value==0){results = teamC.userList
          .where((e) => e['name']
          .toUpperCase()
          .contains(enteredKeyword.toUpperCase()))
          .toList();
      }else if(_value==1){
        results = teamC.userList
            .where((e) => calculateAge(e['dob']).toString()==enteredKeyword)
            .toList();
      }
      // we use the toLowerCase() method to make it case-insensitive
    }
    // Refresh the UI
    setState(() {
      userList = results;
    });
    print(userList);
  }

  @override
  void dispose() {
    super.dispose();
  }
  int? _value = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyResources.backgroundColor,
      body: Material(
        //Create an event add button
        child: Column(
          children: [
            Row(
              children: [
                Container(
                  width: Get.width -125,
                  height: 55,
                  padding: EdgeInsets.all(10),
                  child: TextField(
                    keyboardType: _value == 1?TextInputType.number:TextInputType.text,
                    onChanged: (val) {
                      _runFilter(val);
                    },
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(horizontal: 10,vertical: 0),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      hintText: 'Search',
                    ),
                  ),
                ),
                Wrap(
                    children: [
                      ChoiceChip(
                        elevation: 4,
                        pressElevation: 2,
                        shadowColor: Colors.black,
                        label: Text('Name'),
                        selected: _value == 0,
                        onSelected: (bool selected) {
                          setState(() {
                            _value = selected ? 0 : 0;
                            print(_value.toString());
                          });
                        },
                      ),
                      SizedBox(width: 3,),
                      ChoiceChip(
                        elevation: 4,
                        pressElevation: 2,
                        shadowColor: Colors.black,
                        label: Text('Age'),
                        selected: _value == 1,
                        onSelected: (bool selected) {
                          setState(() {
                            _value = selected ? 1 : 1;
                            print(_value.toString());
                          });
                        },
                      )
                    ]
                ),
              ],
            ),
            //Card


                  Expanded(
                    flex: 1,
                    child: userList.isEmpty?Container(child: Center(child: Text("No Result Found"),),): ListView.builder(
                        itemCount: userList.length,
                        itemBuilder: (BuildContext context, int index) {
                          return getTeamCard(
                                  userList[index]);


                        }),
                  ),
          ],
        ),
      ),
    );
  }

  Widget getTeamCard(Map<String, dynamic> team) {
    return Padding(
      padding: const EdgeInsets.only(top: 8, left: 8, right: 8),
      child: Center(
        child: InkWell(
          /*onTap: () {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) => TeamsDetails()));
                    },*/

          onTap: () => launch("tel://"+team['phone'].toString()),
          child: Container(
            constraints: BoxConstraints(minHeight: 120),

            child: Card(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: (){
                      Get.dialog(
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Center(
                            child: Container(
                              child: Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: CachedNetworkImage(
                                    // height: Get.width,
                                    maxHeightDiskCache:(Get.height*0.6).toInt() ,
                                    fit: BoxFit.cover,
                                    // width: 280,
                                    imageUrl: team['photoUrl'],
                                    placeholder: (context, url) =>
                                        LoadingWidget(),
                                    errorWidget:
                                        (context, url, error) =>
                                        Icon(Icons.error),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                    child: Container(
                      padding: EdgeInsets.all(5),
                        width: Get.width*0.3,
                        child: CircleAvatar(
                          minRadius: 55,
                          backgroundImage: NetworkImage(team['photoUrl']),
                        )),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(height: 8,),
                        Text(team['name'],style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),
                        SizedBox(height: 10,),
                        Row(
                          children: [
                            Icon(Icons.phone,size: 18,color: Colors.grey,),
                            SizedBox(width: 10,),
                            Text(team['phone'],),
                          ],
                        ),
                        SizedBox(height: 10,),
                        Row(
                          children: [
                            Icon(Icons.mail,size: 18,color: Colors.grey,),
                            SizedBox(width: 10,),
                            Expanded(child: Text(team['email'],)),
                          ],
                        ),
                        SizedBox(height: 10,),
                        Row(
                          children: [
                            Text("Age: ",style: TextStyle(fontWeight: FontWeight.w600),),
                            Text(team['dob']!=null?calculateAge(team['dob']).toString():"N/A"),
                          ],
                        ),
                        SizedBox(height: 10,),
                        Row(
                          children: [
                            Text("CHRICHQ ID: ",style: TextStyle(fontWeight: FontWeight.w600),),
                            Text(team['chricHqId']!=null?team['chricHqId']:"N/A"),
                          ],
                        ),
                        SizedBox(height: 10,),
                      ],
                    ),
                  ),
                  Switch(
                    value: team['isActive'],
                    onChanged: (value) {
                      team['isActive']
                          ? Get.dialog(Center(
                              child: DeletpopUp(
                                  message: 'Make this user inactive?',
                                  pressFunc: () async {
                                    await Get.find<GroupsControler>()
                                        .makeUserInactive(team['_id'])
                                        .then((value) {});
                                    Get.back();
                                    Get.find<GroupsControler>()
                                        .refreshGroups();
                                    Get.find<GroupsControler>().refreshGroups();

                                    if (mounted) setState(() {});
                                    getusers();
                                  },
                                  isNotDel: true)))
                          : Get.dialog(Center(
                              child: DeletpopUp(
                                  message: 'Make this user active?',
                                  pressFunc: () async {
                                    await Get.find<GroupsControler>()
                                        .makeUseractive(team['_id'])
                                        .then((value) {
                                      Get.find<GroupsControler>()
                                          .refreshGroups();
                                      Get.find<GroupsControler>()
                                          .refreshGroups();
                                      if (mounted) setState(() {});
                                    });
                                    Get.back();
                                    getusers();
                                  },
                                  isNotDel: true)));
                    },
                    activeTrackColor: Colors.lightGreenAccent,
                    activeColor: Colors.green,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
