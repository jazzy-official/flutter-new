import 'package:ap_academy_admin/constants/util.dart';
import 'package:ap_academy_admin/screens/Teams/groupScreen.dart';
import 'package:ap_academy_admin/screens/Teams/teams_screen.dart';
import 'package:flutter/material.dart';

import 'allGroups.dart';

class TeamGroupTabs extends StatefulWidget {
  @override
  _TeamGroupTabsState createState() => _TeamGroupTabsState();
}

class _TeamGroupTabsState extends State<TeamGroupTabs> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: MyResources.buttonColor,
            toolbarHeight: 10,
            bottom: TabBar(tabs: [
              // Tab(
              //   text: 'Teams',
              // ),
              Tab(
                text: 'STUDENTS',
              ),
              Tab(
                text: 'GROUPS',
              ),
            ]),
          ),
          body: TabBarView(
            children: [GroupScreen(), GroupsScreen()],
          ),
        ));
  }
}
