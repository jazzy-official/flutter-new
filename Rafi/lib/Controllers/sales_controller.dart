
import 'dart:convert';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/RecievePaidChecklist/receive_paid_checklist.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/Recovery/recovery.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/Units%20Model/units.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/account%20model/accounts.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Payables/Model/payable_model.dart';
import 'package:rafi_admin/Components/Screens/Inventory/Models/Unit%20List/unit_list_model.dart';
import 'package:rafi_admin/Components/Screens/Inventory/Models/Unit%20Model/unit_model.dart';
import 'package:rafi_admin/Components/Screens/Sales/Models/deals_model.dart';
import 'package:rafi_admin/Controllers/authcontroller.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SalesController extends GetxController {
  final auth = Get.put(AuthController());
  List<DealModel>? deals;
  late List<DealModel> foundResult = [];
  int? value = 1;
  String? token;
  int? number;

  @override
  Future<void> onInit() async {
    await refreshAll();
    super.onInit();
  }

  Future refreshAll() async {
    token = await auth.getToken();
    number = await auth.getProjectId();
    await getDeals();

  }

  Future getDeals() async {
    http.Response response = await http.get(
      Uri.parse('https://raccounting.azurewebsites.net/api/DealAffiliates/Deals/Get?projectid=$number'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        "Authorization" : "Bearer "+token!,
      },
    );
    if (response.statusCode == 200) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      Map<String, dynamic> dealList = await jsonDecode(response.body);
      List<dynamic> data = dealList["Data"];
      List<DealModel> dealsdata = data
          .map(
            (dynamic item) => DealModel.fromJson(item),
      )
          .toList();
      deals = dealsdata;
      foundResult = deals!;
      print(response.body);
      update();
      // Account account = Account.fromJson(jsonDecode(response.body));
      // return user;
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to get deals data');
    }
  }
  void runFilter(String enteredKeyword) {
    List<DealModel> results = [];
    if (enteredKeyword.isEmpty) {
      // if the search field is empty or only contains white-space, we'll display all users
      results = deals!;
    } else {
      if(value==0){results = deals!
          .where((user) => user.affiliateName
          .toUpperCase()
          .contains(enteredKeyword.toUpperCase()))
          .toList();
      }else if(value==1){
        results = deals!
            .where((user) => user.dealName
            .toUpperCase()
            .contains(enteredKeyword.toUpperCase()))
            .toList();
      }

      // we use the toLowerCase() method to make it case-insensitive
    }

    // Refresh the UI
      foundResult = results;
      update();

  }


}
