import 'package:ap_ground_admin/Screens/Payrole/models/payroll_model.dart';

class Staff {
  final String? id;
  final String designation;
  final String name;
  final String number;
  final String imgUrl;
  final Payroll payroll;

  Staff({this.id,required this.designation, required this.name, required this.number,required this.imgUrl,required this.payroll});

// NOTE: implementing functionality here in the next step!

  factory Staff.fromJson(dynamic json,String id) {
    return Staff(
      id: id.toString(),
      name : json['name'],
      designation : json['designation'],
      number : json['number'],
      imgUrl: json['imgurl'],
      payroll: Payroll.fromJson(json['payroll']),
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = name.toString();
    data['designation'] = designation;
    data['number'] = number;
    data['imgurl'] = imgUrl;
    data['payroll'] = payroll.toJson();
    return data;
  }
}