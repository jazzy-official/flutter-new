import 'package:ap_academy_new/Constants/util.dart';
import 'package:flutter/material.dart';

import 'Notificationtab.dart';
import 'newstab.dart';

class AnnouncementScreen extends StatefulWidget {
  @override
  _AnnouncementScreenState createState() => _AnnouncementScreenState();
}

class _AnnouncementScreenState extends State<AnnouncementScreen> {
  @override
  void dispose() {
    //TODO  closeStream();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: myResources.buttonColor,
            toolbarHeight: 10,
            bottom: const TabBar(tabs: [
              Tab(
                text: 'NOTIFICATIONS',
              ),
              Tab(
                text: 'NEWS',
              ),
            ]),
          ),
          body: TabBarView(
            children: [
              NotificationScreen(),
              NewsScreen(),
            ],
          ),
        ));
  }
}
