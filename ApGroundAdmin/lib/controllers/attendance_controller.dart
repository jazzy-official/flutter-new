import 'package:ap_ground_admin/Screens/Attendance/Models/attendance_model.dart';
import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:intl/intl.dart';


class AttendanceController {
  final DatabaseReference databaseReference =
  FirebaseDatabase.instance.reference().child("Attendance");

  Future<void> saveAttendance(Attendance attendance) async {
    try{ await databaseReference.push().set(attendance.toJson());
    }catch(e){
      displayToast(e.toString());
    }
  }

  Future<void> updateAttendance(Attendance attendance) async {

    databaseReference.update({
      attendance.id! : attendance.toJson()
    });

  }
  Future<void> deleteAttendance(String id) async {
    databaseReference.child(id).remove();

  }

  Future<List<Attendance>> readAttendance() async {
    List<Attendance> attendance = [];
    await databaseReference.once().then((DataSnapshot snapshot) {
      print("im in");
      if(snapshot.value != null){
        snapshot.value.forEach((key, value) {
          Attendance gr = Attendance.fromJson(value,key);
          attendance.add(gr);
        });
      }

    });
    return attendance;
  }

  Future<Attendance> readAttendancebyDate(DateTime date) async {
    List<Attendance> attendanceList = await readAttendance();
    List<Attendance> attendance = attendanceList
        .where((element) => DateFormat('yyyy:MM:dd').format(DateTime.parse(element.date)) == DateFormat('yyyy:MM:dd').format(date))
        .toList();
    return attendance.first;
  }


}
