import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Screens/Home/Widgets/city_card.dart';
import 'package:room_buddy/Screens/Home/Widgets/featured_room_list.dart';
import 'package:room_buddy/Screens/Home/Widgets/localities_list.dart';
import 'package:room_buddy/Screens/Home/Widgets/feature_list_room_card.dart';
import 'package:room_buddy/Screens/Home/Widgets/room_list.dart';
import 'package:room_buddy/Screens/Home/create_listing_page.dart';
import 'package:room_buddy/Screens/Home/room_detail.dart';
import 'package:shimmer/shimmer.dart';

import '../../controllers/explore_controller.dart';

class RoomFeed extends StatefulWidget {
  const RoomFeed({Key? key}) : super(key: key);

  @override
  State<RoomFeed> createState() => _RoomFeedState();
}

class _RoomFeedState extends State<RoomFeed> {
  var expcont = Get.put(ExplorerController());
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 5),
      child: GetBuilder<ExplorerController>(
        init: ExplorerController(),
        builder: (cont) => cont.rooms.isNotEmpty? ListView(
          children: [
//           Container(
//             margin: EdgeInsets.only(top: 15),
//             width: MediaQuery.of(context).size.width,
//             padding: EdgeInsets.all(15),
//             decoration: BoxDecoration(
//               color: myResources.semiColor.withOpacity(0.07),
//               borderRadius: BorderRadius.circular(5),
//             ),
//             child: Row(
//               children: [
//                 Expanded(
//                   flex: 5,
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       const Text(
//                         "Want Room Recommendations?",
//                         style: TextStyle(
//                             color: Colors.black,
//                             fontWeight: FontWeight.bold,
//                             fontSize: 16),
//                       ),
//                       Container(
//                         padding: const EdgeInsets.symmetric(vertical: 10),
//                         child: Text(
//                           """Create a listing and we'll show you room
// recommendations matching your profile""",
//                           style: TextStyle(color: Colors.black45, fontSize: 13),
//                         ),
//                       ),
//                       ElevatedButton(
//                         style: ElevatedButton.styleFrom(
//                           primary: Colors.white,
//                           shape: RoundedRectangleBorder(
//                             borderRadius: BorderRadius.all(Radius.circular(5)),
//                           ),
//                         ),
//                         onPressed: () {
//                           Get.to(ListingPage());
//                         },
//                         child: Text(
//                           'Create a listing',
//                           style: TextStyle(
//                             color: myResources.semiColor,
//                             fontSize: 13,
//                           ),
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//                 Expanded(flex: 2, child: Image.asset("assets/room_3d.png")),
//               ],
//             ),
//           ),
            Padding(
              padding: const EdgeInsets.only(top: 15.0,bottom: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    " Featured Rooms",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  InkWell(
                      onTap: (){},
                      child: Text(
                        "See all",
                        style: TextStyle(
                            color: myResources.themeColor, fontWeight: FontWeight.w600),
                      ))
                ],
              ),
            ),
            const Text(
              " most viewed rooms in and around your area",
              style: TextStyle(fontSize: 13, color: Colors.black45),
            ),
            SizedBox(height: 10,),
            FeaturedRoomList(),
            // Padding(
            //   padding: const EdgeInsets.only(top: 35.0,bottom: 5),
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //     children: [
            //       const Text(
            //         " Localities near you",
            //         style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            //       ),
            //       InkWell(
            //           onTap: (){},
            //           child: Text(
            //             "See all",
            //             style: TextStyle(
            //                 color: myResources.themeColor, fontWeight: FontWeight.w600),
            //           ))
            //     ],
            //   ),
            // ),
            // const Text(
            //   " Explore rooms in popular cities nearby",
            //   style: TextStyle(fontSize: 13, color: Colors.black45),
            // ),
            // SizedBox(height: 10,),
            // LocalitiesList(),
            Padding(
              padding: const EdgeInsets.only(top: 15.0,bottom: 5),
              child: Text(
                " Explore",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
            RoomList(rooms: expcont.foundResult,),
            const Padding(
              padding: EdgeInsets.only(top: 35.0,bottom: 5),
              child: Text(
                " Get in touch",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 100.0,left: 5),
              child: const Text(
                "We love to hear what you love and what we can do better.",
                style: TextStyle(fontSize: 14, color: Colors.black45),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15.0,horizontal: 5),
              child: InkWell(
                onTap: (){

                },
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Icon(Ionicons.send_outline,color: myResources.themeColor,),
                    Text("   Send feedback",style: TextStyle(color: myResources.themeColor,fontWeight: FontWeight.w600,fontSize: 18),)
                  ],
                ),
              ),
            ),
            const SizedBox(height: 50,),
          ],
        ):Shimmer.fromColors(
          baseColor: Colors.grey.shade300,
          highlightColor: Colors.grey.shade100,
          enabled: true,
          child: ListView(

            children: [
              Padding(
                padding: const EdgeInsets.only(top: 15.0,bottom: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 20,
                      width: 150,
                      color: Colors.white,
                    ),
                    Container(
                      height: 15,
                      width: 50,
                      color: Colors.white,
                    ),
                  ],
                ),
              ),
               Container(
                height: 15,
                width: 150,
                color: Colors.white,
              ),
              const SizedBox(height: 10,),
              SizedBox(
                width: MediaQuery.of(context).size.width-30,
                height: 270,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: 3,
                  itemBuilder: (context, index) {
                    return  const Card(
                      child: SizedBox(
                        width: 250,
                      ),
                    );
                  },
                ),
              ),
              Container(
                width: 150,
                height: 20,
                color: Colors.white,
                margin: const EdgeInsets.only(top: 15.0,bottom: 5,right: 250),
              ),
              ListView.builder(
                shrinkWrap: true,
                // physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                controller: ScrollController(),
                itemCount: 4,
                itemBuilder: (context, index) {
                  return  Card(
                    child: SizedBox(
                      height: 300,
                    ),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
