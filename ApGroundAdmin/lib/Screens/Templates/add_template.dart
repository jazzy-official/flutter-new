import 'package:ap_ground_admin/Screens/Grounds/Models/slot_model.dart';
import 'package:ap_ground_admin/Screens/Grounds/Models/template_model.dart';
import 'package:ap_ground_admin/constants/util.dart';
import 'package:ap_ground_admin/controllers/fire_base_controller.dart';
import 'package:ap_ground_admin/controllers/template_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ionicons/ionicons.dart';

import '../../loading_widget.dart';
import '../delete_popup.dart';

class AddTemplate extends StatefulWidget {
  final Template? template;
  const AddTemplate({Key? key, this.template}) : super(key: key);

  @override
  _AddTemplateState createState() => _AddTemplateState();
}

class _AddTemplateState extends State<AddTemplate> {
  void _showDatePicker(ctx) {
    showCupertinoModalPopup(
        context: ctx,
        builder: (_) => Container(
              height: 353,
              color: Color.fromARGB(255, 255, 255, 255),
              child: Column(
                children: [
                  Container(
                    height: 300,
                    child: CupertinoDatePicker(
                        initialDateTime: DateTime.now(),
                        mode: CupertinoDatePickerMode.time,
                        onDateTimeChanged: (val) {
                          setState(() {
                            _chosenDateTime = val;
                          });
                          print(_chosenDateTime);
                        }),
                  ),

                  // Close the modal
                  CupertinoButton(
                    child: Text('OK'),
                    onPressed: () => Navigator.of(ctx).pop(),
                  )
                ],
              ),
            ));
  }

  DateTime? _chosenDateTime = DateTime.now();
  int? hours = 0;
  double? price = 0;
  String name = "";
  List<Slot> slots = [];
  void checkeditadd(){
    if(widget.template!=null){
      setState(() {
        name = widget.template!.name;
        slots = widget.template!.slots;
      });

    }
  }
  @override
  void initState() {
    checkeditadd();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          if(widget.template != null )Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: IconButton(onPressed: () async {
              showDialog(
                  context: context,
                  builder: (_) => AlertDialog(
                    content: DeletepopUp(message: "Are you sure You wanna delete ?", pressFunc: () async {
                      await TemplateController().deleteTemplate(widget.template!.id!);
                      Navigator.pop(context);
                      Navigator.pop(context);
                      displayToast("Deleted successfully");
                    },),
                  )
              );
            },icon:Icon(Icons.delete)),
          )
        ],
        backgroundColor: MyResources.buttonColor,
        title: Text("Add/Edit Template"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              height: 50,
              padding: const EdgeInsets.all(5.0),
              child: TextFormField(
                initialValue: name,
                keyboardType: TextInputType.name,
                onChanged: (val) {
                  setState(() {
                    name = val;
                  });
                },
                decoration: InputDecoration(
                  //Add th Hint text here.
                  hintText: "Template Name",
                  contentPadding: EdgeInsets.only(left: 10),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
            Card(
              elevation: 4,
              child: Padding(
                //Add padding around textfield
                  padding: EdgeInsets.only(top: 15.0, left: 7, right: 7),
                  child: Column(
                    children: [
                      Text(
                        " Add Slot",
                        style:
                        TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          TextButton(
                              onPressed: () {
                                _showDatePicker(context);
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Colors.indigo),
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text(
                                    _chosenDateTime != null
                                        ? DateFormat('hh:mm a')
                                        .format(_chosenDateTime!)
                                        : "Start time",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              )),
                          SizedBox(
                            width: 80,
                            height: 37,
                            child: TextFormField(
                              keyboardType: TextInputType.number,
                              onChanged: (val) {
                                setState(() {
                                  hours = int.parse(val);
                                });
                              },
                              decoration: InputDecoration(
                                //Add th Hint text here.
                                hintText: "Hours",
                                contentPadding: EdgeInsets.only(left: 10),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            width: 80,
                            height: 37,
                            margin: EdgeInsets.symmetric(horizontal: 10),
                            child: TextFormField(
                              keyboardType: TextInputType.number,
                              onChanged: (val) {
                                setState(() {
                                  price = double.parse(val);
                                });
                              },
                              decoration: InputDecoration(
                                //Add th Hint text here.
                                hintText: "Price",
                                contentPadding: EdgeInsets.only(left: 10),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                              ),
                            ),
                          ),
                          TextButton(
                              onPressed: () {
                                slots.add(Slot(
                                    startTime: _chosenDateTime!,
                                    price: price!,
                                    hours: hours!));
                                setState(() {});
                              },
                              child: Container(
                                width: 50,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Colors.indigo),
                                child: const Padding(
                                  padding:
                                  EdgeInsets.symmetric(vertical: 7),
                                  child: Center(
                                      child: Icon(Ionicons.add,color: Colors.white,)),
                                ),
                              )),
                        ],
                      ),
                      SizedBox(height: 5,)
                    ],
                  )),
            ),
            Container(
              margin:
                  const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.black, width: 1),
                  borderRadius: BorderRadius.circular(3)),
              child: Column(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3),
                      color: Colors.grey.shade300,
                    ),
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: const [
                        Expanded(
                            flex: 2, child: Center(child: Text("Slot no"))),
                        Expanded(
                            flex: 4, child: Center(child: Text("price"))),
                        Expanded(
                            flex: 4,
                            child: Center(child: Text("Start Time"))),
                        Expanded(
                            flex: 2, child: Center(child: Text("Hours"))),
                        Expanded(flex: 1, child: Center(child: Text("  "))),
                      ],
                    ),
                  ),
                  Container(
                    constraints:
                        BoxConstraints(minHeight: 50, maxHeight: 220),
                    child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: slots.length,
                        itemBuilder: (BuildContext context, int index) {
                          Slot slot = slots[index];
                          return Card(
                            elevation: 4,
                            child: Container(
                              height: 50,
                              color: const Color(0xfff1f2f8),
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                      flex: 1,
                                      child: Center(
                                          child: Text(
                                              (index + 1).toString()))),
                                  Expanded(
                                      flex: 3,
                                      child: Center(
                                          child:
                                              Text(slot.price.toString()))),
                                  Expanded(
                                      flex: 3,
                                      child: Center(
                                          child: Text(DateFormat('hh:mm a')
                                              .format(slot.startTime)))),
                                  Expanded(
                                      flex: 2,
                                      child: Center(
                                          child: Text(
                                              (slot.hours).toString()))),
                                  Expanded(
                                      flex: 1,
                                      child: IconButton(
                                        onPressed: () {
                                         slots.removeAt(index);
                                          setState(() {

                                          });
                                        },
                                        icon: Icon(
                                          Icons.delete,
                                          color: Colors.red,
                                        ),
                                      )),
                                ],
                              ),
                            ),
                          );
                        }),
                  ),
                ],
              ),
            ),

            Padding(
              padding: EdgeInsets.only(top: 30.0, left: 10, right: 10),
              child: TextButton(
                  onPressed: () async {
                    if(name == ""){
                      displayToast("Please Enter template name");
                    }else if (slots.length == 0){
                      displayToast("Please add at least 1 slot");
                    } else {
                      showDialog(
                          context: context,
                          builder: (context) {
                            return  LoadingWidget();
                          });
                      if(widget.template==null){
                        await TemplateController().saveTemplate(Template(name: name,slots: slots));
                        Navigator.pop(context);
                        Navigator.pop(context);
                        displayToast("Template saved");
                      }else if(widget.template != null){
                        await TemplateController().updateTemplate(Template(id: widget.template!.id!,name: name,slots: slots));
                        Navigator.pop(context);
                        Navigator.pop(context);
                        displayToast("Template updated");
                      }
                    }

                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.9,
                    height: 45,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.indigo),
                    child: Center(
                        child: Text(
                      "Save",
                      style: TextStyle(color: Colors.white),
                    )),
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
