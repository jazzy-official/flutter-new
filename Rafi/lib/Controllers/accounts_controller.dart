
import 'dart:convert';

import 'package:get/get_connect/http/src/response/response.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/RecievePaidChecklist/receive_paid_checklist.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/Recovery/recovery.dart';
import 'package:rafi_admin/Components/Screens/Accounts/Sub%20Screens/Dashboard/Model/account%20model/accounts.dart';
import 'package:rafi_admin/Controllers/authcontroller.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AccountController extends GetxController {
  final auth = Get.put(AuthController());
  Account? acccountslist;
  List<RecoveryAmmount>? recovery;
  List<Received>? received;
  String? token;
  int? number;
  @override
  Future<void> onInit() async {
    await refreshAll();
    super.onInit();
  }

  Future refreshAll() async {
    token = await auth.getToken();
    number = await auth.getProjectId();
    await setAccounts();
    await setPaidReceivedAmmount();
    await setRecovery();

  }
  Future setAccounts() async {
    http.Response response = await http.get(
      Uri.parse('https://raccounting.azurewebsites.net/api/Reports/AccountsChecklist?Projectid=$number'),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );
    print(response.statusCode);
    if (response.statusCode == 200) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      SharedPreferences pref = await SharedPreferences.getInstance();
      String account = response.body;
      acccountslist =  Account.fromJson(jsonDecode(jsonDecode(account)));
      refresh();
      update();

      // Account account = Account.fromJson(jsonDecode(response.body));
      // return user;
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to get account check list');
    }
  }
  Future setRecovery() async {
    http.Response response = await http.get(
      Uri.parse('https://raccounting.azurewebsites.net/api/Reports/DashBoardRecoverySheet?Projectid=$number'),headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    },
    );
    if (response.statusCode == 200) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      String dataa = response.body;
      Map<String, dynamic> recoveryList = await jsonDecode(jsonDecode(dataa));
      List<dynamic> data = recoveryList["Data"];
      List<RecoveryAmmount> recoverd = data
          .map(
            (dynamic item) => RecoveryAmmount.fromJson(item),
      )
          .toList();
      recovery = recoverd;
      refresh();
      update();
      // Account account = Account.fromJson(jsonDecode(response.body));
      // return user;
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to get recovery data');
    }
  }
  Future setPaidReceivedAmmount() async {
    http.Response response = await http.get(
      Uri.parse('https://raccounting.azurewebsites.net/api/Reports/ReceivedPaidChecklist?ProjectId=$number'),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );
    if (response.statusCode == 200) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      SharedPreferences pref = await SharedPreferences.getInstance();
      // print(jsonDecode(response.body));
      String RPdata = response.body;
      Map<String, dynamic> receivedData = await jsonDecode(jsonDecode(RPdata));
      List<dynamic> data = receivedData["Received"];
      List<Received> receives = data
          .map(
            (dynamic item) => Received.fromJson(item),
      )
          .toList();
      received = receives;
      refresh();
      update();
      // Account account = Account.fromJson(jsonDecode(response.body));
      // return user;
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to login.');
    }
  }
  // Future setUnitsData(int projectId,String token) async {
  //
  //   http.Response response = await get(
  //       Uri.parse(
  //           'https://raccounting.azurewebsites.net/api/Units/Count?ProjectId=1'),
  //       headers: {
  //         'Content-Type': 'application/json',
  //         'Accept': 'application/json',
  //         'Authorization': 'Bearer $token',
  //       });
  //   // print(response.statusCode);
  //   if (response.statusCode == 200) {
  //     // If the server did return a 201 CREATED response,
  //     // then parse the JSON.
  //     SharedPreferences pref = await SharedPreferences.getInstance();
  //     String unitData = jsonDecode(response.body);
  //     pref.setString('unitsData', unitData);
  //     // Account account = Account.fromJson(jsonDecode(response.body));
  //     // return user;
  //   } else {
  //     // If the server did not return a 201 CREATED response,
  //     // then throw an exception.
  //     throw Exception('Failed to set unit data');
  //   }
  // }

}
