import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:rafi_admin/Components/Navigation/navigation_bar.dart';
import 'package:http/http.dart' as http;
import 'package:rafi_admin/Components/loader_widget.dart';
import 'package:rafi_admin/Controllers/authcontroller.dart';
import 'package:rafi_admin/Wrapper/wrapper.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'modal/user.dart';


class loginScreen extends StatefulWidget {
   loginScreen({Key? key,}) : super(key: key);

  @override
  State<loginScreen> createState() => _loginScreenState();
}

class _loginScreenState extends State<loginScreen> {
   bool isLoading = true;
   String email = '';
   String pass = '';

   final authcontroller = Get.put(AuthController());

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: const Color(0xff205370),
      body: !isLoading ?  const Center(child: CircularProgressIndicator()) :  Center(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: const EdgeInsets.only(top: 70),
                // height: size.height * 0.3,
                child: Center(
                  child: Container(
                    height: 180,
                    width: 180,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage("assets/images/rafilogo.png"),
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    const Center(
                      child: Text(
                        "Welcome Back",
                        style: TextStyle(
                          fontSize: 30,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),

                    const SizedBox(
                      height: 30,
                    ),
                    Container(
                      padding: EdgeInsets.all(8.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Colors.transparent,
                      ),
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding: const EdgeInsets.symmetric(vertical: 10.0),
                            decoration: BoxDecoration(
                              border: Border(
                                bottom: BorderSide(
                                  color: Colors.grey.shade100,
                                ),
                              ),
                            ),
                            child:  TextFormField(
                              onChanged: (val)  {
                                setState(() {
                                  email = val;
                                });
                              },
                              style: const TextStyle(color: Colors.white),
                              decoration: const InputDecoration(
                                border: InputBorder.none,
                                hintText: "Username",
                                hintStyle: TextStyle(color: Color(0xff9cadbc)),
                              ),
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.symmetric(vertical: 10.0),
                            decoration: BoxDecoration(
                              border: Border(
                                bottom: BorderSide(
                                  color: Colors.grey.shade100,
                                ),
                              ),
                            ),
                            child:  TextFormField(
                              onChanged: (txt){
                                setState(() {
                                  pass=txt;
                                });
                              },
                              obscureText: true,
                              style: const TextStyle(color: Colors.white),
                              decoration: const InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "Password",
                                  hintStyle: TextStyle(color:Color(0xff9cadbc))),
                            ),
                          )
                        ],
                      ),
                    ),

                    const SizedBox(
                      height: 5.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: const [
                        Text(
                          "Forgot Password?",
                          style: TextStyle(
                            color: Color(0xff9cadbc),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 20.0,
                    ),

                    Container(
                      height: 50,
                      margin: EdgeInsets.symmetric(horizontal: 30),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: const Color.fromRGBO(156,173,188, 1),
                      ),
                      child: TextButton(
                        onPressed: () async {
                          if(email == '' && pass == ''){
                            displayToast("Please fill email and password", context);
                          }else if (email != '' && pass == ''){
                            displayToast("Please enter your password", context);
                          }else if(email == '' && pass != ''){
                            displayToast("Please enter your email", context);

                          }else{
                            showDialog<void>(
                              context: context,
                              barrierDismissible: true,
                              // false = user must tap button, true = tap outside dialog
                              builder: (BuildContext dialogContext) {
                                return Container(
                                  height: MediaQuery.of(context).size.height,
                                    width:  MediaQuery.of(context).size.width,
                                    color:Colors.black26,
                                    child: LoadingWidget());
                              },
                            );
                            await authcontroller.userLogin(email, pass);
                          }
                        },
                        child: const Center(
                          child: Text(
                            "Login",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ),

                    const SizedBox(
                      height: 20.0,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }


}


displayToast(String message,BuildContext? context){
  Fluttertoast.showToast(msg: message);
}