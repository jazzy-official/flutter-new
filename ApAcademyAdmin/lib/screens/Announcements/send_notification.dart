import 'package:ap_academy_admin/ModelView/Controller/GroupController.dart';
import 'package:ap_academy_admin/ModelView/Controller/NotificationController.dart';

import 'package:ap_academy_admin/ModelView/Controller/databaseController.dart';
import 'package:ap_academy_admin/ModelView/Controller/matchController.dart';
import 'package:checkbox_grouped/checkbox_grouped.dart';
import 'package:flutter/material.dart';
import 'package:ap_academy_admin/ModelView/Controller/teamsController.dart';
import 'package:ap_academy_admin/constants/util.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../constants/util.dart';
import '../shared/loading_widget.dart';

class SendNotification extends StatefulWidget {
  @override
  _SendNotificationState createState() => _SendNotificationState();
}

//send notification to all users who are participating in the specific event
class _SendNotificationState extends State<SendNotification> {
  final dbHelper = Get.put(DatabaseController());
  final nControl = Get.put(NotificationController());
  final mController = Get.put(MatchController());
  final gController = Get.put(GroupsControler());
  final tController = Get.put(TeamController());
//////////////////////////----------------controllers---------- //////////////
  ///5
  TextEditingController titleController = TextEditingController();
  TextEditingController notifyController = TextEditingController();
  TextEditingController desController = TextEditingController();

  ///////////////////////------------------ List drpdowns
  @override
  void initState() {
    super.initState();
    mController.selectedTeam1 = ''.obs;
  }

  List<String> typeDropDown = ['Matches', 'Trainings', 'Others'];
  List<String> allTokens = [];
  Future<void> saveNotifications() async {
    Map<String, dynamic> notifyModel = {
      'title': titleController.text,
      'description': desController.text,
      'timeCreated': DateTime.now()
    };

    await Get.find<NotificationController>()
        .sendAndRetrieveMessage({
      'title': titleController.text,
      'desc': desController.text,
    }, allTokens);
    bool temp = await nControl.addnewNotification(notifyModel);

    Get.defaultDialog(title: '', content: LoadingWidget(),backgroundColor: Colors.transparent);
    if (temp) {
      await nControl.sendAndRetrieveMessage(
          {"title": titleController.text, 'desc': desController.text},
          allTokens);
      Get.back();
      Get.back();
      Get.back();
      Get.snackbar("Sent", "Data is saved",
          snackPosition: SnackPosition.BOTTOM);


    } else {
      Get.dialog(AlertDialog(
          title: Text("Not saved"), content: Text("Save again Please")));
    }
  }
  int? _value = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyResources.backgroundColor,
      appBar: AppBar(
        backgroundColor: MyResources.buttonColor,
        title: Text(
          'ADD NOTIFICATION',
          style: GoogleFonts.roboto(),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            //title of news
            Padding(
              padding: const EdgeInsets.only(top: 20.0, left: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "  Title:",
                    style: MyResources.appTextStyle,
                  ),
                ],
              ),
            ),
            Padding(
              //Add padding around textfield
              padding: EdgeInsets.only(top: 7.0, left: 20, right: 20),
              child: Container(
                color: MyResources.backgroundColor,
                height: 50,
                child: TextField(
                  controller: titleController,
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),

            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    " Team ",
                    style: GoogleFonts.roboto(
                        fontSize: 15, fontWeight: FontWeight.w400),
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: Container(
                            decoration: BoxDecoration(
                                color: MyResources.backgroundColor,
                                border: Border.all(color: Colors.grey),
                                borderRadius: BorderRadius.circular(10)),
                            //  color: MyResources.backgroundColor,
                            // width: Get.width*0.5,
                            height: 50,
                            child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: MaterialButton(
                                  child: allTokens.length == 0
                                      ? Text('Select Group/Team')
                                      : Text(
                                          'Groups/Teams Selected',
                                          style: TextStyle(color: Colors.green),
                                        ),
                                  onPressed: _value == 0 ?  () async  {
                                    allTokens = [];
                                    List<dynamic> selectedGroup =
                                    await showDialogGroupedCheckbox(
                                        context: context,
                                        cancelDialogText: "cancel",
                                        isMultiSelection: true,
                                        itemsTitle: List.generate(
                                            gController.group.length,
                                                (index) =>
                                            "${gController.group[index]['teamName']}"),
                                        submitDialogText: "select",
                                        dialogTitle: Text("Select Groups"),
                                        values: List.generate(
                                            gController.group.length,
                                                (index) => index));
                                    print(selectedGroup);
                                    for (int i = 0; i < selectedGroup.length; i++) {
                                      await nControl
                                          .getTokensbyGroup(
                                          gController.group[i]['teamName'])
                                          .then((value) {
                                        value!.forEach((element2) {
                                          allTokens.add(element2);
                                        });
                                      });
                                    }
                                    setState(() {});
                                  }
                                  :
                                      () async  {
                                    allTokens = [];
                                    List<dynamic> selectedGroup =
                                    await showDialogGroupedCheckbox(
                                        context: context,
                                        cancelDialogText: "cancel",
                                        isMultiSelection: true,
                                        itemsTitle: List.generate(
                                            tController.teams.length,
                                                (index) =>
                                            "${tController.teams[index]['teamName']}"),
                                        submitDialogText: "select",
                                        dialogTitle: Text("Select Team"),
                                        values: List.generate(
                                            tController.teams.length,
                                                (index) => index));
                                    print(selectedGroup);
                                    for (int i = 0; i < selectedGroup.length; i++) {
                                     nControl
                                          .getTokensbyTeam(
                                          tController.teams[i]['teamName']).then((value) {
                                       print(value);
                                       value!.forEach((element2) {
                                         allTokens.add(element2);
                                       });
                                     });


                                    }
                                    setState(() {});
                                  },
                                ))),
                      ),
                      SizedBox(width: 10,),
                      Center(
                        child: Wrap(
                            children: [
                              ChoiceChip(
                                elevation: 1,
                                pressElevation: 2,
                                shadowColor: Colors.black,
                                label: Text('Group'),
                                labelPadding: EdgeInsets.symmetric(horizontal: 3,vertical: 10),
                                selected: _value == 0,
                                onSelected: (bool selected) {
                                  setState(() {
                                    _value = selected ? 0 : 0;
                                    print(_value.toString());
                                  });
                                },
                              ),
                              SizedBox(width: 10,),
                              ChoiceChip(
                                elevation: 1,
                                pressElevation: 2,
                                shadowColor: Colors.black,
                                labelPadding: EdgeInsets.symmetric(horizontal: 5,vertical: 10),
                                label: Text('Team'),
                                selected: _value == 1,
                                onSelected: (bool selected) {
                                  setState(() {
                                    _value = selected ? 1 : 1;
                                    print(_value.toString());
                                  });
                                },
                              )
                            ]
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 0.0, left: 18),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    " Description:",
                    style: MyResources.appTextStyle,
                  ),
                ],
              ),
            ),
            Padding(
              //Add padding around textfield
              padding: EdgeInsets.only(top: 7.0, left: 20, right: 20),
              child: Container(
                height: 120,
                color: MyResources.backgroundColor,
                child: TextField(
                  controller: desController,
                  keyboardType: TextInputType.multiline,
                  maxLines: 5,
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),

            //submit
            Padding(
              padding: EdgeInsets.only(top: 10.0, left: 10, right: 10),
              child: Container(
                width: Get.width * 0.9,
                height: 45,
                child: RaisedButton(
                  textColor: Colors.white,
                  color: MyResources.buttonColor,
                  child: Text(
                    "SAVE",
                  ),
                  onPressed: () async {
                    await saveNotifications();
                  },
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(10.0),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
