import 'package:ap_academy_admin/constants/util.dart';
import 'package:ap_academy_admin/screens/Events/match_screen.dart';
import 'package:ap_academy_admin/screens/Events/other_event.dart';
import 'package:ap_academy_admin/screens/Events/training_screen.dart';
import 'package:flutter/material.dart';

class EventScreen extends StatefulWidget {
  @override
  _EventScreenState createState() => _EventScreenState();
}

class _EventScreenState extends State<EventScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            toolbarHeight: 0,
            bottom:  TabBar(
                indicatorColor: MyResources.headingColor,
              labelColor: MyResources.buttonColor,
                tabs: [
              Tab(
                text: 'MATCHES',

              ),
              Tab(
                text: 'TRAININGS',
              ),
              Tab(
                text: 'OTHERS',
              ),
            ]),
          ),
          body: TabBarView(
            children: [
              MatchScreen(),
              TrainingScreen(),
              OtherScreen(),
            ],
          ),
        ));
  }
}
