import 'package:ap_academy_new/ModelView/controllers/auth_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/route_manager.dart';
import 'package:google_fonts/google_fonts.dart';
import 'link_sign_up.dart';
import 'login_screen.dart';
import 'package:ap_academy_new/Constants/util.dart';

String? U_id;

class SignupPage extends StatefulWidget {
  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  final authController = Get.put(AuthController());
  /////////////////////////////////Controllers////////////////////////////////////////////////////////
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passController = TextEditingController();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _crichqidController = TextEditingController();
  TextEditingController _passportController = TextEditingController();
  TextEditingController _passConfirmController = TextEditingController();
///////////////////////////////////Pass Visibility//////////////////////////////
  bool _passwordVisible1 = false;
  bool _passwordVisible2 = false;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;

  late String pass;
  late String cpass;
  /////////////////////////////Validator
  bool validator = true;
///////////////////////////////////Firebase Auth////////////////////////////////////////////////////////

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: myResources.backgroundColor,
      appBar: AppBar(
        backgroundColor: myResources.buttonColor,
        title: Container(
          child: Text(
            'SIGN UP',
            style: GoogleFonts.roboto(),
          ),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 10, bottom: 0),
                child: Container(
                  child: Image.asset(
                    "assets/logo.png",
                    width: 180,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 0.0),
                child: Center(
                  child: Text(
                    "Let's Get Started!",
                    style: GoogleFonts.roboto(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                      color: Colors.orange,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: Center(
                  child: Text(
                    "Create an Account to get all features ",
                    style: GoogleFonts.roboto(
                      fontSize: 15,
                    ),
                  ),
                ),
              ),
              Padding(
                //Add padding around textfield
                padding: EdgeInsets.only(top: 25.0, left: 10, right: 10),
                child: TextFormField(
                  controller: _nameController, //N
                  keyboardType: TextInputType.text,
                  validator: (String? arg) {
                    if (arg!.length < 3)
                      return 'Name characters must be more than 2                                                                                                                                                                                                                                                                                  charater';
                    else
                      return null;
                  },

                  // ame Controller
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    hintText: "Student Name",
                    hintStyle: myResources.hintfontStyle,
                    prefixIcon: Icon(Icons.account_circle),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
              Padding(
                //Add padding around textfield
                padding: EdgeInsets.only(top: 15.0, left: 10, right: 10),
                child: TextFormField(
                  controller: _emailController,
                  //Email Controller

                  keyboardType: TextInputType.emailAddress,
                  validator: (val) {
                    //email textfield vlidation
                    if (val!.isEmpty)
                      return "Please enter email";
                    else if (!val.contains("@"))
                      return "Please enter a valid email";
                    else
                      return null;
                  },
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    hintText: "Email (Enter any email)",
                    hintStyle: myResources.hintfontStyle,
                    prefixIcon: Icon(Icons.email),
                    // errorStyle: TextStyle(fontSize: 6),

                    /*errorText:
                        GetUtils.isEmail(_emailController.text)? null
                        : "true",*/
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
              Padding(
                //Add padding around textfield
                padding: EdgeInsets.only(top: 15.0, left: 10, right: 10),
                child: TextFormField(
                  controller: _crichqidController,
                  //Email Controller

                  keyboardType: TextInputType.emailAddress,
                  validator: (val) {
                    //email textfield vlidation
                    if (val!.isEmpty)
                      return "Please enter CricHQId";
                    else
                      return null;
                  },
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    hintText: "CRICHQ",
                    hintStyle: myResources.hintfontStyle,
                    prefixIcon: Icon(Icons.confirmation_number_outlined),
                    // errorStyle: TextStyle(fontSize: 6),

                    /*errorText:
                        GetUtils.isEmail(_emailController.text)? null
                        : "true",*/
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
              Padding(
                //Add padding around textfield
                padding: EdgeInsets.only(top: 15.0, left: 10, right: 10),
                child: TextFormField(
                  controller: _passportController,
                  //Email Controller

                  keyboardType: TextInputType.emailAddress,
                  validator: (val) {
                    //email textfield vlidation
                    if (val!.isEmpty)
                      return "Please enter Passport number";
                    else
                      return null;
                  },
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    hintText: "Passport",
                    hintStyle: myResources.hintfontStyle,
                    prefixIcon: Icon(Icons.fingerprint),
                    // errorStyle: TextStyle(fontSize: 6),

                    /*errorText:
                        GetUtils.isEmail(_emailController.text)? null
                        : "true",*/
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
              Padding(
                //Add padding around textfield
                padding: EdgeInsets.only(top: 15.0, left: 10, right: 10),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  obscureText: !_passwordVisible1,
                  controller: _passController, //// pass Controller
                  validator: (val) {
                    if (val!.length == 0) {
                      return ('Please enter password');
                    } else if (val.length <= 7) {
                      return ('Your password must be at least 8 characters long.');
                    } else {
                      return null;
                    }
                  },
                  onSaved: (val) => pass = val!,
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    hintText: "Password",
                    hintStyle: myResources.hintfontStyle,
                    suffixIcon: IconButton(
                        icon: Icon(
                          // Based on passwordVisible state choose the icon
                          _passwordVisible1
                              ? Icons.visibility
                              : Icons.visibility_off,
                        ),
                        onPressed: () {
                          // Update the state i.e. toogle the state of passwordVisible variable
                          setState(() {
                            _passwordVisible1 = !_passwordVisible1;
                          });
                        }),
                    prefixIcon: Icon(
                      Icons.lock,
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
              Padding(
                //Add padding around textfield
                padding: EdgeInsets.only(top: 15.0, left: 10, right: 10),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  obscureText: !_passwordVisible2,
                  controller: _passConfirmController,
                  validator: (val) {
                    if (val!.length == 0) {
                      return ('Please enter password');
                    } else if (val != _passController.text) {
                      return ('Password did not match');
                    }
                    return null;
                  },
                  onSaved: (val) => cpass = val!,

                  //pass Confirmation controller
                  decoration: InputDecoration(
                    //Add th Hint text here.
                    hintText: "Confirm Password",
                    hintStyle: myResources.hintfontStyle,
                    prefixIcon: const Icon(Icons.lock),
                    suffixIcon: IconButton(
                        icon: Icon(
                          // Based on passwordVisible state choose the icon
                          _passwordVisible2
                              ? Icons.visibility
                              : Icons.visibility_off,
                        ),
                        onPressed: () {
                          // Update the state i.e. toogle the state of passwordVisible variable
                          setState(() {
                            _passwordVisible2 = !_passwordVisible2;
                          });
                        }),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0, left: 10, right: 10),
                child: SizedBox(
                  width: Get.width * 0.9,
                  height: 45,
                  child: RaisedButton(
                    color: myResources.buttonColor,
                    textColor: Colors.white,
                    child: const Text(
                      "Sign Up",
                    ),
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        // If the form is valid, display a snackbar. In the real world,
                        // you'd often call a server or save the information in a database.
                        authController.createUser(
                          _nameController.text,
                          _emailController.text,
                          _passConfirmController.text,
                          _crichqidController.text,
                          _passportController.text,
                        );
                      }
                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0, left: 10, right: 10),
                child: SizedBox(
                  width: Get.width * 0.9,
                  height: 45,
                  child: RaisedButton(
                    color: Colors.green[200],
                    //  textColor: Colors.white,
                    child: const Text(
                      "Sign up as Parent",
                    ),
                    onPressed: () async {
                      Get.to(LinkSignup());
                    },
                    shape: RoundedRectangleBorder(
                      borderRadius:  BorderRadius.circular(10.0),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Already have an Account?",
                        style: GoogleFonts.roboto(
                          fontSize: 14,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                      MaterialButton(
                          onPressed: () {
                            Get.offAll(LoginPage());
                          },
                          child: Text(
                            " Log In",
                            style: myResources.linkStyle,
                          ))
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
