import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Screens/User%20Forms/habits_form.dart';

class ConfirmProfile extends StatelessWidget {
  const ConfirmProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Expanded(flex: 1,child: SizedBox()),
                      const Expanded(flex:6,child: Center(child: Text("Your profile is set up !",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),))),
                      Expanded(
                          flex: 1,
                          child: TextButton(onPressed: () {}, child: Text("Skip",style: TextStyle(color: Colors.black38),)))
                    ],
                  ),
                  Container(
                    // height: 250,
                    margin: EdgeInsets.symmetric(vertical: 40),
                    decoration: BoxDecoration(border: Border.all(color: myResources.themeColor),
                    ),
                    width: 250,
                    child: Image.asset("assets/confirmsteps.png",),
                  ),
                  const Text("One more Step",style: TextStyle(fontSize: 22,fontWeight: FontWeight.bold),),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15.0,vertical: 10),
                    child: Text("Add your habits, interests and personality traits to complete your profile. You can also skip this step for now and complete in the profile section later.",style: TextStyle(fontSize: 13,color: Colors.black54),),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: myResources.themeColor,
                    shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(14))),
                  ),
                  onPressed: () {
                    Get.to(()=> const HabitsForm());
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        vertical: 8, horizontal: 8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Confirm',
                          style: TextStyle(color: Colors.white),
                        ),
                        Container(
                          padding: const EdgeInsets.all(8),
                          decoration: BoxDecoration(
                            borderRadius:
                            const BorderRadius.all(Radius.circular(20)),
                            color: myResources.themeColor,
                          ),
                          child: Icon(
                            Icons.arrow_forward_ios,
                            color: Colors.white,
                            size: 16,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
