

import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:state_management/Models/cart_item_model.dart';
import 'package:state_management/Models/order_model.dart';

class OrderController extends GetxController {
  List<Order> _orders = [];

  List<Order> get orders {
    return [..._orders];
  }

  void addOrder(List<CartItem> cartProducts, double total) {
    _orders.insert(
        0,
        Order(
            id: DateTime.now().toString(),
            products: cartProducts,
            amount: total,
            dateTime: DateTime.now()));
    update();
  }
}