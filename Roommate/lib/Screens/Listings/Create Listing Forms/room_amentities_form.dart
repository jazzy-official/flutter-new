import 'package:chips_choice_null_safety/chips_choice_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:room_buddy/Constants/util.dart';
import 'package:room_buddy/Screens/Listings/Create%20Listing%20Forms/room_availability_price_form.dart';
import 'package:room_buddy/controllers/my_listing_controller.dart';

class RoomAmenities extends StatelessWidget {
   RoomAmenities({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          iconTheme: const IconThemeData(color: Colors.black),
          title: Text("Step 3: Select amenities",style: TextStyle(color: Colors.black),),
          bottom: PreferredSize(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 22),
              child: LinearProgressIndicator(
                color: myResources.themeColor,
                backgroundColor: Colors.grey.shade300,
                minHeight: 6,
                value: 3 / 6,
              ),
            ),
            preferredSize: const Size.fromHeight(50),
          )),
      body: GetBuilder<MyListingController>(
        init: MyListingController(),
        builder: (cont) => Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5.0,vertical: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ChipsChoice<String>.multiple(
                value: cont.amenities,
                wrapped: true,
                onChanged: (val) => {
                  cont.amenities = val,
                  cont.update(),
                },
                // setState(() => tags = val),
                choiceItems: C2Choice.listFrom<String, String>(
                  source: cont.options,
                  value: (i, v) => v,
                  label: (i, v) => v,
                ),
                choiceStyle: C2ChoiceStyle(
                  elevation: 2,
                  color: myResources.themeColor,

                ),
                choiceActiveStyle:  C2ChoiceStyle(
                  color: myResources.themeColor,
                  brightness: Brightness.dark,
                ),
              ),
              const Expanded(child: SizedBox()),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: myResources.themeColor,
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(14))),
                ),
                onPressed: () {
                  Get.to(RoomAvailabiltyForm());
                },
                child: Container(
                  padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SizedBox(width: 20,),
                      Text(
                        'Proceed',
                        style: TextStyle(color: Colors.white),
                      ),
                      Container(
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                          borderRadius:
                          const BorderRadius.all(Radius.circular(20)),
                          color: myResources.themeColor,
                        ),
                        child: Icon(
                          Icons.arrow_forward_ios,
                          color: Colors.white,
                          size: 16,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
